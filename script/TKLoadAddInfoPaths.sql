USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadAddInfoPaths]    Script Date: 22/05/2020 14:56:51 ******/
DROP PROCEDURE [dbo].[TKLoadAddInfoPaths]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadAddInfoPaths]    Script Date: 22/05/2020 14:56:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKLoadAddInfoPaths]
AS
BEGIN
	DECLARE @Roots CURSOR
	DECLARE @AddInfoId INT
	DECLARE @title NVARCHAR(4000)

	SET NOCOUNT ON;  

	delete TKAddInfoPaths;

	SET @Roots = CURSOR FOR
	SELECT TKAddInfoId, Title FROM TKAddInfos where ParentId is null;
	OPEN @Roots
	FETCH NEXT
	FROM @Roots INTO @AddInfoId, @title
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Inserimento puntamento alla sezione stessa 
		INSERT INTO TKAddInfoPaths (TKAddInfoIdRef, TKAddInfoId, BreadCrumb, RootTitle)
		VALUES (@AddInfoId, @AddInfoId, @title, @title);
		-- inserimento di tutti i figli presenti (ricorsiva)
		-- PRINT N'ROOT: '  + STR(@AddInfoId) + ' ' + @title;
		EXEC [TKLoadAddInfoPathsChilds]  @AddInfoIdRef=@AddInfoId, 
										 @AddInfoId=@AddInfoId, 
										 @title=@title, 
										 @rootTitle=@title
		-- PRINT N'ROOT END: '  + STR(@AddInfoId) + ' ' + @title;
		FETCH NEXT
		FROM @Roots INTO @AddInfoId, @title
	END
	CLOSE @Roots
	DEALLOCATE @Roots
END


GO


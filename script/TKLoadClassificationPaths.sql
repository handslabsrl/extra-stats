USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClassificationPaths]    Script Date: 22/05/2020 14:56:51 ******/
DROP PROCEDURE [dbo].[TKLoadClassificationPaths]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClassificationPaths]    Script Date: 22/05/2020 14:56:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKLoadClassificationPaths]
AS
BEGIN
	DECLARE @Roots CURSOR
	DECLARE @ClassificationId INT
	DECLARE @title NVARCHAR(4000)

	SET NOCOUNT ON;  

	delete TKClassificationPaths;

	SET @Roots = CURSOR FOR
	SELECT TKClassificationId, Title FROM TKClassifications where ParentId is null;
	OPEN @Roots
	FETCH NEXT
	FROM @Roots INTO @ClassificationId, @title
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Inserimento puntamento alla sezione stessa 
		INSERT INTO TKClassificationPaths (TKClassificationIdRef, TKClassificationId, BreadCrumb, RootTitle, RootId)
		VALUES (@ClassificationId, @ClassificationId, @title, @title, @ClassificationId);
		-- inserimento di tutti i figli presenti (ricorsiva)
		PRINT N'ROOT: '  + STR(@ClassificationId) + ' ' + @title;
		EXEC [TKLoadClassificationPathsChilds]  @ClassificationIdRef=@ClassificationId, 
												@ClassificationId=@ClassificationId, 
												@title=@title,
												@rootTitle=@title,
												@rootId=@ClassificationId
		PRINT N'ROOT END: '  + STR(@ClassificationId) + ' ' + @title;
		FETCH NEXT
		FROM @Roots INTO @ClassificationId, @title
	END
	CLOSE @Roots
	DEALLOCATE @Roots
END


GO


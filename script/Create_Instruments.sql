USE [ELITech_ExtraStats]
GO

/****** Object:  Table [dbo].[Instruments]    Script Date: 15/04/2018 19:34:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Instruments](
	[SerialNumber] [nvarchar](50) NOT NULL,
	[CustomerCode] [nvarchar](10) NULL,
	[CompanyName] [nvarchar](250) NULL,
	[Site_Code] [nvarchar](10) NULL,
	[Site_Description] [nvarchar](250) NULL,
	[Site_Address] [nvarchar](250) NULL,
	[Site_City] [nvarchar](250) NULL,
	[Site_Country] [nvarchar](250) NULL,
	[Installation_Date] [datetime2](7) NULL,
	[Commercial_Status] [nvarchar](250) NULL,
	[Finance_Status] [nvarchar](50) NULL,
	[REGION] [varchar](50) NULL,
	[AREA] [varchar](50) NULL,
 CONSTRAINT [PK_Instruments] PRIMARY KEY CLUSTERED 
(
	[SerialNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


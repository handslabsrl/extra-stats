USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[DRLoadSectionPaths]    Script Date: 22/05/2020 14:56:51 ******/
DROP PROCEDURE [dbo].[DRLoadSectionPaths]
GO

/****** Object:  StoredProcedure [dbo].[DRLoadSectionPaths]    Script Date: 22/05/2020 14:56:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DRLoadSectionPaths]
AS
BEGIN
	DECLARE @Roots CURSOR
	DECLARE @SectionId INT
	DECLARE @title NVARCHAR(4000)

	SET NOCOUNT ON;  

	delete DRSectionPaths;

	SET @Roots = CURSOR FOR
	SELECT DRSectionId, Title FROM DRSections where ParentId is null;
	OPEN @Roots
	FETCH NEXT
	FROM @Roots INTO @SectionId, @title
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Inserimento puntamento alla sezione stessa 
		INSERT INTO DRSectionPaths (DRSectionIdRef, DRSectionId, BreadCrumb)
		VALUES (@SectionId, @SectionId, @title);
		-- inserimento di tutti i figli presenti (ricorsiva)
		PRINT N'ROOT: '  + STR(@SectionId) + ' ' + @title;
		EXEC [DRLoadSectionPathsChilds]  @SectionIdRef=@SectionId, @SectionId=@SectionId, @title=@title
		PRINT N'ROOT END: '  + STR(@SectionId) + ' ' + @title;
		FETCH NEXT
		FROM @Roots INTO @SectionId, @title
	END
	CLOSE @Roots
	DEALLOCATE @Roots
END


GO


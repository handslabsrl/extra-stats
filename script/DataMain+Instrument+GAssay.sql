-- DataMain + INSTRUMENT + GassayPrograms (INNER JOIN)
select * from DataMain 
INNER join INSTRUMENTS
ON INSTRUMENTS.SerialNUmber = DataMAIN.SerialNUmber
INNER JOIN GAssayPrograms
ON GAssayPrograms.Serial = DataMAIN.SerialNUmber
AND GAssayPrograms.AssayId = DataMain.GeneralAssayID

-- DataMain Senza riferimento GAssayPrograms
select * from DataMain 
INNER join INSTRUMENTS
ON INSTRUMENTS.SerialNUmber = DataMAIN.SerialNUmber
LEFT JOIN GAssayPrograms
ON GAssayPrograms.Serial = DataMAIN.SerialNUmber
AND GAssayPrograms.AssayId = DataMain.GeneralAssayID
WHERE GAssayPrograms.AssayId  IS NULL 

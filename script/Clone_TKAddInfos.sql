
declare @LastId int
select @LastId=max(TKAddInfoId) from TKAddInfos

declare @RootId int
select @RootId = TKAddInfoId from TKAddInfos where ParentId IS NULL and Title = 'FAULTS InG'

print CONCAT('Rootid=', @RootId)
print CONCAT('LastId=', @LastId)
DBCC CHECKIDENT ('TKAddInfos')

--SET IDENTITY_INSERT TKAddInfos ON 
--Insert into TKAddInfos([TKAddInfoId], [Code],[Deleted],[Reserved],[ParentId],[Title],[TmstInse],[TmstLastUpd],[UserInse],[UserLastUpd])
--select A.TKAddInfoId + @LastId, Replace(A.Code, 'InG_', 'BeG_'), A.Deleted, A.Reserved, A.ParentId + @LastId,
--Replace(A.title, 'InG_', 'BeG_'), getDate(), getDate(), 'D.Millone', 'D.Millone'
--from TKAddInfos A
--INNER JOIN TKAddInfoPaths 
--ON A.TKAddInfoId  = TKAddInfoPaths.TKAddInfoId
-- where TKAddInfoIdRef = @RootId
-- SET IDENTITY_INSERT TKAddInfos OFF

--select @LastId=max(TKAddInfoId) from TKAddInfos
--DBCC CHECKIDENT ('TKAddInfos', RESEED,@LastId)

--exec TKLoadAddInfoPaths

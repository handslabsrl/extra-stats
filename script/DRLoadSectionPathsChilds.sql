USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[DRLoadSectionPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
DROP PROCEDURE [dbo].[DRLoadSectionPathsChilds]
GO

/****** Object:  StoredProcedure [dbo].[DRLoadSectionPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DRLoadSectionPathsChilds]
	@SectionIdRef INT, 
	@SectionId INT, 
	@title NVARCHAR(max)
AS
BEGIN
	DECLARE @Childs CURSOR
	DECLARE @SectionIdChild INT
	DECLARE @ParentIdChild INT
	DECLARE @titleChild NVARCHAR(max)
	DECLARE @percorso NVARCHAR(max)

	SET NOCOUNT ON;  

	--PRINT N'*NEW* ' + STR(@SectionIdRef) + ' ' + STR(@SectionId)  + ' ' + @title;  
	
	SET @Childs = CURSOR FOR
	SELECT DRSectionId, ParentId, Title FROM DRSections where ParentId = @SectionId
	OPEN @Childs
	FETCH NEXT
	FROM @Childs INTO @SectionIdChild, @ParentIdChild, @titleChild
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @percorso = CONCAT(@title, ' > ', @titleChild);
		-- Inserimento percorso 
		PRINT N'STEP/1 - Rif:' + STR(@SectionIdRef) + ' Child:' + STR(@SectionIdChild) + ' Parent:' + STR(@ParentIdChild) ;  
		IF NOT EXISTS (SELECT * FROM DRSectionPaths WHERE DRSectionIdRef = @SectionIdRef and DRSectionId = @SectionIdChild) 
		BEGIN 
			INSERT INTO DRSectionPaths (DRSectionIdRef, DRSectionId, BreadCrumb)
			VALUES (@SectionIdRef, @SectionIdChild, @percorso);
		END 
		
		-- inserimento percorso per tutti i nodi figli (con riferimento alla root passata)
		EXEC [DRLoadSectionPathsChilds] @SectionIdRef, @SectionIdChild,  @percorso

		-- Inserimento puntamento a se stesso (se non esiste)
		IF NOT EXISTS (SELECT * FROM DRSectionPaths WHERE DRSectionIdRef = @SectionIdChild and DRSectionId = @SectionIdChild) 
		BEGIN 
			INSERT INTO DRSectionPaths (DRSectionIdRef, DRSectionId, BreadCrumb)
			VALUES (@SectionIdChild, @SectionIdChild, @percorso);
		END 

		-- inserimento di tutti i figli presenti con riferimento a se stesso 
		PRINT N'STEP/2 - Rif:' + STR(@SectionIdChild) + ' ' + @percorso;  
		EXEC [DRLoadSectionPathsChilds] @SectionIdChild, @SectionIdChild,  @percorso

		FETCH NEXT
		FROM @Childs INTO @SectionIdChild, @ParentIdChild, @titleChild
	END
	CLOSE @Childs
	DEALLOCATE @Childs
END


GO


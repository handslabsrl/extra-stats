CREATE TABLE DRSections
(  
DRSectionId smallint NOT NULL,  
ParentId smallint NULL,  
Title nvarchar(50) NOT NULL,
Locked bit NULL DEFAULT (0)
 CONSTRAINT PK_DRSections PRIMARY KEY CLUSTERED (DRSectionId ASC)   
);  

CREATE TABLE DRSectionPaths
(  
DRSectionIdRef smallint NOT NULL,    
DRSectionId smallint NOT NULL,  
BreadCrumb nvarchar(max ) NOT NULL
 CONSTRAINT PK_DRSectionPaths PRIMARY KEY CLUSTERED (DRSectionIdRef ASC, DRSectionId ASC)   
);  

DELETE FROM DRSections

INSERT INTO DRSections (DRSectionId, ParentId, Title) VALUES   
 (1, NULL, N'ELITe InGenius')
,(2, 1, N'Assay Protocols')  
,(3, 1, N'EXTRA')  
,(4, 1, N'SW Version')  
,(5, 1, N'TAB')  
,(6, 1, N'TBS')  
,(7, 2, N'CLOSE')  
,(8, 2, N'OPEN')  
,(9, 3, N'v 1.3.0.12')  
,(10, NULL,  N'ELITe Star')  
,(11, 10,  N'Service Manual')
,(12, NULL,  N'Remote Access')    
,(13, 12,  N'BeyondTrust Jump Client')    
,(14, 12,  N'BeyondTrust User Manual')    
,(15, 13,  N'Argentina')    
,(16, 13,  N'Australia')    
,(17, 13,  N'Benelux ')    
,(18, 13,  N'Biomedica')    
,(19, 13,  N'Chile')    
,(20, 13,  N'France')
,(21, 20,  N'France1')
,(22, 21,  N'France2')
,(23, 22,  N'France3')
,(24, 23,  N'France4')
,(30, 6, N'TBS1') 
,(31, 6, N'TBS2') 
,(32, 6, N'TBS3') 
,(33, 31, N'TBS2.1') 
,(34, 31, N'TBS2.2') 
,(35, 31, N'TBS2.3') 
,(36, 31, N'TBS2.2.1') 

GO


with tree(DRSectionId, ParentId, Title) as
(
select DRSectionId, ParentId, Title from DRSections --where DRSectionId=1 -- id del padre
union all
select a.DRSectionId, a.ParentId, a.Title from DRSections a
inner join tree t on t.DRSectionId=a.ParentId
)
select DRSectionId, ParentId, Title from tree    




--exec DRLoadSectionPaths
--select * from DRSections 
--select * from DRSectionPaths


--Exec DRSetSectionLocked 4,1
--select * from DRSections


select DRSectionPaths.*, Title, Locked from DRSectionPaths
INNER JOIN DRSections 
On DRSections.DRSectionId = DRSectionPaths.DRSectionId
Insert into GAssayPrograms ([AssayName]
      ,[SampleTypes]
      ,[IVDCleared]
      ,[CtsAndTmOnly]
      ,[Pathogen_TargetName]
      ,[ExtractionInputVolume]
      ,[ExtractedEluteVolume]
      ,[ResultsScaling]
      ,[SonicationOnTime]
      ,[SonicationOffTime]
      ,[SonicationCycle]
      ,[ExtractionCassetteID]
      ,[PCRCassetteID]
      ,[PCRInputEluateVolume]
      ,[PreCycleStepNumber]
      ,[PCRAmplificationCycles]
      ,[PCRAmplificationSteps]
      ,[MeltRequired]
      ,[MeltingOptional]
      ,[StartRampTemperature]
      ,[EndRampTemperature]
      ,[RampRate]
      ,[UpperTolerance]
      ,[LowerTolerance]
      ,[OffSet]
      ,[ConversionFactorIU]
      ,[UserName]
      ,[RegisteredDate]
      ,[SampleMatrix_Id]
      ,[AssayParameterToolVersion]
      ,[Serial]
      ,[AssayId]
      ,[FileName]
      ,[SampleTypesDecode]
      ,[Pathogen_TargetName_Norm]
      ,[RevisionExtra]
      ,[ExtractionData]
      ,[AssayProtocolStatus])
Select Distinct 'Record Ghost'
      ,0
      ,0
      ,0
      ,'Record Ghost'
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,Null
      ,Null
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,0
      ,'wpratini'
      ,Convert(varchar(25), getdate(), 120) 
      ,Null
      ,Null
      ,SerialNumber
      ,GeneralAssayID
      ,'PLSQL'
      ,Null
      ,'Record Ghost'
      ,'2.0.0.0'
      ,getdate()
      ,Null
from DataMain 
where not exists (select * from GAssayPrograms Where AssayId = GeneralAssayId and SerialNumber = Serial) 
/****** Object:  View [dbo].[v_TicketStandBy]    Script Date: 2/13/2021 3:06:34 PM ******/
DROP VIEW [dbo].[v_TicketStandBy]
GO

/****** Object:  View [dbo].[v_TicketStandBy]    Script Date: 2/13/2021 3:06:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[v_TicketStandBy]
AS
SELECT DISTINCT TKRequestId, Object, TmstInse, UserRegistration, SerialNumber, COUNTRY, CITY, USERREGISTRATION_NAME, RootTitle, TMSTLASTEVT
FROM            (SELECT        REQ.TKRequestId, REQ.Object, REQ.TmstInse, REQ.UserRegistration, REQ.SerialNumber, INSTR.Site_Country AS COUNTRY, INSTR.Site_City AS CITY, COALESCE (USER_REG.fullName, 
                                                    REQ.UserRegistration) AS USERREGISTRATION_NAME, PATH.RootTitle, COALESCE
                                                        ((SELECT        MAX(TmstInse) AS Expr1
                                                            FROM            dbo.TKEvents AS EVT
                                                            WHERE        (TKRequestId = REQ.TKRequestId) AND (Operation IN ('SUSPEND'))), REQ.TmstInse) AS TMSTLASTEVT
                          FROM            dbo.TKRequests AS REQ LEFT OUTER JOIN
                                                    dbo.Instruments AS INSTR ON INSTR.SerialNumber = REQ.SerialNumber INNER JOIN
                                                    dbo.TKClassificationPaths AS PATH ON PATH.TKClassificationId = REQ.TKClassificationId INNER JOIN
                                                    dbo.AspNetUsers AS USERCFG_REG ON REQ.UserRegistration = USERCFG_REG.UserName INNER JOIN
                                                    dbo.UserCfg AS USER_REG ON USERCFG_REG.Id = USER_REG.UserID LEFT OUTER JOIN
                                                    dbo.AspNetUsers AS USERCFG_WIP ON REQ.UserInChargeOf = USERCFG_WIP.UserName LEFT OUTER JOIN
                                                    dbo.UserCfg AS USER_WIP ON USERCFG_WIP.Id = USER_WIP.UserID
                          WHERE        (REQ.Status = 'STANDBY')) AS TMP
WHERE        (CAST(TMSTLASTEVT AS DATE) < CAST(DATEADD(d, - 7, GETDATE()) AS DATE))
GO


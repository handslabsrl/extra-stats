select StartRunTime, COUNT(*) AS num_run,
SUM(Process_PCR) as process_prc, SUM(process_Extr) as process_Extr
from (
  select StartRunTime, IIF(Process IN (0, 1) , 1, 0) AS Process_PCR, IIF(Process in (1, 2) , 1, 0) AS process_Extr 
  from DataMain
INNER JOIN SGAT 
ON DataMain.SerialNumber = SGAT.SerialNumber
INNER JOIN GAssayPrograms 
ON DataMain.SerialNumber = GAssayPrograms.Serial
AND DataMain.GeneralAssayID = GAssayPrograms.AssayId

WHERE 1 = 1
AND   DataMain.SerialNumber = '2071603B0069E'
AND SGAT.CustomerCode = 'C00325' 
AND SGAT.Site_Code = '000'
AND SGAT.Site_Country = 'Italy'
AND GAssayPrograms.AssayID = 18
) AS TMP
--where StartRunTime > '2016-12-09' and StartRunTime < '2017-01-01'
GROUP BY StartRunTime


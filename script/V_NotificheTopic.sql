--DROP VIEW V_NotificheTopic
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW V_NotificheTopic AS 
select DRTopics.DRTopicId, DRTopics.Code, DRTopics.Revision, DRTopics.Title, DRTopics.NotifyChange, DRTopics.DRSectionId, 
	   DRTopics.TmstLastUpdResources, DRTopics.Note, 
	   RootSection.DRSectionId as RootSectionId,
	   DRSectionPaths.BreadCrumb, COALESCE(UserCfg.fullName, AspNetUsers.UserName) AS UserName,  
	   AspNetUsers.Email, AspNetUsers.id as Userid, Departments.DepartmentId as JobPositionId, Departments.Description as JobPosition
from DRTopics 
INNER JOIN DRSectionPaths 
ON DRTopics.DRSectionId = DRSectionPaths.DRSectionId
INNER JOIN DRSections as RootSection
ON RootSection.DRSectionId = DRSectionPaths.DRSectionIdRef
AND RootSection.ParentId IS NULL
INNER JOIN UserLinks 
ON UserLinks.LinkID = RootSection.DRSectionId
AND  UserLinks.TypeId = 3     -- abilitazione UserLinkType.DRSection (main)
INNER JOIN AspNetUsers 
 ON AspNetUsers.Id = UserLinks.UserID
 and AspNetUsers.Email IS NOT NULL
 and AspNetUsers.LockoutEnd IS NULL
INNER JOIN UserCfg 
 ON AspNetUsers.Id = UserCfg.UserID
INNER JOIN Departments 
 on UserCfg.DepartmentId = Departments.DepartmentId
 AND Departments.ReceiveNewsWebRepository = 1

GO





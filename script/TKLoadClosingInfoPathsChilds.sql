USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClosingInfoPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
DROP PROCEDURE [dbo].[TKLoadClosingInfoPathsChilds]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClosingInfoPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKLoadClosingInfoPathsChilds]
	@ClosingInfoIdRef INT,
	@ClosingInfoId INT,
	@title NVARCHAR(max),
	@rootTitle NVARCHAR(max)
AS
BEGIN
	DECLARE @Childs CURSOR
	DECLARE @ClosingInfoIdChild INT
	DECLARE @ParentIdChild INT
	DECLARE @titleChild NVARCHAR(max)
	DECLARE @percorso NVARCHAR(max)

	SET NOCOUNT ON;

	SET @Childs = CURSOR FOR
	SELECT TKClosingInfoId, ParentId, Title FROM TKClosingInfos where ParentId = @ClosingInfoId
	OPEN @Childs
	FETCH NEXT
	FROM @Childs INTO @ClosingInfoIdChild, @ParentIdChild, @titleChild
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @percorso = CONCAT(@title, ' > ', @titleChild);
		-- Inserimento percorso
		-- PRINT N'STEP/1 - Rif:' + STR(@ClosingInfoIdRef) + ' Child:' + STR(@ClosingInfoIdChild) + ' Parent:' + STR(@ParentIdChild) ;
		IF NOT EXISTS (SELECT * FROM TKClosingInfoPaths WHERE TKClosingInfoIdRef = @ClosingInfoIdRef and TKClosingInfoId = @ClosingInfoIdChild)
		BEGIN
			INSERT INTO TKClosingInfoPaths (TKClosingInfoIdRef, TKClosingInfoId, BreadCrumb, RootTitle)
			VALUES (@ClosingInfoIdRef, @ClosingInfoIdChild, @percorso, @rootTitle);
		END

		-- inserimento percorso per tutti i nodi figli (con riferimento alla root passata)
		EXEC [TKLoadClosingInfoPathsChilds]  @ClosingInfoIdRef=@ClosingInfoIdRef,
												 @ClosingInfoId=@ClosingInfoIdChild,
												 @title=@percorso,
												 @rootTitle=@rootTitle

		-- Inserimento puntamento a se stesso (se non esiste)
		IF NOT EXISTS (SELECT * FROM TKClosingInfoPaths WHERE TKClosingInfoIdRef = @ClosingInfoIdChild and TKClosingInfoId = @ClosingInfoIdChild)
		BEGIN
			INSERT INTO TKClosingInfoPaths (TKClosingInfoIdRef, TKClosingInfoId, BreadCrumb, RootTitle)
			VALUES (@ClosingInfoIdChild, @ClosingInfoIdChild, @percorso, @rootTitle);
		END

		-- inserimento di tutti i figli presenti con riferimento a se stesso
		-- PRINT N'STEP/2 - Rif:' + STR(@ClosingInfoIdChild) + ' ' + @percorso;
		EXEC [TKLoadClosingInfoPathsChilds]  @ClosingInfoIdRef=@ClosingInfoIdChild,
												 @ClosingInfoId=@ClosingInfoIdChild,
												 @title=@percorso,
												 @rootTitle=@rootTitle
		FETCH NEXT
		FROM @Childs INTO @ClosingInfoIdChild, @ParentIdChild, @titleChild
	END
	CLOSE @Childs
	DEALLOCATE @Childs
END


GO


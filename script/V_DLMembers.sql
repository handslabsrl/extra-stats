SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW V_DLMembers  AS 

SELECT COALESCE(ASPNETUSERS.EMAIL, DLMEMBERS.EXTEMAIL)  AS EMAIL, DLLISTS.DLTYPE, DLLISTS.DLListId, 
			DLMEMBERS.USERID,ASPNETUSERS.UserName
  FROM DLLISTS
INNER JOIN DLMEMBERS ON DLMEMBERS.DLLISTID = DLLISTS.DLLISTID
LEFT JOIN ASPNETUSERS ON ASPNETUSERS.ID= DLMEMBERS.USERID
WHERE DLLISTS.DELETED = 0
GO





USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKSetAddInfoReserved]    Script Date: 22/05/2020 14:57:10 ******/
DROP PROCEDURE [dbo].[TKSetAddInfoReserved]
GO

/****** Object:  StoredProcedure [dbo].[TKSetAddInfoReserved]    Script Date: 22/05/2020 14:57:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKSetAddInfoReserved]
	@AddInfoId INT, 
	@Reserved bit
AS
BEGIN
	DECLARE @tree CURSOR
	SET NOCOUNT ON;  

	with tree(TKAddInfoId, ParentId, Title) as
		(
		select TKAddInfoId, ParentId, Title from TKAddInfos where TKAddInfoId=@AddInfoId
		union all
		select a.TKAddInfoId, a.ParentId, a.Title from TKAddInfos a
		inner join tree t on t.TKAddInfoId=a.ParentId
		)
		UPDATE TKAddInfos SET Reserved = @Reserved
		FROM tree where tree.TKAddInfoId = TKAddInfos.TKAddInfoId
END
GO


USE [ELITech_ExtraStats]
GO

/****** Object:  Table [dbo].[DataMain]    Script Date: 15/04/2018 19:34:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DataMain](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RunSetupID] [int] NULL,
	[TrackNumber] [int] NULL,
	[SampleIDString] [nvarchar](250) NULL,
	[GeneralAssayID] [int] NULL,
	[AssayName] [nvarchar](250) NULL,
	[SampleTypes] [int] NULL,
	[SampleMatrixName] [nvarchar](250) NULL,
	[Process] [int] NOT NULL,
	[PositionToPlaceSample] [int] NOT NULL,
	[Sonication] [int] NOT NULL,
	[SonicationOnTime] [int] NULL,
	[SonicationOffTime] [int] NULL,
	[SonicationCycle] [int] NULL,
	[Melt] [int] NULL,
	[RefEluteTrack] [int] NULL,
	[DilutionFactor] [int] NULL,
	[SerialNumber] [nvarchar](50) NOT NULL,
	[PCRReactionCassetteSerialNumber] [nvarchar](250) NULL,
	[ExtractionCassetteLot] [nvarchar](250) NULL,
	[ExtractionCassetteExpiryDate] [datetime2](7) NULL,
	[ExtractionCassetteSerialNumber] [nvarchar](250) NULL,
	[ReagentLot] [nvarchar](250) NULL,
	[ReagentExpiryDate] [datetime2](7) NULL,
	[ReagentTubeSize] [int] NULL,
	[ICLot] [nvarchar](250) NULL,
	[IcExpiryDate] [datetime2](7) NULL,
	[IcTubeSize] [int] NULL,
	[CalibratorLot] [nvarchar](250) NULL,
	[CalibratorExpiryDate] [datetime2](7) NULL,
	[ControlLot] [nvarchar](250) NULL,
	[ControlExpiryDate] [datetime2](7) NULL,
	[MeasuredCalibrationResultID] [int] NULL,
	[MeasuredControlResultID] [int] NULL,
	[AssayDetail] [nvarchar](500) NULL,
	[LISUploadSelected] [int] NULL,
	[LISStatusv] [int] NULL,
	[ApprovalUserName] [nvarchar](250) NULL,
	[ApprovalUserRole] [int] NULL,
	[ApprovalDateTime] [datetime2](7) NULL,
	[ApprovalStatus] [int] NULL,
	[AssayDetailForCopiesPerMl] [nvarchar](250) NULL,
	[AssayDetailForGeqPerMl] [nvarchar](250) NULL,
	[AssayDetailForIuPerMl] [nvarchar](250) NULL,
	[AssayDetailShortening] [nvarchar](250) NULL,
	[AssayDetailShorteningForCopiesPerMl] [nvarchar](250) NULL,
	[AssayDetailShorteningForGeqPerMl] [nvarchar](250) NULL,
	[AssayDetailShorteningForIuPerMl] [nvarchar](250) NULL,
	[AssayDetailShorteningUnit] [nvarchar](250) NULL,
	[RunName] [nvarchar](250) NULL,
	[UserName] [nvarchar](250) NULL,
	[StartRunTime] [datetime2](7) NULL,
	[EndRunTime] [datetime2](7) NULL,
	[AbortTime] [datetime2](7) NULL,
	[ExtractionInputVolume] [int] NULL,
	[ExtractedEluteVolume] [int] NULL,
	[InventoryBlockConfirmationMethod] [nvarchar](250) NULL,
	[InventoryBlockNumber] [int] NULL,
	[InventoryBlockName] [nvarchar](250) NULL,
	[InventoryBlockBarcode] [nvarchar](250) NULL,
	[RunStatus] [nvarchar](1) NULL,
	[FileName] [nvarchar](250) NULL,
 CONSTRAINT [PK_DataMain] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[DataMain]  WITH CHECK ADD  CONSTRAINT [FK_DataMain_INSTRUMENTS_SerialNumber] FOREIGN KEY([SerialNumber])
REFERENCES [dbo].[Instruments] ([SerialNumber])
GO

ALTER TABLE [dbo].[DataMain] CHECK CONSTRAINT [FK_DataMain_INSTRUMENTS_SerialNumber]
GO

ALTER TABLE [dbo].[DataMain] ADD SampleTypesDecode [nvarchar](50), ProcessDecode [nvarchar](50), MeltDecode [nvarchar](50), ApprovalUserRoleDecode [nvarchar](50);
-- CODIFICHE 
UPDATE DATAMAIN SET SAMPLETYPES = 0 WHERE SAMPLETYPESDECODE = 'Sample' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET SAMPLETYPES = 1 WHERE SAMPLETYPESDECODE = 'Calibrator' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET SAMPLETYPES = 2 WHERE SAMPLETYPESDECODE = 'Control' AND RUNSTATUS IS NULL

UPDATE DATAMAIN SET PROCESS = 0 WHERE PROCESSDECODE = 'PCR' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET PROCESS = 1 WHERE PROCESSDECODE = 'Extraction PCR' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET PROCESS = 2 WHERE PROCESSDECODE = 'Extraction' AND RUNSTATUS IS NULL

UPDATE DATAMAIN SET MELT = 0 WHERE MELTDECODE = 'No Melting' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET MELT = 1 WHERE MELTDECODE = 'No' AND RUNSTATUS IS NULL

UPDATE DATAMAIN SET APPROVALUSERROLE = 0 WHERE APPROVALUSERROLEDECODE = 'Service' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET APPROVALUSERROLE = 1 WHERE APPROVALUSERROLEDECODE = 'Admin' AND RUNSTATUS IS NULL
UPDATE DATAMAIN SET APPROVALUSERROLE = 2 WHERE APPROVALUSERROLEDECODE = 'Analyst' AND RUNSTATUS IS NULL


-- Valid/1
UPDATE DATAMAIN SET RUNSTATUS = 'V' WHERE RUNSTATUS IS NULL AND ApprovalStatus = '2';
-- Aborted 
UPDATE DATAMAIN SET RUNSTATUS = 'A' WHERE RUNSTATUS IS NULL AND AbortTime IS NOT NULL;
-- Invalid
UPDATE DATAMAIN SET RUNSTATUS = 'I' WHERE RUNSTATUS IS NULL AND ApprovalStatus = '1';
-- Error/2
UPDATE DATAMAIN SET RUNSTATUS = 'E' WHERE RUNSTATUS IS NULL AND EndRunTime IS NULL;
-- Valid/2
UPDATE DATAMAIN SET RUNSTATUS = 'V' WHERE RUNSTATUS IS NULL AND ApprovalStatus IS NULL AND Process = 2; -- 2=Extraction
-- Error/2
UPDATE DATAMAIN SET RUNSTATUS = 'E' WHERE RUNSTATUS IS NULL AND ApprovalStatus IS NULL AND Process <> 2; -- 2=Extraction


--select runstatus, count(*) from datamain group by runstatus
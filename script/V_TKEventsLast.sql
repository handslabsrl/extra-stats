SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--DROP VIEW V_TKEventsLast
CREATE VIEW [dbo].[V_TKEventsLast] AS 
select TKEvt .* from TKEvents TKEvt 
JOIN (select LastEvt.TKRequestId, LastEvt.Operation, max(LastEvt.TKEventId) as maxTkEventId 
		from TKEvents LastEvt
	   GROUP BY LastEvt.TKRequestId, LastEvt.Operation) LastEvt
on LastEvt.TKRequestId = TKEvt.TKRequestId
AND LastEvt.Operation = TKEvt.Operation
AND LastEvt.maxTkEventId = TKEvt.TKEventId
GO



begin transaction 
update DRSections  set Title = REPLACE(REPLACE(Title, '\n', ''), '\r', '')
update DRSectionPaths  set BreadCrumb = REPLACE(REPLACE(BreadCrumb, '\n', ''), '\r', '')
update DRTopics set Title = REPLACE(REPLACE(Title, '\n', ''), '\r', '')
update DRTopics set Code = REPLACE(REPLACE(Code, '\n', ''), '\r', '')
update DRResources set Title = REPLACE(REPLACE(Title, '\n', ''), '\r', '')
rollback transaction 


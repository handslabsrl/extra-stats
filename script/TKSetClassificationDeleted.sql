USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKSetClassificationDeleted]    Script Date: 22/05/2020 14:57:10 ******/
DROP PROCEDURE [dbo].[TKSetClassificationDeleted]
GO

/****** Object:  StoredProcedure [dbo].[TKSetClassificationDeleted]    Script Date: 22/05/2020 14:57:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKSetClassificationDeleted]
	@ClassificationId INT, 
	@Deleted bit
AS
BEGIN
	DECLARE @tree CURSOR
	SET NOCOUNT ON;  

	with tree(TKClassificationId, ParentId, Title) as
		(
		select TKClassificationId, ParentId, Title from TKClassifications where TKClassificationId=@ClassificationId
		union all
		select a.TKClassificationId, a.ParentId, a.Title from TKClassifications a
		inner join tree t on t.TKClassificationId=a.ParentId
		)
		UPDATE TKClassifications SET Deleted = @Deleted
		FROM tree where tree.TKClassificationId = TKClassifications.TKClassificationId
END
GO


DECLARE @Script as Varchar(30) = 'ICDisp.scr'   --'ICDisp.scr'   -- PROVA_DNASampleDisp PROVA_ICDisp
DECLARE @ClaimId as numeric = 1
DECLARE @DateRif as Varchar(20) = '01/01/2019'
DECLARE @Session as numeric = 1

select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx,
	                       CFG.CheckType, CFG.CheckName, CFG.CheckSeq, PR.TrackSeq, PR.ClaimPressureSNId, PR.TrackIdx, PR.TrackSeq,
                           PR.ValueInit as Init, PR.ValueAsp as asp, PR.ValueTipOn as tipoOn, PR.ValueDisp as disp, 
	                       PR.ValueLeak1 as leak1, PR.ValueLeak2 as leak2, PR.ValueLeak3 as leak3, 
                           PR.ValueClotAsp as clotAsp, PR.ValueClotDisp as clotDisp, PR.ValueTipOff as tipOff
                      FROM ClaimPressuresSN as PR
                     INNER JOIN ClaimCheckCfgs CFG on CFG.script = PR.script
                     WHERE PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and
                           CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)
                        ORDER BY 1
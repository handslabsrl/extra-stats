 --DROP VIEW V_Executions
CREATE VIEW V_Executions AS 
SELECT Contratti.ContrattoId, 
	   COALESCE((select SUM(DettagliContratto.NumeroUscite) from DettagliContratto 
			where  DettagliContratto.ContrattoId = Contratti.ContrattoId), 0) as UscitePreviste,
	   COALESCE((select SUM(UsciteOrdine.NumeroUscite) from UsciteOrdine 
			INNER JOIN Ordini ON UsciteOrdine.OrdineId = Ordini.OrdineId 
			INNER JOIN DettagliContratto  ON DettagliContratto.ContrattoId = Contratti.ContrattoId 
			where Ordini.DettaglioContrattoId = DettagliContratto.Id), 0) as UsciteAssegnate,
	   Contratti.Anno, 
	   Contratti.Numero, 
	   left(Concat(Contratti.Anno, '/', format(Contratti.Numero,'0000000')),12) AS Contratto_Desc, 
	   Contratti.ClienteId,
	   Clienti.RagioneSociale as RagioneSocialeCliente,
	   ProduttoriEditori.NomeCognome as NomeCognomeProduttore,
	   Contratti.NomeRichiedente, 
	   CASE 
		WHEN Fatture.Numero IS NULL THEN NULL
		--ELSE left(Concat(Fatture.Anno, '/', format(Fatture.Numero,'0000000'), '/', Fatture.Tipo, 
		--				' del ', FORMAT( Fatture.DataFattura, 'dd/MM/yyyy')),30) 
		ELSE LEFT(CONCAT(Fatture.Tipo,' ', FORMAT(Fatture.Numero,'0000000'), ' ', FORMAT( Fatture.DataFattura, 'dd/MM/yyyy')),20) 
	   END AS RifDocumento, 
	   (CASE WHEN Contratti.AnnuncioPrincipaleSportello = 1 THEN 'Si' ELSE 'No' END) AS AnnuncioPrincipaleSportelloDesc,
	   Contratti.IdentificativoSportello,
	   (select MIN(left(Concat(Ordini.Anno, '/', format(Ordini.Numero,'0000000')),12)) from Ordini
			INNER JOIN DettagliContratto  ON DettagliContratto.ContrattoId = Contratti.ContrattoId 
			where Ordini.DettaglioContrattoId = DettagliContratto.Id) as Ordine_Desc,
	   COALESCE((select SUM(ImportoNetto + ImportoIva) from Ordini
			INNER JOIN DettagliContratto  ON DettagliContratto.ContrattoId = Contratti.ContrattoId 
			where Ordini.DettaglioContrattoId = DettagliContratto.Id),0) as Totale
FROM   Contratti
INNER JOIN Clienti ON Clienti.ClienteId = Contratti.ClienteId
INNER JOIN ProduttoriEditori ON ProduttoriEditori.Id = Contratti.ProduttoreEditore
LEFT JOIN Fatture ON Contratti.RifDocId = Fatture.FatturaId
WHERE Contratti.TipoArea = 'S'



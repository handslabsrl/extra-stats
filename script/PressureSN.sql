DECLARE @Script as Varchar(20) = 'ICDISP.scr'
DECLARE @ClaimId as numeric = 1
DECLARE @DateRif as Varchar(20) = '01/01/2019'
DECLARE @Session as numeric = 2

SELECT * FROM (select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueAsp as Pressure, ValueAsp as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script  and CFG.CheckName = 'Asp'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueDisp as Pressure, NULL as asp, ValueDisp as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'Disp'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueTipon as Pressure, NULL as asp, NULL as disp, ValueTipon as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'TipOn'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueTipOff as Pressure, NULL as asp, NULL as disp, NULL as tipon, ValueTipOff as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'TipOff'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueLeak1 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            ValueLeak1 as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'Leak1'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId,  
            ValueInit as init, ValueLeak2 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, ValueLeak2 as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'Leak2'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueLeak3 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, ValueLeak3 as leak3, NULL as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'Leak3'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueClotAsp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, ValueClotAsp as ClotAsp, NULL as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'ClotAsp'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0))
    UNION 
    select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
            ValueInit as init, ValueClotDisp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
            NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, ValueClotDisp as ClotDisp 
        from ClaimPressuresSN as PR
    inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckName = 'ClotDisp'
    where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)  AND ((CFG.CheckType = 0 and PR.TrackIdx = 0) or (CFG.CheckType = 1 and PR.TrackIdx<> 0)) UNION  
select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx, CFG.CheckType, CFG.CheckName, PR.trackIdx, PR.TrackSeq, PR.ClaimPressureSNId, 
    ValueInit as init, ValueInit as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, 
    NULL as leak1, NULL as leak2, NULL as leak3, NULL as ClotAsp, NULL as ClotDisp 
    from ClaimPressuresSN as PR
inner join ClaimCheckCfgs CFG on CFG.script = PR.script and CFG.CheckType <> 1 and CFG.CheckName = 'Init'
where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
            CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103)   AND CFG.CheckType = -1 and PR.TrackIdx = 0 and PR.TrackSeq = 0
) TMP   
WHERE Pressure IS NOT NULL 
ORDER BY PointIdx
USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClosingInfoPaths]    Script Date: 22/05/2020 14:56:51 ******/
DROP PROCEDURE [dbo].[TKLoadClosingInfoPaths]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadClosingInfoPaths]    Script Date: 22/05/2020 14:56:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKLoadClosingInfoPaths]
AS
BEGIN
	DECLARE @Roots CURSOR
	DECLARE @ClosingInfoId INT
	DECLARE @title NVARCHAR(4000)

	SET NOCOUNT ON;

	delete TKClosingInfoPaths;

	SET @Roots = CURSOR FOR
	SELECT TKClosingInfoId, Title FROM TKClosingInfos where ParentId is null;
	OPEN @Roots
	FETCH NEXT
	FROM @Roots INTO @ClosingInfoId, @title
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- Inserimento puntamento alla sezione stessa
		INSERT INTO TKClosingInfoPaths (TKClosingInfoIdRef, TKClosingInfoId, BreadCrumb, RootTitle)
		VALUES (@ClosingInfoId, @ClosingInfoId, @title, @title);
		-- inserimento di tutti i figli presenti (ricorsiva)
		PRINT N'ROOT: '  + STR(@ClosingInfoId) + ' ' + @title;
		EXEC [TKLoadClosingInfoPathsChilds]  @ClosingInfoIdRef=@ClosingInfoId,
										 @ClosingInfoId=@ClosingInfoId,
										 @title=@title,
										 @rootTitle=@title
		PRINT N'ROOT END: '  + STR(@ClosingInfoId) + ' ' + @title;
		FETCH NEXT
		FROM @Roots INTO @ClosingInfoId, @title
	END
	CLOSE @Roots
	DEALLOCATE @Roots
END


GO


USE [ELITech_ExtraStats]
GO

/****** Object:  Table [dbo].[GAssayPrograms]    Script Date: 15/04/2018 19:34:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GAssayPrograms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AssayName] [nvarchar](250) NULL,
	[SampleTypes] [int] NOT NULL,
	[IVDCleared] [int] NOT NULL,
	[CtsAndTmOnly] [int] NOT NULL,
	[Pathogen_TargetName] [nvarchar](250) NULL,
	[ExtractionInputVolume] [int] NOT NULL,
	[ExtractedEluteVolume] [int] NOT NULL,
	[ResultsScaling] [int] NOT NULL,
	[SonicationOnTime] [int] NOT NULL,
	[SonicationOffTime] [int] NOT NULL,
	[SonicationCycle] [int] NOT NULL,
	[ExtractionCassetteID] [nvarchar](250) NULL,
	[PCRCassetteID] [nvarchar](250) NULL,
	[PCRInputEluateVolume] [int] NOT NULL,
	[PreCycleStepNumber] [int] NOT NULL,
	[PCRAmplificationCycles] [int] NOT NULL,
	[PCRAmplificationSteps] [int] NOT NULL,
	[MeltRequired] [int] NOT NULL,
	[MeltingOptional] [int] NOT NULL,
	[StartRampTemperature] [int] NOT NULL,
	[EndRampTemperature] [int] NOT NULL,
	[RampRate] [float] NOT NULL,
	[UpperTolerance] [int] NOT NULL,
	[LowerTolerance] [int] NOT NULL,
	[OffSet] [int] NOT NULL,
	[ConversionFactorIU] [float] NOT NULL,
	[UserName] [nvarchar](250) NULL,
	[RegisteredDate] [datetime2](7) NOT NULL,
	[SampleMatrix_Id] [int] NULL,
	[AssayParameterToolVersion] [nvarchar](250) NULL,
	[Serial] [nvarchar](50) NULL,
	[AssayId] [int] NULL,
	[FileName] [nvarchar](250) NULL,
 CONSTRAINT [PK_GAssayPrograms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[GAssayPrograms]  WITH CHECK ADD  CONSTRAINT [FK_GAssayPrograms_INSTRUMENTS_SerialNumber] FOREIGN KEY([Serial])
REFERENCES [dbo].[Instruments] ([SerialNumber])
GO

ALTER TABLE [dbo].[GAssayPrograms] CHECK CONSTRAINT [FK_GAssayPrograms_INSTRUMENTS_SerialNumber]
GO

ALTER TABLE [dbo].[GAssayPrograms] ADD SampleTypesDecode [nvarchar](50);

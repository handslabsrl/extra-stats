USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKSetClosingInfoDeleted]    Script Date: 22/05/2020 14:57:10 ******/
DROP PROCEDURE [dbo].[TKSetClosingInfoDeleted]
GO

/****** Object:  StoredProcedure [dbo].[TKSetClosingInfoDeleted]    Script Date: 22/05/2020 14:57:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKSetClosingInfoDeleted]
	@ClosingInfoId INT, 
	@Deleted bit
AS
BEGIN
	DECLARE @tree CURSOR
	SET NOCOUNT ON;  

	with tree(TKClosingInfoId, ParentId, Title) as
		(
		select TKClosingInfoId, ParentId, Title from TKClosingInfos where TKClosingInfoId=@ClosingInfoId
		union all
		select a.TKClosingInfoId, a.ParentId, a.Title from TKClosingInfos a
		inner join tree t on t.TKClosingInfoId=a.ParentId
		)
		UPDATE TKClosingInfos SET Deleted = @Deleted
		FROM tree where tree.TKClosingInfoId = TKClosingInfos.TKClosingInfoId
END
GO


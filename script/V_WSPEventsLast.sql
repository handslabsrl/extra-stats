--DROP VIEW V_WSPEventsLast
CREATE VIEW V_WSPEventsLast AS 
select WSPEvt .*, V_UserName.FULLNAME
from WSPEvents WSPEvt 
JOIN (select LastEvt.WSPRequestId, LastEvt.Operation, max(LastEvt.WSPEventId) as maxWSPEventId 
		from WSPEvents LastEvt
	   GROUP BY LastEvt.WSPRequestId, LastEvt.Operation) LastEvt
on LastEvt.WSPRequestId = WSPEvt.WSPRequestId
AND LastEvt.Operation = WSPEvt.Operation
AND LastEvt.maxWSPEventId = WSPEvt.WSPEventId
LEFT JOIN V_UserName ON V_UserName.USERNAME = WSPEvt.UserInse


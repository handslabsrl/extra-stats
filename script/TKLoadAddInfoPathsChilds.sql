USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadAddInfoPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
DROP PROCEDURE [dbo].[TKLoadAddInfoPathsChilds]
GO

/****** Object:  StoredProcedure [dbo].[TKLoadAddInfoPathsChilds]    Script Date: 22/05/2020 14:57:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[TKLoadAddInfoPathsChilds]
	@AddInfoIdRef INT, 
	@AddInfoId INT, 
	@title NVARCHAR(max),
	@rootTitle NVARCHAR(max)
AS
BEGIN
	DECLARE @Childs CURSOR
	DECLARE @AddInfoIdChild INT
	DECLARE @ParentIdChild INT
	DECLARE @titleChild NVARCHAR(max)
	DECLARE @percorso NVARCHAR(max)

	SET NOCOUNT ON;  
	
	SET @Childs = CURSOR FOR
	SELECT TKAddInfoId, ParentId, Title FROM TKAddInfos where ParentId = @AddInfoId
	OPEN @Childs
	FETCH NEXT
	FROM @Childs INTO @AddInfoIdChild, @ParentIdChild, @titleChild
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @percorso = CONCAT(@title, ' > ', @titleChild);
		-- Inserimento percorso 
		-- PRINT N'STEP/1 - Rif:' + STR(@AddInfoIdRef) + ' Child:' + STR(@AddInfoIdChild) + ' Parent:' + STR(@ParentIdChild) ;  
		IF NOT EXISTS (SELECT * FROM TKAddInfoPaths WHERE TKAddInfoIdRef = @AddInfoIdRef and TKAddInfoId = @AddInfoIdChild) 
		BEGIN 
			INSERT INTO TKAddInfoPaths (TKAddInfoIdRef, TKAddInfoId, BreadCrumb, RootTitle)
			VALUES (@AddInfoIdRef, @AddInfoIdChild, @percorso, @rootTitle);
		END 
		
		-- inserimento percorso per tutti i nodi figli (con riferimento alla root passata)
		EXEC [TKLoadAddInfoPathsChilds]  @AddInfoIdRef=@AddInfoIdRef, 
												 @AddInfoId=@AddInfoIdChild, 
												 @title=@percorso, 
												 @rootTitle=@rootTitle

		-- Inserimento puntamento a se stesso (se non esiste)
		IF NOT EXISTS (SELECT * FROM TKAddInfoPaths WHERE TKAddInfoIdRef = @AddInfoIdChild and TKAddInfoId = @AddInfoIdChild) 
		BEGIN 
			INSERT INTO TKAddInfoPaths (TKAddInfoIdRef, TKAddInfoId, BreadCrumb, RootTitle)
			VALUES (@AddInfoIdChild, @AddInfoIdChild, @percorso, @rootTitle);
		END 

		-- inserimento di tutti i figli presenti con riferimento a se stesso 
		-- PRINT N'STEP/2 - Rif:' + STR(@AddInfoIdChild) + ' ' + @percorso;  
		EXEC [TKLoadAddInfoPathsChilds]  @AddInfoIdRef=@AddInfoIdChild, 
												 @AddInfoId=@AddInfoIdChild, 
												 @title=@percorso, 
												 @rootTitle=@rootTitle
		FETCH NEXT
		FROM @Childs INTO @AddInfoIdChild, @ParentIdChild, @titleChild
	END
	CLOSE @Childs
	DEALLOCATE @Childs
END


GO


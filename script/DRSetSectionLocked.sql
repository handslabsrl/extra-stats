USE [ELITeBoard]
GO

/****** Object:  StoredProcedure [dbo].[DRSetSectionLocked]    Script Date: 22/05/2020 14:57:10 ******/
DROP PROCEDURE [dbo].[DRSetSectionLocked]
GO

/****** Object:  StoredProcedure [dbo].[DRSetSectionLocked]    Script Date: 22/05/2020 14:57:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[DRSetSectionLocked]
	@SectionId INT, 
	@locked bit
AS
BEGIN
	DECLARE @tree CURSOR
	SET NOCOUNT ON;  

	with tree(DRSectionId, ParentId, Title) as
		(
		select DRSectionId, ParentId, Title from DRSections where DRSectionId=@SectionId
		union all
		select a.DRSectionId, a.ParentId, a.Title from DRSections a
		inner join tree t on t.DRSectionId=a.ParentId
		)
		UPDATE DRSections SET Locked = @locked
		FROM tree where tree.DRSectionId = DRSections.DRSectionId
END


GO


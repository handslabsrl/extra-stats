select  Pathogen_TargetName_Norm, 
		COUNT(*) AS TOT_RUN, 
		SUM(RUN_OK) AS TOT_RUN_OK, 
		SUM(NUM_SAMPLE) AS TOT_SAMPLE, 
		SUM(NUM_CALIBRATOR) AS TOT_CALIBRATOR, 
		SUM(NUM_CONTROL) AS TOT_CONTROL, 
		SUM(ERROR) AS ERROR, 
		SUM(INVALID) AS INVALID 
from (
SELECT GA.Pathogen_TargetName_Norm, 
    -- n.Run Totali  (esclusi aborted e invalidi)
    IIF(DM.RUNSTATUS NOT IN ('E', 'A', 'I'), 1, 0) AS RUN_OK,	
	-- Conteggio SampleType (??? SOLO TRA QUELLI VALIDI ???)
	GA.SampleTypesDecode, 
    IIF(DM.SampleTypes = 0 AND DM.RUNSTATUS NOT IN ('E', 'A', 'I'), 1, 0) AS NUM_SAMPLE,	
	IIF(DM.SampleTypes = 1 AND DM.RUNSTATUS NOT IN ('E', 'A', 'I'), 1, 0) AS NUM_CALIBRATOR,	
	IIF(DM.SampleTypes = 2 AND DM.RUNSTATUS NOT IN ('E', 'A', 'I'), 1, 0) AS NUM_CONTROL,	
    -- n.Run in ERRORE 
    IIF(DM.RUNSTATUS IN ('E', 'A'), 1, 0) AS ERROR,
	-- INVALID (conteggiati nel totale)
    IIF(DM.RUNSTATUS = 'I', 1, 0) AS INVALID
	FROM DATAMAIN DM
INNER JOIN GAssayPrograms GA
ON GA.AssayName = DM.AssayName 
AND GA.AssayId = DM.GeneralAssayID
AND GA.Serial = DM.SerialNumber
INNER JOIN INSTRUMENTS INSTR
ON DM.SERIALNUMBER = INSTR.SERIALNUMBER  WHERE DM.STARTRUNTIME IS NOT NULL 
AND CONVERT(date, DM.STARTRUNTIME) >= CONVERT(datetime, '25/05/2018', 103) 
AND CONVERT(date, DM.STARTRUNTIME) <= CONVERT(datetime, '25/06/2018', 103)
) TMP
GROUP BY Pathogen_TargetName_Norm
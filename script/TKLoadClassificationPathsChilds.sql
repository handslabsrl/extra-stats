USE [ELITeBoard]
GO
/****** Object:  StoredProcedure [dbo].[TKLoadClassificationPathsChilds]    Script Date: 08/08/2022 17:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[TKLoadClassificationPathsChilds]
	@ClassificationIdRef INT, 
	@ClassificationId INT, 
	@title NVARCHAR(max),
	@rootTitle NVARCHAR(max),
	@rootId INT 
AS
BEGIN
	DECLARE @Childs CURSOR
	DECLARE @ClassificationIdChild INT
	DECLARE @ParentIdChild INT
	DECLARE @titleChild NVARCHAR(max)
	DECLARE @percorso NVARCHAR(max)

	DECLARE @InstrumentParent INT
	DECLARE @OtherPlatformParent INT
	DECLARE @CodArtParent INT
	DECLARE @ClaimLogFileParent INT
	DECLARE @AddInfoParent INT
	DECLARE @TKAddInfoIdParent INT
	DECLARE @ClosingInfoParent INT
	DECLARE @TKClosingInfoIdParent INT
	DECLARE @ReservedParent BIT
	DECLARE @InfoContactParent INT
	DECLARE @ToDoListParent INT
	DECLARE @SupplierEngagementParent INT
	DECLARE @CustomerSGATParent INT
	DECLARE @LocationSGATParent INT
	DECLARE @QuantityParent INT

	SET NOCOUNT ON;  

	-- Lettura cfg nodo superiore 
	SELECT  @InstrumentParent = Instrument, @AddInfoParent = AddInfo, @TKAddInfoIdParent =  TKAddInfoId, 
			@ReservedParent = Reserved, @ClaimLogFileParent = ClaimLogFile, 
			@ClosingInfoParent = ClosingInfo, @TKClosingInfoIdParent =  TKClosingInfoId,
			@OtherPlatformParent = OtherPlatform, @CodArtParent = CodArt, 
			@InfoContactParent = InfoContact, @ToDoListParent = ToDoList, 
			@SupplierEngagementParent = SupplierEngagement, @CustomerSGATParent = CustomerSGAT, 
			@LocationSGATParent = LocationSGAT, @QuantityParent = Quantity
		FROM TKClassifications where TKClassificationId = @ClassificationId
	--PRINT N'CFG PARENT ' + STR(@ClassificationId) + ' Instrument:' + STR(@InstrumentParent) + 
	--       ' AddInfo:' + STR(@AddInfoParent) + '/' + STR(@TKAddInfoIdParent) + 
	--	   ' Reserved:' + STR(@ReservedParent) + ' ClaimLogFile:' + STR(@ClaimLogFileParent) + 
	--	   ' ClosingInfo:' + STR(@ClosingInfoParent) + '/' + STR(@TKClosingInfoIdParent);
	
	SET @Childs = CURSOR FOR
	SELECT TKClassificationId, ParentId, Title FROM TKClassifications where ParentId = @ClassificationId
	OPEN @Childs
	FETCH NEXT
	FROM @Childs INTO @ClassificationIdChild, @ParentIdChild, @titleChild
	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- AGGIORNAMENTO CFG Nodo CHILD rispetto PARENT 
		UPDATE TKClassifications 
		SET Instrument = IIF(@InstrumentParent > Instrument, @InstrumentParent, Instrument),
			OtherPlatform= IIF(@OtherPlatformParent > OtherPlatform, @OtherPlatformParent, OtherPlatform),
			codArt = IIF(@CodArtParent > codArt, @CodArtParent, codArt),
		    AddInfo = IIF(@AddInfoParent > AddInfo, @AddInfoParent, AddInfo),
			TKAddInfoId = IIF(TKAddInfoId IS NULL, @TKAddInfoIdParent, TKAddInfoId),
			Reserved = IIF(@ReservedParent = 1, @ReservedParent, Reserved), 
			ClaimLogFile = IIF(@ClaimLogFileParent > ClaimLogFile, @ClaimLogFileParent, ClaimLogFile),
			ClosingInfo = IIF(@ClosingInfoParent > ClosingInfo, @ClosingInfoParent, ClosingInfo),
			TKClosingInfoId = IIF(TKClosingInfoId IS NULL, @TKClosingInfoIdParent, TKClosingInfoId), 
			InfoContact= IIF(@InfoContactParent > InfoContact, @InfoContactParent, InfoContact),
			ToDoList= IIF(@ToDoListParent > ToDoList, @ToDoListParent, ToDoList),
			SupplierEngagement= IIF(@SupplierEngagementParent > SupplierEngagement, @SupplierEngagementParent, SupplierEngagement),
			CustomerSGAT= IIF(@CustomerSGATParent > CustomerSGAT, @CustomerSGATParent, CustomerSGAT),
			LocationSGAT= IIF(@LocationSGATParent > LocationSGAT, @LocationSGATParent, LocationSGAT),
			Quantity= IIF(@QuantityParent > Quantity, @QuantityParent, Quantity)
		WHERE TKClassificationId = @ClassificationIdChild


		SET @percorso = CONCAT(@title, ' > ', @titleChild);
		-- Inserimento percorso 
		PRINT N'STEP/1 - Rif:' + STR(@ClassificationIdRef) + ' Child:' + STR(@ClassificationIdChild) + ' Parent:' + STR(@ParentIdChild) ;  
		IF NOT EXISTS (SELECT * FROM TKClassificationPaths WHERE TKClassificationIdRef = @ClassificationIdRef and TKClassificationId = @ClassificationIdChild) 
		BEGIN 
			INSERT INTO TKClassificationPaths (TKClassificationIdRef, TKClassificationId, BreadCrumb, RootTitle, RootId)
			VALUES (@ClassificationIdRef, @ClassificationIdChild, @percorso, @rootTitle, @rootId);
		END 
		
		-- inserimento percorso per tutti i nodi figli (con riferimento alla root passata)
		EXEC [TKLoadClassificationPathsChilds]  @ClassificationIdRef=@ClassificationIdRef, 
												@ClassificationId=@ClassificationIdChild, 
												@title=@percorso,
												@rootTitle=@rootTitle, 
												@rootId=@rootId

		-- Inserimento puntamento a se stesso (se non esiste)
		IF NOT EXISTS (SELECT * FROM TKClassificationPaths WHERE TKClassificationIdRef = @ClassificationIdChild and TKClassificationId = @ClassificationIdChild) 
		BEGIN 
			INSERT INTO TKClassificationPaths (TKClassificationIdRef, TKClassificationId, BreadCrumb, RootTitle, RootId)
			VALUES (@ClassificationIdChild, @ClassificationIdChild, @percorso, @rootTitle, @rootId);
		END 

		-- inserimento di tutti i figli presenti con riferimento a se stesso 
		PRINT N'STEP/2 - Rif:' + STR(@ClassificationIdChild) + ' ' + @percorso;  
		EXEC [TKLoadClassificationPathsChilds]  @ClassificationIdRef=@ClassificationIdChild, 
												@ClassificationId=@ClassificationIdChild, 
												@title=@percorso,
												@rootTitle=@rootTitle,
												@rootId=@rootId

		FETCH NEXT
		FROM @Childs INTO  @ClassificationIdChild, @ParentIdChild, @titleChild
	END
	CLOSE @Childs
	DEALLOCATE @Childs
END



﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebApp.Models
{
    public class FunctionDocumentation
    {
        private readonly string FunctionsDocumentationRoot = "Download\\Docs";

        public string functionName { get; set; }
     
        public bool existsPdf
        {
            get
            {
                string fullFileName = this.functionName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.getFullFileNamePdf);
            }
        }

        public string getFileNamePdf
        {
            get
            {
                return string.Format("{0}.pdf", this.functionName.Replace(" ", "_"));
            }
        }

        public string getFullFileNamePdf
        {
            get
            {
                string fileName = this.getFileNamePdf;
                string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), FunctionsDocumentationRoot);
                string fullFileName = System.IO.Path.Combine(relativePath, fileName);
                return fullFileName;
            }
        }
    }
}


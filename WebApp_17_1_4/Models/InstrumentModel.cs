﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class InstrumentModel
    {
        public string SerialNumber { get; set; }
        public string CompanyName { get; set; }
        [Column("Site_Description")]
        public string SiteDescription { get; set; }
        public string SiteCity { get; set; }
        public string SiteCountry { get; set; }
        public string CommercialEntity { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string SerialNumberInfo
        {
            get
            {
                return $"{SerialNumber} ({CompanyName} / {SiteDescription})";
            }
        }
        public string Command { get; set; }
        public DateTime? CommandDateRif { get; set; }
        public string CommandInfo
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Command))
                {
                    return "";
                }
                string ret = this.Command;
                if (this.Command.Equals("DELETE"))
                {
                    ret = string.Format("Deleted");
                }
                if (this.Command.Equals("DISMISS") && this.CommandDateRif != null)
                {
                    ret = string.Format("Dismissed from {0}", ((DateTime)this.CommandDateRif).ToString("dd/MM/yyyy"));
                }
                return ret;
            }
        }

    }
}


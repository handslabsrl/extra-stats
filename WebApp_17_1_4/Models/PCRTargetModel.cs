﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class PCRTargetModel
    {
        [Column("PATHOGEN_TARGETNAME")]
        public string PCRTarget { get; set; }
    }
}


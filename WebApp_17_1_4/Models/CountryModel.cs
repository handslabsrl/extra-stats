﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class CountryModel
    {
        [Column("Site_Country")]
        public string Country { get; set; }
    }
}


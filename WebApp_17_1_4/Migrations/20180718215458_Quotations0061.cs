﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotations0061 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "UserCfg",
                newName: "fullName");

            migrationBuilder.AddColumn<decimal>(
                name: "Help1",
                table: "QuotationsTotal",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Help2",
                table: "QuotationsTotal",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Profit",
                table: "QuotationsTotal",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Help1",
                table: "QuotationsTotal");

            migrationBuilder.DropColumn(
                name: "Help2",
                table: "QuotationsTotal");

            migrationBuilder.DropColumn(
                name: "Profit",
                table: "QuotationsTotal");

            migrationBuilder.RenameColumn(
                name: "fullName",
                table: "UserCfg",
                newName: "FullName");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class QuotationsTotal_003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationsTotal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuotationId = table.Column<int>(nullable: true),
                    Category = table.Column<string>(maxLength: 20, nullable: true),
                    Price = table.Column<decimal>(nullable: true),
                    Cost = table.Column<decimal>(nullable: true),
                    Description = table.Column<string>(maxLength: 100, nullable: true),
                    Margin = table.Column<decimal>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationsTotal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationsTotal_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QuotationsTotalPCR",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuotationId = table.Column<int>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    TestForYear = table.Column<int>(nullable: false),
                    ControlExpiry = table.Column<int>(nullable: false),
                    Control = table.Column<int>(nullable: false),
                    Reaction = table.Column<int>(nullable: false),
                    Target = table.Column<string>(maxLength: 50, nullable: true),
                    PCRType = table.Column<int>(nullable: false),
                    KitSize = table.Column<int>(nullable: false),
                    KitPerYear = table.Column<int>(nullable: false),
                    StandardCost = table.Column<int>(nullable: false),
                    Price = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationsTotalPCR", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationsTotalPCR_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationsTotal_QuotationId",
                table: "QuotationsTotal",
                column: "QuotationId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationsTotalPCR_QuotationId",
                table: "QuotationsTotalPCR",
                column: "QuotationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationsTotal");

            migrationBuilder.DropTable(
                name: "QuotationsTotalPCR");
        }
    }
}

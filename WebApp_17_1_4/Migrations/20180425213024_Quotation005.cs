﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotation005 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "noCompensation",
                table: "ElitePCRKits",
                newName: "NoCompensation");

            migrationBuilder.RenameColumn(
                name: "Compensaton",
                table: "ElitePCRKits",
                newName: "Compensation");

            migrationBuilder.AddColumn<int>(
                name: "ContractDuration",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ContractType",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InstrumentDiscount",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstrumentNumber",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "InstrumentSellingPrice",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InstrumentTypeID",
                table: "Quotations",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SaleType",
                table: "Quotations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContractDuration",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "ContractType",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "InstrumentDiscount",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "InstrumentNumber",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "InstrumentSellingPrice",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "InstrumentTypeID",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "SaleType",
                table: "Quotations");

            migrationBuilder.RenameColumn(
                name: "NoCompensation",
                table: "ElitePCRKits",
                newName: "noCompensation");

            migrationBuilder.RenameColumn(
                name: "Compensation",
                table: "ElitePCRKits",
                newName: "Compensaton");
        }
    }
}

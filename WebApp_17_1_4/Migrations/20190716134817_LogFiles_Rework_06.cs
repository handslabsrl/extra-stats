﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class LogFiles_Rework_06 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ClaimCheckCfgs",
                table: "ClaimCheckCfgs");

            migrationBuilder.DropColumn(
                name: "TrackIdx",
                table: "ClaimCheckCfgs");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClaimCheckCfgs",
                table: "ClaimCheckCfgs",
                columns: new[] { "Script", "CheckName", "CheckType" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_ClaimCheckCfgs",
                table: "ClaimCheckCfgs");

            migrationBuilder.AddColumn<int>(
                name: "TrackIdx",
                table: "ClaimCheckCfgs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ClaimCheckCfgs",
                table: "ClaimCheckCfgs",
                columns: new[] { "Script", "CheckName", "TrackIdx" });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class NewFields002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalMonthlyService",
                table: "Quotations",
                newName: "TotalAnnualService");

            migrationBuilder.RenameColumn(
                name: "TotalMonthlyRent",
                table: "Quotations",
                newName: "TotalAnnualRent");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TotalAnnualService",
                table: "Quotations",
                newName: "TotalMonthlyService");

            migrationBuilder.RenameColumn(
                name: "TotalAnnualRent",
                table: "Quotations",
                newName: "TotalMonthlyRent");
        }
    }
}

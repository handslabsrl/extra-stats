﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotation006 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Quotations_InstrumentTypeID",
                table: "Quotations",
                column: "InstrumentTypeID");

            migrationBuilder.AddForeignKey(
                name: "FK_Quotations_InstrumentType_InstrumentTypeID",
                table: "Quotations",
                column: "InstrumentTypeID",
                principalTable: "InstrumentType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Quotations_InstrumentType_InstrumentTypeID",
                table: "Quotations");

            migrationBuilder.DropIndex(
                name: "IX_Quotations_InstrumentTypeID",
                table: "Quotations");
        }
    }
}

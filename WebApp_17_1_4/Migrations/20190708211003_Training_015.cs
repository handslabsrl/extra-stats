﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Training_015 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "JobPosition",
                table: "TrainingRegistrations",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                table: "TrainingRegistrations",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddressCity",
                table: "TrainingRegistrations",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddressNr",
                table: "TrainingRegistrations",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CompanyAddressZipCode",
                table: "TrainingRegistrations",
                maxLength: 10,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TypeOfParticipant",
                table: "TrainingRegistrations",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "TrainingLocations",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 200, nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingLocations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TrainingTypeOfParticipants",
                columns: table => new
                {
                    Id = table.Column<string>(maxLength: 100, nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrainingTypeOfParticipants", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrainingLocations");

            migrationBuilder.DropTable(
                name: "TrainingTypeOfParticipants");

            migrationBuilder.DropColumn(
                name: "CompanyAddressCity",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "CompanyAddressNr",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "CompanyAddressZipCode",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "TypeOfParticipant",
                table: "TrainingRegistrations");

            migrationBuilder.AlterColumn<string>(
                name: "JobPosition",
                table: "TrainingRegistrations",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "CompanyAddress",
                table: "TrainingRegistrations",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);
        }
    }
}

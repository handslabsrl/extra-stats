﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotations050 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "NewCustomer",
                table: "Quotations",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Compensation",
                table: "Quotations",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Compensation",
                table: "Quotations");

            migrationBuilder.AlterColumn<bool>(
                name: "NewCustomer",
                table: "Quotations",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}

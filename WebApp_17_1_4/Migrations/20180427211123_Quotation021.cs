﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotation021 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuotationElitePCRKits_ElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKits");

            migrationBuilder.DropForeignKey(
                name: "FK_QuotationElitePCRKits_Quotations_QuotationId",
                table: "QuotationElitePCRKits");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuotationElitePCRKits",
                table: "QuotationElitePCRKits");

            migrationBuilder.RenameTable(
                name: "QuotationElitePCRKits",
                newName: "QuotationElitePCRKit");

            migrationBuilder.RenameIndex(
                name: "IX_QuotationElitePCRKits_QuotationId",
                table: "QuotationElitePCRKit",
                newName: "IX_QuotationElitePCRKit_QuotationId");

            migrationBuilder.RenameIndex(
                name: "IX_QuotationElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKit",
                newName: "IX_QuotationElitePCRKit_ElitePCRKitId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuotationElitePCRKit",
                table: "QuotationElitePCRKit",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationElitePCRKit_ElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKit",
                column: "ElitePCRKitId",
                principalTable: "ElitePCRKits",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationElitePCRKit_Quotations_QuotationId",
                table: "QuotationElitePCRKit",
                column: "QuotationId",
                principalTable: "Quotations",
                principalColumn: "QuotationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_QuotationElitePCRKit_ElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKit");

            migrationBuilder.DropForeignKey(
                name: "FK_QuotationElitePCRKit_Quotations_QuotationId",
                table: "QuotationElitePCRKit");

            migrationBuilder.DropPrimaryKey(
                name: "PK_QuotationElitePCRKit",
                table: "QuotationElitePCRKit");

            migrationBuilder.RenameTable(
                name: "QuotationElitePCRKit",
                newName: "QuotationElitePCRKits");

            migrationBuilder.RenameIndex(
                name: "IX_QuotationElitePCRKit_QuotationId",
                table: "QuotationElitePCRKits",
                newName: "IX_QuotationElitePCRKits_QuotationId");

            migrationBuilder.RenameIndex(
                name: "IX_QuotationElitePCRKit_ElitePCRKitId",
                table: "QuotationElitePCRKits",
                newName: "IX_QuotationElitePCRKits_ElitePCRKitId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_QuotationElitePCRKits",
                table: "QuotationElitePCRKits",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationElitePCRKits_ElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKits",
                column: "ElitePCRKitId",
                principalTable: "ElitePCRKits",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_QuotationElitePCRKits_Quotations_QuotationId",
                table: "QuotationElitePCRKits",
                column: "QuotationId",
                principalTable: "Quotations",
                principalColumn: "QuotationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

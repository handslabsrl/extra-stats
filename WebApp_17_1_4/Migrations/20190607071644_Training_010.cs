﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Training_010 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "TrainingRegistrationId",
                table: "TrainingLogs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainingLogs_TrainingRegistrationId",
                table: "TrainingLogs",
                column: "TrainingRegistrationId");

            migrationBuilder.AddForeignKey(
                name: "FK_TrainingLogs_TrainingRegistrations_TrainingRegistrationId",
                table: "TrainingLogs",
                column: "TrainingRegistrationId",
                principalTable: "TrainingRegistrations",
                principalColumn: "TrainingRegistrationId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TrainingLogs_TrainingRegistrations_TrainingRegistrationId",
                table: "TrainingLogs");

            migrationBuilder.DropIndex(
                name: "IX_TrainingLogs_TrainingRegistrationId",
                table: "TrainingLogs");

            migrationBuilder.DropColumn(
                name: "TrainingRegistrationId",
                table: "TrainingLogs");
        }
    }
}

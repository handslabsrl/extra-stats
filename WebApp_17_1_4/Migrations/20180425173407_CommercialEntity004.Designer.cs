﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Data;

namespace WebApp.Migrations
{
    [DbContext(typeof(WebAppDbContext))]
    [Migration("20180425173407_CommercialEntity004")]
    partial class CommercialEntity004
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp.Entity.CommercialEntity", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("SubTotal1");

                    b.Property<int>("SubTotal2");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("Total");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("CommercialEntity");
                });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class NewFields001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MasterMix",
                table: "QuotationOpenPCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CutOff",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinTheoreticalCons",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MultipleReaction",
                table: "ElitePCRKits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MasterMix",
                table: "QuotationOpenPCRKits");

            migrationBuilder.DropColumn(
                name: "CutOff",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "MinTheoreticalCons",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "MultipleReaction",
                table: "ElitePCRKits");
        }
    }
}

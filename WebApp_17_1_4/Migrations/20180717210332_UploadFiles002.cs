﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class UploadFiles002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TmstDelete",
                table: "UploadFiles",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserDelete",
                table: "UploadFiles",
                maxLength: 256,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TmstDelete",
                table: "UploadFiles");

            migrationBuilder.DropColumn(
                name: "UserDelete",
                table: "UploadFiles");
        }
    }
}

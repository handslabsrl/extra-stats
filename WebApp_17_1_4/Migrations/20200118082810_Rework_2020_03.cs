﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rework_2020_03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ValueGet",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "RecType",
                table: "ClaimTemperatures",
                maxLength: 1,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ValueCenter",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueLeft",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ValueRight",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecType",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueCenter",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueLeft",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueRight",
                table: "ClaimTemperatures");

            migrationBuilder.AlterColumn<int>(
                name: "ValueGet",
                table: "ClaimTemperatures",
                nullable: false,
                oldClrType: typeof(int),
                oldDefaultValue: 0);
        }
    }
}

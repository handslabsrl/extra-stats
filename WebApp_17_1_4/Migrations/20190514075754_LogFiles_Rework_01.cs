﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class LogFiles_Rework_01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Folder",
                table: "ClaimRows",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Script",
                table: "ClaimRows",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SessionNumber",
                table: "ClaimRows",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RecType",
                table: "ClaimPressures",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestedDateFrom",
                table: "Claims",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "RequestedDateTo",
                table: "Claims",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SoftwareVersion",
                table: "Claims",
                maxLength: 30,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ClaimPressureRecTypes",
                columns: table => new
                {
                    RecType = table.Column<string>(maxLength: 20, nullable: false),
                    eluateOrder = table.Column<int>(nullable: false),
                    mmixOrder = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimPressureRecTypes", x => x.RecType);
                });

            migrationBuilder.CreateTable(
                name: "ClaimScripts",
                columns: table => new
                {
                    ClaimScriptId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    Folder = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(maxLength: 1000, nullable: true),
                    SessionNumber = table.Column<int>(nullable: false),
                    Status = table.Column<string>(maxLength: 1, nullable: true),
                    TmstGet = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimScripts", x => x.ClaimScriptId);
                    table.ForeignKey(
                        name: "FK_ClaimScripts_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClaimScripts_ClaimId",
                table: "ClaimScripts",
                column: "ClaimId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaimPressureRecTypes");

            migrationBuilder.DropTable(
                name: "ClaimScripts");

            migrationBuilder.DropColumn(
                name: "Folder",
                table: "ClaimRows");

            migrationBuilder.DropColumn(
                name: "Script",
                table: "ClaimRows");

            migrationBuilder.DropColumn(
                name: "SessionNumber",
                table: "ClaimRows");

            migrationBuilder.DropColumn(
                name: "RecType",
                table: "ClaimPressures");

            migrationBuilder.DropColumn(
                name: "RequestedDateFrom",
                table: "Claims");

            migrationBuilder.DropColumn(
                name: "RequestedDateTo",
                table: "Claims");

            migrationBuilder.DropColumn(
                name: "SoftwareVersion",
                table: "Claims");
        }
    }
}

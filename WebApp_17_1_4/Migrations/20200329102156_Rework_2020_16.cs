﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rework_2020_16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserLinks");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserLinks",
                columns: table => new
                {
                    UserID = table.Column<string>(maxLength: 450, nullable: false),
                    TypeId = table.Column<int>(nullable: false),
                    LinkID = table.Column<string>(maxLength: 250, nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserLinks", x => new { x.UserID, x.TypeId, x.LinkID });
                });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Claim004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClaimSnapshots",
                columns: table => new
                {
                    ClaimSnapshotId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    FileName = table.Column<string>(maxLength: 500, nullable: true),
                    tipo = table.Column<string>(maxLength: 1, nullable: true),
                    Note = table.Column<string>(maxLength: 1000, nullable: true),
                    TmstInse = table.Column<DateTime>(nullable: false),
                    UserInse = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimSnapshots", x => x.ClaimSnapshotId);
                    table.ForeignKey(
                        name: "FK_ClaimSnapshots_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClaimSnapshots_ClaimId",
                table: "ClaimSnapshots",
                column: "ClaimId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaimSnapshots");
        }
    }
}

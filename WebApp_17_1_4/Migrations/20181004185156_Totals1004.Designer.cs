﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Data;
using WebApp.Classes;

namespace WebApp.Migrations
{
    [DbContext(typeof(WebAppDbContext))]
    [Migration("20181004185156_Totals1004")]
    partial class Totals1004
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp.Entity.CommercialEntity", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("SubTotal1");

                    b.Property<int>("SubTotal2");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("Total");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("CommercialEntity");
                });

            modelBuilder.Entity("WebApp.Entity.ElitePCRKit", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AnalysisType");

                    b.Property<decimal?>("Compensation");

                    b.Property<decimal?>("Compensation1");

                    b.Property<decimal?>("Compensation2");

                    b.Property<decimal?>("Compensation3");

                    b.Property<decimal?>("Compensation4");

                    b.Property<decimal?>("Compensation5");

                    b.Property<decimal?>("Compensation6");

                    b.Property<int?>("CutOff");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("ExtractionType");

                    b.Property<int?>("KitSize");

                    b.Property<int?>("MinTheoreticalCons");

                    b.Property<int?>("MultipleReaction");

                    b.Property<decimal?>("NoCompensation");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<string>("PCRTarget")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<int>("PCRType");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("StandardCost");

                    b.Property<int?>("Threshold1");

                    b.Property<int?>("Threshold2");

                    b.Property<int?>("Threshold3");

                    b.Property<int?>("Threshold4");

                    b.Property<int?>("Threshold5");

                    b.Property<int?>("Threshold6");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<int?>("ggExpiry");

                    b.HasKey("ID");

                    b.ToTable("ElitePCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.Extraction", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("KitSize");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("Extractions");
                });

            modelBuilder.Entity("WebApp.Entity.InstrumentType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<decimal>("OfficialPrice");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("InstrumentType");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.Property<int>("QuotationId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CommercialEntityID");

                    b.Property<bool?>("Compensation");

                    b.Property<int?>("ContractDuration");

                    b.Property<int?>("ContractType");

                    b.Property<decimal?>("CurrentBusiness");

                    b.Property<int?>("CurrentBusinessPerYear");

                    b.Property<string>("CustomerAddress")
                        .HasMaxLength(250);

                    b.Property<string>("CustomerName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("CustomerType");

                    b.Property<decimal?>("ExtractionEliteMGBKit");

                    b.Property<decimal?>("ExtractionEliteMGBPanel");

                    b.Property<decimal?>("ExtractionOpenKit");

                    b.Property<decimal?>("InstrumentDiscount");

                    b.Property<int?>("InstrumentNumber");

                    b.Property<decimal?>("InstrumentSellingPrice");

                    b.Property<int?>("InstrumentTypeID");

                    b.Property<decimal?>("LISConnection");

                    b.Property<bool?>("NewCustomer");

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<decimal?>("PercService");

                    b.Property<string>("ReferencePerson")
                        .HasMaxLength(100);

                    b.Property<int?>("SaleType");

                    b.Property<int>("Status");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<decimal?>("TotalAnnualRent");

                    b.Property<decimal?>("TotalAnnualService");

                    b.Property<decimal?>("TotalMonthlyRent");

                    b.Property<decimal?>("TotalMonthlyService");

                    b.Property<decimal?>("TotalOtherCosts");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("UserOwner")
                        .HasMaxLength(256);

                    b.Property<int?>("YearRentalDuration");

                    b.HasKey("QuotationId");

                    b.HasIndex("CommercialEntityID");

                    b.HasIndex("InstrumentTypeID");

                    b.ToTable("Quotations");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationElitePCRKit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("Discount");

                    b.Property<int>("ElitePCRKitId");

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<int?>("TestPerYear");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("ElitePCRKitId");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationElitePCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationExtraction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("Discount");

                    b.Property<int>("ExtractionId");

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("ExtractionId");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationExtractions");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationOpenPCRKit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AnalysisType");

                    b.Property<int?>("Control");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<decimal?>("Discount");

                    b.Property<int?>("ExtractionType");

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<decimal?>("KitCosts");

                    b.Property<int?>("KitSize");

                    b.Property<int?>("MasterMix");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<int>("OpenPCRKitId");

                    b.Property<int>("PCRType");

                    b.Property<string>("ProductPartNumber")
                        .HasMaxLength(50);

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<decimal?>("StandardCost");

                    b.Property<int?>("TestPerYear");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<bool>("bHidden");

                    b.Property<int?>("ggExpiry");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationOpenPCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category")
                        .HasMaxLength(20);

                    b.Property<decimal?>("Cost");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<decimal?>("Help1");

                    b.Property<decimal?>("Help2");

                    b.Property<decimal?>("Margin");

                    b.Property<decimal?>("Price");

                    b.Property<decimal?>("Profit");

                    b.Property<int?>("QuotationId");

                    b.Property<bool?>("Visibility");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationsTotal");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotalPCR", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Control");

                    b.Property<int>("ControlExpiry");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<int>("KitPerYear");

                    b.Property<int>("KitSize");

                    b.Property<int>("PCRType");

                    b.Property<decimal>("Price");

                    b.Property<int?>("QuotationId");

                    b.Property<int>("Reaction");

                    b.Property<decimal>("StandardCost");

                    b.Property<string>("Target")
                        .HasMaxLength(50);

                    b.Property<int>("TestForYear");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationsTotalPCR");
                });

            modelBuilder.Entity("WebApp.Entity.UploadFile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileName")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("FileType")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<string>("ReferencePeriod")
                        .IsRequired()
                        .HasMaxLength(6);

                    b.Property<string>("SerialNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<DateTime?>("TmstDelete");

                    b.Property<DateTime?>("TmstProcess");

                    b.Property<DateTime?>("TmstUpload");

                    b.Property<string>("UserDelete")
                        .HasMaxLength(256);

                    b.Property<string>("UserUpload")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("SerialNumber", "FileType");

                    b.ToTable("UploadFiles");
                });

            modelBuilder.Entity("WebApp.Entity.UserCfg", b =>
                {
                    b.Property<string>("UserID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Area")
                        .HasMaxLength(50);

                    b.Property<string>("CommercialEntity")
                        .HasMaxLength(100);

                    b.Property<string>("Country")
                        .HasMaxLength(250);

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("TmstConfirmCfg");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserApproval1")
                        .HasMaxLength(450);

                    b.Property<string>("UserApproval2")
                        .HasMaxLength(450);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("fullName")
                        .HasMaxLength(100);

                    b.HasKey("UserID");

                    b.ToTable("UserCfg");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.HasOne("WebApp.Entity.CommercialEntity", "CommercialEntity")
                        .WithMany()
                        .HasForeignKey("CommercialEntityID");

                    b.HasOne("WebApp.Entity.InstrumentType", "InstrumentType")
                        .WithMany()
                        .HasForeignKey("InstrumentTypeID");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationElitePCRKit", b =>
                {
                    b.HasOne("WebApp.Entity.ElitePCRKit", "ElitePCRKit")
                        .WithMany()
                        .HasForeignKey("ElitePCRKitId");

                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationElitePCRKits")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationExtraction", b =>
                {
                    b.HasOne("WebApp.Entity.Extraction", "Extraction")
                        .WithMany()
                        .HasForeignKey("ExtractionId");

                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationExtractions")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationOpenPCRKit", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationOpenPCRKits")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotal", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany()
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotalPCR", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany()
                        .HasForeignKey("QuotationId");
                });
        }
    }
}

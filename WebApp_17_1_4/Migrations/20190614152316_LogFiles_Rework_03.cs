﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class LogFiles_Rework_03 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClaimCheckCfgs",
                columns: table => new
                {
                    Script = table.Column<string>(maxLength: 50, nullable: false),
                    CheckName = table.Column<string>(maxLength: 10, nullable: false),
                    TrackIdx = table.Column<int>(nullable: false),
                    CheckSeq = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimCheckCfgs", x => new { x.Script, x.CheckName, x.TrackIdx });
                });

            migrationBuilder.CreateTable(
                name: "ClaimPressuresSN",
                columns: table => new
                {
                    ClaimPressureSNId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    Script = table.Column<string>(maxLength: 50, nullable: true),
                    SessionNumber = table.Column<int>(nullable: false),
                    TmstRif = table.Column<DateTime>(nullable: false),
                    TrackIdx = table.Column<int>(nullable: false),
                    ValueInit = table.Column<int>(nullable: true),
                    ValueAsp = table.Column<int>(nullable: true),
                    ValueTipOn = table.Column<int>(nullable: true),
                    ValueDisp = table.Column<int>(nullable: true),
                    ValueLeak1 = table.Column<int>(nullable: true),
                    ValueLeak2 = table.Column<int>(nullable: true),
                    ValueLeak3 = table.Column<int>(nullable: true),
                    ValueClotAsp = table.Column<int>(nullable: true),
                    ValueClotDisp = table.Column<int>(nullable: true),
                    ValueTipOff = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimPressuresSN", x => x.ClaimPressureSNId);
                    table.ForeignKey(
                        name: "FK_ClaimPressuresSN_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClaimPressuresSN_ClaimId",
                table: "ClaimPressuresSN",
                column: "ClaimId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaimCheckCfgs");

            migrationBuilder.DropTable(
                name: "ClaimPressuresSN");
        }
    }
}

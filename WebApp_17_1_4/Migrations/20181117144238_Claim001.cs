﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Claim001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Claims",
                columns: table => new
                {
                    ClaimId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileName = table.Column<string>(maxLength: 500, nullable: true),
                    UserUpload = table.Column<string>(maxLength: 256, nullable: true),
                    TmstUpload = table.Column<DateTime>(nullable: false),
                    NoteUpload = table.Column<string>(maxLength: 1000, nullable: true),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: true),
                    TmstProcess = table.Column<DateTime>(nullable: true),
                    TmstGetPressMin = table.Column<DateTime>(nullable: true),
                    TmstGetPressMax = table.Column<DateTime>(nullable: true),
                    TmstGetTempMin = table.Column<DateTime>(nullable: true),
                    TmstGetTempMax = table.Column<DateTime>(nullable: true),
                    TmstAssign = table.Column<DateTime>(nullable: false),
                    UserAssigned = table.Column<string>(maxLength: 256, nullable: true),
                    NoteAssign = table.Column<string>(maxLength: 1000, nullable: true),
                    NoteClose = table.Column<string>(maxLength: 1000, nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Claims", x => x.ClaimId);
                });

            migrationBuilder.CreateTable(
                name: "ClaimPressures",
                columns: table => new
                {
                    ClaimPressureId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    TmstGet = table.Column<DateTime>(nullable: false),
                    ValueGet01 = table.Column<int>(nullable: false),
                    ValueGet02 = table.Column<int>(nullable: false),
                    ValueGet03 = table.Column<int>(nullable: false),
                    ValueGet04 = table.Column<int>(nullable: false),
                    ValueGet05 = table.Column<int>(nullable: false),
                    ValueGet06 = table.Column<int>(nullable: false),
                    ValueGet07 = table.Column<int>(nullable: false),
                    ValueGet08 = table.Column<int>(nullable: false),
                    ValueGet09 = table.Column<int>(nullable: false),
                    ValueGet10 = table.Column<int>(nullable: false),
                    ValueGet11 = table.Column<int>(nullable: false),
                    ValueGet12 = table.Column<int>(nullable: false),
                    ValueGet13 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimPressures", x => x.ClaimPressureId);
                    table.ForeignKey(
                        name: "FK_ClaimPressures_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClaimRows",
                columns: table => new
                {
                    ClaimRowId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    ErrorCode = table.Column<string>(maxLength: 50, nullable: true),
                    Note = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimRows", x => x.ClaimRowId);
                    table.ForeignKey(
                        name: "FK_ClaimRows_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClaimTemperatures",
                columns: table => new
                {
                    ClaimTemperatureId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    TmstGet = table.Column<DateTime>(nullable: false),
                    ValueGet = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimTemperatures", x => x.ClaimTemperatureId);
                    table.ForeignKey(
                        name: "FK_ClaimTemperatures_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClaimTracks",
                columns: table => new
                {
                    ClaimTrackId = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    Note = table.Column<string>(maxLength: 1000, nullable: true),
                    TmstInse = table.Column<DateTime>(nullable: false),
                    UserInse = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimTracks", x => x.ClaimTrackId);
                    table.ForeignKey(
                        name: "FK_ClaimTracks_Claims_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claims",
                        principalColumn: "ClaimId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClaimPressures_ClaimId",
                table: "ClaimPressures",
                column: "ClaimId");

            migrationBuilder.CreateIndex(
                name: "IX_ClaimRows_ClaimId",
                table: "ClaimRows",
                column: "ClaimId");

            migrationBuilder.CreateIndex(
                name: "IX_ClaimTemperatures_ClaimId",
                table: "ClaimTemperatures",
                column: "ClaimId");

            migrationBuilder.CreateIndex(
                name: "IX_ClaimTracks_ClaimId",
                table: "ClaimTracks",
                column: "ClaimId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClaimPressures");

            migrationBuilder.DropTable(
                name: "ClaimRows");

            migrationBuilder.DropTable(
                name: "ClaimTemperatures");

            migrationBuilder.DropTable(
                name: "ClaimTracks");

            migrationBuilder.DropTable(
                name: "Claims");
        }
    }
}

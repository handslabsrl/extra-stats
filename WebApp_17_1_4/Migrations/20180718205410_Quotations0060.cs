﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Quotations0060 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "UserCfg",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation5",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation6",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold5",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold6",
                table: "ElitePCRKits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "UserCfg");

            migrationBuilder.DropColumn(
                name: "Compensation5",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Compensation6",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold5",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold6",
                table: "ElitePCRKits");
        }
    }
}

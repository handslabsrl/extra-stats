﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Claim008 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProcedureNote",
                table: "ClaimRows",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "ClaimRows",
                maxLength: 1,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Scostamento",
                table: "ClaimPressures",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Tipo",
                table: "ClaimPressures",
                maxLength: 1,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProcedureNote",
                table: "ClaimRows");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "ClaimRows");

            migrationBuilder.DropColumn(
                name: "Scostamento",
                table: "ClaimPressures");

            migrationBuilder.DropColumn(
                name: "Tipo",
                table: "ClaimPressures");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class NewFields004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TotalMonthlyRent",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalMonthlyService",
                table: "Quotations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TotalMonthlyRent",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TotalMonthlyService",
                table: "Quotations");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rework_2020_08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "hotelPayed",
                table: "TrainingRegistrations");

            migrationBuilder.AddColumn<bool>(
                name: "hotelPaid",
                table: "TrainingRegistrations",
                nullable: true,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "hotelPaid",
                table: "TrainingRegistrations");

            migrationBuilder.AddColumn<bool>(
                name: "hotelPayed",
                table: "TrainingRegistrations",
                nullable: true,
                defaultValue: false);
        }
    }
}

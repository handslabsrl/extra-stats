﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class NewFields003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CurrentBusinessPerYear",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note",
                table: "Quotations",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation1",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation2",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation3",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Compensation4",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold1",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold2",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold3",
                table: "ElitePCRKits",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Threshold4",
                table: "ElitePCRKits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CurrentBusinessPerYear",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "Note",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "Compensation1",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Compensation2",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Compensation3",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Compensation4",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold1",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold2",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold3",
                table: "ElitePCRKits");

            migrationBuilder.DropColumn(
                name: "Threshold4",
                table: "ElitePCRKits");
        }
    }
}

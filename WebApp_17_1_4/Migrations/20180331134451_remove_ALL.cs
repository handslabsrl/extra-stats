﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class remove_ALL : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataMain");

            migrationBuilder.DropTable(
                name: "GAssayPrograms");

            migrationBuilder.DropTable(
                name: "SGAT");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GAssayPrograms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AssayName = table.Column<string>(nullable: true),
                    AssayParameterToolVersion = table.Column<string>(nullable: true),
                    ConversionFactorIU = table.Column<double>(nullable: false),
                    CtsAndTmOnly = table.Column<int>(nullable: false),
                    EndRampTemperature = table.Column<int>(nullable: false),
                    ExtractedEluteVolume = table.Column<int>(nullable: false),
                    ExtractionCassetteID = table.Column<string>(nullable: true),
                    ExtractionInputVolume = table.Column<int>(nullable: false),
                    IVDCleared = table.Column<int>(nullable: false),
                    LowerTolerance = table.Column<int>(nullable: false),
                    MeltRequired = table.Column<int>(nullable: false),
                    MeltingOptional = table.Column<int>(nullable: false),
                    OffSet = table.Column<int>(nullable: false),
                    PCRAmplificationCycles = table.Column<int>(nullable: false),
                    PCRAmplificationSteps = table.Column<int>(nullable: false),
                    PCRCassetteID = table.Column<string>(nullable: true),
                    PCRInputEluateVolume = table.Column<int>(nullable: false),
                    Pathogen_TargetName = table.Column<string>(nullable: true),
                    PreCycleStepNumber = table.Column<int>(nullable: false),
                    RampRate = table.Column<double>(nullable: false),
                    RegisteredDate = table.Column<DateTime>(nullable: false),
                    ResultsScaling = table.Column<int>(nullable: false),
                    SampleMatrix_Id = table.Column<int>(nullable: true),
                    SampleTypes = table.Column<int>(nullable: false),
                    Serial = table.Column<string>(nullable: true),
                    SonicationCycle = table.Column<int>(nullable: false),
                    SonicationOffTime = table.Column<int>(nullable: false),
                    SonicationOnTime = table.Column<int>(nullable: false),
                    StartRampTemperature = table.Column<int>(nullable: false),
                    UpperTolerance = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GAssayPrograms", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SGAT",
                columns: table => new
                {
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Commercial_Status = table.Column<string>(maxLength: 500, nullable: true),
                    CompanyName = table.Column<string>(maxLength: 500, nullable: true),
                    CustomerCode = table.Column<string>(maxLength: 10, nullable: true),
                    Finance_Status = table.Column<string>(maxLength: 50, nullable: true),
                    Installation_Date = table.Column<DateTime>(nullable: true),
                    Site_Address = table.Column<string>(maxLength: 500, nullable: true),
                    Site_City = table.Column<string>(maxLength: 500, nullable: true),
                    Site_Code = table.Column<string>(maxLength: 10, nullable: true),
                    Site_Country = table.Column<string>(maxLength: 500, nullable: true),
                    Site_Description = table.Column<string>(maxLength: 500, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SGAT", x => x.SerialNumber);
                });

            migrationBuilder.CreateTable(
                name: "DataMain",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AbortTime = table.Column<DateTime>(nullable: true),
                    ApprovalDateTime = table.Column<DateTime>(nullable: true),
                    ApprovalStatus = table.Column<int>(nullable: true),
                    ApprovalUserName = table.Column<string>(nullable: true),
                    ApprovalUserRole = table.Column<int>(nullable: true),
                    AssayDetail = table.Column<string>(nullable: true),
                    AssayDetailForCopiesPerMl = table.Column<string>(nullable: true),
                    AssayDetailForGeqPerMl = table.Column<string>(nullable: true),
                    AssayDetailForIuPerMl = table.Column<string>(nullable: true),
                    AssayDetailShortening = table.Column<string>(nullable: true),
                    AssayDetailShorteningForCopiesPerMl = table.Column<string>(nullable: true),
                    AssayDetailShorteningForGeqPerMl = table.Column<string>(nullable: true),
                    AssayDetailShorteningForIuPerMl = table.Column<string>(nullable: true),
                    AssayDetailShorteningUnit = table.Column<string>(nullable: true),
                    AssayName = table.Column<string>(nullable: true),
                    CalibratorExpiryDate = table.Column<DateTime>(nullable: true),
                    CalibratorLot = table.Column<string>(nullable: true),
                    ControlExpiryDate = table.Column<DateTime>(nullable: true),
                    ControlLot = table.Column<string>(nullable: true),
                    DilutionFactor = table.Column<int>(nullable: true),
                    EndRunTime = table.Column<DateTime>(nullable: true),
                    ExtractedEluteVolume = table.Column<int>(nullable: true),
                    ExtractionCassetteExpiryDate = table.Column<DateTime>(nullable: true),
                    ExtractionCassetteLot = table.Column<string>(nullable: true),
                    ExtractionCassetteSerialNumber = table.Column<string>(nullable: true),
                    ExtractionInputVolume = table.Column<int>(nullable: true),
                    GeneralAssayID = table.Column<int>(nullable: true),
                    ICLot = table.Column<string>(nullable: true),
                    IcExpiryDate = table.Column<DateTime>(nullable: true),
                    IcTubeSize = table.Column<int>(nullable: true),
                    InventoryBlockBarcode = table.Column<string>(nullable: true),
                    InventoryBlockConfirmationMethod = table.Column<string>(nullable: true),
                    InventoryBlockName = table.Column<string>(nullable: true),
                    InventoryBlockNumber = table.Column<int>(nullable: true),
                    LISStatusv = table.Column<int>(nullable: true),
                    LISUploadSelected = table.Column<int>(nullable: true),
                    MeasuredCalibrationResultID = table.Column<int>(nullable: true),
                    MeasuredControlResultID = table.Column<int>(nullable: true),
                    Melt = table.Column<int>(nullable: true),
                    PCRReactionCassetteSerialNumber = table.Column<string>(nullable: true),
                    PositionToPlaceSample = table.Column<int>(nullable: false),
                    Process = table.Column<int>(nullable: false),
                    ReagentExpiryDate = table.Column<DateTime>(nullable: true),
                    ReagentLot = table.Column<string>(nullable: true),
                    ReagentTubeSize = table.Column<int>(nullable: true),
                    RefEluteTrack = table.Column<int>(nullable: true),
                    RunName = table.Column<string>(nullable: true),
                    RunSetupID = table.Column<int>(nullable: true),
                    SampleID = table.Column<byte[]>(nullable: true),
                    SampleMatrixName = table.Column<string>(nullable: true),
                    SampleTypes = table.Column<int>(nullable: true),
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Sonication = table.Column<int>(nullable: false),
                    SonicationCycle = table.Column<int>(nullable: true),
                    SonicationOffTime = table.Column<int>(nullable: true),
                    SonicationOnTime = table.Column<int>(nullable: true),
                    StartRunTime = table.Column<DateTime>(nullable: true),
                    TrackNumber = table.Column<int>(nullable: true),
                    UserName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataMain", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DataMain_SGAT_SerialNumber",
                        column: x => x.SerialNumber,
                        principalTable: "SGAT",
                        principalColumn: "SerialNumber",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataMain_SerialNumber",
                table: "DataMain",
                column: "SerialNumber");
        }
    }
}

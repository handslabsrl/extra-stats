﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Data;
using WebApp.Classes;

namespace WebApp.Migrations
{
    [DbContext(typeof(WebAppDbContext))]
    [Migration("20180426183928_Quotations010")]
    partial class Quotations010
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp.Entity.CommercialEntity", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("SubTotal1");

                    b.Property<int>("SubTotal2");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("Total");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("CommercialEntity");
                });

            modelBuilder.Entity("WebApp.Entity.ElitePCRKit", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AnalysisType");

                    b.Property<decimal?>("Compensation");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("ExtractionType");

                    b.Property<int?>("KitSize");

                    b.Property<decimal?>("NoCompensation");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<string>("PCRTarget")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<int>("PCRType");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<int?>("ggExpiry");

                    b.HasKey("ID");

                    b.ToTable("ElitePCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.InstrumentType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<decimal>("OfficialPrice");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("InstrumentType");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.Property<int>("QuotationId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CommercialEntityID");

                    b.Property<int?>("ContractDuration");

                    b.Property<int?>("ContractType");

                    b.Property<decimal?>("CurrentBusiness");

                    b.Property<string>("CustomerAddress")
                        .HasMaxLength(250);

                    b.Property<string>("CustomerName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("CustomerType");

                    b.Property<decimal?>("InstrumentDiscount");

                    b.Property<string>("InstrumentNumber");

                    b.Property<decimal?>("InstrumentSellingPrice");

                    b.Property<int?>("InstrumentTypeID");

                    b.Property<bool>("NewCustomer");

                    b.Property<string>("ReferencePerson")
                        .HasMaxLength(100);

                    b.Property<int?>("SaleType");

                    b.Property<int>("Status");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("UserOwner")
                        .HasMaxLength(256);

                    b.HasKey("QuotationId");

                    b.HasIndex("CommercialEntityID");

                    b.HasIndex("InstrumentTypeID");

                    b.ToTable("Quotations");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.HasOne("WebApp.Entity.CommercialEntity", "CommercialEntity")
                        .WithMany()
                        .HasForeignKey("CommercialEntityID");

                    b.HasOne("WebApp.Entity.InstrumentType", "InstrumentType")
                        .WithMany()
                        .HasForeignKey("InstrumentTypeID");
                });
        }
    }
}

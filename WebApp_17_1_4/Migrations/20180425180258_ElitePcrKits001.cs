﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class ElitePcrKits001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ElitePCRKits",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PCRTarget = table.Column<string>(maxLength: 30, nullable: false),
                    ProductPartNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    PCRType = table.Column<int>(nullable: false),
                    ExtractionType = table.Column<int>(nullable: false),
                    AnalysisType = table.Column<int>(nullable: false),
                    ggExpiry = table.Column<int>(nullable: false),
                    KitSize = table.Column<int>(nullable: false),
                    StandardCost = table.Column<decimal>(nullable: false),
                    OfficialPrice = table.Column<decimal>(nullable: false),
                    noCompensation = table.Column<decimal>(nullable: false),
                    Compensaton = table.Column<decimal>(nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElitePCRKits", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ElitePCRKits");
        }
    }
}

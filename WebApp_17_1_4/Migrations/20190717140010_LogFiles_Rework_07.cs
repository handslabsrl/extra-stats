﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class LogFiles_Rework_07 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Script",
                table: "ClaimPressures",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SessionNumber",
                table: "ClaimPressures",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Script",
                table: "ClaimPressures");

            migrationBuilder.DropColumn(
                name: "SessionNumber",
                table: "ClaimPressures");
        }
    }
}

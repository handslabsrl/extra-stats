﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Extraction002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationExtractions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuotationId = table.Column<int>(nullable: true),
                    ExtractionId = table.Column<int>(nullable: false),
                    SellingPrice = table.Column<decimal>(nullable: true),
                    Discount = table.Column<decimal>(nullable: true),
                    UserCreate = table.Column<string>(maxLength: 256, nullable: true),
                    TmstCreate = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationExtractions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationExtractions_Extractions_ExtractionId",
                        column: x => x.ExtractionId,
                        principalTable: "Extractions",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuotationExtractions_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationExtractions_ExtractionId",
                table: "QuotationExtractions",
                column: "ExtractionId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationExtractions_QuotationId",
                table: "QuotationExtractions",
                column: "QuotationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationExtractions");
        }
    }
}

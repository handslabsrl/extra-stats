﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Rework_2020_19 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TmstCleaning",
                table: "Claims",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ProcessRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FileNameDownload = table.Column<string>(maxLength: 256, nullable: true),
                    NoteExec = table.Column<string>(maxLength: 1000, nullable: true),
                    ProcessParms = table.Column<string>(maxLength: 1000, nullable: true),
                    ProcessType = table.Column<string>(maxLength: 30, nullable: false),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    TmstExec = table.Column<DateTime>(nullable: true),
                    TmstInse = table.Column<DateTime>(nullable: false),
                    UserInse = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessRequests", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProcessRequests");

            migrationBuilder.DropColumn(
                name: "TmstCleaning",
                table: "Claims");
        }
    }
}

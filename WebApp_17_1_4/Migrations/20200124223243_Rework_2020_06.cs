﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rework_2020_06 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "viewTrainingPortfolio",
                table: "TrainingTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "hotelPayed",
                table: "TrainingRegistrations",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "EmailTemplates",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "viewTrainingPortfolio",
                table: "TrainingTypes");

            migrationBuilder.DropColumn(
                name: "hotelPayed",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "EmailTemplates");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class OpenPcrKit001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationOpenPCRKits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuotationId = table.Column<int>(nullable: true),
                    OpenPCRKitId = table.Column<int>(nullable: false),
                    TestPerYear = table.Column<int>(nullable: true),
                    SellingPrice = table.Column<decimal>(nullable: true),
                    Discount = table.Column<decimal>(nullable: true),
                    FreeOfChargeKit = table.Column<int>(nullable: true),
                    PCRTarget = table.Column<string>(maxLength: 30, nullable: false),
                    ProductPartNumber = table.Column<string>(maxLength: 50, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    PCRType = table.Column<int>(nullable: false),
                    ExtractionType = table.Column<int>(nullable: true),
                    AnalysisType = table.Column<int>(nullable: true),
                    ggExpiry = table.Column<int>(nullable: true),
                    KitSize = table.Column<int>(nullable: true),
                    Control = table.Column<int>(nullable: false),
                    StandardCost = table.Column<decimal>(nullable: true),
                    OfficialPrice = table.Column<decimal>(nullable: true),
                    TmstCreate = table.Column<DateTime>(nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserCreate = table.Column<string>(maxLength: 256, nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationOpenPCRKits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationOpenPCRKits_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationOpenPCRKits_QuotationId",
                table: "QuotationOpenPCRKits",
                column: "QuotationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationOpenPCRKits");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rework_2020_02 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SessionNumber",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet01",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet02",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet03",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet04",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet05",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet06",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet07",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet08",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet09",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet10",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet11",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGet12",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ValueGetAvg",
                table: "ClaimTemperatures",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "labelVolume",
                table: "ClaimPressuresSN",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SessionNumber",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet01",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet02",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet03",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet04",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet05",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet06",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet07",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet08",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet09",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet10",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet11",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGet12",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "ValueGetAvg",
                table: "ClaimTemperatures");

            migrationBuilder.DropColumn(
                name: "labelVolume",
                table: "ClaimPressuresSN");
        }
    }
}

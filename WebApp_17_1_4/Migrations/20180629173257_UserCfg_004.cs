﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class UserCfg_004 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommercialEntityID",
                table: "UserCfg");

            migrationBuilder.AddColumn<string>(
                name: "CommercialEntity",
                table: "UserCfg",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommercialEntity",
                table: "UserCfg");

            migrationBuilder.AddColumn<int>(
                name: "CommercialEntityID",
                table: "UserCfg",
                nullable: true);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class UserCfg_001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserCfg",
                columns: table => new
                {
                    UserID = table.Column<string>(nullable: false),
                    Area = table.Column<string>(nullable: true),
                    CommercialEntityID = table.Column<int>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Region = table.Column<string>(nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserApproval1 = table.Column<string>(nullable: true),
                    UserApproval2 = table.Column<string>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserCfg", x => x.UserID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserCfg");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Rental01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "LISConnection",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalMonthlyRent",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalMonthlyService",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalOtherCosts",

                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "YearRentalDuration",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "percService",
                table: "Quotations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LISConnection",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TotalMonthlyRent",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TotalMonthlyService",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "TotalOtherCosts",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "YearRentalDuration",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "percService",
                table: "Quotations");
        }
    }
}

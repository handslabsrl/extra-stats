﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class CommercialEntity003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CommercialEntity",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    SubTotal1 = table.Column<int>(nullable: false),
                    SubTotal2 = table.Column<int>(nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    Total = table.Column<int>(nullable: false),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommercialEntity", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "InstrumentType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    OfficialPrice = table.Column<decimal>(nullable: false),
                    Ref = table.Column<string>(maxLength: 50, nullable: false),
                    StandardCost = table.Column<decimal>(nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    Type = table.Column<string>(maxLength: 50, nullable: false),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InstrumentType", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommercialEntity");

            migrationBuilder.DropTable(
                name: "InstrumentType");
        }
    }
}

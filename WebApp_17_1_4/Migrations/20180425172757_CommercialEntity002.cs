﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class CommercialEntity002 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommercialEntity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CommercialEntity",
                columns: table => new
                {
                    CommercialEntityId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CommercialEntityDescription = table.Column<string>(maxLength: 50, nullable: false),
                    SubTotal1 = table.Column<int>(nullable: false),
                    SubTotal2 = table.Column<int>(nullable: false),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    Total = table.Column<int>(nullable: false),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommercialEntity", x => x.CommercialEntityId);
                });
        }
    }
}

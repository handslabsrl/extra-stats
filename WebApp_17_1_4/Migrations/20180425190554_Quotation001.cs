﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Quotation001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Quotations",
                columns: table => new
                {
                    QuotationId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Status = table.Column<int>(nullable: false),
                    CommercialEntityID = table.Column<int>(nullable: false),
                    CustomerName = table.Column<string>(maxLength: 100, nullable: false),
                    CustomerAddress = table.Column<string>(maxLength: 250, nullable: false),
                    ReferencePerson = table.Column<string>(maxLength: 100, nullable: false),
                    CustomerType = table.Column<int>(nullable: false),
                    NewCustomer = table.Column<bool>(nullable: false),
                    CurrentBusiness = table.Column<decimal>(nullable: true),
                    UserOwner = table.Column<string>(maxLength: 256, nullable: true),
                    TmstCreate = table.Column<DateTime>(nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Quotations", x => x.QuotationId);
                    table.ForeignKey(
                        name: "FK_Quotations_CommercialEntity_CommercialEntityID",
                        column: x => x.CommercialEntityID,
                        principalTable: "CommercialEntity",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Quotations_CommercialEntityID",
                table: "Quotations",
                column: "CommercialEntityID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Quotations");
        }
    }
}

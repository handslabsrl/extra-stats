﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class DataMining001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DataMining",
                columns: table => new
                {
                    DataMiningId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateFrom = table.Column<DateTime>(nullable: true),
                    DateTo = table.Column<DateTime>(nullable: true),
                    DateUpToDate = table.Column<DateTime>(nullable: true),
                    ExtractionType = table.Column<string>(maxLength: 20, nullable: false),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    NoteError = table.Column<string>(maxLength: 1000, nullable: true),
                    FileName = table.Column<string>(maxLength: 250, nullable: true),
                    TmstInse = table.Column<DateTime>(nullable: false),
                    UserInse = table.Column<string>(maxLength: 256, nullable: true),
                    VersionExtra = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DataMining", x => x.DataMiningId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DataMining_UserInse",
                table: "DataMining",
                column: "UserInse");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DataMining");
        }
    }
}

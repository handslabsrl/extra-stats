﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Data;
using WebApp.Classes;

namespace WebApp.Migrations
{
    [DbContext(typeof(WebAppDbContext))]
    [Migration("20200214165543_Rework_2020_10")]
    partial class Rework_2020_10
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp.Entity.Claim", b =>
                {
                    b.Property<int>("ClaimId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileName")
                        .HasMaxLength(500);

                    b.Property<string>("NoteAssign")
                        .HasMaxLength(1000);

                    b.Property<string>("NoteClose")
                        .HasMaxLength(1000);

                    b.Property<string>("NoteError")
                        .HasMaxLength(1000);

                    b.Property<string>("NoteUpload")
                        .HasMaxLength(1000);

                    b.Property<DateTime?>("RequestedDateFrom");

                    b.Property<DateTime?>("RequestedDateTo");

                    b.Property<string>("SerialNumber")
                        .HasMaxLength(50);

                    b.Property<string>("SoftwareVersion")
                        .HasMaxLength(30);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<DateTime?>("TmstAssign");

                    b.Property<DateTime?>("TmstGetPressMax");

                    b.Property<DateTime?>("TmstGetPressMin");

                    b.Property<DateTime?>("TmstGetTempMax");

                    b.Property<DateTime?>("TmstGetTempMin");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<DateTime?>("TmstProcess");

                    b.Property<DateTime>("TmstUpload");

                    b.Property<string>("UserAssigned")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("UserUpload")
                        .HasMaxLength(256);

                    b.HasKey("ClaimId");

                    b.ToTable("Claims");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimCheckCfg", b =>
                {
                    b.Property<string>("Script")
                        .HasMaxLength(50);

                    b.Property<string>("CheckName")
                        .HasMaxLength(10);

                    b.Property<int>("CheckType");

                    b.Property<int>("CheckSeq");

                    b.HasKey("Script", "CheckName", "CheckType");

                    b.ToTable("ClaimCheckCfgs");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimPressure", b =>
                {
                    b.Property<long>("ClaimPressureId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("CtrlType")
                        .HasMaxLength(1);

                    b.Property<bool>("OffSet");

                    b.Property<string>("RecType")
                        .HasMaxLength(20);

                    b.Property<string>("Script")
                        .HasMaxLength(50);

                    b.Property<int>("SessionNumber");

                    b.Property<DateTime>("TmstGet");

                    b.Property<int>("ValueGet01");

                    b.Property<int>("ValueGet02");

                    b.Property<int>("ValueGet03");

                    b.Property<int>("ValueGet04");

                    b.Property<int>("ValueGet05");

                    b.Property<int>("ValueGet06");

                    b.Property<int>("ValueGet07");

                    b.Property<int>("ValueGet08");

                    b.Property<int>("ValueGet09");

                    b.Property<int>("ValueGet10");

                    b.Property<int>("ValueGet11");

                    b.Property<int>("ValueGet12");

                    b.Property<int>("ValueGet13");

                    b.HasKey("ClaimPressureId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimPressures");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimPressureRecType", b =>
                {
                    b.Property<string>("RecType")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(20);

                    b.Property<int>("eluateOrder");

                    b.Property<int>("mmixOrder");

                    b.HasKey("RecType");

                    b.ToTable("ClaimPressureRecTypes");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimPressureSN", b =>
                {
                    b.Property<long>("ClaimPressureSNId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("Script")
                        .HasMaxLength(50);

                    b.Property<int>("SessionNumber");

                    b.Property<int>("SubTrack");

                    b.Property<DateTime>("TmstRif");

                    b.Property<int>("TrackIdx");

                    b.Property<int>("TrackSeq");

                    b.Property<int?>("ValueAsp");

                    b.Property<int?>("ValueClotAsp");

                    b.Property<int?>("ValueClotDisp");

                    b.Property<int?>("ValueDisp");

                    b.Property<int?>("ValueInit");

                    b.Property<int?>("ValueLeak1");

                    b.Property<int?>("ValueLeak2");

                    b.Property<int?>("ValueLeak3");

                    b.Property<int?>("ValueLiquidVolume");

                    b.Property<int?>("ValueTipOff");

                    b.Property<int?>("ValueTipOn");

                    b.Property<string>("labelVolume")
                        .HasMaxLength(20);

                    b.HasKey("ClaimPressureSNId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimPressuresSN");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimRow", b =>
                {
                    b.Property<long>("ClaimRowId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("ErrorCode")
                        .HasMaxLength(50);

                    b.Property<string>("Folder")
                        .HasMaxLength(50);

                    b.Property<string>("Note");

                    b.Property<bool>("OutOfSession");

                    b.Property<string>("ProcedureNote")
                        .HasMaxLength(1000);

                    b.Property<string>("Script")
                        .HasMaxLength(50);

                    b.Property<int?>("SessionNumber");

                    b.Property<string>("Status")
                        .HasMaxLength(1);

                    b.Property<DateTime?>("TmstGet");

                    b.HasKey("ClaimRowId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimRows");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimScript", b =>
                {
                    b.Property<long>("ClaimScriptId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("Folder")
                        .HasMaxLength(50);

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<string>("Script")
                        .HasMaxLength(50);

                    b.Property<int>("SessionNumber");

                    b.Property<string>("Status")
                        .HasMaxLength(1);

                    b.Property<DateTime>("TmstGet");

                    b.HasKey("ClaimScriptId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimScripts");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimSnapshot", b =>
                {
                    b.Property<long>("ClaimSnapshotId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("FileName")
                        .HasMaxLength(500);

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("TmstInse");

                    b.Property<string>("UserInse")
                        .HasMaxLength(256);

                    b.Property<string>("tipo")
                        .HasMaxLength(1);

                    b.HasKey("ClaimSnapshotId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimSnapshots");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimTemperature", b =>
                {
                    b.Property<long>("ClaimTemperatureId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("RecType")
                        .HasMaxLength(1);

                    b.Property<int>("SessionNumber");

                    b.Property<DateTime>("TmstGet");

                    b.Property<int>("ValueCenter");

                    b.Property<int>("ValueGet")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(0);

                    b.Property<decimal>("ValueGet01");

                    b.Property<decimal>("ValueGet02");

                    b.Property<decimal>("ValueGet03");

                    b.Property<decimal>("ValueGet04");

                    b.Property<decimal>("ValueGet05");

                    b.Property<decimal>("ValueGet06");

                    b.Property<decimal>("ValueGet07");

                    b.Property<decimal>("ValueGet08");

                    b.Property<decimal>("ValueGet09");

                    b.Property<decimal>("ValueGet10");

                    b.Property<decimal>("ValueGet11");

                    b.Property<decimal>("ValueGet12");

                    b.Property<decimal>("ValueGetAvg");

                    b.Property<int>("ValueLeft");

                    b.Property<int>("ValueRight");

                    b.HasKey("ClaimTemperatureId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimTemperatures");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimTrack", b =>
                {
                    b.Property<long>("ClaimTrackId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ClaimId");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<DateTime>("TmstInse");

                    b.Property<string>("UserInse")
                        .HasMaxLength(256);

                    b.HasKey("ClaimTrackId");

                    b.HasIndex("ClaimId");

                    b.ToTable("ClaimTracks");
                });

            modelBuilder.Entity("WebApp.Entity.CommercialEntity", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("SubTotal1");

                    b.Property<int>("SubTotal2");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("Total");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("CommercialEntity");
                });

            modelBuilder.Entity("WebApp.Entity.Company", b =>
                {
                    b.Property<string>("CompanyId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(200);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("CompanyId");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("WebApp.Entity.Country", b =>
                {
                    b.Property<string>("CountryId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(100);

                    b.Property<string>("CallingCode")
                        .HasMaxLength(10);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("CountryId");

                    b.ToTable("Countries");
                });

            modelBuilder.Entity("WebApp.Entity.DataMining", b =>
                {
                    b.Property<int>("DataMiningId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CommercialEntity")
                        .HasMaxLength(100);

                    b.Property<string>("CommercialStatus")
                        .HasMaxLength(250);

                    b.Property<string>("Country")
                        .HasMaxLength(250);

                    b.Property<DateTime?>("DateFrom");

                    b.Property<DateTime?>("DateTo");

                    b.Property<DateTime?>("DateUpToDate");

                    b.Property<string>("ExtractionType")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("FileName")
                        .HasMaxLength(250);

                    b.Property<string>("NoteError")
                        .HasMaxLength(1000);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<DateTime>("TmstInse");

                    b.Property<string>("UserInse")
                        .HasMaxLength(256);

                    b.Property<string>("VersionExtra")
                        .HasMaxLength(100);

                    b.HasKey("DataMiningId");

                    b.HasIndex("UserInse");

                    b.ToTable("DataMining");
                });

            modelBuilder.Entity("WebApp.Entity.Department", b =>
                {
                    b.Property<int>("DepartmentId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("DepartmentId");

                    b.ToTable("Departments");
                });

            modelBuilder.Entity("WebApp.Entity.ElitePCRKit", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AnalysisType");

                    b.Property<decimal?>("Compensation");

                    b.Property<decimal?>("Compensation1");

                    b.Property<decimal?>("Compensation2");

                    b.Property<decimal?>("Compensation3");

                    b.Property<decimal?>("Compensation4");

                    b.Property<decimal?>("Compensation5");

                    b.Property<decimal?>("Compensation6");

                    b.Property<int?>("CutOff");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int>("ExtractionType");

                    b.Property<int?>("KitSize");

                    b.Property<int?>("MinTheoreticalCons");

                    b.Property<int?>("MultipleReaction");

                    b.Property<decimal?>("NoCompensation");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<string>("PCRTarget")
                        .IsRequired()
                        .HasMaxLength(30);

                    b.Property<int>("PCRType");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("StandardCost");

                    b.Property<int?>("Threshold1");

                    b.Property<int?>("Threshold2");

                    b.Property<int?>("Threshold3");

                    b.Property<int?>("Threshold4");

                    b.Property<int?>("Threshold5");

                    b.Property<int?>("Threshold6");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<int?>("ggExpiry");

                    b.HasKey("ID");

                    b.ToTable("ElitePCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.EmailTemplate", b =>
                {
                    b.Property<string>("EmailTemplateId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(30);

                    b.Property<string>("Body");

                    b.Property<string>("Description")
                        .HasMaxLength(200);

                    b.Property<string>("Subject")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("EmailTemplateId");

                    b.ToTable("EmailTemplates");
                });

            modelBuilder.Entity("WebApp.Entity.ErrorCode", b =>
                {
                    b.Property<string>("ErrorCodeId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50);

                    b.Property<string>("Category")
                        .HasMaxLength(100);

                    b.Property<string>("Description")
                        .HasMaxLength(500);

                    b.Property<string>("ErrorHandling")
                        .HasMaxLength(4000);

                    b.Property<string>("ErrorType")
                        .HasMaxLength(100);

                    b.Property<string>("ErrorUnit")
                        .HasMaxLength(100);

                    b.Property<string>("Message")
                        .HasMaxLength(200);

                    b.HasKey("ErrorCodeId");

                    b.ToTable("ErrorCodes");
                });

            modelBuilder.Entity("WebApp.Entity.Extraction", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("KitSize");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal?>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("Extractions");
                });

            modelBuilder.Entity("WebApp.Entity.Hotel", b =>
                {
                    b.Property<int>("HotelId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .HasMaxLength(200);

                    b.Property<string>("CCAuthorization")
                        .HasMaxLength(250);

                    b.Property<string>("ConfirmationBody");

                    b.Property<string>("ConfirmationSubject")
                        .HasMaxLength(200);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Details")
                        .HasMaxLength(1000);

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("Note")
                        .HasMaxLength(4000);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("HotelId");

                    b.ToTable("Hotels");
                });

            modelBuilder.Entity("WebApp.Entity.InstrumentType", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<decimal>("OfficialPrice");

                    b.Property<string>("ProductPartNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<decimal>("StandardCost");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("Type")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("ID");

                    b.ToTable("InstrumentType");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.Property<int>("QuotationId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CommercialEntityID");

                    b.Property<bool?>("Compensation");

                    b.Property<int?>("ContractDuration");

                    b.Property<int?>("ContractType");

                    b.Property<decimal?>("CurrentBusiness");

                    b.Property<int?>("CurrentBusinessPerYear");

                    b.Property<string>("CustomerAddress")
                        .HasMaxLength(250);

                    b.Property<string>("CustomerName")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<int?>("CustomerType");

                    b.Property<decimal?>("ExtractionEliteMGBKit");

                    b.Property<decimal?>("ExtractionEliteMGBPanel");

                    b.Property<decimal?>("ExtractionOpenKit");

                    b.Property<decimal?>("InstrumentDiscount");

                    b.Property<int?>("InstrumentNumber");

                    b.Property<decimal?>("InstrumentSellingPrice");

                    b.Property<int?>("InstrumentTypeID");

                    b.Property<decimal?>("LISConnection");

                    b.Property<bool?>("NewCustomer");

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<decimal?>("PercService");

                    b.Property<string>("ReferencePerson")
                        .HasMaxLength(100);

                    b.Property<int?>("SaleType");

                    b.Property<int>("Status");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<decimal?>("TotalAnnualRent");

                    b.Property<decimal?>("TotalAnnualService");

                    b.Property<decimal?>("TotalMonthlyRent");

                    b.Property<decimal?>("TotalMonthlyService");

                    b.Property<decimal?>("TotalOtherCosts");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("UserOwner")
                        .HasMaxLength(256);

                    b.Property<int?>("YearRentalDuration");

                    b.HasKey("QuotationId");

                    b.HasIndex("CommercialEntityID");

                    b.HasIndex("InstrumentTypeID");

                    b.ToTable("Quotations");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationElitePCRKit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("Discount");

                    b.Property<int>("ElitePCRKitId");

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<int?>("TestPerYear");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("ElitePCRKitId");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationElitePCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationExtraction", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("Discount");

                    b.Property<int>("ExtractionId");

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("ExtractionId");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationExtractions");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationOpenPCRKit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("AnalysisType");

                    b.Property<int?>("Control");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<decimal?>("Discount");

                    b.Property<int?>("ExtractionType");

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<decimal?>("KitCosts");

                    b.Property<int?>("KitSize");

                    b.Property<int?>("MasterMix");

                    b.Property<decimal?>("OfficialPrice");

                    b.Property<int>("OpenPCRKitId");

                    b.Property<int>("PCRType");

                    b.Property<string>("ProductPartNumber")
                        .HasMaxLength(50);

                    b.Property<int?>("QuotationId");

                    b.Property<decimal?>("SellingPrice");

                    b.Property<decimal?>("StandardCost");

                    b.Property<int?>("TestPerYear");

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserCreate")
                        .HasMaxLength(256);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<bool>("bHidden");

                    b.Property<int?>("ggExpiry");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationOpenPCRKits");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotal", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Category")
                        .HasMaxLength(20);

                    b.Property<decimal?>("Cost");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<decimal?>("Help1");

                    b.Property<decimal?>("Help2");

                    b.Property<decimal?>("Margin");

                    b.Property<decimal?>("Price");

                    b.Property<decimal?>("Profit");

                    b.Property<int?>("QuotationId");

                    b.Property<bool?>("Visibility");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationsTotal");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotalPCR", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Control");

                    b.Property<int>("ControlExpiry");

                    b.Property<string>("Description")
                        .HasMaxLength(100);

                    b.Property<int?>("FreeOfChargeKit");

                    b.Property<int>("KitPerYear");

                    b.Property<int>("KitSize");

                    b.Property<int>("PCRType");

                    b.Property<decimal>("Price");

                    b.Property<int?>("QuotationId");

                    b.Property<int>("Reaction");

                    b.Property<decimal>("StandardCost");

                    b.Property<string>("Target")
                        .HasMaxLength(50);

                    b.Property<int>("TestForYear");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("QuotationId");

                    b.ToTable("QuotationsTotalPCR");
                });

            modelBuilder.Entity("WebApp.Entity.Training", b =>
                {
                    b.Property<int>("TrainingId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Agenda")
                        .HasMaxLength(250);

                    b.Property<int?>("HotelId");

                    b.Property<string>("Location")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Note")
                        .HasMaxLength(4000);

                    b.Property<DateTime>("PeriodFrom");

                    b.Property<DateTime>("PeriodTo");

                    b.Property<bool>("Reserved")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("Trainers")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int>("TrainingTypeId");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("TrainingId");

                    b.HasIndex("HotelId");

                    b.HasIndex("TrainingTypeId");

                    b.ToTable("Trainings");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingJobPosition", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(100);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.ToTable("TrainingJobPositions");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingLocation", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(200);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.ToTable("TrainingLocations");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingLog", b =>
                {
                    b.Property<long>("TrainingLogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("EventId")
                        .HasMaxLength(50);

                    b.Property<string>("Note")
                        .HasMaxLength(4000);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("TrainingId");

                    b.Property<long?>("TrainingRegistrationId");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("TrainingLogId");

                    b.HasIndex("TrainingRegistrationId");

                    b.HasIndex("TrainingId", "EventId");

                    b.ToTable("TrainingLogs");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingRegistration", b =>
                {
                    b.Property<long>("TrainingRegistrationId")
                        .ValueGeneratedOnAdd();

                    b.Property<bool?>("Accomodation")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<DateTime?>("CheckIn");

                    b.Property<DateTime?>("CheckOut");

                    b.Property<string>("CompanyAddress")
                        .HasMaxLength(100);

                    b.Property<string>("CompanyAddressCity")
                        .HasMaxLength(100);

                    b.Property<string>("CompanyAddressNr")
                        .HasMaxLength(10);

                    b.Property<string>("CompanyAddressZipCode")
                        .HasMaxLength(10);

                    b.Property<string>("CompanyName")
                        .HasMaxLength(200);

                    b.Property<string>("CountryId")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("EmailAddress")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<bool?>("Enabled")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(true);

                    b.Property<string>("InvLetterScan")
                        .HasMaxLength(250);

                    b.Property<string>("JobPosition")
                        .HasMaxLength(100);

                    b.Property<string>("MobileNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("PassportScan")
                        .HasMaxLength(250);

                    b.Property<string>("ReservationNumber")
                        .HasMaxLength(50);

                    b.Property<bool?>("SendNews")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<string>("Status")
                        .HasMaxLength(20);

                    b.Property<string>("Surname")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Title")
                        .HasMaxLength(10);

                    b.Property<DateTime?>("TmstCreate");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<int>("TrainingId");

                    b.Property<string>("TypeOfParticipant")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<int>("VISA");

                    b.Property<bool?>("hotelPaid")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValue(false);

                    b.Property<DateTime?>("tmstAnswerRegistration");

                    b.Property<DateTime?>("tmstReservationHotelConfirmed");

                    b.Property<DateTime?>("tmstSendInvitationLetter");

                    b.Property<DateTime?>("tmstSendReservationHotel");

                    b.HasKey("TrainingRegistrationId");

                    b.HasIndex("CountryId");

                    b.HasIndex("TrainingId");

                    b.ToTable("TrainingRegistrations");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingType", b =>
                {
                    b.Property<int>("TrainingTypeId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<bool>("viewTrainingPortfolio");

                    b.HasKey("TrainingTypeId");

                    b.ToTable("TrainingTypes");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingTypeOfParticipant", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(100);

                    b.Property<bool>("Italian");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.ToTable("TrainingTypeOfParticipants");
                });

            modelBuilder.Entity("WebApp.Entity.UploadFile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FileName")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("FileType")
                        .IsRequired()
                        .HasMaxLength(20);

                    b.Property<string>("Note")
                        .HasMaxLength(1000);

                    b.Property<string>("ReferencePeriod")
                        .IsRequired()
                        .HasMaxLength(6);

                    b.Property<string>("RevisionExtra")
                        .HasMaxLength(30);

                    b.Property<string>("SerialNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<string>("Status")
                        .IsRequired()
                        .HasMaxLength(10);

                    b.Property<DateTime?>("TmstDelete");

                    b.Property<DateTime?>("TmstProcess");

                    b.Property<DateTime?>("TmstUpload");

                    b.Property<string>("UserDelete")
                        .HasMaxLength(256);

                    b.Property<string>("UserUpload")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("SerialNumber", "FileType");

                    b.ToTable("UploadFiles");
                });

            modelBuilder.Entity("WebApp.Entity.UserCfg", b =>
                {
                    b.Property<string>("UserID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Area")
                        .HasMaxLength(50);

                    b.Property<string>("CommercialEntity")
                        .HasMaxLength(100);

                    b.Property<string>("Country")
                        .HasMaxLength(250);

                    b.Property<int?>("DepartmentId");

                    b.Property<string>("Region")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("TmstConfirmCfg");

                    b.Property<DateTime?>("TmstLastChangePwd");

                    b.Property<DateTime?>("TmstLastUpd");

                    b.Property<string>("UserApproval1")
                        .HasMaxLength(450);

                    b.Property<string>("UserApproval2")
                        .HasMaxLength(450);

                    b.Property<string>("UserLastUpd")
                        .HasMaxLength(256);

                    b.Property<string>("fullName")
                        .HasMaxLength(100);

                    b.HasKey("UserID");

                    b.ToTable("UserCfg");
                });

            modelBuilder.Entity("WebApp.Entity.V_Training", b =>
                {
                    b.Property<int>("TrainingId");

                    b.Property<string>("Agenda");

                    b.Property<string>("CCAuthorization");

                    b.Property<string>("HotelDesciption");

                    b.Property<int?>("HotelId");

                    b.Property<string>("Location");

                    b.Property<string>("Note");

                    b.Property<DateTime>("PeriodFrom");

                    b.Property<DateTime>("PeriodTo");

                    b.Property<bool>("Reserved");

                    b.Property<string>("Status");

                    b.Property<string>("Title");

                    b.Property<string>("Trainers");

                    b.Property<int>("TrainingTypeId");

                    b.Property<string>("TypeDescription");

                    b.Property<int>("cntConfirmed");

                    b.Property<int>("cntNew");

                    b.Property<int>("cntRefused");

                    b.HasKey("TrainingId");

                    b.ToTable("V_Trainings");
                });

            modelBuilder.Entity("WebApp.Entity.Version", b =>
                {
                    b.Property<string>("Code")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.HasKey("Code");

                    b.ToTable("Versions");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimPressure", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimPressures")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimPressureSN", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimPressuresSN")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimRow", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimRows")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimScript", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimScripts")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimSnapshot", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany()
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimTemperature", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimTemperatures")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.ClaimTrack", b =>
                {
                    b.HasOne("WebApp.Entity.Claim", "Claim")
                        .WithMany("ClaimTracks")
                        .HasForeignKey("ClaimId");
                });

            modelBuilder.Entity("WebApp.Entity.Quotation", b =>
                {
                    b.HasOne("WebApp.Entity.CommercialEntity", "CommercialEntity")
                        .WithMany()
                        .HasForeignKey("CommercialEntityID");

                    b.HasOne("WebApp.Entity.InstrumentType", "InstrumentType")
                        .WithMany()
                        .HasForeignKey("InstrumentTypeID");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationElitePCRKit", b =>
                {
                    b.HasOne("WebApp.Entity.ElitePCRKit", "ElitePCRKit")
                        .WithMany()
                        .HasForeignKey("ElitePCRKitId");

                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationElitePCRKits")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationExtraction", b =>
                {
                    b.HasOne("WebApp.Entity.Extraction", "Extraction")
                        .WithMany()
                        .HasForeignKey("ExtractionId");

                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationExtractions")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationOpenPCRKit", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany("QuotationOpenPCRKits")
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotal", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany()
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.QuotationsTotalPCR", b =>
                {
                    b.HasOne("WebApp.Entity.Quotation", "Quotation")
                        .WithMany()
                        .HasForeignKey("QuotationId");
                });

            modelBuilder.Entity("WebApp.Entity.Training", b =>
                {
                    b.HasOne("WebApp.Entity.Hotel", "Hotel")
                        .WithMany()
                        .HasForeignKey("HotelId");

                    b.HasOne("WebApp.Entity.TrainingType", "TrainingType")
                        .WithMany()
                        .HasForeignKey("TrainingTypeId");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingLog", b =>
                {
                    b.HasOne("WebApp.Entity.Training", "Training")
                        .WithMany()
                        .HasForeignKey("TrainingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("WebApp.Entity.TrainingRegistration", "TrainingRegistration")
                        .WithMany()
                        .HasForeignKey("TrainingRegistrationId");
                });

            modelBuilder.Entity("WebApp.Entity.TrainingRegistration", b =>
                {
                    b.HasOne("WebApp.Entity.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("WebApp.Entity.Training", "Training")
                        .WithMany()
                        .HasForeignKey("TrainingId");
                });
        }
    }
}

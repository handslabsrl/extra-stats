﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Data;

namespace WebApp.Migrations
{
    [DbContext(typeof(WebAppDbContext))]
    [Migration("20180331134237_INIT_DB")]
    partial class INIT_DB
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApp.Entity.DataMain", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime?>("AbortTime");

                    b.Property<DateTime?>("ApprovalDateTime");

                    b.Property<int?>("ApprovalStatus");

                    b.Property<string>("ApprovalUserName");

                    b.Property<int?>("ApprovalUserRole");

                    b.Property<string>("AssayDetail");

                    b.Property<string>("AssayDetailForCopiesPerMl");

                    b.Property<string>("AssayDetailForGeqPerMl");

                    b.Property<string>("AssayDetailForIuPerMl");

                    b.Property<string>("AssayDetailShortening");

                    b.Property<string>("AssayDetailShorteningForCopiesPerMl");

                    b.Property<string>("AssayDetailShorteningForGeqPerMl");

                    b.Property<string>("AssayDetailShorteningForIuPerMl");

                    b.Property<string>("AssayDetailShorteningUnit");

                    b.Property<string>("AssayName");

                    b.Property<DateTime?>("CalibratorExpiryDate");

                    b.Property<string>("CalibratorLot");

                    b.Property<DateTime?>("ControlExpiryDate");

                    b.Property<string>("ControlLot");

                    b.Property<int?>("DilutionFactor");

                    b.Property<DateTime?>("EndRunTime");

                    b.Property<int?>("ExtractedEluteVolume");

                    b.Property<DateTime?>("ExtractionCassetteExpiryDate");

                    b.Property<string>("ExtractionCassetteLot");

                    b.Property<string>("ExtractionCassetteSerialNumber");

                    b.Property<int?>("ExtractionInputVolume");

                    b.Property<int?>("GeneralAssayID");

                    b.Property<string>("ICLot");

                    b.Property<DateTime?>("IcExpiryDate");

                    b.Property<int?>("IcTubeSize");

                    b.Property<string>("InventoryBlockBarcode");

                    b.Property<string>("InventoryBlockConfirmationMethod");

                    b.Property<string>("InventoryBlockName");

                    b.Property<int?>("InventoryBlockNumber");

                    b.Property<int?>("LISStatusv");

                    b.Property<int?>("LISUploadSelected");

                    b.Property<int?>("MeasuredCalibrationResultID");

                    b.Property<int?>("MeasuredControlResultID");

                    b.Property<int?>("Melt");

                    b.Property<string>("PCRReactionCassetteSerialNumber");

                    b.Property<int>("PositionToPlaceSample");

                    b.Property<int>("Process");

                    b.Property<DateTime?>("ReagentExpiryDate");

                    b.Property<string>("ReagentLot");

                    b.Property<int?>("ReagentTubeSize");

                    b.Property<int?>("RefEluteTrack");

                    b.Property<string>("RunName");

                    b.Property<int?>("RunSetupID");

                    b.Property<byte[]>("SampleID");

                    b.Property<string>("SampleMatrixName");

                    b.Property<int?>("SampleTypes");

                    b.Property<string>("SerialNumber")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<int>("Sonication");

                    b.Property<int?>("SonicationCycle");

                    b.Property<int?>("SonicationOffTime");

                    b.Property<int?>("SonicationOnTime");

                    b.Property<DateTime?>("StartRunTime");

                    b.Property<int?>("TrackNumber");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.HasIndex("SerialNumber");

                    b.ToTable("DataMain");
                });

            modelBuilder.Entity("WebApp.Entity.GAssayPrograms", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AssayName");

                    b.Property<string>("AssayParameterToolVersion");

                    b.Property<double>("ConversionFactorIU");

                    b.Property<int>("CtsAndTmOnly");

                    b.Property<int>("EndRampTemperature");

                    b.Property<int>("ExtractedEluteVolume");

                    b.Property<string>("ExtractionCassetteID");

                    b.Property<int>("ExtractionInputVolume");

                    b.Property<int>("IVDCleared");

                    b.Property<int>("LowerTolerance");

                    b.Property<int>("MeltRequired");

                    b.Property<int>("MeltingOptional");

                    b.Property<int>("OffSet");

                    b.Property<int>("PCRAmplificationCycles");

                    b.Property<int>("PCRAmplificationSteps");

                    b.Property<string>("PCRCassetteID");

                    b.Property<int>("PCRInputEluateVolume");

                    b.Property<string>("Pathogen_TargetName");

                    b.Property<int>("PreCycleStepNumber");

                    b.Property<double>("RampRate");

                    b.Property<DateTime>("RegisteredDate");

                    b.Property<int>("ResultsScaling");

                    b.Property<int?>("SampleMatrix_Id");

                    b.Property<int>("SampleTypes");

                    b.Property<string>("Serial");

                    b.Property<int>("SonicationCycle");

                    b.Property<int>("SonicationOffTime");

                    b.Property<int>("SonicationOnTime");

                    b.Property<int>("StartRampTemperature");

                    b.Property<int>("UpperTolerance");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("GAssayPrograms");
                });

            modelBuilder.Entity("WebApp.Entity.SGAT", b =>
                {
                    b.Property<string>("SerialNumber")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(50);

                    b.Property<string>("Commercial_Status")
                        .HasMaxLength(500);

                    b.Property<string>("CompanyName")
                        .HasMaxLength(500);

                    b.Property<string>("CustomerCode")
                        .HasMaxLength(10);

                    b.Property<string>("Finance_Status")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("Installation_Date");

                    b.Property<string>("Site_Address")
                        .HasMaxLength(500);

                    b.Property<string>("Site_City")
                        .HasMaxLength(500);

                    b.Property<string>("Site_Code")
                        .HasMaxLength(10);

                    b.Property<string>("Site_Country")
                        .HasMaxLength(500);

                    b.Property<string>("Site_Description")
                        .HasMaxLength(500);

                    b.HasKey("SerialNumber");

                    b.ToTable("SGAT");
                });

            modelBuilder.Entity("WebApp.Entity.DataMain", b =>
                {
                    b.HasOne("WebApp.Entity.SGAT", "SGAT")
                        .WithMany()
                        .HasForeignKey("SerialNumber")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}

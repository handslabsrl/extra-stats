﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class QTotals001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "QuotationsTotalPCR",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "FreeOfChargeKit",
                table: "QuotationsTotalPCR",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "QuotationsTotalPCR");

            migrationBuilder.DropColumn(
                name: "FreeOfChargeKit",
                table: "QuotationsTotalPCR");
        }
    }
}

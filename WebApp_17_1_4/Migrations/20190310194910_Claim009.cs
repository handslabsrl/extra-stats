﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Claim009 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Tipo",
                table: "ClaimPressures",
                newName: "CtrlType");

            migrationBuilder.RenameColumn(
                name: "Scostamento",
                table: "ClaimPressures",
                newName: "OffSet");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "OffSet",
                table: "ClaimPressures",
                newName: "Scostamento");

            migrationBuilder.RenameColumn(
                name: "CtrlType",
                table: "ClaimPressures",
                newName: "Tipo");
        }
    }
}

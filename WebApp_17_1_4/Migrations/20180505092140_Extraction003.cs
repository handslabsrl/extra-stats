﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Extraction003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ExtractionEliteMGBKit",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtractionEliteMGBPanel",
                table: "Quotations",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtractionOpenKit",
                table: "Quotations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExtractionEliteMGBKit",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "ExtractionEliteMGBPanel",
                table: "Quotations");

            migrationBuilder.DropColumn(
                name: "ExtractionOpenKit",
                table: "Quotations");
        }
    }
}

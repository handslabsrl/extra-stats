﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Training_003 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "Accomodation",
                table: "TrainingRegistrations",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "TrainingRegistrations",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "tmstReservationHotelConfirmed",
                table: "TrainingRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "tmstSendInvitationLetter",
                table: "TrainingRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "tmstSendReservationHotel",
                table: "TrainingRegistrations",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HotelId",
                table: "Trainings",
                nullable: true,
                oldClrType: typeof(int));


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "Status",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "tmstReservationHotelConfirmed",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "tmstSendInvitationLetter",
                table: "TrainingRegistrations");

            migrationBuilder.DropColumn(
                name: "tmstSendReservationHotel",
                table: "TrainingRegistrations");

            migrationBuilder.AlterColumn<bool>(
                name: "Accomodation",
                table: "TrainingRegistrations",
                nullable: true,
                oldClrType: typeof(bool),
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<int>(
                name: "HotelId",
                table: "Trainings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}

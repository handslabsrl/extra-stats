﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WebApp.Migrations
{
    public partial class Training_012 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TrainingLogs_TrainingId",
                table: "TrainingLogs");

            migrationBuilder.AddColumn<string>(
                name: "EventId",
                table: "TrainingLogs",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TrainingLogs_TrainingId_EventId",
                table: "TrainingLogs",
                columns: new[] { "TrainingId", "EventId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TrainingLogs_TrainingId_EventId",
                table: "TrainingLogs");

            migrationBuilder.DropColumn(
                name: "EventId",
                table: "TrainingLogs");

            migrationBuilder.CreateIndex(
                name: "IX_TrainingLogs_TrainingId",
                table: "TrainingLogs",
                column: "TrainingId");
        }
    }
}

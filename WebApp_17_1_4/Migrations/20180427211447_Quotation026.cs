﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Quotation026 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationElitePCRKits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    QuotationId = table.Column<int>(nullable: true),
                    ElitePCRKitId = table.Column<int>(nullable: false),
                    TestPerYear = table.Column<int>(nullable: true),
                    SellingPrice = table.Column<decimal>(nullable: true),
                    Discount = table.Column<decimal>(nullable: true),
                    FreeOfChargeKit = table.Column<int>(nullable: true),
                    TmstCreate = table.Column<DateTime>(nullable: true),
                    UserCreate = table.Column<string>(maxLength: 256, nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationElitePCRKits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationElitePCRKits_ElitePCRKits_ElitePCRKitId",
                        column: x => x.ElitePCRKitId,
                        principalTable: "ElitePCRKits",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QuotationElitePCRKits_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationElitePCRKits_ElitePCRKitId",
                table: "QuotationElitePCRKits",
                column: "ElitePCRKitId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationElitePCRKits_QuotationId",
                table: "QuotationElitePCRKits",
                column: "QuotationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationElitePCRKits");
        }
    }
}

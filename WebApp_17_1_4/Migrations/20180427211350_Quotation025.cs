﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class Quotation025 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "QuotationElitePCRKit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "QuotationElitePCRKit",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Discount = table.Column<decimal>(nullable: true),
                    ElitePCRKitId = table.Column<int>(nullable: false),
                    FreeOfChargeKit = table.Column<int>(nullable: true),
                    QuotationId = table.Column<int>(nullable: true),
                    SellingPrice = table.Column<decimal>(nullable: true),
                    TestPerYear = table.Column<int>(nullable: true),
                    TmstCreate = table.Column<DateTime>(nullable: true),
                    TmstLastUpd = table.Column<DateTime>(nullable: true),
                    UserCreate = table.Column<string>(maxLength: 256, nullable: true),
                    UserLastUpd = table.Column<string>(maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QuotationElitePCRKit", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QuotationElitePCRKit_ElitePCRKits_ElitePCRKitId",
                        column: x => x.ElitePCRKitId,
                        principalTable: "ElitePCRKits",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QuotationElitePCRKit_Quotations_QuotationId",
                        column: x => x.QuotationId,
                        principalTable: "Quotations",
                        principalColumn: "QuotationId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_QuotationElitePCRKit_ElitePCRKitId",
                table: "QuotationElitePCRKit",
                column: "ElitePCRKitId");

            migrationBuilder.CreateIndex(
                name: "IX_QuotationElitePCRKit_QuotationId",
                table: "QuotationElitePCRKit",
                column: "QuotationId");
        }
    }
}

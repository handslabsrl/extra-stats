﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApp.Migrations
{
    public partial class InstallBase001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UploadFiles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: false),
                    UserUpload = table.Column<string>(maxLength: 256, nullable: true),
                    TmstUpload = table.Column<DateTime>(nullable: true),
                    FileName = table.Column<string>(maxLength: 256, nullable: false),
                    FileType = table.Column<string>(maxLength: 20, nullable: false),
                    ReferencePeriod = table.Column<string>(maxLength: 6, nullable: false),
                    Status = table.Column<string>(maxLength: 10, nullable: false),
                    TmstProcess = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(maxLength: 1000, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadFiles", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UploadFiles_SerialNumber_FileType",
                table: "UploadFiles",
                columns: new[] { "SerialNumber", "FileType" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UploadFiles");
        }
    }
}

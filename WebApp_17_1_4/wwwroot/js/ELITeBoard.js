﻿/**
 * ELITeBoard
 *
 */

// bootbox.alert(JSON.stringify(circularReference, getCircularReplacer()));
// {"otherData":123}
const getCircularReplacer = () => {
    const seen = new WeakSet;
    return (key, value) => {
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    };
};

// Aggiornamento parametri QueryString
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}


//// Es. alert(JSON.stringify(getCircularReplacer(rowData)));
//const getCircularReplacer = () => {
//    const seen = new WeakSet;
//    return (key, value) => {
//        if (typeof value === "object" && value !== null) {
//            if (seen.has(value)) {
//                return;
//            }
//            seen.add(value);
//        }
//        return value;
//    };
//};

// Funzione Applicabile a COLUMN di DATAGRID (DEVEXPRESS)
function showCurrency(data) {
    if ((data.value == 0  || (data.value || "") != "")) {
        return "\u20ac " + Number((data.value)).formatMoney(2, ',', '.');
    } else {
        return "";
    }
}

// Funzione Applicabile a COLUMN di DATAGRID (DEVEXPRESS)
function showPercent(data) {
        if ((data.value == 0 || (data.value || "") != "")) {
            return Number((data.value)).toFixed(0) + " %";
        } else {
            return "";
        }
    };


// funzione applicabile ad uno specifico valore
function formatCurrency(valore) {
    if ((valore == 0 || (valore || "") != "")) {
        return "\u20ac " + Number((valore)).formatMoney(2, ',', '.');
    } else {
        return "";
    }
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

// ***************************************************************
// INIT CAMPO BODY con SUMMERNOTE !!
// ***************************************************************
function initEmailBodyField(fieldName, height, opt) {
    var myToolbar = [['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['keywords', ['myButton']]];
    if (opt !== null) {
        if (opt === "NOKEYWORDS") {
            myToolbar = [['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontname', ['fontname']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']]];
        }
    }
   
    $('#' + fieldName).summernote({
        height: (height || 300),
        toolbar: myToolbar,
        buttons: {
            myButton: myButtonFun
        }
    });
}

var myButtonFun = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below:",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Training Title',
                        value: '$TITLE$'
                    },
                    {
                        text: 'Training Location',
                        value: '$LOCATION$'
                    },
                    {
                        text: 'Training Period From/To',
                        value: '$PERIOD$'
                    },
                    {
                        text: 'Training Starting Date',
                        value: '$FROM$'
                    },
                    {
                        text: 'Training Ending Date',
                        value: '$TO$'
                    },
                    {
                        text: 'Hotel Name',
                        value: '$HOTEL$'
                    },
                    {
                        text: 'Hotel Address',
                        value: '$HOTELADDRESS$'
                    },
                    {
                        text: 'Hotel Details',
                        value: '$HOTELDETAILS$'
                    },
                    {
                        text: 'Reservation Number',
                        value: '$HOTELRESERVATIONNUMBER$'
                    },
                    {
                        text: 'Participant Name',
                        value: '$NAME$'
                    },
                    {
                        text: 'Accomodation Check In',
                        value: '$CHECKIN$'
                    },
                    {
                        text: 'Accomodation Check Out',
                        value: '$CHECKOUT$'
                    },
                    {
                        text: 'Registration Info',
                        value: '$REGISTRATION$'
                    },
                    {
                        text: 'Hotel Paid by EGSPA (section)',
                        value: '$PAID_EGSPA_START$ insert you text here $PAID_EGSPA_END$'
                    },
                    {
                        text: 'Hotel Paid by Participant (section)',
                        value: '$PAID_PARTICIPANT_START$ insert you text here $PAID_PARTICIPANT_END$'
                    },
                    {
                        text: 'I need the VISA (section)',
                        value: '$VISA_START$ insert you text here $VISA_END$'
                    },
                    {
                        text: 'I do not need the VISA (section)',
                        value: '$NO_VISA_START$ insert you text here $NO_VISA_END$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });

        }
    });

    return button.render();   // return button as jquery object
}
// ***************************************************************

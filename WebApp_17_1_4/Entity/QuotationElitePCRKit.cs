﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class QuotationElitePCRKit
    {

        public int Id { get; set; }

        [JsonIgnore]
        // Il campo QuotationId viene aggiunto in automatico !!!! 
        public virtual Quotation Quotation { get; set; }

        [Display(Name = "ELITe PCR Kit")]
        public int ElitePCRKitId { get; set; }
        [ForeignKey("ElitePCRKitId")]
        public virtual ElitePCRKit ElitePCRKit { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Test per Year")]
        public int? TestPerYear { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Selling Price")]
        public decimal? SellingPrice { get; set; }

        //[Range(0, 100)]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Discount")]
        public decimal? Discount { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Free of Charge Kit")]
        public int? FreeOfChargeKit { get; set; }

        [StringLength(256)]
        public string UserCreate { get; set; }
        public DateTime? TmstCreate{ get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

    }
}




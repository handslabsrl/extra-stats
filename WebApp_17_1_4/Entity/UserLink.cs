﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class UserLink
    {
        [Required]
        [StringLength(450)]
        public string UserID { get; set; }
        [Required]
        public Lookup.UserLinkType TypeId { get; set; }
        [Required]
        [StringLength(250)]
        public string LinkID { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

    }
}




﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class QuotationOpenPCRKit
    {

        public int Id { get; set; }

        [JsonIgnore]
        // Il campo QuotationId viene aggiunto in automatico !!!! 
        public virtual Quotation Quotation { get; set; }

        [Display(Name = "OPEN PCR Kit")]
        public int OpenPCRKitId { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Test per Year")]
        public int? TestPerYear { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Selling Price")]
        public decimal? SellingPrice { get; set; }

        //[Range(0, 100)]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Discount")]
        public decimal? Discount { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Free of Charge Kit")]
        public int? FreeOfChargeKit { get; set; }

        // DATI DI LISTINO 
        [StringLength(50)]
        public string ProductPartNumber { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        [Required]
        public Lookup.OpenPCRKitType PCRType { get; set; }
        public Lookup.ExtractionType? ExtractionType { get; set; } // ??? CAMPO NON USATO SU OPEN ???? 
        public Lookup.AnalysisType? AnalysisType { get; set; }

        public Lookup.OpenPCRKitsGGExpiry? ggExpiry { get; set; }
        public int? KitSize { get; set; }
        public Lookup.OpenPCRKitControl? Control { get; set; }

        public decimal? StandardCost { get; set; }
        public decimal? OfficialPrice { get; set; }

        [StringLength(256)]
        public string UserCreate { get; set; }
        public DateTime? TmstCreate{ get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public int? MasterMix { get; set; }

        public bool bHidden { get; set; }

        public decimal? KitCosts { get; set; }

        [NotMapped]
        public string OpenPCRKitDeco
        {
            get { return String.Format("OPEN {0}", this.OpenPCRKitId.ToString("#00").Trim()); }
        }

    }
}




﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
{
    public class QuotationsTotalPCR
    {
        public int Id { get; set; }

        [JsonIgnore]
        // Il campo QuotationId viene aggiunto in automatico !!!! 
        public virtual Quotation Quotation { get; set; }

        public int Type { get; set; }
        public int TestForYear { get; set; }
        public int ControlExpiry { get; set; }
        public decimal Control { get; set; }        // Serve a Walter per fare dei calcoli !!! 
        public int Reaction { get; set; }
        [StringLength(50)]
        public string Target { get; set; }
        public int PCRType { get; set; }
        public int KitSize { get; set; }
        public int KitPerYear { get; set; }
        public decimal StandardCost { get; set; }
        public decimal Price { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public int? FreeOfChargeKit { get; set; }
    }
}




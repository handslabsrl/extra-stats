﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApp.Classes;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Entity
{
    public class TrainingLog
    {
        public long TrainingLogId { get; set; }

        [Required]
        public int TrainingId { get; set; }
        public virtual Training Training { get; set; }

        public long? TrainingRegistrationId { get; set; }
        public virtual TrainingRegistration TrainingRegistration { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        [StringLength(50)]
        public string EventId { get; set; }


        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                if (this.TmstLastUpd == null) return "";
                return ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string TmstLastUpdSort
        {
            get
            {
                if (this.TmstLastUpd == null) return "";
                return ((DateTime)this.TmstLastUpd).ToString("yyyyMMddHHmmss");
            }
        }


    }
}




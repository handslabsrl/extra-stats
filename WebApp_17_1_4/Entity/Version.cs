﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class Version
    {
        [Key]
        public string Code { get; set; }
        [Required]
        [StringLength(100)]
        public string Description { get; set; }
    }
}




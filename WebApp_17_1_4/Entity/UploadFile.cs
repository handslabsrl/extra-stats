﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class UploadFile
    {

        public int Id { get; set; }

        [StringLength(50)]
        [Required]
        public string SerialNumber { get; set; }

        [StringLength(256)]
        public string UserUpload { get; set; }
        public DateTime? TmstUpload { get; set; }

        [StringLength(256)]
        [Required]
        public string FileName { get; set; }

        [StringLength(20)]
        [Required]
        /*
         * DATA
         * ASSAYSLIST 
         * INSTALLBASE
         */
        public string FileType { get; set; }

        [StringLength(6)]
        [Required]
        //Formato: YYYYMMDD
        public string ReferencePeriod { get; set; }

        [StringLength(10)]
        [Required]
        /* 
         * WAIT         File Nuovo, da elaborare
         * LOADING      Caricamento in corso 
         * OK           Elaborazione completata con successo
         * ERROR        Elaborazione completata con ERRORE (vedi campi NOTE)
         * RELOAD       File da rielaborare
         * DELETE       Cancellato
         */
        public string Status { get; set; }

        public DateTime? TmstProcess { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        [StringLength(256)]
        public string UserDelete { get; set; }
        public DateTime? TmstDelete { get; set; }

        [StringLength(30)]
        public string RevisionExtra { get; set; }



    }
}




﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class Quotation
    {

        public Quotation()
        {
            Status = Lookup.QStatus.draft;
            NewCustomer = false;
            Compensation = false;
        }

        public int QuotationId { get; set; }

        // SEZIONE DATI CLIENTE
        [Display(Name = "Commercial Entity")]
        public int? CommercialEntityID { get; set; }
        [ForeignKey("CommercialEntityID")]
        public virtual CommercialEntity CommercialEntity { get; set; }

        [Required, StringLength(100), Display(Name = "Account Name")]
        public string CustomerName { get; set; }
        [StringLength(250), Display(Name = "Address")]
        public string CustomerAddress { get; set; }
        [StringLength(100), Display(Name = "Reference Person")]
        public string ReferencePerson { get; set; }
        [Display(Name = "Type of Customer")]
        public Lookup.CustomerType? CustomerType { get; set; }

        [Display(Name = "New Customer")]
        // NULLABLE per evitare il msg di DevExtreme di campo obbligatorio
        public bool? NewCustomer { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Current Business")]
        public decimal? CurrentBusiness { get; set; }

        // SEZIONE CONTRATTO
        [Display(Name = "Type of Sale")]
        public Lookup.SaleType? SaleType { get; set; }
        [Display(Name = "Type of Deal")]
        public Lookup.ContractType? ContractType { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true), Display(Name = "Contract Duration (Year)")]
        public int? ContractDuration { get; set; }

        // SEZIONE STRUMENTO
        [DisplayFormat(DataFormatString = "{N}", ApplyFormatInEditMode = true), Display(Name = "Instrument Type")]
        public int? InstrumentTypeID { get; set; }
        [ForeignKey("InstrumentTypeID")]
        public virtual InstrumentType InstrumentType { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Number of Instrument")]
        public int? InstrumentNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Selling Price")]
        public decimal? InstrumentSellingPrice { get; set; }
        //[Range(0, 100)]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Instrument Discount")]
        public decimal? InstrumentDiscount { get; set; }

        // Other
        [Required]
        public Lookup.QStatus Status { get; set; }

        [Display(Name = "Compensation")]
        // NULLABLE per evitare il msg di DevExtreme di campo obbligatorio
        public bool? Compensation { get; set; }

        // SEZIONE Instrumental Rental 
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Amortization/Rental Duration [Years]")]
        public int? YearRentalDuration { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Service [% of instrument cost]")]
        public decimal? PercService { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Total other costs (Table, Transport costs, Accessories, etc)")]
        public decimal? TotalOtherCosts { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "LIS Connection")]
        public decimal? LISConnection { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Total Service quote [Annual based]")]
        public decimal? TotalAnnualService { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Total Instr Rent quote [Annual based]")]
        public decimal? TotalAnnualRent { get; set; }

        // ?? DA RIMUOVERE 
        public decimal? TotalMonthlyService { get; set; }
        public decimal? TotalMonthlyRent { get; set; }


        // Sezione EXTRACTION Consumables 
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Extraction ELITe MGB kit [CTRCPE]")]
        public decimal? ExtractionEliteMGBKit { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Extraction ELITe MGB Panel [IC500]")]
        public decimal? ExtractionEliteMGBPanel { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Extraction Third party kit/Extraction only")]
        public decimal? ExtractionOpenKit { get; set; }

        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Current Business Per Year")]
        public int? CurrentBusinessPerYear { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        // ALTRI DATI 
        [StringLength(256)]
        public string UserOwner { get; set; }
        public DateTime? TmstCreate{ get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public virtual ICollection<QuotationElitePCRKit> QuotationElitePCRKits { get; set; }
        public virtual ICollection<QuotationOpenPCRKit> QuotationOpenPCRKits { get; set; }
        public virtual ICollection<QuotationExtraction> QuotationExtractions { get; set; }

        [NotMapped]
        public string customerTypeDeco {
            get { return this.CustomerType.ToString(); }
        }
        [NotMapped]
        public string statusDeco
        {
            get { return this.Status.ToString().ToUpper(); }
        }
        [NotMapped]
        public string SaleTypeDeco
        {
            get { return this.SaleType.ToString().ToUpper().Replace("_", " ") ?? ""; }
        }
        //[NotMapped]
        //public string ContractTypeDeco
        //{
        //    get { return this.ContractType.ToString().ToUpper().Replace("_"," ") ?? ""; }
        //}

        [NotMapped]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Max Singleplex")]
        public decimal maxSingleplex { get; set; }
        [NotMapped]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Max Multiplex")]
        public decimal maxMultiplex { get; set; }
        [NotMapped]
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Max OpenKits")]
        public decimal maxOpenKit { get; set; }

    }
}




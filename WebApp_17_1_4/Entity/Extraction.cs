﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class Extraction
    {
        public int ID { get; set; }

        [Required, StringLength(100)]
        public string Description { get; set; }
        [Required, StringLength(50)]
        [Display(Name = "Ref (product p/n)")]
        public string ProductPartNumber { get; set; }

        public int? KitSize { get; set; }

        public decimal? StandardCost { get; set; }
        public decimal? OfficialPrice { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}




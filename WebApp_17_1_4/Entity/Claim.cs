﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using System.IO;
using WebApp.Repository;

namespace WebApp.Entity
{
   
    public class Claim
    {
        public int ClaimId { get; set; }

        [StringLength(256)]
        public string UserUpload { get; set; }
        public DateTime TmstUpload { get; set; }

        [StringLength(256)]
        public string UserAssigned { get; set; }
        public DateTime? TmstAssign { get; set; }

        [StringLength(500)]
        public string FileName { get; set; }    // Nome file, completo di path relativo in UploadClaim

        /* 
         * NEW          Nuovo Claim, da Elaborare 
         * COMPLETED    Elaborazione completata
         * CLOSED       Claim chiuso
         * ERROR        Caricamento in errore 
         */
        [StringLength(10)]
        [Required]
        public string Status { get; set; }
    
        [StringLength(50)]
        [Display(Name = "Instrument")]
        public string SerialNumber { get; set; }
        public DateTime? RequestedDateFrom { get; set; }
        public DateTime? RequestedDateTo { get; set; }

        [StringLength(30)]
        [Display(Name = "Software Version")]
        public string SoftwareVersion { get; set; }

        public DateTime? TmstProcess { get; set; }
        public DateTime? TmstGetTempMin { get; set; }
        public DateTime? TmstGetTempMax { get; set; }
        public DateTime? TmstGetPressMin { get; set; }
        public DateTime? TmstGetPressMax { get; set; }

        [StringLength(1000)]
        [Display(Name = "Upload Notes")]
        public string NoteUpload { get; set; }
        [StringLength(1000)]
        [Display(Name = "Assign Notes")]
        public string NoteAssign { get; set; }
        [StringLength(1000)]
        [Display(Name = "Close Notes")]
        public string NoteClose { get; set; }
        [StringLength(1000)]
        [Display(Name = "Error")]
        public string NoteError { get; set; }

        [StringLength(4000)]
        [Display(Name = "Claim Notes")]
        public string NoteClaim { get; set; }

        [StringLength(100)]
        [Display(Name = "Ref. Complaint Nr.")]
        public string RefComplaintNr { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public DateTime? TmstCleaning { get; set; }

        public virtual ICollection<ClaimRow> ClaimRows { get; set; }
        public virtual ICollection<ClaimTemperature> ClaimTemperatures { get; set; }
        public virtual ICollection<ClaimPressure> ClaimPressures { get; set; }
        public virtual ICollection<ClaimPressureSN> ClaimPressuresSN { get; set; }
        public virtual ICollection<ClaimTrack> ClaimTracks { get; set; }
        public virtual ICollection<ClaimScript> ClaimScripts { get; set; }

        [NotMapped]
        public string TmstUploadDeco
        {
            get { return this.TmstUpload.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        [NotMapped]
        public string TmstUploadSort
        {
            get
            {
                return this.TmstUpload.ToString("yyyyMMddHHmmss");
            }
        }

        [NotMapped]
        public string RequestedDateDesc
        {
            get {
                string from = "";
                string to = "";
                if (this.RequestedDateFrom != null)
                {
                    from = ((DateTime)this.RequestedDateFrom).ToString("dd/MM/yyyy");
                }
                if (this.RequestedDateTo != null)
                {
                    to = ((DateTime)this.RequestedDateTo).ToString("dd/MM/yyyy");
                }
                if (from.Equals(to))
                {
                    return from;
                }
                return (from + " " + to).Trim();
            }
        }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.Claim_Status_NEW))
                        deco = Lookup.Claim_StatusDeco_NEW;
                    if (this.Status.Equals(Lookup.Claim_Status_COMPLETED))
                        deco = Lookup.Claim_StatusDeco_COMPLETED;
                    if (this.Status.Equals(Lookup.Claim_Status_ERROR))
                        deco = Lookup.Claim_StatusDeco_ERROR;
                    if (this.Status.Equals(Lookup.Claim_Status_CLOSED))
                        deco = Lookup.Claim_StatusDeco_CLOSED;
                }
                return deco;
            }
        }

        [NotMapped]
        [Display(Name = "City")]
        public string InstrumentSiteCity
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.SerialNumber))
                {
                    var data = (new InstrumentsBO()).GetInstrument(SerialNumber: this.SerialNumber);
                    if (data != null)
                    {
                        return data.SiteCity;
                    }

                }
                return "";
            }
        }
        [NotMapped]
        [Display(Name = "Country")]
        public string InstrumentSiteCountry
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.SerialNumber))
                {
                    var data = (new InstrumentsBO()).GetInstrument(SerialNumber: this.SerialNumber);
                    if (data != null)
                    {
                        return data.SiteCountry;
                    }

                }
                return "";
            }
        }
        [NotMapped]
        [Display(Name = "CommercialEntity")]
        public string InstrumentCommercialEntity
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.SerialNumber))
                {
                    var data = (new InstrumentsBO()).GetInstrument(SerialNumber: this.SerialNumber);
                    if (data != null)
                    {
                        return data.CommercialEntity;
                    }

                }
                return "";
            }
        }


    }

    public class ClaimRow
    {
        public long ClaimRowId { get; set; }
        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }
        [StringLength(50)]
        public string ErrorCode { get; set; }
        [MaxLength]
        public string Note { get; set; }
        public DateTime? TmstGet { get; set; }

        [StringLength(1000)]
        [Display(Name = "Procedure Note")]
        public string ProcedureNote { get; set; }
        [StringLength(1)]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [StringLength(50)]
        public string Script { get; set; }
        public int? SessionNumber { get; set; }     // Seduta
        [StringLength(50)]
        public string Folder { get; set; }

        public bool OutOfSession { get; set; }
    }

    public class ClaimTemperature
    {
        public long ClaimTemperatureId { get; set; }

        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }

        [StringLength(1)]
        public string RecType { get; set; }      // 'T' temperature - 'C' Cool Block Ext

        public DateTime TmstGet { get; set; }

        public int ValueGet { get; set; }   

        public int SessionNumber { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGetAvg { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet01 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet02 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet03 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet04 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet05 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet06 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet07 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet08 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet09 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet10 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet11 { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true)]
        public decimal ValueGet12 { get; set; }
        public int ValueLeft { get; set; }
        public int ValueRight { get; set; }
        public int ValueCenter { get; set; }

        [NotMapped]
        public string DateGetDeco
        {
            get { return this.TmstGet.ToString("dd/MM"); }
        }

        [NotMapped]
        public string TmstGetDeco
        {
            get { return this.TmstGet.ToString("dd/MM HH:mm:ss.fff"); }
        }
        [NotMapped]
        public int idxJs { get; set; }

    }

    public class ClaimPressure
    {
        public long ClaimPressureId { get; set; }

        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }

        [StringLength(50)]
        public string Script { get; set; }
        public int SessionNumber { get; set; }

        public DateTime TmstGet { get; set; }
        public int ValueGet01 { get; set; }
        public int ValueGet02 { get; set; }
        public int ValueGet03 { get; set; }
        public int ValueGet04 { get; set; }
        public int ValueGet05 { get; set; }
        public int ValueGet06 { get; set; }
        public int ValueGet07 { get; set; }
        public int ValueGet08 { get; set; }
        public int ValueGet09 { get; set; }
        public int ValueGet10 { get; set; }
        public int ValueGet11 { get; set; }
        public int ValueGet12 { get; set; }
        public int ValueGet13 { get; set; }     // controllo

        [StringLength(1)]
        public string CtrlType { get; set; }        // x utilizzi batch: I Init, '' default, 'C' current

        public bool OffSet { get; set; }

        [StringLength(20)]
        public string RecType { get; set; }      // init, aspiration, dispense, tipon, tipoff, leak , leak2, leak3

        [NotMapped]
        public int? idxJs { get; set; }  // Indice Array JS

        [NotMapped]
        public string DateGetDeco
        {
            get { return this.TmstGet.ToString("dd/MM"); }
        }

        [NotMapped]
        public string TmstGetDeco {
            get { return this.TmstGet.ToString("dd/MM HH:mm:ss.fff");  }
        }
        [NotMapped]
        public string TmstGetDecoTime
        {
            get { return this.TmstGet.ToString("HH:mm:ss.fff"); }
        }

    }

    public class ClaimTrack
    {
        public long ClaimTrackId { get; set; }

        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }


    }

    public class ClaimSnapshot
    {
        public long ClaimSnapshotId { get; set; }

        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }
        [StringLength(1)]
        public string tipo { get; set; }
        [StringLength(500)]
        public string FileName { get; set; }    // Nome file, completo di path relativo 

        [StringLength(1000)]
        public string Note { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        //[NotMapped]
        //public string picture { get; set; }
        //public byte[] picture { get; set; }


        [NotMapped]
        public string snapshot
        {
            get
            {
                string fileName = this.FileName;
                string fullFile = System.IO.Path.Combine(Directory.GetCurrentDirectory(), this.FileName);
                if (System.IO.File.Exists(fullFile))
                {
                    byte[] imageByteData = System.IO.File.ReadAllBytes(fullFile);
                    string imageBase64Data = Convert.ToBase64String(imageByteData);
                    if (System.IO.Path.GetExtension(fullFile).ToLower().Equals(".svg"))
                    {
                        return string.Format("data:image/svg+xml;base64,{0}", imageBase64Data);
                    } else {
                        return string.Format("data:image/octet-stream;base64,{0}", imageBase64Data);
                    }
                }
                return "";
            }
        }

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        [NotMapped]
        public string noteNoCRLF
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this.Note)) { return ""; } 
                return this.Note.Replace("\r", string.Empty).Replace("\n", string.Empty);
            }
        }

    }

    // ClaimRow con errori 
    public class ClaimRowError
    {
        public string ErrorCode { get; set; }
        public string Note { get; set; }
        public string Message { get; set; }
        public string Category { get; set; }
        public string ErrorType { get; set; }
        public string ErrorUnit { get; set; }
        public string Description { get; set; }
        public string ErrorHandling { get; set; }
        public string Status { get; set; }
        public string ProcedureNote { get; set; }
        public DateTime? TmstGet { get; set; }
        public string Script { get; set; }
        public int? SessionNumber { get; set; }     // Seduta
        public bool OutOfSession { get; set; }      // msg fuori sessione

        [NotMapped]
        public string TmstGetDeco
        {
            get
            {
                string ret = "";
                if (this.TmstGet != null)
                {
                    ret = ((DateTime)this.TmstGet).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }

        [NotMapped]
        public string group1
        {
            get {
                return string.Format("Category: {0} - Error Number: {1} - Error Unit: {2}", 
                                    (this.Category??"?"), (this.ErrorCode??"?"), (this.ErrorUnit??"?"));
            }
        }

        //[NotMapped]
        //public string procedureNoteCheck
        //{
        //    get
        //    {
        //        return string.IsNullOrWhiteSpace(this.ProcedureNote) ? "No" : "Yes";
        //    }
        //}

        [NotMapped]
        public string SessionNumberInfo
        {
            get
            {
                string ret = string.Format("{0}", this.SessionNumber);
                if (this.OutOfSession) {
                    ret = string.Format("{0} (out)", ret);
                }
                return ret;
            }
        }
    }

    public class ClaimScript
    {
        public long ClaimScriptId { get; set; }
        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }
        [StringLength(50)]
        public string Script { get; set; }

        public DateTime TmstGet { get; set; }

        public int SessionNumber { get; set; }
        [StringLength(50)]
        public string Folder { get; set; }
        [StringLength(1)]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        [NotMapped]
        public string TmstGetDeco
        {
            get { return this.TmstGet.ToString("dd/MM HH:mm:ss.fff"); }
        }
    }


    // questa entità serve solo per controllare le estrazioni, non dipenda da CLAIM !!! 
    public class ClaimPressureRecType
    {
        [Key]
        [StringLength(20)]
        public string RecType { get; set; }
        public int eluateOrder  { get; set; }   // 0 per NON estrarre il tipo record; >0 per impostare l'ordine 
        public int mmixOrder { get; set; }      // 0 per NON estrarre il tipo record; >0 per impostare l'ordine 
    }

    // Pressioni per script Single Nozzle (ICDISP.scr, PCRReagentDisp.scr, SampleDisp.scr, DNASampleDisp.scr)
    public class ClaimPressureSN
    {
        public long ClaimPressureSNId { get; set; }

        [JsonIgnore]
        // Il campo ClaimId viene aggiunto in automatico !!!! 
        public virtual Claim Claim { get; set; }

        [StringLength(50)]
        public string Script { get; set; }
        public int SessionNumber { get; set; }     // Seduta

        public DateTime TmstRif { get; set; }
        public int TrackIdx { get; set; }
        public int TrackSeq { get; set; }
        public int SubTrack { get; set; }

        public int? ValueInit { get; set; }
        public int? ValueTipOn { get; set; }
        public int? ValueAsp { get; set; }
        public int? ValueDisp { get; set; }
        public int? ValueTipOff { get; set; }
        public int? ValueLeak1 { get; set; }
        public int? ValueLeak2 { get; set; }
        public int? ValueLeak3 { get; set; }
        public int? ValueClotAsp { get; set; }
        public int? ValueClotDisp { get; set; }

        public int? ValueLiquidVolume { get; set; }

        [StringLength(20)]
        public string labelVolume { get; set; }

        [NotMapped]
        public string DateRifDeco
        {
            get { return this.TmstRif.ToString("dd/MM"); }
        }

        [NotMapped]
        public string TmstRifDeco
        {
            get { return this.TmstRif.ToString("dd/MM HH:mm:ss.fff"); }
        }
    }
    
    // questa entità serve solo per configurare le estrazioni in base allo script e al tipo di check 
    public class ClaimCheckCfg
    {
        [StringLength(50)]
        public string Script { get; set; }
        [StringLength(10)]
        public string CheckName { get; set; }
        public int CheckSeq { get; set; }       // sequenza nell'ambito della stessa traccia/tipo
        public int CheckType { get; set; }      // tipo CFG: 0 INit, 1 Pre mix/Seq, 2 Track/Seq, 3 Post mix/Seq
    }

    public class ClaimPressureSNDTO
    {

        public int PointIdx { get; set; }
        public int CheckType { get; set; }       
        public string checkName { get; set; }
        public int CheckSeq { get; set; }

        public long ClaimPressureSNId { get; set; }
        public int TrackIdx { get; set; }
        public int TrackSeq { get; set; }
        public int SubTrack { get; set; }

        public int? init { get; set; }
        public int? tipOn { get; set; }
        public int? asp { get; set; }
        public int? disp { get; set; }
        public int? tipOff { get; set; }
        public int? leak1 { get; set; }
        public int? leak2 { get; set; }
        public int? leak3 { get; set; }
        public int? leak { get; set; }
        public int? clotAsp { get; set; }
        public int? clotDisp { get; set; }

        public int? pressure { get; set; }

        public int? volume { get; set; }

        public int? idxJs { get; set; }  // Indice Array JS
        public string track { get; set; }
    }

    public class ClaimVolumeDTO
    {
        public int TrackIdx { get; set; }
        public int? volume { get; set; }
        public string track { get; set; }
        public int? idxJs { get; set; }  // Indice Array JS
        public string tipoVolume { get; set; }
        public string labelVolume { get; set; }
    }

    public class ClaimDateRifDTO
    {
        public DateTime start { get; set; }
        public string content  { get; set; }
        //public int id { get; set; }
        public string className { get; set; }
    }


}




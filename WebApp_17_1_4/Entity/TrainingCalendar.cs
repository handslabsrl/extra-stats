﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace WebApp.Entity
{
    public class TrainingCalendar
    {
        private readonly string DirAttachmentsCalendar = "Attachments\\Calendar";

        [NotMapped]
        public string CalendarFileName
        {
            get
            {
                // leggo il primo file nella cartella 
                if (Directory.Exists(this.CalendarPathName)) {
                    var allFilenames = Directory.EnumerateFiles(this.CalendarPathName, "*.pdf", SearchOption.TopDirectoryOnly);
                    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                    if (candidates.Count() > 0) {
                        return Path.GetFileName(candidates.First());
                    }
                }
                return null;
            }
        }

        [NotMapped]
        public bool CalendarExist
        {
            get
            {
                string fullFileName = this.CalendarFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.CalendarFullFileName);
            }
        }

        [NotMapped]
        public string CalendarPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirAttachmentsCalendar);
            }
        }
        [NotMapped]
        public string CalendarFullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.CalendarPathName, this.CalendarFileName);
            }
        }

    }
}


﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;

namespace WebApp.Entity
{
    [Table("V_Trainings")]
    public class V_Training 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int TrainingId { get; set; }
        public int TrainingTypeId { get; set; }
        public string TypeDescription { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string Agenda { get; set; }
        public string Note { get; set; }
        public int? HotelId { get; set; }
        public string HotelDesciption { get; set; }
        public string CCAuthorization { get; set; }    
        public string Status { get; set; }
        public bool Reserved { get; set; }
        public string Trainers { get; set; }
        public int cntConfirmed { get; set; }
        public int cntNew { get; set; }
        public int cntRefused { get; set; }

        [NotMapped]
        public string PeriodFromDeco
        {
            get { return this.PeriodFrom.ToString("dd/MM/yyyy"); }
        }

        [NotMapped]
        public string PeriodFromSort
        {
            get
            {
                return this.PeriodFrom.ToString("yyyyMMdd");
            }
        }

        [NotMapped]
        public string Year
        {
            get { return this.PeriodFrom.ToString("yyyy"); }
        }

        [NotMapped]
        public string PeriodToDeco
        {
            get { return this.PeriodTo.ToString("dd/MM/yyyy"); }
        }

        [NotMapped]
        public string PeriodToSort
        {
            get
            {
                return this.PeriodTo.ToString("yyyyMMdd");
            }
        }

        [NotMapped]
        public string ReservedDesc
        {
            get { return (this.Reserved ? "Yes" : "No"); }
        }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.Training_Status_CLOSED))
                        deco = "Closed for registration";
                }
                return deco;
            }
        }
    }
}



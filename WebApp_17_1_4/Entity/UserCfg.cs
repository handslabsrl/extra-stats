﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class UserCfg
    {
        [Key]
        public string UserID { get; set; }

        // Attibuti USER
        [StringLength(250)]
        public string Country { get; set; }                 // --> sostituta la Userlink
        [StringLength(50)]
        public string Region { get; set; }
        [StringLength(50)]
        public string Area { get; set; }
        //public int? CommercialEntityID { get; set; }
        [StringLength(100)]
        public string CommercialEntity { get; set; }            // --> sostituta la Userlink
        [StringLength(450)]
        public string UserApproval1 { get; set; }
        [StringLength(450)]
        public string UserApproval2 { get; set; }
        public DateTime? TmstConfirmCfg { get; set; }
        [StringLength(100)]
        public string fullName { get; set; }
        public int? DepartmentId { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public DateTime? TmstLastChangePwd { get; set; }
    }
}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class Country
    {
        [StringLength(100)]
        [Display(Name = "Country")]
        public string CountryId { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(10)]
        [Display(Name = "Calling Code")]
        public string CallingCode { get; set; }

    }
}



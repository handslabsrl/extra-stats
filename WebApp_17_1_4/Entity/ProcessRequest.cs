﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{

    public class ProcessRequest
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        // vedi Lookup.ProcessRequest_Type_....
        public string ProcessType { get; set; }

        // Parametri in formato JSON {use 
        [StringLength(1000)]
        [Display(Name = "Parameters")]
        public string ProcessParms { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(10)]
        [Required]
        // vedi Lookup.ProcessRequest_Status_....
        public string Status { get; set; }
        public DateTime? TmstExec { get; set; }
        [StringLength(1000)]
        public string NoteExec { get; set; }

        [StringLength(256)]
        public string FileNameDownload { get; set; }  // Nome file per Download 

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }

        [NotMapped]
        private ProcessRequestRnD _paramsRnD { get; set; }

        [NotMapped]
        public ProcessRequestRnD paramsRnD
        {
            get
            {
                if (this._paramsRnD == null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ProcessParms))
                    {
                        try
                        {
                            this._paramsRnD = JsonConvert.DeserializeObject<ProcessRequestRnD>(this.ProcessParms);
                            if (!String.IsNullOrWhiteSpace(this._paramsRnD.Instrument))
                            {
                                this._paramsRnD.InstrumentData = (new InstrumentsBO()).GetInstrument(SerialNumber: this._paramsRnD.Instrument);
                                //if (data != null)
                                //{
                                //    this._paramsRnD.CompanyName = data.CompanyName;
                                //    this._paramsRnD.SiteDescription = data.SiteDescription;
                                //    this._paramsRnD.SiteCity = data.SiteCity;
                                //    this._paramsRnD.SiteCountry = data.SiteCountry;
                                //    this._paramsRnD.CommercialEntity = data.CommercialEntity;
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            string msg = String.Format("[{0}] - {1} / {2}", "infoRnD", e.Message, e.Source);
                        }
                    }
                }

                return this._paramsRnD;
            }
        }
    }

    public class ProcessRequestRnD
    {
        public string Instrument { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public string DateFromDeco
        {
            get { return ((this.DateFrom == null) ? "" : ((DateTime)this.DateFrom).ToString("dd/MM/yyyy")); }
        }
        public string DateFromSort
        {
            get { return ((this.DateFrom == null) ? "" : ((DateTime)this.DateFrom).ToString("yyyyMM")); }
        }
        public string DateToDeco
        {
            get { return ((this.DateTo == null) ? "" : ((DateTime)this.DateTo).ToString("dd/MM/yyyy")); }
        }
        public string DateToSort
        {
            get { return ((this.DateTo == null) ? "" : ((DateTime)this.DateTo).ToString("yyyyMMdd")); }
        }
        public InstrumentModel InstrumentData { get; set; }
    }
}




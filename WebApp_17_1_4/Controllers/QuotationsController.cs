using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Localization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;
using NPoco;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using WebApp.Repository;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Quotation,Quotation Approval,Quotation Manager")]
    public class QuotationsController : Controller
    {

        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IHtmlLocalizer<QuotationsController> _localizer;
        private string connectionString;

        //public QuotationsController(WebAppDbContext context, ILogger<QuotationsController> logger, IStringLocalizer<QuotationsController> localizer)
        public QuotationsController(WebAppDbContext context, ILogger<QuotationsController> logger, IHtmlLocalizer<QuotationsController> localizer)
        {
            _context = context;
            _logger = logger;
            _localizer = localizer;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }

        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            List<string> commercialEntitiesCFG = new List<string>();
            if (User.IsInRole(Lookup.Role_QuotationManager)) {
                string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
                commercialEntitiesCFG = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                //if (!string.IsNullOrWhiteSpace(UserId))
                //{
                //    // NOTA: su userCfg � salvata la descrizione, tutto in maiuscolo (dato non normalizzato)
                //    // mentre su quotation � un dato normalizzato, con tabella specifica !!! 
                //    //UserCfg userCfg = _context.UserCfg.Find(UserId);
                //    //if (userCfg != null)
                //    //{
                //    //    commercialEntity = userCfg.CommercialEntity;
                //    //}
                //}
            }

            var quotations = _context.Quotations
                .Include(q => q.CommercialEntity)
                .Include(q => q.InstrumentType)
                .Where(q => q.UserOwner.Equals(User.Identity.Name) || 
                        (User.IsInRole(Lookup.Role_QuotationManager) && 
                            (commercialEntitiesCFG.Contains(q.CommercialEntity.Description.ToUpper()) || commercialEntitiesCFG.Count == 0)));
            return DataSourceLoader.Load(quotations, loadOptions);
        }

        // GET: Quotations
        public async Task<IActionResult> Index()
        {
            var webAppDbContext = _context.Quotations.Include(q => q.CommercialEntity);
            return View(await webAppDbContext.ToListAsync());
        }

        // GET: Quotations/Export/5
        public async Task<IActionResult> Export(int id)
        {
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("ExportDetails");
            int posData = 0;
            int posStart = 0;
            int nCols = 0;

            bool bQManagerTopLevel = false;
            if (User.IsInRole(Lookup.Role_QuotationManager))
            {
                string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    //UserCfg userCfg = _context.UserCfg.Find(UserId);
                    //if (userCfg != null)
                    //{
                    //    bQManagerTopLevel = string.IsNullOrWhiteSpace(userCfg.CommercialEntity);
                    //}
                    // TopLevel -> Nessuna CommerciaEntity configurata
                    bQManagerTopLevel = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity)).Count() == 0;
                }
            }


            // SUMMARY
            posData++; workSheet.InsertRow(posData, 1);
            workSheet.Cells[posData, 1].Value = "Summary";
            using (var range = workSheet.Cells[posData, 1, posData, 1])
            {
                range.Style.Font.Bold = true;
            }

            posData++; workSheet.InsertRow(posData, 1);
            workSheet.Cells[posData, 1].Value = "Description";
            workSheet.Cells[posData, 2].Value = "Price";
            nCols = 2;
            if (bQManagerTopLevel)
            {
                workSheet.Cells[posData, 3].Value = "Margin";
                workSheet.Cells[posData, 4].Value = "Cost";
                workSheet.Cells[posData, 5].Value = "Profit";
                nCols = 5;
            }
            using (var range = workSheet.Cells[posData, 1, posData, nCols])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                range.Style.Font.Color.SetColor(Color.White);
            }

            List<QuotationsTotal> totals =  (from x in _context.QuotationsTotal
                                                  where x.Quotation.QuotationId == id 
                                                    && (User.IsInRole(Lookup.Role_QuotationManager) || x.Visibility == true)
                                             orderby x.Id
                                                  select x).ToList();
            posStart = posData+1;
            foreach (var total in totals)
            {
                posData++; workSheet.InsertRow(posData, 1);
                workSheet.Cells[posData, 1].Value = total.Description;
                workSheet.Cells[posData, 2].Value = total.Price;
                if (bQManagerTopLevel)
                {
                    workSheet.Cells[posData, 3].Value = total.Margin;
                    workSheet.Cells[posData, 4].Value = total.Cost;
                    workSheet.Cells[posData, 5].Value = total.Profit;
                }
            }
            using (var range = workSheet.Cells[posStart, 2, posData, nCols])
            {
                range.Style.Numberformat.Format = "#,##0.00";
            }

            // 1� ciclo: ELITe PCR; 2� ciclo: OPEN PCR
            for (int myType = 0; myType <= 1; myType ++)
            {
                List<QuotationsTotalPCR> details = (from x in _context.QuotationsTotalPCR
                                                    where x.Quotation.QuotationId == id && x.Type == myType
                                                    orderby x.Id
                                                    select x).ToList();
                if (details.Count > 0)
                {
                    posData++; workSheet.InsertRow(posData, 1);
                    posData++; workSheet.InsertRow(posData, 1);
                    workSheet.Cells[posData, 1].Value = (myType == 0 ? "ELITe PCR" : "OPEN PCR");
                    using (var range = workSheet.Cells[posData, 1, posData, 1])
                    {
                        range.Style.Font.Bold = true;
                    }
                    posData++; workSheet.InsertRow(posData, 1);
                    workSheet.Cells[posData, 1].Value = "PCR Target";
                    workSheet.Cells[posData, 2].Value = "Description";
                    workSheet.Cells[posData, 3].Value = "ControlExpiry";
                    workSheet.Cells[posData, 4].Value = "Reaction";
                    workSheet.Cells[posData, 5].Value = "KitSize";
                    workSheet.Cells[posData, 6].Value = "KitPerYear";
                    nCols = 6;
                    using (var range = workSheet.Cells[posData, 1, posData, nCols])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        range.Style.Font.Color.SetColor(Color.White);
                    }
                    posStart = posData + 1;
                    foreach (var detail in details)
                    {
                        posData++; workSheet.InsertRow(posData, 1);
                        workSheet.Cells[posData, 1].Value = detail.Target;
                        workSheet.Cells[posData, 2].Value = detail.Description;
                        workSheet.Cells[posData, 3].Value = detail.ControlExpiry;
                        workSheet.Cells[posData, 4].Value = detail.Reaction;
                        workSheet.Cells[posData, 5].Value = detail.KitSize;
                        workSheet.Cells[posData, 6].Value = detail.KitPerYear;
                    }
                }
            }

            for (int i = 1; i <= 7; i++)
            {
                workSheet.Column(i).AutoFit();
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;
            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("ExportDetails_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
        }

        // GET: Quotations/Details/5
        public async Task<IActionResult> Details(int? id, bool refreshTotals = false)
        {
            if (id == null)
            {
                return BadRequest();
            }

            // Goback da funzione di Crazione nuova Quotation
            if (id == -1) { return RedirectToAction("Index"); }

            // Forzo aggiornamento dei totali
            if (refreshTotals)
            {
                CalcolaTotali((int)id);
            }

            Quotation quotation = await _context.Quotations
                                        .Include(q => q.CommercialEntity)
                                        .Include(q => q.InstrumentType)
                                        .SingleOrDefaultAsync(q => q.QuotationId == id);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", id);
                return RedirectToAction("Index");
            }
            QuotationDetailDTO dto = new QuotationDetailDTO() {
                quotation = quotation,
                ElitePCRKits = _context.QuotationElitePCRKits
                                    .Include(t => t.ElitePCRKit)
                                    .Where(t => t.Quotation.QuotationId == id && t.ElitePCRKit.PCRType != Lookup.ElitePCRKitType.Consumable)
                                    .OrderBy(t => t.ElitePCRKit.PCRTarget)
                                    .ThenBy(t => t.ElitePCRKit.PCRType).ToList(),
                Consumables = _context.QuotationElitePCRKits
                                    .Include(t => t.ElitePCRKit)
                                    .Where(t => t.Quotation.QuotationId == id && t.ElitePCRKit.PCRType == Lookup.ElitePCRKitType.Consumable)
                                    .OrderBy(t => t.ElitePCRKit.Description)
                                    .ThenBy(t => t.ElitePCRKit.PCRType).ToList(),
                OpenPCRKits = _context.QuotationOpenPCRKits
                                    .Where(t => t.Quotation.QuotationId == id && t.bHidden == false)
                                    .OrderBy(t => t.OpenPCRKitId)
                                    .ThenBy(t => t.PCRType).ToList(),
                Extractions = _context.QuotationExtractions
                                    .Include(t => t.Extraction)
                                    .Where(t => t.Quotation.QuotationId == id)
                                    .OrderBy(t => t.Extraction.Description).ToList(),
                Totals = _context.QuotationsTotal
                                 .Where(t => t.Quotation.QuotationId == id && (User.IsInRole(Lookup.Role_QuotationManager) || t.Visibility == true))
                                 .OrderBy(t => t.Id).ToList()

            };
            dto.Elitekit2Del = dto.ElitePCRKits.Select(t => t.ElitePCRKit.PCRTarget).Distinct().ToList();
            dto.Elitekit2Add = (from k in _context.ElitePCRKits
                           where !dto.Elitekit2Del.Contains(k.PCRTarget) && k.PCRType != Lookup.ElitePCRKitType.Consumable
                           select k.PCRTarget).Distinct().ToList();
            dto.Openkit2Del = dto.OpenPCRKits.Select(t => t.OpenPCRKitDeco).Distinct().ToList();

            // TOTALE Extraction 
            var totale = calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.singleplex) + 
                                   calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.multiplex) + 
                                   calcolaSogliaMaxOpenKits(quotation.QuotationId);
            dto.overflowExtraction = (totale < (dto.quotation.ExtractionEliteMGBKit + dto.quotation.ExtractionEliteMGBPanel + dto.quotation.ExtractionOpenKit));

            dto.bQManagerTopLevel = false;
            if (User.IsInRole(Lookup.Role_QuotationManager))
            {
                string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    //UserCfg userCfg = _context.UserCfg.Find(UserId);
                    //if (userCfg != null)
                    //{
                    //    dto.bQManagerTopLevel = string.IsNullOrWhiteSpace(userCfg.CommercialEntity);
                    //}
                    // TopLevel -> Nessuna CommerciaEntity configurata
                    dto.bQManagerTopLevel = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity)).Count() == 0;
                }
            }
            return View(dto);
        }


        [HttpPost]
        public ActionResult GetInstrumentTypeInfo(int id)
        {
            var ret = _context.InstrumentType.SingleOrDefault(t => t.ID == id);
            return Json(ret);
        }


        [HttpGet]
        public object GetElitePcrKits(DataSourceLoadOptions loadOptions, int? QuotationId)
        {
            var kits = _context.QuotationElitePCRKits
                               .Include(t => t.Quotation)
                               .Include(t => t.ElitePCRKit)
                               .Where(t => t.Quotation.QuotationId == QuotationId && t.ElitePCRKit.PCRType != Lookup.ElitePCRKitType.Consumable)
                               .OrderBy(t => t.ElitePCRKit.PCRTarget)
                               .ThenBy(t=> t.ElitePCRKit.PCRType).ToList();
            return DataSourceLoader.Load(kits, loadOptions);
        }
        [HttpGet]
        public object GetOpenPcrKits(DataSourceLoadOptions loadOptions, int? QuotationId)
        {
            var kits = _context.QuotationOpenPCRKits
                               .Include(t => t.Quotation)
                               .Where(t => t.Quotation.QuotationId == QuotationId && t.bHidden == false)
                               .OrderBy(t => t.OpenPCRKitId)
                               .ThenBy(t => t.PCRType).ToList();
            return DataSourceLoader.Load(kits, loadOptions);
        }

        [HttpGet]
        public object GetConsumables(DataSourceLoadOptions loadOptions, int? QuotationId)
        {
            var kits = _context.QuotationElitePCRKits
                               .Include(t => t.Quotation)
                               .Include(t => t.ElitePCRKit)
                               .Where(t => t.Quotation.QuotationId == QuotationId && t.ElitePCRKit.PCRType == Lookup.ElitePCRKitType.Consumable)
                               .OrderBy(t => t.ElitePCRKit.PCRTarget).ToList();
            return DataSourceLoader.Load(kits, loadOptions);
        }

        [HttpGet]
        public object GetExtractions(DataSourceLoadOptions loadOptions, int? QuotationId)
        {
            var kits = _context.QuotationExtractions
                               .Include(t => t.Quotation)
                               .Include(t => t.Extraction)
                               .Where(t => t.Quotation.QuotationId == QuotationId)
                               .OrderBy(t => t.Extraction.Description).ToList();
            return DataSourceLoader.Load(kits, loadOptions);
        }
        // GET: Quotations/AddElitePCRKits
        public IActionResult AddElitePCRKits(int QuotationId, string selectedKit)
        {

            var kits = selectedKit.Split('|');
            var kit2Add = (from k in _context.ElitePCRKits
                           where kits.Contains(k.PCRTarget)
                           select k.ID).ToList();
            Quotation quotation = _context.Quotations.SingleOrDefault(q => q.QuotationId == QuotationId);

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var item in kit2Add)
                    {
                        QuotationElitePCRKit newRec = new QuotationElitePCRKit()
                        {
                            ElitePCRKitId = item,
                            UserCreate = User.Identity.Name,
                            TmstCreate = DateTime.Now,
                            UserLastUpd = User.Identity.Name,
                            TmstLastUpd = DateTime.Now,
                            Quotation = quotation
                        };
                        _context.Add(newRec);
                        _context.SaveChanges();
                    }
                    dbContextTransaction.Commit();
                    return RedirectToAction("EditElitePcrKits", new { id = QuotationId });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    TempData["MsgToLayout"] = String.Format("Error in Quotation AddElitePCRKits Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    return RedirectToAction("Index");
                }
            }
        }

        // GET: Quotations/AddOpenPCRKits
        public IActionResult AddOpenPCRKits(int QuotationId, int count)
        {
            int maxId = 0;
            var openKits = _context.QuotationOpenPCRKits.Where(t => t.Quotation.QuotationId == QuotationId);
            if (openKits.Count()>0) { maxId = openKits.Max(t => t.OpenPCRKitId); }
            
            Quotation quotation = _context.Quotations.SingleOrDefault(q => q.QuotationId == QuotationId);

            if (count > 30) { count = 30; }
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    for (int i=1; i <= count; i++)
                    {
                        maxId++;
                        QuotationOpenPCRKit newRec1 = new QuotationOpenPCRKit()
                        {
                            OpenPCRKitId = maxId,
                            Quotation = quotation,
                            PCRType = Lookup.OpenPCRKitType.Assay,
                            Description = "Open Kit Name " + maxId,
                            ggExpiry = Lookup.OpenPCRKitsGGExpiry.NA,
                            UserCreate = User.Identity.Name,
                            TmstCreate = DateTime.Now,
                            UserLastUpd = User.Identity.Name,
                            TmstLastUpd = DateTime.Now
                        };
                        QuotationOpenPCRKit newRec2 = new QuotationOpenPCRKit()
                        {
                            OpenPCRKitId = maxId,
                            Quotation = quotation,
                            PCRType = Lookup.OpenPCRKitType.Control,
                            Description = "Open Positive Control",
                            ggExpiry = Lookup.OpenPCRKitsGGExpiry.GG15,
                            KitSize = 4,
                            UserCreate = User.Identity.Name,
                            TmstCreate = DateTime.Now,
                            UserLastUpd = User.Identity.Name,
                            TmstLastUpd = DateTime.Now
                        };
                        _context.Add(newRec1);
                        _context.Add(newRec2);
                        _context.SaveChanges();
                    }
                    dbContextTransaction.Commit();
                    return RedirectToAction("EditOpenPcrKits", new { id = QuotationId });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    TempData["MsgToLayout"] = String.Format("Error in Quotation AddOpenPCRKits Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    return RedirectToAction("Index");
                }
            }
        }

        // GET: Quotations/DeleteElitePCRKits
        public IActionResult DeleteElitePCRKits(int QuotationId, string selectedKit)
        {

            var kits = selectedKit.Split('|');

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    List<QuotationElitePCRKit> rec2Del = _context.QuotationElitePCRKits.Include(t => t.ElitePCRKit).Where(t => kits.Contains(t.ElitePCRKit.PCRTarget)).ToList();
                    _context.QuotationElitePCRKits.RemoveRange(rec2Del);
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return RedirectToAction("Details", new { id = QuotationId, refreshTotals=true });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    TempData["MsgToLayout"] = String.Format("Error in Quotation DeleteElitePCRKits Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    return RedirectToAction("Index");
                }
            }
        }

        // GET: Quotations/DeleteOpenPCRKits
        public IActionResult DeleteOpenPCRKits(int QuotationId, string selectedKit)
        {

            var kits = selectedKit.Split('|');

            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    List<QuotationOpenPCRKit> rec2Del = _context.QuotationOpenPCRKits.Where(t => kits.Contains(t.OpenPCRKitDeco)).ToList();
                    _context.QuotationOpenPCRKits.RemoveRange(rec2Del);
                    _context.SaveChanges();
                    dbContextTransaction.Commit();
                    return RedirectToAction("Details", new { id = QuotationId, refreshTotals = true });
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    TempData["MsgToLayout"] = String.Format("Error in Quotation DeleteOpenPCRKits Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    return RedirectToAction("Index");
                }
            }
        }

        [HttpPut]
        public IActionResult PutElitePcrKit(int key, string values)
        {
            QuotationElitePCRKit kit2Update = _context.QuotationElitePCRKits.First(t => t.Id == key);
            try {
                JsonConvert.PopulateObject(values, kit2Update);
                if (!TryValidateModel(kit2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                kit2Update.UserLastUpd = User.Identity.Name;
                kit2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            } 
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult PutOpenPcrKit(int key, string values)
        {
            QuotationOpenPCRKit kit2Update = _context.QuotationOpenPCRKits.First(t => t.Id == key);
            try
            {
                JsonConvert.PopulateObject(values, kit2Update);
                if (!TryValidateModel(kit2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                kit2Update.UserLastUpd = User.Identity.Name;
                kit2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult PutExtraction(int key, string values)
        {
            QuotationExtraction kit2Update = _context.QuotationExtractions.First(t => t.Id == key);
            try
            {
                JsonConvert.PopulateObject(values, kit2Update);
                if (!TryValidateModel(kit2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                kit2Update.UserLastUpd = User.Identity.Name;
                kit2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }




        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var quotation = await _context.Quotations
        //        .Include(q => q.CommercialEntity)
        //        .SingleOrDefaultAsync(m => m.QuotationId == id);
        //    if (quotation == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(quotation);
        //}

        //// GET: Quotations/Create
        //public IActionResult Create(string customerName)
        //{
        //    var consumable = _context.ElitePCRKits.Where(t => t.PCRType == Lookup.ElitePCRKitType.Consumable).ToList();
        //    var extraction = _context.Extractions.ToList();

        //    Quotation quotation = new Quotation()
        //    {
        //        CustomerName = customerName.Trim(),
        //        UserOwner = User.Identity.Name,
        //        TmstCreate= DateTime.Now,
        //        UserLastUpd = User.Identity.Name,
        //        TmstLastUpd = DateTime.Now
        //    };
        //    if (quotation.CustomerName.Length > 100) { quotation.CustomerName = quotation.CustomerName.Substring(0, 100); }

        //    using (var dbContextTransaction = _context.Database.BeginTransaction())
        //    {
        //        try
        //        {
        //            _context.Add(quotation);
        //            _context.SaveChanges();

        //            // inserimento consumable 
        //            foreach (var item in consumable)
        //            {
        //                QuotationElitePCRKit newRec = new QuotationElitePCRKit()
        //                {
        //                    ElitePCRKitId = item.ID,
        //                    Quotation = quotation,
        //                    UserCreate = User.Identity.Name,
        //                    TmstCreate = DateTime.Now,
        //                    UserLastUpd = User.Identity.Name,
        //                    TmstLastUpd = DateTime.Now
        //                };
        //                _context.Add(newRec);
        //                _context.SaveChanges();
        //            }
        //            // inserimento Extractions
        //            foreach (var item in extraction)
        //            {
        //                QuotationExtraction newRec = new QuotationExtraction()
        //                {
        //                    ExtractionId = item.ID,
        //                    Quotation = quotation,
        //                    UserCreate = User.Identity.Name,
        //                    TmstCreate = DateTime.Now,
        //                    UserLastUpd = User.Identity.Name,
        //                    TmstLastUpd = DateTime.Now
        //                };
        //                _context.Add(newRec);
        //                _context.SaveChanges();
        //            }

        //            dbContextTransaction.Commit();
        //            return RedirectToAction("Edit", new { id = quotation.QuotationId });
        //        }
        //        catch (Exception e)
        //        {
        //            dbContextTransaction.Rollback();
        //            TempData["MsgToLayout"] = String.Format("Error in Quotation Create Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
        //            return RedirectToAction("Index");
        //        }
        //    }
        //}

        // GET: Quotations/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            Quotation quotation = new Quotation();
            if (id == null)
            {
                return BadRequest();
            }

            if (id == -1)
            {
                quotation.QuotationId = -1;
            } else {
                quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == id);
                if (quotation == null)
                {
                    TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", id);
                    return RedirectToAction("Index");
                }
            }
            ViewData["CommercialEntityID"] = new SelectList(_context.CommercialEntity, "ID", "Description", quotation.CommercialEntityID);
            return View(quotation);
        }

        // POST: Quotations/EditPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(int QuotationId)
        {
            Quotation quotation = new Quotation();
            var consumable = _context.ElitePCRKits.Where(t => t.PCRType == Lookup.ElitePCRKitType.Consumable).ToList();
            var extraction = _context.Extractions.ToList();
            var bCreateNewQuotation = false;

            if (QuotationId == -1) {
                bCreateNewQuotation = true;
            } else { 
                quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == QuotationId);
                if (quotation == null)
                {
                    TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", QuotationId);
                    return RedirectToAction("Index");
                }
            }
            if (await TryUpdateModelAsync<Quotation>(quotation))
            {
                var context = new ValidationContext(quotation, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(quotation, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (quotation.ContractType != Lookup.ContractType.REAGENT_RENTAL)
                    {
                        quotation.YearRentalDuration = 0;
                        quotation.PercService = 0;
                        quotation.TotalAnnualRent = 0;
                    }
                    quotation.UserLastUpd = User.Identity.Name;
                    quotation.TmstLastUpd = DateTime.Now;

                    if (bCreateNewQuotation)
                    {
                        quotation.ContractDuration = quotation.ContractDuration  ?? 0;
                        quotation.LISConnection = quotation.LISConnection ?? 0;
                        quotation.TotalOtherCosts = quotation.TotalOtherCosts ?? 0;
                        quotation.CurrentBusiness = quotation.CurrentBusiness ?? 0;
                        quotation.CurrentBusinessPerYear = quotation.CurrentBusinessPerYear ?? 0;
                        quotation.TotalAnnualRent = quotation.TotalAnnualRent ?? 0;
                        quotation.TotalAnnualService = quotation.TotalAnnualService ?? 0;
                        quotation.YearRentalDuration = quotation.YearRentalDuration ?? 0;
                        quotation.PercService = quotation.PercService ?? 0;
                        quotation.InstrumentNumber = quotation.InstrumentNumber ?? 1;
                        // assegno lo strumento se ce n'� solo 1 !!
                        if (_context.InstrumentType.Count() == 1)
                        {
                            quotation.InstrumentTypeID = _context.InstrumentType.Select(t => t.ID).FirstOrDefault();
                            quotation.InstrumentDiscount = 0;
                            quotation.InstrumentSellingPrice = _context.InstrumentType.Select(t => t.OfficialPrice).FirstOrDefault();
                        }
                    }

                    if (quotation.ContractType == Lookup.ContractType.REAGENT_RENTAL)
                    {
                        quotation.InstrumentDiscount = 0;  
                        quotation.InstrumentSellingPrice = 0;
                    }

                    using (var dbContextTransaction = _context.Database.BeginTransaction())
                    {
                        try
                        {

                            if (!bCreateNewQuotation)
                            {
                                _context.Update(quotation);
                            }
                            else
                            {
                                quotation.UserOwner = User.Identity.Name;
                                quotation.TmstCreate = DateTime.Now;
                                quotation.QuotationId = 0;
                                _context.Add(quotation);
                            }
                            await _context.SaveChangesAsync();

                            if (bCreateNewQuotation)
                            {
                                // inserimento consumable 
                                foreach (var item in consumable)
                                {
                                    QuotationElitePCRKit newRec = new QuotationElitePCRKit()
                                    {
                                        ElitePCRKitId = item.ID,
                                        Quotation = quotation,
                                        UserCreate = User.Identity.Name,
                                        TmstCreate = DateTime.Now,
                                        UserLastUpd = User.Identity.Name,
                                        TmstLastUpd = DateTime.Now
                                    };
                                    _context.Add(newRec);
                                    await _context.SaveChangesAsync();
                                }
                                // inserimento Extractions
                                foreach (var item in extraction)
                                {
                                    QuotationExtraction newRec = new QuotationExtraction()
                                    {
                                        ExtractionId = item.ID,
                                        Quotation = quotation,
                                        UserCreate = User.Identity.Name,
                                        TmstCreate = DateTime.Now,
                                        UserLastUpd = User.Identity.Name,
                                        TmstLastUpd = DateTime.Now
                                    };
                                    _context.Add(newRec);
                                    await _context.SaveChangesAsync();
                                }
                            }
                            dbContextTransaction.Commit();
                        }
                        catch (Exception e)
                        {
                            dbContextTransaction.Rollback();
                            TempData["MsgToLayout"] = String.Format("Error in Quotation Create Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                            return RedirectToAction("Index");
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", new { id = quotation.QuotationId, refreshTotals = true });
            }

            ViewData["CommercialEntityID"] = new SelectList(_context.CommercialEntity, "ID", "Description", quotation.CommercialEntityID);
            return View("Edit", quotation);
        }


        // GET: Quotations/EditInstrument/5
        public async Task<IActionResult> EditInstrument(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            var quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == id);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", id);
                return RedirectToAction("Index");
            }
            ViewData["InstrumentTypeID"] = new SelectList(_context.InstrumentType, "ID", "Type", quotation.InstrumentTypeID);
            return View(quotation);
        }

        // POST: Quotations/EditInstrumentPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditInstrumentPost(int QuotationId)
        {

            var quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == QuotationId);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", QuotationId);
                return RedirectToAction("Index");
            }
            if (await TryUpdateModelAsync<Quotation>(quotation))
            {
                var context = new ValidationContext(quotation, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(quotation, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }
            
            if (ModelState.IsValid) { 
                if (quotation.InstrumentTypeID == null)
                {
                    ModelState.AddModelError("InstrumentTypeID", "The Instrument Type field is required");
                }
                if (quotation.InstrumentNumber == null)
                {
                    quotation.InstrumentNumber = 1;
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    quotation.UserLastUpd = User.Identity.Name;
                    quotation.TmstLastUpd = DateTime.Now;
                    _context.Update(quotation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", new { id = quotation.QuotationId, refreshTotals = true });
            }

            ViewData["InstrumentTypeID"] = new SelectList(_context.InstrumentType, "ID", "Type", quotation.InstrumentTypeID);
            return View("EditInstrument", quotation);
        }

        // POST: Quotations/EditExtractionsPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditExtractionsPost(int QuotationId)
        {

            var quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == QuotationId);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", QuotationId);
                return RedirectToAction("Index");
            }
            if (await TryUpdateModelAsync<Quotation>(quotation))
            {
                var context = new ValidationContext(quotation, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(quotation, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    quotation.ExtractionEliteMGBKit = quotation.ExtractionEliteMGBKit??0;
                    quotation.ExtractionEliteMGBPanel= quotation.ExtractionEliteMGBPanel ?? 0;
                    quotation.ExtractionOpenKit = quotation.ExtractionOpenKit ?? 0;
                    quotation.UserLastUpd = User.Identity.Name;
                    quotation.TmstLastUpd = DateTime.Now;
                    _context.Update(quotation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", new { id = quotation.QuotationId, refreshTotals = true });
            }

            quotation.maxSingleplex = calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.singleplex);
            quotation.maxMultiplex = calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.multiplex);
            quotation.maxOpenKit = calcolaSogliaMaxOpenKits(quotation.QuotationId);

            return View("EditExtractions", quotation);
        }

        private int calcolaSogliaMaxPcrKits (int quotationId, Lookup.ExtractionType extractionType)
        {
            // somma TestPerYear calcolato solo sul tipo ASSAY
            int tot = _context.QuotationElitePCRKits
                                  .Include(t => t.ElitePCRKit)
                                  .Where(t => t.Quotation.QuotationId == quotationId && t.ElitePCRKit.ExtractionType == extractionType && 
                                              t.ElitePCRKit.PCRType == Lookup.ElitePCRKitType.Assay)
                                  .Sum(t => t.TestPerYear * t.ElitePCRKit.MultipleReactionNormalized).GetValueOrDefault();
            return tot;
        }


        private int calcolaSogliaMaxOpenKits(int quotationId)
        {
            // somma TestPerYear calcolato solo sul tipo ASSAY
            int tot = _context.QuotationOpenPCRKits
                                  .Where(t => t.Quotation.QuotationId == quotationId && t.bHidden == false && t.PCRType == Lookup.OpenPCRKitType.Assay)
                                  .Sum(t => t.TestPerYear).GetValueOrDefault();
            return tot;
        }

        // GET: Quotations/EditElitePcrKits/5
        public async Task<IActionResult> EditElitePcrKits(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            Quotation quotation = await _context.Quotations
                                        .Include(q => q.CommercialEntity)
                                        .Include(q => q.InstrumentType)
                                        .SingleOrDefaultAsync(q => q.QuotationId == id);
            //if (id == null)
            //{
            //    return BadRequest();
            //}

            //var kits = _context.QuotationElitePCRKits
            //                   .Include(t => t.Quotation)
            //                   .Include(t => t.ElitePCRKit)
            //                   .Where(m => m.Quotation.QuotationId == id).ToList();
            //if (kits == null)
            //{
            //    TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", id);
            //    return RedirectToAction("Index");
            //}
            //ViewData["QuotationsId"] = id;
            return View(quotation);
        }

        // POST: Quotations/EditExtractionsPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditElitePcrKitsPost(int QuotationId)
        {

            var quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == QuotationId);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", QuotationId);
                return RedirectToAction("Index");
            }
            if (await TryUpdateModelAsync<Quotation>(quotation))
            {
                var context = new ValidationContext(quotation, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(quotation, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            // Ricalcolo valori Max Singleplex e MaxMultiplex
            if (ModelState.IsValid)
            {
                int currentMax = calcolaSogliaMaxPcrKits(QuotationId, Lookup.ExtractionType.singleplex);
                if (quotation.ExtractionEliteMGBKit > currentMax) quotation.ExtractionEliteMGBKit = currentMax;
                currentMax = calcolaSogliaMaxPcrKits(QuotationId, Lookup.ExtractionType.multiplex);
                if (quotation.ExtractionEliteMGBPanel > currentMax) quotation.ExtractionEliteMGBPanel = currentMax;
            }

            if (ModelState.IsValid)
            {
                try
                {
                    quotation.UserLastUpd = User.Identity.Name;
                    quotation.TmstLastUpd = DateTime.Now;
                    _context.Update(quotation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("Details", new { id = quotation.QuotationId, refreshTotals = true });
            }

            return View("EditElitePcrKits", quotation);
        }

        // POST: Quotations/EditOpenPcrKitsPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditOpenPcrKitsPost(int QuotationId)
        {

            var quotation = await _context.Quotations.SingleOrDefaultAsync(m => m.QuotationId == QuotationId);
            if (quotation == null)
            {
                TempData["MsgToLayout"] = String.Format("Quotation Not Found (id {0})", QuotationId);
                return RedirectToAction("Index");
            }


            try
            {
                //* estraggo i Target degli Assay con control = 0 (included) e aggiorno tutti i 'control' mettendo bHidden = true 
                int result = _context.Database.ExecuteSqlCommand("UPDATE QuotationOpenPCRKits set bHidden = 0 where QuotationId = " + QuotationId);
                // estraggo gli ID degli Assay con INCLUDED
                HashSet<int> idIncluded = new HashSet<int>(from t in _context.QuotationOpenPCRKits
                                                       where t.Quotation.QuotationId == QuotationId && 
                                                             t.PCRType==Lookup.OpenPCRKitType.Assay &&
                                                             t.Control == Lookup.OpenPCRKitControl.included
                                                       select t.OpenPCRKitId);
                // metto a Hidden tutti i Control relativi agli Assay con Included !!
                var controlDaEscludere = _context.QuotationOpenPCRKits.Where(t => t.Quotation.QuotationId == QuotationId &&
                                                                                t.PCRType == Lookup.OpenPCRKitType.Control &&
                                                                                idIncluded.Contains(t.OpenPCRKitId)).ToList();
                foreach (var item in controlDaEscludere)
                {
                    item.bHidden = true;
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            

            //if (await TryUpdateModelAsync<Quotation>(quotation))
            //{
            //    var context = new ValidationContext(quotation, serviceProvider: null, items: null);
            //    var results = new List<ValidationResult>();
            //    var isValid = Validator.TryValidateObject(quotation, context, results);
            //    if (!isValid)
            //    {
            //        foreach (var validationResult in results)
            //        {
            //            ModelState.AddModelError("", validationResult.ErrorMessage);
            //        }
            //    }
            //}

            //if (ModelState.IsValid)
            //{

            //}

            return RedirectToAction("Details", new { id = quotation.QuotationId, refreshTotals = true });
        }

        // GET: Quotations/EditOpenPcrKits/5
        public async Task<IActionResult> EditOpenPcrKits(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Quotation quotation = await _context.Quotations
                                        .Include(q => q.CommercialEntity)
                                        .Include(q => q.InstrumentType)
                                        .SingleOrDefaultAsync(q => q.QuotationId == id);
            return View(quotation);
        }



        // GET: Quotations/EditConsumables/5
        public async Task<IActionResult> EditConsumables(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            Quotation quotation = await _context.Quotations
                                        .Include(q => q.CommercialEntity)
                                        .Include(q => q.InstrumentType)
                                        .SingleOrDefaultAsync(q => q.QuotationId == id);
            return View(quotation);
        }

        // POST: Quotations/EditConsumablesPost/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditConsumablesPost(int QuotationId)
        {
            return RedirectToAction("Details", new { id = QuotationId, refreshTotals = true });
        }

        // GET: Quotations/EditExtractions/5
        public async Task<IActionResult> EditExtractions(int? id)
        {
            if (id == null)
            {
                return BadRequest();
            }

            Quotation quotation = await _context.Quotations
                                        .Include(q => q.CommercialEntity)
                                        .Include(q => q.InstrumentType)
                                        .SingleOrDefaultAsync(q => q.QuotationId == id);

            quotation.maxSingleplex = calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.singleplex);
            quotation.maxMultiplex = calcolaSogliaMaxPcrKits(quotation.QuotationId, Lookup.ExtractionType.multiplex);
            quotation.maxOpenKit = calcolaSogliaMaxOpenKits(quotation.QuotationId);

            return View(quotation);
        }


        // GET:Quotations/Delete/5
        public async Task<IActionResult> DeleteQuotation(int id)
        {
            if (id == null )
            {
                return BadRequest();
            }
            Quotation quotation = _context.Quotations.SingleOrDefault(m => m.QuotationId == id);
            if (quotation == null) {
                return NotFound();
            }
            // ELitePCR + Consumable
            var elitePcr2Del =  _context.QuotationElitePCRKits.Where(t => t.Quotation.QuotationId == id).ToList();
            // OpenPCR
            var openPcr2Del = _context.QuotationOpenPCRKits.Where(t => t.Quotation.QuotationId == id).ToList();
            // Extractions
            var Extraction2Del = _context.QuotationExtractions.Where(t => t.Quotation.QuotationId == id).ToList();
            // TOTALS
            var TotalsPCR = _context.QuotationsTotalPCR.Where(t => t.Quotation.QuotationId == id).ToList();
            var Totals = _context.QuotationsTotal.Where(t => t.Quotation.QuotationId == id).ToList();

            _context.QuotationElitePCRKits.RemoveRange(elitePcr2Del);
            _context.QuotationOpenPCRKits.RemoveRange(openPcr2Del);
            _context.QuotationExtractions.RemoveRange(Extraction2Del);
            _context.QuotationsTotalPCR.RemoveRange(TotalsPCR);
            _context.QuotationsTotal.RemoveRange(Totals);
            _context.Quotations.Remove(quotation);
            await _context.SaveChangesAsync();

            TempData["MsgToLayout"] = String.Format("Quotation successfully deleted");
            return RedirectToAction("Index");
        }

        // GET:Quotations/UpdateCompensation/5
        public async Task<IActionResult> UpdateCompensation(int id, bool value)
        {
            if (id == null)
            {
                return BadRequest();
            }
            Quotation quotation = _context.Quotations.SingleOrDefault(m => m.QuotationId == id);
            if (quotation == null)
            {
                return NotFound();
            }
            try
            {
                quotation.Compensation = value;
                _context.Update(quotation);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return RedirectToAction("Details", new { id = id, refreshTotals = true });
        }

        // GET:Quotations/GenerateReport/5
        public async Task<IActionResult> GenerateReport(int id)
        {
            Quotation quotation = _context.Quotations.SingleOrDefault(m => m.QuotationId == id);
            if (quotation == null)
            {
                return NotFound();
            }
            try
            {
                quotation.Status = Lookup.QStatus.waiting;
                _context.Update(quotation);
                await _context.SaveChangesAsync();
                TempData["MsgToLayout"] = String.Format("Please check your email inbox: a report is on the way!");
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return RedirectToAction("Index");
        }


        private bool QuotationExists(int id)
        {
            return _context.Quotations.Any(e => e.QuotationId == id);
        }

        public void CalcolaTotali(int QuotationId)
        {
            dynamic ret = null;

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Fetch<dynamic>(";EXEC QuotationTotalCalculate @id", new { id = QuotationId });
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("Quotations.CalcolaTotali() - ERRORE {0} {1}", e.Message, e.Source));
            }
        }
    }
}

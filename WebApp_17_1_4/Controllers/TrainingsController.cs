using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using WebApp.Repository;
using System.Collections;
using Microsoft.Extensions.Configuration;

namespace ELITeBoard.Controllers
{

    public class cfgSezione
    {
        public string start { get; set; }
        public string end { get; set; }
        public bool visible { get; set; }
    }

    [Authorize(Roles = "Training BackOffice")]
    public class TrainingsController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;

        public TrainingsController(WebAppDbContext context, ILogger<TRegistryController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
        }

        // GET: Trainings
        public async Task<IActionResult> Index()
        {
            bool bShow = false;
            string valore = "";
            valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TrainingIndex_ShowDeleted);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowDeleted"] = bShow;
            }
            valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TrainingIndex_ShowArchived);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowArchived"] = bShow;
            }
            return View();
        }

        // GET: DistributionList
        public async Task<IActionResult> DistributionList()
        {
            return View();
        }

        // GET: ParticipantList
        public async Task<IActionResult> ParticipantList()
        {
            return View();
        }

        // GET: Summary
        public async Task<IActionResult> Summary()
        {
            return View();
        }

        public async Task<IActionResult> goBackCreateRegistration()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            return RedirectToAction("Index", "Home");
        }


        [HttpGet]
        public object GetTrainings(DataSourceLoadOptions loadOptions, bool bReserved = true, bool bDeleted = false , bool bArchived = false)
        {
            var data = _context.V_Trainings
                               .Where(t => (!t.Status.Equals(Lookup.Training_Status_ARCHIVED) || bArchived == true) && 
                                     (t.Reserved == false || bReserved == true) && 
                                     (!t.Status.Equals(Lookup.Training_Status_DELETED) || bDeleted == true))
                               .OrderByDescending(t => t.PeriodFrom).ThenBy(t => t.Title).ThenBy(t => t.TrainingId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

    
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Get(int TrainingId)
        {
            Training data = _context.Trainings.Find(TrainingId);
            return Ok(data);
        }

        
        [HttpGet]
        [AllowAnonymous]
        public object GetTrainingForRegistration(DataSourceLoadOptions loadOptions)
        {
            var data = _context.V_Trainings
                               .Where(t => t.Status.Equals(Lookup.Training_Status_PUBLISHED))
                               .OrderByDescending(t => t.PeriodFrom).ThenBy(t => t.Title).ThenBy(t => t.TrainingId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetTrainingRegistrations(DataSourceLoadOptions loadOptions, int id)
        {
            var data = _context.TrainingRegistrations.Include(t=>t.Training).Where(t=>t.TrainingId == id)
                               .OrderBy(t => t.Name).ThenBy(t=>t.Surname).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetDistributionList(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingRegistrations
                               .Include(t => t.Training).ThenInclude(t=>t.TrainingType)
                               .Include(t => t.Training).ThenInclude(t => t.Hotel)
                               .Where(t=>t.SendNews == true)
                               .OrderBy(t => t.Name).ThenBy(t => t.Surname).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetParticipantList(DataSourceLoadOptions loadOptions, bool bShowAll = false)
        {
            var data = _context.TrainingRegistrations
                               .Include(t => t.Training).ThenInclude(t => t.TrainingType)
                               .Where(t=>t.Enabled == true|| bShowAll == true)
                               .OrderBy(t => t.Name).ThenBy(t => t.Surname).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        public ActionResult downloadAgenda(int id)
        {
            Training training = _context.Trainings.Find(id);
            if (training == null)
            {
                return NotFound(String.Format("Training not found !! (id: '{0}')", id));
            }
            if (string.IsNullOrWhiteSpace(training.Agenda))
            {
                return NotFound(String.Format("Agenda not valid !! (id: '{0}')", id));
            }
            if (!training.AgendaExist)
            {
                return NotFound(String.Format("Agenda '{0}' not found !!", training.Agenda));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(training.AgendaFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = training.Agenda };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadAgenda()", error));
                return BadRequest(error);
            }
        }

        public ActionResult downloadPassportScan(long id)
        {
            TrainingRegistration registration = _context.TrainingRegistrations.Find(id);
            if (registration == null)
            {
                return NotFound(String.Format("Training Registration not found !! (id: '{0}')", id));
            }
            if (string.IsNullOrWhiteSpace(registration.PassportScan))
            {
                return NotFound(String.Format("Passport Scan not valid !! (id: '{0}')", id));
            }
            if (!registration.PassportScanExist)
            {
                return NotFound(String.Format("Passport Scan '{0}' not found !!", registration.PassportScan));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(registration.PassportScanFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = registration.PassportScan };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadPassportScan()", error));
                return BadRequest(error);
            }
        }

        public ActionResult downloadInvLetter(long id)
        {
            TrainingRegistration registration = _context.TrainingRegistrations.Find(id);
            if (registration == null)
            {
                return NotFound(String.Format("Training Registration not found !! (id: '{0}')", id));
            }
            if (string.IsNullOrWhiteSpace(registration.InvLetterScan))
            {
                return NotFound(String.Format("Invitation Letter not valid !! (id: '{0}')", id));
            }
            if (!registration.InvLetterScanExist)
            {
                return NotFound(String.Format("Invitation Letter '{0}' not found !!", registration.InvLetterScan));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(registration.InvLetterScanFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = registration.InvLetterScan };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadInvLetter()", error));
                return BadRequest(error);
            }
        }
        
        public ActionResult downloadCertificate(long id)
        {
            TrainingRegistration registration = _context.TrainingRegistrations.Find(id);
            if (registration == null)
            {
                return NotFound(String.Format("Training Registration not found !! (id: '{0}')", id));
            }
            //if (string.IsNullOrWhiteSpace(registration.Certificate))
            //{
            //    return NotFound(String.Format("Certificate not valid !! (id: '{0}')", id));
            //}
            if (!registration.CertificateExist)
            {
                return NotFound(String.Format("Certificate '{0}' not found !!", registration.CertificateFileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(registration.CertificateFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = registration.CertificateFileName};
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadCertificate()", error));
                return BadRequest(error);
            }
        }
        //[HttpGet]
        //// ??? DA SOSTITUIRE CON QUELLO IN TREGISTRY
        //public object GetTrainingTypes(DataSourceLoadOptions loadOptions)
        //{
        //    //var data = _context.TrainingTypes.OrderBy(t => t.Description).ToList();
        //    //return DataSourceLoader.Load(data, loadOptions);
        //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(_context.TrainingTypes, loadOptions)), "application/json");
        //}

        //[HttpGet]
        //public object GetCountries(DataSourceLoadOptions loadOptions)
        //{
        //    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(_context.Countries, loadOptions)), "application/json");
        //}

        // GET: Contabilita/Edit
        public IActionResult Edit(int? id)
        {
            EditTrainingDTO dto = new EditTrainingDTO();
            if (id != null)
            {
                Training training = _context.Trainings.Find(id);
                if (training == null)
                {
                    return NotFound();
                }
                PropertyCopier<Training, EditTrainingDTO>.Copy(training, dto); // Copio proprietÓ con stesso nome<>
            } else
            {
                dto.PeriodFrom = DateTime.Now;
                dto.PeriodTo = DateTime.Now.AddDays(5);
                dto.Status = Lookup.Training_Status_NEW;
            }
            dto.trainingTypes = _context.TrainingTypes.OrderBy(t => t.Description).ToList();
            dto.hotels= _context.Hotels.OrderBy(t => t.Description).ToList();
            dto.trainingLocations = _context.TrainingLocations.OrderBy(t => t.Id).ToList();
            return View(dto);
        }

        // POST: Trainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
         public async Task<IActionResult> EditPost(IFormFile file, bool RemoveFile, [Bind("TrainingId,Title,PeriodFrom,PeriodTo,Location,Trainers,TrainingTypeId,HotelId,Note,Reserved,Status,Agenda")] Training training)
        {
            bool bUploadFile = false;
            string file2remove = "";

            if (ModelState.IsValid)
            {
                if (training.TrainingTypeId == 0 )
                {
                    ModelState.AddModelError("TrainingTypeId", "The Training Type field is required");
                }
            }

             if (ModelState.IsValid)
            {
                try
                {
                    if (RemoveFile)
                    {
                        if (!String.IsNullOrWhiteSpace(training.Agenda) && training.AgendaExist)
                        {
                            file2remove = training.AgendaFullFileName;
                        }
                        training.Agenda = null;
                    }
                    else
                    {
                        if (file != null && file.Length > 0)
                        {
                            if (!String.IsNullOrWhiteSpace(training.Agenda) && training.AgendaExist)
                            {
                                file2remove = training.AgendaFullFileName;
                            }
                            training.Agenda = file.FileName;
                            bUploadFile = true;
                        }
                    }

                    training.UserLastUpd = User.Identity.Name;
                    training.TmstLastUpd = DateTime.Now;

                    string msgLog = "";

                    if (training.TrainingId == 0)
                    {
                        _context.Add(training);
                        msgLog = "Create new Training!";
                    }
                    else
                    {
                        _context.Update(training);
                        msgLog = "Update Training!";
                    }
                    TrainingLog log = new TrainingLog
                    {
                        Note = msgLog,
                        TrainingId = training.TrainingId,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    _context.Add(log);
                    await _context.SaveChangesAsync();
                    if (bUploadFile)
                    {
                        uploadAgenda(training.TrainingId, file);
                    }
                    // Non rimuovo i file vecchi !!! 
                    //if (!string.IsNullOrWhiteSpace(file2remove) && !file2remove.Equals(training.AgendaFullFileName))
                    //{
                    //    System.IO.File.Delete(file2remove);
                    //}

                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            EditTrainingDTO dto = new EditTrainingDTO();
            PropertyCopier<Training, EditTrainingDTO>.Copy(training, dto); // Copio proprietÓ con stesso nome<>
            dto.trainingTypes = _context.TrainingTypes.OrderBy(t => t.Description).ToList();
            dto.hotels = _context.Hotels.OrderBy(t => t.Description).ToList();
            dto.trainingLocations = _context.TrainingLocations.OrderBy(t => t.Id).ToList();
            return View(dto);
        }

        private void uploadAgenda(int TrainingId, IFormFile file)
        {
            Training rec2upd = _context.Trainings.Find(TrainingId);
            rec2upd.Agenda = file.FileName;
            string path = rec2upd.AgendaPathName;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filename = rec2upd.AgendaFullFileName;
            using (FileStream fs = System.IO.File.Create(filename))
            {
                file.CopyTo(fs);
            }
            return;
        }

        private void UploadPassportScan(long TrainingRegistrationId, IFormFile file)
        {
            TrainingRegistration rec2upd = _context.TrainingRegistrations.Find(TrainingRegistrationId);
            rec2upd.PassportScan = file.FileName;
            string path = rec2upd.PassportScanPathName;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filename = rec2upd.PassportScanFullFileName;
            using (FileStream fs = System.IO.File.Create(filename))
            {
                file.CopyTo(fs);
            }

            return;
        }
        [HttpGet]
        public IActionResult SaveIndexFilters(bool showDeleted, bool showArchived)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_TrainingIndex_ShowDeleted, showDeleted.ToString());
            HttpContext.Session.SetString(Lookup.glb_sessionkey_TrainingIndex_ShowArchived, showArchived.ToString());
            return Ok("OK");
        }

        [HttpGet]
        public ActionResult Delete(int TrainingId)
        {
            string msgError = "";
            Training training = _context.Trainings.Find(TrainingId);
            try
            {
                training.UserLastUpd = User.Identity.Name;
                training.TmstLastUpd = DateTime.Now;
                training.Status = Lookup.Training_Status_DELETED;
                _context.Update(training);

                TrainingLog log = new TrainingLog
                {
                    Note = string.Format("Change Status - {0}", training.Status),
                    TrainingId = training.TrainingId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(log);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgError = String.Format("{0} / {1}", e.Message, e.Source);
            }
            return Content(JsonConvert.SerializeObject(msgError), "application/json");
        }

        [HttpGet]
        public ActionResult Publish(int TrainingId)
        {
            string msgError = "";
            Training training = _context.Trainings.Find(TrainingId);
            try
            {
                training.UserLastUpd = User.Identity.Name;
                training.TmstLastUpd = DateTime.Now;
                training.Status = Lookup.Training_Status_PUBLISHED;
                _context.Update(training);

                TrainingLog log = new TrainingLog
                {
                    Note = string.Format("Change Status - {0}", training.Status),
                    TrainingId = training.TrainingId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(log);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgError = String.Format("{0} / {1}", e.Message, e.Source);
            }
            return Content(JsonConvert.SerializeObject(msgError), "application/json");
        }

        [HttpGet]
        public ActionResult Close(int TrainingId)
        {
            string msgError = "";
            Training training = _context.Trainings.Find(TrainingId);
            try
            {
                training.UserLastUpd = User.Identity.Name;
                training.TmstLastUpd = DateTime.Now;
                training.Status = Lookup.Training_Status_CLOSED;
                _context.Update(training);

                TrainingLog log = new TrainingLog
                {
                    Note = string.Format("Change Status - {0}", training.Status),
                    TrainingId = training.TrainingId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(log);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgError = String.Format("{0} / {1}", e.Message, e.Source);
            }
            return Content(JsonConvert.SerializeObject(msgError), "application/json");
        }
        
        [HttpGet]
        public ActionResult Restore(int TrainingId)
        {
            string msgError = "";
            string statusPrec = "";
            Training training = _context.Trainings.Find(TrainingId);
            try
            {
                statusPrec = training.Status;

                training.UserLastUpd = User.Identity.Name;
                training.TmstLastUpd = DateTime.Now;
                if (training.Status.Equals(Lookup.Training_Status_CLOSED))
                {
                    training.Status = Lookup.Training_Status_PUBLISHED;
                } else
                {
                    if (_context.TrainingRegistrations.Where(t => t.TrainingId == TrainingId).Count() == 0)
                    {
                        training.Status = Lookup.Training_Status_NEW;
                    } else {
                        training.Status = Lookup.Training_Status_CLOSED;
                    }
                }
                _context.Update(training);

                TrainingLog log = new TrainingLog
                {
                    Note = string.Format("Change Status - Restore from {0} to {1}", statusPrec, training.Status),
                    TrainingId = training.TrainingId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(log);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgError = String.Format("{0} / {1}", e.Message, e.Source);
            }
            return Content(JsonConvert.SerializeObject(msgError), "application/json");
        }

        [HttpGet]
        public ActionResult Archive(int TrainingId)
        {
            string msgError = "";
            string statusPrec = "";
            Training training = _context.Trainings.Find(TrainingId);
            try
            {
                training.Status = Lookup.Training_Status_ARCHIVED;
                training.UserLastUpd = User.Identity.Name;
                training.TmstLastUpd = DateTime.Now;
                _context.Update(training);

                TrainingLog log = new TrainingLog
                {
                    Note = string.Format("Change Status - {0}", training.Status),
                    TrainingId = training.TrainingId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(log);

                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgError = String.Format("{0} / {1}", e.Message, e.Source);
            }
            return Content(JsonConvert.SerializeObject(msgError), "application/json");
        }

        // GET: Trainings/CreateRegistration
        [AllowAnonymous]
        public IActionResult CreateRegistration()
        {
            TrainingRegistrationDTO dto = new TrainingRegistrationDTO()
            {
                Status = Lookup.TrainingRegistration_Status_NEW,
                countries = _context.Countries.OrderBy(t => t.CountryId).ToList(),
                trainings = _context.V_Trainings.Where(t => t.Status.Equals(Lookup.Training_Status_PUBLISHED))
                               .OrderBy(t => t.PeriodFrom).ThenBy(t => t.Title).ThenBy(t => t.TrainingId).ToList(),
                VISA = Lookup.TrainigVISA.yes_with_letter,
                typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList(),
                jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList(),
                typesOfParticipantITA = _context.TrainingTypeOfParticipants.Where(t=>t.Italian == true).OrderBy(t => t.Id).ToList()
            };

            return View(dto);
        }

        // POST: Trainings/CreateRegistration/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> CreateRegistration(IFormFile file, 
                [Bind("TrainingId,Title,Status,Name,Surname,EmailAddress,MobileNumber,CompanyName,CompanyAddress,CompanyAddressNr,CompanyAddressCity,CompanyAddressZipCode,JobPosition,CountryId,VISA,Accomodation,CheckIn,CheckOut,TypeOfParticipant")] TrainingRegistration registration)
        {
            bool bUploadFile = false;
            string msgSendEmail = "";
            if (ModelState.IsValid)
            {
                try
                {
                    if (file != null && file.Length > 0)
                    {
                        registration.PassportScan = file.FileName;
                        bUploadFile = true;
                    }

                    if (!(bool)registration.Accomodation)
                    {
                        registration.CheckIn = null;
                        registration.CheckOut = null;
                    }

                    registration.Status = Lookup.TrainingRegistration_Status_NEW;
                    registration.UserLastUpd = User.Identity.Name;
                    registration.TmstLastUpd = DateTime.Now;
                    registration.TmstCreate = DateTime.Now;

                    _context.Add(registration);

                    TrainingLog log = new TrainingLog
                    {
                        Note = string.Format("Create New Registration - {0} {1} {2}", registration.Name, registration.Surname, registration.EmailAddress),
                        TrainingId = registration.TrainingId,
                        TrainingRegistrationId = registration.TrainingRegistrationId,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    _context.Add(log);

                    await _context.SaveChangesAsync();

                    if (bUploadFile)
                    {
                        UploadPassportScan(registration.TrainingRegistrationId, file);
                    }

                    // INVIA EMAIL RIEPILOGO al Richiedente
                    msgSendEmail = "[OK]";
                    EmailTemplate newReg = GetEmailInfo(registration.TrainingRegistrationId, Lookup.Training_Email_NewRegistration);
                    if (!string.IsNullOrWhiteSpace(newReg.To))
                    {
                        string[] attach = null;
                        Training training = _context.Trainings.Find(registration.TrainingId);
                        if (training != null)
                        {
                            if (training.AgendaExist)
                            {
                                Array.Resize(ref attach, 1);
                                attach[0] = training.AgendaFullFileName;
                            }
                        }
                        var ret = _emailSender.SendEmailAsync(newReg.To, newReg.Subject, newReg.Body, attach, "Training");
                        if (ret.IsFaulted) msgSendEmail = "*** ERROR SendEmailAsync() ***";
                    } else
                    {
                        msgSendEmail = "*** No TO Address ***";
                    }
                    TrainingLog logEmail = new TrainingLog
                    {
                        Note = string.Format("{3} Send Email {0} [{1}] to {2} [{4}]", newReg.Subject, Lookup.Training_Email_NewRegistration, newReg.To, msgSendEmail, registration.FullName),
                        TrainingId = registration.TrainingId,
                        TrainingRegistrationId = registration.TrainingRegistrationId,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    _context.Add(logEmail);
                    await _context.SaveChangesAsync();

                    // INVIA EMAIL BACKOFFICE 
                    msgSendEmail = "[OK]";
                    EmailTemplate notifyNewRec = GetEmailInfo(registration.TrainingRegistrationId, Lookup.Training_Email_NotifyNewRegistration);
                    if (!string.IsNullOrWhiteSpace(notifyNewRec.To))
                    {
                        var ret = _emailSender.SendEmailAsync(notifyNewRec.To, notifyNewRec.Subject, notifyNewRec.Body, null, "Training");
                        if (ret.IsFaulted) msgSendEmail = "*** ERROR SendEmailAsync() ****";
                    } else
                    {
                        msgSendEmail = "*** No TO Address ***";
                    }
                    TrainingLog logEmailBackOffice = new TrainingLog
                    {
                        Note = string.Format("{3} Send Email {0} [{1}] to {2} [{4}]", notifyNewRec.Subject, Lookup.Training_Email_NotifyNewRegistration, notifyNewRec.To, msgSendEmail, registration.FullName),
                        TrainingId = registration.TrainingId,
                        TrainingRegistrationId = registration.TrainingRegistrationId,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    _context.Add(logEmailBackOffice);
                    await _context.SaveChangesAsync();

                    // Conferma 
                    TempData["MsgToLayout"] = "Registration Successful. Please check your email for a confirmation.";
                    if (!User.Identity.IsAuthenticated)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    return RedirectToAction("Index", "Home");

                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            TrainingRegistrationDTO dto = new TrainingRegistrationDTO(); 
            PropertyCopier<TrainingRegistration, TrainingRegistrationDTO>.Copy(registration, dto); // Copio proprietÓ con stesso nome
            dto.countries = _context.Countries.OrderBy(t => t.CountryId).ToList();
            dto.trainings = _context.V_Trainings.Where(t => t.Status.Equals(Lookup.Training_Status_PUBLISHED))
                               .OrderBy(t => t.PeriodFrom).ThenBy(t => t.Title).ThenBy(t => t.TrainingId).ToList();
            dto.typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList();
            dto.jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList();
            dto.typesOfParticipantITA = _context.TrainingTypeOfParticipants.Where(t => t.Italian == true).OrderBy(t => t.Id).ToList();

            return View(dto);
        }

        // GET: Trainings/AddParticipantDL
        [AllowAnonymous]
        public IActionResult AddParticipantDL()
        {
            AddParticipantDLDTO dto = new AddParticipantDLDTO()
            {
                Status = Lookup.TrainingRegistration_Status_NEW,
                countries = _context.Countries.OrderBy(t => t.CountryId).ToList(),
                typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList(),
                jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList()
            };

            return View(dto);
        }

        // POST: Trainings/AddParticipantDL/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> AddParticipantDL(
                [Bind("Title,Name,Surname,EmailAddress,MobileNumber,CompanyName,CompanyAddress,CompanyAddressNr,CompanyAddressCity,CompanyAddressZipCode,CountryId,JobPosition,TypeOfParticipant")] TrainingRegistration registration)
        {
            registration.TrainingId = GetIdDistributionList();
            if (registration.TrainingId == 0)
            {
                ModelState.AddModelError("", "Distribution List Not Defined!!");
            }
            if (ModelState.IsValid)
            {
                try
                {
                    registration.Status = Lookup.TrainingRegistration_Status_ACCEPTED;
                    registration.VISA = Lookup.TrainigVISA.no;
                    registration.SendNews = true;
                    registration.UserLastUpd = User.Identity.Name;
                    registration.TmstLastUpd = DateTime.Now;
                    registration.TmstCreate = DateTime.Now;
                    _context.Add(registration);

                    TrainingLog log = new TrainingLog
                    {
                        Note = string.Format("Create New Registration - {0} {1} {2}", registration.Name, registration.Surname, registration.EmailAddress),
                        TrainingId = registration.TrainingId,
                        TrainingRegistrationId = registration.TrainingRegistrationId,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    _context.Add(log);
                    await _context.SaveChangesAsync();

                    // Conferma 
                    TempData["MsgToLayout"] = "Registration Successful.";
                    return RedirectToAction("DistributionList");

                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            AddParticipantDLDTO dto = new AddParticipantDLDTO();
            PropertyCopier<TrainingRegistration, AddParticipantDLDTO>.Copy(registration, dto); // Copio proprietÓ con stesso nome
            dto.countries = _context.Countries.OrderBy(t => t.CountryId).ToList();
            dto.typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList();
            dto.jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList();

            return View(dto);
        }

        // GET: Trainings/EditRegistration
        [AllowAnonymous]
        public IActionResult EditRegistration(long? TrainingRegistrationId)
        {

            if (TrainingRegistrationId == null)
            {
                return NotFound();
            }
            TrainingRegistration registration = _context.TrainingRegistrations
                                                        .Include(t => t.Training)
                                                        .FirstOrDefault(t => t.TrainingRegistrationId == TrainingRegistrationId);
            if (registration == null)
            {
                return NotFound();
            }
            TrainingRegistrationDTO dto = new TrainingRegistrationDTO();
            PropertyCopier<TrainingRegistration, TrainingRegistrationDTO>.Copy(registration, dto); // Copio proprietÓ con stesso nome<>
            dto.countries = _context.Countries.OrderBy(t => t.CountryId).ToList();
            dto.typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList();
            dto.jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList();

            if (!String.IsNullOrWhiteSpace(registration.CompanyName))
            {
                if (!_context.Companies.Any(e => e.CompanyId == registration.CompanyName))
                {
                    ModelState.AddModelError("CompanyName", "Company Name not found in normalized list");
                }
            }

            return View(dto);
        }

        // POST: Trainings/EditRegistration/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> EditRegistration(IFormFile file, bool RemoveFile,long TrainingRegistrationId)
        {
            bool bUploadFile = false;
            string file2remove = "";

            TrainingRegistration registration = _context.TrainingRegistrations
                                                        .Include(t => t.Training)
                                                        .FirstOrDefault(t => t.TrainingRegistrationId == TrainingRegistrationId);
            if (registration == null)
            {
                return NotFound();
            }

            if (await TryUpdateModelAsync<TrainingRegistration>(registration))
            {
                var context = new ValidationContext(registration, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(registration, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (RemoveFile)
                    {
                        if (!String.IsNullOrWhiteSpace(registration.PassportScan) && registration.PassportScanExist)
                        {
                            file2remove = registration.PassportScan;
                        }
                        registration.PassportScan = null;
                    }
                    else
                    {
                        if (file != null && file.Length > 0)
                        {
                            if (!String.IsNullOrWhiteSpace(registration.PassportScan) && registration.PassportScanExist)
                            {
                                file2remove = registration.PassportScan;
                            }
                            registration.PassportScan = file.FileName;
                            bUploadFile = true;
                        }
                    }

                    registration.UserLastUpd = User.Identity.Name;
                    registration.TmstLastUpd = DateTime.Now;
                    _context.Update(registration);
                    //TrainingLog log = new TrainingLog
                    //{
                    //    Note = string.Format("Registration updated ! {0} {1} {2} (id.{3})", registration.Name, registration.Surname, registration.EmailAddress, registration.TrainingRegistrationId),
                    //    TrainingId = registration.TrainingId,
                    //    TrainingRegistrationId = registration.TrainingRegistrationId,
                    //    UserLastUpd = User.Identity.Name,
                    //    TmstLastUpd = DateTime.Now
                    //};
                    //_context.Add(log);
                    await _context.SaveChangesAsync();
                    if (bUploadFile)
                    {
                        UploadPassportScan(registration.TrainingRegistrationId, file);
                    }
                    if (!string.IsNullOrWhiteSpace(file2remove) && !file2remove.Equals(registration.PassportScanFullFileName))
                    {
                        System.IO.File.Delete(file2remove);
                    }

                    // Invio Email di notifica Completamento/Rifiuto solo la prima volta !!! 
                    if (registration.tmstAnswerRegistration == null)
                    {
                        string idEmail = "";
                        string[] attach = null;
                        bool allegaAgenda = false;
                        bool allegaIndicazioni = false;
                        int idxAttach = -1;
                        if (registration.Status.Equals(Lookup.TrainingRegistration_Status_ACCEPTED))
                        {
                            idEmail = Lookup.Training_Email_ConfRegistration;
                            allegaAgenda = true;
                            allegaIndicazioni = true;
                        }
                        if (registration.Status.Equals(Lookup.TrainingRegistration_Status_REFUSED))
                        {
                            idEmail = Lookup.Training_Email_RefuseRegistration;
                        }

                        if (allegaAgenda && registration.Training != null)
                        {
                            if (registration.Training.AgendaExist)
                            {
                                //Array.Resize(ref attach, 1);
                                //attach[0] = registration.Training.AgendaFullFileName;
                                idxAttach++;
                                Array.Resize(ref attach, idxAttach+1);
                                attach[idxAttach] = registration.Training.AgendaFullFileName;
                            }
                        }
                        if (allegaIndicazioni && !String.IsNullOrWhiteSpace(registration.Training.Location))
                        {
                            TrainingLocation location = _context.TrainingLocations.Where(t => t.Id == registration.Training.Location).FirstOrDefault();
                            if (location != null)
                            {
                                if (location.IndicationsExist)
                                {
                                    idxAttach++;
                                    Array.Resize(ref attach, idxAttach + 1);
                                    attach[idxAttach] = location.IndicationsFullFileName;
                                }
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(idEmail))
                        {
                            string msgSendEmail = "[OK]";
                            EmailTemplate email = GetEmailInfo(registration.TrainingRegistrationId, idEmail);
                            if (!string.IsNullOrWhiteSpace(email.To))
                            {
                                var ret = _emailSender.SendEmailAsync(email.To, email.Subject, email.Body, attach, "Training");
                                if (ret.IsFaulted) msgSendEmail = "*** ERROR SendEmailAsync() ****";
                            } else
                            {
                                msgSendEmail = "*** No TO Address ***";
                            }
                            TrainingLog logEmail = new TrainingLog
                            {
                                Note = string.Format("{3} Send Email {0} [{1}] to {2} [{4}]", email.Subject, idEmail, email.To, msgSendEmail, registration.FullName),
                                TrainingId = registration.TrainingId,
                                TrainingRegistrationId = registration.TrainingRegistrationId,
                                UserLastUpd = User.Identity.Name,
                                TmstLastUpd = DateTime.Now
                            };
                            _context.Add(logEmail);

                            registration.tmstAnswerRegistration = DateTime.Now;
                            _context.Update(registration);

                            await _context.SaveChangesAsync();
                        }
                    }

                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }

            TrainingRegistrationDTO dto = new TrainingRegistrationDTO();
            PropertyCopier<TrainingRegistration, TrainingRegistrationDTO>.Copy(registration, dto); // Copio proprietÓ con stesso nome<>            
            dto.countries = _context.Countries.OrderBy(t => t.CountryId).ToList();
            dto.typesOfParticipant = _context.TrainingTypeOfParticipants.OrderBy(t => t.Id).ToList();
            dto.jobPositions = _context.TrainingJobPositions.OrderBy(t => t.Id).ToList();

            return View(dto);
        }

        // GET: Trainings/RemoveRegistration
        public IActionResult RemoveRegistration(long TrainingRegistrationId)
        {
            TrainingRegistration registration = _context.TrainingRegistrations
                                                        .Include(t => t.Training)
                                                        .FirstOrDefault(t => t.TrainingRegistrationId == TrainingRegistrationId);
            if (registration == null)
            {
                return NotFound();
            }
            // Rimozione riferimenti alla registrazione cancellata
            string sql = @"update TrainingLogs set TrainingRegistrationId=NULL where TrainingId={0} and TrainingRegistrationId={1}";
            List<Object> sqlParamsList = new List<object>();
            sqlParamsList.Add(registration.TrainingId);
            sqlParamsList.Add(registration.TrainingRegistrationId);
            _context.Database.ExecuteSqlCommand(sql, sqlParamsList.ToArray());
            // cancellazione registrazione 
            _context.TrainingRegistrations.Remove(registration);
            // Log
            TrainingLog logRemove = new TrainingLog
            {
                Note = string.Format("Remove {0}", registration.FullName),
                TrainingId = registration.TrainingId,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            _context.Add(logRemove);
            _context.SaveChanges();
            TempData["MsgToNotify"] = "Registration was removed successfully!";
            return RedirectToAction("Index");

        }

        //// GET: Trainings/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var training = await _context.Trainings.SingleOrDefaultAsync(m => m.TrainingId == id);
        //    if (training == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["HotelId"] = new SelectList(_context.Hotels, "HotelId", "Description", training.HotelId);
        //    ViewData["TrainingTypeId"] = new SelectList(_context.TrainingTypes, "TrainingTypeId", "Description", training.TrainingTypeId);
        //    return View(training);
        //}

        //// POST: Trainings/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("TrainingId,Title,PeriodFrom,PeriodTo,Location,Trainers,TrainingTypeId,HotelId,Status,Note,Agenda,Reserved,UserLastUpd,TmstLastUpd")] Training training)
        //{
        //    if (id != training.TrainingId)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(training);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!TrainingExists(training.TrainingId))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    ViewData["HotelId"] = new SelectList(_context.Hotels, "HotelId", "Description", training.HotelId);
        //    ViewData["TrainingTypeId"] = new SelectList(_context.TrainingTypes, "TrainingTypeId", "Description", training.TrainingTypeId);
        //    return View(training);
        //}

        //// GET: Trainings/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var training = await _context.Trainings
        //        .Include(t => t.Hotel)
        //        .Include(t => t.TrainingType)
        //        .SingleOrDefaultAsync(m => m.TrainingId == id);
        //    if (training == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(training);
        //}

        //// POST: Trainings/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var training = await _context.Trainings.SingleOrDefaultAsync(m => m.TrainingId == id);
        //    _context.Trainings.Remove(training);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        private bool TrainingExists(int id)
        {
            return _context.Trainings.Any(e => e.TrainingId == id);
        }

        // GET:Trainings/GetTrainingRegistratinJSON
        public async Task<IActionResult> GetTrainingRegistratinJSON(long TrainingRegistrationId)
        {
            TrainingRegistration data = _context.TrainingRegistrations.Find(TrainingRegistrationId);
            return Json(data);
        }

        // GET:Trainings/GetEmailInfoJSON
        public async Task<IActionResult> GetEmailInfoJSON(long TrainingRegistrationId = 0, string EmailTemplateId = "", long TrainingId = 0)
        {
            EmailTemplate data = GetEmailInfo(TrainingRegistrationId, EmailTemplateId, TrainingId);
            return Json(data);
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult CtrlTrainingRegistrationJson(string CompanyName, string MobileNumber, string CountryId, long? TrainingRegistrationId = 0)
        {
            List<string> myErr = new List<string>();
            List<string> myWarn = new List<string>();
            string callingCode = "";

            if (string.IsNullOrWhiteSpace(CountryId))
            {
                myErr.Add(string.Format("The Country field is required."));
            } else
            {
                callingCode = _context.Countries.Find(CountryId).CallingCode;
            }
            if (!string.IsNullOrWhiteSpace(callingCode))
            {
                if (!MobileNumber.StartsWith(callingCode))
                {
                    myWarn.Add(string.Format("The international prefix is different from that set for the country ({0})", callingCode));
                }
            }
            if (!MobileNumber.StartsWith("+")) {
                myErr.Add("Please include the value '+' at the start of the international prefix (i.e.Italy + 39)");
            }
            if (TrainingRegistrationId != 0 && !String.IsNullOrWhiteSpace(CompanyName))
            {
                if (!_context.Companies.Any(e => e.CompanyId == CompanyName))
                {
                    myWarn.Add(string.Format("Company Name not found in normalized list"));
                }
            }
            return Json(new { error = string.Join("<br>", myErr), warning = string.Join("<br>", myWarn) });
        }

        [AllowAnonymous]
        // Restituisce anche il destinatario previsto (Participant o BackOffice o Hotel)
        public EmailTemplate GetEmailInfo(long TrainingRegistrationId = 0, string EmailTemplateId = "", long TrainingId = 0)
        {
            string valore = "";
            Dictionary<string, string> valori = new Dictionary<string, string>();
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };
            TrainingRegistration registration = _context.TrainingRegistrations
                                                        .FirstOrDefault(t => t.TrainingRegistrationId == TrainingRegistrationId);
            Training training = new Training();
            if (registration != null)
            {
                training = _context.Trainings.Include(t => t.Hotel).Include(t => t.TrainingType)
                                                      .FirstOrDefault(t => t.TrainingId == registration.TrainingId);
            } else
            {
                // Da PARAMETRO (se passato)
                training = _context.Trainings.Include(t => t.Hotel).Include(t => t.TrainingType)
                                      .FirstOrDefault(t => t.TrainingId == TrainingId);
            }
          
            
            if (EmailTemplateId.Equals(Lookup.Training_Email_HOTEL))
            {
                if (registration != null)
                {
                    retEmail.EmailTemplateId = EmailTemplateId;
                    if (training != null)
                    {
                        retEmail.Subject = training.Hotel.ConfirmationSubject;
                        retEmail.Body = training.Hotel.ConfirmationBody;
                    }
                }
            }
            else
            {
                EmailTemplate email = _context.EmailTemplates.Find(EmailTemplateId);
                retEmail.EmailTemplateId = EmailTemplateId;
                if (email == null)
                {
                    retEmail.Subject = String.Format("Email {0} Not Found", EmailTemplateId);
                }
                else
                {
                    retEmail.Subject = email.Subject;
                    retEmail.Body = email.Body;
                    if (registration != null)
                    {
                        retEmail.To = registration.EmailAddress ?? "";
                    }
                    
                }
            }

            if (registration != null)
            {
                if (EmailTemplateId.Equals(Lookup.Training_Email_HOTEL))
                {
                    if (training != null)
                    {
                        retEmail.To = training.Hotel.EmailAddress;
                    }
                } else if (EmailTemplateId.Equals(Lookup.Training_Email_NotifyNewRegistration))
                {
                    var backoffice = (new UsersBO()).Get(filterRoleName: Lookup.Role_TrainingNotification);
                    if (backoffice.Count() > 0)
                    {
                        retEmail.To = string.Join(",", backoffice.Select(t => t.userEmail).ToList());
                    }
                } else
                {
                    retEmail.To = registration.EmailAddress;
                }
            }

            if (registration != null)
            {
                // sostituzione sezioni Start/End 
                // ricerca START e END ed estrai sezione di codice completa (originale)
                // se da nascondere, rimuovi tutto il contenuto, altrimenti solo i bookmark
                // sostituisci al testo originale
                List<cfgSezione> arSezioni = new List<cfgSezione>()
                {
                    new cfgSezione { start = "$PAID_EGSPA_START$", end = "$PAID_EGSPA_END$", visible = (registration.hotelPaid??false) },
                    new cfgSezione { start = "$PAID_PARTICIPANT_START$", end = "$PAID_PARTICIPANT_END$", visible = !(registration.hotelPaid??false) },
                    new cfgSezione { start = "$VISA_START$", end = "$VISA_END$", visible = (registration.VISA == Lookup.TrainigVISA.yes_with_letter || registration.VISA == Lookup.TrainigVISA.yes_no_letter) },
                    new cfgSezione { start = "$NO_VISA_START$", end = "$NO_VISA_END$", visible = (registration.VISA == Lookup.TrainigVISA.no) }
                };

                if (!String.IsNullOrWhiteSpace(retEmail.Body))
                {
                    foreach (var p in arSezioni)
                    {
                        // gestione sezioni multiple
                        while (true)
                        {
                            int nStart = retEmail.Body.IndexOf(p.start);
                            int nEnd = retEmail.Body.IndexOf(p.end, nStart + 1);
                            if (nStart == -1 || nEnd == -1)
                            {
                                break;
                            }
                            string source = retEmail.Body.Substring(nStart, (nEnd - nStart + p.end.Length));
                            string target = p.visible ? source : "";
                            target = target.Replace(p.start, "");
                            target = target.Replace(p.end, "");
                            retEmail.Body = retEmail.Body.Replace(source, target);
                        }
                    }

                }
            }

            // Sostituzioni per TRAINING 
            string allValues = "";
            if (training != null) {
                // Sostituzione valori 
                valori.Add("$TITLE$", training.Title);
                valori.Add("$LOCATION$", training.Location);
                valori.Add("$HOTEL$", training.Hotel?.Description ?? "No Hotel");
                valori.Add("$HOTELADDRESS$", training.Hotel?.Address ?? "No Hotel");
                valori.Add("$HOTELDETAILS$", training.Hotel?.Details ?? "No Hotel");
                valori.Add("$PERIOD$", string.Format("{0} - {1}", training.PeriodFromDeco, training.PeriodToDeco));
                valori.Add("$FROM$", training.PeriodFromDeco);
                valori.Add("$TO$", training.PeriodToDeco);

                allValues += string.Format("Training: {0}\n\r", training.Title);
                allValues += string.Format("Location: {0}\n\r", training.Location);
                allValues += string.Format("Period From: {0}\n\r", training.PeriodFromDeco);
                allValues += string.Format("Period To: {0}\n\r", training.PeriodToDeco);

            }
            // Sostituzioni per REGISTRAZIONE
            if (registration != null) {
                valori.Add("$NAME$", registration.FullName);
                valori.Add("$CHECKIN$", registration.CheckInDeco);
                valori.Add("$CHECKOUT$", registration.CheckOutDeco);
                valori.Add("$HOTELRESERVATIONNUMBER$", registration.ReservationNumber);

                // Riepilogo dati di registrazione 
                allValues += string.Format("Title: {0}\n\r", registration.Title);
                allValues += string.Format("Name: {0}\n\r", registration.Name);
                allValues += string.Format("Surname: {0}\n\r", registration.Surname);
                allValues += string.Format("Email Address: {0}\n\r", registration.EmailAddress);
                allValues += string.Format("Country: {0}\n\r", registration.CountryId);
                allValues += string.Format("Mobile Number: {0}\n\r", registration.MobileNumber);
                if (!String.IsNullOrWhiteSpace(registration.CompanyName))
                {
                    allValues += string.Format("Company Name: {0}\n\r", registration.CompanyName);
                }
                if (!String.IsNullOrWhiteSpace(registration.FullCompanyAddress))
                {
                    allValues += string.Format("Company Address: {0}\n\r", registration.FullCompanyAddress);
                }
                if (!String.IsNullOrWhiteSpace(registration.JobPosition))
                {
                    allValues += string.Format("Job Position: {0}\n\r", registration.JobPosition);
                }
                allValues += string.Format("VISA: {0}\n\r", registration.VISAStatus);
                if (training != null)
                {
                    if (training.HotelId != null)
                    {
                        if (!(bool)registration.Accomodation)
                        {
                            allValues += string.Format("Accomodation: {0}\n\r", "Not Required");
                        }
                        else
                        {
                            allValues += string.Format("Accomodation: {0}\n\r", "Required");
                            allValues += string.Format("Check-In: {0}\n\r", registration.CheckInDeco);
                            allValues += string.Format("Check-Out: {0}\n\r", registration.CheckOutDeco);
                        }
                    }
                }
            }
            valori.Add("$REGISTRATION$", allValues);

            foreach (var item in valori)
            {
                valore = "";
                if (!string.IsNullOrWhiteSpace(item.Value))
                {
                    valore = item.Value.Replace("\n\r", "<br/>").Replace("\n", "<br/>").Replace("\r", "<br/>");
                }
                if (!String.IsNullOrWhiteSpace(retEmail.Subject))
                {
                    retEmail.Subject = retEmail.Subject.Replace(item.Key, valore);
                }
                if (!String.IsNullOrWhiteSpace(retEmail.Body))
                {
                    retEmail.Body =  retEmail.Body.Replace(item.Key, valore);
                }
            }

            return retEmail;
        }

        [HttpGet]
        public IActionResult RemoveFromDistributionList(long TrainingRegistrationId)
        {
            IActionResult ret = Ok("OK");
            var registration = _context.TrainingRegistrations.Find(TrainingRegistrationId);
            if (registration != null)
            {
                registration.SendNews = false;
                _context.SaveChanges();
            }
            return ret;
        }

        //[HttpGet]
        //public IActionResult ActiveParticipant(long TrainingRegistrationId, bool newValue)
        //{
        //    IActionResult ret = Ok("OK");
        //    var registration = _context.TrainingRegistrations.Find(TrainingRegistrationId);
        //    if (registration != null)
        //    {
        //        registration.Enabled = newValue;
        //        _context.SaveChanges();
        //    }
        //    return ret;
        //}

        [HttpPut]
        public IActionResult UpdateParticipant(long key, string values)
        {
            var rec2Update = _context.TrainingRegistrations.Find(key);
            try
            {
                //JsonConvert.PopulateObject(values, rec2Update);
                //if (!TryValidateModel(rec2Update))
                //{
                //    string messages = string.Join(" ", ModelState.Values
                //                        .SelectMany(x => x.Errors)
                //                        .Select(x => x.ErrorMessage));
                //    return BadRequest(messages);
                //}
                TrainingRegistration newVal = JsonConvert.DeserializeObject<TrainingRegistration>(values);
                rec2Update.Enabled = newVal.Enabled ?? rec2Update.Enabled;
                if (rec2Update.Enabled == false)
                {
                    rec2Update.SendNews = false;
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult SendEmailRegistration([FromBody]EmailTemplate DTO)
        {

            string msgException = "";

            TrainingRegistration registration = _context.TrainingRegistrations
                                            .Include(t => t.Training).ThenInclude(t=>t.Hotel)
                                            .FirstOrDefault(t => t.TrainingRegistrationId == DTO.IdRif);
            if (registration == null)
            {
                return NotFound();
            }

            // Aggiorna DATA e ALLEGATI
            // se email a Hotel --> tmstSendReservationHotel
            // se email conferma prenotazione a richiedente --> tmstReservationHotelConfirmed
            // se Invio Invitation letter --> tmstSendInvitationLetter
            // NOTA: le date vengono aggiornate SOLAMENTE se l'invio va a buon fine (potrebbe essere disabled !!!)
            string msgSendEmail = "[OK]";
            string[] attach = null;
            if (DTO.EmailTemplateId.Equals(Lookup.Training_Email_ConfHotel))
            {
                // Attach: CCAuthorization
                if (registration.Training.Hotel != null )
                {
                    if (registration.Training.Hotel.CCAuthorizationExist)
                    {
                        Array.Resize(ref attach, 1);
                        attach[0] = registration.Training.Hotel.CCAuthorizationFullFileName;
                    }
                }

            }
            if (DTO.EmailTemplateId.Equals(Lookup.Training_Email_InvitationLetter))
            {
                // Attach: scansione Invitation Letter
                if (registration.InvLetterScanExist)
                {
                    Array.Resize(ref attach, 1);
                    attach[0] = registration.InvLetterScanFullFileName;
                }
            }

            var retSend = _emailSender.SendEmailAsync(DTO.To, DTO.Subject, DTO.Body, attach, "Training");
            if (retSend.IsFaulted)
            {
                msgSendEmail = "*** ERROR SendEmailAsync() ****";
                if (retSend.Exception != null || retSend.Exception.InnerException  != null)
                {
                    msgException = retSend.Exception.InnerException.Message;
                }
            }
            TrainingLog logEmail = new TrainingLog
            {
                Note = string.Format("{3} Send Email {0} [{1}] to {2} [{4}]", DTO.Subject, DTO.EmailTemplateId, DTO.To, msgSendEmail, registration.FullName),
                TrainingId = registration.TrainingId,
                TrainingRegistrationId = registration.TrainingRegistrationId,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            if (!string.IsNullOrWhiteSpace(msgException))
            {
                logEmail.Note = string.Format("{0} - Exception Message: {1}", logEmail.Note, msgException);
            }
            _context.Add(logEmail);

            // Aggiorna Date/TMST su registrazione per gestione stati (se invio ok)
            if (string.IsNullOrWhiteSpace(msgException))
            {
                if (DTO.EmailTemplateId.Equals(Lookup.Training_Email_HOTEL))
                {
                    registration.tmstSendReservationHotel = DateTime.Now;
                }
                if (DTO.EmailTemplateId.Equals(Lookup.Training_Email_ConfHotel))
                {
                    registration.tmstReservationHotelConfirmed = DateTime.Now;
                }
                if (DTO.EmailTemplateId.Equals(Lookup.Training_Email_InvitationLetter))
                {
                    registration.tmstSendInvitationLetter = DateTime.Now;
                }
                //_context.Update(registration);
            }
            _context.SaveChanges();

            if (!msgSendEmail.Equals("[OK]"))
            {
                return BadRequest(string.Format("{0} - Exception Message: {1}", msgSendEmail, msgException));
            }

            return Json("");
        }

        [HttpPost]
        public IActionResult SendGenericEmail([FromBody]EmailTemplate DTO)
        {

            string msgException = "";
            string msgSendEmail = "[OK]";
            Training training = _context.Trainings.FirstOrDefault(t => t.TrainingId == DTO.IdRif);
            if (training == null)
            {
                return NotFound();
            }

            var retSend = _emailSender.SendEmailAsync(DTO.To, DTO.Subject, DTO.Body, context: "Training");
            if (retSend.IsFaulted)
            {
                msgSendEmail = "*** ERROR SendEmailAsync() ****";
                if (retSend.Exception != null || retSend.Exception.InnerException != null)
                {
                    msgException = retSend.Exception.InnerException.Message;
                }
            }
            TrainingLog logEmail = new TrainingLog
            {
                Note = string.Format("Send Email '{0}' to {1} [{2}]", DTO.Subject, DTO.To, msgSendEmail),
                TrainingId = training.TrainingId,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            if (!string.IsNullOrWhiteSpace(msgException))
            {
                logEmail.Note = string.Format("{0} - Exception Message: {1}", logEmail.Note, msgException);
            }
            _context.Add(logEmail);
            _context.SaveChanges();

            if (!msgSendEmail.Equals("[OK]"))
            {
                return BadRequest(string.Format("{0} - Exception Message: {1}", msgSendEmail, msgException));
            }

            return Json("");
        }

        [HttpPost]
        public object SetRegistrationNumber(long id, string ReservationNumber)
        {
            IActionResult ret = Ok("OK");
            TrainingRegistration registration = _context.TrainingRegistrations
                                            .Include(t => t.Training)
                                            .FirstOrDefault(t => t.TrainingRegistrationId == id);
            if (registration == null)
            {
                return NotFound();
            }
            registration.ReservationNumber = ReservationNumber;
            if (registration.ReservationNumber.Length > 50)
                registration.ReservationNumber = registration.ReservationNumber.Substring(0, 50);
            TrainingLog logrec = new TrainingLog
            {
                Note = string.Format("Set Reservation Number: {0} [{1}]", registration.ReservationNumber, registration.FullName),
                TrainingId = registration.TrainingId,
                TrainingRegistrationId = registration.TrainingRegistrationId,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            _context.Add(logrec);
            _context.SaveChanges();
            return ret;
        }

        [HttpPost]
        public ActionResult UploadInvLetterScan(long TrainingRegistrationId)
        {
            try
            {
                TrainingRegistration registration = _context.TrainingRegistrations
                                                .Include(t => t.Training)
                                                .FirstOrDefault(t => t.TrainingRegistrationId == TrainingRegistrationId);
                if (registration == null)
                {
                    Response.StatusCode = 500;
                }

                var myFile = Request.Form.Files["myFileInvLetter"];
                registration.InvLetterScan = myFile.FileName;

                var fullFileName = registration.InvLetterScanFullFileName;
                if (!Directory.Exists(registration.InvLetterScanPathName))
                {
                    Directory.CreateDirectory(registration.InvLetterScanPathName);
                }
                using (var fileStream = System.IO.File.Create(fullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
                TrainingLog logrec = new TrainingLog
                {
                    Note = string.Format("Upload Invitation Letter [{0}]", registration.FullName),
                    TrainingId = registration.TrainingId,
                    TrainingRegistrationId = registration.TrainingRegistrationId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                _context.Add(logrec);
                _context.SaveChanges();
            }
            catch
            {
                Response.StatusCode = 400;
            }

            return new EmptyResult();
        }

        // GET: Training/TrainingLog/5
        public ActionResult TrainingJournal(int TrainingId)
        {
            IEnumerable<TrainingLog> dati = _context.TrainingLogs.Include(t => t.TrainingRegistration).Where(t => t.TrainingId == TrainingId)
                                                                 .OrderBy(t=>t.TmstLastUpd).ToList();
            return PartialView("_TrainingJournal", dati);
        }

        private int GetIdDistributionList()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
            int ret = 0;
            try
            {
                ret = Int32.Parse(configurationBuilder.Build().GetSection("Training:idDistributionList").Value);
            } 
            catch (Exception e) { }
            return ret;
        }

    }
}




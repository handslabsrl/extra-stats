using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DevExtreme.AspNet.Data;
using WebApp.Entity;
using WebApp.Repository;
using WebApp.Data;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Reflection;
using WebApp.Models;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebApp.Classes;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Assays Protocols")]
    public class AssaysProtocolsController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;

        public AssaysProtocolsController(WebAppDbContext context, ILogger<AssaysProtocolsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(AssaysProcotolsIndexDTO dto, string opt)
        {
            string idCache = "AssaysProtocolsFilters";
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            if (!string.IsNullOrWhiteSpace(UserId))
            {
                if (dto.countriesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                    if (valori.Count == 1)
                    {
                        dto.countriesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.customersSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                    if (valori.Count == 1)
                    {
                        dto.customersSel = new List<string> { valori[0] };
                    }
                }
                if (dto.commercialEntitiesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                    if (valori.Count == 1)
                    {
                        dto.commercialEntitiesSel = new List<string> { valori[0] };
                    }
                }

                UserCfg userCfg = _context.UserCfg.Find(UserId);
                if (userCfg != null)
                {
                    if (!String.IsNullOrWhiteSpace(userCfg.Region))
                    {
                        dto.Region = userCfg.Region;
                        dto.lockRegion = true;
                    }
                    if (!String.IsNullOrWhiteSpace(userCfg.Area))
                    {
                        dto.Area = userCfg.Area;
                        dto.lockArea = true;
                    }
                }
            }

            // nel caso di EXPORT recupero gli ultimi filtri dalla sessione in modo da avere esattamente gli stessi filtri 
            // usati per l'ultima ricerca; attenzione: la cache dura 30minuti !! 
            // opt <> Null indica la richiesta di EXPORT  (forse!)
            if (!String.IsNullOrWhiteSpace(opt))
            {
                try
                {
                    dto = JsonConvert.DeserializeObject<AssaysProcotolsIndexDTO>(HttpContext.Session.GetString(idCache));
                }
                catch
                {
                    // Nessuna azione 
                }
            }

            if (dto.DateFrom == DateTime.MinValue)
            {
                dto.DateFrom = DateTime.Now.AddDays(-31);
            }
            if (dto.DateTo == DateTime.MinValue)
            {
                dto.DateTo = DateTime.Now;
            }

            // ESTRAZIONE DATI 
            AssaysProtocolsModel ext = (new AssaysBO()).GetAssaysProtocols(dto: dto, UserId: UserId);

            if (!String.IsNullOrWhiteSpace(opt))
            {
                if (opt.ToLower().Equals("export"))
                {
                    return File(ExportAssaysProtocols(ext),
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("AssaysProtocols_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                }
            }

            // Salvataggio filtri 
            HttpContext.Session.SetString(idCache, JsonConvert.SerializeObject(dto));
            dto.regions = (new RegionsBO()).GetRegions(UserIdCfg: UserId);
            dto.areas = (new AreasBO()).GetAreas(UserIdCfg: UserId);
            dto.commercialStatuses = (new CommercialStatusBO()).Get();
            dto.Pathogens = (new AssaysBO()).GetPathogens();
            dto.Assays = (new AssaysBO()).GetNames();

            dto.countries = (new CountriesBO()).GetCountries(UserIdCfg: UserId);
            dto.lockCountry = false; 
            if(dto.countries.Count() == 1)
            {
                dto.lockCountry = true;
                dto.countriesSel = new List<string>() { dto.countries.FirstOrDefault().Country } ;
            }

            dto.commercialEntities = (new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId);
            dto.lockCommerialEntity = false;
            if (dto.commercialEntities.Count() == 1)
            {
                dto.lockCommerialEntity = true;
                dto.commercialEntitiesSel = new List<string>() { dto.commercialEntities.FirstOrDefault().CommercialEntity };
            }

            dto.lockCustomer = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer)).Count() == 1;

            // Serie e risutati 

            // Serie e risultati 
            dto.details = ext.AssaysProtocolsDetails;
            dto.summary = ext.filtersDesc;

            return View(dto);
        }

        public Stream ExportAssaysProtocols(AssaysProtocolsModel dati)
        {

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("AssaysProtocols");

            int posData = 1;
            int nRows = 0;
            int nColumns = 13;   // (typeof(T)).GetProperties().Length;

            // CARICAMENTO DATI
            int numColDeleted = 0;
            // DETTAGLIO 
            workSheet.Cells[posData, 1].LoadFromCollection(dati.AssaysProtocolsDetails, true);
            nRows = dati.AssaysProtocolsDetails.Count;

            // Filtri
            posData = 0;
            if (dati.filters != null)
            {
                // Inserisco tante righe vuote quanti sono i filtri da visualizzare + 1 riga vuota 
                workSheet.InsertRow(1, dati.filters.Count + 1);
                foreach (var f in dati.filters)
                {
                    posData++;
                    workSheet.Cells[posData, 1].Value = string.Format("{0}:", f.Key);
                    workSheet.Cells[posData, 2].Value = f.Value;
                }
                using (var range = workSheet.Cells[1, 1, posData, 2])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
            }

            // aggiungo una riga vuota
            posData++;
            workSheet.InsertRow(posData, 1);

            // Filtro automatico !!! 
            posData++;
            posData++;
            nColumns = nColumns - numColDeleted;
            workSheet.Cells[posData, 1, posData + nRows, nColumns].AutoFilter = true;

            // AUTO FIT Parto dal fondo (da destra) perch� vengono cancellate le colonne !!! 
            for (int i = 1; i <= nColumns; i++)
            {
                workSheet.Column(i).AutoFit();
            }
            using (var range = workSheet.Cells[posData, 1, posData, nColumns])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                range.Style.Font.Color.SetColor(Color.White);
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return stream;            //return newFile.FullName;
        }
    }
}
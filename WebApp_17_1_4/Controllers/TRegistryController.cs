using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
//using Microsoft.AspNetCore.Mvc.Localization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Text.RegularExpressions;
using NPoco;

namespace ELITeBoard.Controllers
{
    [Authorize(Roles = "Training BackOffice")]
    public class TRegistryController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        //private readonly IHtmlLocalizer<TRegistryController> _localizer;

        private static string connectionString;

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        //public TRegistryController(WebAppDbContext context, ILogger<TRegistryController> logger, IHtmlLocalizer<TRegistryController> localizer)
        public TRegistryController(WebAppDbContext context, ILogger<TRegistryController> logger)
        {
            _context = context;
            _logger = logger;
            //_localizer = localizer;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }

        }

        // ************************************************************************
        // Gestione COUNTRIES 
        // ************************************************************************
        public ActionResult ManageCountries()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetCountries(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Countries
                               .OrderBy(t => t.CountryId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetCountriesAutoComplete(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Countries.Select(t => t.CountryId).ToList();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(data, loadOptions)), "application/json");
        }

        private bool CountryExists(string CountryId)
        {
            return _context.Countries.Any(e => e.CountryId== CountryId);
        }

        [HttpPost]
        public ActionResult DeleteCountry(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                Country rec2Del = _context.Countries.Where(t => t.CountryId == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.Countries.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.CountryId);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.CountryId, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateCountry(string CountryId, string callingCode)
        {
            var ret = "";

            string newId = CountryId.ToUpper();
            if (!String.IsNullOrWhiteSpace(newId) && newId.Length > 100)
            {
                newId = newId.Substring(0, 100);
            }
            if (!String.IsNullOrWhiteSpace(callingCode) && callingCode.Length > 10)
            {
                callingCode = callingCode.Substring(0, 10);
            }
            if (CountryExists(newId)) { 
                ret = String.Format("Country already exists");
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                if (!String.IsNullOrWhiteSpace(callingCode))
                {
                    Regex regex = new Regex(@"^[0-9+\-\s]+$");
                    if (!regex.IsMatch(callingCode))
                    {
                        ret = String.Format("Calling Code not valid");
                    }
                    if (callingCode.Length > 10)
                    {
                        callingCode = callingCode.Substring(0, 10);
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                Country newRec = new Country()
                {
                    CountryId = newId, 
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    CallingCode = callingCode
                };
                try
                {
                    _context.Countries.Add(newRec);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    ret = String.Format("Create Country - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                }
            }

            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateCountry(string key, string values)
        {
            Country rec2Update = _context.Countries.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                if (!String.IsNullOrWhiteSpace(rec2Update.CallingCode)) {
                    Regex regex = new Regex(@"^[0-9+\-\s]+$");
                    if (!regex.IsMatch(rec2Update.CallingCode))
                    {
                        return BadRequest("Calling Code not valid");
                    }
                    if (rec2Update.CallingCode.Length > 10)
                    {
                        rec2Update.CallingCode = rec2Update.CallingCode.Substring(0, 10);
                    }
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione TRAININGTYPES
        // ************************************************************************
        public ActionResult ManageTrainingTypes()
        {
            return View();
        }

        [HttpGet]
        public object GetTrainingTypes(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingTypes.OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetTrainingTypesPortfolio(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingTypes.OrderBy(t => t.Description).Where(t=>t.viewTrainingPortfolio).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public ActionResult GetTrainingTypes2(DataSourceLoadOptions loadOptions)
        {
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(_context.TrainingTypes, loadOptions)), "application/json");
        }

        [HttpPost]
        public ActionResult DeleteTrainingType(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                TrainingType rec2Del = _context.TrainingTypes.Where(t => t.TrainingTypeId == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.TrainingTypes.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateTrainingType(string Description)
        {
            var ret = "";
            string newDesc = Description;
            if (!String.IsNullOrWhiteSpace(newDesc) && newDesc.Length > 200)
            {
                newDesc = newDesc.Substring(0, 200);
            }

            TrainingType newRec = new TrainingType()
            {
                Description = newDesc,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.TrainingTypes.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create TrainingType - Error: {0} - {1}", e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateTrainingType(int key, string values)
        {
            TrainingType rec2Update = _context.TrainingTypes.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateJobPosition(string key, string values)
        {
            TrainingJobPosition rec2Update = _context.TrainingJobPositions.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }

                if (!key.Equals(rec2Update.Id))
                {
                    if (!String.IsNullOrWhiteSpace(rec2Update.Id) && rec2Update.Id.Length > 100)
                    {
                        rec2Update.Id = rec2Update.Id.Substring(0, 100);
                    }
                    if (String.IsNullOrWhiteSpace(rec2Update.Id))
                    {
                        string messages = String.Format("The Job Position field is required");
                        return BadRequest(messages);
                    }

                    if (JobPositionExists(rec2Update.Id))
                    {
                        string messages = String.Format("Job Position already exists");
                        return BadRequest(messages);
                    }

                    // Modifica Chiave JOBPosition
                    List<object> args = new List<object>();
                    var Sql = @"UPDATE TrainingJobPositions 
                                   SET ID = @newID,
                                       TmstLastUpd = CONVERT(datetime, @TmstLastUpd, 103),
                                       UserLastUpd = @UserLastUpd
                                 WHERE ID = @oldID";
                    var SqlRegistration = @"UPDATE TrainingRegistrations 
                                               SET JobPosition = @newID
                                             WHERE JobPosition = @oldID";
                    args.Add(new { newID = rec2Update.Id });
                    args.Add(new { oldID = key });
                    args.Add(new { TmstLastUpd = DateTime.Now });
                    args.Add(new { UserLastUpd = User.Identity.Name });

                    try
                    {
                        using (IDatabase db = Connection)
                        {
                            db.Execute(Sql, args.ToArray());
                            db.Execute(SqlRegistration, args.ToArray());
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpPut]
        public IActionResult UpdateTypeOfParticipants(string key, string values)
        {
            TrainingTypeOfParticipant rec2Update = _context.TrainingTypeOfParticipants.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }

                if (!key.Equals(rec2Update.Id))
                {
                    if (!String.IsNullOrWhiteSpace(rec2Update.Id) && rec2Update.Id.Length > 100)
                    {
                        rec2Update.Id = rec2Update.Id.Substring(0, 100);
                    }
                    if (String.IsNullOrWhiteSpace(rec2Update.Id))
                    {
                        string messages = String.Format("The Type Of Participant field is required");
                        return BadRequest(messages);
                    }
                    if (TypeOfParticipantExists(rec2Update.Id))
                    {
                        string messages = String.Format("Type Of Participant already exists");
                        return BadRequest(messages);
                    }

                    // Modifica Chiave JOBPosition
                    List<object> args = new List<object>();
                    var Sql = @"UPDATE TrainingTypeOfParticipants 
                                   SET ID = @newID,
                                       ITALIAN = @italian, 
                                       TmstLastUpd = CONVERT(datetime, @TmstLastUpd, 103),
                                       UserLastUpd = @UserLastUpd
                                 WHERE ID = @oldID";
                    var SqlRegistration = @"UPDATE TrainingRegistrations 
                                               SET TypeOfParticipant = @newID
                                             WHERE TypeOfParticipant = @oldID";
                    args.Add(new { newID = rec2Update.Id });
                    args.Add(new { italian = rec2Update.Italian });
                    args.Add(new { oldID = key });
                    args.Add(new { TmstLastUpd = DateTime.Now });
                    args.Add(new { UserLastUpd = User.Identity.Name });
                    try
                    {
                        using (IDatabase db = Connection)
                        {
                            db.Execute(Sql, args.ToArray());
                            db.Execute(SqlRegistration, args.ToArray());
                        }
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                } else
                {
                    // aggiorna solo flag Italian 
                    rec2Update.UserLastUpd = User.Identity.Name;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [AllowAnonymous]
        public ActionResult downloadFile(int id, string fileType)
        {
            TrainingType trainingType = _context.TrainingTypes.Find(id);
            if (trainingType == null)
            {
                return NotFound(String.Format("Training Type not found !! (id: '{0}')", id));
            }

            bool fileExists = false;
            string fileDesc = "";
            string fileName = "";
            string fullFileName = "";
            switch (fileType)
            {
                case "C":       // Certificate Template
                    fileDesc = "Certificate Template";
                    fileExists = trainingType.CertificateTemplateExist;
                    fileName = trainingType.CertificateTemplateFileName;
                    fullFileName = trainingType.CertificateTemplateFullFileName;
                    break;
                case "A":       // Agenda
                    fileDesc = "Agenda";
                    fileExists = trainingType.AgendaExist;
                    fileName = trainingType.AgendaFileName;
                    fullFileName = trainingType.AgendaFullFileName;
                    break;
                case "P":       // Training Portfolio 
                    fileDesc = "Training Portfolio";
                    fileExists = trainingType.TrainingPortfolioExist;
                    fileName = trainingType.TrainingPortfolioFileName;
                    fullFileName = trainingType.TrainingPortfolioFullFileName;
                    break;
                default:
                    break;
            }
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return NotFound(String.Format("File Type '{0}' not valid !!", fileType));
            }

            if (!fileExists)
            {
                return NotFound(String.Format("{0} '{1}' not found !!", fileDesc, fileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(fullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = fileName };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadFile()", error));
                return BadRequest(error);
            }
        }

        [HttpPost]
        public ActionResult UploadFile(int TrainingTypeId, string fileType)
        {
            try
            {
                TrainingType trainingType = _context.TrainingTypes.Find(TrainingTypeId);
                if (trainingType == null)
                {
                    Response.StatusCode = 500;
                }

                var myFile = Request.Form.Files["myFile"];
                var fullFileName = "";
                var pathName = "";

                switch (fileType)
                {
                    case "C":       // Certificate Template
                        fullFileName = trainingType.CertificateTemplateFullFileName;
                        pathName = trainingType.CertificateTemplatePathName;
                        break;
                    case "A":       // Agenda
                        fullFileName = trainingType.AgendaFullFileName;
                        pathName = trainingType.AgendaPathName;
                        break;
                    case "P":       // Training Portfolio 
                        fullFileName = trainingType.TrainingPortfolioFullFileName;
                        pathName = trainingType.TrainingPortfolioPathName;
                        break;
                    default:
                        Response.StatusCode = 500;
                        break;
                }
                if (!Directory.Exists(pathName))
                {
                    Directory.CreateDirectory(pathName);
                }
                using (var fileStream = System.IO.File.Create(fullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }

            return new EmptyResult();
        }

        // ************************************************************************
        // Gestione HOTELS
        // ************************************************************************
        public ActionResult ManageHotels()
        {
            return View();
        }

        [HttpGet]
        public object GetHotels(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Hotels.OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        private bool HoteExists(int HotelId)
        {
            return _context.Hotels.Any(e => e.HotelId == HotelId);
        }

        // GET: TRegistry/CreateHotel
        public IActionResult CreateHotel()
        {
            return View();
        }

        // POST: TRegistry/CreateHotel
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateHotel(IFormFile file, [Bind("Description,Address,Details,Note,EmailAddress,ConfirmationSubject,ConfirmationBody")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.Length > 0)
                {
                    hotel.CCAuthorization = file.FileName;
                }
                hotel.UserLastUpd = User.Identity.Name;
                hotel.TmstLastUpd = DateTime.Now;
                _context.Add(hotel);
                await _context.SaveChangesAsync();
                if (file != null && file.Length > 0)
                {
                    uploadCCAuthorization(hotel.HotelId, file);
                }
                return RedirectToAction("ManageHotels");
            }
            return View(hotel);
        }

        // GET: TRegistry/EditHotel/5
        public async Task<IActionResult> EditHotel(int id)
        {

            var hotel = await _context.Hotels.SingleOrDefaultAsync(m => m.HotelId == id);
            if (hotel == null)
            {
                return NotFound();
            }
            return View(hotel);
        }

        //// POST: TRegistry/EditHotel/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditHotel(IFormFile file, bool RemoveFile, [Bind("HotelId,Description,Address,Details,Note,EmailAddress,ConfirmationSubject,ConfirmationBody,CCAuthorization")] Hotel hotel)
        {
            bool bUploadFile = false;
            string file2remove = "";
            if (!HoteExists(hotel.HotelId))
            { 
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (RemoveFile) {
                        if (!String.IsNullOrWhiteSpace(hotel.CCAuthorization) && hotel.CCAuthorizationExist)
                        {
                            file2remove = hotel.CCAuthorizationFullFileName;
                        }
                        hotel.CCAuthorization = "";
                    } else
                    {
                        if (file != null && file.Length > 0)
                        {
                            if (!String.IsNullOrWhiteSpace(hotel.CCAuthorization) && hotel.CCAuthorizationExist)
                            {
                                file2remove = hotel.CCAuthorizationFullFileName;
                            }
                            hotel.CCAuthorization = file.FileName;
                            bUploadFile = true;
                        }
                    }
                    hotel.UserLastUpd = User.Identity.Name;
                    hotel.TmstLastUpd = DateTime.Now;
                    _context.Update(hotel);
                    await _context.SaveChangesAsync();
                    if (bUploadFile)
                    {
                        uploadCCAuthorization(hotel.HotelId, file);
                    }
                    // Non rimuovo i file vecchi !!! 
                    //if (!string.IsNullOrWhiteSpace(file2remove) && !file2remove.Equals(hotel.CCAuthorizationFullFileName))
                    //{
                    //    System.IO.File.Delete(file2remove);
                    //}
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("ManageHotels");
            }

            return View(hotel);
        }

        private void uploadCCAuthorization(int hotelId, IFormFile file)
        {
            Hotel rec2upd = _context.Hotels.Find(hotelId);
            rec2upd.CCAuthorization = file.FileName;
            string path = rec2upd.CCAuthorizationPathName;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filename = rec2upd.CCAuthorizationFullFileName;
            using (FileStream fs = System.IO.File.Create(filename))
            {
                file.CopyTo(fs);
            }
            return;
        }

        [HttpPost]
        public ActionResult DeleteHotel(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                Hotel rec2Del = _context.Hotels.Where(t => t.HotelId == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.Hotels.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        
        public ActionResult downloadCCAuthorization(int  id)
        {
            Hotel hotel = _context.Hotels.Find(id);
            if (hotel == null) {
                return NotFound(String.Format("Hotel not found !! (id: '{0}')", id));
            }
            if (string.IsNullOrWhiteSpace(hotel.CCAuthorization))
            {
                return NotFound(String.Format("CCAuthorization not valid !! (id: '{0}')", id));
            }
            if (!hotel.CCAuthorizationExist)
            {
                return NotFound(String.Format("CC Authorization Document '{0}' not found !!", hotel.CCAuthorization));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(hotel.CCAuthorizationFullFileName);
                //FileContentResult result = new FileContentResult(fs, "application/pdf")
                //{
                //    FileDownloadName = hotel.CCAuthorization
                //};
                //return result;
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = hotel.CCAuthorization };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadCCAuthorization()", error));
                return BadRequest(error);
            }
        }


        // ************************************************************************
        // Gestione EMAILTEMPLATES
        // ************************************************************************
        public ActionResult ManageEmailTemplates()
        {
            return View();
        }

        [HttpGet]
        public object GetEmailTemplates(DataSourceLoadOptions loadOptions)
        {
            var data = _context.EmailTemplates.OrderBy(t => t.EmailTemplateId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetGenericEmailTemplates(DataSourceLoadOptions loadOptions)
        {
            var data = _context.EmailTemplates.Where(t=>!t.EmailTemplateId.StartsWith("_")).OrderBy(t => t.EmailTemplateId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }


        private bool EmailTemplateExists(string EmailTemplateId)
        {
            return _context.EmailTemplates.Any(e => e.EmailTemplateId == EmailTemplateId);
        }

        // GET: TRegistry/CreateEmailTemplate
        public IActionResult CreateEmailTemplate()
        {
            return View();
        }

        // POST: TRegistry/CreateEmailTemplate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateEmailTemplate([Bind("EmailTemplateId,Subject,Body,Description")] EmailTemplate emailTemplate)
        {
            emailTemplate.EmailTemplateId = emailTemplate.EmailTemplateId.ToUpper();
            if (EmailTemplateExists(emailTemplate.EmailTemplateId))
            {
                ModelState.AddModelError("EmailTemplateId", "Email Code already exists");
            }

            if (ModelState.IsValid)
            {
                emailTemplate.UserLastUpd = User.Identity.Name;
                emailTemplate.TmstLastUpd = DateTime.Now;
                _context.Add(emailTemplate);
                await _context.SaveChangesAsync();
                return RedirectToAction("ManageEmailTemplates");
            }
            return View(emailTemplate);
        }

        // GET: TRegistry/EditEmailTemplate/5
        public async Task<IActionResult> EditEmailTemplate(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return NotFound();
            }

            var emailTemplate = await _context.EmailTemplates.SingleOrDefaultAsync(m => m.EmailTemplateId == id);
            if (emailTemplate == null)
            {
                return NotFound();
            }
            return View(emailTemplate);
        }

        //// POST: TRegistry/EditEmailTemplate/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditEmailTemplate([Bind("EmailTemplateId,Subject,Body,Description")] EmailTemplate emailTemplate)
        {

            if (string.IsNullOrWhiteSpace(emailTemplate.EmailTemplateId)) 
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    emailTemplate.UserLastUpd = User.Identity.Name;
                    emailTemplate.TmstLastUpd = DateTime.Now;
                    _context.Update(emailTemplate);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
                return RedirectToAction("ManageEmailTemplates");
            }
            return View(emailTemplate);
        }

        [HttpPost]
        public ActionResult DeleteEmailTemplate(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                EmailTemplate rec2Del = _context.EmailTemplates.Where(t => t.EmailTemplateId == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.EmailTemplates.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.EmailTemplateId);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.EmailTemplateId, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        // ************************************************************************
        // Gestione TRAINING CALENDAR
        // ************************************************************************
        // GET: TRegistry/ManageCalendar/5
        public ActionResult ManageCalendar()
        {
            return View(new TrainingCalendar());
        }

        //// POST: TRegistry/ManageCalendar/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ManageCalendar(IFormFile file)
        {
            TrainingCalendar calendar = new TrainingCalendar();
            try
            {
                if (file != null && file.Length > 0)
                {
                    
                    if (!String.IsNullOrWhiteSpace(calendar.CalendarFileName) && calendar.CalendarExist)
                    {
                        var backup =  string.Format("{0}_{1}",calendar.CalendarFileName, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
                        System.IO.File.Move(calendar.CalendarFullFileName, System.IO.Path.Combine(calendar.CalendarPathName, backup));
                    }

                    if (!Directory.Exists(calendar.CalendarPathName))
                    {
                        Directory.CreateDirectory(calendar.CalendarPathName);
                    }
                    string fullFileName = System.IO.Path.Combine(calendar.CalendarPathName, file.FileName);
                    using (FileStream fs = System.IO.File.Create(fullFileName))
                    {
                        file.CopyTo(fs);
                        TempData["MsgToLayout"] = $"Upload completed!";
                    }
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return View(new TrainingCalendar());
        }
        [AllowAnonymous]
        public ActionResult downloadCalendar()
        {
            TrainingCalendar calendar = new TrainingCalendar();
            if (string.IsNullOrWhiteSpace(calendar.CalendarFileName))
            {
                return NotFound(String.Format("Calendar FileName not valid !!"));
            }
            if (!calendar.CalendarExist)
            {
                return NotFound(String.Format("CC Calendar Document '{0}' not found !!", calendar.CalendarFileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(calendar.CalendarFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = calendar.CalendarFileName };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadCalendar()", error));
                return BadRequest(error);
            }
        }

        // ************************************************************************
        // Gestione COMPANIES (Ragione Sociale 'normalizzate')
        // ************************************************************************
        public ActionResult ManageCompanies()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetCompanies(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Companies
                               .OrderBy(t => t.CompanyId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult GetCompaniesAutoComplete(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Companies.Select(t => t.CompanyId).ToList();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(data, loadOptions)), "application/json");
        }

        private bool CompanyExists(string CompanyId)
        {
            return _context.Companies.Any(e => e.CompanyId == CompanyId);
        }

        [HttpPost]
        public ActionResult DeleteCompany(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                Company rec2Del = _context.Companies.Where(t => t.CompanyId == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.Companies.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.CompanyId);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.CompanyId, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateCompany(string CompanyId)
        {
            var ret = "";

            string newId = CompanyId;
            if (!String.IsNullOrWhiteSpace(newId) && newId.Length > 100)
            {
                newId = newId.Substring(0, 100);
            }

            if (CompanyExists(newId))
            {
                ret = String.Format("Company already exists");
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                Company newRec = new Company()
                {
                    CompanyId = newId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                try
                {
                    _context.Companies.Add(newRec);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    ret = String.Format("Create Company - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                }
            }

            return Json(ret);
        }

        // ************************************************************************
        // Gestione LOCATIONS 
        // ************************************************************************
        public ActionResult ManageLocations()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetLocations(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingLocations
                               .OrderBy(t => t.Id).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        private bool LocationExists(string Location)
        {
            return _context.TrainingLocations.Any(e => e.Id == Location);
        }

        [HttpPost]
        public ActionResult DeleteLocation(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                TrainingLocation rec2Del = _context.TrainingLocations.Where(t => t.Id == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    if (_context.Trainings.Where(t=>t.Location.Equals(rec2Del.Id)).Count() > 0 )
                    {
                        ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                    } else
                    {
                        try
                        {
                            _context.TrainingLocations.Remove(rec2Del);
                            _context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            var sqlex = e.InnerException as SqlException;
                            if (sqlex != null && sqlex.Number == 547)
                            {
                                ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                            }
                            else
                            {
                                ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Id, e.Message, e.InnerException.ToString());
                            }
                            break;
                        }
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateLocation(string Location)
        {
            var ret = "";

            string newId = Location.ToUpper();
            if (!String.IsNullOrWhiteSpace(newId) && newId.Length > 200)
            {
                newId = newId.Substring(0, 200);
            }
            if (LocationExists(newId))
            {
                ret = String.Format("Location already exists");
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                TrainingLocation newRec = new TrainingLocation()
                {
                    Id = newId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                try
                {
                    _context.TrainingLocations.Add(newRec);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    ret = String.Format("Create TrainingLocation - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                }
            }

            return Json(ret);
        }

        public ActionResult downloadIndications(string id)
        {
            TrainingLocation location = _context.TrainingLocations.Where(t => t.Id == id).FirstOrDefault();
            if (location == null)
            {
                return NotFound(String.Format("location {0} not found!", id));
            }
            if (!location.IndicationsExist)
            {
                return NotFound(String.Format("{0} not found !!", location.IndicationsFileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(location.IndicationsFullFileName);
                return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = location.IndicationsFileName };
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "downloadIndications()", error));
                return BadRequest(error);
            }
        }

        [HttpPost]
        public ActionResult UploadIndications(string id)
        {
            try
            {
                TrainingLocation location = _context.TrainingLocations.Where(t => t.Id == id).FirstOrDefault();
                if (location == null)
                {
                    Response.StatusCode = 500;
                }
                var myFile = Request.Form.Files["myFile"];
                if (!Directory.Exists(location.IndicationsPathName))
                {
                    Directory.CreateDirectory(location.IndicationsPathName);
                }
                using (var fileStream = System.IO.File.Create(location.IndicationsFullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }

            return new EmptyResult();
        }

        // ************************************************************************
        // Gestione  TYPES oF PARTICIPANTS 
        // ************************************************************************
        public ActionResult ManageTypeOfParticipants()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetTypeOfParticipants(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingTypeOfParticipants
                               .OrderBy(t => t.Id).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        private bool TypeOfParticipantExists(string TypeOfParticipant)
        {
            return _context.TrainingTypeOfParticipants.Any(e => e.Id.ToUpper().Equals(TypeOfParticipant.ToUpper()));
        }

        [HttpPost]
        public ActionResult DeleteTypeOfParticipant(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                TrainingTypeOfParticipant rec2Del = _context.TrainingTypeOfParticipants.Where(t => t.Id == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    if (_context.TrainingRegistrations.Where(t => t.TypeOfParticipant.Equals(rec2Del.Id)).Count() > 0)
                    {
                        ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                    }
                    else
                    {
                        try
                        {
                            _context.TrainingTypeOfParticipants.Remove(rec2Del);
                            _context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            var sqlex = e.InnerException as SqlException;
                            if (sqlex != null && sqlex.Number == 547)
                            {
                                ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                            }
                            else
                            {
                                ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Id, e.Message, e.InnerException.ToString());
                            }
                            break;
                        }
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateTypeOfParticipant(string TypeOfParticipant)
        {
            var ret = "";

            //string newId = TypeOfParticipant.ToUpper();
            string newId = TypeOfParticipant;
            if (!String.IsNullOrWhiteSpace(newId) && newId.Length > 100)
            {
                newId = newId.Substring(0, 100);
            }
            if (TypeOfParticipantExists(newId))
            {
                ret = String.Format("Type Of Participant already exists");
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                TrainingTypeOfParticipant newRec = new TrainingTypeOfParticipant()
                {
                    Id = newId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                try
                {
                    _context.TrainingTypeOfParticipants.Add(newRec);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    ret = String.Format("Create TrainingTypeOfParticipant - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                }
            }

            return Json(ret);
        }

        // ************************************************************************
        // Gestione  JOB POSITIONS
        // ************************************************************************
        public ActionResult ManageJobPositions()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetJobPositions(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TrainingJobPositions
                               .OrderBy(t => t.Id).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        private bool JobPositionExists(string JobPosition)
        {
            return _context.TrainingJobPositions.Any(e => e.Id.ToUpper().Equals(JobPosition.ToUpper()));
        }

        [HttpPost]
        public ActionResult DeleteJobPosition(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                TrainingJobPosition rec2Del = _context.TrainingJobPositions.Where(t => t.Id == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    if (_context.TrainingRegistrations.Where(t => t.JobPosition.Equals(rec2Del.Id)).Count() > 0)
                    {
                        ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                    }
                    else
                    {
                        try
                        {
                            _context.TrainingJobPositions.Remove(rec2Del);
                            _context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            var sqlex = e.InnerException as SqlException;
                            if (sqlex != null && sqlex.Number == 547)
                            {
                                ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Id);
                            }
                            else
                            {
                                ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Id, e.Message, e.InnerException.ToString());
                            }
                            break;
                        }
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateJobPosition(string JobPosition)
        {
            var ret = "";

            //string newId = JobPosition.ToUpper();
            string newId = JobPosition;
            if (!String.IsNullOrWhiteSpace(newId) && newId.Length > 100)
            {
                newId = newId.Substring(0, 100);
            }
            if (JobPositionExists(newId))
            {
                ret = String.Format("Job Position already exists");
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                TrainingJobPosition newRec = new TrainingJobPosition()
                {
                    Id = newId,
                    UserLastUpd = User.Identity.Name,
                    TmstLastUpd = DateTime.Now
                };
                try
                {
                    _context.TrainingJobPositions.Add(newRec);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    ret = String.Format("Create TrainingJobPosition - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                }
            }

            return Json(ret);
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using System.Data.SqlClient;
using WebApp.Entity;
using WebApp.Classes;
using WebApp.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Data;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly ILogger _logger;
        private static string connectionString;
        private readonly WebAppDbContext _context;
        private readonly IEmailSender _emailSender;

        public UsersController(ILogger<DashboardController> logger, WebAppDbContext context, IEmailSender emailSender)
        {
            _logger = logger;
            _context = context;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }
        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public IActionResult Index()
        {

            UsersIndexDTO dto = new UsersIndexDTO() { };
            dto.users = (new UsersBO()).Get();
            // Lettura RUOLI (lista)
            foreach (var item in dto.users)
            {
                List<string> ruoli = (new UsersBO()).GetRoles(item.userId);
                string sep = "";
                foreach (var ruolo in ruoli)
                {
                    item.ruoli += sep + ruolo;
                    sep = ", ";
                }

                //UserCfg usercfg = _context.UserCfg.Find(item.userId);
                //if (usercfg != null)
                //{
                //    item.Country = usercfg.Country;
                //    item.CommercialEntity = usercfg.CommercialEntity;
                //}
                // Country configurate
                List<string> valori = (new UsersBO()).GetLinks(item.userId, Lookup.UserLinkType.Country);
                sep = "";
                foreach (var valore in valori)
                {
                    item.Country += sep + valore;
                    sep = ", ";
                }
                // Commercial Entity configurate
                valori = (new UsersBO()).GetLinks(item.userId, Lookup.UserLinkType.CommercialEntity);
                sep = "";
                foreach (var valore in valori)
                {
                    item.CommercialEntity += sep + valore;
                    sep = ", ";
                }

            }

            return View(dto);
        }


        // GET: Users/ChangeStatus/5
        public IActionResult ChangeStatus(string id)
        {
            (new UsersBO()).ChangeStatus(id);
            return RedirectToAction("Index");
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(String id)
        {
            UserEditDTO dto = new UserEditDTO();
            User user = (new UsersBO()).Get(id).FirstOrDefault();
            PropertyCopier <User, UserEditDTO>.Copy(user , dto); // Copio proprietÓ con stesso nome
            UserCfg usercfg = _context.UserCfg.Find(id);
            if (usercfg != null)
            {
                PropertyCopier<UserCfg, UserEditDTO>.Copy(usercfg, dto);    // Copio proprietÓ con stesso nome
            }

            dto.UserRoles = (new UsersBO()).GetRoles(id, "ID");

            dto.countries = (new CountriesBO()).GetCountries();
            dto.regions = (new RegionsBO()).GetRegions();
            dto.areas = (new AreasBO()).GetAreas();
            //dto.CommercialEntity = _context.CommercialEntity.ToList();
            dto.CommercialEntities = (new InstrumentsBO()).GetCommercialEntities();
            dto.approvers = (new UsersBO()).Get(filterRoleName: Lookup.Role_QuotationApproval);
            dto.roles = (new UsersBO()).GetAllRoles();
            dto.departments = _context.Departments.OrderBy(t => t.Description).ToList();
            dto.customers = (new CustomersBO()).GetCustomers();   // tutti i clienti presenti in anagrafica
        
            dto.countriesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Country);
            dto.commercialEntitiesLink= (new UsersBO()).GetLinks(id, Lookup.UserLinkType.CommercialEntity);            
            dto.customersLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Customer);

            return View(dto);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(String id, string selectedRoles)
        {
            User user = (new UsersBO()).Get(id).FirstOrDefault();
            UserCfg userCfg = _context.UserCfg.Find(id);

            UserEditDTO dto = new UserEditDTO();
            PropertyCopier<User, UserEditDTO>.Copy(user, dto); // Copio proprietÓ con stesso nome

            if (await TryUpdateModelAsync<UserEditDTO>(dto))
            {
                var context = new ValidationContext(dto, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(dto, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (String.IsNullOrWhiteSpace(dto.userEmail))
                {
                    ModelState.AddModelError("userEmail", "The User Email field is required");
                }
            }

            // AGGIORNAMENTO
            if (ModelState.IsValid)
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        // EMAIL 
                        if (!user.userEmail.Equals(dto.userEmail))
                        {
                            (new UsersBO()).setUserEmail(user.userName, dto.userEmail);
                        }

                        // ROLES
                        (new UsersBO()).RemoteAllRoles(id);
                        if (!String.IsNullOrWhiteSpace(selectedRoles))
                        {
                            foreach (string roleId in selectedRoles.Split('|'))
                            {
                                (new UsersBO()).InsertRoles(id, roleId);
                            }
                        }
                        // CONFIG
                        if (userCfg == null)
                        {
                            userCfg = new UserCfg()
                            {
                                UserID = id
                            };
                            _context.UserCfg.Add(userCfg);
                            await _context.SaveChangesAsync();
                        }

                        bool bSendEmailConfirmCfg = false;
                        if (userCfg.TmstConfirmCfg == null && !String.IsNullOrWhiteSpace(selectedRoles))
                        {
                            bSendEmailConfirmCfg = true;
                            userCfg.TmstConfirmCfg = DateTime.Now;
                        }

                        PropertyCopier<UserEditDTO, UserCfg>.Copy(dto, userCfg); // Copio proprietÓ con stesso nome
                        userCfg.TmstLastUpd = DateTime.Now;
                        userCfg.UserLastUpd = User.Identity.Name;
                        //userCfg.DepartmentId = dto.DepartmentId;        // non viene ribaltato dalla COPIA
                        //userCfg.Country = ...country
                        //userCfg.CommercialEntity = ... commercial Entity 
                        _context.Update(userCfg);
                        await _context.SaveChangesAsync();

                        // AGGIORNA ENTITA' CONFIGURATE (MULTISELEZIONE)
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.Country,  dto.countriesLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.CommercialEntity, dto.commercialEntitiesLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.Customer, dto.customersLink, User.Identity.Name);

                        // COMMIT !! 
                        dbContextTransaction.Commit();

                        // Invio Email 
                        if (bSendEmailConfirmCfg)
                        {
                            await _emailSender.SendEmailAsync(dto.userEmail, "Welcome in ELITeBoard",
                            String.Format("Dear {0},<br><br>Now you can access to ELITeBoard System.<br><br>Regards.<br><br>" +
                                          "Note. Do not reply to this email.", dto.userName));
                        }

                    }
                    catch (Exception e)
                    {
                        dbContextTransaction.Rollback();
                        TempData["MsgToLayout"] = String.Format("Error in Update User Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    }
                    return RedirectToAction("Index");
                }
            }
            dto.UserRoles = (new UsersBO()).GetRoles(id, "ID");

            dto.countries = (new CountriesBO()).GetCountries();
            dto.regions = (new RegionsBO()).GetRegions();
            dto.areas = (new AreasBO()).GetAreas();
            //dto.CommercialEntity = _context.CommercialEntity.ToList();
            dto.CommercialEntities = (new InstrumentsBO()).GetCommercialEntities();
            dto.approvers = (new UsersBO()).Get(filterRoleName: Lookup.Role_QuotationApproval);
            dto.roles = (new UsersBO()).GetAllRoles();
            dto.customers = (new CustomersBO()).GetCustomers();   // tutti i clienti presenti in anagrafica

            dto.countriesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Country);
            dto.commercialEntitiesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.CommercialEntity);
            dto.customersLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Customer);

            return View("Edit", dto);
        }

    }
}




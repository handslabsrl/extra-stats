using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Localization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;

namespace ELITeBoard.Controllers
{
    [Authorize(Roles = "Quotation BackOffice")]
    public class QRegistryController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IHtmlLocalizer<QRegistryController> _localizer;

        public QRegistryController(WebAppDbContext context, ILogger<QRegistryController> logger, IHtmlLocalizer<QRegistryController> localizer)
        {
            _context = context;
            _logger = logger;
            _localizer = localizer;
        }

        // ************************************************************************
        // Gestione EXTRACTIONS
        // ************************************************************************
        public ActionResult ManageExtractions()
        {
            return View();
        }

        [HttpGet]
        public object GetExtractions(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Extractions
                               .OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public ActionResult DeleteExtraction(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach(string id in items)
            {
                int id2Del = Int32.Parse(id);
                Extraction rec2Del = _context.Extractions.Where(t => t.ID == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.Extractions.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateExtraction(string Description, string ProductPartNumber)
        {
            var ret = "";
            Extraction newRec = new Extraction()
            {
                Description = Description,
                ProductPartNumber = ProductPartNumber,
                KitSize = 0,
                OfficialPrice = 0,
                StandardCost = 0,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.Extractions.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create Extraction - Error: {0} - {1}",e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateExtraction(int key, string values)
        {
            Extraction rec2Update = _context.Extractions.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione INSTRUMENT TYPES
        // ************************************************************************
        public ActionResult ManageInstrumentTypes()
        {
            return View();
        }

        [HttpGet]
        public object GetInstrumentTypes(DataSourceLoadOptions loadOptions)
        {
            var data = _context.InstrumentType
                               .OrderBy(t => t.Type).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public ActionResult DeleteInstrumentType(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                InstrumentType rec2Del = _context.InstrumentType.Where(t => t.ID == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.InstrumentType.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateInstrumentType(string Type, string Description, string ProductPartNumber)
        {
            var ret = "";
            InstrumentType newRec = new InstrumentType()
            {
                Type = Type,
                Description = Description,
                ProductPartNumber = ProductPartNumber,
                OfficialPrice = 0,
                StandardCost = 0,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.InstrumentType.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create InstrumentType - Error: {0} - {1}", e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateInstrumentType(int key, string values)
        {
            InstrumentType rec2Update = _context.InstrumentType.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione COMMERCIAL ENTITY
        // ************************************************************************
        public ActionResult ManageCommercialEntity()
        {
            return View();
        }

        [HttpGet]
        public object GetCommercialEntity(DataSourceLoadOptions loadOptions)
        {
            var data = _context.CommercialEntity
                               .OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public ActionResult DeleteCommercialEntity(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                CommercialEntity rec2Del = _context.CommercialEntity.Where(t => t.ID == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.CommercialEntity.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateCommercialEntity(string Description)
        {
            var ret = "";
            CommercialEntity newRec = new CommercialEntity()
            {
                Description = Description,
                SubTotal1 = 0,
                SubTotal2 = 0,
                Total = 0,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.CommercialEntity.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create CommercialEntity - Error: {0} - {1}", e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateCommercialEntity(int key, string values)
        {
            CommercialEntity rec2Update = _context.CommercialEntity.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione CONSUMABLES
        // ************************************************************************
        public ActionResult ManageConsumables()
        {
            return View();
        }

        [HttpGet]
        public object GetConsumables(DataSourceLoadOptions loadOptions)
        {
            var data = _context.ElitePCRKits.Where (t=>t.PCRType == Lookup.ElitePCRKitType.Consumable)
                               .OrderBy(t => t.PCRTarget).ThenBy(t=>t.ProductPartNumber).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public ActionResult DeleteConsumable(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                ElitePCRKit rec2Del = _context.ElitePCRKits.Where(t => t.ID == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.ElitePCRKits.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateConsumable(string PCRTarget, string Description, string ProductPartNumber)
        {
            var ret = "";
            ElitePCRKit newRec = new ElitePCRKit()
            {
                PCRTarget = PCRTarget,
                ProductPartNumber = ProductPartNumber,
                Description = Description,
                PCRType = Lookup.ElitePCRKitType.Consumable,
                ExtractionType = Lookup.ExtractionType.noExtraction,
                KitSize = 0,
                OfficialPrice = 0,
                StandardCost = 0,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.ElitePCRKits.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create Consumable - Error: {0} - {1}", e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateConsumable(int key, string values)
        {
            ElitePCRKit rec2Update = _context.ElitePCRKits.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        // ************************************************************************
        // Gestione ELITE PCR KITS
        // ************************************************************************
        public ActionResult ManageElitePcrKits()
        {
            // Cancellazione di eventuali KIT orfani (senza KIT di tipo ASSAY con stesso PCRTarget)
            var TargetAssay = _context.ElitePCRKits.Where(t => t.PCRType == Lookup.ElitePCRKitType.Assay).Select(t => t.PCRTarget.ToUpper()).ToList();
            var kitOrfani = (from k in _context.ElitePCRKits
                            where (k.PCRType == Lookup.ElitePCRKitType.Control || k.PCRType == Lookup.ElitePCRKitType.Standard) && !TargetAssay.Contains(k.PCRTarget.ToUpper())
                            select k).ToList();
            if (kitOrfani.Count > 0)
            {
                try
                {
                    _context.ElitePCRKits.RemoveRange(kitOrfani);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    // Nessuna azione 
                }
            }
            return View();
        }

        [HttpGet]
        public object GetElitePcrKits(DataSourceLoadOptions loadOptions)
        {
            HashSet<Lookup.ElitePCRKitType> typesOk = new HashSet<Lookup.ElitePCRKitType>();
            typesOk.Add(Lookup.ElitePCRKitType.Assay);
            typesOk.Add(Lookup.ElitePCRKitType.Control);
            typesOk.Add(Lookup.ElitePCRKitType.Standard);
            var data = _context.ElitePCRKits.Where(t => typesOk.Contains(t.PCRType))
                               .OrderBy(t => t.PCRTarget).ThenBy(t => t.PCRType).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }


        [HttpPost]
        public ActionResult DeleteElitePcrKit(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                int id2Del = Int32.Parse(id);
                ElitePCRKit rec2Del = _context.ElitePCRKits.Where(t => t.ID == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        _context.ElitePCRKits.Remove(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        var sqlex = e.InnerException as SqlException;
                        if (sqlex != null && sqlex.Number == 547)
                        {
                            ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                        }
                        else
                        {
                            ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                        }
                        break;
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateElitePcrKit(string PCRTarget, string DescriptionAssay, string ProductPartNumberAssay, 
                                                                string DescriptionControl, string ProductPartNumberControl,
                                                                string DescriptionStandard, string ProductPartNumberStandard)
        {
            var ret = "";
            List<string> msg = new List<string>();

            var KitEsistenti = _context.ElitePCRKits.Where(t => t.PCRTarget.ToUpper().Equals(PCRTarget.ToUpper())).Select(t => t.PCRType).ToList();
            var bEsisteAssay = KitEsistenti.Contains(Lookup.ElitePCRKitType.Assay);

            if (!String.IsNullOrWhiteSpace(DescriptionAssay) && !String.IsNullOrWhiteSpace(ProductPartNumberAssay))
            {
                if (KitEsistenti.Contains(Lookup.ElitePCRKitType.Assay))
                {
                    msg.Add(String.Format("Assay Kit already exists"));
                } else { 
                    ElitePCRKit newAssay = new ElitePCRKit()
                    {
                        PCRTarget = PCRTarget,
                        Description = DescriptionAssay,
                        ProductPartNumber = ProductPartNumberAssay,
                        PCRType = Lookup.ElitePCRKitType.Assay,
                        ExtractionType = Lookup.ExtractionType.noExtraction,
                        KitSize = 0,
                        OfficialPrice = 0,
                        StandardCost = 0,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    try
                    {
                        _context.ElitePCRKits.Add(newAssay);
                        _context.SaveChanges();
                        bEsisteAssay = true;
                    }
                    catch (Exception e)
                    {
                        ret = String.Format("Create ElitePcrKit/Assay - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(ret) && !String.IsNullOrWhiteSpace(DescriptionControl) && !String.IsNullOrWhiteSpace(ProductPartNumberControl))
            {
                if (KitEsistenti.Contains(Lookup.ElitePCRKitType.Control))
                {
                    msg.Add(String.Format("Control Kit already exists"));
                }
                else if (!bEsisteAssay)
                {
                    msg.Add(String.Format("Control Kit was not created because Assay Kit is missing"));
                }
                else
                {
                    ElitePCRKit newControl = new ElitePCRKit()
                    {
                        PCRTarget = PCRTarget,
                        Description = DescriptionControl,
                        ProductPartNumber = ProductPartNumberControl,
                        PCRType = Lookup.ElitePCRKitType.Control,
                        ExtractionType = Lookup.ExtractionType.noExtraction,
                        KitSize = 0,
                        OfficialPrice = 0,
                        StandardCost = 0,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    try
                    {
                        _context.ElitePCRKits.Add(newControl);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        ret = String.Format("Create ElitePcrKit/Control - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(ret) && !String.IsNullOrWhiteSpace(DescriptionStandard) && !String.IsNullOrWhiteSpace(ProductPartNumberStandard))
            {
                if (KitEsistenti.Contains(Lookup.ElitePCRKitType.Standard))
                {
                    msg.Add(String.Format("Standard Kit already exists"));
                }
                else if (!bEsisteAssay)
                {
                    msg.Add(String.Format("Standard Kit was not created because Assay Kit is missing"));
                }
                else
                {
                    ElitePCRKit newStandard = new ElitePCRKit()
                    {
                        PCRTarget = PCRTarget,
                        Description = DescriptionStandard,
                        ProductPartNumber = ProductPartNumberStandard,
                        PCRType = Lookup.ElitePCRKitType.Standard,
                        ExtractionType = Lookup.ExtractionType.noExtraction,
                        KitSize = 0,
                        OfficialPrice = 0,
                        StandardCost = 0,
                        UserLastUpd = User.Identity.Name,
                        TmstLastUpd = DateTime.Now
                    };
                    try
                    {
                        _context.ElitePCRKits.Add(newStandard);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        ret = String.Format("Create ElitePcrKit/Standard - Error: {0} - {1}", e.Message, e.InnerException.ToString());
                    }
                }
            }

            if (String.IsNullOrWhiteSpace(ret))
            {
                ret = string.Join("<br>", msg.ToArray()); 
            }

            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateElitePcrKit(int key, string values)
        {
            ElitePCRKit rec2Update = _context.ElitePCRKits.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

    }
}
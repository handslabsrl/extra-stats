using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Models;
using WebApp.Classes;
using System.IO;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using DevExtreme.AspNet.Data;

namespace WebApp.Controllers
{
    [Authorize]
    public class UtilsController : Controller
    {
        private readonly ILogger _logger;
        private readonly WebAppDbContext _context;

        public UtilsController(ILogger<UtilsController> logger, WebAppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult FunctionsDocumentation()
        {
            return View();
        }

        public ActionResult ShowFunctionPdf(string functionName)
        {
            FunctionDocumentation item = new FunctionDocumentation() { functionName = functionName };
            string fileNamePdf = item.getFileNamePdf;
            if (!item.existsPdf)
            {
                return NotFound(String.Format("Function Documentation '{0}' not found !!", item.functionName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(item.getFullFileNamePdf);
                //    //FileStream fs = new FileStream(filePathName, FileMode.Open, FileAccess.Read, FileShare.Read);
                //    return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml");
                return new FileContentResult(fs, "application/pdf");
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "Utils.ShowFunctionPdf()", error));
                return BadRequest(error);
            }
        }

        [AllowAnonymous]
        public ActionResult DownloadDoc(string fileName)
        {
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Download\\Docs");
            string fullFileName = System.IO.Path.Combine(relativePath, fileName);

            if (!System.IO.File.Exists(fullFileName))
            {
                return NotFound(String.Format("Document '{0}' not found !!", fileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(fullFileName);
                return new FileContentResult(fs, "application/pdf");
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadDoc)", error));
                return BadRequest(error);
            }
        }



        [HttpGet]
        public ActionResult GetFunctionNames(DataSourceLoadOptions loadOptions)
        {
            List<FunctionDocumentation> items = new List<FunctionDocumentation>();
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DailyExecution });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_AssayUtilization });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_UserRoles });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DataImport });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DataMining });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_LogFiles }); 
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_UserManual });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_RnDExtractions });
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(items.OrderBy(t=>t.functionName), loadOptions)), "application/json");
        }

        [HttpPost]
        public ActionResult UploadPdf(string functionName)
        {
            try
            {
                FunctionDocumentation item = new FunctionDocumentation() { functionName = functionName };
                if (item == null)
                {
                    Response.StatusCode = 500;
                }
                var myFile = Request.Form.Files["myFile"];
                var fullFileName = item.getFullFileNamePdf;
                if (!Directory.Exists(Path.GetDirectoryName(fullFileName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullFileName));
                }
                using (var fileStream = System.IO.File.Create(fullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }

            return new EmptyResult();
        }

        // POST: Utils/deleteFilePdf/5
        public JsonResult deleteFilePdf(string id)
        {
            string ret = "";
            FunctionDocumentation item = new FunctionDocumentation() { functionName = id };
            string fileNamePdf = item.getFileNamePdf;
            if (!item.existsPdf)
            {
                ret = String.Format("File '{0}' not found !!", fileNamePdf);
            } else
            {
                try
                {
                    System.IO.File.Delete(item.getFullFileNamePdf);
                }
                catch (Exception e)
                {
                    ret = String.Format("[deleteFilePdf] - {0} / {1}", e.Message, e.Source);
                }
            }
            return Json(ret);
        }

    }
}


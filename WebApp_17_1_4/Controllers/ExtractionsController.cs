using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApp.Repository;
using WebApp.Entity;
using WebApp.Data;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using WebApp.Classes;
using System.Data.SqlClient;
using NPoco;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Roles = "RnD Analysis")]
    public class ExtractionsController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;

        private readonly string DirExport = "Download\\Extractions";
        private readonly string idCacheCreateRnDExtraction = "CreateRnDExtraction";

        public ExtractionsController(WebAppDbContext context, ILogger<QuotationsController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult IndexRnD()
        {
            return View();
        }



        public ActionResult CreateRnDExtraction(CreateRnDExtractionDTO dto)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            if (!string.IsNullOrWhiteSpace(UserId))
            {
                if (dto.countriesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                    if (valori.Count == 1)
                    {
                        dto.countriesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.customersSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                    if (valori.Count == 1)
                    {
                        dto.customersSel = new List<string> { valori[0] };
                    }
                }
                if (dto.commercialEntitiesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                    if (valori.Count == 1)
                    {
                        dto.commercialEntitiesSel = new List<string> { valori[0] };
                    }
                }

                UserCfg userCfg = _context.UserCfg.Find(UserId);
                if (userCfg != null)
                {
                    if (!String.IsNullOrWhiteSpace(userCfg.Region))
                    {
                        dto.Region = userCfg.Region;
                        dto.lockRegion = true;
                    }
                    if (!String.IsNullOrWhiteSpace(userCfg.Area))
                    {
                        dto.Area = userCfg.Area;
                        dto.lockArea = true;
                    }
                }

            }

            if (dto.DateFrom == DateTime.MinValue)
            {
                dto.DateFrom = DateTime.Now.AddDays(-31);
            }
            if (dto.DateTo == DateTime.MinValue)
            {
                dto.DateTo = DateTime.Now;
            }

            // Inserimento Richieste 
            if (!string.IsNullOrWhiteSpace(dto.intrumentsSelJSON))
            {
                String[] strumenti = dto.intrumentsSelJSON.Split('|');
                foreach (var item in strumenti)
                {
                    ProcessRequestRnD info = new ProcessRequestRnD()
                    {
                        Instrument = item,
                        DateFrom = dto.DateFrom,
                        DateTo = dto.DateTo
                    };
                    ProcessRequest newRequest = new ProcessRequest()
                    {
                        ProcessType = Lookup.ProcessRequest_Type_RnDExtractions,
                        ProcessParms = JsonConvert.SerializeObject(new { info.Instrument, info.DateFrom, info.DateTo }),
                        Status = Lookup.ProcessRequest_Status_NEW,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.ProcessRequests.Add(newRequest);
                    _context.SaveChanges();
                }
                TempData["MsgToLayout"] = string.Format("Completed! {0} new requests area created!", strumenti.Count());
                return RedirectToAction("IndexRnD");
            }

            // Salvataggio filtri 
            HttpContext.Session.SetString(idCacheCreateRnDExtraction, JsonConvert.SerializeObject(dto));

            dto.commercialStatuses = (new CommercialStatusBO()).Get();
            dto.regions = (new RegionsBO()).GetRegions(UserIdCfg: UserId);
            dto.areas = (new AreasBO()).GetAreas(UserIdCfg: UserId);

            //String UserIdLink = "";
            //UserIdLink = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country).Count() > 0 ? UserId : "");
            dto.countries = (new CountriesBO()).GetCountries(UserIdCfg: UserId);
            dto.commercialEntities = (new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId);
            return View(dto);
        }

        // GET: LogFIles/Delete
        public IActionResult Delete(int RequestId)
        {
            ProcessRequest rec2Delete = _context.ProcessRequests.Find(RequestId);
            if (rec2Delete != null)
            {
                _context.ProcessRequests.Remove(rec2Delete);
                _context.SaveChanges();
            }
            return RedirectToAction("IndexRnD");
        }

        public ActionResult DownloadFile(int Id)
        {
            ProcessRequest data = _context.ProcessRequests.Find(Id);
            if (data == null)
            {
                return NotFound(string.Format("ProcessRequest {0} not found", Id));
            }
            if (string.IsNullOrWhiteSpace(data.FileNameDownload))
            {
                return BadRequest("File Name Missing");
            }
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirExport);
            string fullPathName = System.IO.Path.Combine(relativePath, data.TmstInse.ToString("yyyyMM"));
            string filePathName = System.IO.Path.Combine(fullPathName, data.FileNameDownload);
            _logger.LogInformation(string.Format("Download - Path: >{0}<", filePathName));
            if (!System.IO.File.Exists(filePathName))
            {
                return BadRequest(String.Format("File {0} not found!!", filePathName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(filePathName);
                return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", data.FileNameDownload);
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadFile.Get()", error));
                return BadRequest(error);
            }
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object GetRnDExtractions(DataSourceLoadOptions loadOptions)
        {
            var data = _context.ProcessRequests.Where(t => t.UserInse.Equals(User.Identity.Name) && t.ProcessType == Lookup.ProcessRequest_Type_RnDExtractions).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }
    }
}

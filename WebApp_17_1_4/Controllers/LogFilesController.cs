using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApp.Repository;
using WebApp.Entity;
using WebApp.Data;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using WebApp.Classes;
using System.Data.SqlClient;
using NPoco;
using OfficeOpenXml;
using System.Reflection;
//using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Log Files,Log Files Supervisor")]
    public class LogFilesController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private string connectionString;

        private readonly string DirUploadClaim = "Upload\\CLAIMS";
        //private readonly string DirImages = "Images";

        public LogFilesController(WebAppDbContext context, ILogger<QuotationsController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public IActionResult Index()
        {
            LogFilesIndexDTO dto = new LogFilesIndexDTO();
            dto.users2Assign = (new UsersBO()).Get(filterRoleName: Lookup.Role_LogFiles).Where(t=> !t.userName.Equals(User.Identity.Name)).ToList();
            return View(dto);
        }

        [HttpGet]
        [AllowAnonymous]
        public object getUsers2Assign(DataSourceLoadOptions loadOptions, int ClaimId)
        {
            List<User> data = new List<User>();
            if (ClaimId > 0)
            {
                var claim = _context.Claims.Find(ClaimId);
                string CommercialEntity = claim?.InstrumentCommercialEntity;
                if (String.IsNullOrWhiteSpace(CommercialEntity))
                {
                    CommercialEntity = "*MANCANTE*";
                }
                string Country  = claim?.InstrumentSiteCountry;
                if (String.IsNullOrWhiteSpace(Country))
                {
                    Country = "*MANCANTE*";
                }
                data = (new UsersBO()).Get(filterRoleName: Lookup.Role_LogFiles, supportForCommEntity: CommercialEntity, supportForCountry: Country).Where(t => !t.userName.Equals(User.Identity.Name)).ToList();
            }
            return DataSourceLoader.Load(data, loadOptions);
        }

        public IActionResult Detail([Bind("claimId,folderSel,scriptSel,tab,options,DateRif,SessionNumber,rowsList")] LogFileDetailDTO dto)
        {
            // Imposta default 
            if (String.IsNullOrWhiteSpace(dto.tab)) { dto.tab = "H"; }        
            if (String.IsNullOrWhiteSpace(dto.folderSel)) { dto.folderSel = Lookup.Claim_Folder_System; }        
            if (String.IsNullOrWhiteSpace(dto.rowsList)) { dto.rowsList = Lookup.Claim_RowsList_Group1; }

            // Workaound: viene fatta le redirect per nascondere i parametri di QueryString
            return RedirectToAction("DetailClaim", dto);
        }

        public IActionResult DetailClaim(LogFileDetailDTO dto)
        {
            bool loadPressureSN = false;
            bool loadPressure12N = false;
            bool showLeak = false;
            bool bExport = (dto.options??"").ToLower().Equals("export");
            string scriptSelOrig = dto.scriptSel;
            int? maxSession = 0;

            dto.options = "";
            dto.claim = _context.Claims.Find(dto.claimId);
            if (dto.claim == null)
            {
                TempData["MsgToLayout"] = $"Claim N.{dto.claimId} not found";
                return RedirectToAction("Index");
            }

            // normalizzazione script Selezionato per rimuovere eventuali descrizioni/Note
            if (!string.IsNullOrWhiteSpace(dto.scriptSel))
            {
                var newVal = Lookup.LogFiles_ScriptDecode.Where(t => t.descrizione.ToLower().Equals(dto.scriptSel.ToLower())).Select(t => t.id).FirstOrDefault();
                if (newVal != null)
                {
                    dto.scriptSel = newVal;
                }
            }
            // Caricamento dati per tab DETAIL/HEADER
            if (dto.tab.Equals("H"))
            {
                dto.dateRifList = GetDateRifList(dto.claim.ClaimId);
            }
            // Caricamento dati per tab TEMPERATURE
            if (dto.tab.Equals("T"))
            {
                if (dto.DateRif == null)
                {
                    // la min, da tutte le registrazioni !!! 
                    dto.DateRif = (from t in _context.ClaimTemperatures
                                   where t.Claim.ClaimId == dto.claimId
                                   select t.TmstGet.Date).DefaultIfEmpty().Min();
                }
                if (dto.SessionNumber == 0) { dto.SessionNumber = 1; }
                if (dto.DateRif != null)
                {
                    dto.ChartSubTitle = string.Format("Date: {0} - Run Number:{1}", dto.DateRifDeco, dto.SessionNumber);
                    if (bExport)
                    {
                        dto.temperatures = GetTemperaturesGeneric(dto.claimId, dto.SessionNumber, (DateTime)dto.DateRif, Lookup.Claim_TempRecType_Temp);
                        dto.firstTemperature = dto.temperatures.FirstOrDefault();
                        dto.coolBlockExt = GetTemperaturesGeneric(dto.claimId, dto.SessionNumber, (DateTime)dto.DateRif, Lookup.Claim_TempRecType_CoolBlock);
                        dto.firstCoolBlockExt = dto.coolBlockExt.FirstOrDefault();
                        return File(ExportLogFilesTemperatures(dto),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                            string.Format("LogFiles_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    }
                    else
                    {
                        // per la visualizzazione, faccio le chiamate REMOTE e quindi devo estrarre solo il primo record
                        dto.firstTemperature = GetTemperaturesGeneric(dto.claimId, dto.SessionNumber, (DateTime)dto.DateRif, Lookup.Claim_TempRecType_Temp).FirstOrDefault();
                        dto.firstCoolBlockExt = GetTemperaturesGeneric(dto.claimId, dto.SessionNumber, (DateTime)dto.DateRif, Lookup.Claim_TempRecType_CoolBlock).FirstOrDefault();
                    }

                    // Max Sessione per tutte le registrazioni di temperatura 
                    maxSession = _context.ClaimTemperatures.Where(t => t.Claim.ClaimId == dto.claimId && t.TmstGet.Date == (DateTime)dto.DateRif)
                                                           .Max(t => (int?)t.SessionNumber);
                }
            }
            // Caricamento dati per tab PRESSIONI
            if (dto.tab.Equals("P"))
            {
                if (dto.DateRif == null)
                {
                    dto.DateRif = (from t in _context.ClaimPressuresSN
                                    where t.Claim.ClaimId == dto.claimId && (t.Script.Equals(dto.scriptSel) || string.IsNullOrWhiteSpace(dto.scriptSel))
                                    select t.TmstRif.Date).DefaultIfEmpty().Min();
                }
                if (dto.SessionNumber == 0) { dto.SessionNumber = 1; }
                if (!String.IsNullOrWhiteSpace(dto.scriptSel) && dto.DateRif != null)
                {
                    dto.ChartSubTitle = string.Format("Script: {0} - Date: {1} - Run Number:{2}", dto.scriptSel, dto.DateRifDeco, dto.SessionNumber);
                    switch (dto.scriptSel.ToLower())
                    {
                        case "icdisp.scr":
                            dto.ChartTitle = "IC";
                            showLeak = true;
                            loadPressureSN = true;
                            break;
                        case "pcrreagentdisp.scr":
                            dto.ChartTitle = "MMX";
                            loadPressureSN = true;
                            break;
                        case "sampledisp.scr":
                            dto.ChartTitle = "SAMPLE";
                            loadPressureSN = true;
                            break;
                        case "dnasampledisp.scr":
                            dto.ChartTitle = "ELUATES";
                            loadPressureSN = true;
                            break;
                        default:
                            loadPressure12N = true;
                            dto.ChartTitle = "12 Nozzles";
                            break;
                    }
                    // Lettura Pressioni Single Nozzle
                    if (loadPressureSN)
                    {
                        dto.pressuresSN = GetPressuresSN(dto.claimId, dto.scriptSel, (DateTime)dto.DateRif, dto.SessionNumber);
                        dto.pressuresSNfromDB = _context.ClaimPressuresSN.Where(t => t.Claim.ClaimId == dto.claimId && t.Script == dto.scriptSel &&  
                                                                                     t.SessionNumber == dto.SessionNumber && t.TrackIdx >= 1 && t.TrackIdx <= 12 && 
                                                                                     t.TmstRif.Date == (DateTime)dto.DateRif).OrderBy(t => t.ClaimPressureSNId).ToList();
                        // Le serie ClotAsp e ClotDisp vengono visualizzate solo se sono configurati i check corrispondenti
                        dto.checks = (from t in _context.ClaimCheckCfgs
                                            where t.Script.ToLower().Equals(dto.scriptSel.ToLower())
                                            select t.CheckName.ToLower()).ToList();
                        maxSession = _context.ClaimPressuresSN.Where(t => t.Claim.ClaimId == dto.claimId && t.Script == dto.scriptSel &&
                                                                          t.TrackIdx >= 1 && t.TrackIdx <= 12 && t.TmstRif.Date == (DateTime)dto.DateRif)
                                                              .Max(t => (int?)t.SessionNumber);
                    }
                    // opzione per mostrare/nascondere i punti leak
                    dto.ChartSerieLeak = showLeak;
                    // Lettura Pressioni 12N
                    if (loadPressure12N)
                    {
                        dto.pressures = _context.ClaimPressures.Where(t => t.Claim.ClaimId == dto.claimId && t.OffSet == false &&
                                                                                         t.Script == dto.scriptSel && t.SessionNumber == dto.SessionNumber &&
                                                                                         t.TmstGet.Date == ((DateTime)dto.DateRif).Date).OrderBy(t => t.ClaimPressureId).ToList();
                        int idxJs = 1;
                        foreach (var item in dto.pressures)
                        {
                            item.idxJs = idxJs++;
                        }
                        maxSession = _context.ClaimPressures.Where(t => t.Claim.ClaimId == dto.claimId && t.OffSet == false &&
                                                                        t.Script == dto.scriptSel && t.TmstGet.Date == ((DateTime)dto.DateRif).Date)
                                                                .Max(t=>(int?)t.SessionNumber);
                    }
                    if ((dto.pressuresSN != null && dto.pressuresSN.Count() > 0) || (dto.pressures != null && dto.pressures.Count() > 0))
                    {
                        // ho messo direttamente nella view il riferimento a wwwroot/images/<nomescript>.png 
                        // in questo modo, non dovrebbe scaricarla ogni volta 
                        //dto.imgLegenda = getImageLegenda(dto.scriptSel);
                        dto.imgLegenda = dto.scriptSel;
                    }

                    if (bExport)
                    {
                        return File(ExportLogFilesPressuses(dto),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                            string.Format("LogFiles_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    }
                }
                // Script con pressioni pressioniSN o 12N
                List<string> script1 = (from k in _context.ClaimPressuresSN
                                        where k.Claim.ClaimId == dto.claimId
                                        select k.Script).Distinct().ToList();
                List<string> script2 = (from k in _context.ClaimPressures
                                        where k.Claim.ClaimId == dto.claimId
                                        select k.Script).Distinct().ToList();
                dto.scripts = script1.Union(script2).Distinct().OrderBy(x => x.ToString()).ToList();
            }
            // Caricamento dati per tab VOLUME
            if (dto.tab.Equals("V"))
            {
                if (dto.DateRif == null)
                {
                    dto.DateRif = (from t in _context.ClaimPressuresSN where t.Claim.ClaimId == dto.claimId
                                   select t.TmstRif.Date).DefaultIfEmpty().Min();
                }
                if (dto.SessionNumber == 0) { dto.SessionNumber = 1; }
                if (dto.DateRif != null && dto.DateRif != DateTime.MinValue)
                {
                    dto.ChartSubTitle = string.Format("Date: {0} - Run Number:{1}", dto.DateRifDeco, dto.SessionNumber);
                    dto.volumeEluates = GetVolumeEluates(dto.claimId, (DateTime)dto.DateRif, dto.SessionNumber);
                    dto.inventoryBlock = GetVolumeInventoryBlock(dto.claimId, (DateTime)dto.DateRif, dto.SessionNumber);
                    string[] scriptVol = { "dnasampledisp.scr", "ICDIsp.scr", "PCRreagentDisp.scr" };
                    maxSession = _context.ClaimPressuresSN.Where(t => t.Claim.ClaimId == dto.claimId && scriptVol.Contains(t.Script) &&
                                                                      t.TrackIdx >= 1 && t.TrackIdx <= 12 && t.TmstRif.Date == (DateTime)dto.DateRif)
                                                                     .Max(t => (int?)t.SessionNumber);
                    if (bExport)
                    {
                        return File(ExportLogFilesVolume(dto),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                            string.Format("LogFiles_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    }
                }
            }
            // Caricamento dati per tab RIGHE ERRORI
            if (dto.tab.Equals("R")) 
            {
                dto.scripts = (from k in _context.ClaimScripts
                               where k.Claim.ClaimId == dto.claimId
                               orderby k.Script
                               select k.Script).Distinct().ToList();
            }
            //if ("RPT".Contains(dto.tab))
            //{
            //    dto.scripts = (from k in _context.ClaimRows
            //                   where k.Claim.ClaimId == dto.claimId
            //                   orderby k.Script
            //                   select k.Script).Distinct().ToList();
            //}
            //if (dto.tab.Equals("O"))
            //{
            //    dto.pressuresOffset = _context.ClaimPressures.Where(t => t.Claim.ClaimId == dto.claimId   && t.OffSet == true).OrderBy(t => t.ClaimPressureId).ToList();
            //}

            //Personalizzazione nome script, aggiungendo descrizione/note 
            if (dto.scripts != null)
            {
                for (int i = 0; i < dto.scripts.Count; i++)
                {
                    var newVal = Lookup.LogFiles_ScriptDecode.Where(t => t.id.ToLower().Equals(dto.scripts[i].ToLower())).Select(t => t.descrizione).FirstOrDefault();
                    if (newVal != null)
                    {
                        dto.scripts[i] = newVal;
                    }
                }
            }

            // ripristino scriptSel originale 
            dto.scriptSel = scriptSelOrig;

            // MaxSessione 
            if (maxSession != null)
            {
                dto.maxSessionNumber = (int)maxSession;
            }

            switch (dto.tab)
            {
                case "T": return View("DetailTemp", dto);
                case "P": return View("DetailPress", dto);
                //case "O": return View("DetailPressOffset", dto);
                case "R": return View("DetailRows", dto);
                case "L": return View("DetailTracks", dto);
                case "S": return View("DetailSnapshot", dto);
                case "V": return View("DetailVolume", dto);
                default: return View("Detail", dto);
            }
        }

        //private string getImageLegenda(string scriptSel)
        //{
        //    string ret = "";
        //    if (!string.IsNullOrWhiteSpace(scriptSel))
        //    {
        //        string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirImages);
        //        string fullPathName = System.IO.Path.Combine(relativePath, String.Format("{0}.png", scriptSel.Replace(" ", String.Empty)));
        //        try
        //        {
        //            if (System.IO.File.Exists(fullPathName))
        //            {
        //                byte[] byteData = System.IO.File.ReadAllBytes(fullPathName);
        //                //Convert byte arry to base64string   
        //                string imreBase64Data = Convert.ToBase64String(byteData);
        //                ret = string.Format("data:image/png;base64,{0}", imreBase64Data);
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            // ignora errore su immagine 
        //        }
        //    }
        //    return ret;
        //}

        public Stream ExportLogFilesPressuses(LogFileDetailDTO dto)
        {

            ExcelPackage excel = new ExcelPackage();
            OfficeOpenXml.ExcelWorksheet workSheet = null;

            if (dto.pressuresSNfromDB != null)
            {
                workSheet = excel.Workbook.Worksheets.Add("pressures");
                var checks = (from t in _context.ClaimCheckCfgs
                              where t.Script.ToLower().Equals(dto.scriptSel.ToLower())
                              select t.CheckName.ToLower()).Distinct().ToList();
                var idxRow = 1;
                var idxCol = 0;
                var offsetCol = 1;
                var maxCol = 0;
                //////var aTrack = dto.pressuresSNfromDB.GroupBy(d => new { d.TrackIdx, d.SubTrack})
                //////       .Select(m => new { m.Key.TrackIdx, m.Key.SubTrack}).OrderBy(m => m.TrackIdx).ThenBy(m=>m.SubTrack).ToList();
                ////var aTrack = dto.pressuresSNfromDB.OrderBy(t => t.TrackSeq).ThenBy(t => t.TrackIdx).Select(t=>t.TrackIdx).Distinct().ToList();
                ////var aPos = new int[99];
                ////// Intestazione TRACK
                //////Dictionary<string, int> idxTrk = new Dictionary<string, int>();
                ////foreach (var item in aTrack)
                ////{
                ////    idxCol++;
                ////    //idxTrk[String.Format("{0}_{1}", item.TrackIdx, item.SubTrack)] = idxCol;
                ////    aPos[item] = idxCol + offsetCol;
                ////    //workSheet.Cells[idxRow, aPos[idxCol]].Value = String.Format("#{0}", item.TrackIdx);  // ??
                ////    workSheet.Cells[idxRow, aPos[item]].Value = String.Format("track {0}", item);
                ////}
                Dictionary<long, int> aPos = new Dictionary<long, int>();
                var aTrack = dto.pressuresSN.Where (t=>!string.IsNullOrWhiteSpace(t.track)).OrderBy(t => t.ClaimPressureSNId).Select(t => new { t.ClaimPressureSNId, t.track }).Distinct().ToList();
                foreach (var item in aTrack)
                {
                    if (!aPos.ContainsKey(item.ClaimPressureSNId))
                    {
                        idxCol++;
                        aPos.Add(item.ClaimPressureSNId, idxCol + offsetCol);
                        workSheet.Cells[idxRow, idxCol + offsetCol].Value = String.Format("track {0}", item.track);
                        if (idxCol + offsetCol > maxCol)
                        {
                            maxCol = idxCol + offsetCol;
                        }
                    }
                }

                // Righe / CHECK 
                string[] allChecks = {"Init","Asp","ClotAsp","ClotDisp","Disp","TipOn","TipoOff","Leak1","Leak2","Leak3"};
                foreach (var myCheck in allChecks)
                {
                    if (checks.Contains(myCheck.ToLower()))
                    {
                        idxRow++;
                        if (myCheck.StartsWith("Leak"))
                        {
                            if (myCheck.ToLower().Equals("leak1"))
                            {
                                workSheet.Cells[idxRow, 1].Value = "Leak";
                            }
                        } else 
                        {
                            workSheet.Cells[idxRow, 1].Value = myCheck;
                        }
                        var Fld = String.Format("Value{0}", myCheck);
                        // Il primo carattere del campo sempre LowCase !!! 
                        //var Fld = String.Format("{0}{1}", myCheck.Substring(0,1).ToLower(), myCheck.Substring(1));
                        foreach (var item in dto.pressuresSNfromDB)
                        {
                            var propertyInfo = item.GetType().GetProperty(Fld);
                            if (propertyInfo != null) 
                            {
                                if (aPos.TryGetValue(item.ClaimPressureSNId, out idxCol))
                                {
                                    var xxx = propertyInfo.GetValue(item, null);
                                    workSheet.Cells[idxRow, idxCol].Value = propertyInfo.GetValue(item, null);
                                    //idxCol = idxTrk[String.Format("{0}_{1}", item.TrackIdx, item.SubTrack)];
                                    //workSheet.Cells[idxRow, aPos[idxCol]].Value = propertyInfo.GetValue(item, null);
                                }
                            }
                        }
                    }
                }
                if (maxCol > 0)
                {
                    using (var range = workSheet.Cells[1, 1, idxRow, maxCol])
                    {
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
            }
            if (dto.pressuresSNfromDB != null)
            {
                workSheet = excel.Workbook.Worksheets.Add("pressuresSN (from DB)");
                workSheet.Cells[1, 1].LoadFromCollection(dto.pressuresSNfromDB, true);
            }
            if (dto.pressuresSN != null)
            {
                workSheet = excel.Workbook.Worksheets.Add("pressuresSN");
                workSheet.Cells[1, 1].LoadFromCollection(dto.pressuresSN, true);
            }
            if (dto.volumeEluates != null)
            {
                workSheet = excel.Workbook.Worksheets.Add("Eluates");
                workSheet.Cells[1, 1].LoadFromCollection(dto.volumeEluates, true);
            }
            if (dto.pressures != null)
            {
                workSheet = excel.Workbook.Worksheets.Add("pressures (from DB)");
                workSheet.Cells[1, 1].LoadFromCollection(dto.pressures, true);
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return stream;            //return newFile.FullName;
        }

        public Stream ExportLogFilesVolume(LogFileDetailDTO dto)
        {
            ExcelPackage excel = new ExcelPackage();
            OfficeOpenXml.ExcelWorksheet workSheet = null;

            workSheet = excel.Workbook.Worksheets.Add("Volume");

            //if (dto.volumeEluates != null)
            //{
            //    workSheet = excel.Workbook.Worksheets.Add("Eluates");
            //    workSheet.Cells[1, 1].LoadFromCollection(dto.volumeEluates, true);
            //    var idxRow = 1;
            //    var idxCol = 0;
            //    var offsetCol = 1;
            //    //var aTrack = dto.pressuresSNfromDB.GroupBy(d => new { d.TrackIdx, d.SubTrack})
            //    //       .Select(m => new { m.Key.TrackIdx, m.Key.SubTrack}).OrderBy(m => m.TrackIdx).ThenBy(m=>m.SubTrack).ToList();
            //    var aTrack = dto.pressuresSNfromDB.OrderBy(t => t.TrackSeq).ThenBy(t => t.TrackIdx).Select(t => t.TrackIdx).Distinct().ToList();
            //    var aPos = new int[99];
            //    // Intestazione TRACK
            //    //Dictionary<string, int> idxTrk = new Dictionary<string, int>();
            //    foreach (var item in aTrack)
            //    {
            //        idxCol++;
            //        //idxTrk[String.Format("{0}_{1}", item.TrackIdx, item.SubTrack)] = idxCol;
            //        aPos[item] = idxCol + offsetCol;
            //        //workSheet.Cells[idxRow, aPos[idxCol]].Value = String.Format("#{0}", item.TrackIdx);  // ??
            //        workSheet.Cells[idxRow, aPos[item]].Value = String.Format("track {0}", item);
            //    }
            //    // Righe / CHECK 
            //    string[] allChecks = { "Init", "Asp", "ClotAsp", "ClotDisp", "Disp", "TipOn", "TipoOff", "Leak1", "Leak2", "Leak3" };
            //    foreach (var myCheck in allChecks)
            //    {
            //        if (checks.Contains(myCheck.ToLower()))
            //        {
            //            idxRow++;
            //            if (myCheck.StartsWith("Leak"))
            //            {
            //                if (myCheck.ToLower().Equals("leak1"))
            //                {
            //                    workSheet.Cells[idxRow, 1].Value = "Leak";
            //                }
            //            }
            //            else
            //            {
            //                workSheet.Cells[idxRow, 1].Value = myCheck;
            //            }
            //            var Fld = String.Format("Value{0}", myCheck);
            //            foreach (var item in dto.pressuresSNfromDB)
            //            {
            //                var propertyInfo = item.GetType().GetProperty(Fld);
            //                if (propertyInfo != null)
            //                {
            //                    workSheet.Cells[idxRow, aPos[item.TrackIdx]].Value = propertyInfo.GetValue(item, null);
            //                    //idxCol = idxTrk[String.Format("{0}_{1}", item.TrackIdx, item.SubTrack)];
            //                    //workSheet.Cells[idxRow, aPos[idxCol]].Value = propertyInfo.GetValue(item, null);
            //                }
            //            }
            //        }
            //    }
            //    using (var range = workSheet.Cells[1, 1, idxRow, idxCol + offsetCol])
            //    {
            //        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            //        range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
            //        range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
            //        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            //        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            //    }
            //}
            //if (dto.pressuresSNfromDB != null)
            //{
            //    workSheet = excel.Workbook.Worksheets.Add("pressuresSN (from DB)");
            //    workSheet.Cells[1, 1].LoadFromCollection(dto.pressuresSNfromDB, true);
            //}
            int idxRow = 0;
            if (dto.volumeEluates != null)
            {
                idxRow += 2;
                var idxStartAt = idxRow;
                workSheet.Cells[idxRow, 1].Value = "Eluates";
                idxRow++;
                var dati = dto.volumeEluates.Select(m => new { m.track, m.volume }).ToList();
                workSheet.Cells[idxRow, 1].LoadFromCollection(dati, true);
                workSheet.Cells[idxRow, 1].Value = "Track";
                workSheet.Cells[idxRow, 2].Value = "Volume";
                idxRow += dati.Count();

                using (var range = workSheet.Cells[idxStartAt, 1, idxStartAt, 2])
                {
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
                using (var range = workSheet.Cells[idxStartAt + 1, 1, idxStartAt + 1, 2])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                }
                using (var range = workSheet.Cells[idxStartAt, 1, idxRow, 2])
                {
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }

            }
            if (dto.inventoryBlock != null)
            {
                idxRow += 2;
                var idxStartAt = idxRow;
                workSheet.Cells[idxRow, 1].Value = "Inventory Block";
                idxRow++;
                var dati = dto.inventoryBlock.Select(m => new { m.labelVolume, m.volume }).ToList();
                workSheet.Cells[idxRow, 1].LoadFromCollection(dati, true);
                workSheet.Cells[idxRow, 1].Value = "Label";
                workSheet.Cells[idxRow, 2].Value = "Volume";
                idxRow += dati.Count();

                using (var range = workSheet.Cells[idxStartAt, 1, idxStartAt, 2])
                {
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
                using (var range = workSheet.Cells[idxStartAt + 1, 1, idxStartAt + 1, 2])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                }
                using (var range = workSheet.Cells[idxStartAt, 1, idxRow, 2])
                {
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
            }

            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;
            return stream;
        }

        public Stream ExportLogFilesTemperatures(LogFileDetailDTO dto)
        {
            ExcelPackage excel = new ExcelPackage();
            OfficeOpenXml.ExcelWorksheet workSheet = null;

            workSheet = excel.Workbook.Worksheets.Add("Temperatures");
            int idxRow = 0;
            int idxCol = 1;

            if (dto.temperatures != null && dto.temperatures.Count() > 0)
            {
                idxRow = 2;
                var idxStartAt = idxRow;
                workSheet.Cells[idxRow, idxCol].Value = "PCR block & hot collar";
                idxRow++;
                var datiExp = dto.temperatures.Select(t => new
                {
                    t.idxJs,
                    t.ValueGet01,
                    t.ValueGet02,
                    t.ValueGet03,
                    t.ValueGet04,
                    t.ValueGet05,
                    t.ValueGet06,
                    t.ValueGet07,
                    t.ValueGet08,
                    t.ValueGet09,
                    t.ValueGet10,
                    t.ValueGet11,
                    t.ValueGet12,
                    t.ValueLeft,
                    t.ValueCenter,
                    t.ValueRight
                }).ToList();
                workSheet.Cells[idxRow, idxCol].LoadFromCollection(datiExp, true);
                workSheet.Cells[idxRow, idxCol].Value = "Point";
                for (int i = 1; i <= 12; i++)
                {
                    workSheet.Cells[idxRow, idxCol + i].Value = String.Format("Value {0}", i);
                }
                workSheet.Cells[idxRow, 14].Value = "Left";
                workSheet.Cells[idxRow, 15].Value = "Center";
                workSheet.Cells[idxRow, 16].Value = "Right";
                idxRow += datiExp.Count();
                using (var range = workSheet.Cells[idxStartAt, idxCol, idxStartAt, idxCol + 15])
                {
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
                using (var range = workSheet.Cells[idxStartAt + 1, idxCol, idxStartAt + 1, idxCol + 15])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                }
                using (var range = workSheet.Cells[idxStartAt, idxCol, idxRow, idxCol + 15])
                {
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
                using (var range = workSheet.Cells[idxStartAt+2 , idxCol + 1, idxRow, (idxCol + 15) - 3])
                {
                    range.Style.Numberformat.Format = "#,##0.00";
                }
                idxCol += 17;
            }

            if (dto.coolBlockExt != null && dto.coolBlockExt.Count() > 0)
            {
                idxRow = 2;
                var idxStartAt = idxRow;
                workSheet.Cells[idxRow, idxCol].Value = "Cool Block EXT";
                idxRow++;
                var datiExp = dto.coolBlockExt.Select(t => new
                {
                    t.idxJs,
                    t.ValueGet
                }).ToList();
                workSheet.Cells[idxRow, idxCol].LoadFromCollection(datiExp, true);
                workSheet.Cells[idxRow, idxCol].Value = "Point";
                workSheet.Cells[idxRow, idxCol + 1].Value = "Value";
                idxRow += datiExp.Count();
                using (var range = workSheet.Cells[idxStartAt, idxCol, idxStartAt, idxCol + 1])
                {
                    range.Merge = true;
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
                using (var range = workSheet.Cells[idxStartAt + 1, idxCol, idxStartAt + 1, idxCol + 1])
                {
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                }
                using (var range = workSheet.Cells[idxStartAt, idxCol, idxRow, idxCol + 1])
                {
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                }
            }

            if (idxRow == 0)
            {
                workSheet.Cells[3, 1].Value = "No Data Found";
                using (var range = workSheet.Cells[3, 1, 3, 1])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Font.Color.SetColor(Color.Red);
                }
            }

            int nColumns = 21;
            for (int i = 1; i <= nColumns; i++)
            {
                workSheet.Column(i).AutoFit();
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;
            return stream;
        }

        [HttpPost]
        public async Task<IActionResult> CreateSnapshotJPG(int ClaimId, string Note, string image64, string SnapshotType)
        {
            string msg = "";
            try
            {
                Claim claim = _context.Claims.Find(ClaimId);
                if (claim == null)
                {
                    return NotFound();
                }
                if (!String.IsNullOrWhiteSpace(Note) && Note.Length > 1000)
                {
                    Note = Note.Substring(0, 1000);
                }

                string dirClaim = System.IO.Path.GetDirectoryName(string.Format("{0}\\{1}", DirUploadClaim, claim.FileName));
                string relativePath = string.Format("{0}\\Snapshots", dirClaim);
                string fullDirPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), relativePath);
                if (!Directory.Exists(fullDirPath))
                {
                    Directory.CreateDirectory(fullDirPath);
                }
                // Salvataggio file IMG
                string fileName = string.Format("S{0}_{1}_{2}.jpg", claim.ClaimId, SnapshotType, DateTime.Now.ToString("yyyyMMddHHmmss"));        // system.IO.Path.GetRandomFileName()
                string fullFileName = string.Format("{0}\\{1}", fullDirPath, fileName);

                //remove the image header details
                //string trimmedData = image64.Replace("data:image/png;base64,", "");
                //convert the base 64 string image to byte array
                //byte[] uploadedImage = Convert.FromBase64String(trimmedData);

                string convert = image64.Replace("data:image/png;base64,", String.Empty);
                convert = convert.Replace("data:image/octet-stream;base64,", String.Empty);
                byte[] uploadedImage = Convert.FromBase64String(convert);

                System.IO.File.WriteAllBytes(fullFileName, uploadedImage);

                //string myImagebase64 = image64;
                //byte[] contents = Convert.FromBase64String(myImagebase64);
                //System.IO.File.WriteAllText(fullFileName, @"<?xml version=""1.0"" standalone=""no""?><!DOCTYPE svg PUBLIC ""-//W3C//DTD SVG 1.1//EN"" ""http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"">");
                //using (var stream = new FileStream(fullFileName, FileMode.Append))
                //{
                //    stream.Write(contents, 0, contents.Length);
                //}

                //ClaimSnapshot snapshot = new ClaimSnapshot()
                //{
                //    Claim = claim,
                //    FileName = string.Format("{0}\\{1}", relativePath, fileName),
                //    tipo = SnapshotType,
                //    Note = Note,
                //    TmstInse = DateTime.Now,
                //    UserInse = User.Identity.Name
                //};
                //_context.ClaimSnapshots.Add(snapshot);
                //ClaimTrack newTrack = new ClaimTrack()
                //{
                //    Claim = claim,
                //    Description = "Snapshot Created",
                //    Note = Note,
                //    TmstInse = DateTime.Now,
                //    UserInse = User.Identity.Name
                //};
                //_context.ClaimTracks.Add(newTrack);
                //_context.SaveChanges();

            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "CreateSnapshotJPG", e.Message, e.Source);
            }

            return Json(msg);
        }

        [HttpPost]
        public async Task<IActionResult> CreateChartSnapshot(int ClaimId, string Note, string image64, string SnapshotType)
        {
            string msg = "";
            try
            {
                Claim claim = _context.Claims.Find(ClaimId);
                if (claim == null)
                {
                    return NotFound();
                }
                if (!String.IsNullOrWhiteSpace(Note) && Note.Length > 1000)
                {
                    Note = Note.Substring(0, 1000);
                }

                string dirClaim = System.IO.Path.GetDirectoryName(string.Format("{0}\\{1}", DirUploadClaim, claim.FileName));
                string relativePath = string.Format("{0}\\Snapshots", dirClaim);
                string fullDirPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), relativePath);
                if (!Directory.Exists(fullDirPath))
                {
                    Directory.CreateDirectory(fullDirPath);
                }

                // Salvataggio file IMG
                string fileName = "";
                string fullFileName = "";

                if (image64.StartsWith("data:") && image64.Contains(";base64,"))
                {
                    // salvataggio JPG
                    fileName = string.Format("S{0}_{1}_{2}.jpg", claim.ClaimId, SnapshotType, DateTime.Now.ToString("yyyyMMddHHmmss"));        // system.IO.Path.GetRandomFileName()
                    fullFileName = string.Format("{0}\\{1}", fullDirPath, fileName);
                    string convert = image64.Replace("data:image/png;base64,", String.Empty);
                    convert = convert.Replace("data:image/octet-stream;base64,", String.Empty);
                    byte[] uploadedImage = Convert.FromBase64String(convert);
                    System.IO.File.WriteAllBytes(fullFileName, uploadedImage);
                } else { 
                    // Salvataggio file svg
                    fileName = string.Format("S{0}_{1}_{2}.svg", claim.ClaimId, SnapshotType, DateTime.Now.ToString("yyyyMMddHHmmss"));        // system.IO.Path.GetRandomFileName()
                    fullFileName = string.Format("{0}\\{1}", fullDirPath, fileName);
                    string myImagebase64 = image64;
                    byte[] contents = Convert.FromBase64String(myImagebase64);
                    System.IO.File.WriteAllText(fullFileName, @"<?xml version=""1.0"" standalone=""no""?><!DOCTYPE svg PUBLIC ""-//W3C//DTD SVG 1.1//EN"" ""http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"">");
                    using (var stream = new FileStream(fullFileName, FileMode.Append))
                    {
                        stream.Write(contents, 0, contents.Length);
                    }
                }
                
                ClaimSnapshot snapshot = new ClaimSnapshot()
                {
                    Claim = claim,
                    FileName = string.Format("{0}\\{1}", relativePath, fileName),
                    tipo = SnapshotType,
                    Note = Note,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimSnapshots.Add(snapshot);
                ClaimTrack newTrack = new ClaimTrack()
                {
                    Claim = claim,
                    Description = "Snapshot Created",
                    Note = Note, 
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimTracks.Add(newTrack);
                _context.SaveChanges();

            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "CreateChartSnapshot", e.Message, e.Source);
            }

            return Json(msg);
        }

        public IActionResult Create()
        {
            try
            {
                CreateClaimDTO dto = new CreateClaimDTO();
                if (Directory.Exists(dto.FtpPathName))
                {
                    var fileEntries = Directory.EnumerateFiles(dto.FtpPathName, "exportlog*.zip", SearchOption.TopDirectoryOnly);
                    foreach (string item in fileEntries)
                    {
                        dto.filesByFTP.Add(Path.GetFileName(item));
                    }
                }
                return View(dto);
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("Create - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

         
        }

        [HttpPost]
        //public async Task<IActionResult> Create(CreateClaimDTO dto,  IFormFile file, [Bind("NoteUpload,RequestedDateFrom,RequestedDateTo")] Claim newClaim)
        public async Task<IActionResult> Create(CreateClaimDTO dto, IFormFile file)
        {
            if (string.IsNullOrWhiteSpace(dto.fileSelected) && file == null)
            {
                ModelState.AddModelError("", "Select a file to upload");
                return View(dto);
            }
            if (!string.IsNullOrWhiteSpace(dto.fileSelected) && file != null)
            {
                ModelState.AddModelError("", "Select one file to upload");
                return View(dto);
            }

      
            string filebyFTP = "";
            if (!string.IsNullOrWhiteSpace(dto.fileSelected))
            {
                string ftpPath = dto.FtpPathName;
                string myFile = System.IO.Path.Combine(ftpPath, dto.fileSelected);
                if (!System.IO.File.Exists(myFile))
                {
                    ModelState.AddModelError("", "Selected file not exists in FTP Area!");
                    return View(dto);
                }
                filebyFTP = myFile;
            }
            if (file != null && file.Length == 0)
            {
                ModelState.AddModelError("", "Selected file not valid (len=0)");
                return View(dto);
            }

            Claim newClaim = dto.claim;
            newClaim.UserUpload = User.Identity.Name;
            newClaim.TmstUpload = DateTime.Now;

            string relativePath = string.Format("{0}\\{1}", newClaim.TmstUpload.ToString("yyyyMM"), Guid.NewGuid().ToString("N").ToUpper());
            string path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format("{0}\\{1}", DirUploadClaim, relativePath));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string fileName2Log = "";
            if (!string.IsNullOrWhiteSpace(filebyFTP))
            {
                string filename = System.IO.Path.Combine(path, dto.fileSelected);
                System.IO.File.Move(filebyFTP, filename);
                newClaim.FileName = System.IO.Path.Combine(relativePath, dto.fileSelected);
                fileName2Log = string.Format("{0} (FTP)", dto.fileSelected);
            } else {
                string filename = System.IO.Path.Combine(path, file.FileName);
                using (FileStream fs = System.IO.File.Create(filename))
                {
                    file.CopyTo(fs);
                }
                newClaim.FileName = System.IO.Path.Combine(relativePath, file.FileName);
                fileName2Log = string.Format("{0} (upload)", file.FileName);
            }
            newClaim.Status = Lookup.Claim_Status_NEW;
            newClaim.UserAssigned = newClaim.UserUpload;
            newClaim.TmstAssign = newClaim.TmstUpload;
            _context.Claims.Add(newClaim);

            ClaimTrack newTrack = new ClaimTrack()
            {
                Claim = newClaim,
                Description = "Upload file",
                Note = fileName2Log,
                TmstInse = newClaim.TmstUpload,
                UserInse = newClaim.UserUpload
            };
            _context.ClaimTracks.Add(newTrack);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public object GetSnapshots(DataSourceLoadOptions loadOptions, int ClaimId, string tipo)
        {
            var rows = _context.ClaimSnapshots
                               .Where(t => t.Claim.ClaimId == ClaimId && (t.tipo.Equals(tipo) || string.IsNullOrWhiteSpace(tipo)))
                               .OrderBy(t => t.TmstInse).ToList();
            //foreach (var item in rows)
            //{
            //    string fullDirPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), item.FileName);
            //    //item.picture = System.IO.File.ReadAllBytes(fullDirPath);
            //    item.picture = System.IO.Path.GetDirectoryName(string.Format("..\\..\\{0}\\{1}", DirUploadClaim, item.FileName));
            //}
            return DataSourceLoader.Load(rows, loadOptions);
        }


        //[HttpGet]
        //public object GetPressures(DataSourceLoadOptions loadOptions, int ClaimId)
        //{
        //    var data = _context.ClaimPressures.Where(t => t.Claim.ClaimId == ClaimId && t.OffSet == false)
        //                       .OrderBy(t => t.ClaimPressureId).ToList();
        //    return DataSourceLoader.Load(data, loadOptions);
        //}

        private List<ClaimPressureSNDTO> GetPressuresSN(int ClaimId, string Script, DateTime DateRif, int SessionNumber)
        {

            List<ClaimPressureSNDTO> rows = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> rowsOrig = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> retsPressOK = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> rowsPost = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> rowsPre = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> rowsInit = new List<ClaimPressureSNDTO>();
            List<ClaimPressureSNDTO> rowsSeq = new List<ClaimPressureSNDTO>();
            List<object> args = new List<object>();
            string Sql = "";

            //Sql = @"select PR.TrackSeq * 10000 + PR.trackIdx * 100 + CFG.CheckSeq as PointIdx,
            // La chiave di ordinamento viene composta come AABBCDD dove
            // - AA � il valore TrackSeq (serve per gestire il Multi-Mix (valido da 0 a 99)
            // - BB � la traccia (valido da 1 a 12)
            // - C � la sotto-traccia (valido da 0 a 9) serve per gestire le doppie letture per traccia 
            // - DDD � la sequenza configurata 
            Sql = @"select CAST(CONCAT(FORMAT(PR.TrackSeq, 'D2'), FORMAT(PR.trackIdx, 'D2'), FORMAT(pr.SubTrack, 'D1'), FORMAT(CFG.CheckSeq, 'D3')) AS INT) as PointIdx, 
                           CFG.CheckType, CFG.CheckName, CFG.CheckSeq, PR.TrackSeq, PR.ClaimPressureSNId, PR.TrackIdx, Pr.SubTrack, PR.TrackSeq,
                           PR.ValueInit as Init, PR.ValueAsp as asp, PR.ValueTipOn as tipOn, PR.ValueDisp as disp, 
	                       PR.ValueLeak1 as leak1, PR.ValueLeak2 as leak2, PR.ValueLeak3 as leak3, 
                           PR.ValueClotAsp as clotAsp, PR.ValueClotDisp as clotDisp, PR.ValueTipOff as tipOff
                      FROM ClaimPressuresSN as PR
                     INNER JOIN ClaimCheckCfgs CFG on CFG.script = PR.script
                     WHERE PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and
                           CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103) and 
                           PR.TrackIdx BETWEEN 1 AND 12 and PR.SubTrack BETWEEN 0 AND 9 and 
                           PR.TrackSeq BETWEEN 0 AND 99 and CFG.CheckSeq BETWEEN 0 AND 999
                        ORDER BY 1";

            args.Add(new { Script = Script });
            args.Add(new { ClaimId = ClaimId });
            args.Add(new { Session = SessionNumber });
            args.Add(new { DateRif = DateRif });
            try
            {
                using (IDatabase db = Connection)
                {
                    rowsOrig = db.Query<ClaimPressureSNDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetPressureSN - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

            // Analizzo tutte le righe estratte, ed in base al nome del check, creo i record per le serie (dove la pressione � significativa)
            foreach (var item in rowsOrig)
            {
                ClaimPressureSNDTO myRec = new ClaimPressureSNDTO()
                {
                    PointIdx = item.PointIdx,
                    CheckType = item.CheckType,
                    checkName = item.checkName,
                    CheckSeq = item.CheckSeq,
                    ClaimPressureSNId = item.ClaimPressureSNId,
                    TrackIdx = item.TrackIdx,
                    TrackSeq = item.TrackSeq,
                    SubTrack = item.SubTrack,
                    init = item.init
                };

                switch (item.checkName.ToLower())
                {
                    case "init":
                        myRec.pressure = item.init;
                        myRec.init = item.init;
                        break;
                    case "asp":
                        myRec.pressure = item.asp;
                        myRec.asp = item.asp;
                        break;
                    case "clotasp":
                        myRec.pressure = item.clotAsp;
                        myRec.clotAsp = item.clotAsp;
                        break;
                    case "clotdisp":
                        myRec.pressure = item.clotDisp;
                        myRec.clotDisp = item.clotDisp;
                        break;
                    case "disp":
                        myRec.pressure = item.disp;
                        myRec.disp = item.disp;
                        break;
                    case "leak1":
                        myRec.pressure = item.leak1;
                        myRec.leak = item.leak1;
                        break;
                    case "leak2":
                        myRec.pressure = item.leak2;
                        myRec.leak = item.leak2;
                        break;
                    case "leak3":
                        myRec.pressure = item.leak3;
                        myRec.leak = item.leak3;
                        break;
                    case "tipon":
                        myRec.pressure = item.tipOn;
                        myRec.tipOn = item.tipOn;
                        break;
                    case "tipoff":
                        myRec.pressure = item.tipOff;
                        myRec.tipOff = item.tipOff;
                        break;
                }
                if (myRec.pressure != null)
                    retsPressOK.Add(myRec);
            }

            // Scorro tutti i record con le Pressioni valorizzate ed elaboro i record in base al checkType corrispondente 
            int trackSeqPrec = -1;
            bool bInit = true;
            long idRecSeqFirst = 0;
            long idRecSeqPrec = 0;
            bool bShowIM = false;
            bool bCheckDoppiValori = (Script.ToLower().Equals("icdisp.scr"));
            foreach (var item in retsPressOK)
            {
                if (trackSeqPrec != item.TrackSeq)
                {
                    if (trackSeqPrec != -1)     
                    {
                        rows.AddRange(rowsInit);
                        rows.AddRange(rowsPre);
                        rows.AddRange(rowsSeq);
                        rows.AddRange(rowsPost);
                        rowsInit.Clear();
                        rowsPre.Clear();
                        rowsSeq.Clear();
                        rowsPost.Clear();
                    }
                    // inizio nuovo BLOCCO 
                    idRecSeqFirst = item.ClaimPressureSNId;
                    trackSeqPrec = item.TrackSeq;
                }

                // 0 - Operazioni iniziali (il blocco si intende fino al primo record con type <> 0)
                if (item.CheckType == 0 && bInit)
                {
                    rowsInit.Add(item);
                } else
                {
                    bInit = false;
                }
                // 1 - PRE MIX/SEQ, operazioni da eseguirsi PRIMA di ogni blocco MIX/SEQ (da eseguirsi SOLO sul primo RECORD del blocco)
                if (item.CheckType == 1 && idRecSeqFirst == item.ClaimPressureSNId)
                {

                    //// Per ICDisp.scr, in base alla configurazione, i check Leak sono definiti sia nella sezione type '1' che nella sezione type '2'
                    //// questo comporta che questi punti vengono duplicati per il primo record (quello che serve per determinare l'IM)
                    //// soluzione: aggiungere il check di tipo 1, SOLO se NON ESISTE uno stesso checkName con tipo 2 per lo stesso record ID
                    //bool bExistType2 = retsPressOK.Where(t => t.ClaimPressureSNId == item.ClaimPressureSNId && t.CheckType == 2 && 
                    //                                          t.checkName == item.checkName).Count() > 0;
                    //if (!bExistType2)
                    //{
                        item.TrackIdx = 0;  // Per forzare IM !!! 
                        rowsPre.Add(item);
                        bShowIM = true;   // Operazioni per Inventory Manager
                    //}
                }
                // 2 - Track MIX/SEQ (lette sempre)
                if (item.CheckType == 2)
                {
                    // Per ICDisp.scr, in base alla configurazione, i check Leak sono definiti sia nella sezione type '1' che nella sezione type '2'
                    // questo comporta che questi punti vengono duplicati per il primo record (quello che serve per determinare l'IM)
                    // soluzione: verifica se nell'elenco delle righe PRE gi� elaborate c'� lo stesso CheckName per lo stesso record ID 
                    if (bCheckDoppiValori)
                    {
                        var CheckType1 = rowsPre.Where(t => t.ClaimPressureSNId == item.ClaimPressureSNId && t.checkName == item.checkName);
                        if (CheckType1.Count() == 0) 
                        {
                            rowsSeq.Add(item);
                        }
                    } else
                    {
                        rowsSeq.Add(item);
                    }
                }
                // 3 - POST MIX/SEQ, operazioni da eseguirsi DOPO ogni blocco MIX/SEQ (prendo i dati dell'ultimo record !!!)
                if (item.CheckType == 3)
                {
                    if (idRecSeqPrec != item.ClaimPressureSNId)
                    {
                        rowsPost.Clear(); 
                    }
                    rowsPost.Add(item);
                    idRecSeqPrec = item.ClaimPressureSNId;
                }
            }

            rows.AddRange(rowsInit);
            rows.AddRange(rowsPre);
            rows.AddRange(rowsSeq);
            rows.AddRange(rowsPost);

            int trackIdxPrec = -1;
            int subTrackPrec = -1;
            int idxJs = 1;  // da 1 per array JS 
            foreach (var item in rows)
            {
                if (idxJs > 1)  // salto il primo punto perch� non viene visualizzato sul grafico !!! 
                {
                    if (item.TrackIdx != trackIdxPrec || item.SubTrack != subTrackPrec)
                    {
                        if (item.TrackIdx == 0 && item.SubTrack == 0)
                        {
                            if (bShowIM)
                            {
                                item.track = string.Format("IM");
                            }
                        } else
                        {
                            // track con base '20' per gestire mix successive
                            //item.track = string.Format("track {0}", (item.TrackIdx % 20));
                            //item.track = string.Format("track {0}", (item.TrackIdx));
                            //if (bShowIM)
                            //{
                            //    item.track = string.Format("#{0}", (item.TrackIdx));
                            //} else
                            //{
                            //    item.track = string.Format("track{0}", (item.TrackIdx));
                            //}
                            item.track = string.Format("{0}", (item.TrackIdx));
                        }
                        trackIdxPrec = item.TrackIdx;
                        subTrackPrec = item.SubTrack;
                    }
                }
                item.idxJs = idxJs++;
            }
            return rows;
        }

        private List<ClaimVolumeDTO> GetVolumeEluates(int ClaimId, DateTime DateRif, int SessionNumber)
        {

            List<ClaimVolumeDTO> rows = new List<ClaimVolumeDTO>();
            List<object> args = new List<object>();
            string Sql = @"select PR.trackIdx, PR.ValueLiquidVolume as volume
                             from ClaimPressuresSN  AS PR
                            where PR.claimid = @ClaimId and PR.script = @Script and PR.SessionNumber = @Session and 
                                  CONVERT(date, PR.TmstRif) = CONVERT(date, @DateRif, 103) and PR.TrackIdx BETWEEN 1 AND 12";
            Sql += @" ORDER BY PR.TrackSeq, PR.trackIdx";

            args.Add(new { Script = "dnasampledisp.scr" });
            args.Add(new { ClaimId = ClaimId });
            args.Add(new { Session = SessionNumber });
            args.Add(new { DateRif = DateRif });
            try
            {
                using (IDatabase db = Connection)
                {
                    rows = db.Query<ClaimVolumeDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetVolumeEluates - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

            int idxJs = 1;  // da 1 per array JS 
            foreach (var item in rows)
            {
                //item.track = string.Format("{0}", (item.TrackIdx % 20));
                item.track = string.Format("{0}", (item.TrackIdx));
                item.idxJs = idxJs++;
            }
            return rows;
        }

        private List<ClaimVolumeDTO> GetVolumeInventoryBlock(int ClaimId, DateTime DateRif, int SessionNumber)
        {

            List<ClaimVolumeDTO> rows = new List<ClaimVolumeDTO>();
            List<object> args = new List<object>();
            // Check da inserire nelle TRACK (in ordine, come da configurazione)
            string Sql = @"select '1' as TipoVolume, ClaimPressureSNId, ValueLiquidVolume as volume, labelVolume
                             from ClaimPressuresSN 
                            where claimid = @ClaimId and script = @ScriptICDisp and SessionNumber = @Session and 
                                  CONVERT(date, TmstRif) = CONVERT(date, @DateRif, 103) and TrackIdx BETWEEN 1 AND 12 And 
                                  labelVolume is Not null and ValueLiquidVolume IS NOT NULL
                          UNION
                            select '2' as TipoVolume, ClaimPressureSNId, ValueLiquidVolume, labelVolume
                             from ClaimPressuresSN
                            where claimid = @ClaimId and script = @ScriptPCR and SessionNumber = @Session and
                                  CONVERT(date, TmstRif) = CONVERT(date, @DateRif, 103) and TrackIdx BETWEEN 1 AND 12 And 
                                  labelVolume is Not null and ValueLiquidVolume IS NOT NULL
                            ORDER BY 1, 2";

            args.Add(new { ScriptICDisp = "ICDIsp.scr" });
            args.Add(new { ScriptPCR = "PCRreagentDisp.scr" });
            args.Add(new { ClaimId = ClaimId });
            args.Add(new { Session = SessionNumber });
            args.Add(new { DateRif = DateRif });
            try
            {
                using (IDatabase db = Connection)
                {
                    rows = db.Query<ClaimVolumeDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetVolumeInventoryBlock - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

            int idxJs = 1;  // da 1 per array JS 
            foreach (var item in rows)
            {
                item.idxJs = idxJs++;
            }
            return rows;
        }

       
        [HttpGet]
        public object GetClaimRows(DataSourceLoadOptions loadOptions, int ClaimId, string Folder, string Script)
        {
            List<ClaimRowError> rows = new List<ClaimRowError>();
            List<object> args = new List<object>();
            var Sql = @"SELECT CLAIMROWS.ERRORCODE, CLAIMROWS.NOTE, CLAIMROWS.TMSTGET, 
                               CLAIMROWS.STATUS, CLAIMROWS.PROCEDURENOTE, CLAIMROWS.SCRIPT, 
                               CLAIMROWS.SESSIONNUMBER, CLAIMROWS.OUTOFSESSION, 
                               ERRORCODES.MESSAGE, ERRORCODES.CATEGORY, ERRORCODES.ERRORTYPE, 
                               ERRORCODES.ERRORUNIT, ERRORCODES.DESCRIPTION, ERRORCODES.ERRORHANDLING
                          FROM CLAIMROWS
                          LEFT JOIN ERRORCODES ON CLAIMROWS.ERRORCODE = ERRORCODES.ERRORCODEID
                          WHERE  CLAIMROWS.CLAIMID = @ClaimId AND CLAIMROWS.FOLDER = @Folder";
            if (!String.IsNullOrWhiteSpace(Script))
            {
                Sql += @" AND CLAIMROWS.SCRIPT = @Script";

                var newVal = Lookup.LogFiles_ScriptDecode.Where(t => t.descrizione.ToLower().Equals(Script.ToLower())).Select(t => t.id).FirstOrDefault();
                if (newVal != null)
                {
                    Script = newVal;
                }
                args.Add(new { Script = Script });
            }
            Sql += @" ORDER BY CLAIMROWS.TMSTGET";
            //Sql += @" ORDER BY ERRORCODES.CATEGORY, ERRORCODES.ERRORTYPE, CLAIMROWS.ERRORCODE, CLAIMROWS.TMSTGET";
            args.Add(new { ClaimId = ClaimId });
            args.Add(new { Folder = Folder });
            try
            {
                using (IDatabase db = Connection)
                {
                    rows = db.Query<ClaimRowError>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetClaimRows - ERRORE {0} {1}", e.Message, e.Source));
                return BadRequest();
            }

            return DataSourceLoader.Load(rows, loadOptions);
        }

        [HttpGet]
        public ActionResult GetTemperatures(DataSourceLoadOptions loadOptions, int Tipo, int ClaimId, int SessionNumber, DateTime? DateRif)
        {
            string myRectype = Lookup.Claim_TempRecType_Temp;
            if (Tipo == 3)
            {
                myRectype = Lookup.Claim_TempRecType_CoolBlock;
            }
            List<ClaimTemperature> dati = new List<ClaimTemperature>();
            if (DateRif != null)
            {
                dati = GetTemperaturesGeneric(ClaimId, SessionNumber, (DateTime)DateRif, myRectype);
                // TIpo: 1 Temperature; 2 hotCollar (solo Left/Right/Center); 3 CoolBlock (solo Value)
                if (Tipo == 2)
                {
                    var datiTmp = dati.Select(t => new { t.idxJs, t.ValueLeft, t.ValueRight, t.ValueCenter });
                    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(datiTmp, loadOptions)), "application/json");
                }
                if (Tipo == 3)
                {
                    var datiTmp = dati.Select(t => new { t.idxJs, t.ValueGet });
                    return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(datiTmp, loadOptions)), "application/json");
                }
            }
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(dati, loadOptions)), "application/json");
        }

        private List<ClaimTemperature> GetTemperaturesGeneric(int ClaimId, int SessionNumber, DateTime DateRif, string recType)
        {
            var dati = _context.ClaimTemperatures.Where(t => t.Claim.ClaimId == ClaimId && t.RecType.Equals(recType) &&
                                                            t.SessionNumber == SessionNumber && t.TmstGet.Date == DateRif
                                                            ).OrderBy(t => t.ClaimTemperatureId).ToList();
            int idxJs = 1;
            foreach (var item in dati)
            {
                item.idxJs = idxJs++;
            }
            return dati;
        }

        [HttpGet]
        public object GetClaimScriptRows(DataSourceLoadOptions loadOptions, int ClaimId, string Script)
        {
            //List<ClaimScript> dati = new List<ClaimScript>();
            if (!string.IsNullOrWhiteSpace(Script))
            {
                var newVal= Lookup.LogFiles_ScriptDecode.Where(t => t.descrizione.ToLower().Equals(Script.ToLower())).Select(t => t.id).FirstOrDefault();
                if (newVal != null)
                {
                    Script = newVal;
                }
            }
            var dati = _context.ClaimScripts.Where(t => t.Claim.ClaimId == ClaimId && !string.IsNullOrWhiteSpace(Script) && t.Script.Equals(Script) ).OrderBy(t => t.TmstGet).ToList();
            return DataSourceLoader.Load(dati, loadOptions);
        }

        [HttpGet]
        public object GetClaimTracks(DataSourceLoadOptions loadOptions, int ClaimId)
        {
            var tracks = _context.ClaimTracks
                               .Where(t => t.Claim.ClaimId == ClaimId)
                               .OrderByDescending(t => t.ClaimTrackId).ToList();
            return DataSourceLoader.Load(tracks, loadOptions);
        }
        // GET:LogFiles/DeleteSnapshot/5
        public async Task<IActionResult> DeleteSnapshot(int ClaimId, int ClaimSnapshotId)
        {
            string msg = "";
            try { 
                Claim claim = _context.Claims.Find(ClaimId);
                if (claim == null)
                {
                    return NotFound();
                }
                ClaimSnapshot rec2del = _context.ClaimSnapshots.SingleOrDefault(m => m.ClaimSnapshotId == ClaimSnapshotId);
                if (rec2del == null)
                {
                    return NotFound();
                }
                _context.ClaimSnapshots.Remove(rec2del);
                ClaimTrack newTrack = new ClaimTrack()
                {
                    Claim = claim,
                    Description = "Snapshot Delete",
                    Note = null, 
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimTracks.Add(newTrack);
                await _context.SaveChangesAsync();

                // DELETE FILE ???

            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteSnapshot", e.Message, e.Source);
            }

            return Json(msg);
        }
        // GET: LogFIles/Assign
        public IActionResult Assign(int ClaimId, string userName, string NoteAssign)
        {
            if (!String.IsNullOrWhiteSpace(NoteAssign) && NoteAssign.Length > 1000)
            {
                NoteAssign = NoteAssign.Substring(0, 1000);
            }

            Claim rec2Update = _context.Claims.Find(ClaimId);
            if (rec2Update != null)
            {
                rec2Update.UserAssigned = userName;
                rec2Update.TmstAssign = DateTime.Now;
                rec2Update.NoteAssign = NoteAssign;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Claims.Update(rec2Update);
                ClaimTrack newTrack = new ClaimTrack()
                {
                    Claim = rec2Update,
                    Description = String.Format("Assigned to {0}", userName),
                    Note = NoteAssign,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimTracks.Add(newTrack);
                _context.SaveChanges();

                string email = (new UsersBO()).GetEmail(userName);
                if (!string.IsNullOrWhiteSpace(email))
                {
                    //_emailSender.SendEmailAsync(email, "LogFile - Assignment Notification", 
                    //                            string.Format("Hi! You have been assigned the Claim nr.{0}<br><br>Regards.", ClaimId));
                    // Richiesta Notifica Assegnazione Claim per State Machine 
                    ProcessRequest newRequest = new ProcessRequest()
                    {
                        ProcessType = Lookup.ProcessRequest_Type_NotifyClaimAssign,
                        ProcessParms = JsonConvert.SerializeObject(new { ClaimId = ClaimId, Email = email, userName = userName }),
                        Status = Lookup.ProcessRequest_Status_NEW,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.ProcessRequests.Add(newRequest);
                    _context.SaveChanges();
                }
            }
            return RedirectToAction("Index");
        }

        // GET: LogFIles/Close
        public IActionResult Close(int ClaimId, string NoteClose)
        {
            if (!String.IsNullOrWhiteSpace(NoteClose) && NoteClose.Length > 1000)
            {
                NoteClose = NoteClose.Substring(0, 1000);
            }

            Claim rec2Update = _context.Claims.Find(ClaimId);
            if (rec2Update != null)
            {
                rec2Update.Status = Lookup.Claim_Status_CLOSED;
                rec2Update.NoteClose = NoteClose;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Claims.Update(rec2Update);
                ClaimTrack newTrack = new ClaimTrack()
                {
                    Claim = rec2Update,
                    Description = "Claim Closed", 
                    Note = NoteClose,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimTracks.Add(newTrack);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        // GET: LogFIles/DeleteClaim
        public IActionResult DeleteClaim(int ClaimId)
        {
            Claim rec2Delete = _context.Claims.Find(ClaimId);
            if (rec2Delete != null)
            {
                string dirClaim = System.IO.Path.GetDirectoryName(string.Format("{0}\\{1}", DirUploadClaim, rec2Delete.FileName));
                string fullDirPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), dirClaim);
                try
                {
                    if (Directory.Exists(fullDirPath))
                    {
                        Directory.Delete(fullDirPath, true);
                    }
                    _context.ClaimRows.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimRows.Remove(r));
                    _context.ClaimPressures.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimPressures.Remove(r));
                    _context.ClaimPressuresSN.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimPressuresSN.Remove(r));
                    _context.ClaimSnapshots.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimSnapshots.Remove(r));
                    _context.ClaimTemperatures.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimTemperatures.Remove(r));
                    _context.ClaimTracks.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimTracks.Remove(r));
                    _context.ClaimScripts.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimScripts.Remove(r));
                    _context.Claims.Remove(rec2Delete);
                    // Richiesta Cancellazione file per State Machine 
                    ProcessRequest newRequest = new ProcessRequest()
                    {
                        ProcessType = Lookup.ProcessRequest_Type_DeleteClaim,
                        ProcessParms = JsonConvert.SerializeObject(new { ClaimId = ClaimId }),
                        Status = Lookup.ProcessRequest_Status_NEW,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.ProcessRequests.Add(newRequest);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("DeleteClaim - ERRORE {0} {1}", e.Message, e.Source));
                    return BadRequest();
                }
            }
            return RedirectToAction("Index");
        }

        // GET: LogFIles/Reopen
        public IActionResult Reopen(int ClaimId)
        {
            Claim rec2Update = _context.Claims.Find(ClaimId);
            if (rec2Update != null)
            {
                rec2Update.Status = Lookup.Claim_Status_COMPLETED;
                rec2Update.NoteClose = null;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Claims.Update(rec2Update);
                ClaimTrack newTrack = new ClaimTrack()
                {
                    Claim = rec2Update,
                    Description = "Claim Reopened",
                    Note = null, 
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ClaimTracks.Add(newTrack);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        // GET: LogFIles/Reprocessing
        public IActionResult Reprocessing(int ClaimId)
        {
            Claim rec2Update = _context.Claims.Find(ClaimId);
            if (rec2Update != null)
            {
                try
                {
                    // Cancellazione dei dati (tranne Track e claim)
                    _context.ClaimRows.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimRows.Remove(r));
                    _context.ClaimPressures.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimPressures.Remove(r));
                    _context.ClaimPressuresSN.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimPressuresSN.Remove(r));
                    _context.ClaimSnapshots.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimSnapshots.Remove(r));
                    _context.ClaimTemperatures.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimTemperatures.Remove(r));
                    _context.ClaimScripts.Where(t => t.Claim.ClaimId == ClaimId).ToList().ForEach(r => _context.ClaimScripts.Remove(r));
                    // Aggiornamento Claim 
                    rec2Update.Status = Lookup.Claim_Status_NEW;
                    rec2Update.NoteClose = null;
                    rec2Update.UserLastUpd = User.Identity.Name;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    _context.Claims.Update(rec2Update);
                    // Inserimento log 
                    ClaimTrack newTrack = new ClaimTrack()
                    {
                        Claim = rec2Update,
                        Description = "Claim Reprocessing",
                        Note = null,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.ClaimTracks.Add(newTrack);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("Reprocessing - ClaimId:{2} - ERRORE {0} {1}", e.Message, e.Source, ClaimId));
                    return BadRequest();
                }
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object GetClaims(DataSourceLoadOptions loadOptions)
        {
            // Estrazione dei CLAIM assegnati dall'utente, oppure caricati dall'utente
            // se l'utente � un supervisore, vede tutto !! 
            var data = _context.Claims.Where(t => t.UserAssigned.Equals(User.Identity.Name) || 
                                                  t.UserUpload.Equals(User.Identity.Name) ||
                                                  User.IsInRole(Lookup.Role_LogFilesSupervisor))
                                    .ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        //public ActionResult GetSessionListJson(int claimId)
        //{
        //    var data = GetDateRifList(claimId);
        //    return Json(data);
        //}


        private List<ClaimDateRifDTO> GetDateRifList(int ClaimId)
        {
            List<ClaimDateRifDTO> rows = new List<ClaimDateRifDTO>();
            List<object> args = new List<object>();
            string Sql = @"SELECT TMSTRIF as start, COUNT(*) as content FROM (
                                SELECT  CONVERT(Date, TMSTRIF) AS TMSTRIF, SESSIONNUMBER FROM CLAIMPRESSURESSN 
                                      WHERE ClaimId =  @ClaimId AND TrackIdx BETWEEN 1 AND 12
                                UNION SELECT CONVERT(Date, TMSTGET), SESSIONNUMBER FROM CLAIMPRESSURES WHERE ClaimId =  @ClaimId
                                UNION SELECT CONVERT(Date, TMSTGET), SESSIONNUMBER FROM CLAIMTEMPERATURES WHERE ClaimId =  @ClaimId
                            ) TMP 
                            WHERE SESSIONNUMBER != 0
                            GROUP BY TMSTRIF 
                            ORDER BY 1";
            args.Add(new { ClaimId = ClaimId });
            try
            {
                using (IDatabase db = Connection)
                {
                    rows = db.Query<ClaimDateRifDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetDateRifList - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            int idx = 1;
            foreach (var item in rows)
            {
                //item.id = idx++;
                //item.className = string.Format("color_{0}", item.content);
                item.content = string.Format("{0} {1}", item.content, Int32.Parse(item.content) == 1 ? "run" : "runs");
            }
            return rows;
        }

        [HttpPost]
        public IActionResult UpdateDetailJSON([FromBody]Claim DTO)
        {
            Claim claim2Update = _context.Claims.Find(DTO.ClaimId);
            if (claim2Update == null)
            {
                return NotFound();
            }
            claim2Update.RefComplaintNr = DTO.RefComplaintNr;
            if (!string.IsNullOrWhiteSpace(claim2Update.RefComplaintNr)) {
                if (claim2Update.RefComplaintNr.Length > 100) {
                    claim2Update.RefComplaintNr = claim2Update.RefComplaintNr.Substring(0, 100);
                }
            }
            claim2Update.NoteClaim = DTO.NoteClaim;
            if (!string.IsNullOrWhiteSpace(claim2Update.NoteClaim))
            {
                if (claim2Update.NoteClaim.Length > 4000)
                {
                    claim2Update.NoteClaim = claim2Update.NoteClaim.Substring(0, 4000);
                }
            }
            claim2Update.UserLastUpd = User.Identity.Name;
            claim2Update.TmstLastUpd = DateTime.Now;
            _context.Update(claim2Update);
            _context.SaveChanges();
            return Json("");
        }
    }
}
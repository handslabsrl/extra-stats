﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class InstallBaseIndexDTO
    {
        public List<LookUpString> mesi { get; set; }
        public string selMese { get; set; }
        public string datatype { get; set; }
    }
   
}


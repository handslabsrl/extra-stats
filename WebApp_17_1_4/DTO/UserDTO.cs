﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class User
    {
        [Column("ID")]
        public string userId { get; set; }
        public string userName { get; set; }
        [Column("EMAIL")]
        public string userEmail { get; set; }
        public string ruoli { get; set; }
        public string locked { get; set; }
        public string fullName { get; set; }

        public string CommercialEntity { get; set; }
        public string Country { get; set; }

        public DateTime? TmstLastChangePwd { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string userNameFull
        {
            get
            {
                string ret = this.userName;
                if (!string.IsNullOrWhiteSpace(this.fullName))
                {
                    ret += string.Format(" [{0}]", this.fullName);
                }
                return ret;
            }
        }
    }

    public class Roles
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }


    public class UsersIndexDTO
    {
        public List<User> users { get; set; }
    }

    // Estende USER
    public class UserEditDTO: User
    {
        // Attibuti USER
        //public string Country { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        //public string CommercialEntity { get; set; } 
        public string UserApproval1 { get; set; }
        public string UserApproval2 { get; set; }
        public int?  DepartmentId { get; set; }

        public List<String> UserRoles { get; set; }
 
        // elenchi per combo
        public List<CountryModel> countries { get; set; }
        public List<RegionModel> regions { get; set; }
        public List<AreaModel> areas { get; set; }
        public List<CommercialEntityModel> CommercialEntities { get; set; }
        public List<User> approvers { get; set; }
        public List<Department> departments { get; set; }

        public List<string> countriesLink { get; set; }
        public List<string> commercialEntitiesLink { get; set; }
        public List<string> customersLink { get; set; }

        public List<CustomerModel> customers { get; set; }

        // Elenco dei RUOLI 
        public List<Roles> roles { get; set; }
    }
}


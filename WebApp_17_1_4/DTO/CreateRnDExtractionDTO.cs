﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class CreateRnDExtractionDTO
    {

        public string City { get; set; }
        public string SiteCode { get; set; }
        public string commercialStatus { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }

        public bool lockRegion { get; set; }
        public bool lockArea { get; set; }


        public List<CountryModel> countries { get; set; }
        public List<CommStatusModel> commercialStatuses { get; set; }
        public List<CommercialEntityModel> commercialEntities{ get; set; }
        public List<RegionModel> regions { get; set; }
        public List<AreaModel> areas { get; set; }

        public List<string> countriesSel { get; set; }          // MultiSelezione
        public List<string> customersSel { get; set; }          // MultiSelezione
        public List<string> commercialEntitiesSel { get; set; } // MultiSelezione

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string intrumentsSelJSON { get; set; }
    }
}


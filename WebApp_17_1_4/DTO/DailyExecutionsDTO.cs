﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class DailyExecutionsDTO
    {

        //public DailyExecutionsDTO()
        //{
        //    commercialEntity = new List<string>();
        //}

        public List<ExecutionCounters> series { get; set; }
        public List<CountryModel> countries { get; set; }
        public List<RegionModel> regions { get; set; }
        public List<AreaModel> areas { get; set; }
        public List<CommStatusModel> commercialStatuses { get; set; }
        public List<CommercialEntityModel> commercialEntities{ get; set; }
        public string City { get; set; }
        public string SiteCode { get; set; }
        public string Instrument { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string commercialStatus { get; set; }
        public List<string> countriesSel { get; set; }          // MultiSelezione
        public List<string> customersSel { get; set; }          // MultiSelezione
        public List<string> commercialEntitiesSel { get; set; } // MultiSelezione
        public bool? excludeSatSun { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime? InstallDateFrom { get; set; }
        public DateTime? InstallDateTo { get; set; }
        public DateTime? ExtractionDataFrom { get; set; }
        public DateTime? ExtractionDataTo { get; set; }
        public string summary { get; set; }
        public int cntInstruments { get; set; }

        public bool lockRegion { get; set; }
        public bool lockArea { get; set; }
        public bool lockCountry { get; set; }
        public bool lockCommerialEntity { get; set; }
        public bool lockCustomer { get; set; }

        public DateTime tmstUpdCache { get; set; }

        //public List<CityModel> cities{ get; set; }}
        //public string Country { get; set; }
        //public string Customer { get; set; }
        //public List<string> commercialEntity { get; set; }
        //public string PCRTargetType { get; set; }
        //public string PCRTarget { get; set; }

    }

}


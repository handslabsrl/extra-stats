﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApp.Entity;

namespace WebApp.Data
    {
    public class WebAppDbContext : DbContext
        {

        //public WebAppDbContext()
        //    {

        //    }

        public WebAppDbContext(DbContextOptions<WebAppDbContext> options)
        : base(options)
            { }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
            optionsBuilder.EnableSensitiveDataLogging();
            }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
            {

            // Default
            //modelBuilder.Entity<Testata>()
            //    .Property(b => b.CaratteriPerRiga)
            //    .HasDefaultValue(0);
            //modelBuilder.Entity<Testata>()
            //    .Property(b => b.UscitaDom)
            //    .HasDefaultValue(false);
            //modelBuilder.Entity<Cliente>()
            //    .Property(b => b.Frazionamento)
            //    .HasDefaultValue(1);
            //modelBuilder.Entity<Cliente>()
            //   .Property(b => b.TipoArea)
            //   .HasDefaultValue("S");

            // Integrità referenziale
            modelBuilder.Entity<Quotation>()
                        .HasOne(e => e.CommercialEntity)
                        .WithMany()
                        .HasForeignKey(e => e.CommercialEntityID)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Quotation>()
                        .HasOne(e => e.InstrumentType)
                        .WithMany()
                        .HasForeignKey(e => e.InstrumentTypeID)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<QuotationElitePCRKit>()
                        .HasOne(e => e.ElitePCRKit)
                        .WithMany()
                        .HasForeignKey(e => e.ElitePCRKitId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<QuotationExtraction>()
                        .HasOne(e => e.Extraction)
                        .WithMany()
                        .HasForeignKey(e => e.ExtractionId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Training>()
                        .HasOne(t => t.TrainingType)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingTypeId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Training>()
                        .HasOne(t => t.Hotel)
                        .WithMany()
                        .HasForeignKey(t => t.HotelId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingRegistration>()
                        .HasOne(t => t.Training)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingRegistration>()
                        .HasOne(t => t.Country)
                        .WithMany()
                        .HasForeignKey(t => t.CountryId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingLog>()
                        .HasOne(t => t.Training)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingId)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Training>()
               .Property(b => b.Reserved)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.Accomodation)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.SendNews)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.Enabled)
               .HasDefaultValue(true);
            modelBuilder.Entity<ClaimTemperature>()
               .Property(b => b.ValueGet)
               .HasDefaultValue(0);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.hotelPaid)
               .HasDefaultValue(false);
            //modelBuilder.Entity<TipoPubblicita>()
            //            .HasOne(e => e.Testata)
            //            .WithMany()
            //            .HasForeignKey(e => e.testataId)
            //            .OnDelete(DeleteBehavior.Cascade);

            // Chiavi composite
            modelBuilder.Entity<ClaimCheckCfg>().HasKey(table => new
                                                { table.Script, table.CheckName, table.CheckType });
            modelBuilder.Entity<UserLink>().HasKey(table => new
                                                { table.UserID, table.TypeId, table.LinkID });

            // Indici 
            //modelBuilder.Entity<QuotationElitePCRKit>()
            //            .HasIndex(t => new { t.Quotation.QuotationId, t.ElitePCRKitId });
            modelBuilder.Entity<UploadFile>()
                        .HasIndex(t => new { t.SerialNumber, t.FileType});
            modelBuilder.Entity<DataMining>()
                        .HasIndex(t => new { t.UserInse });
            //modelBuilder.Entity<ClaimScript>()
            //            .HasIndex(t => new { t.Claim, t.Script});
            modelBuilder.Entity<TrainingLog>()
                        .HasIndex(t => new { t.TrainingId, t.EventId });
        }

        // TABELLA SENZA CLASSE CORRISPONDENTE: DATAMAIN, INSTRUMENTS, GASSAYPROGRAMS

        public DbSet<CommercialEntity> CommercialEntity { get; set; }
        public DbSet<InstrumentType> InstrumentType { get; set; }
        public DbSet<ElitePCRKit> ElitePCRKits { get; set; }
        public DbSet<Quotation> Quotations { get; set; }
        public DbSet<Extraction> Extractions { get; set; }
        public DbSet<QuotationElitePCRKit> QuotationElitePCRKits { get; set; }
        public DbSet<QuotationOpenPCRKit> QuotationOpenPCRKits { get; set; }
        public DbSet<QuotationExtraction> QuotationExtractions { get; set; }
        public DbSet<QuotationsTotal> QuotationsTotal { get; set; }
        public DbSet<QuotationsTotalPCR> QuotationsTotalPCR { get; set; }

        public DbSet<UploadFile> UploadFiles { get; set; }
        
        public DbSet<UserCfg> UserCfg { get; set; }
        public DbSet<UserLink> UserLinks { get; set; }

        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimRow> ClaimRows { get; set; }
        public DbSet<ClaimTemperature> ClaimTemperatures { get; set; }
        public DbSet<ClaimPressure> ClaimPressures { get; set; }
        public DbSet<ClaimTrack> ClaimTracks { get; set; }
        public DbSet<ClaimSnapshot> ClaimSnapshots { get; set; }
        public DbSet<ClaimScript> ClaimScripts { get; set; }
        public DbSet<ClaimPressureSN> ClaimPressuresSN { get; set; }
        public DbSet<ClaimCheckCfg> ClaimCheckCfgs { get; set; }

        public DbSet<ClaimPressureRecType> ClaimPressureRecTypes { get; set; }

        public DbSet<ErrorCode> ErrorCodes { get; set; }

        public DbSet<DataMining> DataMining { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Version> Versions { get; set; }

        public DbSet<TrainingType> TrainingTypes { get; set; }
        public DbSet<TrainingLocation> TrainingLocations { get; set; }
        public DbSet<TrainingJobPosition> TrainingJobPositions { get; set; }
        public DbSet<TrainingTypeOfParticipant> TrainingTypeOfParticipants { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<TrainingRegistration> TrainingRegistrations { get; set; }
        public DbSet<TrainingLog> TrainingLogs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates{ get; set; }
        public DbSet<Company> Companies { get; set; }

        public DbSet<ProcessRequest> ProcessRequests { get; set; }
        

        public DbSet<V_Training> V_Trainings { get; set; }

        public object UserRoles { get; internal set; }
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Entity;
using WebApp.Repository;
using System.Collections.Generic;
using System.Reflection;

namespace WebApp.Classes
{
    public static class Lookup
    {
        // ***************************************************************
        // RUOLI / SICUREZZA 
        // DASHBOARD (accesso completo oppure limitato a area/regione di competenza)
        // ***************************************************************
        public static string Role_Admin { get { return "Admin"; } }
        public static string Role_DailyExec_AssayUtil { get { return "Daily Execution + Assay Utilization"; } }
        public static string Role_Quotation { get { return "Quotation"; } }
        public static string Role_QuotationApproval { get { return "Quotation Approval"; } }
        public static string Role_QuotationBackOffice { get { return "Quotation BackOffice"; } }
        public static string Role_QuotationManager { get { return "Quotation Manager"; } }
        public static string Role_AssaysProtocols { get { return "Assays Protocols"; } }
        public static string Role_DataMining { get { return "Data Mining"; } }
        public static string Role_LogFiles { get { return "Log Files"; } }
        public static string Role_LogFilesSupervisor { get { return "Log Files Supervisor"; } }
        public static string Role_DataImport { get { return "Data Import"; } }
        public static string Role_Upload_DataMain { get { return "Upload Data Main"; } }
        public static string Role_Upload_Assays { get { return "Upload Assays List"; } }
        public static string Role_Upload_InstallBase { get { return "Upload Install Base"; } }
        public static string Role_TrainingBackOffice { get { return "Training BackOffice"; } }
        public static string Role_TrainingNotification { get { return "Training Notification"; } }
        public static string Role_RnDAnalysis { get { return "RnD Analysis"; } }

        // UserLink 
        public enum UserLinkType : int
        {
            Country = 0,
            CommercialEntity,
            Customer
        }

        // ***************************************************************
        // CLAIM 
        // ***************************************************************
        // Status 
        public static string Claim_Status_NEW { get { return "NEW"; } }
        public static string Claim_Status_COMPLETED { get { return "COMPLETED"; } }
        public static string Claim_Status_CLOSED { get { return "CLOSED"; } }
        public static string Claim_Status_ERROR { get { return "ERROR"; } }

        // Status DECO
        public static string Claim_StatusDeco_NEW { get { return "Processing"; } }
        public static string Claim_StatusDeco_COMPLETED { get { return "Process complete"; } }
        public static string Claim_StatusDeco_CLOSED { get { return "Closed"; } }
        public static string Claim_StatusDeco_ERROR { get { return "ERROR"; } }

        // Folder
        public static string Claim_Folder_Fisics { get { return "FISICS"; } }
        public static string Claim_Folder_Operation { get { return "OPERATION"; } }
        public static string Claim_Folder_System{ get { return "SYSTEM"; } }

        // View Rows 
        public static string Claim_RowsList_None { get { return "NONE"; } }
        public static string Claim_RowsList_Group1 { get { return "GROUP1"; } }

        // personalizzazione SCRIPT 
        public static List<LookUpString> LogFiles_ScriptDecode = new List<LookUpString>{
                    new LookUpString() { id = "SampleDisp.scr", descrizione = "Sample (SampleDisp.scr)"},
                    new LookUpString() { id = "ICDisp.scr", descrizione = "IC (ICDisp.scr)"},
                    new LookUpString() { id = "Extraction.scr", descrizione = "Extraction 12Nozzle (Extraction.scr)"},
                    new LookUpString() { id = "PCRReagentDisp.scr", descrizione = "MMix (PCRReagentDisp.scr)"},
                    new LookUpString() { id = "DNASampleDisp.scr", descrizione = "Eluate (DNASampleDisp.scr)"}
        };

        // Tipi di registrazione della Temperatura
        public static string Claim_TempRecType_Temp { get { return "T"; } }
        public static string Claim_TempRecType_CoolBlock { get { return "C"; } }

        // ***************************************************************
        // PROCESS REQUEST
        // ***************************************************************
        // Status
        public static string ProcessRequest_Status_NEW { get { return "NEW"; } }
        public static string ProcessRequest_Status_READY { get { return "READY"; } }
        public static string ProcessRequest_Status_ERROR { get { return "ERROR"; } }

        public static string ProcessRequest_Type_DeleteClaim { get { return "DELETE_CLAIM"; } }
        public static string ProcessRequest_Type_RnDExtractions { get { return "R&D_EXTRACTION"; } }
        public static string ProcessRequest_Type_NotifyClaimAssign { get { return "NOTIFY_CLAIM_ASSIGN"; } }


        // ***************************************************************
        // DATA MINING 
        // ***************************************************************
        // Status
        public static string DataMining_Status_NEW { get { return "NEW"; } }
        public static string DataMining_Status_READY { get { return "READY"; } }
        public static string DataMining_Status_ERROR { get { return "ERROR"; } }

        public static string DataMining_Type_RunningRate { get { return "RUNNING RATE"; } }
        public static string DataMining_Type_RunningRateDesc { get { return "Cost Per Reportable Result"; } }
        public static string DataMining_Type_RunsByType { get { return "RUNS BY TYPE"; } }
        public static string DataMining_Type_RunsByTypeDesc { get { return "Data Mining - Runs by Type"; } }
        public static string DataMining_Type_RunningByComponent { get { return "RUNNING RATE COM"; } }
        public static string DataMining_Type_RunningByComponentDesc { get { return "Billing Each Component"; } }

        // ***************************************************************
        // QUOTATIONS 
        // ***************************************************************
        // Target Type
        public static string TargetType_STD { get { return "STD"; } }
        public static string TargetType_OPEN { get { return "OPEN"; } }
        public static string TargetTypeDesc_STD { get { return "Elite PCR Kit"; } }
        public static string TargetTypeDesc_OPEN { get { return "Open PCR Kits"; } }

        public static string FileDataType_InstallBase { get { return "INSTALLBASE"; } }
        public static string FileDataType_Data { get { return "DATA"; } }
        public static string FileDataType_AssaysList { get { return "ASSAYSLIST"; } }

        //public static string RoleId_QuotationApproval { get { return "QUOTATION_APPROVAL"; } }
        //public static string RoleId_LogFiles { get { return "LOG_FILES"; } }
        public static string SampleTypesDecode(int? SampleTypes)
        {
            if (SampleTypes == null)
            {
                return "";
            }
            else
            {
                switch (SampleTypes)
                {
                    case 0: return "Sample";
                    case 1: return "Calibrator";
                    case 2: return "Control";
                    default: return SampleTypes.ToString();
                }
            }
        }

        public enum QStatus : int
        {
            //[Display(Name = "DRAFT")]
            draft = 0,
            waiting = 1,        // Waiting Approval
            approved = 2,
            deleted = 9
        }

        public enum ElitePCRKitType : int
        {
            Assay = 0,
            Control,
            Standard,
            Consumable = 9
        }
        public enum ExtractionType : int
        {
            singleplex = 0,
            noExtraction,
            multiplex
        }
        public enum AnalysisType : int
        {
            none = 0,
            qualitative,
            quantitative
        }
        public enum OpenPCRKitType : int
        {
            Assay = 0,
            Control
        }
        public enum CustomerType : int
        {
            PUBLIC = 0,
            PRIVATE = 1,
            DISTRIBUTOR = 2,
            OTHER = 3
        }
        public enum SaleType : int
        {
            TENDER = 0,
            DIRECT_SALE
        }
        public enum ContractType : int
        {
            [Display(Name = "Reagent Rental")]
            REAGENT_RENTAL = 0,
            [Display(Name = "Cash Sales (Direct Sale)")]
            SALE,
            [Display(Name = "ASR only")]
            OTHER
        }
        public enum OpenPCRKitsGGExpiry : int
        {
            NA,
            GG15 = 15,
            GG20 = 20,
            GG25 = 25,
            GG30 = 30,
            GG45 = 45,
            GG60 = 60,
            GG90 = 90
        }
        public enum OpenPCRKitControl : int
        {
            included = 0,
            not_included = 1
        }

        public static List<LookUpString> listElitePCRKitType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Assay, descrizione = Lookup.ElitePCRKitType.Assay.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Control, descrizione = Lookup.ElitePCRKitType.Control.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Standard, descrizione = Lookup.ElitePCRKitType.Standard.ToString() }
        };

        public static List<LookUpString> listExtractionType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.noExtraction, descrizione = Lookup.ExtractionType.noExtraction.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.singleplex, descrizione = Lookup.ExtractionType.singleplex.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.multiplex, descrizione = Lookup.ExtractionType.multiplex.ToString() }
        };

        public static List<LookUpString> listAnalysisType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.none, descrizione = Lookup.AnalysisType.none.ToString() },
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.qualitative, descrizione = Lookup.AnalysisType.qualitative.ToString() },
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.quantitative, descrizione = Lookup.AnalysisType.quantitative.ToString() }
        };
        public static List<LookUpString> listOpenPCRKitsGGExpiry = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.NA, descrizione = "n.a." },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG15, descrizione = "15" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG20, descrizione = "20" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG25, descrizione = "25" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG30, descrizione = "30" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG45, descrizione = "45" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG60, descrizione = "60" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG90, descrizione = "90" }
        };
        public static List<LookUpString> listOpenPCRKitControl = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitControl.included, descrizione = Lookup.OpenPCRKitControl.included.ToString().Replace("_", " ") },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitControl.not_included, descrizione = Lookup.OpenPCRKitControl.not_included.ToString().Replace("_", " ") }
        };

        // ***************************************************************
        // TRAINING
        // ***************************************************************
        // Status 
        public static string Training_Status_NEW { get { return "New"; } }
        public static string Training_Status_PUBLISHED { get { return "Published"; } }
        public static string Training_Status_CLOSED { get { return "Closed"; } }
        public static string Training_Status_ARCHIVED { get { return "Archived"; } }
        public static string Training_Status_DELETED { get { return "Deleted"; } }

        public enum TrainigVISA: int
        {
            yes_with_letter = 0,
            yes_no_letter, 
            no
        }
    
        public static List<LookUpString> listTrainigVISA = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.yes_with_letter, descrizione = "I need the VISA, please send me an Invitation letter" },
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.yes_no_letter, descrizione = "I need the VISA but I do not need the Invitation letter" },
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.no, descrizione = "I do not need the VISA" }                    
        };

        public static string TrainingRegistration_Status_NEW { get { return "New"; } }
        public static string TrainingRegistration_Status_ACCEPTED { get { return "Accepted"; } }
        public static string TrainingRegistration_Status_REFUSED { get { return "Refused"; } }

        public static List<LookUpString> listRegistrationStatus = new List<LookUpString>{
                    new LookUpString() { id = TrainingRegistration_Status_NEW, descrizione = TrainingRegistration_Status_NEW },
                    new LookUpString() { id = TrainingRegistration_Status_ACCEPTED, descrizione = TrainingRegistration_Status_ACCEPTED },
                    new LookUpString() { id = TrainingRegistration_Status_REFUSED, descrizione = TrainingRegistration_Status_REFUSED}
        };

        public static List<LookUpString> listRegistrationTitle = new List<LookUpString>{
                    new LookUpString() { id = "Mr.", descrizione = "Mr." },
                    new LookUpString() { id = "Mrs.", descrizione = "Mrs." },
                    new LookUpString() { id = "Ms.", descrizione = "Ms." },
                    new LookUpString() { id = "Miss", descrizione = "Miss" }
        };
        
        public static string TrainingRegistration_Accomodation_NOT_REQUIRED { get { return "Not Required"; } }
        public static string TrainingRegistration_Accomodation_REQUIRED { get { return "Required"; } }
        public static string TrainingRegistration_Accomodation_AWAITING { get { return "Awaiting"; } }
        public static string TrainingRegistration_Accomodation_CONFIRMED { get { return "Confirmed"; } }
        public static string TrainingRegistration_Accomodation_COMPLETED { get { return "Completed"; } }

        public static string TrainingRegistration_VISA_NOT_REQUIRED { get { return "Not Required"; } }
        //public static string TrainingRegistration_VISA_NOT_NECESSARY { get { return "Not Necessary"; } }
        public static string TrainingRegistration_VISA_REQUIRED { get { return "Required"; } }
        public static string TrainingRegistration_VISA_NOT_SENT { get { return "Not Sent"; } }
        public static string TrainingRegistration_VISA_SENT { get { return "Completed"; } }

        public const string glb_sessionkey_TrainingIndex_ShowDeleted = "_TrainingIndex_ShowDeleted";
        public const string glb_sessionkey_TrainingIndex_ShowArchived = "_TrainingIndex_ShowArchived";

        public const string Training_Email_NewRegistration = "_NEWREG";
        public const string Training_Email_NotifyNewRegistration = "_NOTIFY_NEWREG";
        public const string Training_Email_ConfRegistration = "_CONF_REG";
        public const string Training_Email_RefuseRegistration = "_REJECT_REG";
        public const string Training_Email_ConfHotel = "_CONF_HOTEL";
        public const string Training_Email_InvitationLetter = "_INVITATION_LETTER";
        public const string Training_Email_HOTEL = "***HOTEL***";  // da Hotel

        // ***************************************************************
        // GENERAL
        // ***************************************************************
        public static string Function_Documentation_DailyExecution { get { return "Daily Execution"; } }
        public static string Function_Documentation_AssayUtilization { get { return "Assay Utilization"; } }
        public static string Function_Documentation_UserRoles { get { return "User Roles"; } }
        public static string Function_Documentation_DataImport { get { return "Data Import"; } }
        public static string Function_Documentation_DataMining { get { return "Data Mining"; } }
        public static string Function_Documentation_LogFiles { get { return "Log Files"; } }
        public static string Function_Documentation_UserManual { get { return "User Manual"; } }
        public static string Function_Documentation_RnDExtractions { get { return "RnD Analysis"; } }

        public static string idCache_DailyExecutionsFilters = "DailyExecutionsFilters";
        public static string idCache_AssaysUtilizationFilters = "AssaysUtilizationFilters";

        public static string SpecialMode_ChangePassword = "ChangePassword";

        private static string cache_connectionString = "";
        public static string getConnectionString
        {
            get
            {   
                // x ottimizzare gli accessi, viene letto solo la prima volta
                if (String.IsNullOrWhiteSpace(cache_connectionString))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_connectionString = configurationBuilder.Build().GetConnectionString("NPOCOConnection");
                }
                return cache_connectionString;
            }
        }

        public static string GetConfigAdminEmail
        {
            get
            {   
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("Config:Admin").Value;
            }
        }

        public static string GetEnvironment
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("Config:Environment").Value;
            }
        }

        private static string cache_version = "";
        public static string getVersion
        {
            get
            {
                // x ottimizzare gli accessi, viene letto solo la prima volta
                if (String.IsNullOrWhiteSpace(cache_version))
                {
                    List<WebApp.Entity.Version> dati = (new VersionsBO()).Get();
                    //string WebApp = dati.Where(t => t.Code.Equals("WEBAPP")).Select(t => t.Description).FirstOrDefault();
                    //string StateMachine = dati.Where(t => t.Code.Equals("STATEMACHINE")).Select(t => t.Description).FirstOrDefault();
                    Assembly asm = Assembly.GetEntryAssembly();
                    FileInfo fi = new FileInfo(asm.Location);
                    DateTime myBuildDate = fi.LastWriteTime;
                    string build = String.Format("{0}.{1}.{2}.{3}", myBuildDate.Year - 2018, myBuildDate.DayOfYear, myBuildDate.Hour, myBuildDate.Minute);
                    cache_version = String.Format("WebApp {0} Build {1} - {2} {3}", "1.2", build, 
                                                                dati.Select(t => t.Code).FirstOrDefault(), 
                                                                dati.Select(t => t.Description).FirstOrDefault());
                }
                return cache_version;
            }
        }
    }
}





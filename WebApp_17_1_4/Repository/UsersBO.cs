﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class UsersBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public UsersBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // lettura email
        public string GetEmail(String userName)
        {

            var ret = "";
            List<object> args = new List<object>();

            var Sql = @"SELECT EMAIL FROM ASPNETUSERS WHERE UPPER(UserName) = @userName";
            args.Add(new { userName = userName.ToUpper() });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // lettura UserId
        public string GetUserId(String userName)
        {
            var ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT ID FROM ASPNETUSERS WHERE UserName = @userName";
            args.Add(new { userName = userName });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetUserId() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        public void SetNormalizedUserName(string userName)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE ASPNETUSERS SET NORMALIZEDUSERNAME = NORMALIZEDEMAIL  WHERE USERNAME = @userName";
            args.Add(new { userName = userName });

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SetNormalizedUserName() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;

        }

        public void SetLastChangePassword(string userName)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE USERCFG  SET TMSTLASTCHANGEPWD = GETDATE()
                          FROM USERCFG 
                          INNER JOIN ASPNETUSERS
				          ON USERCFG.USERID = ASPNETUSERS.ID
						  where ASPNETUSERS.USERNAME = @userName";
            args.Add(new { userName = userName });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SetLastChangePassword() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }

        public bool CheckPasswordExpired(String userName = "", string email = "")
        {
            bool ret = false;
            User user = null;
            if (!string.IsNullOrWhiteSpace(email))
            {
                user = this.Get(email: email).FirstOrDefault();
            }
            if (!string.IsNullOrWhiteSpace(userName))
            {
                user = this.Get(this.GetUserId(userName)).FirstOrDefault();
            }

            if (user != null)
            {
                if (user.TmstLastChangePwd == null || user.TmstLastChangePwd < DateTime.Now.AddYears(-1))
                {
                    ret = true;
                }
            }
            return ret;
        }


        public void setUserEmail(string userName, string email)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE ASPNETUSERS SET EMAIL = @email , NORMALIZEDEMAIL=@normalizedEmail WHERE USERNAME = @userName";
            args.Add(new { userName = userName });
            args.Add(new { email = email });
            args.Add(new { normalizedEmail = email.ToUpper() });

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                    SetNormalizedUserName(userName);
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.setUserEmail() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;

        }

        // elenco USER
        public List<User> Get(string id=null, string filterRoleName = "", string email = "", string supportForCommEntity = "", string supportForCountry = "")
        {
            var ret = new List<User>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASPNETUSERS.ID, ASPNETUSERS.USERNAME, ASPNETUSERS.EMAIL, 
                            IIF(ASPNETUSERS.LockoutEnd IS NULL, '',  'Locked') AS LOCKED,  
                            USERCFG.FULLNAME, USERCFG.TMSTLASTCHANGEPWD
                          FROM ASPNETUSERS
                          LEFT JOIN USERCFG 
				          ON USERCFG.USERID = ASPNETUSERS.ID";
            if (!string.IsNullOrWhiteSpace(filterRoleName))
            {
                Sql += @"  INNER JOIN ASPNETUSERROLES
                            ON ASPNETUSERROLES.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETROLES 
                            ON ASPNETROLES.ID = ASPNETUSERROLES.RoleId
                            AND ASPNETROLES.Name =  @roleName";
                args.Add(new { roleName  = filterRoleName });
            }
            if (!string.IsNullOrWhiteSpace(supportForCommEntity) || !string.IsNullOrWhiteSpace(supportForCountry))
            {
                // estrai solo gli utenti configurati come SUPPERT (il controllo sulla commercial Entity è fatto dopo)
                Sql += @"  INNER JOIN Departments
                            ON  USERCFG.DepartmentId = Departments.DepartmentId
                            AND Departments.DepartmentType = 'SUPPORT'";
            }
            Sql += " WHERE 1 = 1 ";

            if (!string.IsNullOrWhiteSpace(id))
            {
                Sql += " AND ID = @id";
                args.Add(new { id = id  });
            }
            if (!string.IsNullOrWhiteSpace(email))
            {
                Sql += " AND UPPER(ASPNETUSERS.EMAIL) = @email";
                args.Add(new { email = email });
            }

            if (!string.IsNullOrWhiteSpace(supportForCommEntity))
            {
                // configurato sulla Commercial Entity passata, oppure SENZA nessuna Commercial Entity configurate 
                Sql += @" 	AND (EXISTS (SELECT 1 FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCommerciale
					                        AND UPPER(USERLINKS.LINKID) = UPPER(@CommercialEntity)
					                        AND USERLINKS.USERID = ASPNETUSERS.ID)
		                            OR   (SELECT COUNT(*) FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCommerciale
					                        AND USERLINKS.USERID = ASPNETUSERS.ID) = 0)";
                args.Add(new { TypeIdCommerciale = (int)Lookup.UserLinkType.CommercialEntity });
                args.Add(new { CommercialEntity = supportForCommEntity.ToUpper() });
            }
            if (!string.IsNullOrWhiteSpace(supportForCountry))
            {
                // configurato sulla Country passata, oppure SENZA nessuna Country configurata
                Sql += @" 	AND (EXISTS (SELECT 1 FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCountry
					                        AND UPPER(USERLINKS.LINKID) = UPPER(@Country)
					                        AND USERLINKS.USERID = ASPNETUSERS.ID)
		                            OR   (SELECT COUNT(*) FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCountry
					                        AND USERLINKS.USERID = ASPNETUSERS.ID) = 0)";
                args.Add(new { TypeIdCountry = (int)Lookup.UserLinkType.Country });
                args.Add(new { Country = supportForCountry.ToUpper() });
            }
            Sql += " ORDER BY USERNAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<User>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // userCfg
        public UserCfg getUserCfg(string UserId)
        {
            UserCfg ret = new UserCfg();
            List<object> args = new List<object>();
            var Sql = @"SELECT * FROM USERCFG 
				          WHERE USERID = @UserId";
            args.Add(new { UserId = UserId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = (db.Query<UserCfg>(Sql, args.ToArray()).ToList()).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.getUserCfg() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco ROLES
        public List<String> GetRoles(string userId, string fieldName = "NAME")
        {
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql =  $@"SELECT DISTINCT {fieldName} FROM ASPNETUSERROLES 
                            INNER JOIN ASPNETROLES
                            ON ASPNETROLES.ID = ASPNETUSERROLES.ROLEID
                            WHERE USERID = @userId 
                            ORDER BY 1";
            args.Add(new { userId = userId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<String>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetRoles() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco LINKS
        public List<String> GetLinks(string userId, Lookup.UserLinkType TypeId)
        {
            var ret = new List<string>();
            List<object> args = new List<object>();

            var Sql = $@"SELECT DISTINCT LINKID 
                            FROM USERLINKS
                           WHERE USERID = @userId 
                            AND TYPEID = @TypeId
                            ORDER BY LINKID";
            args.Add(new { userId = userId });
            args.Add(new { TypeId = (int)TypeId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<String>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetLinks() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // Aggiornamento LINKS
        public void UpdateLinks(string userId, Lookup.UserLinkType TypeId, List<String> links, string UserUpd)
        {
            var ret = new List<string>();
            List<object> args = new List<object>();

            string Sql = @"DELETE USERLINKS WHERE USERID = @UserId AND TYPEID = @TypeId";
            args.Add(new { UserId = userId });
            args.Add(new { TypeId = (int)TypeId });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.UpdateLinks()/DELETE - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

            if (links !=  null)
            {
                foreach (var item in links)
                {
                    args.Clear();
                    Sql = @"INSERT INTO UserLinks(UserID, TypeId, LinkID, TmstLastUpd, UserLastUpd) 
                                VALUES (@UserId, @TypeId, @LinkId, GETDATE(), @UserUpd)";
                    args.Add(new { UserId = userId });
                    args.Add(new { TypeId = (int)TypeId });
                    args.Add(new { LinkId = item});
                    args.Add(new { UserUpd = UserUpd });
                    try
                    {
                        using (IDatabase db = Connection)
                        {
                            db.Execute(Sql, args.ToArray());
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Error(string.Format("UsersBO.UpdateLinks()/INSERT - ERRORE {0} {1}", e.Message, e.Source));
                        throw;
                    }
                }
            }
            return;
        }



        public void ChangeStatus(string id)
        {
            List<object> args = new List<object>();
            string Sql = "";
            User user2upd = Get(id).FirstOrDefault();
            if (user2upd != null)
            {
                Sql = @"UPDATE ASPNETUSERS SET ";
                if (String.IsNullOrWhiteSpace(user2upd.locked))
                {
                    Sql += " LOCKOUTEND = @dataEnd";
                    args.Add(new { dataEnd = new DateTime(2099, 12, 31)});
                } else
                {
                    Sql += @" LOCKOUTEND = NULL";
                }
                Sql += "  WHERE ID = @id";
                args.Add(new { id = id });
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.ChangeStatus() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }

        // elenco RUOLI
        public List<Roles> GetAllRoles()
        {
            var ret = new List<Roles>();
         
            var Sql = @"SELECT ID, NAME FROM ASPNETROLES ORDER BY NAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Roles>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetAllRoles() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public void RemoteAllRoles(string id)
        {
            List<object> args = new List<object>();
            string Sql = @"DELETE ASPNETUSERROLES WHERE USERID = @id";
            args.Add(new { id = id });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.RemoteAllRoles() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }
        public void InsertRoles(string id, string RoleId)
        {
            List<object> args = new List<object>();
            string Sql = @"INSERT INTO ASPNETUSERROLES (USERID, ROLEID) VALUES (@id, @RoleId)";
            args.Add(new { id = id });
            args.Add(new { RoleId = RoleId });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.InsertRoles() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }

    }
}

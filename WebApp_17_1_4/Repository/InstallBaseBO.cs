﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class InstallBaseBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public InstallBaseBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco FILE caricati
        public List<UploadFilesModel> GetDataImport(String periodReference, string UserId)
        {
            var ret = new List<UploadFilesModel>();
            List<object> args = new List<object>();
            List<string> countriesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
            List<string> commercialEntitiesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
            // per ogni STRUMENTO vengono visualizzati i riferimenti al file DATA e/o ASSAYSLIST che sono caricati per il periodo 
            // Nella tabella UPLOADFILES posso aver caricato un riferimento parziale che verrà poi risolto dalla STATE MACHINE quando elabora il file 
            // x visualizzare questi riferimenti parziali, estraggo da UploadFiles i SerialNumber che NON esistono su Instruments (che non siano cancellati)
            var Sql = @"SELECT * FROM (
                        SELECT INSTR.SerialNumber , INSTR.ORIGINE, INSTR.CompanyName, INSTR.Site_Description, INSTR.Site_City,  
                               INSTR.Site_Country, INSTR.CommercialEntity, INSTR.Region, instr.Area, INSTR.Commercial_Status,
                               DATA.id as id_data, COALESCE(DATA.Status, 'MISSING') AS status_data, DATA.Filename as filename_data, 
                               DATA.TmstUpload as TmstUpload_data, DATA.UserUpload as UserUpload_data, DATA.TmstProcess as TmstProcess_data, 
                               DATA.Note as Note_data, DATA.RevisionExtra as revExtra_data,
                               ASSAYSLIST.id as id_assayslist, COALESCE(ASSAYSLIST.Status, 'MISSING') AS status_assayslist, ASSAYSLIST.Filename as filename_assayslist, 
                               ASSAYSLIST.TmstUpload as TmstUpload_assayslist, ASSAYSLIST.UserUpload as UserUpload_assayslist, ASSAYSLIST.TmstProcess as TmstProcess_assayslist,
                               ASSAYSLIST.Note as Note_assayslist, ASSAYSLIST.RevisionExtra as revExtra_assayslist
                            --FROM Instruments INSTR
                              FROM (
		                            SELECT 'INSTRUMENTS' as Origine, SerialNumber, CompanyName, Site_Description, Site_City, Site_Country, 
                                           CommercialEntity, Region, Area, Commercial_Status, Command
		                              FROM INSTRUMENTS
		                            UNION SELECT 'UPLOADFILES', SerialNumber, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
			                          FROM UPLOADFILES A WHERE SerialNumber != @InstallBase AND Status != 'DELETE'
                                                        AND not exists (select 1 from Instruments B where A.SerialNumber = B.SerialNumber)
                                ) INSTR
                            LEFT JOIN UploadFiles DATA
	                                ON DATA.id = (select max(id) FROM UploadFiles 
                                                WHERE  SerialNumber = INSTR.SerialNumber AND FileType= 'DATA' 
                                                   AND ReferencePeriod = @periodReference AND Status != 'DELETE')
                            LEFT JOIN UploadFiles ASSAYSLIST
	                                ON ASSAYSLIST.id = (select max(id) FROM UploadFiles 
                                                WHERE  SerialNumber = INSTR.SerialNumber AND FileType= 'ASSAYSLIST' 
                                                   AND ReferencePeriod = @periodReference AND Status != 'DELETE')
                            --LEFT JOIN UploadFiles INSTALLBASE
	                        --        ON INSTALLBASE.id = (select max(id) FROM UploadFiles 
                            --                    WHERE  SerialNumber = INSTR.SerialNumber AND FileType= 'INSTALLBASE' 
                            --                       AND ReferencePeriod = @periodReference AND Status != 'DELETE')
							INNER JOIN UserCfg
							 ON Usercfg.UserID = @UserId
							 AND (UserCFg.area IS NULL or UserCFg.area = INSTR.Area)
							 AND (UserCFg.Region IS NULL or UserCFg.Region = INSTR.REGION)";
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND INSTR.Site_Country IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.CommercialEntity) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }            
            Sql += @" WHERE COALESCE(INSTR.Command, '') <> @CommandDelete
                    ) AS AAA
                    --WHERE Origine = 'INSTRUMENTS' OR status_data !='MISSING' OR status_assayslist != 'MISSING'
                    ORDER BY SerialNumber";

            args.Add(new { periodReference = periodReference });
            args.Add(new { CommandDelete = "DELETE" });
            args.Add(new { UserId = UserId });
            args.Add(new { InstallBase = Lookup.FileDataType_InstallBase });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<UploadFilesModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstallBaseBO.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco FILE caricati
        public List<UploadFilesModel> GetInstallBase()
        {
            var ret = new List<UploadFilesModel>();
            var Sql = @"SELECT id as id_installbase, Status AS status_installbase, Filename as filename_installbase, 
                               TmstUpload as TmstUpload_installbase, UserUpload as UserUpload_installbase, TmstProcess as TmstProcess_installbase,
                               Note as Note_installbase,  RevisionExtra as revExtra_installbase
                        FROM UploadFiles
	                    WHERE FileType= 'INSTALLBASE' 
                        AND   Status != 'DELETE'
                        ORDER BY id DESC";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<UploadFilesModel>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstallBaseBO.GetInstallBase() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // Estrazione SerialNumber partendo da un pezzo del codice (x UPLOAD)
        // Controlla anche che sia abilitato per l'utente 
        // Se ne vengono trovati più di uno, restituisce "*" !!! 
        public string GetSerialNumberEnabled(String id, string UserId)
        {
            //string ret = "";
            var ret = new List<string>();
            List<object> args = new List<object>();
            List<string> countriesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
            List<string> commercialEntitiesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
            var Sql = @"SELECT INSTR.SERIALNUMBER 
	                      FROM INSTRUMENTS INSTR
	                    INNER JOIN USERCFG
	                      ON  Usercfg.UserID = @UserId
    	                  AND (USERCFG.AREA IS NULL OR USERCFG.AREA = INSTR.AREA)
                          AND (USERCFG.REGION IS NULL OR USERCFG.REGION = INSTR.REGION)";
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND INSTR.Site_Country IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.CommercialEntity) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            Sql += @" WHERE INSTR.SERIALNUMBER LIKE @serialNumberLike";
            args.Add(new { serialNumberLike = string.Format("%{0}", id) });
            args.Add(new { UserId = UserId });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<string>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstallBaseBO.GetSerialNumberEnabled() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            
            if (ret.Count == 1) return ret.First();
            if (ret.Count > 1) return "*";

            return null;
        }
    }
}

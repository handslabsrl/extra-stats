﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class AssaysBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public AssaysBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco AssaysProtocols
        public AssaysProtocolsModel GetAssaysProtocols(AssaysProcotolsIndexDTO dto, string UserId)
        {

            var ret = new AssaysProtocolsModel();
            List<object> args = new List<object>();
            var SqlFields = "";
            var SqlFrom = "";
            var SqlWhere = "";
            var SqlOrderBy = "";

            SqlFields += @"SELECT DISTINCT ASSAY.ASSAYNAME as assay_name, 
                                           ASSAY.PATHOGEN_TARGETNAME_NORM as pathogen_name, 
                                           INSTR.SITE_COUNTRY as Country, INSTR.SITE_CITY as City, 
                                           INSTR.COMPANYNAME as Customer, INSTR.SITE_DESCRIPTION as Site, 
                                           INSTR.SERIALNUMBER as SerialNumber, 
                                           INSTR.COMMERCIALENTITY as CommercialEntity, 
                                           INSTR.COMMERCIAL_STATUS as CommercialStatus,
                                           ASSAY.FileName as FileName, 
                                           ASSAY.ASSAYPROTOCOLSTATUS as AssayProtocolStatus";
            SqlFrom = @"FROM GASSAYPROGRAMS ASSAY 
                        INNER JOIN INSTRUMENTS INSTR
                          ON ASSAY.SERIAL = INSTR.SERIALNUMBER";
            //SqlFrom = @"FROM GASSAYPROGRAMS ASSAY 
            //            INNER JOIN DATAMAIN DM
            //              ON  DM.GENERALASSAYID = ASSAY.ASSAYID
            //              AND DM.SERIALNUMBER = ASSAY.SERIAL
            //            INNER JOIN INSTRUMENTS INSTR
            //              ON DM.SERIALNUMBER = INSTR.SERIALNUMBER";
            SqlOrderBy = "ORDER BY ASSAY.ASSAYNAME, ASSAY.PATHOGEN_TARGETNAME_NORM";

            SqlWhere = @" WHERE 1=1";
            // Filtri 
            // Country (multiselezione)
            List<string> valoriCountries = dto.countriesSel;
            if (valoriCountries == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                if (links.Count() > 0)
                {
                    valoriCountries = links;
                }
            }
            if (valoriCountries != null)
            {
                SqlWhere += " AND INSTR.SITE_COUNTRY IN (@countriesList)";
                var arr = valoriCountries.Select(el => el).ToList();
                args.Add(new { countriesList = arr });
                ret.filters.Add("Country", string.Join(" / ", arr.ToArray()));
            }
            else
            {
                if (String.IsNullOrWhiteSpace(dto.City) && dto.customersSel == null && String.IsNullOrWhiteSpace(dto.Instrument) &&
                        String.IsNullOrWhiteSpace(dto.Region) && String.IsNullOrWhiteSpace(dto.Area))
                {
                    ret.filters.Add("Country", "ALL");
                }
            }
            // City
            if (!String.IsNullOrWhiteSpace(dto.City))
            {
                SqlWhere += " AND INSTR.SITE_CITY = @City";
                args.Add(new { City = dto.City });
                ret.filters.Add("City", dto.City);
            }
            // Customers (multiselezione)
            List<string> valoriCustomers = dto.customersSel;
            if (valoriCustomers == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                if (links.Count() > 0)
                {
                    valoriCustomers = links;
                }
            }
            if (valoriCustomers != null)
            {
                SqlWhere += " AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                List<string> arr = valoriCustomers.Select(el => el.ToUpper()).ToList();
                args.Add(new { customersList = arr });
                List<string> arrDeco = new List<string>(arr);
                for (int i = 0; i < arr.Count; i++)
                {
                    arrDeco[i] = new CustomersBO().GetDesc(arr[i]);
                }
                ret.filters.Add("Customer", string.Join(" / ", arrDeco.ToArray()));
            }
            // SiteCode
            if (!String.IsNullOrWhiteSpace(dto.SiteCode))
            {
                SqlWhere += " AND INSTR.SITE_CODE = @SiteCode";
                args.Add(new { SiteCode = dto.SiteCode });
                ret.filters.Add("Site", (new SitesBO().GetDesc(dto.SiteCode, (dto.customersSel == null ? "" : string.Join(",", dto.customersSel)))));
            }
            // Instrument
            if (!String.IsNullOrWhiteSpace(dto.Instrument))
            {
                SqlWhere += " AND INSTR.SerialNumber = @SerialNumber";
                args.Add(new { SerialNumber = dto.Instrument });
                ret.filters.Add("Instrument", dto.Instrument);
            }
            // Region 
            if (!String.IsNullOrWhiteSpace(dto.Region))
            {
                SqlWhere += " AND INSTR.REGION = @Region";
                args.Add(new { Region = dto.Region });
                ret.filters.Add("Region", dto.Region);
            }
            // Area 
            if (!String.IsNullOrWhiteSpace(dto.Area))
            {
                SqlWhere += " AND INSTR.AREA = @Area";
                args.Add(new { Area = dto.Area });
                ret.filters.Add("Area", dto.Area);
            }
            // CommercialStatus 
            if (!String.IsNullOrWhiteSpace(dto.commercialStatus))
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIAL_STATUS) = @CommercialStatus";
                args.Add(new { CommercialStatus = dto.commercialStatus.ToUpper() });
                ret.filters.Add("Commercial Status", dto.commercialStatus.ToUpper());
            }
            // CommercialEntity (Multiselezione) 
            List<string> valoriCommercialEntities = dto.commercialEntitiesSel;
            if (valoriCommercialEntities == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                if (links.Count() > 0)
                {
                    valoriCommercialEntities = links;
                }
            }
            if (valoriCommercialEntities != null)
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitySel)";
                List<string> arr = valoriCommercialEntities.Select(el => el.ToUpper()).ToList();
                args.Add(new { commercialEntitySel = arr });
                ret.filters.Add("Commercial Entity", string.Join(" / ", arr.ToArray()));
            }
            // SampleType
            if (dto.SampleType != null)
            {
                SqlWhere += " AND ASSAY.SAMPLETYPES = @SampleType";
                args.Add(new { SampleType = dto.SampleType });
                ret.filters.Add("Sample Type", Lookup.SampleTypesDecode(dto.SampleType));
            }
            // PathogenName 
            if (!String.IsNullOrWhiteSpace(dto.PathogenName))
            {
                SqlWhere += " AND ASSAY.PATHOGEN_TARGETNAME_NORM = @PathogenName";
                args.Add(new { PathogenName = dto.PathogenName });
                ret.filters.Add("Pathogen Name", dto.PathogenName);
            }
            // Assay Name
            if (!String.IsNullOrWhiteSpace(dto.AssayName))
            {
                SqlWhere += " AND ASSAY.ASSAYNAME = @AssayName";
                args.Add(new { AssayName = dto.AssayName });
                ret.filters.Add("Assay Name", dto.AssayName);
            }
            // Installation Date FROM/TO 
            if (dto.InstallDateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) >= CONVERT(datetime, @InstallDateFrom, 103)";
                args.Add(new { InstallDateFrom = ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date From", ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.InstallDateTo != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) <= CONVERT(datetime, @InstallDateTo, 103)";
                args.Add(new { InstallDateTo = ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date To", ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy"));
            }

            // Estrazione Dati SERIE
            var sqlDetail = "";
            sqlDetail = SqlFields + " " + SqlFrom + " " + SqlWhere + " " + SqlOrderBy;
            try
            {
                using (IDatabase db = Connection)
                {
                    ret.AssaysProtocolsDetails = db.Query<AssaysProtocolsDetails>(sqlDetail, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetAssaysProtocols() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // elenco Pathogens
        public List<PathogenModel> GetPathogens()
        {
            var ret = new List<PathogenModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT PATHOGEN_TARGETNAME_NORM as NAME 
                          FROM GASSAYPROGRAMS 
                         ORDER BY PATHOGEN_TARGETNAME_NORM";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<PathogenModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetPathogens() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco assayNames
        public List<AssayModel> GetNames()
        {
            var ret = new List<AssayModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASSAYNAME as NAME 
                          FROM GASSAYPROGRAMS 
                         ORDER BY ASSAYNAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<AssayModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetNames() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

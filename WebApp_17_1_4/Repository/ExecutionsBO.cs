﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class ExecutionsBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        //public static IConfiguration Configuration { get; set; }

        public enum getType : int
        {
            series,
            exportDetail,
            exportSeries,
            exportDetailShort
        }

        public ExecutionsBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco DailyExecutions
        public ExecutionModel GetDailyExecutions(DailyExecutionsDTO dto, string UserId, getType tipo = getType.series)
        {
             
            var ret = new ExecutionModel();
            List<object> args = new List<object>();
            var SqlFields = "";
            var SqlFrom = "";
            var SqlWhere = "";

            if (tipo == getType.series)
            {
                // Stati: V (VALID), I (Invalid), E (Error), A (Aborted)
                //SqlFields += @"SELECT DATEADD(dd, 0, DATEDIFF(dd, 0,DM.STARTRUNTIME)) AS STARTRUNTIME, 
                SqlFields += @"SELECT FORMAT(DATEADD(dd, 0, DATEDIFF(dd, 0,DM.STARTRUNTIME)), 'dd/MM/yyyy', 'en-US') AS STARTRUNTIME, 
                                FORMAT(DM.STARTRUNTIME, 'yyyy/MM/dd', 'en-US') AS STARTRUNTIME_ORDER, 
                                DM.SERIALNUMBER,
                                -- n.Test Totali  (Validi + Invalidi + Error , esclusi Aborted)
                                IIF(DM.RUNSTATUS IN ('V', 'I', 'E'), 1, 0) AS RUN_OK,
                                -- n.Test ERROR
                                IIF(DM.RUNSTATUS IN ('E'), 1, 0) AS ERROR,
                                -- n.Test ABORTED
                                IIF(DM.RUNSTATUS IN ('A'), 1, 0) AS ABORTED,
                                -- PCR or EXTRACTION+PCR (Validi + Invalidi + Error , esclusi Aborted)
                                IIF(DM.PROCESS IN(0, 1) AND DM.RUNSTATUS IN ('V', 'I', 'E'), 1, 0) AS PROCESS_PCR,
                                -- EXTRACTION+PCR + EXTRACTION (Validi + Invalidi + Error , esclusi Aborted)
                                IIF(DM.PROCESS IN(1, 2) AND DM.RUNSTATUS IN ('V', 'I', 'E'), 1, 0) AS PROCESS_EXTR,
                                -- INVALID (conteggiati nel totale)
                                IIF(DM.RUNSTATUS = 'I', 1, 0) AS INVALID";
            }
            else if (tipo == getType.exportDetail || tipo == getType.exportDetailShort)
            {
                SqlFields += @"SELECT DM.ID, DM.RUNSETUPID, DM.TRACKNUMBER, DM.SAMPLEIDSTRING, DM.GENERALASSAYID, DM.ASSAYNAME, DM.SAMPLETYPES, 
                                DM.SAMPLEMATRIXNAME, DM.PROCESS, DM.POSITIONTOPLACESAMPLE, DM.SONICATION, DM.SONICATIONONTIME, DM.SONICATIONOFFTIME, 
                                DM.SONICATIONCYCLE, DM.MELT, DM.REFELUTETRACK, DM.DILUTIONFACTOR, DM.SERIALNUMBER, DM.PCRREACTIONCASSETTESERIALNUMBER, 
                                DM.EXTRACTIONCASSETTELOT, DM.EXTRACTIONCASSETTEEXPIRYDATE, DM.EXTRACTIONCASSETTESERIALNUMBER, DM.REAGENTLOT, 
                                DM.REAGENTEXPIRYDATE, DM.REAGENTTUBESIZE, DM.ICLOT, DM.ICEXPIRYDATE, DM.ICTUBESIZE, DM.CALIBRATORLOT, 
                                DM.CALIBRATOREXPIRYDATE, DM.CONTROLLOT, DM.CONTROLEXPIRYDATE, DM.MEASUREDCALIBRATIONRESULTID, DM.MEASUREDCONTROLRESULTID, 
                                DM.ASSAYDETAIL, DM.LISUPLOADSELECTED, DM.LISSTATUSV, DM.APPROVALUSERNAME, DM.APPROVALUSERROLE, DM.APPROVALDATETIME, 
                                DM.APPROVALSTATUS, DM.ASSAYDETAILFORCOPIESPERML, DM.ASSAYDETAILFORGEQPERML, DM.ASSAYDETAILFORIUPERML, DM.ASSAYDETAILSHORTENING, 
                                DM.ASSAYDETAILSHORTENINGFORCOPIESPERML, DM.ASSAYDETAILSHORTENINGFORGEQPERML, DM.ASSAYDETAILSHORTENINGFORIUPERML, 
                                DM.ASSAYDETAILSHORTENINGUNIT, DM.RUNNAME, DM.USERNAME, DM.STARTRUNTIME, DM.ENDRUNTIME, DM.ABORTTIME, DM.EXTRACTIONINPUTVOLUME,
                                DM.EXTRACTEDELUTEVOLUME, DM.INVENTORYBLOCKCONFIRMATIONMETHOD, DM.INVENTORYBLOCKNUMBER, DM.INVENTORYBLOCKNAME, 
                                DM.INVENTORYBLOCKBARCODE, DM.RUNSTATUS, DM.EXTRACTIONDATA, DM.REVISIONEXTRA, DM.ASSAYDETAILDECODE, DM.APPROVALSTATUSDECODE,
                                INSTR.CUSTOMERCODE, INSTR.COMPANYNAME, INSTR.SITE_CODE, INSTR.SITE_DESCRIPTION, INSTR.SITE_ADDRESS,
                                INSTR.SITE_CITY, INSTR.SITE_COUNTRY, INSTR.INSTALLATION_DATE, INSTR.COMMERCIAL_STATUS, INSTR.FINANCE_STATUS, 
                                INSTR.REGION, INSTR.AREA, 
                                ASSAY.SAMPLETYPES AS SAMPLETYPES_ASSAY, ASSAY.IVDCLEARED, ASSAY.CTSANDTMONLY, ASSAY.PATHOGEN_TARGETNAME, ASSAY.EXTRACTIONINPUTVOLUME AS EXTRACTIONINPUTVOLUME_ASSAY, 
                                ASSAY.EXTRACTEDELUTEVOLUME AS EXTRACTEDELUTEVOLUME_ASSAY, ASSAY.RESULTSSCALING, ASSAY.SONICATIONONTIME AS SONICATIONONTIME_ASSAY, 
                                ASSAY.SONICATIONOFFTIME AS SONICATIONOFFTIME_ASSAY, ASSAY.SONICATIONCYCLE AS SONICATIONCYCLE_ASSAY, ASSAY.EXTRACTIONCASSETTEID, 
                                ASSAY.PCRCASSETTEID, ASSAY.PCRINPUTELUATEVOLUME, ASSAY.PRECYCLESTEPNUMBER, 
                                ASSAY.PCRAMPLIFICATIONCYCLES, ASSAY.PCRAMPLIFICATIONSTEPS, ASSAY.MELTREQUIRED, ASSAY.MELTINGOPTIONAL, ASSAY.STARTRAMPTEMPERATURE, 
                                ASSAY.ENDRAMPTEMPERATURE, ASSAY.RAMPRATE, ASSAY.UPPERTOLERANCE, ASSAY.LOWERTOLERANCE, ASSAY.OFFSET, ASSAY.CONVERSIONFACTORIU, 
                                ASSAY.USERNAME AS USERNAME_ASSAY, ASSAY.REGISTEREDDATE, ASSAY.SAMPLEMATRIX_ID, ASSAY.ASSAYPARAMETERTOOLVERSION, ASSAY.SERIAL, ASSAY.ASSAYID";
            }
            else {
                throw new System.InvalidOperationException(String.Format("ExecutionsBO.getDailyExecutions() - type {0} not handled", tipo));
            }

            SqlFrom = @" FROM DATAMAIN DM
                         INNER JOIN INSTRUMENTS INSTR
                            ON DM.SERIALNUMBER = INSTR.SERIALNUMBER";
            //if (tipo == getType.exportDetail || !String.IsNullOrWhiteSpace(dto.PCRTarget) || !String.IsNullOrWhiteSpace(dto.PCRTargetType))
            if (tipo == getType.exportDetail || tipo == getType.exportDetailShort)
            {
                SqlFrom += @" LEFT JOIN GASSAYPROGRAMS ASSAY
                           ON  DM.GENERALASSAYID = ASSAY.ASSAYID
                           AND DM.SERIALNUMBER = ASSAY.SERIAL";
            }

            SqlWhere = @" WHERE DM.STARTRUNTIME IS NOT NULL";

            // Filtri 
            if (dto.DateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, DM.STARTRUNTIME) >= CONVERT(datetime, @datainizio, 103)";
                args.Add(new { datainizio = ((DateTime)dto.DateFrom).ToString("dd/MM/yyyy")});
                ret.filters.Add ("Date From",  ((DateTime)dto.DateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.DateTo != null)
            {
                SqlWhere += " AND CONVERT(date, DM.STARTRUNTIME) <= CONVERT(datetime, @datafine, 103)";
                args.Add(new { datafine = ((DateTime)dto.DateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Date To", ((DateTime)dto.DateTo).ToString("dd/MM/yyyy"));
            }
            if (dto.excludeSatSun== true)
            {
                //SqlWhere += " AND DATEPART(dw, DM.STARTRUNTIME) NOT IN (6,7)";           // dipende dalla configurazione sqlserver !! @@DATEFIRST
                SqlWhere += @" AND (((DATEPART(dw, DM.STARTRUNTIME) + @@@DATEFIRST - 2) % 7) + 1) NOT IN (6,7)";
                ret.filters.Add("Saturday/Sunday", "Excluded");
            }

            // Country (multiselezione)
            List<string> valoriCountries = dto.countriesSel;
            if (valoriCountries == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                if (links.Count() > 0)
                {
                    valoriCountries = links;
                }
            }
            if (valoriCountries != null)
            {
                SqlWhere += " AND INSTR.SITE_COUNTRY IN (@countriesList)";
                var arr = valoriCountries.Select(el => el).ToList();
                args.Add(new { countriesList = arr });
                ret.filters.Add("Country", string.Join(" / ", arr.ToArray()));
            }
            else
            {
                if (String.IsNullOrWhiteSpace(dto.City) && dto.customersSel == null && String.IsNullOrWhiteSpace(dto.Instrument) &&
                        String.IsNullOrWhiteSpace(dto.Region) && String.IsNullOrWhiteSpace(dto.Area))
                {
                    ret.filters.Add("Country", "ALL");
                }
            }
            // City 
            if (!String.IsNullOrWhiteSpace(dto.City))
            {
                SqlWhere += " AND INSTR.SITE_CITY = @City";
                args.Add(new { City = dto.City });
                ret.filters.Add("City", dto.City);
            }
            // Customers (multiselezione)
            List<string> valoriCustomers = dto.customersSel;
            if (valoriCustomers == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                if (links.Count() > 0)
                {
                    valoriCustomers = links;
                }
            }
            if (valoriCustomers != null)
            {
                SqlWhere += " AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                List<string> arr = valoriCustomers.Select(el => el.ToUpper()).ToList();
                args.Add(new { customersList = arr });
                List<string> arrDeco = new List<string>(arr);
                for (int i = 0; i < arr.Count; i++)
                {
                    arrDeco[i] = new CustomersBO().GetDesc(arr[i]);
                }
                ret.filters.Add("Customer", string.Join(" / ", arrDeco.ToArray()));
            }
            // Site
            if (!String.IsNullOrWhiteSpace(dto.SiteCode))
            {
                SqlWhere += " AND INSTR.SITE_CODE = @SiteCode";
                args.Add(new { SiteCode = dto.SiteCode });
                ret.filters.Add("Site", (new SitesBO().GetDesc(dto.SiteCode, (dto.customersSel == null ? "" : string.Join(",", dto.customersSel)))));
            }
            if (!String.IsNullOrWhiteSpace(dto.Instrument))
            {
                SqlWhere += " AND INSTR.SerialNumber = @SerialNumber";
                args.Add(new { SerialNumber = dto.Instrument });
                ret.filters.Add("Instrument", dto.Instrument);
            }
            if (!String.IsNullOrWhiteSpace(dto.Region))
            {
                SqlWhere += " AND INSTR.REGION = @Region";
                args.Add(new { Region = dto.Region });
                ret.filters.Add("Region", dto.Region);
            }
            if (!String.IsNullOrWhiteSpace(dto.Area))
            {
                SqlWhere += " AND INSTR.AREA = @Area";
                args.Add(new { Area = dto.Area });
                ret.filters.Add("Area", dto.Area);
            }
            if (!String.IsNullOrWhiteSpace(dto.commercialStatus))
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIAL_STATUS) = @CommercialStatus";
                args.Add(new { CommercialStatus = dto.commercialStatus.ToUpper() });
                ret.filters.Add("Commercial Status", dto.commercialStatus.ToUpper());
            }
            // CommercialEntity (Multiselezione) 
            List<string> valoriCommercialEntities = dto.commercialEntitiesSel;
            if (valoriCommercialEntities == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                if (links.Count() > 0)
                {
                    valoriCommercialEntities = links;
                }
            }
            if (valoriCommercialEntities != null)
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitySel)";
                List<string> arr = valoriCommercialEntities.Select(el => el.ToUpper()).ToList();
                args.Add(new { commercialEntitySel = arr });
                ret.filters.Add("Commercial Entity", string.Join(" / ", arr.ToArray()));
            }
            //if (!String.IsNullOrWhiteSpace(dto.PCRTargetType))
            //{
            //    if (dto.PCRTargetType.Equals(Lookup.TargetType_STD))
            //    {
            //        SqlWhere += " AND ASSAY.ASSAYNAME LIKE @AssayName";
            //        ret.filters.Add("PCR Target Type", Lookup.TargetTypeDesc_STD);
            //    } else
            //    {
            //        SqlWhere += " AND ASSAY.ASSAYNAME NOT LIKE @AssayName";
            //        ret.filters.Add("PCR Target Type", Lookup.TargetTypeDesc_OPEN);
            //    }
            //    args.Add(new { AssayName = "Elite" });
            //}
            //if (!String.IsNullOrWhiteSpace(dto.PCRTarget))
            //{
            //    SqlWhere += " AND ASSAY.PATHOGEN_TARGETNAME = @PCRTarget";
            //    args.Add(new { PCRTarget = dto.PCRTarget });
            //    ret.filters.Add("PCR Target", dto.PCRTarget);
            //}
            if (dto.InstallDateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) >= CONVERT(datetime, @InstallDateFrom, 103)";
                args.Add(new { InstallDateFrom = ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date From", ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.InstallDateTo != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) <= CONVERT(datetime, @InstallDateTo, 103)";
                args.Add(new { InstallDateTo = ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date To", ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy"));
            }
            if (dto.ExtractionDataFrom != null)
            {
                SqlWhere += " AND CONVERT(date, DM.EXTRACTIONDATA) >= CONVERT(datetime, @ExtractionDataFrom, 103)";
                args.Add(new { ExtractionDataFrom = ((DateTime)dto.ExtractionDataFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Extraction Data From", ((DateTime)dto.ExtractionDataFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.ExtractionDataTo != null)
            {
                SqlWhere += " AND CONVERT(date, DM.EXTRACTIONDATA) <= CONVERT(datetime, @ExtractionDataTo, 103)";
                args.Add(new { ExtractionDataTo = ((DateTime)dto.ExtractionDataTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Extraction Data To", ((DateTime)dto.ExtractionDataTo).ToString("dd/MM/yyyy"));
            }

            // Estrazione Dati SERIE o DETTAGLIO
            var sql = "";
            if (tipo == getType.series)
            {
                sql = @"SELECT STARTRUNTIME, STARTRUNTIME_ORDER,
                           SUM(RUN_OK) AS NUM_TEST, 
                           SUM(PROCESS_PCR) AS PROCESS_PCR, 
                           SUM(PROCESS_EXTR) AS PROCESS_EXTR,
                           SUM(INVALID) AS INVALID, 
                           SUM(ERROR) AS ERROR,
                           SUM(ABORTED) AS ABORTED
                      FROM (" + SqlFields + " " + SqlFrom + " " + SqlWhere  +
                          @") TMP 
                              GROUP BY STARTRUNTIME, STARTRUNTIME_ORDER
                              ORDER BY STARTRUNTIME_ORDER";
            } else
            {
                sql = SqlFields + " " + SqlFrom + " " + SqlWhere + @" ORDER BY DM.ID";
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    if (tipo == getType.series)
                    {
                        ret.dailyExecutions = db.Query<ExecutionCounters>(sql, args.ToArray()).ToList();
                    } else if (tipo == getType.exportDetail)
                    {
                        ret.exportDetails = db.Query<ExecutionDetail>(sql, args.ToArray()).ToList();
                    } else 
                    {
                        ret.exportDetailShorts = db.Query<ExecutionDetailShort>(sql, args.ToArray()).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("ExecutionsBO.getDailyExecutions() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            // Conteggio n.Strumenti 
            sql = @"SELECT COUNT(DISTINCT DM.SERIALNUMBER) AS CNT " + SqlFrom + " " + SqlWhere;
            try
            {
                using (IDatabase db = Connection)
                {
                    ret.cntInstruments = db.ExecuteScalar<int>(sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("ExecutionsBO.getDailyExecutions()/CntInstruments - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            //// ATTENZIONE: il summary viene composto sempre come ', <nomecampo>:<valore>'
            //// rimuovo il primo ', '
            //if (!String.IsNullOrWhiteSpace(ret.summary))
            //{
            //    ret.summary = ret.summary.Substring(2, ret.summary.Length - 2);
            //}

            return ret;
        }

        // elenco AssaysUtilization
        public AssayUtilizationModel GetAssaysUtilization(AssaysUtilizationDTO dto, string UserId)
        {

            var ret = new AssayUtilizationModel();
            List<object> args = new List<object>();
            var SqlFields = "";
            var SqlFrom = "";
            var sqlJoinAssay = "";
            var SqlWhere = "";

            // Stati: V (VALID), I (Invalid), E (Error), A (Aborted)
            //SqlFields += @"SELECT ASSAY.PATHOGEN_TARGETNAME_NORM as PATHOGEN_NAME, 
            //                      ASSAY.SAMPLETYPES as SAMPLETYPES,
            //                      -- (Validi + Invalidi + Error , esclusi Aborted)
            //                      IIF(DM.RUNSTATUS IN ('V', 'I', 'E') AND ASSAY.AssayName like '%ELITE%', 1, 0) as ASSAY_ELITECH,
            //                      -- n.Test Totali (Validi + Invalidi + Error , esclusi Aborted)
            //                      IIF(DM.RUNSTATUS IN ('V', 'I', 'E'), 1, 0) AS RUN_OK, 
            //                      --n.Test in ERRORE
            //                      IIF(DM.RUNSTATUS IN ('E', 'A'), 1, 0) AS ERROR,
            //                      --INVALID(conteggiati nel totale)
            //                      IIF(DM.RUNSTATUS = 'I', 1, 0) AS INVALID,
            //                      -- n.Test TOtali x SampleType ! (Validi + Invalidi + Error , esclusi Aborted)
            //   IIF(DM.RUNSTATUS IN ('V', 'I', 'E') AND ASSAY.SampleTypes = 0, 1, 0) AS RUN_OK_SAMPLE,
            //   IIF(DM.RUNSTATUS IN ('V', 'I', 'E') AND ASSAY.SampleTypes = 1, 1, 0) AS RUN_OK_CALIBRATOR,
            //   IIF(DM.RUNSTATUS IN ('V', 'I', 'E') AND ASSAY.SampleTypes = 2, 1, 0) AS RUN_OK_CONTROL";
            SqlFields += @"SELECT ASSAY.PATHOGEN_TARGETNAME_NORM as PATHOGEN_NAME, 
                                  ASSAY.SAMPLETYPES as SAMPLETYPES, DM.SERIALNUMBER,
                                  -- (Validi + Invalidi + Error , esclusi Aborted)
                                  IIF(DM.RUNSTATUS IN ('V', 'I', 'E') AND ASSAY.AssayName like '%ELITE%', 1, 0) as ASSAY_ELITECH,
                                  --n.Test in ERRORE
                                  IIF(DM.RUNSTATUS IN ('E', 'A'), 1, 0) AS ERROR,
                                  --INVALID(conteggiati nel totale)
                                  IIF(DM.RUNSTATUS = 'I', 1, 0) AS INVALID,
                                  -- n.Test Totali (Validi + Invalidi + Error , esclusi Aborted)
                                  IIF(DM.RUNSTATUS IN ('V'), 1, 0) AS RUN_OK_V, 
                                  IIF(DM.RUNSTATUS IN ('I'), 1, 0) AS RUN_OK_I, 
                                  IIF(DM.RUNSTATUS IN ('E'), 1, 0) AS RUN_OK_E, 
                                  -- n.Test TOtali x SampleType (Validi + Invalidi + Error , esclusi Aborted)
                                  -- SAMPLE
                                  IIF(DM.RUNSTATUS IN ('V') AND ASSAY.SampleTypes = 0, 1, 0) AS RUN_OK_SAMPLE_V,
                                  IIF(DM.RUNSTATUS IN ('I') AND ASSAY.SampleTypes = 0, 1, 0) AS RUN_OK_SAMPLE_I,
                                  IIF(DM.RUNSTATUS IN ('E') AND ASSAY.SampleTypes = 0, 1, 0) AS RUN_OK_SAMPLE_E,
                                  -- CALIBRATOR
                                  IIF(DM.RUNSTATUS IN ('V') AND ASSAY.SampleTypes = 1, 1, 0) AS RUN_OK_CALIBRATOR_V,
                                  IIF(DM.RUNSTATUS IN ('I') AND ASSAY.SampleTypes = 1, 1, 0) AS RUN_OK_CALIBRATOR_I,
                                  IIF(DM.RUNSTATUS IN ('E') AND ASSAY.SampleTypes = 1, 1, 0) AS RUN_OK_CALIBRATOR_E,
                                  -- CONTROL
                                  IIF(DM.RUNSTATUS IN ('V') AND ASSAY.SampleTypes = 2, 1, 0) AS RUN_OK_CONTROL_V,
                                  IIF(DM.RUNSTATUS IN ('I') AND ASSAY.SampleTypes = 2, 1, 0) AS RUN_OK_CONTROL_I,
                                  IIF(DM.RUNSTATUS IN ('E') AND ASSAY.SampleTypes = 2, 1, 0) AS RUN_OK_CONTROL_E";
            SqlFrom = @" FROM DATAMAIN DM
                         INNER JOIN INSTRUMENTS INSTR
                            ON DM.SERIALNUMBER = INSTR.SERIALNUMBER";
            sqlJoinAssay = @" INNER JOIN GASSAYPROGRAMS ASSAY
                                ON  DM.GENERALASSAYID = ASSAY.ASSAYID
                                AND DM.SERIALNUMBER = ASSAY.SERIAL";

            SqlWhere = @" WHERE 1=1";
            // FIltri 
            if (dto.DateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, DM.STARTRUNTIME) >= CONVERT(datetime, @datainizio, 103)";
                args.Add(new { datainizio = ((DateTime)dto.DateFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Date From", ((DateTime)dto.DateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.DateTo != null)
            {
                SqlWhere += " AND CONVERT(date, DM.STARTRUNTIME) <= CONVERT(datetime, @datafine, 103)";
                args.Add(new { datafine = ((DateTime)dto.DateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Date To", ((DateTime)dto.DateTo).ToString("dd/MM/yyyy"));
            }
            if (dto.excludeSatSun == true)
            {
                //SqlWhere += " AND DATEPART(dw, DM.STARTRUNTIME) NOT IN (6,7)";           // dipende dalla configurazione sqlserver !! @@DATEFIRST
                SqlWhere += @" AND (((DATEPART(dw, DM.STARTRUNTIME) + @@@DATEFIRST - 2) % 7) + 1) NOT IN (6,7)";         
                ret.filters.Add("Saturday/Sunday", "Excluded");
            }
            // Country (multiselezione)
            List<string> valoriCountries = dto.countriesSel;
            if (valoriCountries == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                if (links.Count() > 0)
                {
                    valoriCountries = links;
                }
            }
            if (valoriCountries != null)
            {
                SqlWhere += " AND INSTR.SITE_COUNTRY IN (@countriesList)";
                var arr = valoriCountries.Select(el => el).ToList();
                args.Add(new { countriesList = arr });
                ret.filters.Add("Country", string.Join(" / ", arr.ToArray()));
            }
            else
            {
                if (String.IsNullOrWhiteSpace(dto.City) && dto.customersSel == null && String.IsNullOrWhiteSpace(dto.Instrument) &&
                        String.IsNullOrWhiteSpace(dto.Region) && String.IsNullOrWhiteSpace(dto.Area))
                {
                    ret.filters.Add("Country", "ALL");
                }
            }
            // City
            if (!String.IsNullOrWhiteSpace(dto.City))
            {
                SqlWhere += " AND INSTR.SITE_CITY = @City";
                args.Add(new { City = dto.City });
                ret.filters.Add("City", dto.City);
            }
            // Customers (multiselezione)
            List<string> valoriCustomers = dto.customersSel;
            if (valoriCustomers == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                if (links.Count() > 0)
                {
                    valoriCustomers = links;
                }
            }
            if (valoriCustomers != null)
            {
                SqlWhere += " AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                List<string> arr = valoriCustomers.Select(el => el.ToUpper()).ToList();
                args.Add(new { customersList = arr });
                List<string> arrDeco = new List<string>(arr);
                for (int i = 0; i < arr.Count; i++)
                {
                    arrDeco[i] = new CustomersBO().GetDesc(arr[i]);
                }
                ret.filters.Add("Customer", string.Join(" / ", arrDeco.ToArray()));
            }
            // SiteCode
            if (!String.IsNullOrWhiteSpace(dto.SiteCode))
            {
                SqlWhere += " AND INSTR.SITE_CODE = @SiteCode";
                args.Add(new { SiteCode = dto.SiteCode });
                ret.filters.Add("Site", (new SitesBO().GetDesc(dto.SiteCode, (dto.customersSel == null ? "" : string.Join(",", dto.customersSel)))));
            }
            // Instrument 
            if (!String.IsNullOrWhiteSpace(dto.Instrument))
            {
                SqlWhere += " AND INSTR.SerialNumber = @SerialNumber";
                args.Add(new { SerialNumber = dto.Instrument });
                ret.filters.Add("Instrument", dto.Instrument);
            }
            // Region
            if (!String.IsNullOrWhiteSpace(dto.Region))
            {
                SqlWhere += " AND INSTR.REGION = @Region";
                args.Add(new { Region = dto.Region });
                ret.filters.Add("Region", dto.Region);
            }
            // Area 
            if (!String.IsNullOrWhiteSpace(dto.Area))
            {
                SqlWhere += " AND INSTR.AREA = @Area";
                args.Add(new { Area = dto.Area });
                ret.filters.Add("Area", dto.Area);
            }
            // Commercial Status 
            if (!String.IsNullOrWhiteSpace(dto.commercialStatus))
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIAL_STATUS) = @CommercialStatus";
                args.Add(new { CommercialStatus = dto.commercialStatus.ToUpper() });
                ret.filters.Add("Commercial Status", dto.commercialStatus.ToUpper());
            }
            // CommercialEntity (Multiselezione) 
            List<string> valoriCommercialEntities = dto.commercialEntitiesSel;
            if (valoriCommercialEntities == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                if (links.Count() > 0)
                {
                    valoriCommercialEntities = links;
                }
            }
            if (valoriCommercialEntities != null)
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitySel)";
                List<string> arr = valoriCommercialEntities.Select(el => el.ToUpper()).ToList();
                args.Add(new { commercialEntitySel = arr });
                ret.filters.Add("Commercial Entity", string.Join(" / ", arr.ToArray()));
            }
            // Sample Type 
            if (dto.SampleType != null)
            {
                SqlWhere += " AND ASSAY.SAMPLETYPES = @SampleType";
                args.Add(new { SampleType = dto.SampleType });
                ret.filters.Add("Sample Type", Lookup.SampleTypesDecode(dto.SampleType));
            }
            // PathogenName 
            if (!String.IsNullOrWhiteSpace(dto.PathogenName))
            {
                SqlWhere += " AND ASSAY.PATHOGEN_TARGETNAME_NORM = @PathogenName";
                args.Add(new { PathogenName = dto.PathogenName });
                ret.filters.Add("Pathogen Name", dto.PathogenName);
            }
            // INstallaDateFrom/TO 
            if (dto.InstallDateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) >= CONVERT(datetime, @InstallDateFrom, 103)";
                args.Add(new { InstallDateFrom = ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date From", ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.InstallDateTo != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) <= CONVERT(datetime, @InstallDateTo, 103)";
                args.Add(new { InstallDateTo = ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date To", ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy"));
            }

            // Estrazione Dati SERIE
            var sqlCount = @"SELECT PATHOGEN_NAME, 
                                SUM(RUN_OK_V+RUN_OK_I+RUN_OK_E) AS NUM_TEST
                                FROM (" + SqlFields + " " + SqlFrom + " " + sqlJoinAssay + " " + SqlWhere + @") TMP 
                                GROUP BY PATHOGEN_NAME
                                ORDER BY NUM_TEST DESC";
            var sqlDetail = @"SELECT PATHOGEN_NAME, 
                                     SUM(RUN_OK_V+RUN_OK_I+RUN_OK_E) AS NUM_TEST,
                                     SUM(RUN_OK_V) AS NUM_TEST_V,
                                     SUM(RUN_OK_I) AS NUM_TEST_I,
                                     SUM(RUN_OK_E) AS NUM_TEST_E,
                                     SUM(RUN_OK_SAMPLE_V+RUN_OK_SAMPLE_I+RUN_OK_SAMPLE_E) AS NUM_SAMPLE,
                                     SUM(RUN_OK_SAMPLE_V) AS NUM_SAMPLE_V,
                                     SUM(RUN_OK_SAMPLE_I) AS NUM_SAMPLE_I,
                                     SUM(RUN_OK_SAMPLE_E) AS NUM_SAMPLE_E,
                                     SUM(RUN_OK_CALIBRATOR_V+RUN_OK_CALIBRATOR_I+RUN_OK_CALIBRATOR_E) AS NUM_CALIBRATOR,
                                     SUM(RUN_OK_CALIBRATOR_V) AS NUM_CALIBRATOR_V,
                                     SUM(RUN_OK_CALIBRATOR_I) AS NUM_CALIBRATOR_I,
                                     SUM(RUN_OK_CALIBRATOR_E) AS NUM_CALIBRATOR_E,
                                     SUM(RUN_OK_CONTROL_V+RUN_OK_CONTROL_I+RUN_OK_CONTROL_E) AS NUM_CONTROL,
                                     SUM(RUN_OK_CONTROL_V) AS NUM_CONTROL_V,
                                     SUM(RUN_OK_CONTROL_I) AS NUM_CONTROL_I,
                                     SUM(RUN_OK_CONTROL_E) AS NUM_CONTROL_E,
                                     SUM(INVALID) AS INVALID,
                                     SUM(ERROR) AS ERROR
                                FROM (" + SqlFields + " " + SqlFrom + " " + sqlJoinAssay + " " + SqlWhere + @") TMP 
                                GROUP BY PATHOGEN_NAME
                                ORDER BY NUM_TEST DESC";
            var sqlRunCounters = @"SELECT SUM(ASSAY_ELITECH) as NUM_TEST_ELITE, SUM(RUN_OK_V+RUN_OK_I+RUN_OK_E) as TOT_TEST
                                     FROM (" + SqlFields + " " + SqlFrom + " " + sqlJoinAssay + " " + SqlWhere + @") TMP";
            try
            {
                using (IDatabase db = Connection )
                {
                    db.OneTimeCommandTimeout = 120;
                    ret.AssaysUtilization = db.Query<AssayUtilizationCounters>(sqlCount, args.ToArray()).ToList();
                    ret.AssayUtilizationDetails = db.Query<AssayUtilizationDetails>(sqlDetail, args.ToArray()).ToList();
                    List<RunAssaysCounters> cnt = db.Query<RunAssaysCounters>(sqlRunCounters, args.ToArray()).ToList();
                    ret.CntRunAssayElite = cnt[0].num_test_elite;
                    ret.TotRun = cnt[0].tot_test;
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("ExecutionsBO.GetAssaysUtilization() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            // Conteggio n.Strumenti 
            var sql = @"SELECT COUNT(DISTINCT DM.SERIALNUMBER) AS CNT " + SqlFrom + " ";
            if (SqlWhere.IndexOf("ASSAY.") > 0)
            {
                sql += sqlJoinAssay + " ";
            }
            sql +=  SqlWhere;
            try
            {
                using (IDatabase db = Connection)
                {
                    db.OneTimeCommandTimeout = 120;
                    ret.cntInstruments = db.ExecuteScalar<int>(sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("ExecutionsBO.GetAssaysUtilization()/CntInstruments - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

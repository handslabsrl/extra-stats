﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class CommercialStatusBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public CommercialStatusBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco STATUS
        public List<CommStatusModel> Get()
        {
             
            var ret = new List<CommStatusModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT UPPER(COMMERCIAL_STATUS) as COMMERCIALSTATUS FROM INSTRUMENTS WHERE COMMERCIAL_STATUS IS NOT NULL ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CommStatusModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CommercialStatusBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

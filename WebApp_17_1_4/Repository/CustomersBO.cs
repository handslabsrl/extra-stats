﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class CustomersBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public CustomersBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco CLIENTI
        public List<CustomerModel> GetCustomers(String Country = null, string City = null, string UserIdCfg = "")
        {

            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            string regionCfg = "";
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    regionCfg = userCfg.Region;
                    areaCfg = userCfg.Area;
                }
            }

            var ret = new List<CustomerModel>();
            List<object> args = new List<object>();
            //var Sql = @"SELECT DISTINCT CUSTOMERCODE, COMPANYNAME, SITE_COUNTRY, REGION, AREA FROM INSTRUMENTS ORDER BY COMPANYNAME";
            var Sql = @"SELECT DISTINCT CUSTOMERCODE, COMPANYNAME FROM INSTRUMENTS";
            Sql += "  WHERE 1 = 1";
            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(regionCfg))
            {
                Sql += " AND UPPER(REGION) = @RegionCfg";
                args.Add(new { RegionCfg = regionCfg.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }
            // Filtri 
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(SITE_COUNTRY) IN (@countriesList)";
                String[] arr = Country.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { countriesList = arr });
            }
            if (!String.IsNullOrWhiteSpace(City))
            {
                Sql += " AND SITE_CITY = @City";
                args.Add(new { City = City });
            }
            Sql += @" ORDER BY COMPANYNAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CustomerModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CustomersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // descrizione CUSTOMER
        public string GetDesc(string CustomerCode)
        {

            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT TOP(1) COMPANYNAME FROM INSTRUMENTS WHERE CUSTOMERCODE = @customerCode";
            args.Add(new { customerCode = CustomerCode });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CustomersBO.GetDesc() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

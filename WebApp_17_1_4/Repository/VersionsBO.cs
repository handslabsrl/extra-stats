﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class VersionsBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public VersionsBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco AREE
        public List<WebApp.Entity.Version> Get()
        {

            var ret = new List<WebApp.Entity.Version>();
            List<object> args = new List<object>();
            var Sql = @"SELECT CODE, DESCRIPTION FROM VERSIONS";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<WebApp.Entity.Version>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("VersionsBO.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class CountriesBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public CountriesBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco Paesi
        public List<CountryModel> GetCountries(string UserIdCfg = "")
        {
            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            string regionCfg = "";
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);

                Entity.UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    regionCfg = userCfg.Region;
                    areaCfg = userCfg.Area;
                }
            }

            var ret = new List<CountryModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT SITE_COUNTRY FROM INSTRUMENTS WHERE 1=1";

            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(regionCfg))
            {
                Sql += " AND UPPER(REGION) = @RegionCfg";
                args.Add(new { RegionCfg = regionCfg.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }

            Sql += @" ORDER BY SITE_COUNTRY";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CountryModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CustomersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // Commercial Entity associata alla Country
        public String GetCommercialEntityDefault(string Country)
        {

            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT COMMERCIALENTITY 
                          FROM INSTRUMENTS 
                         WHERE SITE_COUNTRY = @country
                           AND COMMERCIALENTITY IS NOT NULL";
            args.Add(new { country = Country });
            if (!String.IsNullOrWhiteSpace(Country))
            {
                try
                {
                    using (IDatabase db = Connection)
                    {
                        ret = db.ExecuteScalar<string>(Sql, args.ToArray()).ToUpper();
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(string.Format("CustomersBO.GetCommercialEntityDefault() - ERRORE {0} {1}", e.Message, e.Source));
                    return null;
                }
            }
            return ret;
        }

    }
}

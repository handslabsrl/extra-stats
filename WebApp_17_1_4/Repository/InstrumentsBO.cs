﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Authorization;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class InstrumentsBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public InstrumentsBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco Istruments di Compenteza (in base alla configurazione dell'utente passato)
        public List<InstrumentModel> GetInstruments(string CustomerCode = null, string SiteCode = null, string Country = "", string City = "", 
                                                        string SerialNumber = "", string CommercialEntity = "", string Region = "", string Area = "",
                                                        string UserIdCfg = "")
        {
            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            string regionCfg = "";
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);

                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    regionCfg = userCfg.Region;
                    areaCfg = userCfg.Area;
                }
            }
            var ret = new List<InstrumentModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT SERIALNUMBER, COMPANYNAME, SITE_DESCRIPTION, COMMAND, COMMANDDATERIF, SITE_CITY, SITE_COUNTRY, 
                                        COMMERCIALENTITY, REGION, AREA
                                    FROM INSTRUMENTS WHERE 1 = 1";
            // filtri per configurazioni 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(regionCfg))
            {
                Sql += " AND UPPER(REGION) = @RegionCfg";
                args.Add(new { RegionCfg = regionCfg.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }
            // FILTRI 
            if (!String.IsNullOrWhiteSpace(CustomerCode))
            {
                Sql += " AND UPPER(CUSTOMERCODE) IN (@customersList)";
                String[] arr = CustomerCode.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { customersList = arr });
            }
            if (!String.IsNullOrWhiteSpace(SiteCode))
            {
                Sql += " AND SITE_CODE = @SiteCode";
                args.Add(new { SiteCode = SiteCode });
            }
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(SITE_COUNTRY) IN (@countriesList)";
                String[] arr = Country.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { countriesList = arr });
            }
            if (!String.IsNullOrWhiteSpace(City))
            {
                Sql += " AND SITE_CITY = @City";
                args.Add(new { City = City });
            }
            if (!String.IsNullOrWhiteSpace(Region))
            {
                Sql += " AND UPPER(REGION) = @Region";
                args.Add(new { Region = Region.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(Area))
            {
                Sql += " AND UPPER(AREA) = @Area";
                args.Add(new { Area = Area.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(SerialNumber))
            {
                Sql += " AND SERIALNUMBER = @SerialNumber";
                args.Add(new { SerialNumber = SerialNumber });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(COMMERCIALENTITY) IN (@CommercialEntityList)";
                String[] arr = CommercialEntity.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { CommercialEntityList = arr });
            }
            Sql += @" ORDER BY SERIALNUMBER";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<InstrumentModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.GetInstruments() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // Dati Instrument
        public InstrumentModel GetInstrument(string SerialNumber = "")
        {

            var ret = new InstrumentModel();
            List<object> args = new List<object>();
            var Sql = @"SELECT SERIALNUMBER, COMPANYNAME, SITE_DESCRIPTION, COMMAND, COMMANDDATERIF, SITE_CITY, SITE_COUNTRY, COMMERCIALENTITY
                                   FROM INSTRUMENTS WHERE  SERIALNUMBER = @SerialNumber";
            args.Add(new { SerialNumber = SerialNumber });
            
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<InstrumentModel>(Sql, args.ToArray()).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.GetInstrument() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco CommerialEntity 
        public List<CommercialEntityModel> GetCommercialEntities(string Country = "", string CommercialEntity = "", string UserIdCfg = "")
        {
            var ret = new List<CommercialEntityModel>();
            List<object> args = new List<object>();

            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            string regionCfg = "";
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);

                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    regionCfg = userCfg.Region;
                    areaCfg = userCfg.Area;
                }
            }

            var Sql = @"SELECT DISTINCT UPPER(COMMERCIALENTITY) AS COMMERCIALENTITY FROM INSTRUMENTS";
            Sql += " WHERE COMMERCIALENTITY IS NOT NULL";

            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(regionCfg))
            {
                Sql += " AND UPPER(REGION) = @RegionCfg";
                args.Add(new { RegionCfg = regionCfg.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }

            // altri filtri 
            if (!String.IsNullOrWhiteSpace(Country))
            {
                //Sql += " AND UPPER(SITE_COUNTRY) = @Country";
                //args.Add(new { Country = Country.ToUpper() });
                // Separatori validi | oppure ,
                Sql += " AND UPPER(SITE_COUNTRY) IN (@CountryList)";
                var arr = Country.ToUpper().Replace("|",",").Split(',').ToList();
                args.Add(new { CountryList = arr });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(COMMERCIALENTITY) = @CommercialEntity";
                args.Add(new { CommercialEntity = CommercialEntity.ToUpper() });
            }
            Sql += " ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CommercialEntityModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetCommercialEntities.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco Country 
        public List<CountryModel> GetCountries(string Country = "", string CommercialEntity = "")
        {

            var ret = new List<CountryModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT UPPER(SITE_COUNTRY) AS COUNTRY FROM INSTRUMENTS WHERE SITE_COUNTRY IS NOT NULL ";
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(SITE_COUNTRY) = @Country";
                args.Add(new { Country = Country.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(COMMERCIALENTITY) = @CommercialEntity";
                args.Add(new { CommercialEntity = CommercialEntity.ToUpper() });
            }
            Sql += " ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CountryModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetCountries.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public void SetCommand(string SerialNumber, string Command, DateTime DateRif)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE INSTRUMENTS SET COMMAND = @Command,
                                               COMMANDDATERIF = CONVERT(datetime, @DateRif, 103) 
                                      WHERE SERIALNUMBER = @SerialNumber";
            args.Add(new { Command = Command });
            args.Add(new { DateRif = ((DateTime)DateRif).ToString("dd/MM/yyyy") });
            args.Add(new { SerialNumber = SerialNumber });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.SetCommand() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }

    }
}

﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Services
{
    public interface IEmailSender
    {
        //Task SendEmailAsync(string email, string subject, string message);
        Task SendEmailAsync(string email, string subject, string message, string[] attachments = null, string context = "",
                            string emailBcc = "", string emailCC = "", bool deleteAttachments = false, ILogger _logger = null,
                            string emailReplayTo = "");
    }
}

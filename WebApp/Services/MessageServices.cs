﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MimeKit;
using WebApp.Classes;

namespace WebApp.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link https://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private string getEmailConfig(string param)
        {
            var configurationBuilder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
            return configurationBuilder.Build().GetSection(String.Format("EmailCfg:{0}", param)).Value;
        }

        //public Task SendEmailAsync(string email, string subject, string message)
        //{
        //    try
        //    {
        //        string FromAddress = getEmailConfig("Sender");
        //        if (!String.IsNullOrWhiteSpace(FromAddress) && !String.IsNullOrWhiteSpace(email))
        //        {
        //            string ToAddress = email; 
        //            string SmtpServer = getEmailConfig("SmtpServer");
        //            int SmtpPortNumber = Int32.Parse(getEmailConfig("SmtpPortNumber"));
        //            bool useSSL = bool.Parse(getEmailConfig("useSSL"));
        //            string User = getEmailConfig("User");
        //            string Password = getEmailConfig("Password");
        //            var mimeMessage = new MimeMessage();
        //            mimeMessage.From.Add(new MailboxAddress("ELITeBoard", FromAddress));
        //            mimeMessage.To.Add(new MailboxAddress(ToAddress));

        //            mimeMessage.Subject = subject; //Subject  
        //            mimeMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
        //            {
        //                //Text = "<b>" + message + "</b>"
        //                Text = message
        //            };

        //            using (var client = new SmtpClient())
        //            {
        //                client.Connect(SmtpServer, SmtpPortNumber, useSSL);
        //                client.Authenticate(User, Password);
        //                client.Send(mimeMessage);
        //                client.Disconnect(true);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        // ???? 
        //    }

        //    return Task.FromResult(0);
        //}

        public Task SendEmailAsync(string email, string subject, string message, string[] attachments = null, string context = "", 
                                   string emailBcc = "", string emailCC = "", bool deleteAttachments= false, ILogger _logger = null,
                                   string emailReplayTo = "")
        {
            try
            {
                bool bEnabled = bool.Parse(getEmailConfig(string.Format("Enabled{0}", context.ToUpper())));
                string FromAddress = getEmailConfig(string.Format("Sender{0}", context.ToUpper()));
                string nameSender = getEmailConfig(string.Format("NameSender{0}", context.ToUpper()));

                if (bEnabled && !String.IsNullOrWhiteSpace(FromAddress) && !String.IsNullOrWhiteSpace(email))
                {
                    string ToAddress = email;
                    string ForcedReceiver = getEmailConfig(string.Format("ForcedRecipient{0}", context.ToUpper()));
                    if (!String.IsNullOrWhiteSpace(ForcedReceiver))
                    {
                        ToAddress = ForcedReceiver;
                    }

                    if (_logger != null)
                    {
                        string msg = String.Format("context: {2}, subject: {3} from: {0}, to: {1}", FromAddress, email, context, subject);
                        _logger.LogError(String.Format("{0} - {1}", "SendEmailAsync()", msg));
                    }

                    string SmtpServer = getEmailConfig("SmtpServer");
                    int SmtpPortNumber = Int32.Parse(getEmailConfig("SmtpPortNumber"));
                    //bool useSSL = bool.Parse(getEmailConfig("useSSL"));
                    string useSSL = getEmailConfig("useSSL");
                    string User = getEmailConfig("User");
                    string Password = getEmailConfig("Password");
                    var mimeMessage = new MimeMessage();
                    mimeMessage.From.Add(new MailboxAddress(nameSender, FromAddress));
                    //mimeMessage.To.Add(new MailboxAddress(ToAddress));
                    InternetAddressList listTo = new InternetAddressList();
                    if (!email.Equals("NONE"))
                    {
                        foreach (var item in ToAddress.Split(','))
                        {
                            listTo.Add(new MailboxAddress(item.Trim()));
                        }
                    }
                    mimeMessage.To.AddRange(listTo);
                    // BCC
                    InternetAddressList listBCC = new InternetAddressList();
                    if (!string.IsNullOrWhiteSpace(emailBcc))
                    {
                        if (!String.IsNullOrWhiteSpace(ForcedReceiver))
                        {
                            listBCC.Add(new MailboxAddress(ForcedReceiver));
                        }
                        else
                        {
                            foreach (var item in emailBcc.Split(','))
                            {
                                listBCC.Add(new MailboxAddress(item.Trim()));
                            }
                        }
                        mimeMessage.Bcc.AddRange(listBCC);
                    }
                    // CC
                    InternetAddressList listCC = new InternetAddressList();
                    if (!string.IsNullOrWhiteSpace(emailCC))
                    {
                        if (!String.IsNullOrWhiteSpace(ForcedReceiver))
                        {
                            listCC.Add(new MailboxAddress(ForcedReceiver));
                        }
                        else
                        {
                            foreach (var item in emailCC.Split(','))
                            {
                                listCC.Add(new MailboxAddress(item.Trim()));
                            }
                        }
                        mimeMessage.Cc.AddRange(listCC);
                    }
                    // REPLAY TO 
                    InternetAddressList listReplayTo = new InternetAddressList();
                    if (!string.IsNullOrWhiteSpace(emailReplayTo))
                    {
                        foreach (var item in emailReplayTo.Split(','))
                        {
                            listReplayTo.Add(new MailboxAddress(item.Trim()));
                        }
                        mimeMessage.ReplyTo.AddRange(listReplayTo);
                    }
                    mimeMessage.Subject = subject; //Subject  
                    var builder = new BodyBuilder();
                    // Set the plain-text version of the message text
                    builder.HtmlBody = message;
                    // We may also want to attach a calendar event for Monica's party...
                    if (attachments != null)
                    {
                        foreach (var attach in attachments)
                        {
                            builder.Attachments.Add(@attach);
                        }
                    }
                    // Now we just need to set the message body and we're done
                    mimeMessage.Body = builder.ToMessageBody();
                    foreach (var body in mimeMessage.BodyParts.OfType<TextPart>())
                        body.ContentTransferEncoding = ContentEncoding.Default;
                        //body.ContentTransferEncoding = ContentEncoding.UUEncode;

                    using (var client = new SmtpClient())
                    {
                        if (useSSL.Equals("auto"))
                        {
                            // per problemi di certificato scaduto ????
                            client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                            client.CheckCertificateRevocation = false;
                            client.Connect(SmtpServer, SmtpPortNumber, SecureSocketOptions.Auto);
                        } else
                        {
                            bool bUseSSL = false;
                            if (useSSL.ToLower().Equals("true")) {
                                bUseSSL = true;
                            }
                            client.Connect(SmtpServer, SmtpPortNumber, bUseSSL);
                        }
                        client.Authenticate(User, Password);
                        client.Send(mimeMessage);
                        client.Disconnect(true);

                        if (attachments != null && deleteAttachments)
                        {
                            foreach (var attach in attachments)
                            {
                                try
                                {
                                    File.Delete(attach);
                                }
                                catch ( Exception e )
                                {
                                    // nessuna azione
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception e)
            {
                if (_logger != null)
                {
                    string error = String.Format("{0} / {1}", e.Message, e.Source);
                    _logger.LogError(String.Format("{0} - {1}", "SendEmailAsync()", error));
                }
                return Task.FromException(e);
            }

            return Task.FromResult(0);
        }


    public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }
}

﻿/**
 * JS Applicativo
 *
 */


// bootbox.alert(JSON.stringify(circularReference, getCircularReplacer()));
 //{"otherData":123}
const getCircularReplacer = () => {
    const seen = new WeakSet;
    return (key, value) => {
        if (typeof value === "object" && value !== null) {
            if (seen.has(value)) {
                return;
            }
            seen.add(value);
        }
        return value;
    };
};

// Aggiornamento parametri QueryString
function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

// Validazione Email 
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// Es. alert(JSON.stringify(getCircularReplacer(rowData)));
//const getCircularReplacer = () => {
//    const seen = new WeakSet;
//    return (key, value) => {
//        if (typeof value === "object" && value !== null) {
//            if (seen.has(value)) {
//                return;
//            }
//            seen.add(value);
//        }
//        return value;
//    };
//};

// Funzione Applicabile a COLUMN di DATAGRID (DEVEXPRESS)
function showCurrency(data) {
    if ((data.value == 0  || (data.value || "") != "")) {
        return "\u20ac " + Number((data.value)).formatMoney(2, ',', '.');
    } else {
        return "";
    }
}

// Funzione Applicabile a COLUMN di DATAGRID (DEVEXPRESS)
function showPercent(data) {
        if ((data.value == 0 || (data.value || "") != "")) {
            return Number((data.value)).toFixed(0) + " %";
        } else {
            return "";
        }
    };


// funzione applicabile ad uno specifico valore
function formatCurrency(valore) {
    if ((valore == 0 || (valore || "") != "")) {
        return "\u20ac " + Number((valore)).formatMoney(2, ',', '.');
    } else {
        return "";
    }
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};


function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}

// ***************************************************************
// INIT CAMPO BODY con SUMMERNOTE !!
// ***************************************************************
//function initEmailBodyField(fieldName, height, opt, cache) {
function initEmailBodyField(fieldName, height, opt) {
    var myToolbar = [['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['table', ['table']],
                    ['keywords', ['myButton']]];
    var buttonFunctionId = "default";
    //console.log(opt);
    if (opt !== null) {
        if (opt === "TRAINING" || opt === "SUPPORT" || opt === "WEBREPOSITORY" || opt === "WARRANTY" || opt === "ASSAYNOTIFICATION" ||
            opt === "SUPPORTOPENAP3P" || opt === "BOOKING" || opt === "REFURBISH") {
            buttonFunctionId = opt.toLowerCase()
        }
    }
   
    $('#' + fieldName).summernote({
        height: (height || 300),
        toolbar: myToolbar,
        disableDragAndDrop: true, 
        buttons: {
            myButton: dyn_buttonFunctions[buttonFunctionId]
        },
        codeviewFilter: false,
        codeviewIframeFilter: true,
        callbacks: {
            //onInit: function (e) {
            //    if ((cache || false)) {
            //        console.log("callbacks - OnInit " + fieldName);
            //        var cache = localStorage.getItem("cache_summernotedata_" + fieldName);
            //        if ((cache || '') != '') {
            //            $('#' + fieldName).summernote("code", cache);
            //        }
            //    }
            //},
            //onChange: function (contents, $editable) {
            //    if ((cache || false)) {
            //        console.log('onChange:', contents, fieldName);
            //        localStorage.setItem("cache_summernotedata_" + fieldName, $('#' + fieldName).summernote("code"));
            //    }
            //}
        }
    });
}

var dyn_buttonFunctions = {};
dyn_buttonFunctions["default"] = function (context) {
    // non ritorna nulla !!!
}
dyn_buttonFunctions["training"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Training Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Training Title',
                        value: '$TITLE$'
                    },
                    {
                        text: 'Training Location',
                        value: '$LOCATION$'
                    },
                    {
                        text: 'Training Period From/To',
                        value: '$PERIOD$'
                    },
                    {
                        text: 'Training Starting Date',
                        value: '$FROM$'
                    },
                    {
                        text: 'Training Ending Date',
                        value: '$TO$'
                    },
                    {
                        text: 'Hotel Name',
                        value: '$HOTEL$'
                    },
                    {
                        text: 'Hotel Address',
                        value: '$HOTELADDRESS$'
                    },
                    {
                        text: 'Hotel Details',
                        value: '$HOTELDETAILS$'
                    },
                    {
                        text: 'Reservation Number',
                        value: '$HOTELRESERVATIONNUMBER$'
                    },
                    {
                        text: 'Participant Name',
                        value: '$NAME$'
                    },
                    {
                        text: 'Accomodation Check In',
                        value: '$CHECKIN$'
                    },
                    {
                        text: 'Accomodation Check Out',
                        value: '$CHECKOUT$'
                    },
                    {
                        text: 'Registration Info',
                        value: '$REGISTRATION$'
                    },
                    {
                        text: 'Hotel Paid by EGSPA (section)',
                        value: '$PAID_EGSPA_START$ insert you text here $PAID_EGSPA_END$'
                    },
                    {
                        text: 'Hotel Paid by Participant (section)',
                        value: '$PAID_PARTICIPANT_START$ insert you text here $PAID_PARTICIPANT_END$'
                    },
                    {
                        text: 'I need the VISA (section)',
                        value: '$VISA_START$ insert you text here $VISA_END$'
                    },
                    {
                        text: 'I do not need the VISA (section)',
                        value: '$NO_VISA_START$ insert you text here $NO_VISA_END$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });
        }
    });
    return button.render();   // return button as jquery object
}

dyn_buttonFunctions["support"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Support Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Request ID',
                        value: '$REQUESTID$'
                    },
                    {
                        text: 'Area (Code)',
                        value: '$AREACODE$'
                    },
                    {
                        text: 'Area (Title)',
                        value: '$AREATITLE$'
                    },
                    {
                        text: 'Object',
                        value: '$OBJECT$'
                    },
                    {
                        text: 'Description',
                        value: '$DESCRIPTION$'
                    },
                    {
                        text: 'Serial Number',
                        value: '$SERIALNUMBER$'
                    },
                    {
                        text: 'Location',
                        value: '$LOCATION$'
                    },
                    {
                        text: 'Date of the request',
                        value: '$REQUESTDATE$'
                    },
                    {
                        text: 'User Registration',
                        value: '$USERREGISTRATION$'
                    },
                    {
                        text: 'Country (User Registration)',
                        value: '$COUNTRY$'
                    },
                    {
                        text: 'Note/Comment',
                        value: '$NOTE$'
                    }, 
                    {
                        text: 'User',
                        value: '$USER$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });

        }
    });
    return button.render();   // return button as jquery object
}

dyn_buttonFunctions["supportopenap3p"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Support Open AP Third Party</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Request ID',
                        value: '$CODE$'
                    },
                    {
                        text: 'Date of the request',
                        value: '$REQUESTDATE$'
                    },
                    {
                        text: 'User Registration',
                        value: '$USERREGISTRATION$'
                    },
                    { 
                        text: 'InGenius SW Version',
                        value: '$SWVERSION$'
                    },
                    {
                        text: 'Customer Selection',
                        value: '$CUSTOMERSELECTION$'
                    },
                    {
                        text: 'Target',
                        value: '$TARGET$'
                    },
                    {
                        text: 'Manufacturer',
                        value: '$MANUFACTURER$'
                    },
                    {
                        text: 'PurchaseOrder',
                        value: '$PURCHASEORDER$'
                    },
                    {
                        text: 'User',
                        value: '$USER$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });

        }
    });
    return button.render();   // return button as jquery object
}

dyn_buttonFunctions["warranty"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Warranty Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Request ID',
                        value: '$REQUESTID$'
                    },
                    {
                        text: 'Code',
                        value: '$CODE$'
                    },
                    {
                        text: 'Issue Date',
                        value: '$ISSUEDATE$'
                    },
                    {
                        text: 'Spare Part',
                        value: '$SPAREPART$'
                    },
                    {
                        text: 'Decontamined',
                        value: '$DECONTAMINED$'
                    },
                    {
                        text: 'Request Notes',
                        value: '$REQUESTNOTE$'
                    },
                    {
                        text: 'Warranty Status Notes',
                        value: '$WARRANTYSTATUSNOTE$'
                    },
                    {
                        text: 'User Registration',
                        value: '$USERREGISTRATION$'
                    },
                    {
                        text: 'User',
                        value: '$USER$'
                    },
                    {
                        text: 'User Commercial Entity',
                        value: '$USERCOMMERCIALENTITY$'
                    },
                    {
                        text: 'User Customer',
                        value: '$USERCUSTOMER$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });

        }
    });
    return button.render();   // return button as jquery object
}
dyn_buttonFunctions["webrepository"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>WebRepository Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Title',
                        value: '$TITLE$'
                    },
                    {
                        text: 'Code',
                        value: '$CODE$'
                    },
                    {
                        text: 'Revision',
                        value: '$REVISION$'
                    },
                    {
                        text: 'Section',
                        value: '$SECTION$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });

        }
    });
    return button.render();   // return button as jquery object
}
dyn_buttonFunctions["assaynotification"] = function (context) {
    var ui = $.summernote.ui;

    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Assay Notification Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Assay Protocol',
                        value: '$ASSAY PROTOCOL$'
                    },
                    {
                        text: 'Deadline Mandatory',
                        value: '$DEADLINEMANDATORY$'
                    },
                    {
                        text: 'Deadline No Mandatory',
                        value: '$DEADLINENOTMANDATORY$'
                    },
                    {
                        text: 'Name File Protocol',
                        value: '$NAME FILE PROTOCOL$'
                    },
                    {
                        text: 'Serial',
                        value: '$SERIAL$'
                    },
                    {
                        text: 'Version',
                        value: '$VERSION$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });
        }
    });
    return button.render();   // return button as jquery object
}
dyn_buttonFunctions["booking"] = function (context) {
    var ui = $.summernote.ui;
    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Assay Notification Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Room/Site',
                        value: '$ROOM$'
                    },
                    {
                        text: 'Instrument',
                        value: '$SERIALNUMBER$'
                    },
                    {
                        text: 'Internal Code',
                        value: '$INTERNALCODE$'
                    },
                    {
                        text: 'From',
                        value: '$FROM$'
                    },
                    {
                        text: 'To',
                        value: '$TO'
                    },
                    {
                        text: 'Title',
                        value: '$TITLE$'
                    },
                    {
                        text: 'Note',
                        value: '$NOTE$'
                    },
                    {
                        text: 'User',
                        value: '$USER$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });
        }
    });
    return button.render();   // return button as jquery object
}
dyn_buttonFunctions["refurbish"] = function (context) {
    var ui = $.summernote.ui;
    // create button
    var button = ui.button({
        contents: '<i class="fa fa-bookmark"/> keywords',
        tooltip: 'Insert special keywords',
        click: function () {
            bootbox.prompt({
                title: "Please select a keywords below: <br/><i class='small'>Refurbish Area</i>",
                inputType: 'select',
                inputOptions: [
                    {
                        text: 'Choose one...',
                        value: ''
                    },
                    {
                        text: 'Refurbish Request Id',
                        value: '$REQUESTID$'
                    },
                    {
                        text: 'Status',
                        value: '$STATUS$'
                    },
                    {
                        text: 'Serial Number',
                        value: '$SERIALNUMBER$'
                    },
                    {
                        text: 'Commercial Entity',
                        value: '$COMMERCIALENTITY$'
                    },
                    {
                        text: 'Country',
                        value: '$SITECOUNTRY$'
                    },
                    {
                        text: 'Customer',
                        value: '$CUSTOMER$'
                    },
                    {
                        text: 'City',
                        value: '$CITY$'
                    },
                    {
                        text: 'Site',
                        value: '$SITE$'
                    },
                    {
                        text: 'Asset No.',
                        value: '$ASSETNUMBER$'
                    },
                    {
                        text: 'Residual Value',
                        value: '$RESIDUALVALUE$'
                    },
                    {
                        text: 'Residual Value Date',
                        value: '$RESIDUALVALUEDATE$'
                    },
                    {
                        text: 'Final Destination',
                        value: '$FINALDESTINATION$'
                    },
                    {
                        text: 'Needed By',
                        value: '$NEEDEDBYDATE$'
                    },
                    {
                        text: 'Additional Operations',
                        value: '$ADDITIONALOPERATIONS$'
                    },
                    {
                        text: 'Note',
                        value: '$NOTE$'
                    },
                    {
                        text: 'RDA Number',
                        value: '$RDANUMBER$'
                    },
                    {
                        text: 'Entr yDate',
                        value: '$ENTRYDATE$'
                    },
                    {
                        text: 'Processing Start Date',
                        value: '$PROCESSINGSTARTDATE$'
                    },
                    {
                        text: 'Processing End Date',
                        value: '$PROCESSINGENDDATE$'
                    },
                    {
                        text: 'SGAT WO Number',
                        value: '$SGATWONUMBER$'
                    },
                    {
                        text: 'Processing Cost',
                        value: '$PROCESSINGCOST$'
                    },
                    {
                        text: 'Spare Parts Cost',
                        value: '$SPAREPARTSCOST$'
                    },
                    {
                        text: 'Insert Date',
                        value: '$INSERTDATE$'
                    },
                    {
                        text: 'User',
                        value: '$USERINSENAME$'
                    },
                    {
                        text: 'Final Cost',
                        value: '$FINALCOST$'
                    }
                ],
                callback: function (result) {
                    if (result != null) {
                        context.invoke('editor.insertText', result);
                    }
                }
            });
        }
    });
    return button.render();   // return button as jquery object
}
// ***************************************************************

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

function getFormattedTime() {
    var today = new Date();
    var y = today.getFullYear();
    // JavaScript months are 0-based.
    var m = today.getMonth() + 1;
    var d = today.getDate();
    var h = today.getHours();
    var mi = today.getMinutes();
    var s = today.getSeconds();
    return y + padDigits(m, 2) + padDigits(d, 2) + "_" + padDigits(h, 2) + padDigits(mi, 2) + padDigits(s, 2);
}

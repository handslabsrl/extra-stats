﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Models;

namespace WebApp.Repository
{
    public class PCRTargetBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public PCRTargetBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco SerialNumber
        public List<PCRTargetModel> Get(string CustomerCode = null, string SiteCode = null, string Country = "", string PCRTargetType = null)
        {
             
            var ret = new List<PCRTargetModel>();
            List<object> args = new List<object>();

            var Sql = @"SELECT DISTINCT PATHOGEN_TARGETNAME FROM GASSAYPROGRAMS
                         INNER JOIN INSTRUMENTS 
                         ON INSTRUMENTS.SERIALNUMBER = GASSAYPROGRAMS.SERIAL
                        WHERE 1 = 1";

            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND INSTRUMENTS.SITE_COUNTRY = @Country";
                args.Add(new { Country = Country });
            }
            if (!String.IsNullOrWhiteSpace(CustomerCode))
            {
                Sql += " AND INSTRUMENTS.CUSTOMERCODE = @CustomerCode";
                args.Add(new { CustomerCode = CustomerCode });
            }
            if (!String.IsNullOrWhiteSpace(SiteCode))
            {
                Sql += " AND INSTRUMENTS.SITE_CODE = @SiteCode";
                args.Add(new { SiteCode = SiteCode });
            }
            if (!String.IsNullOrWhiteSpace(PCRTargetType) && PCRTargetType.Equals("STD")) 
            {
                Sql += " AND GASSAYPROGRAMS.ASSAYNAME LIKE  '%ELITE%'";
            }
            if (!String.IsNullOrWhiteSpace(PCRTargetType) && PCRTargetType.Equals("OPEN"))
            {
                Sql += " AND GASSAYPROGRAMS.ASSAYNAME NOT LIKE '%ELITE%'";
            }

            Sql += @" ORDER BY PATHOGEN_TARGETNAME";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<PCRTargetModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AreaBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

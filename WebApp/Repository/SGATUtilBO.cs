﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Authorization;
using WebApp.Entity;
using Microsoft.AspNetCore.Http;
using WebApp.Data;
using System.Net;

namespace WebApp.Repository
{
    public class SGATUtilBO
    {

        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly WebAppDbContext _context;

        private class InfoFromDashboar_listofsn_IN
        {
            public string cod_soc { get; set; }
            public string cod_fil { get; set; }
            public string cod_clifor { get; set; }
            public string cod_sito { get; set; }
            public string id_mac { get; set; }
            public string sn_mac { get; set; }
            public string cod_dmg { get; set; }
            public string dt_inst_eff { get; set; }
            public string cod_cfs { get; set; }
            public string cod_sito_new { get; set; }
            public string rag_sociale { get; set; }
            public string indirizzo { get; set; }
            public string localita { get; set; }
            public string cod_naz { get; set; }
        }
        private class InfoFromDashboar_params
        {
            public List<InfoFromDashboar_listofsn_IN> listofsn { get; set; }
        }
        private class InfoFromDashboar_listofsn_OUT
        {
            public string guid { get; set; }
            public string id_mac { get; set; }
            public string sn_mac { get; set; }
            public string cod_clifor { get; set; }
            public string cod_sito { get; set; }
            public string esito { get; set; }
            public string cod_sito_new { get; set; }
            public string anomalia { get; set; }
        }
        private class InfoFromDashboar_response
        {
            public string esito { get; set; }
            public string exception { get; set; }
            public string listofsn { get; set; }
        }


        public SGATUtilBO(WebAppDbContext context)
        {
            _context = context;

        }

        public string UpdateInstrumentInformations(ProcessRequestInstrumentUpdate dto)
        {
            string msgErr = "";
            string url = Lookup.GetInfoFromDashboardURL;
            string authorization = Lookup.GetInfoFromDashboardAuthorization;
            string company = Lookup.GetInfoFromDashboardCompany;
            string branch = Lookup.GetInfoFromDashboardBranch;
            string dataString = "";
            string retString = "";

            try
            {
                InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(dto.SerialNumber);
                if (instrument == null)
                {
                    msgErr = string.Format("Instrument {0} not found!", dto.SerialNumber);
                }
                if (string.IsNullOrWhiteSpace(instrument.IdMac))
                {
                    msgErr = string.Format("Instrument {0} - IdMac not valorized!", dto.SerialNumber);
                }

                _logger.Trace(string.Format("SGATUtilBO.UpdateInstrumentInformations() - CommercialStatus {0}", dto.CommercialStatus));
                string CommercialStatusCode = (new CommercialStatusBO()).GetCode(dto.CommercialStatus);
                _logger.Trace(string.Format("SGATUtilBO.UpdateInstrumentInformations() - CommercialStatusCode {0}", CommercialStatusCode));

                if (string.IsNullOrWhiteSpace(msgErr))
                {
                    InfoFromDashboar_params paramsIn = new InfoFromDashboar_params()
                    {
                        listofsn = new List<InfoFromDashboar_listofsn_IN>()
                    };
                    InfoFromDashboar_listofsn_IN item = new InfoFromDashboar_listofsn_IN()
                    {
                        cod_soc = company,
                        cod_fil = branch,
                        cod_clifor = instrument.CustomerCode,
                        cod_sito = instrument.SiteCode,
                        //id_mac = string.Format("{0}{1}", Lookup.GetEnvironment, dto.SerialNumber),   // Forzatura per ambiente DEV/TEST
                        //id_mac = string.Format("{0}{1}", Lookup.GetEnvironment, instrument.IdMac),   // Forzatura per ambiente DEV/TEST
                        id_mac = instrument.IdMac,
                        cod_dmg = CommercialStatusCode,
                        dt_inst_eff = DateTime.Now.ToString("yyyy-MM-dd"),
                        cod_cfs = dto.Version ?? "",
                        rag_sociale = dto.SiteDescription ?? "",
                        //indirizzo = "Via Nole 51",
                        localita = dto.SiteCity ?? "",
                        cod_naz = dto.Country ?? ""
                    };
                    if (dto.Operation.Equals(Lookup.Instrument_InstrumentUpdateRequest_OptCurrentSite) && instrument.Installation_Date != null) 
                    {
                        item.dt_inst_eff = ((DateTime)instrument.Installation_Date).ToString("yyyy-MM-dd");
                    }
                    if (!string.IsNullOrWhiteSpace(dto.NewSiteCode))
                    {
                        // Creazione nuovo sito 
                        item.cod_sito_new = string.Format("{0}{1}", Lookup.Instrument_SGAT_NewSitePrefix, dto.NewSiteCode);
                    }
                    if (!string.IsNullOrWhiteSpace(dto.ExistingSiteCode))
                    {
                        // Aggiornamento sito esistente 
                        item.cod_sito_new = dto.ExistingSiteCode;
                    }
                    paramsIn.listofsn.Add(item);

                    dataString = Newtonsoft.Json.JsonConvert.SerializeObject(paramsIn);

                    using (WebClient webClient = new WebClient())
                    {
                        webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                        webClient.Headers.Add("authorization", authorization);
                        retString = webClient.UploadString(new Uri(url), "POST", dataString);
                        InfoFromDashboar_response response = Newtonsoft.Json.JsonConvert.DeserializeObject<InfoFromDashboar_response>(retString);
                        List<InfoFromDashboar_listofsn_OUT> listofsn_OUT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<InfoFromDashboar_listofsn_OUT>>(response.listofsn);
                        if (response.esito.ToUpper().Equals("FALSE"))
                        {
                            msgErr = response.exception;
                        } 
                        if (listofsn_OUT.Count > 0)
                        {
                            if (listofsn_OUT[0].esito.ToUpper().Equals("FALSE"))
                            {
                                msgErr += listofsn_OUT[0].anomalia;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Lookup.GetEnvironment) && dto.SerialNumber.Equals("2071411B0019E"))
                        {
                            msgErr = "";      // FORZATURA PER TEST  --> nessun errore per SerialNumber 2071411B0019E
                        }
                        _logger.Warn(string.Format("SGATUtilBO.UpdateInstrumentInformations() - SUCCESS - url:{0}, params:{1}, ret:{2}", url, dataString, retString));
                    }
                }
            }
            catch (Exception e)
            {
                msgErr= String.Format("UpdateInstrument - {0} / {1}", e.Message, e.Source);
                _logger.Error(string.Format("SGATUtilBO.UpdateInstrumentInformations() - ERROR {0} - url:{1}, params:{2}, ret:{3}", msgErr, url, dataString, retString));
            }

            return msgErr;
        }
    }
}


﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class CommercialStatusBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public CommercialStatusBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco STATUS
        public List<CommStatusModel> Get(bool? enabledISR = null)
        {
            var ret = new List<CommStatusModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT UPPER(COMMERCIAL_STATUS) as COMMERCIALSTATUS FROM INSTRUMENTS 
                            WHERE COALESCE(COMMERCIAL_STATUS, '') <> ''";
        
            if (enabledISR != null)
            {
                Sql += @" and UPPER(COMMERCIAL_STATUS) IN(SELECT UPPER(DescTab) FROM TABGENCONFIGS WHERE IdTab = @CommercialStatusISR AND ENABLEDISR = @EnabledISR)";
                args.Add(new { CommercialStatusISR = Lookup.TabGenConfig_CommercialStatusISR });
                args.Add(new { EnabledISR = enabledISR });
            }
            Sql += " ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CommStatusModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CommercialStatusBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // lettura Codice CommercialStatus
        public string GetCode(string Description)
        {

            string ret = "";
            if (!string.IsNullOrWhiteSpace(Description))
            {
                List<object> args = new List<object>();
                //var Sql = @"SELECT top(1) Commercial_Status_Code FROM INSTRUMENTS 
                //                WHERE UPPER(COALESCE(COMMERCIAL_STATUS, '')) = @CommercialStatus ";
                var Sql = @"select Top(1) Commercial_Status_Code 
                            FROM (
                                SELECT Commercial_Status_Code FROM INSTRUMENTS
                                  WHERE UPPER(COALESCE(COMMERCIAL_STATUS, '')) = @CommercialStatus 
                                UNION 
                                SELECT COD_ELE FROM STDTABLES
                                    WHERE UPPER(COALESCE(DESCRIZ, '')) = @CommercialStatus 
                                    AND COD_TAB = 'DMG'
                                ) TMP
                            WHERE Commercial_Status_Code IS NOT NULL";
                args.Add(new { CommercialStatus = Description.ToUpper() });
                try
                {
                    using (IDatabase db = Connection)
                    {
                        ret = db.ExecuteScalar<String>(Sql, args.ToArray());
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(string.Format("GetCode.get() - ERRORE {0} {1}", e.Message, e.Source));
                    return null;
                }
            }
            return ret;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class SitesBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public SitesBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco SITE
        public List<SiteModel> GetSites(string CustomerCode = null, string UserIdCfg = "")
        {
             
            var ret = new List<SiteModel>();
            List<object> args = new List<object>();

            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            List<string> regionsCfg = new List<string>();
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                regionsCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Region);

                Entity.UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    areaCfg = userCfg.Area;
                }
            }

            var Sql = @"SELECT DISTINCT CONCAT(CUSTOMERCODE, '_', SITE_CODE) as SITE_CODE, SITE_DESCRIPTION FROM INSTRUMENTS WHERE 1 = 1";

            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }
            // ALtri filtri 
            if (!String.IsNullOrWhiteSpace(CustomerCode))
            {
                Sql += " AND UPPER(CUSTOMERCODE) IN (@customersList)";
                String[] arr = CustomerCode.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { customersList = arr });
            }

            Sql += @" ORDER BY SITE_DESCRIPTION";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<SiteModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("SitesBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // descrizione SITE
        public string GetDesc(string SiteCode, string CustomerCode)
        {

            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT TOP(1) SITE_DESCRIPTION FROM INSTRUMENTS WHERE (SITE_CODE = @siteCode OR CONCAT(CUSTOMERCODE, '_', SITE_CODE) =  @siteCode) ";
            args.Add(new { siteCode = SiteCode });
            if (!String.IsNullOrWhiteSpace(CustomerCode))
            {
                Sql += " AND CUSTOMERCODE IN (@CustomerCodeList)";
                String[] arr = CustomerCode.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { CustomerCodeList = arr });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("SitesBO.GetDesc() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

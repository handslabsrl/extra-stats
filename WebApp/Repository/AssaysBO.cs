﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class AssaysBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public AssaysBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // ASSAY
        public List<Assay> GetAllAssay()
        {
            List<Assay> ret = new List<Assay>();
            var Sql = @"select  ASSAY.AssayID, 
                                ASSAY.AssayCod, 
                                ASSAY.Attivo, 
                                ASSAY.TmstLastUpd, 
                                ASSAY.UserLastUpd, 
                                ASSAY.AssayMadre, 
                                ASSAY.AssayMadreVersione, 
                                ASSAY.Note, 
                                ASSAY.Tipo, 
                                ASSAY.Validated, 
                                ASSAY.StatoManleva, 
                                ASSAY.Analyte, 
                                ASSAY.Panel, 
                                ASSAY.BusinessType, 
                                Analyte.Descriz AS AnalyteDesc,
                                Panel.Descriz AS PanelDesc,
                                BusinessType.Descriz AS BusinessTypeDesc,
                                ASSAY.InstrumentType,
                                LastVersion.Obbligatorio, 
                                LastVersion.Date1Notify
                        from Assay
                        LEFT JOIN StdTables AS Analyte On Analyte.Cod_Tab = 'ASSAY_ANALYTE' and Analyte.Cod_Ele = ASSAY.Analyte
                        LEFT JOIN StdTables AS Panel On Panel.Cod_Tab = 'ASSAY_PANEL' and Panel.Cod_Ele = ASSAY.Panel
                        LEFT JOIN StdTables AS BusinessType On BusinessType.Cod_Tab = 'ASSAY_BUSINESS_TYPE' and BusinessType.Cod_Ele = ASSAY.BusinessType
                        LEFT JOIN v_Assay_LastVersion as LastVersion on LastVersion.AssayId = Assay.AssayID
                    ORDER BY AssayID";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Assay>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetAllAssay() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco AssaysProtocols
        public AssaysProtocolsModel GetAssaysProtocols(AssaysProcotolsIndexDTO dto, string UserId)
        {

            var ret = new AssaysProtocolsModel();
            List<object> args = new List<object>();
            var SqlFields = "";
            var SqlFrom = "";
            var SqlWhere = "";
            var SqlOrderBy = "";

            SqlFields += @"SELECT DISTINCT ASSAY.ASSAYNAME as assay_name, 
                                           ASSAY.PATHOGEN_TARGETNAME_NORM as pathogen_name, 
                                           INSTR.SITE_COUNTRY as Country, INSTR.SITE_CITY as City, 
                                           INSTR.COMPANYNAME as Customer, INSTR.SITE_DESCRIPTION as Site, 
                                           INSTR.SERIALNUMBER as SerialNumber, 
                                           INSTR.COMMERCIALENTITY as CommercialEntity, 
                                           INSTR.COMMERCIAL_STATUS as CommercialStatus,
                                           ASSAY.FileName as FileName, 
                                           ASSAY.ASSAYPROTOCOLSTATUS as AssayProtocolStatus";
            SqlFrom = @"FROM GASSAYPROGRAMS ASSAY 
                        INNER JOIN INSTRUMENTS INSTR
                          ON ASSAY.SERIAL = INSTR.SERIALNUMBER";
            //SqlFrom = @"FROM GASSAYPROGRAMS ASSAY 
            //            INNER JOIN DATAMAIN DM
            //              ON  DM.GENERALASSAYID = ASSAY.ASSAYID
            //              AND DM.SERIALNUMBER = ASSAY.SERIAL
            //            INNER JOIN INSTRUMENTS INSTR
            //              ON DM.SERIALNUMBER = INSTR.SERIALNUMBER";
            SqlOrderBy = "ORDER BY ASSAY.ASSAYNAME, ASSAY.PATHOGEN_TARGETNAME_NORM";

            SqlWhere = @" WHERE 1=1";
            
            // Filtri 
            SqlWhere += @" AND UPPER(INSTR.MODELTYPE) IN (@ModelTypeEliteInstruments)";
            args.Add(new { ModelTypeEliteInstruments = Lookup.InstrumentModelType_EliteInstruments.Select(el => el.id.ToUpper()).ToList() });

            // Country (multiselezione)
            List<string> valoriCountries = dto.countriesSel;
            if (valoriCountries == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                if (links.Count() > 0)
                {
                    valoriCountries = links;
                }
            }
            if (valoriCountries != null)
            {
                SqlWhere += " AND INSTR.SITE_COUNTRY IN (@countriesList)";
                var arr = valoriCountries.Select(el => el).ToList();
                args.Add(new { countriesList = arr });
                ret.filters.Add("Country", string.Join(" / ", arr.ToArray()));
            }
            else
            {
                if (dto.citiesSel == null && dto.customersSel == null && dto.instrumentsSel == null &&
                    dto.regionsSel == null && String.IsNullOrWhiteSpace(dto.Area))
                {
                    ret.filters.Add("Country", "ALL");
                }
            }
            // City (multiselezione)
            if (dto.citiesSel != null)
            {
                SqlWhere += " AND INSTR.SITE_CITY IN (@citiesList)";
                var arr = dto.citiesSel.Select(el => el).ToList();
                args.Add(new { citiesList = arr });
                ret.filters.Add("City", string.Join(" / ", arr.ToArray()));
            }
            // Customers (multiselezione)
            List<string> valoriCustomers = dto.customersSel;
            if (valoriCustomers == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                if (links.Count() > 0)
                {
                    valoriCustomers = links;
                }
            }
            if (valoriCustomers != null)
            {
                SqlWhere += " AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                List<string> arr = valoriCustomers.Select(el => el.ToUpper()).ToList();
                args.Add(new { customersList = arr });
                List<string> arrDeco = new List<string>(arr);
                for (int i = 0; i < arr.Count; i++)
                {
                    arrDeco[i] = new CustomersBO().GetDesc(arr[i]);
                }
                ret.filters.Add("Customer", string.Join(" / ", arrDeco.ToArray()));
            }
            // SiteCode (multiselezione)
            if (dto.siteCodesSel != null)
            {
                SqlWhere += " AND CONCAT(INSTR.CUSTOMERCODE, '_', INSTR.SITE_CODE) IN (@siteCodesList)";
                var arr = dto.siteCodesSel.Select(el => el).ToList();
                args.Add(new { siteCodesList = arr });
                List<string> arrDeco = new List<string>(arr);
                for (int i = 0; i < arr.Count; i++)
                {
                    arrDeco[i] = (new SitesBO().GetDesc(arr[i], (dto.customersSel == null ? "" : string.Join(",", dto.customersSel))));
                }
                ret.filters.Add("Site", string.Join(" / ", arrDeco.ToArray()));
            }
            // Instrument (multiselezione)
            if (dto.instrumentsSel != null)
            {
                SqlWhere += " AND INSTR.SerialNumber IN (@SerialNumberList)";
                var arr = dto.instrumentsSel.Select(el => el).ToList();
                args.Add(new { SerialNumberList = arr });
                ret.filters.Add("Instrument", string.Join(" / ", arr.ToArray()));
            }
            // Region (multiselezione)
            List<string> valoriRegions = dto.regionsSel;
            if (valoriRegions == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Region);
                if (links.Count() > 0)
                {
                    valoriRegions = links;
                }
            }
            if (valoriRegions != null)
            {
                SqlWhere += " AND INSTR.REGION IN (@regionsList)";
                List<string> arr = valoriRegions.Select(el => el.ToUpper()).ToList();
                args.Add(new { regionsList = arr });
                ret.filters.Add("Region", string.Join(" / ", arr.ToArray()));
            }
            // Area 
            if (!String.IsNullOrWhiteSpace(dto.Area))
            {
                SqlWhere += " AND INSTR.AREA = @Area";
                args.Add(new { Area = dto.Area });
                ret.filters.Add("Area", dto.Area);
            }
            // CommercialStatus 
            if (!String.IsNullOrWhiteSpace(dto.commercialStatus))
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIAL_STATUS) = @CommercialStatus";
                args.Add(new { CommercialStatus = dto.commercialStatus.ToUpper() });
                ret.filters.Add("Commercial Status", dto.commercialStatus.ToUpper());
            }
            // CommercialEntity (Multiselezione) 
            List<string> valoriCommercialEntities = dto.commercialEntitiesSel;
            if (valoriCommercialEntities == null)
            {
                List<string> links = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                if (links.Count() > 0)
                {
                    valoriCommercialEntities = links;
                }
            }
            if (valoriCommercialEntities != null)
            {
                SqlWhere += " AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitySel)";
                List<string> arr = valoriCommercialEntities.Select(el => el.ToUpper()).ToList();
                args.Add(new { commercialEntitySel = arr });
                ret.filters.Add("Commercial Entity", string.Join(" / ", arr.ToArray()));
            }
            // SampleType
            if (dto.SampleType != null)
            {
                SqlWhere += " AND ASSAY.SAMPLETYPES = @SampleType";
                args.Add(new { SampleType = dto.SampleType });
                ret.filters.Add("Sample Type", Lookup.SampleTypesDecode(dto.SampleType));
            }
            // PathogenName 
            if (!String.IsNullOrWhiteSpace(dto.PathogenName))
            {
                SqlWhere += " AND ASSAY.PATHOGEN_TARGETNAME_NORM = @PathogenName";
                args.Add(new { PathogenName = dto.PathogenName });
                ret.filters.Add("Pathogen Name", dto.PathogenName);
            }
            // Assay Name
            if (!String.IsNullOrWhiteSpace(dto.AssayName))
            {
                SqlWhere += " AND ASSAY.ASSAYNAME = @AssayName";
                args.Add(new { AssayName = dto.AssayName });
                ret.filters.Add("Assay Name", dto.AssayName);
            }
            // Installation Date FROM/TO 
            if (dto.InstallDateFrom != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) >= CONVERT(datetime, @InstallDateFrom, 103)";
                args.Add(new { InstallDateFrom = ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date From", ((DateTime)dto.InstallDateFrom).ToString("dd/MM/yyyy"));
            }
            if (dto.InstallDateTo != null)
            {
                SqlWhere += " AND CONVERT(date, INSTR.INSTALLATION_DATE) <= CONVERT(datetime, @InstallDateTo, 103)";
                args.Add(new { InstallDateTo = ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy") });
                ret.filters.Add("Installation Date To", ((DateTime)dto.InstallDateTo).ToString("dd/MM/yyyy"));
            }
            if (dto.excludeDismiss == true)
            {
                SqlWhere += @" AND CHARINDEX('_DIS_', UPPER(INSTR.SERIALNUMBER)) = 0 ";
                ret.filters.Add("Instr.Dismiss", "Excluded");
            }
            // ModelType (multiselezione)
            List<string> valoriModelType = dto.modelTypesSel;
            if (valoriModelType != null)
            {
                SqlWhere += " AND UPPER(INSTR.MODELTYPE) IN (@modelTypeSel)";
                List<string> arr = valoriModelType.Select(el => el.ToUpper()).ToList();
                args.Add(new { modelTypeSel = arr });
                ret.filters.Add("Model Type", string.Join(" / ", arr.Select(x => Lookup.GetInstrumentModelDescbyCode(x))));
            }

            // Estrazione Dati SERIE
            var sqlDetail = "";
            sqlDetail = SqlFields + " " + SqlFrom + " " + SqlWhere + " " + SqlOrderBy;
            try
            {
                using (IDatabase db = Connection)
                {
                    ret.AssaysProtocolsDetails = db.Query<AssaysProtocolsDetails>(sqlDetail, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetAssaysProtocols() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // elenco Pathogens
        public List<PathogenModel> GetPathogens()
        {
            var ret = new List<PathogenModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT PATHOGEN_TARGETNAME_NORM as NAME 
                          FROM GASSAYPROGRAMS 
                         ORDER BY PATHOGEN_TARGETNAME_NORM";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<PathogenModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetPathogens() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco assayNames
        public List<AssayModel> GetNames()
        {
            var ret = new List<AssayModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASSAYNAME as NAME 
                          FROM GASSAYPROGRAMS 
                         ORDER BY ASSAYNAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<AssayModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("AssaysBO.GetNames() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // riepilogo Versions 
        public string GetAllVersions(int AssayID)
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT *  FROM ASSAYVERSION
                        WHERE ASSAYID = @AssayId
                        ORDER BY AssayVersionId";
            args.Add(new { AssayId = AssayID });
            try
            {
                using (IDatabase db = Connection)
                {
                    List<AssayVersion> data = db.Query<AssayVersion>(Sql, args.ToArray()).ToList();
                    foreach (var item in data)
                    {
                        ret += String.IsNullOrWhiteSpace(ret) ? "" : "\n";
                        ret += string.Format("{0} [{1}] {2} {3}", item.Version.Trim(), item.StatusSendDesc, (item.Obbligatorio  ? "[MANDATORY]" : "") , (item.Attivo ? "[ACTIVE]" : ""));
                    }
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("GetAllVersions.get() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.Error(msg);
                return msg;
            }
            return ret;
        }

        // ultima VERSIONE
        public string GetLastVersion(int AssayID)
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT TOP(1) * FROM ASSAYVERSION
                        WHERE ASSAYID = @AssayId
                        ORDER BY AssayVersionId DESC";
            args.Add(new { AssayId = AssayID });
            try
            {
                using (IDatabase db = Connection)
                {
                    List<AssayVersion> data = db.Query<AssayVersion>(Sql, args.ToArray()).ToList();
                    if (data.Count > 0)
                    {
                        ret = data[0].Version;
                    }
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("GetAllVersions.get() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.Error(msg);
                return msg;
            }
            return ret;
        }

        // Strumenti Abilitati e Lettera di Manleva x Assay
        // passare AssayId = 0 per estrarre TUTTI gli assay relativi ai customers/Instruments di competenza !!! 
        public List<ManlevaDTO> GetLettersByAssay(int AssayId, string UserId, string letterType)
        {
            var ret = new List<ManlevaDTO>();
            List<object> args = new List<object>();
            List<string> countriesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
            List<string> commercialEntitiesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
            List<string> regionsCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Region);
            var Sql = @"SELECT DISTINCT INSTR.CUSTOMERCODE,  INSTR.CompanyName, LETTERASSAYS.ASSAYCOD,
                                        MANLEVA.LetterId, MANLEVA.FileName,  MANLEVA.SignedFileName, MANLEVA.SignedTmst,
                                        COALESCE(SignedUser.FULLNAME, MANLEVA.SignedUser) AS SignedUser
		                FROM INSTRUMENTS INSTR
                        INNER JOIN LETTERS MANLEVA 
                            ON  MANLEVA.CUSTOMERCODE = INSTR.CUSTOMERCODE
                            -- AND MANLEVA.SERIALNUMBER = INSTR.SERIALNUMBER  
                            AND MANLEVA.LETTERTYPE = @LetterType
                        INNER JOIN LETTERASSAYS LETTERASSAYS
                            ON LETTERASSAYS.LETTERID = MANLEVA.LETTERID
                            AND LETTERASSAYS.DISABLED = 0
                        INNER JOIN ASSAY ASSAY
                            ON ASSAY.ASSAYCOD =  LETTERASSAYS.ASSAYCOD
						LEFT JOIN UserCfg  SignedUser
							 ON SignedUser.UserID = MANLEVA.SignedUser
						INNER JOIN UserCfg
							 ON Usercfg.UserID = @UserId
							 AND (UserCFg.area IS NULL or UserCFg.area = INSTR.Area)";
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND INSTR.Site_Country IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.CommercialEntity) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            Sql += @" WHERE COALESCE(INSTR.Command, '') <> @CommandDelete";

            // Solo ELITE INSTRUMENTS
            Sql += @" AND UPPER(INSTR.MODELTYPE) IN (@ModelTypeEliteIntruments)";
            args.Add(new { ModelTypeEliteIntruments = Lookup.InstrumentModelType_EliteInstruments.Select(el => el.id.ToUpper()).ToList() });

            // Filtro per strumenti NON Dismessi
            Sql += @" AND CHARINDEX('_DIS_', UPPER(INSTR.SerialNumber)) = 0";
            if (AssayId > 0)
            { 
                Sql += @" AND ASSAY.ASSAYID = @AssayId";
                args.Add(new { AssayId = AssayId });
            }
            Sql += @" ORDER BY INSTR.CUSTOMERCODE";

            
            args.Add(new { LetterType = letterType });
            args.Add(new { CommandDelete = "DELETE" });
            args.Add(new { UserId = UserId });
            args.Add(new { InstallBase = Lookup.FileDataType_InstallBase });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<ManlevaDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetLettersByAssay.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

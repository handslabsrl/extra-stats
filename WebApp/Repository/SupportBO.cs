﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using WebApp.Entity;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace WebApp.Repository
{
    public class SupportBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public SupportBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }


        // elenco Classification
        public List<TKClassification> GetClassifications(bool bHideRoot = false, int excludeClassificationId = 0, bool bExpandAll = false,
                                                            bool bLoadBreadCrumb = false, string configId = "")
        {
            var ret = new List<TKClassification>();
            List<object> args = new List<object>();
            var SqlFields = @"SELECT CLASSIF.TKClassificationId, CLASSIF.CODE, CLASSIF.ParentId, CLASSIF.Title, CLASSIF.INSTRUMENT, 
                                     CLASSIF.Reserved, CLASSIF.NOTIFYAREAMANAGER,
                                     CLASSIF.ADDINFO, CLASSIF.TKADDINFOID, ADDINFO.Title AS ADDINFOTITLE, 
                                     CLASSIF.CLOSINGINFO, CLASSIF.TKCLOSINGINFOID, CLOSINGINFO.Title AS CLOSINGINFOTITLE,
                                     CLASSIF.CLAIMLOGFILE, CLASSIF.OTHERPLATFORM, CLASSIF.CODART,
                                     CLASSIF.INFOCONTACT, CLASSIF.QUANTITY, CLASSIF.TODOLIST, 
                                     CLASSIF.SUPPLIERENGAGEMENT, CLASSIF.CUSTOMERSGAT, CLASSIF.LOCATIONSGAT";
            var SqlFrom = @"FROM TKCLASSIFICATIONS CLASSIF
                            LEFT JOIN TKAddInfos ADDINFO ON ADDINFO.TKADDINFOID = CLASSIF.TKADDINFOID
                            LEFT JOIN TKClosingInfos CLOSINGINFO ON CLOSINGINFO.TKCLOSINGINFOID = CLASSIF.TKCLOSINGINFOID";
            var sqlWhere = "WHERE CLASSIF.DELETED = 0";
            var sqlGroupBy = "";
            var sqlOrderBy = " ORDER BY CLASSIF.TITLE, CLASSIF.TKClassificationID";
            if (excludeClassificationId > 0)
            {
                sqlWhere += " AND CLASSIF.TKClassificationId NOT IN (SELECT TKClassificationId FROM TKCLASSIFICATIONPATHS WHERE TKClassificationIdRef = @ClassificationToExclude)";
                args.Add(new { ClassificationToExclude = excludeClassificationId });
            }
            // Valorizzazione attributo BreadCrumb
            if (bLoadBreadCrumb)
            {
                SqlFields += ", COALESCE(PATH.BREADCRUMB, CLASSIF.TITLE) AS BREADCRUMB";
                SqlFrom += @" LEFT JOIN TKCLASSIFICATIONPATHS PATH ON PATH.TKCLASSIFICATIONIDREF = CLASSIF.TKClassificationId AND PATH.TKCLASSIFICATIONIDREF = PATH.TKCLASSIFICATIONID";
            }
            if (!string.IsNullOrWhiteSpace(configId))
            {
                sqlWhere += " AND CLASSIF.ConfigId = @ConfigId";
                args.Add(new { ConfigId = configId });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{SqlFields} {SqlFrom} {sqlWhere} {sqlGroupBy} {sqlOrderBy}";
                    ret = db.Query<TKClassification>(sql, args.ToArray()).ToList();
                    if (!bHideRoot)
                    {
                        // aggiungo un record fittizio come root (id = -1) 
                        ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                        ret.Add(new TKClassification() { TKClassificationId = -1, Title = "Requests Classification", Expanded = true });
                    }
                    if (bExpandAll)
                    {
                        ret.ForEach(o => o.Expanded = true);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetClassifications.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco Classification
        public List<ClassificationExportDTO> GetClassificationsForExport(string configId)
        {
            var ret = new List<ClassificationExportDTO>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT CLASSIF.TKCLASSIFICATIONID AS ID, CLASSIF.PARENTID, CLASSIF.CODE, CLASSIF.TITLE, 
                                COALESCE(PATH.BREADCRUMB, CLASSIF.TITLE) as BREADCRUMB, 
                                CLASSIF.RESERVED, CLASSIF.DELETED, CLASSIF.NOTIFYAREAMANAGER,
                                CLASSIF.INSTRUMENT, CLASSIF.ADDINFO AS ADDITIONALINFO, ADDINFO.TITLE AS ADDITIONALINFOTITLE,
                                CLASSIF.CLOSINGINFO, CLOSINGINFO.TITLE AS CLOSINGINFOTITLE, 
                                CLASSIF.CLAIMLOGFILE, CLASSIF.OTHERPLATFORM, CLASSIF.CODART AS PRODUCT,
                                CLASSIF.INFOCONTACT, CLASSIF.QUANTITY, CLASSIF.TODOLIST, 
                                CLASSIF.SUPPLIERENGAGEMENT, CLASSIF.CUSTOMERSGAT, CLASSIF.LOCATIONSGAT
                           FROM TKCLASSIFICATIONS CLASSIF
                           LEFT JOIN TKCLASSIFICATIONPATHS PATH
                              ON PATH.TKCLASSIFICATIONID = CLASSIF.TKCLASSIFICATIONID
                             AND PATH.TKCLASSIFICATIONIDREF = CLASSIF.TKCLASSIFICATIONID
                           LEFT JOIN TKADDINFOS ADDINFO
                              ON ADDINFO.TKADDINFOID = CLASSIF.TKADDINFOID
                           LEFT JOIN TKCLOSINGINFOS CLOSINGINFO
                              ON CLOSINGINFO.TKCLOSINGINFOID = CLASSIF.TKCLOSINGINFOID
                          WHERE CLASSIF.ConfigId = @ConfigId
                        ORDER BY PATH.BREADCRUMB, CLASSIF.TITLE,  CLASSIF.TKCLASSIFICATIONID";
            args.Add(new { ConfigId = configId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<ClassificationExportDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetClassificationsForExport.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }


        public List<ClassificationAreaDTO> GetClassificationAreas(string context, string userName)
        {
            var ret = new List<ClassificationAreaDTO>();
            List<object> args = new List<object>();

            string UserId = (new UsersBO()).GetUserId(userName);
            List<string> tkSupportTypesLink = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.TKSupportType);
            if (tkSupportTypesLink.Count == 0)
            {
                return ret;
            }

            bool bCountPendingNoComplaint = (context.Equals(Lookup.TKRequest_ActionIndex_Manage));

            var Sql = $@"SELECT TKClassificationId AS ID, TITLE as TEXT, CONFIGID";
            if (bCountPendingNoComplaint) 
            {
                Sql += $@", (SELECT COUNT(*)
                               FROM TKREQUESTS REQ
                              INNER JOIN TKCLASSIFICATIONPATHS PATH
                                ON PATH.TKCLASSIFICATIONID = REQ.TKCLASSIFICATIONID
                                AND PATH.TKCLASSIFICATIONIDREF = TKCLASSIFICATIONS.TKCLASSIFICATIONID
                                WHERE REQ.STATUS IN (@StateIn) AND REQ.COMPLAINT = 0) AS CNTPENDING";
                string[] StateIn = new string[] { Lookup.TKRequest_Status_PENDING, Lookup.TKRequest_Status_REOPENED };
                args.Add(new { StateIn = StateIn });
            }
            Sql += $@" FROM TKCLASSIFICATIONS 
                           WHERE DELETED = 0
                             AND PARENTID IS NULL 
                             AND CONFIGID IN (@configIdList)";
            args.Add(new { configIdList = tkSupportTypesLink.Select(el => el).ToList() });

            if (context.Equals(Lookup.TKRequest_ActionIndex_Complaint))
            {
                List<TKSupportConfigModel> configs = Lookup.GetTKSupportConfigs();
                Sql += " AND CONFIGID IN (@HasComplaint)";
                args.Add(new { HasComplaint = configs.Where(a => a.HasComplaint).Select(a => a.ID).ToList()});
            }

            Sql += " ORDER BY TITLE";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<ClassificationAreaDTO>(Sql, args.ToArray()).ToList();
                    foreach (var item in ret)
                    {
                        item.configModel = Lookup.GetTKSupportConfig(item.configId);
                        if (bCountPendingNoComplaint)
                        {
                            if (item.cntPending > 0)
                            {
                                item.text += string.Format(" [{0}]", item.cntPending);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetClassificationAreas.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // recupera la ROOT di una classificazione; 
        public TKClassification GetClassificationRoot(int TKClassificationId)
        {
            var ret = new TKClassification();
            List<object> args = new List<object>();
            var Sql = $@"SELECT RADICE.* FROM TKCLASSIFICATIONS RADICE
                            WHERE RADICE.TKCLASSIFICATIONID IN (
                            SELECT TOP(1) CHILD.TKCLASSIFICATIONIDREF 
                                FROM TKCLASSIFICATIONPATHS CHILD
                                WHERE CHILD.TKCLASSIFICATIONID = @Id
                                AND NOT EXISTS (SELECT 1 FROM TKCLASSIFICATIONPATHS REF
                                WHERE REF.TKCLASSIFICATIONID != REF.TKCLASSIFICATIONIDREF
                                AND REF.TKCLASSIFICATIONID = CHILD.TKCLASSIFICATIONIDREF)
                            )";
            args.Add(new { Id = TKClassificationId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<TKClassification>(Sql, args.ToArray()).ToList().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetClassificationRoot.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        public List<IndexRequestDTO> GetRequests(int ClassificationAreaId, string UserName, string UserId,
                                                 Lookup.GetRequestsType getType, bool bGetClosed, bool bGetOnlyRequestData)
        {
            var ret = new List<IndexRequestDTO>();
            List<object> args = new List<object>();
            string fldUserRif = "USERINCHARGEOF";
            string sqlCheckComplaint = "";

            if (getType == Lookup.GetRequestsType.YourRequests)
            {
                fldUserRif = "USERREGISTRATION";
            }
            if (getType == Lookup.GetRequestsType.Manage)
            {
                sqlCheckComplaint = " AND REQ.COMPLAINT = 0";
            }
            if (getType == Lookup.GetRequestsType.ManageComplaint || getType == Lookup.GetRequestsType.onlyCCA)
            {
                sqlCheckComplaint = " AND REQ.COMPLAINT = 1 ";
                if (bGetOnlyRequestData == true)
                {
                    sqlCheckComplaint += " AND COALESCE(REQ.COMPLAINTSTATUS, '')  = @RequestData ";
                    args.Add(new { RequestData = Lookup.Code_Complaint_Status_REQUEST_DATA });
                }
            }

            // *** CNTNEWS - Conteggio News ***
            // - MANAGE: numero di operazioni (qualsiasi tipo) eseguite da altri utenti, DOPO l'ultima operazione tracciata per l'utente che ha in carico la richiesta !!! 
            // - YourRequests: numero di operazioni PUBBLICHE eseguite da altri utenti, DOPO l'ultima operazione tracciata per l'utente che ha registrato la richiesta !!! 
            var Sql = $@"SELECT REQ.TKREQUESTID, REQ.CODE, REQ.OBJECT, REQ.STATUS, REQ.TKCLASSIFICATIONID, REQ.TMSTCLOSED, REQ.TMSTINCHARGEOF,
                                REQ.TMSTINSE, REQ.TMSTLASTUPD, REQ.USERCLOSED, REQ.USERINCHARGEOF, REQ.USERINSE, REQ.USERLASTUPD, REQ.USERREGISTRATION, 
                                REQ.TKADDINFOID, REQ.EMAILSKEEPINFORMED, REQ.SERIALNUMBER, INSTR.SITE_COUNTRY AS COUNTRY, INSTR.SITE_CITY AS CITY, 
                                INSTR.COMMERCIALENTITY, 
                                COALESCE(USER_REG.FULLNAME, REQ.USERREGISTRATION) AS USERREGISTRATION_NAME,
                                COALESCE(USER_WIP.FULLNAME, REQ.USERINCHARGEOF) AS USERINCHARGEOF_NAME,
                                IIF(COALESCE(REQ.{fldUserRif}, '') = @UserName, '1', '9') AS PRIORITY,
                                (SELECT MAX(EVT.TMSTINSE) FROM TKEVENTS EVT 
                                     WHERE EVT.TKREQUESTID = REQ.TKREQUESTID
                                     AND   EVT.RESERVED = 0
                                     AND   EVT.Operation IN (@OperationLastEvt)) AS LASTPUBLICEVT,
                                REQ.USERREGISTRATION_LASTACCESS,
                                REQ.COMPLAINT, REQ.COMPLAINTLOTNUMBER, REQ.COMPLAINTCUSTOMER, REQ.COMPLAINTCITY,
                                REQ.COMPLAINTINCIDENT, REQ.COMPLAINTINCIDENTNOTE, 
                                REQ.COMPLAINTLINKOMMINFO, REQ.TKCOMPLAINTCLASSIFICATIONID,
                                COALESCE(USER_LINKOMM.FULLNAME, REQ.USERLINKOMMTRACKER) AS USERLINKOMMTRACKER_NAME,
                                REPLACE(COALESCE(CMPLCLASSIFPATH.BREADCRUMB, CMPLCLASSIF.TITLE), CONCAT(COALESCE(CMPLCLASSIFPATH.ROOTTITLE, ''), ' > '), '') AS COMPLAINTCLASSIFICATIONDECO,
                                REQ.COMPLAINTSTATUS, CMPLSTATUS.DESCRIPTION AS COMPLAINTSTATUSDECO,
                                REQ.PERCTODOLIST,
                                (SELECT MIN(COMPANYNAME) FROM V_UsersLinkCustomers AS USERSLINK
									WHERE USERSLINK.USERNAME = REQ.USERREGISTRATION) AS USERREGISTRATION_FIRSTCUSTOMER,
                                (SELECT Count(*) FROM   tkevents EVT
                                    WHERE  EVT.tkrequestid = REQ.tkrequestid
                                       AND EVT.operation IN ( @NoteUser2Support )
                                       AND EVT.tkeventid > COALESCE((SELECT Max(LASTREAD.tkeventid) FROM TKEVENTREADS LASTREAD
                                                                        WHERE  LASTREAD.tkrequestid = REQ.tkrequestid
                                                                        AND LASTREAD.userinse = @UserName), 0)) AS CntNews_User2Support,
                                (SELECT Count(*) FROM   tkevents EVT
                                    WHERE  EVT.tkrequestid = REQ.tkrequestid
                                       AND EVT.operation IN ( @NoteSupport2User )
                                       AND EVT.tkeventid > COALESCE((SELECT Max(LASTREAD.tkeventid) FROM TKEVENTREADS LASTREAD
                                                                        WHERE  LASTREAD.tkrequestid = REQ.tkrequestid
                                                                        AND LASTREAD.userinse = @UserName), 0)) AS CntNews_Support2User,
                                (SELECT Count(*) FROM   tkevents EVT
                                    WHERE  EVT.tkrequestid = REQ.tkrequestid
                                       AND EVT.operation IN ( @NoteSupport2CCA )
                                       AND EVT.tkeventid > COALESCE((SELECT Max(LASTREAD.tkeventid) FROM TKEVENTREADS LASTREAD
                                                                        WHERE  LASTREAD.tkrequestid = REQ.tkrequestid
                                                                        AND LASTREAD.userinse = @UserName), 0)) AS CntNews_Support2CCA,
                                (SELECT Count(*) FROM   tkevents EVT
                                    WHERE  EVT.tkrequestid = REQ.tkrequestid
                                       AND EVT.operation IN ( @NoteCCA2Support )
                                       AND EVT.tkeventid > COALESCE((SELECT Max(LASTREAD.tkeventid) FROM TKEVENTREADS LASTREAD
                                                                        WHERE  LASTREAD.tkrequestid = REQ.tkrequestid
                                                                        AND LASTREAD.userinse = @UserName), 0)) AS CntNews_CCA2Support
                        FROM TKREQUESTS REQ
                           LEFT JOIN INSTRUMENTS INSTR
                                 ON INSTR.SERIALNUMBER = REQ.SERIALNUMBER 
                           INNER JOIN TKCLASSIFICATIONPATHS PATH 
                                 ON PATH.TKCLASSIFICATIONID = REQ.TKCLASSIFICATIONID 
                                 AND PATH.TKCLASSIFICATIONIDREF = @Area
	                        LEFT JOIN TKADDINFOS  CMPLCLASSIF
                                   ON CMPLCLASSIF.TKADDINFOID = REQ.TKCOMPLAINTCLASSIFICATIONID
	                        LEFT JOIN TKADDINFOPATHS CMPLCLASSIFPATH
	                               ON CMPLCLASSIFPATH.TKADDINFOID = REQ.TKCOMPLAINTCLASSIFICATIONID 
                                   AND CMPLCLASSIFPATH.TKADDINFOIDREF = REQ.TKCOMPLAINTCLASSIFICATIONID 
                            LEFT JOIN CODES CMPLSTATUS ON CMPLSTATUS.CODEID = REQ.COMPLAINTSTATUS
                           INNER JOIN ASPNETUSERS USERCFG_REG ON REQ.USERREGISTRATION = USERCFG_REG.USERNAME
                           INNER JOIN USERCFG USER_REG ON USERCFG_REG.ID = USER_REG.USERID
                           LEFT JOIN ASPNETUSERS USERCFG_WIP ON REQ.USERINCHARGEOF = USERCFG_WIP.USERNAME
                           LEFT JOIN USERCFG USER_WIP ON USERCFG_WIP.ID = USER_WIP.USERID
                           LEFT JOIN ASPNETUSERS USERCFG_LINKOMM ON REQ.USERLINKOMMTRACKER = USERCFG_LINKOMM.USERNAME
                           LEFT JOIN USERCFG USER_LINKOMM ON USERCFG_LINKOMM.ID = USER_LINKOMM.USERID
                        WHERE REQ.STATUS NOT IN (@StateNotIn)";
            if (!string.IsNullOrWhiteSpace(sqlCheckComplaint))
            {
                Sql += sqlCheckComplaint;
            }
            args.Add(new { Area = ClassificationAreaId });
            args.Add(new { UserName = UserName });
            args.Add(new { OperationLastEvt = new string[] { Lookup.TKEvent_Operation_ADDATTACH,
                                                             Lookup.TKEvent_Operation_REMOVEATTACH,
                                                             Lookup.TKEvent_Operation_ADDNOTE_INFO,
                                                             Lookup.TKEvent_Operation_REOPEN,
                                                             Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER} });
            args.Add(new { NoteUser2Support = new string[] { Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT } });
            args.Add(new { NoteSupport2User = new string[] { Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER } });
            args.Add(new { NoteSupport2CCA = new string[] { Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA} });
            args.Add(new { NoteCCA2Support = new string[] { Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT } });

            //, (SELECT 1 FROM TKEVENTS EVT
            //WHERE EVT.TKREQUESTID = REQ.TKREQUESTID
            //AND OPERATION = @OperationAssign
            //AND EVT.TKEVENTID = (SELECT MAX(EVT2.TKEVENTID) FROM TKEVENTS EVT2
            //    WHERE EVT2.TKREQUESTID = EVT.TKREQUESTID)) AS ASSIGNED
            //args.Add(new { OperationAssign = Lookup.TKEvent_Operation_ASSIGN });
            string[] StateNotIn = new string[] { Lookup.TKRequest_Status_DRAFT, Lookup.TKRequest_Status_DELETED };
            if ((getType == Lookup.GetRequestsType.Manage || getType == Lookup.GetRequestsType.ManageComplaint || getType == Lookup.GetRequestsType.onlyCCA) && bGetClosed == false)
            {
                Array.Resize(ref StateNotIn, StateNotIn.Length + 1);
                StateNotIn[StateNotIn.Length - 1] = Lookup.TKRequest_Status_CLOSED;
            }
            args.Add(new { StateNotIn = StateNotIn });

            // Ticket propri o della propria organizzazione per elenco YOUR REQUESTS
            if (getType == Lookup.GetRequestsType.YourRequests)
            {
                Sql += " AND  (REQ.USERREGISTRATION = @UserNameRegistration";
                args.Add(new { UserNameRegistration = UserName });

                // filtro dei ticket della stessa Country per lo strumento selezionato oppure delle country dell'utente (se non c'è lo strumento)
                List<string> countriesCfg = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                if (countriesCfg.Count > 0)
                {
                    Sql += @" OR (REQ.SERIALNUMBER IS NOT NULL AND UPPER(INSTR.SITE_COUNTRY) IN (@countriesList))
                              OR (REQ.serialnumber IS NULL AND (SELECT COUNT(*) FROM USERLINKS 
                                                                 WHERE USERLINKS.USERID = USER_REG.USERID
						                                           AND USERLINKS.TYPEID = @TypeCountry
                                                                   AND USERLINKS.LINKID IN (@countriesList) ) > 0)";
                    args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
                    args.Add(new { TypeCountry = Lookup.UserLinkType.Country });
                }
                Sql += ")";
            }
            // per onlyCCA (Customer Complaint Analyst), estraggo solo le richieste che hanno ComplaintStatus = ForwartToCCA o OpenToLinkomm
            if (getType == Lookup.GetRequestsType.onlyCCA)
            {
                //Sql += " AND REQ.COMPLAINTSTATUS IN (@ComplaintStatusCCA)";
                //string[] ComplaintStatusCCA = new string[] { Lookup.Code_Complaint_Status_FORWARD_TO_CCA, Lookup.Code_Complaint_Status_OPEN_ON_LINKOMM };
                //args.Add(new { ComplaintStatusCCA = ComplaintStatusCCA });
            }
            Sql += " ORDER BY REQ.TKREQUESTID";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<IndexRequestDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequests.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public List<TKRequest> GetRequests4AP3P()
        {
            var ret = new List<TKRequest>();
            List<object> args = new List<object>();

            var Sql = $@"SELECT REQ.TKREQUESTID, REQ.OBJECT
                           FROM TKREQUESTS REQ
                        WHERE REQ.STATUS IN (@StateIn)
                         AND REQ.TKCLASSIFICATIONID = @ClassificationId";
            args.Add(new { ClassificationId = Lookup.GetClassificationId4AP3P() });
            string[] StateIn = new string[] { Lookup.TKRequest_Status_PENDING, Lookup.TKRequest_Status_PROCESSING };
            args.Add(new { StateIn = StateIn });
            Sql += " ORDER BY REQ.TKREQUESTID";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<TKRequest>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequests4AP3P.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public List<TKAttachment> GetRequestAttachments(int requestId, bool draft = false, string userInseDraft = "", bool bOnlyPublic = true)
        {
            var ret = new List<TKAttachment>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT TKATTACHMENTS.*, COALESCE(USERCFG.FULLNAME, TKATTACHMENTS.USERINSE) AS USERINSENAME
                           FROM TKATTACHMENTS  
                           INNER JOIN  ASPNETUSERS ON TKATTACHMENTS.USERINSE = ASPNETUSERS.USERNAME
                           INNER JOIN  USERCFG ON ASPNETUSERS.ID = USERCFG.USERID
                           WHERE TKREQUESTID = @requestId 
                             AND TKATTACHMENTS.DELETED = 0
                             AND TKATTACHMENTS.DRAFT = @draft";
            args.Add(new { requestId = requestId });
            args.Add(new { draft = draft });

            if (draft && !string.IsNullOrWhiteSpace(userInseDraft))
            {
                Sql += " AND TKATTACHMENTS.USERINSE = @userInseDraft";
                args.Add(new { userInseDraft = userInseDraft });
            }
            if (bOnlyPublic)
            {
                Sql += " AND TKATTACHMENTS.RESERVED = 0";
            }
            Sql += " ORDER BY TKATTACHMENTS.TKATTACHMENTID";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<TKAttachment>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequestAttachments.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        public List<StatisticsDTO> GetStatistics(int ClassificationId, DateTime DateFrom, DateTime DateTo, string ConfigId)
        {
            var ret = new List<StatisticsDTO>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT REQ.TKREQUESTID,  REQ.STATUS, REQ.TMSTINSE,
	                            PATH.ROOTTITLE AS AREA, COALESCE(PATH.BREADCRUMB, CLASSIF.TITLE) AS CLASSIFICATION, 
	                            REQ.SERIALNUMBER, REQ.OTHERPLATFORM, REQ.CODART, INSTR.SITE_CITY AS CITY, INSTR.SITE_COUNTRY AS COUNTRY,
	                            COALESCE(ADDINFOPATH.BREADCRUMB, ADDINFO.TITLE) AS ADDINFO, ADDINFOPATH.ROOTTITLE AS AREAADDINFO,
								PATH_REG.ROOTTITLE AS AREAREGISTRATION, COALESCE(PATH_REG.BREADCRUMB, CLASSIF_REG.TITLE) AS CLASSIFICATIONREGISTRATION,
								REQ.SERIALNUMBERREGISTRATION, REQ.OTHERPLATFORMREGISTRATION, REQ.CODARTREGISTRATION, 
                                INSTR_REG.SITE_CITY AS CITYREGISTRATION, INSTR_REG.SITE_COUNTRY AS COUNTYREGISTRATION,
								COALESCE(ADDINFOPATH_REG.BREADCRUMB, ADDINFO_REG.TITLE) AS ADDINFOREGISTRATION, ADDINFOPATH_REG.ROOTTITLE AS AREAADDINFOREGISTRATION,
								COALESCE(USER_REG.FULLNAME, REQ.USERREGISTRATION) AS USERREGISTRATION_NAME,
								DATEDIFF(MINUTE, REQ.TMSTINSE, REQ.TMSTINCHARGEOF) AS MINUTEINCHARGEOF,  
                                REQ.USERINCHARGEOF, REQ.TMSTINCHARGEOF, 
	                            (DATEDIFF(MINUTE, REQ.TMSTINCHARGEOF, REQ.TMSTCLOSED) - COALESCE(STANBYMINUTES, 0)) AS MINUTEPROCESSING,
								COALESCE(CLOSINGINFOPATH.BREADCRUMB, CLOSINGINFO.TITLE) AS CLOSINGINFO,
								COALESCE(USER_CLO.FULLNAME, REQ.USERCLOSED) AS USERCLOSED_NAME,
								REQ.TMSTCLOSED, REQ.CLOSINGNOTE, 
                                REQ.COMPLAINT, REQ.COMPLAINTLOTNUMBER, REQ.COMPLAINTCUSTOMER, REQ.COMPLAINTCITY,
                                REQ.COMPLAINTINCIDENT, REQ.COMPLAINTINCIDENTNOTE, 
                                REQ.COMPLAINTLINKOMMINFO, REQ.TKCOMPLAINTCLASSIFICATIONID,
                                REPLACE(COALESCE(CMPLCLASSIFPATH.BREADCRUMB, CMPLCLASSIF.TITLE), CONCAT(COALESCE(CMPLCLASSIFPATH.ROOTTITLE, ''), ' > '), '') AS COMPLAINTCLASSIFICATIONDECO,
                                REQ.COMPLAINTSTATUS, CMPLSTATUS.DESCRIPTION AS COMPLAINTSTATUSDECO
	                      FROM TKREQUESTS REQ
	                        INNER JOIN TKCLASSIFICATIONS  CLASSIF 
	                               ON REQ.TKCLASSIFICATIONID = CLASSIF.TKCLASSIFICATIONID
	                        LEFT JOIN TKCLASSIFICATIONPATHS PATH
	                               ON PATH.TKCLASSIFICATIONIDREF = CLASSIF.TKCLASSIFICATIONID 
	                               AND PATH.TKCLASSIFICATIONIDREF = PATH.TKCLASSIFICATIONID 
	                        LEFT JOIN TKCLASSIFICATIONS  CLASSIF_REG 
	                               ON REQ.TKCLASSIFICATIONIDREGISTRATION = CLASSIF_REG.TKCLASSIFICATIONID
	                        LEFT JOIN TKCLASSIFICATIONPATHS PATH_REG
	                               ON PATH_REG.TKCLASSIFICATIONIDREF = CLASSIF_REG.TKCLASSIFICATIONID 
	                               AND PATH_REG.TKCLASSIFICATIONIDREF = PATH_REG.TKCLASSIFICATIONID 
	                        LEFT JOIN INSTRUMENTS INSTR
	                               ON INSTR.SERIALNUMBER = REQ.SERIALNUMBER 
	                        LEFT JOIN INSTRUMENTS INSTR_REG
	                               ON INSTR_REG.SERIALNUMBER = REQ.SERIALNUMBERREGISTRATION 
	                        LEFT JOIN TKADDINFOS  ADDINFO
                                   ON REQ.TKADDINFOID = ADDINFO.TKADDINFOID
	                        LEFT JOIN TKADDINFOPATHS ADDINFOPATH
	                               ON ADDINFOPATH.TKADDINFOIDREF = REQ.TKADDINFOID 
                                   AND ADDINFOPATH.TKADDINFOIDREF = ADDINFOPATH.TKADDINFOID 
	                        LEFT JOIN TKADDINFOS  ADDINFO_REG
                                   ON REQ.TKADDINFOIDREGISTRATION = ADDINFO_REG.TKADDINFOID
	                        LEFT JOIN TKADDINFOPATHS ADDINFOPATH_REG
	                               ON ADDINFOPATH_REG.TKADDINFOIDREF = REQ.TKADDINFOIDREGISTRATION
                                   AND ADDINFOPATH_REG.TKADDINFOIDREF = ADDINFOPATH_REG.TKADDINFOID 
	                        LEFT JOIN TKCLOSINGINFOS  CLOSINGINFO
                                   ON REQ.TKCLOSINGINFOID = CLOSINGINFO.TKCLOSINGINFOID
	                        LEFT JOIN TKCLOSINGINFOPATHS CLOSINGINFOPATH
	                               ON CLOSINGINFOPATH.TKCLOSINGINFOIDREF = REQ.TKCLOSINGINFOID
                                   AND CLOSINGINFOPATH.TKCLOSINGINFOIDREF = CLOSINGINFOPATH.TKCLOSINGINFOIDREF
	                        LEFT JOIN TKADDINFOS  CMPLCLASSIF
                                   ON CMPLCLASSIF.TKADDINFOID = REQ.TKCOMPLAINTCLASSIFICATIONID
	                        LEFT JOIN TKADDINFOPATHS CMPLCLASSIFPATH
	                               ON CMPLCLASSIFPATH.TKADDINFOID = REQ.TKCOMPLAINTCLASSIFICATIONID 
                                   AND CMPLCLASSIFPATH.TKADDINFOIDREF = REQ.TKCOMPLAINTCLASSIFICATIONID 
                            LEFT JOIN CODES CMPLSTATUS ON CMPLSTATUS.CODEID = REQ.COMPLAINTSTATUS
                            INNER JOIN ASPNETUSERS USERCFG_REG ON REQ.USERREGISTRATION = USERCFG_REG.USERNAME
                            INNER JOIN USERCFG USER_REG ON USERCFG_REG.ID = USER_REG.USERID
                            LEFT JOIN ASPNETUSERS USERCFG_CLO ON REQ.USERCLOSED = USERCFG_CLO.USERNAME
                            LEFT JOIN USERCFG USER_CLO ON USERCFG_CLO.ID = USER_CLO.USERID
	                    WHERE REQ.STATUS NOT IN(@StateNotIn)
                          AND CONVERT(date, REQ.TMSTINSE) BETWEEN CONVERT(date, @DateFrom, 103) AND CONVERT(date, @DateTo, 103)
                          AND CLASSIF.CONFIGID = @ConfigId";
            args.Add(new { StateNotIn = new string[] { Lookup.TKRequest_Status_DRAFT, Lookup.TKRequest_Status_DELETED } });
            args.Add(new { DateFrom = DateFrom });
            args.Add(new { DateTo = DateTo });
            args.Add(new { ConfigId = ConfigId });

            if (ClassificationId > 0)
            {
                Sql += $@" AND EXISTS (SELECT 1 FROM TKCLASSIFICATIONPATHS CLASSIF_FILTERS 
                                        WHERE CLASSIF_FILTERS.TKCLASSIFICATIONidREF = @ClassifId AND CLASSIF_FILTERS.TKCLASSIFICATIONID = REQ.TKCLASSIFICATIONID) ";
                args.Add(new { ClassifId = ClassificationId });
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<StatisticsDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetStatistics.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco AddInfo
        public List<TKAddInfo> GetPublicAddInfos(bool bHideRoot = false, int excludeAddInfoId = 0, bool bExpandAll = false, int relativeRootId = 0, 
                                                 bool bLoadBreadCrumb = false, string configId = "", string groupId = "")
        {
            var ret = new List<TKAddInfo>();
            List<object> args = new List<object>();
            var SqlFields = @"SELECT ADDINFO.TKAddInfoId, ADDINFO.CODE, ADDINFO.Reserved, ADDINFO.ParentId, ADDINFO.Title, ADDINFO.Color";
            var SqlFrom = @"FROM TKAddInfoS ADDINFO";
            var sqlWhere = "WHERE DELETED = 0";
            var sqlGroupBy = "";
            var sqlOrderBy = " ORDER BY ADDINFO.TITLE, ADDINFO.TKAddInfoID";
            // Esclusione di tutti i sotto-elementi un nodo (es. per MOVE)
            if (excludeAddInfoId > 0)
            {
                sqlWhere += " AND ADDINFO.TKAddInfoId NOT IN (SELECT TKAddInfoId FROM TKAddInfoPATHS WHERE TKAddInfoIdRef = @AddInfoToExclude)";
                args.Add(new { AddInfoToExclude = excludeAddInfoId });
            }
            // Estrazione dei soli sotto-elementi di un nodo (che viene escluso)
            if (relativeRootId > 0)
            {
                sqlWhere += " AND ADDINFO.TKAddInfoId IN (SELECT TKAddInfoId FROM TKAddInfoPATHS WHERE TKAddInfoIdRef = @relativeRootId) " +
                    "         AND ADDINFO.TKAddInfoId <> @relativeRootId";
                args.Add(new { relativeRootId = relativeRootId });
            }
            if (!string.IsNullOrWhiteSpace(configId))
            {
                sqlWhere += " AND ADDINFO.ConfigId = @configId";
                args.Add(new { configId = configId });
            }
            if (!string.IsNullOrWhiteSpace(groupId))
            {
                sqlWhere += " AND ADDINFO.GroupId = @groupId";
                args.Add(new { groupId = groupId });
            }
            // Valorizzazione attributo BreadCrumb
            if (bLoadBreadCrumb)
            {
                SqlFields += ", COALESCE(PATH.BREADCRUMB, ADDINFO.TITLE) AS BREADCRUMB";
                SqlFrom += @" LEFT JOIN TKADDINFOPATHS PATH ON PATH.TKADDINFOIDREF = ADDINFO.TKADDINFOID AND PATH.TKADDINFOIDREF = PATH.TKADDINFOID";
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{SqlFields} {SqlFrom} {sqlWhere} {sqlGroupBy} {sqlOrderBy}";
                    ret = db.Query<TKAddInfo>(sql, args.ToArray()).ToList();
                    if (!bHideRoot)
                    {
                        string rootName = "Additional Informations";
                        if (!string.IsNullOrWhiteSpace(groupId))
                        {
                            if (groupId.Equals(Lookup.TKSupport_Config_GroupId_TODOLIST))
                            {
                                rootName = "ToDo List Templates";
                            }
                        }
                        // aggiungo un record fittizio come root (id = -1) 
                        ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                        ret.Add(new TKAddInfo() { TKAddInfoId = -1, Title = rootName, Expanded = true });
                    }
                    if (bExpandAll)
                    {
                        ret.ForEach(o => o.Expanded = true);
                    }
                    if (relativeRootId > 0)
                    {
                        // il nodo Root è stato estratto, quindi tutti i puntatori al Parent vengono impostati a NULL !! 
                        ret.ForEach(r => r.ParentId = (r.ParentId == relativeRootId ? null : r.ParentId));
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetPublicAddInfos.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco AddInfo
        public List<AddInfoExportDTO> GetAddInfosForExport(int TKAddInfoId)
        {
            var ret = new List<AddInfoExportDTO>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT ADDINFO.TKAddInfoID AS ID, ADDINFO.PARENTID, ADDINFO.CODE, ADDINFO.TITLE, 
                                COALESCE(PATH.BREADCRUMB, ADDINFO.TITLE) as BREADCRUMB, ADDINFO.RESERVED, ADDINFO.DELETED
                           FROM TKAddInfoS ADDINFO
                           LEFT JOIN TKAddInfoPATHS PATH
                              ON PATH.TKAddInfoID = ADDINFO.TKAddInfoID
                             AND PATH.TKAddInfoIDREF = ADDINFO.TKAddInfoID
						WHERE EXISTS (SELECT 1 FROM TKADDINFOPATHS 
								        WHERE TKADDINFOPATHS.TKADDINFOIDREF = @TKAddInfoId 
										AND TKADDINFOPATHS.TKADDINFOID = ADDINFO.TKADDINFOID  )
                        ORDER BY PATH.BREADCRUMB, ADDINFO.TITLE,  ADDINFO.TKAddInfoID";
            args.Add(new { TKAddInfoId = TKAddInfoId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<AddInfoExportDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetAddInfosForExport.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco ClosingInfo
        public List<TKClosingInfo> GetPublicClosingInfos(bool bHideRoot = false, int excludeClosingInfoId = 0, bool bExpandAll = false, int relativeRootId = 0,
                                                         bool bLoadBreadCrumb = false, string configId = "")
        {
            var ret = new List<TKClosingInfo>();
            List<object> args = new List<object>();
            var SqlFields = @"SELECT ClosingInfo.TKClosingInfoId, ClosingInfo.CODE, ClosingInfo.ParentId, ClosingInfo.Title";
            var SqlFrom = @"FROM TKClosingInfoS ClosingInfo";
            var sqlWhere = "WHERE DELETED = 0";
            var sqlGroupBy = "";
            var sqlOrderBy = " ORDER BY ClosingInfo.TITLE, ClosingInfo.TKClosingInfoID";
            // Esclusione di tutti i sotto-elementi un nodo (es. per MOVE)
            if (excludeClosingInfoId > 0)
            {
                sqlWhere += " AND ClosingInfo.TKClosingInfoId NOT IN (SELECT TKClosingInfoId FROM TKClosingInfoPATHS WHERE TKClosingInfoIdRef = @ClosingInfoToExclude)";
                args.Add(new { ClosingInfoToExclude = excludeClosingInfoId });
            }
            // Estrazione dei soli sotto-elementi di un nodo (che viene escluso)
            if (relativeRootId > 0)
            {
                sqlWhere += " AND ClosingInfo.TKClosingInfoId IN (SELECT TKClosingInfoId FROM TKClosingInfoPATHS WHERE TKClosingInfoIdRef = @relativeRootId) " +
                    "         AND ClosingInfo.TKClosingInfoId <> @relativeRootId";
                args.Add(new { relativeRootId = relativeRootId });
            }
            // Valorizzazione attributo BreadCrumb
            if (bLoadBreadCrumb)
            {
                SqlFields += ", COALESCE(PATH.BREADCRUMB, ClosingInfo.TITLE) AS BREADCRUMB";
                SqlFrom += @" LEFT JOIN TKClosingInfoPATHS PATH ON PATH.TKClosingInfoIDREF = ClosingInfo.TKClosingInfoID AND PATH.TKClosingInfoIDREF = PATH.TKClosingInfoID";
            }
            if (!string.IsNullOrWhiteSpace(configId))
            {
                sqlWhere += " AND ClosingInfo.ConfigId = @ConfigId";
                args.Add(new { ConfigId = configId });
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{SqlFields} {SqlFrom} {sqlWhere} {sqlGroupBy} {sqlOrderBy}";
                    ret = db.Query<TKClosingInfo>(sql, args.ToArray()).ToList();
                    if (!bHideRoot)
                    {
                        // aggiungo un record fittizio come root (id = -1) 
                        ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                        ret.Add(new TKClosingInfo() { TKClosingInfoId = -1, Title = "Closing Informations", Expanded = true });
                    }
                    if (bExpandAll)
                    {
                        ret.ForEach(o => o.Expanded = true);
                    }
                    if (relativeRootId > 0)
                    {
                        // il nodo Root è stato estratto, quindi tutti i puntatori al Parent vengono impostati a NULL !! 
                        ret.ForEach(r => r.ParentId = (r.ParentId == relativeRootId ? null : r.ParentId));
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetPublicClosingInfos.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco ClosingInfo
        public List<ClosingInfoExportDTO> GetClosingInfosForExport(int TKClosingInfoId)
        {
            var ret = new List<ClosingInfoExportDTO>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT ClosingInfo.TKClosingInfoID AS ID, ClosingInfo.PARENTID, ClosingInfo.CODE, ClosingInfo.TITLE, 
                                COALESCE(PATH.BREADCRUMB, ClosingInfo.TITLE) as BREADCRUMB, ClosingInfo.DELETED
                           FROM TKClosingInfoS ClosingInfo
                           LEFT JOIN TKClosingInfoPATHS PATH
                              ON PATH.TKClosingInfoID = ClosingInfo.TKClosingInfoID
                             AND PATH.TKClosingInfoIDREF = ClosingInfo.TKClosingInfoID
                        WHERE EXISTS (SELECT 1 FROM TKClosingInfoPATHS 
                                        WHERE TKClosingInfoPATHS.TKClosingInfoIDREF = @TKClosingInfoId 
                                        AND TKClosingInfoPATHS.TKClosingInfoID = ClosingInfo.TKClosingInfoID  )
                        ORDER BY PATH.BREADCRUMB, ClosingInfo.TITLE,  ClosingInfo.TKClosingInfoID";
            args.Add(new { TKClosingInfoId = TKClosingInfoId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<ClosingInfoExportDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetClosingInfosForExport.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        public List<Product> GetProducts(bool excludeOffLine = true)
        {
            var ret = new List<Product>();
            var Sql = @"SELECT CODART, DESCRIPTION, OFFLINE, INWARRANTY, INVESTIGATIONFORBREAKING, SPAREPART, LIMITEDWARRANTY
                            FROM PRODUCTS 
                        WHERE 1=1";
            if (excludeOffLine)
            {
                Sql += " AND OFFLINE = 0";
            }
            Sql += @" ORDER BY CODART";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Product>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("SupportBO.GetProducts() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // Verifica se la richiesta appartiene ad un'area che prevede la notifica ai SALES 
        public bool isNotifyToAreaManager(int TKRequestId)
        {
            int cnt = 0;
            List<object> args = new List<object>();
            var Sql = $@"SELECT COUNT(*) FROM TKREQUESTS 
                                INNER JOIN V_TKCLASSIFICATIONSTOSALES 
                                ON V_TKCLASSIFICATIONSTOSALES.TKCLASSIFICATIONID = TKREQUESTS.TKCLASSIFICATIONID
                                AND V_TKCLASSIFICATIONSTOSALES.TKCLASSIFICATIONIDREF = TKREQUESTS.TKCLASSIFICATIONID
                            WHERE TKREQUESTID = @RequestId";
            args.Add(new { RequestId = TKRequestId });
            try
            {
                using (IDatabase db = Connection)
                {
                    cnt = db.ExecuteScalar<int>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("isNotifyToAreaManager.get() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return (cnt > 0);
        }


        public string GetEmailAreaManagers(string userRegistration)
        {
            string ret = "";
            List<string> userCountries = (new UsersBO()).GetLinks(userRegistration, Lookup.UserLinkType.Country);
            if (userCountries.Count > 0)
            {
                List<UsersbyLinkId> sales = (new UsersBO()).getUsersByLinkId(userCountries.ToList(), Lookup.UserLinkType.AreaManager);
                string sepemail = "";
                foreach (var user in sales)
                {
                    ret += sepemail + user.Email;
                    sepemail = ", ";
                }
            }
            return ret;
        }

        public List<String> GetTemplateToDoList(string ConfigId, int TKRequestId)
        {
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT DISTINCT ITEM.TITLE FROM TKTODOITEMS ITEM
                            INNER JOIN TKREQUESTS REQ ON REQ.TKREQUESTID = ITEM.TKREQUESTID
                            INNER JOIN TKCLASSIFICATIONS CLASSIF ON CLASSIF.TKCLASSIFICATIONID = REQ.TKCLASSIFICATIONID
                            AND CLASSIF.CONFIGID = @ConfigId
                            AND NOT EXISTS (select 1 from TKToDoItems ITEM2 WHERE UPPER(ITEM2.TITLE) = UPPER(ITEM.TITLE) AND ITEM2.TKREQUESTID = @RequestId)
                            ORDER BY 1";
            args.Add(new { ConfigId = ConfigId });
            args.Add(new { RequestId = TKRequestId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<String>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("SupportBO.GetTemplateToDoList() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

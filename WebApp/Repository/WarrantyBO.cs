﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Authorization;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class WarrantyBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public WarrantyBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // Elenco delle richieste relative allo strumento passato 
        public List<WSPRequestDTO> GetInstrumentRequests4WSPIndex(string SerialNumber)
        {
            var ret = new List<WSPRequestDTO>();
            List<object> args = new List<object>();
            // per l'utente, le richieste in PROCESSING e IN_WARRANTY_NO_REPLACEMENT, oppure COMPLIANT = YES, sono da considerare chiuse !!!
            var Sql = @"SELECT REQ.WSPREQUESTID, REQ.SERIALNUMBER, REQ.CODE, REQ.TMSTISSUE, 
                               REQ.CODART, REQ.NUMBEROFPARTS, REQ.NOTES, REQ.DECONTAMINED, 
                               REQ.ORIGINALPART, REQ.POREFERENCE, WORIGINALPART.DESCRIPTION AS ORIGINALPARTDECO,
                               IIF(REQ.STATUS = @StatusProcessing AND 
                                            (COALESCE(REQ.WARRANTYSTATUS, '') = @WarrantyNoReturn OR REQ.COMPLIANT = @CompliantYES), 
                                   @StatusClosed, 
                                   REQ.STATUS)  as STATUS,
                               REQ.WARRANTYSTATUS, REQ.WARRANTYSTATUSNOTE, 
                               REQ.EXCHANGEREQUEST, REQ.EXCHANGEREQUESTNOTE, 
                               REQ.COMPLIANT, REQ.COMPLIANTNOTE, 
                               REQ.WSPACTION, REQ.WSPACTIONNOTE, 
                               REQ.CARRIERNOTE, REQ.SHIPPINGNOTE, 
                               REQ.INVOICENUMBER, REQ.INVOICEDATE, 
                               REQ.USERINSE, REQ.TMSTINSE, REQ.USERLASTUPD, REQ.TMSTLASTUPD, 
                               CONCAT(REQ.CODART, ' ' , PROD.DESCRIPTION) AS CODARTDECO, 
                               WSTATUS.DESCRIPTION AS WARRANTYSTATUSDECO,
                               COALESCE(USER_INSE.FULLNAME, REQ.USERINSE) AS USERINSE_NAME, 
                               INSTR.MODELTYPE
                        FROM WSPREQUESTS REQ
                        LEFT JOIN PRODUCTS PROD ON PROD.CODART = REQ.CODART
                        LEFT JOIN CODES WSTATUS ON WSTATUS.CODEID = REQ.WARRANTYSTATUS
                        LEFT JOIN CODES WORIGINALPART ON WORIGINALPART.CODEID = REQ.ORIGINALPART
                        LEFT JOIN ASPNETUSERS USERCFG_INSE ON REQ.USERINSE = USERCFG_INSE.USERNAME
                        LEFT JOIN USERCFG USER_INSE ON USERCFG_INSE.ID = USER_INSE.USERID 
                       LEFT JOIN INSTRUMENTS  INSTR ON INSTR.SERIALNUMBER = REQ.SERIALNUMBER
                      WHERE REQ.SerialNumber = @Instrument
                       AND  REQ.STATUS NOT IN (@StatiNotIn)
                      ORDER BY REQ.CODE DESC";
            args.Add(new { Instrument = SerialNumber });
            args.Add(new { StatusProcessing = Lookup.WSPRequest_Status_PROCESSING });
            args.Add(new { WarrantyNoReturn = Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT });
            args.Add(new { StatusClosed = Lookup.WSPRequest_Status_CLOSED });
            args.Add(new { CompliantYES = Lookup.Code_WSP_Compliant_YES });

            String[] arr = new string[] { Lookup.WSPRequest_Status_DELETED, Lookup.WSPRequest_Status_DRAFT };
            args.Add(new { StatiNotIn = arr });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<WSPRequestDTO>(Sql, args.ToArray()).ToList();
                    //foreach (var item in ret)
                    //{
                    //    if (item.Status.Equals(Lookup.WSPRequest_Status_PROCESSING))
                    //    {
                    //        if (!String.IsNullOrWhiteSpace(item.Compliant) && item.Compliant.Equals(Lookup.Code_WSP_Compliant_YES))
                    //            item.Status = Lookup.WSPRequest_Status_CLOSED;
                    //    }
                    //}
                }
                return ret;
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.GetInstrumentRequests4WSPIndex() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // Elenco Richieste per gestione 
        public List<WSPRequestDTO> getRequests(string WarrantyStatus = "", string ExchangeRequest = "", string Compliant = "", bool ShowClosed = false, 
                                               string WSPAction = "", int WSPRequestId = 0)
        {
            var ret = new List<WSPRequestDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT  REQ.WSPREQUESTID, REQ.STATUS, REQ.CODE, REQ.TMSTISSUE, 
			                    REQ.CODART, REQ.NUMBEROFPARTS, REQ.NOTES, REQ.DECONTAMINED, 
                                REQ.ORIGINALPART, REQ.POREFERENCE, WORIGINALPART.DESCRIPTION AS ORIGINALPARTDECO,
                    			REQ.SERIALNUMBER, INSTR.COMMERCIALENTITY, INSTR.SITE_COUNTRY AS SITECOUNTRY,
			                    INSTR.COMPANYNAME, INSTR.SITE_CITY AS SITECITY, INSTR.SITE_DESCRIPTION AS SITEDESCRIPTION, 
			                    INSTR.WARRANTY_FROM, INSTR.WARRANTY_TO,
                                REQ.WARRANTYSTATUS, WSRTDECO.DESCRIPTION AS WARRANTYSTATUSDECO, REQ.WARRANTYSTATUSNOTE,
			                    REQ.EXCHANGEREQUEST, EXCGDECO.DESCRIPTION AS EXCHANGEREQUESTDECO, REQ.EXCHANGEREQUESTNOTE, 
			                    REQ.CARRIERNOTE, REQ.SHIPPINGNOTE, 
			                    REQ.COMPLIANT, COMPLDECO.DESCRIPTION AS COMPLIANTDECO, REQ.COMPLIANTNOTE, 
			                    REQ.WSPACTION, ACTDECO.DESCRIPTION AS WSPACTIONDECO, REQ.WSPACTIONNOTE, 
			                    REQ.INVOICENUMBER, REQ.INVOICEDATE, 
			                    REQ.USERINSE, REQ.TMSTINSE, REQ.USERLASTUPD, REQ.TMSTLASTUPD, 
			                    CONCAT(REQ.CODART, ' ' , PROD.DESCRIPTION) AS CODARTDECO, 
			                    COALESCE(USER_INSE.FULLNAME, REQ.USERINSE) AS USERINSE_NAME, 
                                INSTR.MODELTYPE
	                    FROM WSPREQUESTS REQ
	                    LEFT JOIN PRODUCTS PROD ON PROD.CODART = REQ.CODART
	                    LEFT JOIN CODES WSRTDECO ON WSRTDECO.CODEID = REQ.WARRANTYSTATUS
	                    LEFT JOIN CODES EXCGDECO ON EXCGDECO.CODEID = REQ.EXCHANGEREQUEST
	                    LEFT JOIN CODES COMPLDECO ON COMPLDECO.CODEID = REQ.COMPLIANT
	                    LEFT JOIN CODES ACTDECO ON ACTDECO.CODEID = REQ.WSPACTION
                        LEFT JOIN CODES WORIGINALPART ON WORIGINALPART.CODEID = REQ.ORIGINALPART
	                    LEFT JOIN ASPNETUSERS USERCFG_INSE ON REQ.USERINSE = USERCFG_INSE.USERNAME
                        LEFT JOIN USERCFG USER_INSE ON USERCFG_INSE.ID = USER_INSE.USERID
                        LEFT JOIN INSTRUMENTS  INSTR ON INSTR.SERIALNUMBER = REQ.SERIALNUMBER
                      WHERE 1 = 1";
            if (WSPRequestId > 0)
            {
                Sql += @" AND REQ.WSPREQUESTID = @WspRequestId";
                args.Add(new { WspRequestId = WSPRequestId });
            }
            else
            {
                Sql += @" AND REQ.STATUS NOT IN (@StatiNotIn)";
                String[] arr = arr = new string[] { Lookup.WSPRequest_Status_DELETED, Lookup.WSPRequest_Status_DRAFT, Lookup.WSPRequest_Status_CLOSED };
                if (ShowClosed)
                {
                    arr = arr.Where((source, index) => !source.Equals(Lookup.WSPRequest_Status_CLOSED)).ToArray();
                }
                args.Add(new { StatiNotIn = arr });

                // FILTRO -> WarrantyStatus: CODICI + <Any> + <None>
                if (!string.IsNullOrWhiteSpace(WarrantyStatus))
                {
                    if (WarrantyStatus.Equals(Lookup.Code_WSP_ANY_id))
                    {
                        Sql += @" AND REQ.WARRANTYSTATUS IS NOT NULL";
                    }
                    else if (WarrantyStatus.Equals(Lookup.Code_WSP_NONE_id))
                    {
                        Sql += @" AND REQ.WARRANTYSTATUS IS NULL";
                    }
                    else
                    {
                        Sql += @" AND REQ.WARRANTYSTATUS = @WarrantyStatus";
                        args.Add(new { WarrantyStatus = WarrantyStatus });
                    }
                }
                // FILTRO -> ExchangeRequest: CODICI + <Any> + <None>
                if (!string.IsNullOrWhiteSpace(ExchangeRequest))
                {
                    if (ExchangeRequest.Equals(Lookup.Code_WSP_ANY_id))
                    {
                        Sql += @" AND REQ.EXCHANGEREQUEST IS NOT NULL";
                    }
                    else if (ExchangeRequest.Equals(Lookup.Code_WSP_NONE_id))
                    {
                        Sql += @" AND REQ.EXCHANGEREQUEST IS NULL";
                    }
                    else
                    {
                        Sql += @" AND REQ.EXCHANGEREQUEST = @ExchangeRequest";
                        args.Add(new { ExchangeRequest = ExchangeRequest });
                    }
                }
                // FILTRO -> Compliant: CODICI + <Any> + <None>
                if (!string.IsNullOrWhiteSpace(Compliant))
                {
                    Sql += @" AND REQ.COMPLIANT = @Compliant";
                    args.Add(new { Compliant = Compliant });
                }
                // FILTRO -> WSPACTION: CODICI + <Any> + <None>
                if (!string.IsNullOrWhiteSpace(WSPAction))
                {
                    if (WSPAction.Equals(Lookup.Code_WSP_ANY_id))
                    {
                        Sql += @" AND REQ.WSPACTION IS NOT NULL";
                    }
                    else if (WSPAction.Equals(Lookup.Code_WSP_NONE_id))
                    {
                        Sql += @" AND REQ.WSPACTION IS NULL";
                    }
                    else
                    {
                        Sql += @" AND REQ.WSPACTION = @WSPAction";
                        args.Add(new { WSPAction = WSPAction });
                    }
                }
            }

            Sql += " ORDER BY REQ.CODE DESC";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<WSPRequestDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.getRequests() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // Elenco Richieste per gestione 
        public List<WSPStepDTO> GetRequestSteps(int WSPRequestId)
        {
            var ret = new List<WSPStepDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT * FROM (
                        SELECT 1 AS STEP, 'Warranty Status' AS DESCRIPTION, REQ.WARRANTYSTATUSNOTE AS NOTE, REQ.WARRANTYSTATUS AS STATUS, CODES.DESCRIPTION AS STATUSDECO
	                        FROM WSPREQUESTS REQ
	                        LEFT JOIN CODES ON CODEID = REQ.WARRANTYSTATUS
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId
                        UNION
                        SELECT 2, 'Exchange Request', REQ.EXCHANGEREQUESTNOTE, REQ.EXCHANGEREQUEST, CODES.DESCRIPTION
	                        FROM WSPREQUESTS REQ
	                        LEFT JOIN CODES ON CODEID = REQ.EXCHANGEREQUEST
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        UNION
                        SELECT 3, 'Carrier Note', REQ.CARRIERNOTE, NULL, NULL
	                        FROM WSPREQUESTS REQ
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        UNION
                        SELECT 4, 'Shipping Note', REQ.SHIPPINGNOTE, NULL, NULL
	                        FROM WSPREQUESTS REQ
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        UNION
                        SELECT 5, 'Defective part management', REQ.WSPACTIONNOTE, REQ.WSPACTION, CODES.DESCRIPTION
	                        FROM WSPREQUESTS REQ
	                        LEFT JOIN CODES ON CODEID = REQ.WSPACTION
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        UNION
                        SELECT 6, 'Shipment of the new part from supplier', REQ.COMPLIANTNOTE, REQ.COMPLIANT, CODES.DESCRIPTION
	                        FROM WSPREQUESTS REQ
	                        LEFT JOIN CODES ON CODEID = REQ.COMPLIANT
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        UNION
                        SELECT 7, 'Invoice Information', NULL, NULL, CONCAT(REQ.InvoiceNUmber, ' ' , FORMAT(REQ.InvoiceDate, 'dd/MM/yyyy', 'en-US'))
	                        FROM WSPREQUESTS REQ
	                        WHERE REQ.WSPREQUESTID = @WSPRequestId 
                        ) TMP 
                        --WHERE STATUS IS NOT NULL OR NOTE IS NOT NULL
                        ORDER BY STEP";
            //var Sql = @"SELECT * FROM (
            //            SELECT 1 AS STEP, 'Set Warranty Status' AS DESCRIPTION, V_WSPEVENTSLAST.NOTE AS NOTE, V_WSPEVENTSLAST.TMSTINSE,  V_WSPEVENTSLAST.FULLNAME,
            //                   REQ.WARRANTYSTATUS AS STATUS, CODES.DESCRIPTION AS STATUSDECO
	           //             FROM WSPREQUESTS REQ
	           //             LEFT JOIN CODES ON CODEID = REQ.WARRANTYSTATUS
	           //             LEFT JOIN V_WSPEVENTSLAST
		          //              ON V_WSPEVENTSLAST.WSPREQUESTID = REQ.WSPREQUESTID AND V_WSPEVENTSLAST.OPERATION = @SetWarrantyStatus
	           //             WHERE REQ.WSPREQUESTID = @WSPRequestId
            //            UNION
            //            SELECT 2, 'Set Exchange Request', V_WSPEVENTSLAST.NOTE, V_WSPEVENTSLAST.TMSTINSE,  V_WSPEVENTSLAST.FULLNAME,
            //                   REQ.EXCHANGEREQUEST, CODES.DESCRIPTION
	           //             FROM WSPREQUESTS REQ
	           //             LEFT JOIN CODES ON CODEID = REQ.EXCHANGEREQUEST
	           //             LEFT JOIN V_WSPEVENTSLAST
		          //              ON V_WSPEVENTSLAST.WSPREQUESTID = REQ.WSPREQUESTID AND V_WSPEVENTSLAST.OPERATION = @SetExchangeRequest
	           //             WHERE REQ.WSPREQUESTID = @WSPRequestId 
            //            UNION
            //            SELECT 3, 'Set Compliance', V_WSPEVENTSLAST.NOTE, V_WSPEVENTSLAST.TMSTINSE,  V_WSPEVENTSLAST.FULLNAME,
            //                   REQ.COMPLIANT, CODES.DESCRIPTION
	           //             FROM WSPREQUESTS REQ
	           //             LEFT JOIN CODES ON CODEID = REQ.COMPLIANT
	           //             LEFT JOIN V_WSPEVENTSLAST 
		          //              ON V_WSPEVENTSLAST.WSPREQUESTID = REQ.WSPREQUESTID AND V_WSPEVENTSLAST.OPERATION = @SetCompliance
	           //             WHERE REQ.WSPREQUESTID = @WSPRequestId 
            //            UNION
            //            SELECT 4, 'Set Invoice Information', V_WSPEVENTSLAST.NOTE, V_WSPEVENTSLAST.TMSTINSE,  V_WSPEVENTSLAST.FULLNAME,
            //                   NULL, NULL
	           //             FROM WSPREQUESTS REQ
	           //             LEFT JOIN V_WSPEVENTSLAST 
		          //              ON V_WSPEVENTSLAST.WSPREQUESTID = REQ.WSPREQUESTID AND V_WSPEVENTSLAST.OPERATION = @SetInvoiceInfo
	           //             WHERE REQ.WSPREQUESTID = @WSPRequestId 
            //            ) TMP 
            //            --WHERE STATUS IS NOT NULL OR NOTE IS NOT NULL
            //            ORDER BY STEP";
            args.Add(new { WSPRequestId = WSPRequestId });
            //args.Add(new { SetWarrantyStatus = Lookup.WSPEvent_Operation_SetWarrantyStatus });
            //args.Add(new { SetExchangeRequest = Lookup.WSPEvent_Operation_SetExchangeRequest });
            //args.Add(new { SetCompliance = Lookup.WSPEvent_Operation_SetCompliance });
            //args.Add(new { SetInvoiceInfo = Lookup.WSPEvent_Operation_SetInvoiceInfo });
            //args.Add(new { SetAction = Lookup.WSPEvent_Operation_SetAction });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<WSPStepDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.GetRequestSteps() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public List<Product> GetSpareParts(bool excludeOffLine = true)
        {
            var ret = new List<Product>();
            var Sql = @"SELECT CODART, DESCRIPTION, OFFLINE, INWARRANTY, INVESTIGATIONFORBREAKING, SPAREPART, LIMITEDWARRANTY
                            FROM PRODUCTS 
                            WHERE SPAREPART = 1";
            if (excludeOffLine)
            {
                Sql += " AND OFFLINE = 0";
            }
            Sql +=@" ORDER BY CODART";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Product>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.GetSpareParts() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public Product GetSparePart(string CodArt)
        {
            Product ret = new Product();
            if (string.IsNullOrWhiteSpace(CodArt)) { return ret; }
            List<object> args = new List<object>();
            var Sql = @"SELECT CODART, DESCRIPTION, OFFLINE, INWARRANTY, INVESTIGATIONFORBREAKING, SPAREPART, LIMITEDWARRANTY
                            FROM PRODUCTS 
                            WHERE CODART = @CodArt";
            args.Add(new { CodArt = CodArt });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Product>(Sql, args.ToArray()).ToList().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.GetSparePart() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public List<WSPAttachment> GetRequestAttachments(int requestId)
        {
            var ret = new List<WSPAttachment>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT WSPAttachmentS.*, COALESCE(USERCFG.FULLNAME, WSPAttachmentS.USERINSE) AS USERINSENAME
                           FROM WSPAttachmentS  
                           INNER JOIN  ASPNETUSERS ON WSPAttachmentS.USERINSE = ASPNETUSERS.USERNAME
                           INNER JOIN  USERCFG ON ASPNETUSERS.ID = USERCFG.USERID
                           WHERE WSPREQUESTID = @requestId AND WSPAttachmentS.DELETED = 0";
            args.Add(new { requestId = requestId });
            Sql += " ORDER BY WSPAttachmentS.WSPAttachmentID";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<WSPAttachment>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequestAttachments.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public void UpdateSparePart(Product data, String User)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE PRODUCTS SET DESCRIPTION = @Description, 
                                            OFFLINE = @OffLine, 
                                            INWARRANTY = @InWarranty, 
                                            INVESTIGATIONFORBREAKING = @InvestigationForBreaking, 
                                            LIMITEDWARRANTY = @LimitedWarranty,
                                            TMSTLASTUPD = @TmstLastUpd,
                                            USERLASTUPD = @UserLastUpd
                                WHERE CODART = @CodArt";
            args.Add(new { CodArt = data.CodArt });
            args.Add(new { Description = data.Description });
            args.Add(new { OffLine = data.OffLine });
            args.Add(new { InWarranty = data.InWarranty });
            args.Add(new { InvestigationForBreaking = data.InvestigationForBreaking});
            args.Add(new { LimitedWarranty = data.LimitedWarranty });
            args.Add(new { TmstLastUpd = DateTime.Now });
            args.Add(new { UserLastUpd = User });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("WarrantyBO.UpdateSparePart() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }
    }
}


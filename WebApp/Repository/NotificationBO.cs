﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
//using NLog;
using WebApp.Classes;
using WebApp.Data;
using WebApp.Services;
using WebApp.Entity;
using WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace WebApp.Repository
{
    public class NotificationBO
    {
        //private static Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ILogger _logger;
        private readonly WebAppDbContext _context;
        private readonly IEmailSender _emailSender;

        public NotificationBO(WebAppDbContext context, IEmailSender emailSender, ILogger logger)
        {
            _emailSender = emailSender;
            _context = context;
            _logger = logger;

        }

        // ***********************************************************
        // INVIO EMAIL PER SUPPORTO / TICKETING
        // ***********************************************************
        public string SendNotificationTK(int TKRequestId, Lookup.Request_NotificationType NotificationType, string userName, string note = "",
                                         string title = "", string extTo = "", string extCC = "", bool bTrack = false, string extReplyTo = "")
        {
            string msgErr = "";
            string templateId = "";
            string body = "";
            //string subject = "";
            string to = "";
            string cc = "";
            string bcc = "";
            string replyTo = "";
            EmailTemplate email = new EmailTemplate();

            TKRequest request = _context.TKRequests.Find(TKRequestId);
            if (request == null)
            {
                msgErr = string.Format("TKRequest {0} not found", TKRequestId);
                _logger.LogError(msgErr);
                return msgErr;
            }
            TKClassification classification = _context.TKClassifications.Find(request.TKClassificationId);
            TKSupportConfigModel configModel = Lookup.GetTKSupportConfig(classification.ConfigId);

            List<string> roles = new List<string>();
            roles.Add(Lookup.Role_SupportAdministrator);
            if (configModel.HasComplaint)
            {
                roles.Add(Lookup.Role_CustomerComplaintAnalyst);
            }
            string emailListSupportAndCCA = (new UsersBO()).GetEmailsInRole(roles: roles.ToArray(), ConfigId: classification.ConfigId);
            string emailListSupportCCA = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_CustomerComplaintAnalyst}, ConfigId: classification.ConfigId);

            string emailUserRegistration = (new UsersBO()).GetEmail(request.UserRegistration);
            string emailUserInChargeOf = "";
            if (!string.IsNullOrWhiteSpace(request.UserInChargeOf))
            {
                emailUserInChargeOf = (new UsersBO()).GetEmail(request.UserInChargeOf);
            }
            string emailAreaManagers = "";
            if ((new SupportBO()).isNotifyToAreaManager(TKRequestId))
            {
                // Country configurate
                emailAreaManagers = (new SupportBO()).GetEmailAreaManagers(request.UserRegistration);
                if (string.IsNullOrWhiteSpace(emailAreaManagers) )
                {
                    msgErr = string.Format("No Country for user {0}", request.UserRegistration);
                    _logger.LogWarning(msgErr);
                    //return msgErr;
                }
            }

            switch (NotificationType)
            {
                case Lookup.Request_NotificationType.NewRequest:
                    templateId = Lookup.Support_Email_NewRequest;
                    to = emailListSupportAndCCA;
                    cc = concatEmails(new[] { request.EmailsKeepInformed, emailAreaManagers });
                    break;
                case Lookup.Request_NotificationType.CloseRequest:
                    templateId = Lookup.Support_Email_CloseRequest;
                    to = emailUserRegistration;
                    cc = concatEmails(new[] { request.EmailsKeepInformed, emailAreaManagers });
                    break;
                case Lookup.Request_NotificationType.AssignRequest:
                    templateId = Lookup.Support_Email_AssignRequest;
                    to = emailUserInChargeOf;
                    break;
                case Lookup.Request_NotificationType.AddReservedNote:
                    if (!request.UserInChargeOf.Equals(userName))
                    {
                        templateId = Lookup.Support_Email_NoteToSupport;
                        to = emailUserInChargeOf;
                    }
                    break;
                case Lookup.Request_NotificationType.AddPublicNote:
                    templateId = Lookup.Support_Email_NoteToReporter;
                    to = emailUserRegistration;
                    cc = request.EmailsKeepInformed;
                    break;
                case Lookup.Request_NotificationType.AddPublicNoteCCAreaManager:
                    // ???? DA RIVEDERE ????
                    templateId = Lookup.Support_Email_NoteToReporter;
                    to = emailUserRegistration;
                    cc = concatEmails(new[] { request.EmailsKeepInformed, emailAreaManagers });
                    break;
                case Lookup.Request_NotificationType.AddReporterNote:
                    templateId = Lookup.Support_Email_NoteToSupport;
                    to = emailUserInChargeOf;
                    cc = request.EmailsKeepInformed;
                    break;
                case Lookup.Request_NotificationType.ChangeAttach:
                    templateId = Lookup.Support_Email_NoteToSupport;
                    to = emailUserInChargeOf;
                    //cc = request.EmailsKeepInformed;
                    break;
                case Lookup.Request_NotificationType.TakingCharge:
                    templateId = Lookup.Support_Email_TakingCharge;
                    to = emailUserRegistration;
                    cc = request.EmailsKeepInformed;
                    break;
                case Lookup.Request_NotificationType.ReopenRequest:
                    templateId = Lookup.Support_Email_ReopenRequest;
                    to = emailListSupportAndCCA;
                    cc = concatEmails(new[] { request.EmailsKeepInformed, emailAreaManagers });
                    break;
                case Lookup.Request_NotificationType.AreaManager:
                    templateId = "NON_PRESENTE";
                    to = emailAreaManagers;
                    break;
                case Lookup.Request_NotificationType.ComplaintReject:
                    templateId = Lookup.Support_Email_ComplaintReject;
                    to = emailUserRegistration;
                    cc = emailUserInChargeOf;
                    break;
                case Lookup.Request_NotificationType.ComplaintForwardToCCA:
                    templateId = Lookup.Support_Email_ComplaintForwardToCCA;
                    to = emailListSupportCCA;
                    break;
                case Lookup.Request_NotificationType.ComplaintRequestData:
                    templateId = Lookup.Support_Email_ComplaintRequestData;
                    to = emailUserInChargeOf;
                    break;
                case Lookup.Request_NotificationType.EngagementSupplier:
                    to = !string.IsNullOrWhiteSpace(extTo) ? extTo : to;
                    cc = !string.IsNullOrWhiteSpace(extCC) ? extCC : cc;
                    replyTo = !string.IsNullOrWhiteSpace(extReplyTo) ? extReplyTo : replyTo;
                    email.Subject = title;
                    email.Body = note;
                    break;
                default: return "NotificationType" + NotificationType + " undefined";
            }

            if (!string.IsNullOrWhiteSpace(templateId))
            {
                email = GetEmailInfoTK(TKRequestId: TKRequestId, templateId: templateId, userName: userName, note: note, title: title);
            }

            if (string.IsNullOrWhiteSpace(to))
            {
                return "No recipient addresses found";
            }

            SendNotification(label: "SendNotificationTK/SUPPORT", 
                             templateId: templateId, 
                             requestID: TKRequestId.ToString(), 
                             to: to, 
                             cc: cc, 
                             bcc: bcc, 
                             replyTo: replyTo,
                             subject: email.Subject,
                             body: email.Body, 
                             context: "SUPPORT");

            if (bTrack)
            {
                TKEvent evento = new TKEvent()
                {
                    Note = string.Format("Send Email '{0}' to {1}", email.Subject, to),
                    TKRequestId = request.TKRequestId,
                    Operation = Lookup.TKEvent_Operation_SENDEMAIL,
                    TmstInse = DateTime.Now,
                    UserInse = userName,
                    Reserved = true
                };
                _context.TKEvents.Add(evento);
                _context.SaveChanges();
            }

            return "";
        }

        private string concatEmails(string[] email)
        {
            string ret = "";
            string sep = ""; 
            foreach(var item in email) {
                if (!string.IsNullOrWhiteSpace(item))
                {
                    ret += sep + item;
                    sep = ",";
                }
            }
            return ret;
        }

        // ***********************************************************
        // INVIO EMAIL PER SUPPORTO / TICKETING AP3P
        // ***********************************************************
        public void SendNotificationTK3P(int TK3PRequestId, Lookup.Request_NotificationType NotificationType, string userName, string note = "", string title = "")
        {
            string templateId = "";
            string body = "";
            string subject = "";
            string to = "";
            string cc = "";
            string bcc = "";

            string emailList_Support = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_OpenAP3P_SupportAdministrator});
            string emailList_ReD = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_OpenAP3P_ResearchAndDevelopment });
            string emailList_QA = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_OpenAP3P_Quality });
            string emailList_OD_QA_Support = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_OpenAP3P_Quality, Lookup.Role_OpenAP3P_SupportAdministrator, Lookup.Role_OpenAP3P_OrderDepartment });
            string emailList_OD = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_OpenAP3P_OrderDepartment });

            TK3PRequest request = _context.TK3PRequests.Find(TK3PRequestId);
            if (request == null)
            {
                _logger.LogError(string.Format("TKRequest {0} not found", TK3PRequestId));
                return;
            }

            string emailUserRegistration = (new UsersBO()).GetEmail(request.UserInse);

            switch (NotificationType)
            {
                case Lookup.Request_NotificationType.NewRequestAP3P:
                    templateId = Lookup.Support_Email_NewRequestAP3P;
                    to = emailList_Support;
                    bcc = emailList_OD;
                    break;
                case Lookup.Request_NotificationType.RefuseRequestAP3P:
                    templateId = Lookup.Support_Email_RefuseRequestAP3P;
                    to = emailUserRegistration;
                    break;
                case Lookup.Request_NotificationType.ApproveRequestAP3P:
                    templateId = Lookup.Support_Email_ApproveRequestAP3P;
                    to = emailUserRegistration;
                    bcc = emailList_ReD;
                    break;
                case Lookup.Request_NotificationType.ReadyRequestAP3P:
                    templateId = Lookup.Support_Email_ReadyRequestAP3P;
                    to = emailList_QA;
                    break;
                case Lookup.Request_NotificationType.ReleasedRequestAP3P:
                    templateId = Lookup.Support_Email_ReleasedRequestAP3P;
                    to = emailUserRegistration;
                    bcc = emailList_OD_QA_Support;
                    break;
                case Lookup.Request_NotificationType.DownloadRequestAP3P:
                    templateId = Lookup.Support_Email_DownloadRequestAP3P;
                    to = emailList_Support;
                    break;
                default: return;
            }

            EmailTemplate email = _context.EmailTemplates.Find(templateId);
            if (email == null)
            {
                subject = title ?? "Subject Not Defined";
                body = note ?? "Body Not Defined";
            }
            else
            {
                subject = email.Subject;
                body = email.Body;
            }

            Dictionary<string, string> valori = new Dictionary<string, string>();
            valori.Add("$CODE$", request.Code);
            valori.Add("$REQUESTDATE$", request.TmstInseDeco);
            valori.Add("$USERREGISTRATION$", (new UsersBO()).GetFullName(request.UserInse));
            valori.Add("$USER$", (new UsersBO()).GetFullName(userName));

            string swVersion = "";
            if (!string.IsNullOrWhiteSpace(request.SwVersion))
            {
                V_InGeniusSWVersion version = _context.V_InGeniusSWVersions.Where(t => t.Cod_Ele.Equals(request.SwVersion)).FirstOrDefault();
                if (version != null)
                {
                    swVersion = version.Descriz;
                }
            }
            valori.Add("$SWVERSION$", swVersion);
            valori.Add("$CUSTOMERSELECTION$", request.CustomerSelection);
            valori.Add("$TARGET$", request.Target);
            valori.Add("$MANUFACTURER$", request.Manufacturer);
            valori.Add("$PURCHASEORDER$", request.PurchaseOrder);
            subject = SostituisciPlaceholder(testo: subject, valori: valori);
            body = SostituisciPlaceholder(testo: body, valori: valori);

            SendNotification(label: "SendNotificationTK/SUPPORTAP3P",
                             templateId: templateId,
                             requestID: TK3PRequestId.ToString(),
                             to: to,
                             cc: cc,
                             bcc: bcc,
                             subject: subject,
                             body: body,
                             context: "SUPPORTAP3P");
            return;
        }



        // ***********************************************************
        // INVIO EMAIL PER REFURBISH 
        // ***********************************************************
        public void SendNotificationRefurbish(int RefurbishRequestId, Lookup.Request_NotificationType NotificationType, string userName,
                                        string note = "", string title = "", string extTo = "", string extCC = "")
        {
            string templateId = "";
            string to = "";
            string cc = "";
            string bcc = "";
            EmailTemplate email = new EmailTemplate();

            RefurbishRequest request = _context.RefurbishRequests.Find(RefurbishRequestId);
            if (request == null)
            {
                _logger.LogError(string.Format("Refurbish Request {0} not found", RefurbishRequestId));
                return;
            }

            switch (NotificationType)
            {
                case Lookup.Request_NotificationType.RefurbishCreate:
                    templateId = Lookup.Refurbish_Email_NewRequest;
                    to = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_RefurbishAdministrator });
                    break;
                case Lookup.Request_NotificationType.RefurbishReadyToShipped:
                    templateId = Lookup.Refurbish_Email_ReadyToBeShipped;
                    to = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_RefurbishAdministrator, Lookup.Role_RefurbishFinance});
                    break;
                default: return;
            }
            if (!string.IsNullOrWhiteSpace(templateId))
            {
                email = GetEmailInfoRefurbish(RefurbishRequestId: RefurbishRequestId, templateId: templateId, userName: userName, note: note, title: title);
            }
            to = !string.IsNullOrWhiteSpace(to) ? to : extTo;
            cc = !string.IsNullOrWhiteSpace(cc) ? cc : extCC;

            SendNotification(label: "SendNotificationRefurbish",
                             templateId: templateId,
                             requestID: RefurbishRequestId.ToString(),
                             to: to,
                             cc: cc,
                             bcc: bcc,
                             subject: email.Subject,
                             body: email.Body,
                             context: "REFURBISH");
            return;
        }
        public EmailTemplate GetEmailInfoRefurbish(int RefurbishRequestId, string templateId, string userName, string note = "", string title = "")
        {
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };

            RefurbishRequestDTO data = ((new InstrumentsBO()).GetRefurbishRequests(refurbishRequestId: RefurbishRequestId)).FirstOrDefault();

            EmailTemplate email = _context.EmailTemplates.Find(templateId);
            if (email == null)
            {
                retEmail.Subject = title;
                retEmail.Body = note;
            }
            else
            {
                retEmail.Subject = email.Subject;
                retEmail.Body = email.Body;
            }
            Dictionary<string, string> valori = new Dictionary<string, string>();
            valori.Add("$REQUESTID$", data.RefurbishRequestId.ToString());
            valori.Add("$STATUS$", data.StatusDeco);
            valori.Add("$SERIALNUMBER$", data.SerialNumber);
            valori.Add("$COMMERCIALENTITY$", data.CommercialEntity);
            valori.Add("$SITECOUNTRY$", data.SiteCountry);
            valori.Add("$CUSTOMER$", data.CompanyName);
            valori.Add("$CITY$", data.SiteCity);
            valori.Add("$SITE$", data.SiteDescription);
            valori.Add("$ASSETNUMBER$", data.AssetNumber);
            valori.Add("$RESIDUALVALUE$", data.ResidualValue.ToString());
            valori.Add("$RESIDUALVALUEDATE$", data.ResidualValueDateDeco);
            valori.Add("$FINALDESTINATION$", data.FinalDestination);
            valori.Add("$NEEDEDBYDATE$", data.NeededByDateDeco);
            valori.Add("$ADDITIONALOPERATIONS$", data.AdditionalOperations);
            valori.Add("$NOTE$", data.Note);
            valori.Add("$RDANUMBER$", data.RDANumber);
            valori.Add("$ENTRYDATE$", data.EntryDateDeco);
            valori.Add("$PROCESSINGSTARTDATE$", data.ProcessingStartDateDeco);
            valori.Add("$PROCESSINGENDDATE$", data.ProcessingEndDateDeco);
            valori.Add("$SGATWONUMBER$", data.SGATWONumber);
            valori.Add("$PROCESSINGCOST$", data.ProcessingCost.HasValue ? data.ProcessingCost.Value.ToString("0.00") : null);
            valori.Add("$SPAREPARTSCOST$", data.SparePartsCost.HasValue ? data.SparePartsCost.Value.ToString("0.00") : null); 
            valori.Add("$INSERTDATE$", data.TmstInseDeco);
            valori.Add("$USERINSENAME$", data.UserInseName);
            valori.Add("$FINALCOST$", data.FinalCost.ToString("0.00"));

            retEmail.Subject = SostituisciPlaceholder(testo: retEmail.Subject, valori: valori);
            retEmail.Body = SostituisciPlaceholder(testo: retEmail.Body, valori: valori);
           
            return retEmail;
        }
        // ***********************************************************
        // INVIO EMAIL PER WARRANTY 
        // ***********************************************************
        public void SendNotificationWSP(int WSPRequestId, Lookup.Request_NotificationType NotificationType, string userName, 
                                        string note = "", string title = "", string extTo = "", string  extCC = "",
                                        string[] attachments = null, bool deleteAttachments = false)
        {
            string templateId = "";
            string to = "";
            string cc = "";
            string bcc = "";
            EmailTemplate email = new EmailTemplate();

            WSPRequestDTO request = (new WarrantyBO()).getRequests(WSPRequestId: WSPRequestId).FirstOrDefault();
            if (request == null)
            {
                _logger.LogError(string.Format("WSPRequest {0} not found", WSPRequestId));
                return;
            }

            string emailUserRegistration = (new UsersBO()).GetEmail(request.UserInse);

            switch (NotificationType)
            {
                case Lookup.Request_NotificationType.NewRequest:
                    templateId = Lookup.Warranty_Email_NewRequest;  
                    to = (new UsersBO()).GetEmailsInRole(roles: new[] { Lookup.Role_WarrantyAdministrator });
                    break;
                case Lookup.Request_NotificationType.SetWarrantyStatus:
                    templateId = string.Format(Lookup.Warranty_Email_SetWarrantyStatus, request.WarrantyStatus);
                    to = emailUserRegistration;
                    cc = (new UtilBO()).GetDLEmails(DLType: Lookup.Code_DL_ListType_OrderDepartments);
                    break;
                case Lookup.Request_NotificationType.CloseRequest:
                    templateId = Lookup.Warranty_Email_CloseRequest;
                    to = emailUserRegistration;
                    break;
                case Lookup.Request_NotificationType.EngagementSupplier:
                    if (!string.IsNullOrWhiteSpace(extTo) )
                    {
                        to = extTo;
                        cc = extCC;
                        email.Subject = title;
                        email.Body = note;
                    }
                    break;
                default: return;
            }
            if (!string.IsNullOrWhiteSpace(templateId))
            {
                email = GetEmailInfoWSP(WSPRequestId: WSPRequestId, templateId: templateId, userName: userName, note: note, title: title);
            }
            to = !string.IsNullOrWhiteSpace(to) ? to : extTo;
            cc = !string.IsNullOrWhiteSpace(cc) ? cc : extCC;

            SendNotification(label: "SendNotificationWSP/WARRANTY",
                             templateId: templateId,
                             requestID: WSPRequestId.ToString(),
                             to: to,
                             cc: cc,
                             bcc: bcc,
                             subject: email.Subject,
                             body: email.Body,
                             context: "WARRANTY", 
                             attachments: attachments, 
                             deleteAttachments: deleteAttachments);
            return;
        }

        public EmailTemplate GetEmailInfoTK(int TKRequestId, string templateId, string userName, string note = "", string title = "")
        {
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };

            TKRequest request = _context.TKRequests.Find(TKRequestId);
            if (request == null)
            {
                _logger.LogError(string.Format("TKRequestId {0} not found", TKRequestId));
                return retEmail;
            }
            InstrumentModel instrument = null;
            if (!string.IsNullOrWhiteSpace(request.SerialNumber))
            {
                instrument = (new InstrumentsBO()).GetInstrument(request.SerialNumber); 
            }
            Site location = null;
            if (request.LocationId != null)
            {
                location = _context.Sites.Find(request.LocationId);
            }
            
            TKClassification RootArea = (new SupportBO()).GetClassificationRoot(request.TKClassificationId);

            EmailTemplate email = _context.EmailTemplates.Find(templateId);
            if (email == null)
            {
                retEmail.Subject = title;
                retEmail.Body = note;
            }
            else
            {
                retEmail.Subject = email.Subject;
                retEmail.Body = email.Body;
            }

            Dictionary<string, string> valori = new Dictionary<string, string>();
            valori.Add("$REQUESTID$", request.TKRequestId.ToString() + (request.Complaint ? "/COMPLAINT":""));
            valori.Add("$OBJECT$", request.Object);
            valori.Add("$DESCRIPTION$", request.Description);
            valori.Add("$REQUESTDATE$", request.TmstInseDeco);
            valori.Add("$USERREGISTRATION$", (new UsersBO()).GetFullName(request.UserRegistration));
            valori.Add("$USER$", (new UsersBO()).GetFullName(userName));
            List<string> country = (new UsersBO()).GetLinks(request.UserRegistration, Lookup.UserLinkType.Country);
            string sep = "";
            string listCountry = "";
            foreach (var item in country)
            {
                listCountry += sep + item;
                sep = ", ";
            }
            valori.Add("$COUNTRY$", listCountry);
            valori.Add("$NOTE$", note);

            valori.Add("$AREACODE$", RootArea?.Code);
            valori.Add("$AREATITLE$", RootArea?.Title);


            valori.Add("$SERIALNUMBER$", instrument?.SerialNumberInfo ?? request.SerialNumber);
            valori.Add("$LOCATION$", location?.DescriptionFull );


            retEmail.Subject = SostituisciPlaceholder(testo: retEmail.Subject, valori: valori);
            retEmail.Body = SostituisciPlaceholder(testo: retEmail.Body, valori: valori);

            return retEmail;
        }

        public EmailTemplate GetEmailInfoWSP(int WSPRequestId, string templateId, string userName, string note = "", string title = "")
        {
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };

            WSPRequestDTO request = (new WarrantyBO()).getRequests(WSPRequestId: WSPRequestId).FirstOrDefault();
            if (request == null)
            {
                _logger.LogError(string.Format("WSPRequest {0} not found", WSPRequestId));
            } else
            {
                EmailTemplate email = _context.EmailTemplates.Find(templateId);
                if (email == null)
                {
                    retEmail.Subject = title;
                    retEmail.Body = note;
                }
                else
                {
                    retEmail.Subject = email.Subject;
                    retEmail.Body = email.Body;
                }
                Dictionary<string, string> valori = new Dictionary<string, string>();
                valori.Add("$REQUESTID$", request.WSPRequestId.ToString());
                valori.Add("$CODE$", request.Code);
                valori.Add("$ISSUEDATE$", request.TmstIssueDeco);
                valori.Add("$SPAREPART$", request.CodArtDeco);
                valori.Add("$DECONTAMINED$", request.DecontaminedDeco);
                valori.Add("$REQUESTNOTE$", request.Notes);
                valori.Add("$USERREGISTRATION$", (new UsersBO()).GetFullName(request.UserInse));
                valori.Add("$USER$", (new UsersBO()).GetFullName(userName));
                valori.Add("$WARRANTYSTATUSNOTE$", request.WarrantyStatusNote);

                string myStr = "";
                string mySep = "";
                List<string> commercialEntitiesLink = (new UsersBO()).GetLinks(request.UserInse, Lookup.UserLinkType.CommercialEntity);
                foreach (var item in commercialEntitiesLink)
                {
                    myStr  += mySep + item;
                    mySep = ", ";
                }
                valori.Add("$USERCOMMERCIALENTITY$", myStr);

                myStr = "";
                mySep = "";
                List<string> customersLink = (new UsersBO()).GetLinks(request.UserInse, Lookup.UserLinkType.Customer);
                foreach (var item in customersLink)
                {
                    string nomeCliente = new CustomersBO().GetDesc(item);
                    myStr += mySep + nomeCliente ?? item;
                    mySep = ", ";
                }
                valori.Add("$USERCUSTOMER$", myStr);

                retEmail.Subject = SostituisciPlaceholder(testo: retEmail.Subject, valori: valori);
                retEmail.Body = SostituisciPlaceholder(testo: retEmail.Body, valori: valori);
            }
            return retEmail;
        }

        public EmailTemplate GetEmailInfoGeneric(string templateId, string userName, string note = "", string title = "")
        {
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };

            EmailTemplate email = _context.EmailTemplates.Find(templateId);
            if (email == null)
            {
                retEmail.Subject = title;
                retEmail.Body = note;
            }
            else
            {
                retEmail.Subject = email.Subject;
                retEmail.Body = email.Body;
            }
            return retEmail;
        }

        private string SostituisciPlaceholder(string testo, Dictionary<string, string> valori)
        {
            string valore = "";
            if (!String.IsNullOrWhiteSpace(testo))
            {
                foreach (var item in valori)
                {
                    valore = "";
                    if (!string.IsNullOrWhiteSpace(item.Value))
                    {
                        valore = item.Value.Replace("\n\r", "<br/>").Replace("\n", "<br/>").Replace("\r", "<br/>");
                    }
                    testo = testo.Replace(item.Key, valore);
                }
            }
            return testo;
        }

        // ***********************************************************
        // INVIO EMAIL PER BOOKING 
        // ***********************************************************
        public void SendNotificationBooking(int BookingId, Lookup.Request_NotificationType NotificationType, string userName,
                                            string note = "", string title = "", string extTo = "", string extCC = "")
        {
            string templateId = "", to = "", cc = "", bcc = "";
            EmailTemplate email = new EmailTemplate();

            BookingDTO data = (new InstrumentsBO()).GetBookings(userName: userName, BookingId: BookingId).FirstOrDefault();

            if (data == null)
            {
                _logger.LogError(string.Format("Booking {0} not found", BookingId));
                return;
            }
            InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(data.SerialNumber);
            if (instrument == null)
            {
                _logger.LogError(string.Format("Instrument {0} not found", data.SerialNumber));
                return;
            }
            string emailOwnerGroup = "";
            if (instrument.OwnerGroupId != null && instrument.OwnerGroupId > 0)
            {
                emailOwnerGroup = (new UtilBO()).GetDLEmails(DLListId: (int)instrument.OwnerGroupId);
            }
            switch (NotificationType)
            {
                case Lookup.Request_NotificationType.BookingNewReservation:
                    templateId = Lookup.Booking_Email_NewReservation;
                    to = emailOwnerGroup;
                    break;
                case Lookup.Request_NotificationType.BookingModifyReservation:
                    templateId = Lookup.Booking_Email_ModifyReservation;
                    to = emailOwnerGroup;
                    break;
                case Lookup.Request_NotificationType.BookingDeleteReservation:
                    templateId = Lookup.Booking_Email_DeleteReservation;
                    to = emailOwnerGroup;
                    break;
                default: return;
            }

            if (!string.IsNullOrWhiteSpace(templateId))
            {
                email = GetEmailInfoBooking(dto: data, templateId: templateId, userName: userName, note: note, title: title);
            }
            to = !string.IsNullOrWhiteSpace(extTo) ? extTo : to;
            cc = !string.IsNullOrWhiteSpace(extCC) ? extTo : cc;

            SendNotification(label: "SendNotificationBooking",
                             templateId: templateId,
                             requestID: BookingId.ToString(),
                             to: to,
                             cc: cc,
                             bcc: bcc,
                             subject: email.Subject,
                             body: email.Body,
                             context: "BOOKING");
            return;
        }


        public EmailTemplate GetEmailInfoBooking(BookingDTO dto, string templateId, string userName, string note = "", string title = "")
        {
            EmailTemplate retEmail = new EmailTemplate()
            {
                Subject = "",
                Body = "",
                To = ""
            };

            EmailTemplate email = _context.EmailTemplates.Find(templateId);
            if (email == null)
            {
                retEmail.Subject = title;
                retEmail.Body = note;
            }
            else
            {
                retEmail.Subject = email.Subject;
                retEmail.Body = email.Body;
            }

            InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(dto.SerialNumber);

            Dictionary<string, string> valori = new Dictionary<string, string>();
            valori.Add("$ROOM$", instrument.SiteDescription);
            valori.Add("$SERIALNUMBER$", dto.SerialNumber);
            valori.Add("$INTERNALCODE$", instrument.Machine_Description);
            valori.Add("$FROM$", dto.TmstFromDeco);
            valori.Add("$TO$", dto.TmstToDeco);
            valori.Add("$TITLE$", dto.Title);
            valori.Add("$NOTE$", dto.Note);
            valori.Add("$USER$", dto.UserInse_Name);

            retEmail.Subject = SostituisciPlaceholder(testo: retEmail.Subject, valori: valori);
            retEmail.Body = SostituisciPlaceholder(testo: retEmail.Body, valori: valori);

            return retEmail;
        }
        // ***********************************************************
        // INVIO EMAIL - FUNZIONE INZIO 
        // **********************************************************
        public void SendNotification(string label, string templateId = "", string requestID = "", string to = "", string cc = "", string bcc = "",
                                            string subject = "", string body = "", string context = "", string[] attachments = null, bool deleteAttachments = false, string replyTo = "")
        {
            _logger.LogInformation(string.Format("*** {1} - template: {0} ***", templateId, label));
            _logger.LogInformation(string.Format("requestId: {0}", requestID));
            _logger.LogInformation(string.Format("to: {0}", to));
            _logger.LogInformation(string.Format("cc: {0}", cc));
            _logger.LogInformation(string.Format("bcc: {0}", bcc));
            _logger.LogInformation(string.Format("replyTo: {0}", replyTo));
            _logger.LogInformation(string.Format("subject: {0}", subject));
            _logger.LogInformation(string.Format("body: {0}", body));

            if (!string.IsNullOrWhiteSpace(to))
            {
                var destinatari = to.Split(',');
                if (destinatari != null && destinatari.Length > 0)
                {
                    var subDestinatari = destinatari
                                            .Select((x, i) => new { Index = i, Value = x })
                                            .GroupBy(x => x.Index / 50)
                                            .Select(x => x.Select(v => v.Value).ToList())
                                            .ToList();
                    foreach (var item in subDestinatari)
                    {
                        to = string.Join(",", item.Select(p => p).ToArray());
                        string msgException = "";
                        var retSend = _emailSender.SendEmailAsync(email: to, subject: subject, message: body,
                                                                  emailCC: cc, emailBcc: bcc, context: context, 
                                                                  attachments: attachments, deleteAttachments: deleteAttachments, 
                                                                  _logger: _logger, emailReplayTo: replyTo);
                        if (retSend.IsFaulted)
                        {
                            if (retSend.Exception != null || retSend.Exception.InnerException != null)
                            {
                                msgException += retSend.Exception.InnerException.Message;
                            }
                            _logger.LogError(string.Format("*** ERROR SendEmailAsync() **** {0}", msgException));
                        }
                        // azzero CC e BCC per evitare di rimandarli nel caso di invio della email a blocchi per ridurre il numero di destinatari
                        bcc = "";
                        cc = "";
                    }
                }
                //DA VERIFICARE ??? (ERRORE SSL ??)
                //{
                //    string msgException = "";
                //    if (retSend.Exception != null || retSend.Exception.InnerException != null)
                //    {
                //        msgException = retSend.Exception.InnerException.Message;
                //    }
                //    _logger.LogError(string.Format("*** ERROR SendEmailAsync() **** {0}", msgException));
                //}
            }

            return;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class DocumentalRepositoryBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public DocumentalRepositoryBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco Topics
        public List<Topic> GetTopics(int SectionId = 0, string FileName = "", string UserIdCfg = "", bool bAllTopics = false, bool showSubSections = true,
                                    bool bGetResourcesNew = false, bool bOnlyAuthorized = false)
        {
             
            var ret = new List<Topic>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT TOPIC.DRTOPICID AS TOPICID, TOPIC.CODE, TOPIC.NOTE, TOPIC.REVISION, TOPIC.TITLE, TOPIC.RELEASEDDATE,
                                        COALESCE(PATHS.BREADCRUMB, TOPIC.TITLE) as BREADCRUMB, TOPIC.TMSTLASTUPDRESOURCES, TOPIC.DRSECTIONID as SECTIONID, ";
            // Estrazione Numero di risorse da leggere per il TOPIC 
            if (bGetResourcesNew && !string.IsNullOrWhiteSpace(UserIdCfg))
            {
                Sql += @"(SELECT count(*) from DRRESOURCES WHERE DRTOPICID = TOPIC.DRTOPICID AND DRRESOURCES.DELETED = 0
                            AND NOT EXISTS (SELECT 1 FROM DRTRACKINGS
                            WHERE EXTID = DRRESOURCEID AND DRTRACKINGS.USERINSE = @UserId AND EXTTYPE = 'RESOURCE' 
                              AND OPERATION = 'GET' AND DRTRACKINGS.TMSTINSE > DRRESOURCES.TmstLastUpd)
                         )";
                args.Add(new { UserId = (new UsersBO()).GetUserName(UserIdCfg) });
            } else
            {
                Sql += "0";
            }
            Sql += @" AS CntResourcesNew";

            Sql += @" FROM DRTOPICS TOPIC
                            INNER JOIN DRSECTIONS SECTION
                                ON SECTION.DRSECTIONID = TOPIC.DRSECTIONID 
                            INNER JOIN DRSECTIONPATHS PATHS
                                ON PATHS.DRSECTIONID = TOPIC.DRSECTIONID
                                AND PATHS.DRSECTIONIDREF IN (@Sezioni)";
            if (!showSubSections)
            {
                // Estraggo solo i TOPIC della sezione corrente 
                Sql += " AND PATHS.DRSECTIONIDREF = PATHS.DRSECTIONID";
            }
            if (!bAllTopics)
            {
                // Visualizzo solo i Topic validi (non bloccati)
                Sql += " AND SECTION.LOCKED = 0";
            }
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                // Ricerca anche nel TITLE del TOPIC 
                Sql += @" AND (EXISTS(SELECT 1 FROM DRRESOURCES 
                                        WHERE DRRESOURCES.DRTOPICID = TOPIC.DRTOPICID 
                                         AND DRRESOURCES.DELETED = 0 
                                         AND (UPPER(TITLE) LIKE @NomeFileLike OR UPPER(SOURCE) LIKE @NomeFileLike))
                                OR UPPER(TOPIC.TITLE) LIKE @NomeFileLike 
                            )";
                args.Add(new { NomeFileLike = string.Format("%{0}%", FileName.ToUpper()) });
            }

            Sql += " WHERE 1 = 1";
            if (bOnlyAuthorized && !string.IsNullOrWhiteSpace(UserIdCfg))
            {
                // Escludo TUTTE le SEZIONI che nella risalita HANNO una SEZIONE NON ABILITATA 
                List<int> sessioni = getSectionsToExclude(UserIdCfg);
                if (sessioni.Count() > 0 )
                {
                    Sql += @" AND NOT EXISTS(select 1 from DRSectionPaths PATHSNO
                                  WHERE PATHSNO.DRSectionIdRef IN (@SezioniNonAutorizzate)
                               and PATHSNO.DRSectionId = TOPIC.DRSECTIONID)";
                    args.Add(new { SezioniNonAutorizzate = sessioni });
                }
            }
            if (!bAllTopics)
            {
                Sql += " ORDER BY TOPIC.TMSTLASTUPDRESOURCES DESC";
            } else
            {
                Sql += " ORDER BY TOPIC.CODE, TOPIC.REVISION, TOPIC.TITLE";
            }

            if (SectionId > 0)
            {
                args.Add(new { Sezioni = SectionId });
            } else
            {
                List<int> mainSection = new List<int>();
                if (!string.IsNullOrWhiteSpace(UserIdCfg))
                {
                    var list = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.DRSection);
                    if (list != null && list.Count > 0)
                    {
                        list.ForEach(i => mainSection.Add(Int32.Parse(i))); 
                    } else
                    {
                        mainSection.Add(-1);  // Default per NON estrarre NULLA 
                    }
                } else
                {
                    // Visualizzo TUTTE le MAIN
                    var list = (new DocumentalRepositoryBO()).GetMainSections();
                    list.ForEach(i => mainSection.Add(i.DRSectionId));
                }
                args.Add(new { Sezioni = mainSection });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Topic>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetTopics.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco Risorse di un topic, con indicazione ULTIMA Lettura da parte dell'utente 
        public List<DRResource> GetResourcesWithLastGet(int TopicId, string UserName)
        {
            var ret = new List<DRResource>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DRRESOURCEID, DRTOPICID, SOURCE, TITLE, TYPE, TMSTLASTUPD,
                               (SELECT MAX(TMSTINSE) FROM DRTRACKINGS 
                                    WHERE DRTRACKINGS.USERINSE = @UserName
	                                  AND EXTTYPE = 'RESOURCE' AND EXTID = DRRESOURCEID
                                      AND OPERATION = 'GET') AS TmstLastGet
                        FROM DRRESOURCES
                        WHERE DRTOPICID = @TopicId 
                          AND DELETED = 0 
                        ORDER BY TYPE, TITLE";
            args.Add(new { UserName = UserName });
            args.Add(new { TopicId = TopicId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<DRResource>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetResourcesWithLastGet.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco SECTION ROOT (primo livello)
        public List<DRSection> GetMainSections()
        {

            var ret = new List<DRSection>();
            var Sql = @"SELECT * FROM DRSECTIONS WHERE ParentId IS NULL ORDER BY TITLE ";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<DRSection>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetMainSections.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco SECTION primo e secondo livello (per abilitazione)
        public List<DRSection> GetSectionAuthorizables(string userId = "", bool onlyAuthorized = false, bool bAddRoot = false, bool bOnlyActive = false, bool bGetSubSections = false)
        {
            var ret = new List<DRSection>();
            List<object> args = new List<object>();
            var select = @"SELECT V_DRSECTIONAUTHORIZABLE.*";
            var from = @"FROM V_DRSECTIONAUTHORIZABLE";
            var where = @"WHERE 1 = 1";
            var orderby = @"ORDER BY V_DRSECTIONAUTHORIZABLE.TITLE, V_DRSECTIONAUTHORIZABLE.DRSECTIONID";

            if (!string.IsNullOrWhiteSpace(userId))
            {
                select += ", IIF(UserLinks.LinkID IS NULL, 0, 1) as Selected";
                from += @" LEFT JOIN UserLinks
                            ON V_DRSECTIONAUTHORIZABLE.DRSectionId = CAST(UserLinks.LinkID AS int)
                            AND UserLinks.TypeId = @TypeIdDRSection
                            AND userLinks.UserID = @UserId";
                if (onlyAuthorized)
                {
                    where += " AND UserLinks.LinkID  IS NOT NULL";
                }
                args.Add(new { TypeIdDRSection = (int)Lookup.UserLinkType.DRSection });
                args.Add(new { UserId = userId });
            } else
            {
                select += ", 0 as Selected";  // False 
            }
            if (bOnlyActive)
            {
                // Estraggo solo le section NON LOCKED 
                where += " AND V_DRSECTIONAUTHORIZABLE.LOCKED = 0";  
            }
            if (bGetSubSections)
            {
                // from += @" INNER JOIN DRSECTIONPATHS PATHS ON PATHS.DRSECTIONID = V_DRSECTIONAUTHORIZABLE.DRSECTIONID";
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{select} {from} {where} {orderby}";
                    ret = db.Query<DRSection>(sql, args.ToArray()).ToList();
                    ret.ForEach(o => o.Expanded = true);
                    if (bAddRoot)
                    {
                        // aggiungo un record fittizio come root (id = -1) 
                        ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                        ret.Add(new DRSection() { DRSectionId = -1, Title = "All Sections", Expanded = true });
                    }
                    //ret.ForEach(o => o.Selected = false);
                    //if (!string.IsNullOrWhiteSpace(userId))
                    //{
                    //    // Propongo le Section già selezionate
                    //    var list = (new UsersBO()).GetLinks(userId, Lookup.UserLinkType.DRSection);
                    //    if (list != null && list.Count > 0)
                    //    {
                    //        foreach (var i in list)
                    //        {
                    //            int sectionId = Int32.Parse(i);
                    //            foreach (var r in ret)
                    //            {
                    //                if (r.DRSectionId == sectionId)
                    //                {
                    //                    r.Selected = true;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetSectionAuthorizables.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // vengono estratte tutte le SEZIONI LOCKED oppure NON autorizzate 
        private List<int> getSectionsToExclude(string userId = "")
        {
            var ret = new List<int>();
            List<object> args = new List<object>();
            var sql = @"SELECT DRSECTIONID
                        FROM V_DRSECTIONAUTHORIZABLE
                        WHERE LOCKED = 1 
                           OR DRSECTIONID NOT IN 
                            (SELECT CAST(USERLINKS.LINKID AS int) 
                                FROM USERLINKS
                                WHERE UserLinks.TypeId = @TypeIdDRSection
                                AND userLinks.UserID = @UserId)";
                args.Add(new { TypeIdDRSection = (int)Lookup.UserLinkType.DRSection });
                args.Add(new { UserId = userId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<int>(sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("getSectionsToExclude.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco delle abilitazioni di tutti gli utenti su una specifica Session, escludendo DELETE e LOCKED 
        public List<DRUserAuthorizationsForSessionDTO> GetUserAuthorizationsForSession(int DRSessionId)
        {
            var ret = new List<DRUserAuthorizationsForSessionDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT ASPNETUSERS.USERNAME, IIF(UserLinks.LinkID IS NULL, 'No', 'Authorized') AS AUTHORIZED
                               FROM ASPNETUSERS
                          LEFT JOIN USERCFG 
				            ON USERCFG.USERID = ASPNETUSERS.ID
                         LEFT JOIN UserLinks
                            ON UserLinks.USERID = ASPNETUSERS.ID
                           AND UserLinks.TypeId = @TypeIdDRSection
                           AND Userlinks.LinkID = @SessionId
                        WHERE ASPNETUSERS.LockoutEnd IS NULL
                          AND USERCFG.DELETED = 0
                         ORDER BY USERNAME";
            args.Add(new { TypeIdDRSection = (int)Lookup.UserLinkType.DRSection });
            args.Add(new { SessionId = DRSessionId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<DRUserAuthorizationsForSessionDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetUserAuthorizationsForSession.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }


        // solo user validi (no locked e non deleted)
        public List<UsersForAuthor> GetUsersForAuthor(int sectionId)
        {
            var ret = new List<UsersForAuthor>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASPNETUSERS.ID AS USERID, ASPNETUSERS.USERNAME, USERCFG.FULLNAME, 
                               USERCFG.COMPANYNAME, USERCFG.CITY, DEPARTMENTS.DESCRIPTION AS DEPARTMENT, 
                               LINKS.LinkID
                            FROM ASPNETUSERS
                            INNER JOIN USERCFG 
                                ON USERCFG.USERID = ASPNETUSERS.ID
                            LEFT JOIN USERLINKS LINKS 
                                ON LINKS.UserID = ASPNETUSERS.ID
                                AND LINKS.TypeId = @TypeIdDRSection
                                AND LINKS.LinkID = @sectionId
                            LEFT JOIN DEPARTMENTS 
						        ON DEPARTMENTS.DEPARTMENTID = USERCFG.DEPARTMENTID
                            WHERE ASPNETUSERS.LockoutEnd IS NULL AND USERCFG.DELETED = 0 
                        ORDER BY USERNAME";
            args.Add(new { TypeIdDRSection = (int)Lookup.UserLinkType.DRSection });
            args.Add(new { sectionId = sectionId.ToString() });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<UsersForAuthor>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetUsersForAuthor.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // elenco Sezioni
        public List<DRSection> GetSections(bool bHideRoot = false, bool bShowCounter = false, int excludeSectionId=0, 
                                           bool bExpandAll = false, int SelectedId = 0)
        {
            var ret = new List<DRSection>();
            List<object> args = new List<object>();
            var SqlFields = @"SELECT SECTIONS.DRSectionId, SECTIONS.Locked, SECTIONS.ParentId, SECTIONS.Title";
            var SqlFrom = @"FROM DRSECTIONS SECTIONS";
            var sqlWhere = "WHERE 1 = 1";
            var sqlGroupBy = "";
            var sqlOrderBy = " ORDER BY SECTIONS.TITLE, SECTIONS.DRSECTIONID";

            if (bShowCounter)
            {
                //SqlFields += @", COUNT(DISTINCT DRTopics.DRTopicId) AS cntTopics, COUNT(DISTINCT DRResources.DRResourceId) as cntResources";
                //SqlFrom += @" LEFT JOIN DRTopics ON DRTopics.DRSectionId = SECTIONS.DRSectionId
                //              LEFT JOIN DRResources ON DRResources.DRTopicId = DRTopics.DRTopicId AND DELETED = 0";
                //sqlGroupBy = " GROUP BY SECTIONS.DRSectionId, SECTIONS.Locked, SECTIONS.ParentId, SECTIONS.Title";
                SqlFields += @", COUNT(DISTINCT DRTopics.DRTopicId) AS cntTopics";
                SqlFrom += @" LEFT JOIN DRTopics ON DRTopics.DRSectionId = SECTIONS.DRSectionId";
                sqlGroupBy = " GROUP BY SECTIONS.DRSectionId, SECTIONS.Locked, SECTIONS.ParentId, SECTIONS.Title";
            }
            if (excludeSectionId > 0)
            {
                sqlWhere += " AND SECTIONS.DRSectionId NOT IN (SELECT DRSECTIONID FROM  DRSECTIONPATHS WHERE DRSectionIdRef = @SectionToExclude)";
                args.Add(new { SectionToExclude = excludeSectionId });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{SqlFields} {SqlFrom} {sqlWhere} {sqlGroupBy} {sqlOrderBy}";
                    ret = db.Query<DRSection>(sql, args.ToArray()).ToList();
                    if (!bHideRoot)
                    {
                        // aggiungo un record fittizio come root (id = -1) 
                        ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                        ret.Add(new DRSection() { DRSectionId = -1, Title = "All Sections", Expanded = true });
                        if (bExpandAll)
                        {
                            ret.ForEach(o => o.Expanded = true);
                        }
                        foreach(DRSection item in ret)
                        {
                            if (SelectedId > 0 && item.DRSectionId == SelectedId)
                            {
                                item.Selected = true;
                                item.Expanded = true;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetSections.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // Estrazioni delle sole sezione abilitate all'utente e che NON sono bloccate e che NON hanno nel path un sessione NON abilitata
        public List<DRSection> GetUserSections(string UserIdCfg)
        {
            var ret = new List<DRSection>();
            List<object> args = new List<object>();
            var SqlFields = @"SELECT DISTINCT SECTIONS.DRSectionId, SECTIONS.Locked, SECTIONS.ParentId, SECTIONS.Title";
            var SqlFrom = @"FROM DRSECTIONS SECTIONS";
            var sqlWhere = "WHERE 1 = 1";
            var sqlGroupBy = "";
            var sqlOrderBy = " ORDER BY SECTIONS.TITLE, SECTIONS.DRSECTIONID";

            // Estraggo SOLO le Sezioni NON BLOCCATE e abilitate all'utente passato 
            List<int> mainSection = new List<int>();
            var list = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.DRSection);
            if (list != null && list.Count > 0)
            {
                list.ForEach(i => mainSection.Add(Int32.Parse(i)));
            }
            else
            {
                mainSection.Add(0); // per non estrarre nulla
            }
            SqlFrom += @" INNER JOIN DRSECTIONPATHS PATHS 
                                    ON PATHS.DRSECTIONID = SECTIONS.DRSECTIONID
                                    AND PATHS.DRSECTIONIDREF IN(@Sezioni)";
            args.Add(new { Sezioni = mainSection });
            sqlWhere += " AND SECTIONS.LOCKED = 0";

            // Escludo TUTTE le SEZIONI che nella risalita HANNO una SEZIONE NON ABILITATA 
            List<int> sessioni = getSectionsToExclude(UserIdCfg);
            if (sessioni.Count() > 0)
            {
                sqlWhere += @" AND NOT EXISTS(select 1 from DRSectionPaths PATHSNO
                                  WHERE PATHSNO.DRSectionIdRef  IN (@SezioniNonAutorizzate)
                               and PATHSNO.DRSectionId = SECTIONS.DRSECTIONID)";
                args.Add(new { SezioniNonAutorizzate = sessioni });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    string sql = $"{SqlFields} {SqlFrom} {sqlWhere} {sqlGroupBy} {sqlOrderBy}";
                    ret = db.Query<DRSection>(sql, args.ToArray()).ToList();
                    //ret.ForEach(o => o.Expanded = true);
                    // aggiungo un record fittizio come root (id = -1) 
                    ret.ForEach(r => r.ParentId = (r.ParentId ?? -1));
                    ret.Add(new DRSection() { DRSectionId = -1, Title = "All Sections", Expanded = true });
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetSections.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // riepilogo Risorse 
        public string GetSummaryResources(int TopicId)
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT *  FROM DRRESOURCES
                        WHERE DRTOPICID = @TopicId AND DELETED = 0 
                        ORDER BY TYPE, TITLE";
            args.Add(new { TopicId = TopicId });
            try
            {
                using (IDatabase db = Connection)
                {
                    List<DRResource> data = db.Query<DRResource>(Sql, args.ToArray()).ToList();
                    foreach(var item in data)
                    {
                        ret += String.IsNullOrWhiteSpace(ret) ? "" : "\n";
                        ret += string.Format("{0} - {1}", item.TypeDeco, item.Title);
                        if (item.Title != item.Source)
                        {
                            ret += string.Format(" [{0}]", item.Source);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string msg = string.Format("GetSummaryResources.get() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.Error(msg);
                return msg;
            }
            return ret;
        }

        // getResourceTrackings
        public List<ResourceTracking> getResourceTrackings(int SectionId, string FileName, bool OnlyLastGet, bool showDeleted)
        {
            var ret = new List<ResourceTracking>();
            List<object> args = new List<object>();
            var Sql = @"SELECT TRACKING.OPERATION, TRACKING.EXTID, TRACKING.EXTTYPE";
            if (OnlyLastGet)
            {
                Sql += @", (SELECT MAX(TRACKING2.TMSTINSE) FROM DRTRACKINGS TRACKING2 
	                                 WHERE TRACKING2.EXTID = TRACKING.EXTID
	                                 AND   TRACKING2.EXTTYPE = TRACKING.EXTTYPE
	                                 AND   TRACKING2.OPERATION = TRACKING.OPERATION) AS TMSTINSE";
            } else
            {
                Sql += ", TRACKING.DRTRACKINGID, TRACKING.TMSTINSE AS TMSTINSE, TRACKING.NOTE AS TRACKING_NOTE";
            }
            Sql += @", RESOURCE.DRRESOURCEID, RESOURCE.DELETED, RESOURCE.SOURCE as RESOURCE_SOURCE , RESOURCE.TYPE as RESOURCE_TYPE, RESOURCE.TITLE AS RESOURCE_TITLE, 
                       TOPIC.DRTOPICID, TOPIC.CODE, TOPIC.REVISION, TOPIC.NOTE AS TOPIC_NOTE, TOPIC.TITLE AS TOPIC_TITLE,
                       SECTION.DRSECTIONID, SECTION.LOCKED, SECTION.TITLE AS SECTION_TITLE, 
	                   COALESCE(PATHS.BREADCRUMB, TOPIC.TITLE) as BREADCRUMB, COALESCE(USERCFG.FULLNAME, ASPNETUSERS.USERNAME) AS FULLNAME";
            Sql += @" FROM DRRESOURCES RESOURCE 
                        INNER JOIN DRTRACKINGS TRACKING
                            ON TRACKING.EXTID = RESOURCE.DRRESOURCEID 
                            AND TRACKING.EXTTYPE = 'RESOURCE'
                        INNER JOIN DRTOPICS TOPIC
                            ON TOPIC.DRTOPICID = RESOURCE.DRTOPICID
                        INNER JOIN DRSECTIONS SECTION
                            ON SECTION.DRSECTIONID = TOPIC.DRSECTIONID 
                        INNER JOIN DRSECTIONPATHS PATHS
                            ON PATHS.DRSECTIONID = TOPIC.DRSECTIONID
                            AND PATHS.DRSECTIONIDREF IN (@Sezioni)
                        INNER JOIN ASPNETUSERS 
                            ON TRACKING.USERINSE = ASPNETUSERS.USERNAME
                        INNER JOIN USERCFG 
                            ON ASPNETUSERS.ID = USERCFG.USERID";
            if (SectionId > 0)
            {
                args.Add(new { Sezioni = SectionId });
            }
            else
            {
                List<int> mainSection = new List<int>();
                var list = this.GetMainSections();
                list.ForEach(i => mainSection.Add(i.DRSectionId));
                args.Add(new { Sezioni = mainSection });
            }
            Sql += " WHERE 1 = 1";
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                Sql += @" AND EXISTS(SELECT 1 FROM DRRESOURCES 
                                        WHERE DRRESOURCES.DRTOPICID = TOPIC.DRTOPICID 
                                         AND DELETED = 0 
                                         AND (UPPER(TITLE) LIKE @NomeFileLike OR UPPER(SOURCE) LIKE @NomeFileLike))";
                args.Add(new { NomeFileLike = string.Format("%{0}%", FileName) });
            }
            if (OnlyLastGet)
            {
                Sql += " AND TRACKING.OPERATION = 'GET'";
            }
            if (!showDeleted)
            {
                Sql += " AND RESOURCE.DELETED = 0";
            }

            if (OnlyLastGet)
            {
                Sql += @" GROUP BY TRACKING.OPERATION, USERCFG.FULLNAME, ASPNETUSERS.USERNAME, TRACKING.ExtId, TRACKING.ExtType, 
                                   RESOURCE.DRRESOURCEID, RESOURCE.DELETED, RESOURCE.SOURCE, RESOURCE.TYPE,  RESOURCE.TITLE, 
                                   TOPIC.DRTOPICID, TOPIC.CODE, TOPIC.REVISION, TOPIC.NOTE, TOPIC.TITLE,
		                           SECTION.DRSECTIONID, SECTION.LOCKED, SECTION.TITLE,  PATHS.BREADCRUMB";
            }
            Sql += " ORDER BY PATHS.BREADCRUMB, TOPIC.NOTE, RESOURCE.TYPE, RESOURCE.TITLE, TMSTINSE DESC";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<ResourceTracking>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("getResourceTrackings.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco EMAIL per NOTIFICHE su TOPIC
        public List<EmailTopic> GetEmailNotificaTopic(DRTopic topic)
        {
            var allEmail = new List<EmailTopic>();
            List<object> args = new List<object>();
            // dalla vista vengono esclusi gli utenti LOCKED, DELETED e quelli che NON vogliono la notifica !!! 
            var Sql = @"SELECT DISTINCT USERNAME, EMAIL, USERID FROM V_NotificheTopic WHERE DRTopicId = @TopicId";
            args.Add(new { TopicId = topic.DRTopicId });
            try
            {
                using (IDatabase db = Connection)
                {
                    allEmail = db.Query<EmailTopic>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetEmailNotificaTopic.get()/1 - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            // Sull'elenco di tutte le email ottenuto, elimino gli user cancellati, bloccati, che non vogliono email o che NON hanno la sezione tra quelle abilitate 
            var ret = new List<EmailTopic>();
            foreach (var item in allEmail)
            {
                List<int> sections = (new DocumentalRepositoryBO()).GetUserSections(UserIdCfg: item.UserId).Select(t => t.DRSectionId).ToList();
                if (sections.Contains(topic.DRSectionId))
                {
                    ret.Add(item);
                }
            }
            return ret;
        }

        //// elenco TUTTE le email 
        //public List<EmailTopic> GetAllEmailNotificaTopic()
        //{
        //    var ret = new List<EmailTopic>();
        //    List<object> args = new List<object>();
        //    var Sql = @"SELECT DISTINCT USERNAME, EMAIL FROM V_NotificheTopic";
        //    try
        //    {
        //        using (IDatabase db = Connection)
        //        {
        //            ret = db.Query<EmailTopic>(Sql).ToList();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.Error(string.Format("GetAllEmailNotificaTopic.get() - ERRORE {0} {1}", e.Message, e.Source));
        //        return null;
        //    }
        //    return ret;
        //}

    }
}

﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class UsersBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public UsersBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // lettura email
        public string GetEmail(String userName)
        {

            var ret = "";
            List<object> args = new List<object>();

            var Sql = @"SELECT EMAIL FROM ASPNETUSERS WHERE UPPER(UserName) = @userName";
            args.Add(new { userName = userName.ToUpper() });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // lettura UserId
        public string GetUserId(String userName)
        {
            var ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT ID FROM ASPNETUSERS WHERE UserName = @userName";
            args.Add(new { userName = userName });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetUserId() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // lettura UserId
        public string GetUserName(String ID)
        {
            var ret = ID;
            List<object> args = new List<object>();
            var Sql = @"SELECT UserName FROM ASPNETUSERS WHERE ID = @Id";
            args.Add(new { Id = ID });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetUserName() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // lettura FullUserName
        public string GetFullName(String key)
        {
            if (string.IsNullOrWhiteSpace(key)) {
                return "";
            }

            var ret = key;
            List<object> args = new List<object>();
            var Sql = @"SELECT COALESCE(USERCFG.FULLNAME, ASPNETUSERS.USERNAME)
                          FROM ASPNETUSERS 
                          LEFT JOIN  USERCFG 
                          ON ASPNETUSERS.ID = USERCFG.USERID
                         WHERE UPPER(ASPNETUSERS.USERNAME) = @key OR UPPER(ASPNETUSERS.ID) = @key";
            args.Add(new { key = key.ToUpper() });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetFullName() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        public void SetNormalizedUserName(string userName)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE ASPNETUSERS SET NORMALIZEDUSERNAME = NORMALIZEDEMAIL  WHERE USERNAME = @userName";
            args.Add(new { userName = userName });

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SetNormalizedUserName() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;

        }

        public void SetLastChangePassword(string userName)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE USERCFG  SET TMSTLASTCHANGEPWD = GETDATE()
                          FROM USERCFG 
                          INNER JOIN ASPNETUSERS
				          ON USERCFG.USERID = ASPNETUSERS.ID
						  where ASPNETUSERS.USERNAME = @userName";
            args.Add(new { userName = userName });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SetLastChangePassword() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }

        public bool CheckPasswordExpired(String userName = "", string email = "")
        {
            bool ret = false;
            User user = null;
            if (!string.IsNullOrWhiteSpace(email))
            {
                user = this.Get(email: email).FirstOrDefault();
            }
            if (!string.IsNullOrWhiteSpace(userName))
            {
                user = this.Get(this.GetUserId(userName)).FirstOrDefault();
            }

            if (user != null)
            {
                //if (user.TmstLastChangePwd == null || user.TmstLastChangePwd < DateTime.Now.AddYears(-1))
                if (user.TmstLastChangePwd == null || user.TmstLastChangePwd < DateTime.Now.AddMonths(-6))
                {
                    ret = true;
                }
            }
            return ret;
        }


        public void setUserEmail(string userName, string email)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE ASPNETUSERS SET EMAIL = @email , NORMALIZEDEMAIL=@normalizedEmail WHERE USERNAME = @userName";
            args.Add(new { userName = userName });
            args.Add(new { email = email });
            args.Add(new { normalizedEmail = email.ToUpper() });

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                    SetNormalizedUserName(userName);
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.setUserEmail() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;

        }

        // elenco USER (SOLO QUELLI NON CANCELLATI)
        public List<User> Get(string id = null, string filterRoleName = "", string email = "", string supportForCommEntity = "",
                              string supportForCountry = "", bool ExcludeLocked = false, bool ExcludeDeleted = false, bool OnlyMaster = false)
        {
            var ret = new List<User>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASPNETUSERS.ID, ASPNETUSERS.USERNAME, ASPNETUSERS.EMAIL, 
                            IIF(ASPNETUSERS.LockoutEnd IS NULL, '',  'Locked') AS LOCKED,  
                            USERCFG.FULLNAME, USERCFG.TMSTLASTCHANGEPWD,
                            USERCFG.TMSTINSE, DEPARTMENTS.DESCRIPTION AS DEPARTMENT,
                            USERCFG.COMPANYNAME, USERCFG.TMSTLASTACTIVITY,
                            USERCFG.COUNTRY
                          FROM ASPNETUSERS
                          LEFT JOIN USERCFG 
				          ON USERCFG.USERID = ASPNETUSERS.ID
                          LEFT JOIN DEPARTMENTS 
						  ON DEPARTMENTS.DEPARTMENTID = USERCFG.DEPARTMENTID";
            if (!string.IsNullOrWhiteSpace(filterRoleName))
            {
                Sql += @"  INNER JOIN ASPNETUSERROLES
                            ON ASPNETUSERROLES.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETROLES 
                            ON ASPNETROLES.ID = ASPNETUSERROLES.RoleId
                            AND ASPNETROLES.Name =  @roleName";
                args.Add(new { roleName = filterRoleName });
            }
            if (!string.IsNullOrWhiteSpace(supportForCommEntity) || !string.IsNullOrWhiteSpace(supportForCountry))
            {
                // estrai solo gli utenti configurati come SUPPORT (il controllo sulla commercial Entity è fatto dopo)
                Sql += @"  INNER JOIN Departments AS DEP_SUPPORT
                            ON  USERCFG.DepartmentId = DEP_SUPPORT.DepartmentId
                            AND DEP_SUPPORT.DepartmentType = 'SUPPORT'";
            }
            Sql += " WHERE 1 = 1 ";

            if (!string.IsNullOrWhiteSpace(id))
            {
                Sql += " AND ID = @id";
                args.Add(new { id = id });
            }
            if (!string.IsNullOrWhiteSpace(email))
            {
                Sql += " AND UPPER(ASPNETUSERS.EMAIL) = @email";
                args.Add(new { email = email });
            }
            if (ExcludeLocked)
            {
                Sql += " AND ASPNETUSERS.LockoutEnd IS NULL";
            }
            if (ExcludeDeleted)
            {
                Sql += " AND USERCFG.DELETED = 0";
            }
            if (OnlyMaster)
            {
                Sql += " AND USERCFG.MASTER = 1";
            }

            if (!string.IsNullOrWhiteSpace(supportForCommEntity))
            {
                // configurato sulla Commercial Entity passata, oppure SENZA nessuna Commercial Entity configurate 
                Sql += @" 	AND (EXISTS (SELECT 1 FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCommerciale
					                        AND UPPER(USERLINKS.LINKID) = UPPER(@CommercialEntity)
					                        AND USERLINKS.USERID = ASPNETUSERS.ID)
		                            OR   (SELECT COUNT(*) FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCommerciale
					                        AND USERLINKS.USERID = ASPNETUSERS.ID) = 0)";
                args.Add(new { TypeIdCommerciale = (int)Lookup.UserLinkType.CommercialEntity });
                args.Add(new { CommercialEntity = supportForCommEntity.ToUpper() });
            }
            if (!string.IsNullOrWhiteSpace(supportForCountry))
            {
                // configurato sulla Country passata, oppure SENZA nessuna Country configurata
                Sql += @" 	AND (EXISTS (SELECT 1 FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCountry
					                        AND UPPER(USERLINKS.LINKID) = UPPER(@Country)
					                        AND USERLINKS.USERID = ASPNETUSERS.ID)
		                            OR   (SELECT COUNT(*) FROM USERLINKS 
					                        WHERE USERLINKS.TYPEID = @TypeIdCountry
					                        AND USERLINKS.USERID = ASPNETUSERS.ID) = 0)";
                args.Add(new { TypeIdCountry = (int)Lookup.UserLinkType.Country });
                args.Add(new { Country = supportForCountry.ToUpper() });
            }
            Sql += " ORDER BY USERNAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<User>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // lettura County (Default)
        public string GetDefaultCountry(String key)
        {
            if (string.IsNullOrWhiteSpace(key)) return "";

            var ret = key;
            List<object> args = new List<object>();
            var Sql = @"SELECT USERCFG.COUNTRY
                          FROM ASPNETUSERS 
                          LEFT JOIN  USERCFG 
                          ON ASPNETUSERS.ID = USERCFG.USERID
                         WHERE UPPER(ASPNETUSERS.USERNAME) = @key OR UPPER(ASPNETUSERS.ID) = @key";
            args.Add(new { key = key.ToUpper() });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetDefaultCountry() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }


        // elenco Email in ruolo
        public string GetEmailsInRole(string[] roles, string separator = ",", string ConfigId = "")
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASPNETUSERS.EMAIL
                            FROM ASPNETUSERS
                            INNER JOIN USERCFG 
	                            ON USERCFG.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETUSERROLES 
	                            ON ASPNETUSERROLES.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETROLES 
                                ON ASPNETROLES.ID = ASPNETUSERROLES.ROLEID
	                    WHERE ASPNETROLES.NAME IN (@roles)
                          AND ASPNETUSERS.LockoutEnd IS NULL";
            args.Add(new { roles = roles.Select(el => el.ToUpper()).ToList() });
            if (!string.IsNullOrWhiteSpace(ConfigId))
            {
                Sql += @" AND EXISTS (SELECT 1 FROM USERLINKS 
							    WHERE USERLINKS.TYPEID = @SupportType
							    AND USERLINKS.USERID = ASPNETUSERS.ID
							    AND USERLINKS.LINKID = @ConfigId)";
                args.Add(new { SupportType = (int)Lookup.UserLinkType.TKSupportType });
                args.Add(new { ConfigId = ConfigId });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    List<string> emails = new List<string>();
                    emails = db.Query<string>(Sql, args.ToArray()).ToList();
                    if (emails != null)
                    {
                        ret = string.Join(separator, emails);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetEmailsInRole.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco UserId in ruolo
        public string GetUsersInRole(string[] roles, string separator = ",", bool excludeLocked = false)
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT ASPNETUSERS.UserName
                            FROM ASPNETUSERS
                            INNER JOIN USERCFG 
	                            ON USERCFG.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETUSERROLES 
	                            ON ASPNETUSERROLES.USERID = ASPNETUSERS.ID
                            INNER JOIN ASPNETROLES 
                                ON ASPNETROLES.ID = ASPNETUSERROLES.ROLEID
	                    WHERE ASPNETROLES.NAME IN (@roles)";
            if (excludeLocked)
            {
                Sql += @" AND ASPNETUSERS.LockoutEnd IS NULL";
            }
            args.Add(new { roles = roles.Select(el => el.ToUpper()).ToList() });
            try
            {
                using (IDatabase db = Connection)
                {
                    List<string> users = new List<string>();
                    users = db.Query<string>(Sql, args.ToArray()).ToList();
                    if (users != null)
                    {
                        ret = string.Join(separator, users);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetUsersInRole.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // userCfg
        public UserCfg getUserCfg(string UserId)
        {
            UserCfg ret = new UserCfg();
            List<object> args = new List<object>();
            var Sql = @"SELECT * FROM USERCFG 
				          WHERE USERID = @UserId";
            args.Add(new { UserId = UserId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = (db.Query<UserCfg>(Sql, args.ToArray()).ToList()).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.getUserCfg() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco ROLES
        public List<String> GetRoles(string userId, string fieldName = "NAME")
        {
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT DISTINCT {fieldName} FROM ASPNETUSERROLES 
                            INNER JOIN ASPNETROLES
                            ON ASPNETROLES.ID = ASPNETUSERROLES.ROLEID
                            WHERE USERID = @userId 
                            ORDER BY 1";
            args.Add(new { userId = userId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<String>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetRoles() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // elenco LINKS
        public List<String> GetLinks(string key, Lookup.UserLinkType TypeId, string ExcludeId = "", bool bValueList = false)
        {
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT DISTINCT LINKID 
                          FROM USERLINKS 
                          INNER JOIN ASPNETUSERS 
                          ON ASPNETUSERS.ID = USERLINKS.USERID
                          WHERE (UPPER(ASPNETUSERS.USERNAME) = @key OR UPPER(ASPNETUSERS.ID) = @key)
                            AND USERLINKS.TYPEID = @TypeId";
            if (!string.IsNullOrWhiteSpace(ExcludeId))
            {
                Sql += @" AND UPPER(USERLINKS.LINKID) != @ExcludeId ";
                args.Add(new { ExcludeId = ExcludeId.ToUpper() });
            }
            args.Add(new { key = key.ToUpper() });
            args.Add(new { TypeId = (int)TypeId });
            Sql += @"  ORDER BY USERLINKS.LINKID";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<String>(Sql, args.ToArray()).ToList();
                    if (bValueList && ret.Count() > 1)
                    {
                        ret = new List<string>{ string.Join(" ", string.Join(", ", ret)) };
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetLinks() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // Elenco Utenti collegati ad un elemento 
        public List<UsersbyLinkId> getUsersByLinkId(List<string> linkIDs, Lookup.UserLinkType TypeId)
        {
            var ret = new List<UsersbyLinkId>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT DISTINCT COALESCE(USERCFG.FULLNAME, ASPNETUSERS.USERNAME) AS FULLNAME, 
                                         ASPNETUSERS.USERNAME AS USERNAME, 
                                         ASPNETUSERS.EMAIL AS EMAIL, 
                                         ASPNETUSERS.ID AS USERID
                            FROM USERLINKS 
                            INNER JOIN ASPNETUSERS 
                                ON ASPNETUSERS.ID = USERLINKS.USERID
	                        INNER JOIN USERCFG 
	                            ON USERCFG.USERID = ASPNETUSERS.ID
                            WHERE USERLINKS.LINKID IN (@listLinkdId)
                                AND USERLINKS.TYPEID = @TypeId
                                AND ASPNETUSERS.LockoutEnd IS NULL 
                                AND USERCFG.DELETED = 0 
                            ORDER BY 1";
            args.Add(new { listLinkdId = linkIDs.ToList() });
            args.Add(new { TypeId = (int)TypeId });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<UsersbyLinkId>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.getUsersByLinkId() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }

        // Aggiornamento LINKS
        public void UpdateLinks(string userId, Lookup.UserLinkType TypeId, List<String> links, string UserUpd)
        {
            var ret = new List<string>();
            List<object> args = new List<object>();

            string Sql = @"DELETE USERLINKS WHERE USERID = @UserId AND TYPEID = @TypeId";
            args.Add(new { UserId = userId });
            args.Add(new { TypeId = (int)TypeId });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.UpdateLinks()/DELETE - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }

            if (links !=  null)
            {
                foreach (var item in links)
                {
                    args.Clear();
                    Sql = @"INSERT INTO UserLinks(UserID, TypeId, LinkID, TmstLastUpd, UserLastUpd) 
                                VALUES (@UserId, @TypeId, @LinkId, GETDATE(), @UserUpd)";
                    args.Add(new { UserId = userId });
                    args.Add(new { TypeId = (int)TypeId });
                    args.Add(new { LinkId = item});
                    args.Add(new { UserUpd = UserUpd });
                    try
                    {
                        using (IDatabase db = Connection)
                        {
                            db.Execute(Sql, args.ToArray());
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.Error(string.Format("UsersBO.UpdateLinks()/INSERT - ERRORE {0} {1}", e.Message, e.Source));
                        throw;
                    }
                }
            }
            return;
        }

        // Aggiornamento LINKS
        public void AddUserLink(string userId, Lookup.UserLinkType TypeId, string links, string UserUpd)
        {
            // ????? DA RIVEDERE ???? 
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql = @"INSERT INTO UserLinks(UserID, TypeId, LinkID, TmstLastUpd, UserLastUpd) 
                        VALUES (@UserId, @TypeId, @LinkId, GETDATE(), @UserUpd)";
            args.Add(new { UserId = userId });
            args.Add(new { TypeId = (int)TypeId });
            args.Add(new { LinkId = links });
            args.Add(new { UserUpd = UserUpd });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.AddUserLink()/INSERT - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;
        }
        public void RemoveUserLink(string userId, Lookup.UserLinkType TypeId, string links)
        {
            // ????? DA RIVEDERE ???? 
            var ret = new List<string>();
            List<object> args = new List<object>();
            var Sql = @"DELETE FROM UserLinks 
                        WHERE UserID = @UserId 
                        AND   TypeId = @TypeId
                        AND   LinkID = @LinkId";
            args.Add(new { UserId = userId });
            args.Add(new { TypeId = (int)TypeId });
            args.Add(new { LinkId = links });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.RemoveUserLink()/INSERT - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;
        }

        public void ChangeStatus(string id)
        {
            List<object> args = new List<object>();
            string Sql = "";
            User user2upd = Get(id).FirstOrDefault();
            if (user2upd != null)
            {
                Sql = @"UPDATE ASPNETUSERS SET ";
                if (String.IsNullOrWhiteSpace(user2upd.locked))
                {
                    Sql += " LOCKOUTEND = @dataEnd";
                    args.Add(new { dataEnd = new DateTime(2099, 12, 31)});
                } else
                {
                    Sql += @" LOCKOUTEND = NULL";
                }
                Sql += "  WHERE ID = @id";
                args.Add(new { id = id });
            }

            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.ChangeStatus() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }

        // elenco RUOLI
        public List<Roles> GetAllRoles()
        {
            var ret = new List<Roles>();
         
            var Sql = @"SELECT ID, NAME, NOTE, PROGR FROM ASPNETROLES ORDER BY PROGR, NAME";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Roles>(Sql).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetAllRoles() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        
        public Roles GetRole(string Id)
        {
            Roles ret = new Roles();
            List<object> args = new List<object>();
            var Sql = @"SELECT ID, NAME, NOTE, PROGR FROM ASPNETROLES WHERE ID = @Id";
            args.Add(new { Id = Id });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Roles>(Sql, args.ToArray()).ToList().FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetRole() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }


        public void RemoteAllRoles(string id)
        {
            List<object> args = new List<object>();
            string Sql = @"DELETE ASPNETUSERROLES WHERE USERID = @id";
            args.Add(new { id = id });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.RemoteAllRoles() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }
        public void InsertRoles(string id, string RoleId)
        {
            List<object> args = new List<object>();
            string Sql = @"INSERT INTO ASPNETUSERROLES (USERID, ROLEID) VALUES (@id, @RoleId)";
            args.Add(new { id = id });
            args.Add(new { RoleId = RoleId });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.InsertRoles() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;

        }

        // isSupport
        public bool isSupport(string id)
        {
            bool ret = false;
            List<object> args = new List<object>();
            var Sql = @"SELECT ASPNETUSERS.USERNAME FROM ASPNETUSERS
                        INNER JOIN USERCFG 
                        ON USERCFG.USERID = ASPNETUSERS.ID
                        INNER JOIN DEPARTMENTS 
                        ON DEPARTMENTS.DEPARTMENTID = USERCFG.DEPARTMENTID
                        AND Departments.DepartmentType = 'SUPPORT'
                        WHERE UPPER(ASPNETUSERS.USERNAME) = @id OR UPPER(ASPNETUSERS.ID) = @id";
            args.Add(new { id = id });
            try
            {
                using (IDatabase db = Connection)
                {
                    var dati = db.Query<User>(Sql, args.ToArray()).ToList();
                    ret = dati.Count() > 0;
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.isSupport() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return ret;
        }

        public void SyncSessionAuthorization()
        {

            // ????? DA RIVEDERE ???? 

            // Rimuovo le abilitazioni date a tutte le sessioni che NON sono soggetto ad autorizzazione/profilatura
            List<object> args = new List<object>();
            string Sql = @"DELETE FROM USERLINKS 
                            WHERE TYPEID = @TypeIdDRSession
                            AND LINKID NOT IN ( SELECT DRSECTIONID FROM V_DRSECTIONAUTHORIZABLE)";
            args.Add(new { TypeIdDRSession = (int)Lookup.UserLinkType.DRSection });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SyncSessionAuthorization() - ERRORE {0} {1}", e.Message, e.Source));
                throw;
            }
            return;
        }

        public void SetLastActivity(string userName)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE USERCFG  SET TMSTLASTACTIVITY = GETDATE()
                              FROM USERCFG 
                              INNER JOIN ASPNETUSERS
                  ON USERCFG.USERID = ASPNETUSERS.ID
            where ASPNETUSERS.USERNAME = @userName";
            args.Add(new { userName = userName });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.SetLastActivity() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }

    }
}

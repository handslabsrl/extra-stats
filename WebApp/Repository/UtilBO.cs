﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Authorization;
using WebApp.Entity;
using Microsoft.AspNetCore.Http;

namespace WebApp.Repository
{
    public class UtilBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public UtilBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // Decodifica 
        public string GetCodeDescription(String CodeId)
        {
            if (String.IsNullOrWhiteSpace(CodeId))
            {
                return "<NoCode>";
            }
            var ret = string.Format("<{0}>", CodeId);
            List<object> args = new List<object>();
            var Sql = @"SELECT DESCRIPTION FROM CODES WHERE UPPER(CODEID) = @CodeId";
            args.Add(new { CodeId = CodeId.ToUpper() });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<string>(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // Sviluppo Distribution List
        public string GetDLEmails(int DLListId = 0, string DLType = "")
        {
            string  ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT EMAIL 
                          FROM V_DLMEMBERS
                         WHERE 1 = 1";
            if (DLListId > 0)
            {
                Sql += @" AND DLLISTID = @DLListId";
                args.Add(new { DLListId = DLListId });
            }
            if (!string.IsNullOrWhiteSpace(DLType))
            {
                Sql += @" AND DLTYPE = @DLType";
                args.Add(new { DLType = DLType });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    List<string> emails = new List<string>();
                    emails = db.Query<string>(Sql, args.ToArray()).ToList();
                    if (emails.Count > 0)
                    {
                        ret = string.Join(",", emails);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetDLEmails() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public List<DLMembersDTO> GetDLUsers(int DLListId)
        {
            List<DLMembersDTO> ret = new List<DLMembersDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT EMAIL, USERID, USERNAME, DLTYPE, DLLISTID
                          FROM V_DLMEMBERS
                         WHERE DLLISTID = @DLListId
                            AND USERID IS NOT NULL";
            args.Add(new { DLListId = DLListId });
            try
            {
                using (IDatabase db = Connection)
                {
                    List<string> emails = new List<string>();
                    ret = db.Query<DLMembersDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UsersBO.GetDLUsers() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }



        // HANGFIRE
        public int GetLastHangFireJobId()
        {
            int ret = 0;
            var Sql = @"SELECT MAX(Id) from HangFire.Job";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.ExecuteScalar<int>(Sql);
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("UtilBO.GetLastHangFireJobId() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return ret;
        }
        // Elenco elementi StdTable
        public List<LookUpString> GetStdTables(String CodTab, bool bExcludeDeleted = false, int? id = 0)
        {
            List<LookUpString> ret = new List<LookUpString>();
            List<object> args = new List<object>();
            var Sql = @"SELECT ID AS IDNUM,  Cod_ELe AS ID, Descriz AS DESCRIZIONE, ModelType as ATTR1 
                           FROM StdTables where Cod_Tab = @CodTab ";
            if (id > 0)
            {
                Sql += " AND ID = @Id";
                args.Add(new { Id = id });
            }
            if (bExcludeDeleted)
            {
                Sql += " AND COALESCE(FLAG_TRATT, '') != 'A'";
            }
            Sql+= " ORDER BY DESCRIZ";
            args.Add(new { CodTab = CodTab });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<LookUpString>(Sql, args.ToArray()).ToList();

                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetStdTables.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        // Elenco Siti di SGAT
        public List<LookUpString> GetSGATSites(String cliente)
        {
            List<LookUpString> ret = new List<LookUpString>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT COD_SITO as ID, CONCAT(Cod_sito, ' - ' , Rag_Sociale) as DESCRIZIONE FROM SITES WHERE COD_CLIFOR = @CliFor";
            Sql += " ORDER BY COD_SITO";
            args.Add(new { CliFor = cliente });

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<LookUpString>(Sql, args.ToArray()).ToList();

                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetSGATSites.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
    }
}


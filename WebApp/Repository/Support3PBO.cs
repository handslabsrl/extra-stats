﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Repository
{
    public class Support3PBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public Support3PBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public List<Index3PRequestDTO> GetRequests(string UserIdOwner, List<string> StatusIn)
        {
            var ret = new List<Index3PRequestDTO>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT REQ.TK3PREQUESTID, REQ.CODE, REQ.STATUS, 
                                REQ.TMSTINSE, REQ.USERINSE, REQ.CUSTOMERSELECTION,
                                COALESCE(USERCFG_REG.FULLNAME, REQ.USERINSE) AS USERINSE_NAME,
                                USERCFG_REG.COMPANYNAME AS USERINSE_COMPANYNAME,
                                REQ.TARGET, REQ.MANUFACTURER, REQ.PURCHASEORDER, REQ.TICKETID, 
                                REQ.FILENAMEAP, REQ.TMSTRELEASE, REQ.TMSTLASTDOWNLOAD, 
                                (SELECT COUNT(*) FROM TK3PATTACHMENTS  IFU
                                    WHERE IFU.TK3PREQUESTID = REQ.TK3PREQUESTID 
                                      AND IFU.DELETED = 0 
                                      AND IFU.TYPE = @TypeIFU) AS CNTIFU,
                                (SELECT COUNT(*) FROM TK3PATTACHMENTS  EVALFORM
                                    WHERE EVALFORM.TK3PREQUESTID = REQ.TK3PREQUESTID 
                                      AND EVALFORM.DELETED = 0 
                                      AND EVALFORM.TYPE = @TypeEvalForm) AS CNTEVALFORM,
                                COALESCE(SWVERSION.DESCRIZ, REQ.SWVERSION) AS SWVERSION_DESC
                        FROM TK3PREQUESTS REQ
                        INNER JOIN ASPNETUSERS USER_REG ON REQ.USERINSE = USER_REG.USERNAME
                        INNER JOIN USERCFG USERCFG_REG ON USER_REG.ID = USERCFG_REG.USERID
                        LEFT JOIN V_INGENIUSSWVERSIONS SWVERSION ON SWVERSION.COD_ELE = REQ.SWVERSION
                        WHERE REQ.STATUS NOT IN (@StatusNotIn)";
            string[] StatusNotIn = new string[] { Lookup.TK3PRequest_Status_DRAFT , Lookup.TK3PRequest_Status_DELETED };
            args.Add(new { StatusNotIn = StatusNotIn });
            args.Add(new { TypeIFU = (int)Lookup.TK3PKAttachType.IFU });
            args.Add(new { TypeEvalForm = (int)Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm });
            if (StatusIn.Count != 0)
            {
                Sql += " AND REQ.STATUS IN(@StatusIn)";
                args.Add(new { StatusIn = StatusIn });
            }
            if (!string.IsNullOrWhiteSpace(UserIdOwner))
            {
                // Estrazione dei soli ticket personali 
                Sql += " AND REQ.USERINSE = @UserName";
                args.Add(new { UserName = UserIdOwner });
            }
            Sql += " ORDER BY REQ.TK3PREQUESTID DESC";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<Index3PRequestDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequests.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        public List<TK3PAttachment> GetRequestAttachments(int requestId, Lookup.TK3PKAttachType FileType)
        {
            var ret = new List<TK3PAttachment>();
            List<object> args = new List<object>();
            var Sql = $@"SELECT TK3PAttachmentS.*, COALESCE(USERCFG.FULLNAME, TK3PAttachmentS.USERINSE) AS USERINSENAME
                           FROM TK3PAttachmentS  
                           INNER JOIN  ASPNETUSERS ON TK3PAttachmentS.USERINSE = ASPNETUSERS.USERNAME
                           INNER JOIN  USERCFG ON ASPNETUSERS.ID = USERCFG.USERID
                           WHERE TK3PREQUESTID = @requestId 
                             AND TK3PAttachmentS.DELETED = 0
                             AND TK3PAttachmentS.TYPE = @FileType";
            args.Add(new { requestId = requestId });
            args.Add(new { FileType = (int)FileType});
            Sql += " ORDER BY TK3PAttachmentS.TK3PAttachmentID";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<TK3PAttachment>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetRequestAttachments.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
    }
}

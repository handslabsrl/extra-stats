﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Authorization;
using WebApp.Entity;
using System.Text.RegularExpressions;

namespace WebApp.Repository
{
    public class InstrumentsBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public InstrumentsBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco strumenti di competenza (in base alla configurazione dell'utente passato)
        public List<InstrumentModel> GetInstruments(string CustomerCode = null, string SiteCode = null, string Country = "", string City = "",
                                                        string SerialNumber = "", string CommercialEntity = "", string Region = "", string Area = "",
                                                        string UserIdCfg = "", bool ExcludeDeleted = false, bool ExcludeDismiss = false,
                                                        bool CountWSPRequests = false, string WSPFilter = "", string modelType = "", 
                                                        bool MaintenanceInfo = false, bool GetAllModelTypes = false)
        {
            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            List<string> regionsCfg = new List<string>();
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                regionsCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Region);

                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    areaCfg = userCfg.Area;
                }
            }
            var ret = new List<InstrumentModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT INSTR.SERIALNUMBER, INSTR.COMPANYNAME, INSTR.SITE_DESCRIPTION, INSTR.COMMAND, INSTR.COMMANDDATERIF, 
                                        INSTR.SITE_CITY, INSTR.SITE_COUNTRY, INSTR.COMMERCIALENTITY, INSTR.REGION, INSTR.AREA, 
                                        INSTR.COMMERCIAL_STATUS AS COMMERCIALSTATUS, INSTR.WARRANTY_FROM, INSTR.WARRANTY_TO,
                                        INSTR.MACHINE_DESCRIPTION, INSTR.OWNERGROUPID,
                                        INSTR.MAINTENANCE_WARNINGAFTERGG, 
                                        INSTR.MODELTYPE, INSTR.BOOKING, INSTR.LOCKED,
                                        INSTR.CALIBRATION_GGNEXT, INSTR.CALIBRATION_WARNINGAFTERGG,
                                        INSTR.FIBERS_DEFAULT, INSTR.FIBERS_WARNINGAFTERPERC";
            if (CountWSPRequests)
            {
                // per l'utente, le richieste in PROCESSING e IN_WARRANTY_NO_REPLACEMENT, oppure COMPLIANT = YES, sono da considerare chiuse !!!
                Sql += @", (SELECT COUNT(*) FROM WSPRequests 
                             WHERE WSPRequests.SerialNumber = INSTR.SerialNumber 
                               AND IIF(WSPRequests.STATUS = @StatusProcessing AND 
                                               (COALESCE(WSPRequests.WARRANTYSTATUS, '') = @WarrantyNoReturn OR WSPRequests.COMPLIANT = @CompliantYES), 
                                        @StatusClosed, 
                                        WSPRequests.STATUS) in (@statiWarrantySparePart)) as OpenWSPRequests";
                args.Add(new { StatusProcessing = Lookup.WSPRequest_Status_PROCESSING });
                args.Add(new { WarrantyNoReturn = Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT });
                args.Add(new { StatusClosed = Lookup.WSPRequest_Status_CLOSED });
                args.Add(new { CompliantYES = Lookup.Code_WSP_Compliant_YES });

                String[] arr = new string[] { Lookup.WSPRequest_Status_PROCESSING, Lookup.WSPRequest_Status_PENDING };
                args.Add(new { statiWarrantySparePart = arr });
            }
            if (MaintenanceInfo)
            {
                Sql += @", INSTR.DT_LAST_MAINTENANCE, 
				           INSTR.DT_NEXT_MAINTENANCE, 
                           MCALIB.TMSTLASTCALIBRATION, 
				           MCALIB.TMSTNEXTCALIBRATION, 
				           MPACK.FIBERSNUMBER,
                           COALESCE((SELECT TOP(1) 1 FROM V_DLMembers 
            		                        INNER JOIN ASPNETUSERS OWNER ON OWNER.ID = V_DLMembers.USERID 
			                                WHERE V_DLMembers.DLListId = INSTR.OWNERGROUPID  
			                                AND  OWNER.UserName = @UserName), 0)  AS SHOWHISTORYMAINTENANCE";
                args.Add(new { UserName = (new UsersBO()).GetUserName(UserIdCfg) });
            }

            Sql += " FROM INSTRUMENTS INSTR";
            if (MaintenanceInfo)
            {
                Sql += @" LEFT JOIN MAINTENANCE MCALIB
                                ON MCALIB.SERIALNUMBER = INSTR.SERIALNUMBER AND MCALIB.MAINTENANCETYPE = @Calibration AND MCALIB.LASTREC = 1
                          LEFT JOIN Maintenance MPACK
                                ON MPACK.SERIALNUMBER = INSTR.SERIALNUMBER AND MPACK.MAINTENANCETYPE = @Pack AND MPACK.LASTREC = 1";
                args.Add(new { Calibration = Lookup.Instrument_MaintenanceType_Calibration });
                args.Add(new { Pack = Lookup.Instrument_MaintenanceType_Pack });
            }
            // filtri per configurazioni 
            Sql += " WHERE 1 = 1";
            
            // per default estraggo solo gli strumenti ELITE (inGenius e BeGenius)
            if (GetAllModelTypes == false)
            {
                Sql += @" AND UPPER(INSTR.MODELTYPE) IN (@ModelTypeEliteInstruments)";
                args.Add(new { ModelTypeEliteInstruments = Lookup.InstrumentModelType_EliteInstruments.Select(el => el.id.ToUpper()).ToList() });
            }

            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.SITE_COUNTRY) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(INSTR.AREA) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }
            // FILTRI 
            if (!String.IsNullOrWhiteSpace(CustomerCode))
            {
                Sql += " AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                String[] arr = CustomerCode.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { customersList = arr });
            }
            if (!String.IsNullOrWhiteSpace(SiteCode))
            {
                Sql += " AND UPPER(CONCAT(INSTR.CUSTOMERCODE, '_', INSTR.SITE_CODE)) IN (@siteCodeList)";
                String[] arr = SiteCode.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { siteCodeList = arr });
            }
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(INSTR.SITE_COUNTRY) IN (@countriesList)";
                String[] arr = Country.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { countriesList = arr });
            }
            if (!String.IsNullOrWhiteSpace(City))
            {
                Sql += " AND UPPER(INSTR.SITE_CITY) IN (@citiesList)";
                String[] arr = City.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { citiesList = arr });
            }
            if (!String.IsNullOrWhiteSpace(Region))
            {
                Sql += " AND UPPER(INSTR.REGION) IN (@regionsList)";
                String[] arr = Region.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { regionsList = arr });
            }
            if (!String.IsNullOrWhiteSpace(Area))
            {
                Sql += " AND UPPER(INSTR.AREA) = @Area";
                args.Add(new { Area = Area.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(SerialNumber))
            {
                Sql += " AND INSTR.SERIALNUMBER IN (@instrumentsList)";
                String[] arr = SerialNumber.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { instrumentsList = arr });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(INSTR.COMMERCIALENTITY) IN (@CommercialEntityList)";
                String[] arr = CommercialEntity.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { CommercialEntityList = arr });
            }
            if (ExcludeDeleted)
            {
                Sql += @" AND COALESCE(INSTR.COMMAND, '') <> @CommandDelete";   /// DA RIVEDERE ???? 
                args.Add(new { CommandDelete = "DELETE" });
            }
            if (ExcludeDismiss)
            {
                Sql += @" AND CHARINDEX('_DIS_', UPPER(INSTR.SERIALNUMBER)) = 0 ";  // DA RIVEDERE ??? 
            }
            if (!string.IsNullOrWhiteSpace(WSPFilter))
            {
                Sql += @" AND EXISTS(SELECT 1 FROM WSPREQUESTS
                                        WHERE WSPREQUESTS.SERIALNUMBER = INSTR.SERIALNUMBER
                                          AND WSPREQUESTS.STATUS in (@statiFiltroRichiesteStrumento))";
                String[] arr = new string[] { "UNDEF" };
                if (WSPFilter == Lookup.Code_WSP_InstrumentFilters_WithOpenRequests)
                {
                    arr = new string[] { Lookup.WSPRequest_Status_PROCESSING, Lookup.WSPRequest_Status_PENDING };
                }
                if (WSPFilter == Lookup.Code_WSP_InstrumentFilters_WithCloseRequests)
                {
                    arr = new string[] { Lookup.WSPRequest_Status_CLOSED };
                }
                if (WSPFilter == Lookup.Code_WSP_InstrumentFilters_WithAtLeast1Request)
                {
                    arr = new string[] { Lookup.WSPRequest_Status_PROCESSING, Lookup.WSPRequest_Status_PENDING, Lookup.WSPRequest_Status_CLOSED };
                }
                args.Add(new { statiFiltroRichiesteStrumento = arr });
            }
            if (!string.IsNullOrWhiteSpace(modelType))
            {
                Sql += @" AND UPPER(INSTR.MODELTYPE) IN (@modelTypeList)";
                String[] arr = modelType.Split(',');
                args.Add(new { modelTypeList = arr });
            }
            Sql += @" ORDER BY INSTR.SERIALNUMBER";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<InstrumentModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.GetInstruments() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public List<MaintenanceModel> GetMaintenance(string SerialNumber, string MaintenanceType, string UserNameInse)
        {
            var ret = new List<MaintenanceModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT MAIN.TmstInse as TmstInseMaintenance, 
                               MAIN.UserInse as UserInseMaintenance,
                               MAIN.TmstLastCalibration, 
                               MAIN.TmstNextCalibration, 
                               MAIN.FibersNumber,
                               MAIN.FibersNewPack, 
                               COALESCE(USERINSECFG.FULLNAME, MAIN.USERINSE) AS UserInseMaintenance_Name
                        FROM Maintenance MAIN
                        LEFT JOIN ASPNETUSERS USERINSE ON MAIN.USERINSE = USERINSE.USERNAME
                        LEFT JOIN USERCFG USERINSECFG ON USERINSE.ID = USERINSECFG.USERID
                        WHERE MAIN.SerialNumber = @SerialNumber
                          AND MAIN.MaintenanceType = @MaintenanceType";
            args.Add(new { SerialNumber = SerialNumber });
            args.Add(new { MaintenanceType = MaintenanceType });
            if (!string.IsNullOrWhiteSpace(UserNameInse))
            {
                Sql += " AND MAIN.USERINSE = @UserInse";
                args.Add(new { UserInse = UserNameInse });
            }
            Sql += " ORDER BY MAIN.TmstInse DESC ";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<MaintenanceModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.GetMaintenance() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        // Dati Instrument
        public InstrumentModel GetInstrument(string SerialNumber = "")
        {

            var ret = new InstrumentModel();
            List<object> args = new List<object>();
            var Sql = @"SELECT SERIALNUMBER, COMPANYNAME, SITE_DESCRIPTION, COMMAND, COMMANDDATERIF, SITE_CITY, SITE_COUNTRY, COMMERCIALENTITY,
                               WARRANTY_FROM, WARRANTY_TO, INSTRUMENT_MODEL, COMMERCIAL_STATUS AS COMMERCIALSTATUS, OWNERGROUPID, MACHINE_DESCRIPTION,
                               BOOKING, LOCKED, CUSTOMERCODE, SITE_CODE, ID_MAC, MODELTYPE, INSTALLATION_DATE
                          FROM INSTRUMENTS WHERE  SERIALNUMBER = @SerialNumber";
            args.Add(new { SerialNumber = SerialNumber });

            if (!string.IsNullOrWhiteSpace(SerialNumber))
            {
                try
                {
                    using (IDatabase db = Connection)
                    {
                        ret = db.Query<InstrumentModel>(Sql, args.ToArray()).FirstOrDefault();
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(string.Format("InstrumentsBO.GetInstrument() - ERRORE {0} {1}", e.Message, e.Source));
                    return null;
                }
            }
            return ret;
        }

        // elenco CommerialEntity 
        public List<CommercialEntityModel> GetCommercialEntities(string Country = "", string CommercialEntity = "", string UserIdCfg = "")
        {
            var ret = new List<CommercialEntityModel>();
            List<object> args = new List<object>();

            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            List<string> regionsCfg = new List<string>();
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                regionsCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Region);

                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    areaCfg = userCfg.Area;
                }
            }

            var Sql = @"SELECT DISTINCT UPPER(COMMERCIALENTITY) AS COMMERCIALENTITY FROM INSTRUMENTS";
            Sql += " WHERE COMMERCIALENTITY IS NOT NULL";

            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }

            // altri filtri 
            if (!String.IsNullOrWhiteSpace(Country))
            {
                //Sql += " AND UPPER(SITE_COUNTRY) = @Country";
                //args.Add(new { Country = Country.ToUpper() });
                // Separatori validi | oppure ,
                Sql += " AND UPPER(SITE_COUNTRY) IN (@CountryList)";
                var arr = Country.ToUpper().Replace("|",",").Split(',').ToList();
                args.Add(new { CountryList = arr });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(COMMERCIALENTITY) = @CommercialEntity";
                args.Add(new { CommercialEntity = CommercialEntity.ToUpper() });
            }
            Sql += " ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CommercialEntityModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetCommercialEntities.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
        // elenco Country 
        public List<CountryModel> GetCountries(string Country = "", string CommercialEntity = "")
        {

            var ret = new List<CountryModel>();
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT UPPER(SITE_COUNTRY) AS COUNTRY FROM INSTRUMENTS WHERE SITE_COUNTRY IS NOT NULL ";
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(SITE_COUNTRY) = @Country";
                args.Add(new { Country = Country.ToUpper() });
            }
            if (!String.IsNullOrWhiteSpace(CommercialEntity))
            {
                Sql += " AND UPPER(COMMERCIALENTITY) = @CommercialEntity";
                args.Add(new { CommercialEntity = CommercialEntity.ToUpper() });
            }
            Sql += " ORDER BY 1";
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CountryModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetCountries.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public void SetCommand(string SerialNumber, string Command, DateTime DateRif)
        {
            List<object> args = new List<object>();
            var Sql = @"UPDATE INSTRUMENTS SET COMMAND = @Command,
                                               COMMANDDATERIF = CONVERT(datetime, @DateRif, 103) 
                                      WHERE SERIALNUMBER = @SerialNumber";
            args.Add(new { Command = Command });
            args.Add(new { DateRif = ((DateTime)DateRif).ToString("dd/MM/yyyy") });
            args.Add(new { SerialNumber = SerialNumber });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.SetCommand() - ERRORE {0} {1}", e.Message, e.Source));
            }
            return;
        }

        // elenco Prenotazioni 
        public List<BookingDTO> GetBookings(DateTime? From = null, DateTime? To = null, string modelType = "", string userName = "", 
                                            int? BookingId = 0, string SerialNumber = "", int? BookingIdExclude= 0, bool checkOverlap = false, 
                                            string SerialNumberList = "")
        {
            var ret = new List<BookingDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT  BOOKINGS.*, 
                                COALESCE(USERINSECFG.FULLNAME, BOOKINGS.USERINSE) AS USERINSE_NAME,
                                COALESCE(USERAPPROVALCFG.FULLNAME, BOOKINGS.USERAPPROVAL) AS USERAPPROVAL_NAME,
                                INSTR.LOCKED,
                                IIF(BOOKINGS.USERINSE = @UserName, 1, 0) AS isEDITABLE, 
                        		COALESCE((SELECT TOP(1) 1 FROM V_DLMembers 
            		                            INNER JOIN ASPNETUSERS APPROVER ON APPROVER.ID = V_DLMembers.USERID 
			                                    WHERE V_DLMembers.DLListId = INSTR.OWNERGROUPID  
			                                    AND  APPROVER.UserName = @UserName), 0)  AS isAPPROVER
                        FROM BOOKINGS 
                        LEFT JOIN INSTRUMENTS INSTR ON INSTR.SerialNumber = Bookings.SerialNumber
                        LEFT JOIN ASPNETUSERS USERINSE ON BOOKINGS.USERINSE = USERINSE.USERNAME
                        LEFT JOIN USERCFG USERINSECFG ON USERINSE.ID = USERINSECFG.USERID
                        LEFT JOIN ASPNETUSERS USERAPPROVAL ON BOOKINGS.USERAPPROVAL = USERAPPROVAL.USERNAME
                        LEFT JOIN USERCFG USERAPPROVALCFG ON USERAPPROVAL.ID = USERAPPROVALCFG.USERID
                        WHERE INSTR.BOOKING = @Booking
                          AND BOOKINGS.STATUS <> @Deleted";
            args.Add(new { Booking = true });
            args.Add(new { UserName = userName });
            args.Add(new { Deleted = Lookup.Booking_Status_DELETED });
            if (BookingId != 0)
            {
                Sql += @" AND BOOKINGS.BOOKINGID = @BookingId";
                args.Add(new { BookingId = BookingId  });
            }
            if (BookingIdExclude != 0)
            {
                Sql += @" AND BOOKINGS.BOOKINGID <> @BookingIdExclude";
                args.Add(new { BookingIdExclude = BookingIdExclude });
            }
            if (!string.IsNullOrWhiteSpace(modelType))
            {
                Sql += @" AND UPPER(INSTR.MODELTYPE) = @ModelType";
                args.Add(new { ModelType = modelType.ToUpper() });
            }
            if (From != null && To != null) 
            {
                if (checkOverlap)
                {
                    Sql += @" AND  TmstFrom < @dateTo AND TmstTo > @dateFrom";   // Tutte le prenotazioni nel periodo di estraziones
                } else
                {
                    Sql += @" AND  TmstFrom <= @dateTo AND TmstTo >= @dateFrom";   // Tutte le prenotazioni nel periodo di estraziones
                }
                args.Add(new { dateFrom = ((DateTime)From) });
                args.Add(new { dateTo = ((DateTime)To) });
            }
            if (!string.IsNullOrWhiteSpace(SerialNumber))
            {
                Sql += @" AND UPPER(BOOKINGS.SERIALNUMBER) = @SerialNumber";
                args.Add(new { SerialNumber = SerialNumber.ToUpper() });
            }
            if (!string.IsNullOrWhiteSpace(SerialNumberList))
            { 
                Sql += @" AND UPPER(BOOKINGS.SERIALNUMBER) IN (@SerialNumberList)";
                args.Add(new { SerialNumberList = SerialNumberList.Split(",").ToList() });
            }
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<BookingDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("GetBookings.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        
        public List<RefurbishRequestDTO> GetRefurbishRequests(string UserIdCfg = "", string StatusIn = "", int refurbishRequestId = 0)
        {
            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            List<string> regionsCfg = new List<string>();
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                regionsCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Region);

                UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    areaCfg = userCfg.Area;
                }
            }
            var ret = new List<RefurbishRequestDTO>();
            List<object> args = new List<object>();
            var Sql = @"SELECT REQ.REFURBISHREQUESTID,  REQ.STATUS,  REQ.SERIALNUMBER,  REQ.TMSTINSE, 
                                  REQ.USERINSE,  REQ.TMSTLASTUPD,  REQ.USERLASTUPD, 
                                  REQ.ASSETNUMBER, REQ.RESIDUALVALUE, REQ.RESIDUALVALUEDATE, 
                                  REQ.FINALDESTINATION, REQ.NEEDEDBYDATE, REQ.ADDITIONALOPERATIONS, REQ.NOTE, 
                                  REQ.RDANUMBER, REQ.ENTRYDATE, REQ.PROCESSINGSTARTDATE, REQ.PROCESSINGENDDATE, 
                                  REQ.SGATWONUMBER, REQ.PROCESSINGCOST, REQ.SPAREPARTSCOST, 
                                  COALESCE(USERCFG.FULLNAME, REQ.USERINSE) AS USERINSENAME,
                                  INSTR.MODELTYPE, INSTR.COMPANYNAME, INSTR.SITE_DESCRIPTION, 
                                  INSTR.SITE_CITY, INSTR.SITE_COUNTRY, INSTR.COMMERCIALENTITY, INSTR.CUSTOMERCODE, 
                                  INSTR.SITE_CODE, INSTR.REGION, INSTR.AREA, INSTR.COMMERCIAL_STATUS";
            Sql += @" FROM REFURBISHREQUESTS AS REQ 
                        INNER JOIN INSTRUMENTS INSTR
                            ON INSTR.SERIALNUMBER = REQ.SERIALNUMBER 
                            AND COALESCE(INSTR.COMMAND, '') <> @CommandDelete
                        LEFT JOIN  ASPNETUSERS ON REQ.USERINSE = ASPNETUSERS.USERNAME
                        LEFT JOIN  USERCFG ON ASPNETUSERS.ID = USERCFG.USERID";
            args.Add(new { CommandDelete = "DELETE" });

            Sql += @" WHERE REQ.STATUS <> @ReqDelete ";
            args.Add(new { ReqDelete = Lookup.Refurbish_Status_DELETED });

            if (refurbishRequestId > 0)
            {
                Sql += @" AND REQ.REFURBISHREQUESTID = @refurbishRequestId ";
                args.Add(new { refurbishRequestId = refurbishRequestId });
            }
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.SITE_COUNTRY) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(INSTR.REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(INSTR.AREA) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }
            if (!string.IsNullOrWhiteSpace(StatusIn))
            {
                Sql += @" AND REQ.STATUS IN (@StatusList)";
                args.Add(new { StatusList = StatusIn.Split(",") });
            }
            //// estraggo solo gli strumenti ELITE (inGenius e BeGenius)
            //Sql += @" AND UPPER(INSTR.MODELTYPE) IN (@ModelTypeEliteInstruments)";
            //args.Add(new { ModelTypeEliteInstruments = Lookup.InstrumentModelType_EliteInstruments.Select(el => el.id.ToUpper()).ToList() });

            Sql += @" ORDER BY INSTR.SERIALNUMBER";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<RefurbishRequestDTO>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("InstrumentsBO.GetRefurbishRequests() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }

        public string GetSupplierEmails(string SerialNumber)
        {
            string ret = "";
            List<object> args = new List<object>();
            var Sql = @"SELECT TOP 1 CONTACTS from SERVICECENTERS 
                            INNER JOIN INSTRUMENTS 
                                ON SERVICECENTERS.SERVICECENTERCODE = INSTRUMENTS.INSTRUMENT_MODEL
                                AND SERVICECENTERS.DELETED = 0
                          WHERE INSTRUMENTS.SERIALNUMBER = @SerialNumber";
            args.Add(new { SerialNumber = SerialNumber });

            if (!string.IsNullOrWhiteSpace(SerialNumber))
            {
                try
                {
                    string contacts = "";
                    List<string> emails = new List<string>();
                    using (IDatabase db = Connection)
                    {
                        contacts = db.Query<string>(Sql, args.ToArray()).FirstOrDefault();
                    }
                    // estrai tutte le email valide 
                    Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase);
                    MatchCollection emailMatches = emailRegex.Matches(contacts);
                    foreach (Match emailMatch in emailMatches)
                    {
                        emails.Add(emailMatch.Value);
                    }
                    ret = string.Join(',', emails);
                }
                catch (Exception e)
                {
                    _logger.Error(string.Format("InstrumentsBO.GetInstrument() - ERRORE {0} {1}", e.Message, e.Source));
                    return null;
                }
            }
            return ret;
        }
    }
}


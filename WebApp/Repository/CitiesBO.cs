﻿using System.Collections.Generic;
using System.Linq;
using NPoco;
using System.Data.SqlClient;
using System;
using NLog;
using WebApp.Models;
using WebApp.Classes;

namespace WebApp.Repository
{
    public class CitiesBO
    {

        private static string connectionString;
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public CitiesBO()
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // elenco CITY
        public List<CityModel> GetCities(String Country = null, string UserIdCfg = "")
        {
            var ret = new List<CityModel>();
            List<object> args = new List<object>();
            List<string> countriesCfg = new List<string>();
            List<string> customersCfg = new List<string>();
            List<string> commercialEntitiesCfg = new List<string>();
            List<string> regionsCfg = new List<string>();
            string areaCfg = "";
            if (!string.IsNullOrWhiteSpace(UserIdCfg))
            {
                countriesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Country);
                customersCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Customer);
                commercialEntitiesCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.CommercialEntity);
                regionsCfg = (new UsersBO()).GetLinks(UserIdCfg, Lookup.UserLinkType.Region);

                Entity.UserCfg userCfg = (new UsersBO()).getUserCfg(UserIdCfg);
                if (userCfg != null)
                {
                    areaCfg = userCfg.Area;
                }
            }
            var Sql = @"SELECT DISTINCT SITE_CITY, SITE_COUNTRY FROM INSTRUMENTS 
                                WHERE SITE_CITY IS NOT NULL ";

            // filtri per configurazione Utente 
            if (countriesCfg.Count > 0)
            {
                Sql += @" AND UPPER(Site_Country) IN (@countriesList)";
                args.Add(new { countriesList = countriesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (customersCfg.Count > 0)
            {
                Sql += @" AND UPPER(CUSTOMERCODE) IN (@customersList)";
                args.Add(new { customersList = customersCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (commercialEntitiesCfg.Count > 0)
            {
                Sql += @" AND UPPER(COMMERCIALENTITY) IN (@commercialEntitiesList)";
                args.Add(new { commercialEntitiesList = commercialEntitiesCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (regionsCfg.Count > 0)
            {
                Sql += @" AND UPPER(REGION) IN (@regionsList)";
                args.Add(new { regionsList = regionsCfg.Select(el => el.ToUpper()).ToList() });
            }
            if (!String.IsNullOrWhiteSpace(areaCfg))
            {
                Sql += " AND UPPER(Area) = @AreaCfg";
                args.Add(new { AreaCfg = areaCfg.ToUpper() });
            }

            // Altri filtri 
            if (!String.IsNullOrWhiteSpace(Country))
            {
                Sql += " AND UPPER(SITE_COUNTRY) IN (@countriesList)";
                String[] arr = Country.ToUpper().Replace("|", ",").Split(',');
                args.Add(new { countriesList = arr });
            }
            Sql += @" ORDER BY SITE_CITY";

            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<CityModel>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.Error(string.Format("CitiesBO.Get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return ret;
        }
    }
}

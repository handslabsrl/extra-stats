﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Data
    {
    public class WebAppDbContext : DbContext
        {

        //public WebAppDbContext()
        //    {

        //    }

        public WebAppDbContext(DbContextOptions<WebAppDbContext> options)
        : base(options)
            {
            }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
            {

            // Default
            //modelBuilder.Entity<Testata>()
            //    .Property(b => b.CaratteriPerRiga)
            //    .HasDefaultValue(0);
            //modelBuilder.Entity<Testata>()
            //    .Property(b => b.UscitaDom)
            //    .HasDefaultValue(false);
            //modelBuilder.Entity<Cliente>()
            //    .Property(b => b.Frazionamento)
            //    .HasDefaultValue(1);
            //modelBuilder.Entity<Cliente>()
            //   .Property(b => b.TipoArea)
            //   .HasDefaultValue("S");

            // Integrità referenziale
            modelBuilder.Entity<Quotation>()
                        .HasOne(e => e.CommercialEntity)
                        .WithMany()
                        .HasForeignKey(e => e.CommercialEntityID)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Quotation>()
                        .HasOne(e => e.InstrumentType)
                        .WithMany()
                        .HasForeignKey(e => e.InstrumentTypeID)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<QuotationElitePCRKit>()
                        .HasOne(e => e.ElitePCRKit)
                        .WithMany()
                        .HasForeignKey(e => e.ElitePCRKitId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<QuotationExtraction>()
                        .HasOne(e => e.Extraction)
                        .WithMany()
                        .HasForeignKey(e => e.ExtractionId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Training>()
                        .HasOne(t => t.TrainingType)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingTypeId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Training>()
                        .HasOne(t => t.Hotel)
                        .WithMany()
                        .HasForeignKey(t => t.HotelId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingRegistration>()
                        .HasOne(t => t.Training)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingRegistration>()
                        .HasOne(t => t.Country)
                        .WithMany()
                        .HasForeignKey(t => t.CountryId)
                        .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TrainingLog>()
                        .HasOne(t => t.Training)
                        .WithMany()
                        .HasForeignKey(t => t.TrainingId)
                        .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Training>()
               .Property(b => b.Reserved)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.Accomodation)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.SendNews)
               .HasDefaultValue(false);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.Enabled)
               .HasDefaultValue(true);
            modelBuilder.Entity<ClaimTemperature>()
               .Property(b => b.ValueGet)
               .HasDefaultValue(0);
            modelBuilder.Entity<TrainingRegistration>()
               .Property(b => b.hotelPaid)
               .HasDefaultValue(false);
            modelBuilder.Entity<DRSection>()
                .Property(b => b.Locked)
                .HasDefaultValue(false);
            modelBuilder.Entity<DRResource>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<DRTopic>()
                .Property(b => b.NotifyChange)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKClassification>()
                .Property(b => b.Reserved)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKClassification>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKAttachment>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKAddInfo>()
                .Property(b => b.Reserved)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKAddInfo>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKEvent>()
                .Property(b => b.Reserved)
                .HasDefaultValue(false);
            modelBuilder.Entity<UserCfg>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<TKClosingInfo>()
                .Property(b => b.Deleted)
                .HasDefaultValue(false);
            modelBuilder.Entity<ClaimMessage>()
                .Property(b => b.MessageType)
                .HasDefaultValue(0);
            modelBuilder.Entity<Assay>()
                .Property(b => b.Validated)
                .HasDefaultValue(false);
            modelBuilder.Entity<Assay>()
                .Property(b => b.StatoManleva)
                .HasDefaultValue(Lookup.Assay_StatoManleva.DaCreare);

            //modelBuilder.Entity<TipoPubblicita>()
            //            .HasOne(e => e.Testata)
            //            .WithMany()
            //            .HasForeignKey(e => e.testataId)
            //            .OnDelete(DeleteBehavior.Cascade);

            // ColumnType/Precision 
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet01).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet02).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet03).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet04).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet05).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet06).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet07).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet08).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet09).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet10).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet11).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet12).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueGet13).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp01).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp02).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp03).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp04).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp05).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp06).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp07).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp08).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp09).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp10).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp11).HasColumnType("decimal(18, 6)");
            modelBuilder.Entity<ClaimFluo>().Property(b => b.ValueTemp12).HasColumnType("decimal(18, 6)");


            // Chiavi composite
            modelBuilder.Entity<ClaimCheckCfg>().HasKey(table => new
                                                { table.Script, table.CheckName, table.CheckType });
            modelBuilder.Entity<UserLink>().HasKey(table => new
                                                { table.UserID, table.TypeId, table.LinkID });
            modelBuilder.Entity<DRSectionPaths>().HasKey(table => new
                                                { table.DRSectionIdRef, table.DRSectionId });
            modelBuilder.Entity<AssayDTO>().HasKey(table => new
                                                { table.AssayId, table.AssayVersionId});
            modelBuilder.Entity<TKClassificationPath>().HasKey(table => new
            { table.TKClassificationIdRef, table.TKClassificationId });
            modelBuilder.Entity<TKAddInfoPath>().HasKey(table => new
            { table.TKAddInfoIdRef, table.TKAddInfoId });
            modelBuilder.Entity<TKClosingInfoPath>().HasKey(table => new
            { table.TKClosingInfoIdRef, table.TKClosingInfoId });
            modelBuilder.Entity<ClaimSession>().HasKey(table => new
            { table.ClaimId, table.TmstRif, table.SessionNumber});
            modelBuilder.Entity<LetterAssay>().HasKey(table => new
            { table.LetterId, table.AssayCod });
            modelBuilder.Entity<TKEventRead>().HasKey(table => new
            { table.TKRequestId, table.UserInse, table.TmstInse });

            // Indici 
            //modelBuilder.Entity<QuotationElitePCRKit>()
            //            .HasIndex(t => new { t.Quotation.QuotationId, t.ElitePCRKitId });
            modelBuilder.Entity<UploadFile>()
                        .HasIndex(t => new { t.SerialNumber, t.FileType});
            modelBuilder.Entity<DataMining>()
                        .HasIndex(t => new { t.UserInse });
            //modelBuilder.Entity<ClaimScript>()
            //            .HasIndex(t => new { t.Claim, t.Script});
            modelBuilder.Entity<TrainingLog>()
                        .HasIndex(t => new { t.TrainingId, t.EventId });
            modelBuilder.Entity<DRTracking>()
                        .HasIndex(t => new { t.UserInse, t.ExtType, t.ExtId});
            modelBuilder.Entity<TKRequest>()
                        .HasIndex(t => new { t.Code });
            modelBuilder.Entity<WSPRequest>()
                        .HasIndex(t => new { t.SerialNumber, t.Status});
            modelBuilder.Entity<TK3PRequest>()
                        .HasIndex(t => new { t.Code });

            // Creato da script !!! 
            //modelBuilder.Entity<ClaimFluo>()
            //            .HasIndex(t => new { t.Claim.ClaimId, t.TmstRif, t.SessionNumber })
            //            .HasName("IX_ClaimFluo_ClaimId");

        }

        // *******************************************************************************************+
        // IMPORTANTE: le seguenti tabelle NON hanno la classe corrispondente:
        //              DATAMAIN
        //              INSTRUMENTS
        //              GASSAYPROGRAMS
        //              PRODUCTS
        // *******************************************************************************************+

        public DbSet<CommercialEntity> CommercialEntity { get; set; }
        public DbSet<InstrumentType> InstrumentType { get; set; }
        public DbSet<ElitePCRKit> ElitePCRKits { get; set; }
        public DbSet<Quotation> Quotations { get; set; }
        public DbSet<Extraction> Extractions { get; set; }
        public DbSet<QuotationElitePCRKit> QuotationElitePCRKits { get; set; }
        public DbSet<QuotationOpenPCRKit> QuotationOpenPCRKits { get; set; }
        public DbSet<QuotationExtraction> QuotationExtractions { get; set; }
        public DbSet<QuotationsTotal> QuotationsTotal { get; set; }
        public DbSet<QuotationsTotalPCR> QuotationsTotalPCR { get; set; }

        public DbSet<UploadFile> UploadFiles { get; set; }
        
        public DbSet<UserCfg> UserCfg { get; set; }
        public DbSet<UserLink> UserLinks { get; set; }

        public DbSet<Claim> Claims { get; set; }
        public DbSet<ClaimRow> ClaimRows { get; set; }
        public DbSet<ClaimTemperature> ClaimTemperatures { get; set; }
        public DbSet<ClaimPressure> ClaimPressures { get; set; }
        public DbSet<ClaimTrack> ClaimTracks { get; set; }
        public DbSet<ClaimSnapshot> ClaimSnapshots { get; set; }
        public DbSet<ClaimScript> ClaimScripts { get; set; }
        public DbSet<ClaimPressureSN> ClaimPressuresSN { get; set; }
        public DbSet<ClaimCheckCfg> ClaimCheckCfgs { get; set; }
        public DbSet<ClaimFluo> ClaimFluo { get; set; }
        public DbSet<ClaimMessage> ClaimMessages { get; set; }
        public DbSet<ClaimSession> ClaimSessions { get; set; }

        public DbSet<ClaimPressureRecType> ClaimPressureRecTypes { get; set; }

        public DbSet<ErrorCode> ErrorCodes { get; set; }

        public DbSet<DataMining> DataMining { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Version> Versions { get; set; }

        public DbSet<Instrument> Instruments { get; set; }

        public DbSet<TrainingType> TrainingTypes { get; set; }
        public DbSet<TrainingLocation> TrainingLocations { get; set; }
        public DbSet<TrainingJobPosition> TrainingJobPositions { get; set; }
        public DbSet<TrainingTypeOfParticipant> TrainingTypeOfParticipants { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<TrainingRegistration> TrainingRegistrations { get; set; }
        public DbSet<TrainingLog> TrainingLogs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates{ get; set; }
        public DbSet<Company> Companies { get; set; }

        public DbSet<ProcessRequest> ProcessRequests { get; set; }

        public DbSet<DRSection> DRSections { get; set; }
        public DbSet<DRSectionPaths> DRSectionPaths { get; set; }
        public DbSet<DRTopic> DRTopics { get; set; }
        public DbSet<DRResource> DRResources{ get; set; }
        public DbSet<DRTracking> DRTrackings { get; set; }

        public DbSet<TKClassification> TKClassifications { get; set; }
        public DbSet<TKClassificationPath> TKClassificationPaths { get; set; }
        public DbSet<TKRequest> TKRequests { get; set; }
        public DbSet<TKAttachment> TKAttachments { get; set; }
        public DbSet<TKEvent> TKEvents { get; set; }
        public DbSet<TKAddInfo> TKAddInfos { get; set; }
        public DbSet<TKAddInfoPath> TKAddInfoPaths { get; set; }
        public DbSet<TKClosingInfo> TKClosingInfos { get; set; }
        public DbSet<TKClosingInfoPath> TKClosingInfoPaths { get; set; }
        public DbSet<TKNotification> TKNotifications { get; set; }
        public DbSet<TKEventRead> TKEventReads { get; set; }
        public DbSet<TKToDoItem> TKToDoItems { get; set; }

        public DbSet<TK3PRequest> TK3PRequests { get; set; }
        public DbSet<TK3PAttachment> TK3PAttachments { get; set; }
        public DbSet<TK3PEvent> TK3PEvents { get; set; }

        public DbSet<Booking> Bookings { get; set; }
        public DbSet<RefurbishRequest> RefurbishRequests { get; set; }
        public DbSet<RefurbishEvent> RefurbishEvents { get; set; }

        public DbSet<Assay> Assay { get; set; }
        public DbSet<AssayVersion> AssayVersion { get; set; }
        public DbSet<AssayDTO> v_Assay { get; set; }

        public DbSet<WSPRequest> WSPRequests { get; set; }
        public DbSet<WSPEvent> WSPEvents { get; set; }
        public DbSet<WSPAttachment> WSPAttachments { get; set; }

        public DbSet<Code> Codes { get; set; }

        public DbSet<DLList> DLLists { get; set; }
        public DbSet<DLMember> DLMembers { get; set; }

        public DbSet<Letter> Letters { get; set; }
        public DbSet<LetterAssay> LetterAssays { get; set; }

        public DbSet<WMessage> WMessages { get; set; }

        public DbSet<TabGenConfig> TabGenConfigs { get; set; }

        public DbSet<V_Training> V_Trainings { get; set; }
        public DbSet<V_InGeniusSWVersion> V_InGeniusSWVersions { get; set; }

        public object UserRoles { get; internal set; }

        public DbSet<ServiceCenter> ServiceCenters { get; set; }
        public DbSet<Site> Sites { get; set; }
    }
}

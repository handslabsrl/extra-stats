﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
   
    public class TabGenConfig
    {
        public int TabGenConfigId { get; set; }

        [StringLength(20)]
        public string IdTab { get; set; }
        [StringLength(300)]
        public string DescTab  { get; set; }

        public bool EnabledISR { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

    }
}




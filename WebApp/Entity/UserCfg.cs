﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class UserCfg
    {
        [Key]
        public string UserID { get; set; }

        // Attibuti USER
        [StringLength(250)]
        public string Country { get; set; }                 // --> sostituita con Userlink (tipo = 0)
        [StringLength(50)]
        public string Region { get; set; }                 // --> sostituita con Userlink (tipo = 4)
        [StringLength(50)]
        public string Area { get; set; }
        //public int? CommercialEntityID { get; set; }
        [StringLength(100)]
        public string CommercialEntity { get; set; }            //  --> sostituita con Userlink (tipo = 1)
        [StringLength(450)]
        public string UserApproval1 { get; set; }
        [StringLength(450)]
        public string UserApproval2 { get; set; }
        public DateTime? TmstConfirmCfg { get; set; }
        [StringLength(100)]
        public string fullName { get; set; }
        public int? DepartmentId { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public DateTime? TmstLastChangePwd { get; set; }
        public DateTime? TmstInse { get; set; }

        [StringLength(200)]
        public string CompanyName { get; set; }
        [StringLength(100)]
        public string City { get; set; }

        public bool? Deleted { get; set; }     // utente cancellato (default = false)
        public bool? Master { get; set; }     // utente riferimento da cui copiare ruoli e configurazioni 
        public DateTime? TmstLastActivity { get; set; }

    }
}




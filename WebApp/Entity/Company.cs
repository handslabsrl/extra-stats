﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    // Elenco COMPANY normalizzate (da proporre come suggerimento nella modifica della registrazione)
    public class Company
    {
        [StringLength(200)]
        [Display(Name = "Company")]
        public string CompanyId { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}



﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
    {
    [ModelBinder(BinderType = typeof(DataSourceLoadOptionsBinder))]
    public class DataSourceLoadOptionsExtraStats : DataSourceLoadOptionsBase
        {
        }

    public class DataSourceLoadOptionsBinder : IModelBinder
        {

        public Task BindModelAsync(ModelBindingContext bindingContext)
            {
            var loadOptions = new DataSourceLoadOptionsExtraStats();
            DataSourceLoadOptionsParser.Parse(loadOptions , key => bindingContext.ValueProvider.GetValue(key).FirstOrDefault());
            bindingContext.Result = ModelBindingResult.Success(loadOptions);
            return Task.CompletedTask;
            }

        }
    }

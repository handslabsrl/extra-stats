﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class TrainingJobPosition
    {
        [StringLength(100)]
        [Display(Name = "Job Position")]
        public string Id { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}



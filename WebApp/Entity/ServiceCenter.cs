﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Entity
{
    public class ServiceCenter
    {
        public int ServiceCenterId { get; set; }
        [StringLength(50)]
        public string ServiceCenterCode { get; set; }
        public bool Deleted { get; set; }
        [StringLength(500)]
        public string Note { get; set; }
        [StringLength(2000)]
        public string Contacts { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}

    
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class Instrument
    {
        [Key]
        public string SerialNumber { get; set; }
        public bool? Booking { get; set; }
        public bool? Locked { get; set; }
        public int? OwnerGroupId { get; set; } // Referente/Proprietario strumento -> id tabella DLLists
        public int? Maintenance_WarningAfterGG { get; set; }
        public int? Calibration_GGNext { get; set; }
        public int? Calibration_WarningAfterGG { get; set; }
        public int? Fibers_Default { get; set; }
        public decimal? Fibers_WarningAfterPerc { get; set; }
        [NotMapped]
        public string BookingDeco
        {
            get
            {
                string ret = "";
                if (this.Booking != null)
                {
                    ret = (bool)this.Booking ? "Yes" : "No";
                }
                return ret;
            }
        }
        [NotMapped]
        public string LockedDeco
        {
            get
            {
                string ret = "";
                if (this.Locked != null)
                {
                    ret = (bool)this.Locked ? "Yes" : "No";
                }
                return ret;
            }
        }

    }

    //BOOKING
    public class Booking
    {

        public int BookingId { get; set; }
        public string Status { get; set; }
        public string Title { get; set; }
        public bool EntireDay { get; set; }
        public string SerialNumber { get; set; }
        public string Note { get; set; }
        public DateTime TmstFrom { get; set; }
        public DateTime TmstTo { get; set; }
        public DateTime TmstInse { get; set; }
        public string UserInse { get; set; }
        public DateTime? TmstApproval { get; set; }
        public string UserApproval { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        public string UserLastUpd { get; set; }

        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
        public string TmstLastUpdSort
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("yyyyMMddHHmmss");
                }
                return ret;
            }
        }
        public string DataLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string TmstApprovalDeco
        {
            get
            {
                string ret = "";
                if (this.TmstApproval != null)
                {
                    ret = ((DateTime)this.TmstApproval).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.Booking_Status_WAITING))
                        deco = Lookup.Booking_StatusDeco_WAINING;
                    if (this.Status.Equals(Lookup.Booking_Status_CONFIRMED))
                        deco = Lookup.Booking_StatusDeco_CONFIRMED;
                    if (this.Status.Equals(Lookup.Booking_Status_DELETED))
                        deco = Lookup.Booking_StatusDeco_DELETED;
                }
                return deco;
            }
        }
        public string TmstFromDecoUCT
        {
            get { return this.TmstFrom.ToString("dd/MM/yyyy HH:mm"); }
        }
        public string TmstToDecoUCT
        {
            get { return this.TmstTo.ToString("dd/MM/yyyy HH:mm"); }
        }
        public string TmstFromDeco
        {
            get { return this.TmstFrom.AddHours(1).ToString("dd/MM/yyyy HH:mm"); }
        }
        public string TmstToDeco
        {
            get { return this.TmstTo.AddHours(1).ToString("dd/MM/yyyy HH:mm"); }
        }
        public string SignLastUpd
        {
            get
            {
                return string.Format("{0}-{1}", this.UserLastUpd, this.TmstLastUpdSort);
            }
        }
    }

    //REFURBISH 
    public class RefurbishRequest
    {
        public int RefurbishRequestId { get; set; }
        public string Status { get; set; }
        public string SerialNumber { get; set; }
        public DateTime TmstInse { get; set; }
        public string UserInse { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        public string UserLastUpd { get; set; }

        [StringLength(100)]
        public string AssetNumber { get; set; }
        public decimal? ResidualValue { get; set; }
        public DateTime? ResidualValueDate { get; set; }
        [StringLength(100)]
        public string FinalDestination { get; set; }
        public DateTime? NeededByDate { get; set; }
        [StringLength(500)]
        public string AdditionalOperations { get; set; }
        [StringLength(500)]
        public string Note { get; set; }
        [StringLength(100)]
        public string RDANumber { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? ProcessingStartDate { get; set; }
        public DateTime? ProcessingEndDate { get; set; }
        [StringLength(100)]
        public string SGATWONumber { get; set; }
        public decimal? ProcessingCost { get; set; }
        public decimal? SparePartsCost { get; set; }

        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm"); }
        }
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
        public string TmstLastUpdSort
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("yyyyMMddHHmmss");
                }
                return ret;
            }
        }
        public string DataLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.Refurbish_Status_WAITING))
                        deco = Lookup.Refurbish_StatusDeco_WAITING;
                    if (this.Status.Equals(Lookup.Refurbish_Status_PROCESSING))
                        deco = Lookup.Refurbish_StatusDeco_PROCESSING;
                    if (this.Status.Equals(Lookup.Refurbish_Status_COMPLETE))
                        deco = Lookup.Refurbish_StatusDeco_COMPLETE;
                    if (this.Status.Equals(Lookup.Refurbish_Status_READY))
                        deco = Lookup.Refurbish_StatusDeco_READY;
                    if (this.Status.Equals(Lookup.Refurbish_Status_CLOSED))
                        deco = Lookup.Refurbish_StatusDeco_CLOSED;
                }
                return deco;
            }
        }
        public string ResidualValueDateDeco
        {
            get
            {
                string ret = "";
                if (this.ResidualValueDate != null)
                {
                    ret = ((DateTime)this.ResidualValueDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string NeededByDateDeco
        {
            get
            {
                string ret = "";
                if (this.NeededByDate != null)
                {
                    ret = ((DateTime)this.NeededByDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string EntryDateDeco
        {
            get
            {
                string ret = "";
                if (this.EntryDate != null)
                {
                    ret = ((DateTime)this.EntryDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string ProcessingStartDateDeco
        {
            get
            {
                string ret = "";
                if (this.ProcessingStartDate != null)
                {
                    ret = ((DateTime)this.ProcessingStartDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string ProcessingEndDateDeco
        {
            get
            {
                string ret = "";
                if (this.ProcessingEndDate != null)
                {
                    ret = ((DateTime)this.ProcessingEndDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }

    }
    public class RefurbishEvent
    {
        public long RefurbishEventId { get; set; }

        [Required]
        public int RefurbishRequestId { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        public string Note { get; set; }

        [StringLength(30)]
        public string Operation { get; set; }

        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
    }
}


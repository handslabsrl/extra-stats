﻿using System;

namespace WebApp.Entity
{
    public class WMessage
    {
        public int WMessageId { get; set; }
        public string Note { get; set; }
        public DateTime? TmstFrom { get; set; }
        public DateTime? TmstTo { get; set; }
        public bool Deleted { get; set; }
        public string UserInse { get; set; }
        public DateTime? TmstInse { get; set; }
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

    }
}

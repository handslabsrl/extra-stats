﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class TrainingTypeOfParticipant
    {
        [StringLength(100)]
        [Display(Name = "Type Of Participant")]
        public string Id { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [Display(Name = "Italian Participant")]
        public bool Italian { get; set; }
    }
}



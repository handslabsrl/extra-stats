﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace WebApp.Entity
{
    public class TrainingType
    {

        private readonly string DirTemplates = "Attachments\\TrainingTypes\\{0}";

        public int TrainingTypeId { get; set; }

        [StringLength(200)]
        [Required]
        public string Description { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public bool viewTrainingPortfolio { get; set; }

        public bool GenerateCertificate { get; set; }

        [NotMapped]
        public bool CertificateTemplateExist
        {
            get
            {
                string fullFileName = this.CertificateTemplateFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.CertificateTemplateFullFileName);
            }
        }

        [NotMapped]
        public string CertificateTemplateFileName
        {
            get
            {
                return "CertificateTemplate.pdf";
                //// leggo il primo file *.pdf nella cartella 
                //if (Directory.Exists(this.CertificateTemplatePathName))
                //{
                //    var allFilenames = Directory.EnumerateFiles(this.CertificateTemplatePathName, "*.pdf", SearchOption.TopDirectoryOnly);
                //    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                //    if (candidates.Count() > 0)
                //    {
                //        return Path.GetFileName(candidates.First());
                //    }
                //}
                //return null;
            }
        }
        [NotMapped]
        public string CertificateTemplatePathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirTemplates, this.TrainingTypeId));
            }
        }
        [NotMapped]
        public string CertificateTemplateFullFileName
        {
            get
            {
                if (String.IsNullOrEmpty(this.CertificateTemplateFileName))
                {
                    return null;
                }
                return System.IO.Path.Combine(this.CertificateTemplatePathName, this.CertificateTemplateFileName);
            }
        }

        [NotMapped]
        public bool TrainingPortfolioExist
        {
            get
            {
                string fullFileName = this.TrainingPortfolioFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.TrainingPortfolioFullFileName);
            }
        }

        [NotMapped]
        public string TrainingPortfolioFileName
        {
            get
            {
                return "TrainingPortfolio.pdf";
                //// leggo il primo file *.pdf nella cartella 
                //if (Directory.Exists(this.TrainingPortfolioPathName))
                //{
                //    var allFilenames = Directory.EnumerateFiles(this.TrainingPortfolioPathName, "*.pdf", SearchOption.TopDirectoryOnly);
                //    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                //    if (candidates.Count() > 0)
                //    {
                //        return Path.GetFileName(candidates.First());
                //    }
                //}
                //return null;
            }
        }
        [NotMapped]
        public string TrainingPortfolioPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirTemplates, this.TrainingTypeId));
            }
        }
        [NotMapped]
        public string TrainingPortfolioFullFileName
        {
            get
            {
                if (String.IsNullOrEmpty(this.TrainingPortfolioFileName))
                {
                    return null;
                }
                return System.IO.Path.Combine(this.TrainingPortfolioPathName, this.TrainingPortfolioFileName);
            }
        }

        [NotMapped]
        public bool AgendaExist
        {
            get
            {
                string fullFileName = this.AgendaFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.AgendaFullFileName);
            }
        }

        [NotMapped]
        public string AgendaFileName
        {
            get
            {
                return "Agenda.pdf";
                //// leggo il primo file *.pdf nella cartella 
                //if (Directory.Exists(this.AgendaPathName))
                //{
                //    var allFilenames = Directory.EnumerateFiles(this.AgendaPathName, "*.pdf", SearchOption.TopDirectoryOnly);
                //    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                //    if (candidates.Count() > 0)
                //    {
                //        return Path.GetFileName(candidates.First());
                //    }
                //}
                //return null;
            }
        }
        [NotMapped]
        public string AgendaPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirTemplates, this.TrainingTypeId));
            }
        }
        [NotMapped]
        public string AgendaFullFileName
        {
            get
            {
                if (String.IsNullOrEmpty(this.AgendaFileName))
                {
                    return null;
                }
                return System.IO.Path.Combine(this.AgendaPathName, this.AgendaFileName);
            }
        }

    }
}


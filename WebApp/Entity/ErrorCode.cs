﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
{
    public class ErrorCode
    {
        [StringLength(50)]
        public string ErrorCodeId { get; set; }

        [StringLength(200)]
        public string Message { get; set; }
        [StringLength(100)]
        public string Category { get; set; }
        [StringLength(100)]
        public string ErrorType { get; set; }
        [StringLength(100)]
        public string ErrorUnit { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [StringLength(4000)]
        public string ErrorHandling { get; set; }

    }
}




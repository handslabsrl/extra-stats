﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace WebApp.Entity
{

    public class TrainingLocation
    {
        private readonly string DirTemplates = "Attachments\\Locations";

        [StringLength(200)]
        [Display(Name = "Location")]
        public string Id { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [NotMapped]
        public bool IndicationsExist
        {
            get
            {
                string fullFileName = this.IndicationsFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.IndicationsFullFileName);
            }
        }

        [NotMapped]
        public string IndicationsFileName
        {
            get
            {
                string ret = string.Format("Indications_{0}.pdf", this.Id.ToUpper().Replace(" ", "_")).Replace("_-_", "_");
                ret = string.Join("_", ret.Split(Path.GetInvalidFileNameChars()));
                return ret;
                //// leggo il primo file *.pdf nella cartella 
                //if (Directory.Exists(this.IndicationsPathName))
                //{
                //    var allFilenames = Directory.EnumerateFiles(this.IndicationsPathName, "*.pdf", SearchOption.TopDirectoryOnly);
                //    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                //    if (candidates.Count() > 0)
                //    {
                //        return Path.GetFileName(candidates.First());
                //    }
                //}
                //return null;
            }
        }
        [NotMapped]
        public string IndicationsPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirTemplates);
            }
        }
        [NotMapped]
        public string IndicationsFullFileName
        {
            get
            {
                if (String.IsNullOrEmpty(this.IndicationsFileName))
                {
                    return null;
                }
                return System.IO.Path.Combine(this.IndicationsPathName, this.IndicationsFileName);
            }
        }
    }
}



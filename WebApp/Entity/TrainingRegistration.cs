﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApp.Classes;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace WebApp.Entity
{
    public class TrainingRegistration
    {
        // ....\<Id Training>\<Id registation>
        private readonly string DirAttachmentsPassportScan = "Attachments\\Trainings\\{0}\\{1}";
        private readonly string DirAttachmentsInvLetterScan = "Attachments\\Trainings\\{0}\\{1}";
        private readonly string DirAttachmentsCertificate = "Attachments\\Trainings\\{0}\\{1}";


        public long TrainingRegistrationId  { get; set; }

        [Required]
        public int TrainingId { get; set; }
        public virtual Training Training { get; set; }

        [Required]
        [StringLength(200)]
        public string Name  { get; set; }
        [Required] 
        [StringLength(200)]
        public string Surname { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(50)]
        [Phone]
        [Display(Name = "Mobile Number")]
        //[RegularExpression("^(?!0+$)(\\+\\d{1,3}[- ]?)?(?!0+$)\\d{10,15}$", ErrorMessage = "Please enter valid Mobile Number")]
        public string MobileNumber { get; set; }

        [StringLength(200)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [StringLength(100)]
        [Display(Name = "Company Address")]
        public string CompanyAddress { get; set; }
        [StringLength(10)]
        [Display(Name = "Address Nr")]
        public string CompanyAddressNr { get; set; }
        [StringLength(100)]
        [Display(Name = "City")]
        public string CompanyAddressCity { get; set; }
        [StringLength(10)]
        [Display(Name = "ZipCode")]
        public string CompanyAddressZipCode { get; set; }

        [StringLength(100)]
        [Display(Name = "Job Position")]
        public string JobPosition { get; set; }  // lookup su listJobPosition

        [StringLength(100)]
        [Required]
        [Display(Name = "Country")]
        public string CountryId { get; set; }
        public virtual Country Country { get; set; }
        [Required]
        public Lookup.TrainigVISA VISA { get; set; }

        [StringLength(250)]
        [Display(Name = "Passport Scan")]
        public string PassportScan { get; set; }    // Nome file Scansione Passaporto (in \Attachment)

        [Display(Name = "Accomodation")]
        // NULLABLE per evitare il msg di DevExtreme di campo obbligatorio
        public bool? Accomodation { get; set; }     // indica se è richiesta la prenotazione all'Hotel (se previsto)
        public DateTime? CheckIn { get; set; }
        public DateTime? CheckOut { get; set; }

        // NULLABLE per evitare il msg di DevExtreme di campo obbligatorio
        public bool? SendNews { get; set; }  

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(20)]
        public string Status { get; set; }

        public DateTime? TmstCreate { get; set; }

        [StringLength(10)]
        public string Title { get; set; }

        public DateTime? tmstSendReservationHotel { get; set; }  // inviata email prenotazione
        public DateTime? tmstReservationHotelConfirmed { get; set; }
        [StringLength(50)]
        public string ReservationNumber { get; set; }

        [StringLength(250)]
        public string InvLetterScan { get; set; }    // Nome file Scansione Invitation Letter (in \Attachment)
        public DateTime? tmstSendInvitationLetter { get; set; }     // lo scan della lettera è in \Attachment !!

        public DateTime? tmstAnswerRegistration { get; set; }     // serve per tracciare la prima risposta sulla registrazione nel passaggio 
                                                                  // dallo stato NEW a ACCEPTED o REFUSED
        [Required]
        [StringLength(100)]
        [Display(Name = "Type of participant")]
        public string TypeOfParticipant { get; set; }       // lookup  su TrainingTypeOfParticipant

        [Display(Name = "Hotel paid by EGSPA")]
        public bool? hotelPaid { get; set; }

        // attivo per lista partecipanti 
        public bool? Enabled { get; set; }


        [NotMapped]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2}", this.Title, this.Name, this.Surname).Trim();
            }
        }

        [NotMapped]
        public string FullCompanyAddress
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", this.CompanyAddress, this.CompanyAddressNr, this.CompanyAddressCity, this.CompanyAddressZipCode).Trim();
            }
        }


        [NotMapped]
        public bool bCanEdit
        {
            get
            {
                if (this.Training == null) return false;
                HashSet<string> statiEdit = new HashSet<string>();
                statiEdit.Add(Lookup.Training_Status_NEW);
                statiEdit.Add(Lookup.Training_Status_PUBLISHED);
                statiEdit.Add(Lookup.Training_Status_CLOSED);
                return statiEdit.Contains(this.Training.Status);
            }
        }

        [NotMapped]
        public string AccomodationStatus
        {
            get
            {
                string ret = Lookup.TrainingRegistration_Accomodation_NOT_REQUIRED;
                if (this.Accomodation != null)
                {
                    if ((bool)this.Accomodation)
                    {
                        ret = Lookup.TrainingRegistration_Accomodation_REQUIRED;
                        /* sequenza di stati:
                         * - REQUIRED e richiesta ACCOMODATION 
                         * - AWAITING se è stata mandata la email all'hotel per la reservation  
                         * - CONFIRMED se l'hotel ha risposto ed è stato inserito il ReservationNumber
                         * - COMPLETED se è stata mandata la email al richiedente con la conferma della prenotazione dell'hotel
                         */
                        if (this.tmstSendReservationHotel != null)
                        {
                            ret = Lookup.TrainingRegistration_Accomodation_AWAITING;                        
                        }
                        if (!string.IsNullOrWhiteSpace(this.ReservationNumber))
                        {
                            ret = Lookup.TrainingRegistration_Accomodation_CONFIRMED;
                        }
                        if (this.tmstReservationHotelConfirmed != null)
                        {
                            ret = Lookup.TrainingRegistration_Accomodation_COMPLETED;
                        }
                    }
                }
                return ret;
            }
        }
        [NotMapped]
        public string VISAStatus
        {
            get
            {
                /* sequenza di stati:
                 * - NOT REQUIRED se non richiesta oppure se 
                 * - NOT REQUIRED se richiesta VISA ma non richiesta invitation letter (ex NOT NECESSARY)
                 * - REQUIRED se richiesta ma manca ancora il caricamento della scansione dell'Invitation Lettere
                 * - NOT SENT email con scansione da spedire 
                 * - SENT se email spedita 
                 */
                string ret = Lookup.TrainingRegistration_VISA_NOT_REQUIRED;
                if (this.VISA == Lookup.TrainigVISA.yes_no_letter)
                {
                    //ret = Lookup.TrainingRegistration_VISA_NOT_NECESSARY;
                    ret = Lookup.TrainingRegistration_VISA_NOT_REQUIRED;
                }
                if (this.VISA == Lookup.TrainigVISA.yes_with_letter)
                {
                    ret = Lookup.TrainingRegistration_VISA_REQUIRED;
                    if (!String.IsNullOrWhiteSpace(this.InvLetterScan))
                    {
                        ret = Lookup.TrainingRegistration_VISA_NOT_SENT;
                    }
                    if (this.tmstSendInvitationLetter != null)
                    {
                        ret = Lookup.TrainingRegistration_VISA_SENT;
                    }
                }
                return ret;
            }
        }

        [NotMapped]
        public string DecorationStatus
        {
            get
            {
                string ret = "statusRegistration-TO-DO";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    if (this.Status.Equals(Lookup.TrainingRegistration_Status_REFUSED))
                    {
                        ret = "statusRegistration-REFUSED";
                    }
                    if (this.Status.Equals(Lookup.TrainingRegistration_Status_ACCEPTED))
                    {
                        ret = "statusRegistration-DONE";
                    }
                }
                return ret;
            }
        }

        [NotMapped]
        public string DecorationAccomodationStatus
        {
            get
            {
                string ret = "statusRegistration-NOT-REQUIRED";
                if (this.AccomodationStatus.Equals(Lookup.TrainingRegistration_Accomodation_REQUIRED))
                {
                    ret = "statusRegistration-TO-DO";
                }
                if (this.AccomodationStatus.Equals(Lookup.TrainingRegistration_Accomodation_AWAITING) ||
                    this.AccomodationStatus.Equals(Lookup.TrainingRegistration_Accomodation_CONFIRMED))
                {
                    ret = "statusRegistration-WAIT";
                }
                if (this.AccomodationStatus.Equals(Lookup.TrainingRegistration_Accomodation_COMPLETED))
                {
                    ret = "statusRegistration-DONE";
                }
                return ret;
            }
        }

        [NotMapped]
        public string DecorationVisaStatus
        {
            get
            {
                string ret = "statusRegistration-NOT-REQUIRED";
                if (this.VISAStatus.Equals(Lookup.TrainingRegistration_VISA_NOT_SENT) || 
                    this.VISAStatus.Equals(Lookup.TrainingRegistration_VISA_REQUIRED))
                {
                    ret = "statusRegistration-TO-DO";
                }
                if (this.VISAStatus.Equals(Lookup.TrainingRegistration_VISA_SENT))
                {
                    ret = "statusRegistration-DONE";
                }
                return ret;
            }
        }


        [NotMapped]
        public string DecorationCertificationStatus
        {
            get
            {
                string ret = "statusRegistration-TO-DO";
                // ??? 
                //if (this.Acc.Equals(Lookup.TrainingRegistration_Status_REFUSED))
                //{
                //    ret = "statusRegistration-REFUSED";
                //}


                return ret;
            }
        }

        [NotMapped]
        public bool PassportScanExist
        {
            get
            {
                string fullFileName = this.PassportScanFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.PassportScanFullFileName);
            }
        }

        [NotMapped]
        public string PassportScanFileName
        {
            get
            {
                //if (string.IsNullOrWhiteSpace(this.PassportScan))
                //{
                //    return "";
                //}
                //return string.Format("{0}_{1}", this.TrainingRegistrationId, this.PassportScan);
                return string.Format("{0}", this.PassportScan);
            }
        }
        [NotMapped]
        public string PassportScanPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachmentsPassportScan, this.TrainingId, this.TrainingRegistrationId));
            }
        }
        [NotMapped]
        public string PassportScanFullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.PassportScanPathName, this.PassportScanFileName);
            }
        }

        [NotMapped]
        public string TmstCreateDeco
        {
            get
            {
                if (this.TmstCreate == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstCreate).ToString("dd MMM yyyy HH:mm:ss");
            }
        }

        [NotMapped]
        public string CheckInDeco
        {
            get
            {
                if (this.CheckIn == null)
                {
                    return "";
                }
                return ((DateTime)this.CheckIn).ToString("dd/MM/yyyy");
            }

        }

        [NotMapped]
        public string CheckOutDeco
        {
            get
            {
                if (this.CheckOut == null)
                {
                    return "";
                }
                return ((DateTime)this.CheckOut).ToString("dd/MM/yyyy");
            }
        }


        [NotMapped]
        public string tmstSendReservationHotelDeco
        {
            get
            {
                if (this.tmstSendReservationHotel == null) return "";
                return ((DateTime)this.tmstSendReservationHotel).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string tmstAnswerRegistrationdDeco
        {
            get
            {
                if (this.tmstAnswerRegistration == null) return "";
                return ((DateTime)this.tmstAnswerRegistration).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string tmstReservationHotelConfirmedDeco
        {
            get
            {
                if (this.tmstReservationHotelConfirmed == null) return "";
                return ((DateTime)this.tmstReservationHotelConfirmed).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        [NotMapped]
        public bool InvLetterScanExist
        {
            get
            {
                string fullFileName = this.InvLetterScanFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.InvLetterScanFullFileName);
            }
        }

        [NotMapped]
        public string InvLetterScanFileName
        {
            get
            {
                //if (string.IsNullOrWhiteSpace(this.InvLetterScan))
                //{
                //    return "";
                //}
                //return string.Format("{0}_{1}", this.TrainingRegistrationId, this.InvLetterScan);
                return string.Format("{0}", this.InvLetterScan);
            }
        }
        [NotMapped]
        public string InvLetterScanPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachmentsInvLetterScan, this.TrainingId, this.TrainingRegistrationId));
            }
        }
        [NotMapped]
        public string InvLetterScanFullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.InvLetterScanPathName, this.InvLetterScanFileName);
            }
        }

        [NotMapped]
        public string tmstSendInvitationLetterDeco
        {
            get
            {
                if (this.tmstSendInvitationLetter == null) return "";
                return ((DateTime)this.tmstSendInvitationLetter).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        [NotMapped]
        public bool CertificateExist
        {
            get
            {
                string fullFileName = this.CertificateFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.CertificateFullFileName);
            }
        }

        [NotMapped]
        public string CertificateFileName
        {
            get
            {
                // leggo il primo file Certificate*.pdf nella cartella 
                if (Directory.Exists(this.CertificatePathName))
                {
                    var allFilenames = Directory.EnumerateFiles(this.CertificatePathName, "Certificate*.pdf", SearchOption.TopDirectoryOnly);
                    var candidates = allFilenames.Where(fn => Path.GetExtension(fn) == ".pdf");
                    if (candidates.Count() > 0)
                    {
                        return Path.GetFileName(candidates.First());
                    }
                }
                return null;
                //return "Certificate.Pdf";
            }
        }
        [NotMapped]
        public string CertificatePathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachmentsCertificate, this.TrainingId, this.TrainingRegistrationId));
            }
        }
        [NotMapped]
        public string CertificateFullFileName
        {
            get
            {
                if (String.IsNullOrEmpty(this.CertificateFileName)) {
                    return null;
                }
                return System.IO.Path.Combine(this.CertificatePathName, this.CertificateFileName);
            }
        }

    }
}


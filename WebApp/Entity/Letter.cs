﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;
using System.IO;
using WebApp.Repository;
using NPoco;

namespace WebApp.Entity
{
    public class Letter
    {
        public int LetterId { get; set; }
        public string SerialNumber { get; set; }
        public string CustomerCode { get; set; }
        public string LetterType { get; set; }
        public string FileName { get; set; }
        public string SignedFileName { get; set; }
        public string SignedUser { get; set; }
        public DateTime? SignedTmst { get; set; }
        public DateTime TmstInse { get; set; }
    }

    public class LetterAssay
    {
        public int LetterId { get; set; }
        public string AssayCod { get; set; }
        public bool Disabled { get; set; }
        public DateTime TmstInse { get; set; }
    }

}

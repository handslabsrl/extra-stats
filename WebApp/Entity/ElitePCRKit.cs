﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class ElitePCRKit
    {
        /* Questa tabella contiene i KIT ELITePCR e i CONSUMABLE (Type=9) */


        public int ID { get; set; }

        [Required, StringLength(30)]
        [Display(Name = "PCR Target")]
        public string PCRTarget { get; set; }
        [Required, StringLength(50)]
        [Display(Name = "Ref (product p/n)")]
        public string ProductPartNumber { get; set; }

        [Required, StringLength(100)]
        public string Description { get; set; }

        [Required]
        public Lookup.ElitePCRKitType  PCRType { get; set; }
        [Required]
        public Lookup.ExtractionType ExtractionType { get; set; }
        [Required]
        public Lookup.AnalysisType AnalysisType { get; set; }

        public int? ggExpiry { get; set; }
        public int? KitSize { get; set; }

        public decimal? StandardCost { get; set; }
        public decimal? OfficialPrice { get; set; }

        // ?? DA RIMUOVERE 
        [Range(0, 100)]
        public decimal? NoCompensation { get; set; }
        [Range(0, 100)]
        public decimal? Compensation { get; set; }

        // Compensation
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Threshold 1")]
        public int? Threshold1 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 1")]
        public decimal? Compensation1 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Threshold 2")]
        public int? Threshold2 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 2")]
        public decimal? Compensation2 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Threshold 3")]
        public int? Threshold3 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 3")]
        public decimal? Compensation3 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Threshold 4")]
        public int? Threshold4 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 4")]
        public decimal? Compensation4 { get; set; }
        public int? Threshold5 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 5")]
        public decimal? Compensation5 { get; set; }
        public int? Threshold6 { get; set; }
        [DisplayFormat(DataFormatString = "{0}", ApplyFormatInEditMode = true), Display(Name = "Compensation 6")]
        public decimal? Compensation6 { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public int? MultipleReaction { get; set; }
        public int? MinTheoreticalCons { get; set; }
        public int? CutOff { get; set; }

        [NotMapped]
        public int MultipleReactionNormalized
        {
            get
            {
                int ret = this.MultipleReaction ?? 0;
                if (ret <= 0) { ret = 1; }
                return ret;
            }
        }
    }
}




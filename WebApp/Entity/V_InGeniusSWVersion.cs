﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class V_InGeniusSWVersion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Cod_Ele { get; set; }
        public string Descriz { get; set; }
        public string Flag_Tratt { get; set; }
    }
}




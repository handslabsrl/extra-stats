﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Entity
{
    public class EmailTemplate
    {
        [Required]
        [StringLength(30)]
        [Display(Name = "Email Code")]
        public string EmailTemplateId { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }
        [Display(Name = "Message")]
        public string Body { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(200)]
        [Display(Name = "Description (reserved)")]
        public string Description { get; set; }

        [StringLength(30)]
        public string Area { get; set; }

        // valorizzata dalla funzione GetEmailInfo
        [NotMapped]
        public string To { get; set; }
        [NotMapped]
        public long IdRif { get; set; }
        [NotMapped]
        public string CC { get; set; }
        [NotMapped]
        public string IdRifCode { get; set; }
        [NotMapped]
        public string[] attach { get; set; }
        [NotMapped]
        public string ReplyTo { get; set; }

    }
}

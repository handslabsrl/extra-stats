﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Entity
{
    public class DLList
    {
        public int DLListId { get; set; }
        [StringLength(200)]
        public string Description { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public bool Deleted { get; set; }

        [StringLength(10)]
        public string DLType { get; set; }  // Decodifica su CODES

        public virtual ICollection<DLMember> Members { get; set; }

        [NotMapped]
        public string DLTypeDesc { get; set; }
        [NotMapped]
        public bool ExternalMembers { get; set; }

    }

    public class DLMember
    {
        public int DLMemberId{ get; set; }

        [Required]
        public int DLListId { get; set; }
        public virtual DLList DLList { get; set; }

        [StringLength(256)]
        public string UserId { get; set; }  // se non valorizzato, significa che è un membro esterno 

        [StringLength(100)]
        public string ExtName { get; set; }
        [StringLength(256)]
        public string ExtEmail { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(10)]
        public string Status { get; set; }  // '' valido, altri valori da valutare !!! 


    }
}






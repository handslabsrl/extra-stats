﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;
using System.IO;

namespace WebApp.Entity
{

    public class TK3PRequest
    {
        private readonly string DirAttachments = "Attachments\\TK3PRequest\\{0}";
        private readonly string DirTemplateMod1923 = "Attachments\\TK3PRequest\\Template";

        public int TK3PRequestId { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
        public string SwVersion { get; set; }
        public string CustomerSelection { get; set; }
        public DateTime TmstInse { get; set; }
        public string UserInse { get; set; }
        public string Target { get; set; }
        public string Manufacturer { get; set; }
        public string PurchaseOrder { get; set; }
        public int? TicketId { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        public string UserLastUpd { get; set; }
        public string FileNameAP { get; set; }
        public DateTime? TmstRelease { get; set; }
        public DateTime? TmstLastDownload { get; set; }
 

        // DA DEFINIRE ????
        // Versione ??? --> Codice della versione ?? 
        // CustomerCode 
        // InGenius Serial Number
        // InGenius Location
        // Assay Compatiblity Evaluation Form: versione scaricata (ultima versione?), file compilato, user/data upload 


        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }
        [NotMapped]
        public string DateInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy"); }
        }
        [NotMapped]
        public string DateInseSort
        {
            get { return this.TmstInse.ToString("yyyyMMdd"); }
        }
        [NotMapped]
        public string DateReleaseDeco
        {
            get { return TmstRelease == null ? "" : ((DateTime)this.TmstRelease).ToString("dd/MM/yyyy"); }
        }
        [NotMapped]
        public string DateReleaseSort
        {
            get { return TmstRelease == null ? "" : ((DateTime)this.TmstRelease).ToString("yyyyMMdd"); }
        }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_DRAFT))
                        deco = Lookup.TK3PRequest_StatusDeco_DRAFT;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_PENDING))
                        deco = Lookup.TK3PRequest_StatusDeco_PENDING;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_PROCESSING))
                        deco = Lookup.TK3PRequest_StatusDeco_PROCESSING;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_READY))
                        deco = Lookup.TK3PRequest_StatusDeco_READY;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_CLOSED))
                        deco = Lookup.TK3PRequest_StatusDeco_CLOSED;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_REFUSED))
                        deco = Lookup.TK3PRequest_StatusDeco_REFUSED;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_DELETED))
                        deco = Lookup.TK3PRequest_StatusDeco_DELETED;
                    if (this.Status.Equals(Lookup.TK3PRequest_Status_RELEASED))
                        deco = Lookup.TK3PRequest_StatusDeco_RELEASED;
                }
                return deco;
            }
        }
        [NotMapped]
        public string AttachmentsPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, this.TK3PRequestId));
            }
        }
        [NotMapped]
        public string TempPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, "TEMP"));
            }
        }
        [NotMapped]
        public string MOD1923FileName
        {
            get
            {
                return "MOD19_23_AssayCompatibilityEvaluationForm";
            }
        }
        [NotMapped]
        public string TemplateMod1923Path
        {

            get
            {
                return this.DirTemplateMod1923;
            }
        }
        [NotMapped]
        public string TemplateMod1923Fullname
        {
            
            get
            {
                return System.IO.Path.Combine(this.TemplateMod1923Path, "MOD_19_23.pdf");
            }
        }

        [NotMapped]
        public bool canEdit
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ?  false : this.Status.Equals(Lookup.TK3PRequest_Status_PENDING);
            }
        }
        [NotMapped]
        public bool canEvaluate
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ? false : this.Status.Equals(Lookup.TK3PRequest_Status_PENDING);
            }
        }
        [NotMapped]
        public bool canClose
        {
            get
            {
                //return string.IsNullOrWhiteSpace(this.Status) ? false : (this.Status.Equals(Lookup.TK3PRequest_Status_RELEASED) && this.TmstLastDownload != null);
                return string.IsNullOrWhiteSpace(this.Status) ? false : (this.Status.Equals(Lookup.TK3PRequest_Status_RELEASED));
            }
        }
        [NotMapped]
        public bool canUploadAssay
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ? false : 
                                (this.Status.Equals(Lookup.TK3PRequest_Status_PROCESSING) || this.Status.Equals(Lookup.TK3PRequest_Status_READY));
            }
        }
        [NotMapped]
        public bool canSetRelease
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ? false :
                                (this.Status.Equals(Lookup.TK3PRequest_Status_PROCESSING) || this.Status.Equals(Lookup.TK3PRequest_Status_READY));
            }
        }
        [NotMapped]
        public bool canReopen
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ? false : this.Status.Equals(Lookup.TK3PRequest_Status_CLOSED);
            }
        }
        [NotMapped]
        public bool canDownloadAP
        {
            get
            {
                return string.IsNullOrWhiteSpace(this.Status) ? false :
                ((this.Status.Equals(Lookup.TK3PRequest_Status_RELEASED) || this.Status.Equals(Lookup.TK3PRequest_Status_CLOSED)) && this.TmstRelease != null);
            }
        }
    }

    public class TK3PAttachment
    {
        public int TK3PAttachmentId { get; set; }
        public bool? Deleted { get; set; }
        public string PhysicalFileName { get; set; }
        public string Source { get; set; }
        [Required]
        public int TK3PRequestId { get; set; }
        public virtual TK3PRequest Request { get; set; }
        public DateTime TmstInse { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        public Lookup.TK3PKAttachType Type { get; set; }
        public string UserInse { get; set; }
        public string UserLastUpd { get; set; }
    
        [NotMapped]
        public string TypeDeco
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.TK3PKAttachType.IFU: return "IFU";
                    default: return this.Type.ToString();
                }
            }
        }
        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string UserInseName { get; set; }
        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
    }

    public class TK3PEvent
    {
        public long TK3PEventId { get; set; }
        public string Note { get; set; }
        public string Operation { get; set; }
        [Required]
        public int TK3PRequestId { get; set; }
        public virtual TK3PRequest Request { get; set; }
        public DateTime TmstInse { get; set; }
        public string UserInse { get; set; }
        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

    }
}




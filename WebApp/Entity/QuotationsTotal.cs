﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
{
    public class QuotationsTotal
    {
        public int Id { get; set; }

        [JsonIgnore]
        // Il campo QuotationId viene aggiunto in automatico !!!! 
        public virtual Quotation Quotation { get; set; }

        [StringLength(20)]
        public string Category { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public decimal? Margin { get; set; }
        public decimal? Profit { get; set; }
        public decimal? Help1 { get; set; }
        public decimal? Help2 { get; set; }
        public bool? Visibility { get; set; }

    }
}




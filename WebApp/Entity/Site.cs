﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
   
    public class Site
    {
        [Required]
        public int SiteId { get; set; }

        public string Cod_CliFor { get; set; }
        public string Cod_Sito { get; set; }
        public string Rag_Sociale { get; set; }
        public string Indirizzo { get; set; }
        public string Cap { get; set; }
        public string Localita { get; set; }
        public string Prov { get; set; }
        public string Cod_Naz { get; set; }
        public string Flag_Tratt { get; set; }

        public DateTime? LastDataImported { get; set; }

        public string DescriptionFull
        {
            get {
                return string.Format("{0} [{1}] - {2} {3} {4} {5} {6} [{7}]", this.Rag_Sociale, this.Cod_CliFor, this.Indirizzo, this.Localita, this.Cap, this.Prov, this.Cod_Naz, this.Cod_Sito);
            }
        }

    }
}




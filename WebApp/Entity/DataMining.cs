﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Authorization;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using System.IO;
using WebApp.Classes;

namespace WebApp.Entity
{
   
    public class DataMining
    {
        public DataMining()
        {
            Status = Lookup.DataMining_Status_NEW;
        }
        public int DataMiningId { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(250)]
        public string FileName { get; set; }    // Nome file 

        /* 
         * RUNNING RATE ( Extraction «cost per reportable result» )
         * RUNS BY TYPE ( Extraction «DATA MINING -RUNS BY TYPE»  )
         * RUNNING RATE COM ( Extraction «billing each component»  )
        */
        [StringLength(20)]
        [Required]
        public string ExtractionType { get; set; }

        /* 
         * NEW          Nuova Richiesta Estrazione
         * READY        Estrazione pronta
         * ERROR        Richiesta estrazione in errore 
         */
        [StringLength(10)]
        [Required]
        public string Status { get; set; }

        [StringLength(1000)]
        [Display(Name = "Error")]
        public string NoteError { get; set; }

        [Display(Name = "Date From")]
        public DateTime? DateFrom { get; set; }
        [Display(Name = "Date To")]
        public DateTime? DateTo { get; set; }
        [Display(Name = "Up To Date Extraction")]
        public DateTime? DateUpToDate { get; set; }
        [StringLength(100)]
        [Display(Name = "Version Extra")]
        public string VersionExtra { get; set; }
        [StringLength(30)]
        [Display(Name = "Model Type")]
        public string ModelType { get; set; }

        //[StringLength(250)] -- rimossa lunghezza per gestione multivalore 
        public string Country { get; set; }
        //[StringLength(100)] -- rimossa lunghezza per gestione multivalore 
        public string CommercialEntity { get; set; }
        [StringLength(250)]
        public string CommercialStatus { get; set; }

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }
        [NotMapped]
        public string DateFromDeco
        {
            get { return this.DateFrom != null ? ((DateTime)this.DateFrom).ToString("dd/MM/yyyy") : ""; }
        }
        [NotMapped]
        public string DateToDeco
        {
            get { return this.DateTo != null ? ((DateTime)this.DateTo).ToString("dd/MM/yyyy") : ""; }
        }
        [NotMapped]
        public string DateUpToDateDeco
        {
            get { return this.DateUpToDate != null ? ((DateTime)this.DateUpToDate).ToString("dd/MM/yyyy") : ""; }
        }
        [NotMapped]
        public string ExtractionTypeDeco
        {
            get {
                if (this.ExtractionType.Equals(Lookup.DataMining_Type_RunningRate)) {
                    return Lookup.DataMining_Type_RunningRateDesc;
                }
                if (this.ExtractionType.Equals(Lookup.DataMining_Type_RunsByType))
                {
                    return Lookup.DataMining_Type_RunsByTypeDesc;
                }
                if (this.ExtractionType.Equals(Lookup.DataMining_Type_RunningByComponent))
                {
                    return Lookup.DataMining_Type_RunningByComponentDesc;
                }
                return string.Format("<{0}>", this.ExtractionType);
            }
        }

        [NotMapped]
        public string ModelTypeDeco
        {
            get
            {
                string ret = this.ModelType;
                if (!string.IsNullOrWhiteSpace(this.ModelType))
                {
                    var item = Lookup.InstrumentModelTypeFilters.Find(x => x.id.Equals(this.ModelType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }
    }
}




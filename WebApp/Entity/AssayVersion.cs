﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;
using System.IO;
using WebApp.Repository;
using NPoco;

namespace WebApp.Entity
{
    [TableName("Assay")]
    public class Assay
    {
        public int AssayID { get; set; }
        [StringLength(250)]
        public String AssayCod { get; set; }
        public bool Attivo { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public bool? Validated { get; set; }  // Validato da Elitech 
        public Lookup.Assay_StatoManleva? StatoManleva { get; set; }

        [StringLength(250)]
        public string Note { get; set; }
        [StringLength(20)]
        public string Tipo { get; set; }
        [StringLength(255)]
        public string AssayMadre { get; set; }
        [StringLength(255)]
        public string AssayMadreVersione { get; set; }

        public string Analyte { get; set; }
        public string Panel { get; set; }
        public string BusinessType { get; set; }

        public string InstrumentType { get; set; }

        public virtual ICollection<AssayVersion> AssayVersions { get; set; }

        [NotMapped]
        public string AllVersions
        {
            get
            {
                return (new AssaysBO()).GetAllVersions(this.AssayID);
            }
            set { }
        }

        [NotMapped]
        public string LastVersion
        {
            get
            {
                return (new AssaysBO()).GetLastVersion(this.AssayID);
            }
            set { }
        }

        [NotMapped]
        public string AttivoDesc
        {
            get
            {
                return this.Attivo ? "ACTIVE" : "";
            }
            set { }
        }

        [NotMapped]
        public Lookup.Assay_StatoManleva? StatoManlevaCalc { get; set;  }
        [NotMapped]
        public string AnalyteDesc { get; set; }
        [NotMapped]
        public string PanelDesc { get; set; }
        [NotMapped]
        public string BusinessTypeDesc { get; set; }

        [NotMapped]
        public bool Obbligatorio { get; set; }
        [NotMapped]
        public DateTime? Date1Notify { get; set; }
        [NotMapped]
        public string ObbligatorioDeco
        {
            get
            {
                return this.Obbligatorio ? "Yes" : "No";
            }
        }
        [NotMapped]
        public string Date1NotifyDeco
        {
            get
            {
                if (this.Date1Notify == null)
                {
                    return "";
                }
                return ((DateTime)this.Date1Notify).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string Date1NotifySort
        {
            get
            {
                if (this.Date1Notify == null)
                {
                    return "";
                }
                return ((DateTime)this.Date1Notify).ToString("yyyyMMddHHmmss");
            }
        }
        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                if (this.TmstLastUpd == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string TmstLastUpdSort
        {
            get
            {
                if (this.TmstLastUpd == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstLastUpd).ToString("yyyyMMddHHmmss");
            }
        }

        [NotMapped]
        public string ModelTypeDeco
        {
            get
            {
                string ret = this.InstrumentType;
                if (!string.IsNullOrWhiteSpace(this.InstrumentType))
                {
                    var item = Lookup.InstrumentModelType_EliteInstruments.Find(x => x.id.Equals(this.InstrumentType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }

    }

    [TableName("AssayVersion")]
    public class AssayVersion
    {
        public int AssayVersionID { get; set; }
        public int AssayId { get; set; }
        public virtual Assay Assay { get; set; }
        [StringLength(250)]
        public String Version { get; set; }
        public bool Obbligatorio { get; set; }
        public bool Attivo { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        public Lookup.AssayVersion_StatusSend? StatusSend { get; set; }

        [NotMapped]
        public string StatusSendDesc
        {
            get
            {
                string ret = "(not sent)";
                if (this.StatusSend != null)
                {
                    ret = this.StatusSend.ToString();
                }
                return ret;
            }
            set { }
        }
    }
}

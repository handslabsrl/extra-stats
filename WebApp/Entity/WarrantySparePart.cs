﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class WSPRequest
    {
        private readonly string DirAttachments = "Attachments\\WSPRequest\\{0}";

        public int WSPRequestId { get; set; }
        [Required]
        [StringLength(50)]
        [Display(Name = "Instrument")]
        public string SerialNumber { get; set; }    // FK INSTRUMENTS 
        [Required]
        [StringLength(10)]
        [Display(Name = "Warranty Code")]
        public string Code { get; set; }
        [Required]
        [Display(Name = "IssueDate")]
        public DateTime TmstIssue { get; set; }
        [StringLength(30)]
        [Display(Name = "Spare Part")]
        public string CodArt { get; set; }  // FK PRODUCTS
        [Display(Name = "Number of Parts")]
        public int NumberOfParts { get; set; }
        [StringLength(4000)]
        [Display(Name = "Failure Description")]
        public string Notes { get; set; }
        [Display(Name = "Decontamined")]
        public bool Decontamined { get; set; }
        [StringLength(20)]
        [Display(Name = "LogFile Id")]
        public string LogFileId { get; set; }
        [StringLength(20)]
        [Display(Name = "Ticket Id")]
        public string TicketId { get; set; }

        [StringLength(10)]
        [Display(Name = "OriginalPart")]
        public string OriginalPart { get; set; }    // FK CODES - group = WSP_ORIGINAL_PART
        [StringLength(100)]
        [Display(Name = "PO Reference")]
        public string POReference { get; set; }

        [StringLength(10)]
        [Display(Name = "ClosingOption")]
        public string ClosingOption { get; set; }


        [StringLength(1000)]
        public string ReturnedPartNote { get; set; }  // note spedizione parte da restituire !! 

        [Required]
        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(10)]
        [Display(Name = "Warranty Status")]
        public string WarrantyStatus { get; set; }    // FK CODES - group = WSP_WARRANTYSTATUS
        [StringLength(1000)]
        [Display(Name = "Warranty Status Note")]
        public string WarrantyStatusNote { get; set; }
        // Data e utente da ultimo evento SetWarningStatusNote

        [StringLength(10)]
        [Display(Name = "Exchange Request")]
        public string ExchangeRequest { get; set; }    // FK CODES - group = WSP_EXCHANGEREQUEST
        [StringLength(1000)]
        [Display(Name = "Exchange Request Note")]
        public string ExchangeRequestNote { get; set; }
        // Data e utente da ultimo evento SetExchangeRequest

        [StringLength(1000)]
        [Display(Name = "Carrier Note")]
        public string CarrierNote { get; set; }

        [StringLength(4000)]
        [Display(Name = "Shipping Note")]
        public string ShippingNote { get; set; }
        // Data e utente da ultimo evento setShippingNote

        [StringLength(10)]
        [Display(Name = "Shipment of the new part from supplier")]
        public string Compliant { get; set; }    // FK CODES - group = WSP_COMPLIANT
        [StringLength(1000)]
        [Display(Name = "Note abount Shipment of the new part from supplier")]
        public string CompliantNote { get; set; }
        // Data e utente da ultimo evento SetCompliant

        [StringLength(10)]
        [Display(Name = "Defective part management ")]
        public string WspAction { get; set; }    // FK CODES - group = WSP_ACTION
        [StringLength(1000)]
        [Display(Name = "Action Note")]
        public string WspActionNote { get; set; }
        // Data e utente da ultimo evento SetAction

        [StringLength(20)]
        [Display(Name = "Invoice Number")]
        public string InvoiceNUmber { get; set; }
        [Display(Name = "Invoice Date")]
        public DateTime? InvoiceDate { get; set; }
        // Data e utente da ultimo evento SetInvoiceInfo

        [StringLength(256)]
        public string UserInse { get; set; }
        [Display(Name = "Request Date")]
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public virtual ICollection<WSPEvent> Events { get; set; }
        public virtual ICollection<WSPAttachment> Attachments { get; set; }

        [NotMapped]
        public string TmstIssueDeco
        {
            get { return this.TmstIssue.ToString("dd/MM/yyyy"); }
        }

        [NotMapped]
        public string NotesPreview
        {
            get
            {
                string ret = "";
                if (!string.IsNullOrWhiteSpace(this.Notes))
                {
                    ret = this.Notes;
                    if (ret.Length > 200)
                    {
                        ret = string.Format("{0}[...]", ret.Substring(0, 200));
                    }
                }
                return ret;
            }
        }

        [NotMapped]
        public string TmstIssueSort
        {
            get
            {
                return this.TmstIssue.ToString("yyyyMMdd");
            }
        }
        [NotMapped]
        public string DataInseDeco
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy");
            }
        }
        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.WSPRequest_Status_DRAFT))
                        deco = "DRAFT";
                    if (this.Status.Equals(Lookup.WSPRequest_Status_PENDING))
                        deco = "Pending";
                    if (this.Status.Equals(Lookup.WSPRequest_Status_PROCESSING))
                        deco = "Processing";
                    if (this.Status.Equals(Lookup.WSPRequest_Status_CLOSED))
                        deco = "Closed";
                    if (this.Status.Equals(Lookup.WSPRequest_Status_DELETED))
                        deco = "Deleted";
                }
                return deco;
            }
        }

        [NotMapped]
        public string InvoiceDateDeco
        {
            get { return ((this.InvoiceDate == null) ? "" : ((DateTime)this.InvoiceDate).ToString("dd/MM/yyyy")); }
        }


        [NotMapped]
        public string AttachmentsPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, this.WSPRequestId));
            }
        }
        [NotMapped]
        public string DecontaminedDeco
        {
            get
            {
                return this.Decontamined ? Lookup.glb_YES : Lookup.glb_NO;
            }
        }
        [NotMapped]
        public string RGAPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, this.WSPRequestId));
            }
        }
        [NotMapped]
        public string RGATemplateName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, "MOD19,24_02_Decontamination Certificate.pdf"));
            }
        }
    }

    public class WSPEvent
    {
        public long WSPEventId { get; set; }

        [Required]
        public int WSPRequestId { get; set; }
        public virtual WSPRequest Request { get; set; }
        [StringLength(4000)]
        public string Note { get; set; }
        [StringLength(30)]
        public string Operation { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("yyyyMMddHHmmss");
            }
        }
    }

    public class WSPAttachment
    {
        public int WSPAttachmentId { get; set; }

        [Required]
        public int WSPRequestId { get; set; }
        public virtual WSPRequest Request { get; set; }

        [Required]
        public Lookup.WSPAttachType Type { get; set; }

        [StringLength(250)]
        public string Source { get; set; }

        [StringLength(250)]
        public string PhysicalFileName { get; set; }

        public bool Deleted { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [NotMapped]
        public string TypeDeco
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.WSPAttachType.file: return "FILE";
                    case Lookup.WSPAttachType.url: return "LINK";
                    default: return this.Type.ToString();
                }
            }
        }
        [NotMapped]
        public string ResourceClass
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.WSPAttachType.file: return "glyphicon glyphicon-file";
                    case Lookup.WSPAttachType.url: return "glyphicon glyphicon-link";
                    default: return "";
                }
            }
        }
        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string UserInseName { get; set; }

        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
    }

    public class Product         
    {
        //public long Id { get; set; }
        [StringLength(30)]
        public string CodArt { get; set; }
        [StringLength(100)]
        public string Description { get; set; }
        public bool OffLine { get; set; }
        public bool InWarranty { get; set; }
        public bool InvestigationForBreaking { get; set; }
        public bool SparePart { get; set; }
        public bool LimitedWarranty { get; set; }

        public string DescriptionFull
        {
            get
            {
                return string.Format("{0} - {1}", this.CodArt, this.Description);
            }
        }

    }
}

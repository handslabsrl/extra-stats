﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
{
    public class CommercialEntity
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        public int SubTotal1 { get; set; }
        public int SubTotal2 { get; set; }
        public int Total { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}




﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Entity
{
    public class InstrumentType
    {
        public int ID { get; set; }

        [Required, StringLength(50)]
        [Display(Name = "Instrument Type")]
        public string Type { get; set; }
        [Required, StringLength(50)]
        [Display(Name = "Ref (product p/n)")]
        public string ProductPartNumber { get; set; }
        [Required, StringLength(100)]
        public string Description { get; set; }

        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name ="Standard Cost €")]
        public decimal StandardCost { get; set; }
        [DisplayFormat(DataFormatString = "{0:N2}", ApplyFormatInEditMode = true), Display(Name = "Official Price €")]
        public decimal OfficialPrice { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
    }
}




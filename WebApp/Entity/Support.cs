﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;
using WebApp.Models;
using System.IO;

namespace WebApp.Entity
{

    public class TKClassification
    {
        public TKClassification()
        {
            Instrument = Lookup.Classification_RadioGroup.No;
            OtherPlatform = Lookup.Classification_RadioGroup.No; 
            CodArt = Lookup.Classification_RadioGroup.No;
            AddInfo = Lookup.Classification_RadioGroup.No;
            ClosingInfo = Lookup.Classification_RadioGroup.No;
            ClaimLogFile = Lookup.Classification_RadioGroup.No;
            InfoContact = Lookup.Classification_RadioGroup.No;
            Quantity = Lookup.Classification_RadioGroup.No;
            ToDoList = Lookup.Classification_RadioGroup.No;
            SupplierEngagement = Lookup.Classification_RadioGroup.No;
            LocationSGAT = Lookup.Classification_RadioGroup.No;
            CustomerSGAT = Lookup.Classification_RadioGroup.No;
        }

        [Required]
        public int TKClassificationId { get; set; }
        public int? ParentId { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public bool? Reserved { get; set; }  // Solo Supporto, default = false
        public bool? Deleted { get; set; }

        [StringLength(20)]
        public string ConfigId { get; set; }

        public Lookup.Classification_RadioGroup? Instrument { get; set; }
        public Lookup.Classification_RadioGroup? OtherPlatform { get; set; }
        public Lookup.Classification_RadioGroup? CodArt { get; set; }
        public Lookup.Classification_RadioGroup? AddInfo { get; set; }
        public int? TKAddInfoId { get; set; }
        public Lookup.Classification_RadioGroup? ClosingInfo { get; set; }
        public int? TKClosingInfoId { get; set; }
        public Lookup.Classification_RadioGroup? ClaimLogFile { get; set; }

        public Lookup.Classification_RadioGroup? InfoContact { get; set; }
        public Lookup.Classification_RadioGroup? Quantity { get; set; }
        public Lookup.Classification_RadioGroup? ToDoList { get; set; }
        public Lookup.Classification_RadioGroup? SupplierEngagement { get; set; }
        public Lookup.Classification_RadioGroup? LocationSGAT { get; set; }     // LocationId (tabella Site)
        public Lookup.Classification_RadioGroup? CustomerSGAT { get; set; }     // NON USATO 

        public bool? NotifyAreaManager { get; set; }  // valido solo per ROOT

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [NotMapped]
        public string TitleFull
        {
            get
            {
                string ret = this.Title;
                if (!string.IsNullOrWhiteSpace(this.Code))
                {
                    ret = string.Format("{0} [{1}]", this.Title.Trim(), this.Code.Trim()); 
                }
                return ret;
            }
        }
        [NotMapped]
        public string icon
        {
            get
            {
                string ret = "";
                if (this.TKClassificationId <= 0)
                {
                    ret = "fa fa-sitemap";
                }
                if ((this.Reserved ?? false))
                {
                    ret = "fa fa-lock";
                }
                return ret;
            }
        }
        [NotMapped]
        public bool Expanded { get; set; }
        [NotMapped]
        public string BreadCrumb { get; set; }
        [NotMapped]
        public string AddInfoTitle { get; set; }
        [NotMapped]
        public string ClosingInfoTitle { get; set; }

        [NotMapped]
        public string InstrumentDeco
        {
            get
            {
                string ret = "";
                if (this.Instrument != null && this.TKClassificationId >= 0)
                {
                    ret = this.Instrument.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string OtherPlatformDeco
        {
            get
            {
                string ret = "";
                if (this.OtherPlatform != null && this.TKClassificationId >= 0)
                {
                    ret = this.OtherPlatform.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string CodArtDeco
        {
            get
            {
                string ret = "";
                if (this.CodArt != null && this.TKClassificationId >= 0)
                {
                    ret = this.CodArt.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string AddInfoDeco
        {
            get
            {
                string ret = "";
                if (this.AddInfo != null && this.TKClassificationId >= 0)
                {
                    ret = this.AddInfo.ToString();
                    if (!string.IsNullOrWhiteSpace(this.AddInfoTitle))
                    {
                        ret = string.Format("{0} [{1}]", ret, this.AddInfoTitle);
                    }
                }
                return ret;
            }
        }
        [NotMapped]
        public string ClosingInfoDeco
        {
            get
            {
                string ret = "";
                if (this.ClosingInfo != null && this.TKClassificationId >= 0)
                {
                    ret = this.ClosingInfo.ToString();
                    if (!string.IsNullOrWhiteSpace(this.ClosingInfoTitle))
                    {
                        ret = string.Format("{0} [{1}]", ret, this.ClosingInfoTitle);
                    }
                }
                return ret;
            }
        }
        [NotMapped]
        public string ClaimLogFileDeco
        {
            get
            {
                string ret = "";
                if (this.ClaimLogFile != null && this.TKClassificationId >= 0)
                {
                    ret = this.ClaimLogFile.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string InfoContactDeco
        {
            get
            {
                string ret = "";
                if (this.InfoContact != null && this.TKClassificationId >= 0)
                {
                    ret = this.InfoContact.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string QuantityDeco
        {
            get
            {
                string ret = "";
                if (this.Quantity != null && this.TKClassificationId >= 0)
                {
                    ret = this.Quantity.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string ToDoListDeco
        {
            get
            {
                string ret = "";
                if (this.ToDoList != null && this.TKClassificationId >= 0)
                {
                    ret = this.ToDoList.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string SupplierEngagementDeco
        {
            get
            {
                string ret = "";
                if (this.SupplierEngagement != null && this.TKClassificationId >= 0)
                {
                    ret = this.SupplierEngagement.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string CustomerSGATDeco
        {
            get
            {
                string ret = "";
                if (this.CustomerSGAT != null && this.TKClassificationId >= 0)
                {
                    ret = this.CustomerSGAT.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string LocationSGATDeco
        {
            get
            {
                string ret = "";
                if (this.LocationSGAT != null && this.TKClassificationId >= 0)
                {
                    ret = this.LocationSGAT.ToString();
                }
                return ret;
            }
        }
        [NotMapped]
        public string ReservedDeco
        {
            get
            {
                string ret = "";
                if (this.TKClassificationId >= 0)
                {
                    ret = (this.Reserved == true ? "Reserved" : "Public");
                }
                return ret;
            }
        }
        [NotMapped]
        public string NotifyAreaManagerDeco
        {
            get
            {
                string ret = "";
                if (this.TKClassificationId >= 0)
                {
                    ret = (this.NotifyAreaManager == true ? "Yes" : "");
                }
                return ret;
            }
        }
        [NotMapped]
        public string configDescription
        {
            get
            {
                string ret = "";
                if (!string.IsNullOrWhiteSpace(this.ConfigId))
                {
                    ret = Lookup.GetTKSupportConfig(this.ConfigId).Description;
                }
                return ret;
            }
        }
    }

    public class TKClassificationPath
    {
        [Required]
        public int TKClassificationIdRef { get; set; }
        [Required]
        public int TKClassificationId { get; set; }
        public string BreadCrumb { get; set; }
        public string RootTitle { get; set; }
        public int? RootId{ get; set; }
    }

    public class TKRequest
    {
        private readonly string DirAttachments = "Attachments\\TKRequest\\{0}";

        public TKRequest()
        {
            //NotifyChange = false;
        }

        public int TKRequestId { get; set; }

        [StringLength(100)]
        [Display(Name = "Claim LogFile")]
        public string Code { get; set; }
        [StringLength(50)]
        [Display(Name = "Code (Registration)")]
        public string CodeRegistration{ get; set; }
        [StringLength(300)]
        [Display(Name = "Subject")]
        public string Object { get; set; }
        [StringLength(300)]
        [Display(Name = "Subject (Registration)")]
        public string ObjectRegistration { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Description (Registration)")]
        public string DescriptionRegistration { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        [Display(Name = "Date of the request")]
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        [Display(Name = "User Registration")]
        public string UserRegistration { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [Required]
        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(256)]
        public string UserInChargeOf { get; set; }
        public DateTime? TmstInChargeOf { get; set; }

        [StringLength(256)]
        public string UserClosed { get; set; }
        public DateTime? TmstClosed { get; set; }
        [Display(Name = "Closing Note")]
        public string ClosingNote { get; set; }

        [Required]
        [Display(Name = "Classification")]
        public int TKClassificationId { get; set; }
        public virtual TKClassification Classification{ get; set; }

        [Required]
        [Display(Name = "Classification (Registration)")]
        public int TKClassificationIdRegistration { get; set; }

        [StringLength(50)]
        [Display(Name = "Instrument")]
        public string SerialNumber { get; set; }
        [StringLength(50)]
        [Display(Name = "Instrument (Registration)")]
        public string SerialNumberRegistration { get; set; }

        [Display(Name = "Additional Information")]
        public int? TKAddInfoId { get; set; }
        public virtual TKAddInfo AddInfo { get; set; }

        [Display(Name = "Additional Information (Registration)")]
        public int? TKAddInfoIdRegistration { get; set; }

        [StringLength(1000)]
        [Display(Name = "CCS Email Addresses")]
        public string EmailsKeepInformed { get; set; }

        public int? StanbyMinutes { get; set; }

        [Display(Name = "Closing Information")]
        public int? TKClosingInfoId { get; set; }
        public virtual TKClosingInfo ClosingInfo { get; set; }

        [StringLength(1000)]
        [Display(Name = "Other Platform")]
        public string OtherPlatform { get; set; }
        [StringLength(1000)]
        [Display(Name = "Other Platform (Registration)")]
        public string OtherPlatformRegistration { get; set; }

        [StringLength(30)]
        [Display(Name = "Product")]
        public string CodArt { get; set; }
        [StringLength(30)]
        [Display(Name = "Product (Registration)")]
        public string CodArtRegistration { get; set; }

        public DateTime? UserRegistration_LastAccess { get; set; }

        public bool Complaint { get; set; }
        [Display(Name = "Complait Action")]
        public string ComplaintStatus { get; set; }
        [Display(Name = "Defect Observed")]
        public string DefectObserved { get; set; }
        [Display(Name = "Consequences for the end-user")]
        public string ConsequencesForEndUser { get; set; }
        public string ComplaintStatusNote { get; set; }
        [Display(Name = "Lot Number")]
        public string ComplaintLotNumber { get; set; }
        [Display(Name = "End user’s site")]
        public string ComplaintCustomer { get; set; }
        [Display(Name = "Incident")]
        public bool ComplaintIncident { get; set; }
        [Display(Name = "Incident Note")]
        public string ComplaintIncidentNote { get; set; }
        [Display(Name = "City")]
        public string ComplaintCity { get; set; }

        [Display(Name = "Complaint Classification")]
        public int? TKComplaintClassificationId { get; set; }
        //public virtual TKAddInfo ComplaintClassification { get; set; }

        [Display(Name = "Linkomm Information")]
        public string ComplaintLinkommInfo { get; set; }

        [Display(Name = "Linkomm Tracker")]
        public string UserLinkommTracker { get; set; }

        [StringLength(200)]
        public string InfoContact { get; set; }
        [StringLength(200)]
        public string InfoContactRegistration { get; set; }

        public decimal? Quantity { get; set; }
        public decimal? QuantityRegistration { get; set; }

        // Location -> tabella Site
        public int? LocationId { get; set; }
        public int? LocationIdRegistration { get; set; }

        public int? PercToDoList { get; set; }

        public virtual ICollection<TKAttachment> Attachments { get; set; }
        public virtual ICollection<TKEvent> Events { get; set; }
        public virtual ICollection<TKNotification> Notifications { get; set; }

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }
        [NotMapped]
        public string ComplaintClassificationDeco { get; set; }
        [NotMapped]
        public string ComplaintStatusDeco { get; set; }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.TKRequest_Status_DRAFT))
                        deco = Lookup.TKRequest_StatusDeco_DRAFT;
                    if (this.Status.Equals(Lookup.TKRequest_Status_PENDING))
                        deco = Lookup.TKRequest_StatusDeco_PENDING;
                    if (this.Status.Equals(Lookup.TKRequest_Status_REOPENED))
                        deco = Lookup.TKRequest_StatusDeco_REOPENED;
                    if (this.Status.Equals(Lookup.TKRequest_Status_PROCESSING))
                        deco = Lookup.TKRequest_StatusDeco_PROCESSING;
                    if (this.Status.Equals(Lookup.TKRequest_Status_CLOSED))
                        deco = Lookup.TKRequest_StatusDeco_CLOSED;
                    if (this.Status.Equals(Lookup.TKRequest_Status_STANDBY))
                        deco = Lookup.TKRequest_StatusDeco_STANDBY;
                    if (this.Status.Equals(Lookup.TKRequest_Status_DELETED))
                        deco = Lookup.TKRequest_StatusDeco_DELETED;
                    if (this.Status.Equals(Lookup.TKRequest_Status_UNDER_INVESTIGATION))
                        deco = Lookup.TKRequest_StatusDeco_UNDER_INVESTIGATION;
                }
                return deco;
            }
        }

        [NotMapped]
        public string RequestInfo
        {
            get { return string.Format("{0} - {1}", this.TKRequestId, this.Object); }
        }

        [NotMapped]
        public string AttachmentsPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachments, this.TKRequestId));
            }
        }
        [NotMapped]
        public string ComplaintIncidentDeco
        {
            get
            {
                return this.ComplaintIncident ? "YES" : "NO";
            }
        }
        [NotMapped]
        public string RequestType
        {
            get
            {
                return this.Complaint ? "Complaint" : "Ticket";
            }
        }
        [NotMapped]
        public string ToDoItemsJSON { get; set; }

        [NotMapped]
        // Dati per invio email 
        public string EmailSubject { get; set; }
        [NotMapped]
        public string EmailBody { get; set; }



    }
    public class TKAttachment
    {
        public int TKAttachmentId { get; set; }

        [Required]
        public int TKRequestId { get; set; }
        public virtual TKRequest Request { get; set; }

        [Required]
        public Lookup.TKAttachType Type { get; set; }

        [StringLength(250)]
        public string Source { get; set; }

        [StringLength(250)]
        public string PhysicalFileName { get; set; }

        public bool? Deleted { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public bool? Draft { get; set; }        // l'allegato TEMPORANEO (collegato ad una NOTA in bozza) 
        public long TKEventId { get; set; }     // Attach legato ad una NOTA

        public bool? Reserved { get; set; }  // Solo Supporto, default = false

        [NotMapped]
        public string TypeDeco
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.TKAttachType.file: return "FILE";
                    case Lookup.TKAttachType.url: return "LINK";
                    default: return this.Type.ToString();
                }
            }
        }
        [NotMapped]
        public string ResourceClass
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.TKAttachType.file: return "glyphicon glyphicon-file";
                    case Lookup.TKAttachType.url: return "glyphicon glyphicon-link";
                    default: return "";
                }
            }
        }
        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string UserInseName { get; set; }

        //[NotMapped]
        //public string ResourceClass
        //{
        //    get
        //    {
        //        switch (this.Type)
        //        {
        //            case Lookup.DRResourceType.file: return "glyphicon glyphicon-file";
        //            case Lookup.DRResourceType.attachment: return "glyphicon glyphicon-paperclip";
        //            case Lookup.DRResourceType.url: return "glyphicon glyphicon-link";
        //            default: return "";
        //        }
        //    }
        //}
        //[NotMapped]
        //public DateTime? TmstLastGet { get; set; }

        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
    }

    public class TKEvent
    {
        public long TKEventId { get; set; }

        [Required]
        public int TKRequestId { get; set; }
        public virtual TKRequest Request { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        public string Note { get; set; }

        [StringLength(30)]
        public string Operation { get; set; }

        public bool? Reserved { get; set; }  // Solo Supporto, default = false

        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [NotMapped]
        public string ReservedDeco
        {
            get
            {
                string ret = "";
                if (this.Reserved != null)
                {
                    ret = (this.Reserved == true ? "Reserved" : "");
                }
                return ret;
            }
        }
        [NotMapped]
        public string ReservedDecoShort
        {
            get
            {
                string ret = "";
                if (this.Reserved != null)
                {
                    ret = (this.Reserved == true ? "R" : "");
                }
                return ret;
            }
        }

        //[NotMapped]
        //public string NoteNoCRLF
        //{
        //    get
        //    {
        //        if (string.IsNullOrWhiteSpace(this.Note))
        //        {
        //            return "";
        //        }
        //        return this.Note.Replace("\r", "/").Replace("\n", "/").Replace("//", "/").Replace("/", " / ");
        //    }
        //}
    }

    public class TKEventRead
    {
        public int TKRequestId { get; set; }
        public long TKEventId { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
    }

    public class TKAddInfo
    {
        [Required]
        public int TKAddInfoId { get; set; }
        public int? ParentId { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public bool? Reserved { get; set; }  // Solo Supporto, default = false
        public bool? Deleted { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(20)]
        public string ConfigId { get; set; }
        [StringLength(20)]
        public string GroupId { get; set; }
        [StringLength(100)]
        public string Color { get; set; }


        [NotMapped]
        public string TitleFull
        {
            get
            {
                string ret = this.Title;
                if (!string.IsNullOrWhiteSpace(this.Code))
                {
                    ret = string.Format("{0} [{1}]", this.Title.Trim(), this.Code.Trim());
                }
                return ret;
            }
        }

        [NotMapped]
        public string icon
        {
            get
            {
                string ret = "";
                if (this.TKAddInfoId <= 0)
                {
                    ret = "fa fa-sitemap";
                }
                if ((this.Reserved ?? false))
                {
                    ret = "fa fa-lock";
                }
                return ret;
            }
        }
        [NotMapped]
        public string colorInfoHtml
        {
            get
            {
                string ret = "";
                if (!string.IsNullOrWhiteSpace(this.Color))
                {
                    ret = "<span style='margin-left:10px!important;'>&nbsp;</span>";
                    try
                    {
                        List<string> colori = JsonConvert.DeserializeObject<List<string>>(this.Color);
                        foreach (var item in colori)
                        {
                            //ret += "<span class='badge' style='margin-top: 0px!importa; margin-left:10px!important; background-color:" + item + "'>&nbsp;</span>";
                            ret += "<span class='fa fa-square' style='color:" + item + "; font-size: 20px!important;'>&nbsp;</span>";
                        }
                    }
                    catch (Exception e)
                    {
                        //
                    }
                }
                return ret;
            }
        }

        [NotMapped]
        public bool Expanded { get; set; }
        [NotMapped]
        public string BreadCrumb { get; set; }
        [NotMapped]
        public string BreadCrumbShort
        {
            get
            {
                string ret = this.BreadCrumb;
                if (!string.IsNullOrWhiteSpace(ret))
                {
                    int nStart = ret.IndexOf(">");
                    if (nStart>=0)
                    {
                        ret = ret.Substring(nStart+1).Trim();
                    }
                }
                return ret;
            }
        }
    }

    public class TKAddInfoPath
    {
        [Required]
        public int TKAddInfoIdRef { get; set; }
        [Required]
        public int TKAddInfoId { get; set; }
        public string BreadCrumb { get; set; }
        public string RootTitle { get; set; }
    }

    public class TKClosingInfo
    {
        [Required]
        public int TKClosingInfoId { get; set; }
        public int? ParentId { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public bool? Deleted { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(20)]
        public string ConfigId { get; set; }

        [NotMapped]
        public string TitleFull
        {
            get
            {
                string ret = this.Title;
                if (!string.IsNullOrWhiteSpace(this.Code))
                {
                    ret = string.Format("{0} [{1}]", this.Title.Trim(), this.Code.Trim());
                }
                return ret;
            }
        }

        [NotMapped]
        public string icon
        {
            get
            {
                string ret = "";
                if (this.TKClosingInfoId <= 0)
                {
                    ret = "fa fa-sitemap";
                }
                return ret;
            }
        }
        [NotMapped]
        public bool Expanded { get; set; }
        [NotMapped]
        public string BreadCrumb { get; set; }
        [NotMapped]
        public string BreadCrumbShort
        {
            get
            {
                string ret = this.BreadCrumb;
                if (!string.IsNullOrWhiteSpace(ret))
                {
                    int nStart = ret.IndexOf(">");
                    if (nStart >= 0)
                    {
                        ret = ret.Substring(nStart + 1).Trim();
                    }
                }
                return ret;
            }
        }
    }

    public class TKClosingInfoPath
    {
        [Required]
        public int TKClosingInfoIdRef { get; set; }
        [Required]
        public int TKClosingInfoId { get; set; }
        public string BreadCrumb { get; set; }
        public string RootTitle { get; set; }
    }

    public class TKNotification
    {
        public long TKNotificationId { get; set; }

        [Required]
        public int TKRequestId { get; set; }
        public virtual TKRequest Request { get; set; }

        [StringLength(50)]
        public string Operation { get; set; }
        public string Note { get; set; }

        public DateTime TmstInse { get; set; }

        [NotMapped]
        public string TmstInseDeco
        {
            get
            {
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
    }

    public class TKToDoItem
    {
        public long TKToDoItemId { get; set; }

        [Required]
        public int TKRequestId { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public int sequence { get; set; }

        public DateTime? TmstLastUpd { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstComplete { get; set; }
        [StringLength(256)]
        public string UserComplete { get; set; }

        [NotMapped]
        public string statusNewItem { get; set; }

        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;

            }
        }
        [NotMapped]
        public string TmstCompleteDeco
        {
            get
            {
                string ret = "";
                if (this.TmstComplete != null)
                {
                    ret = ((DateTime)this.TmstComplete).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;

            }
        }
    }
}




﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
   
    public class Department
    {
        public int DepartmentId { get; set; }

        [Required]
        [StringLength(100)]
        public string Description { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [StringLength(20)]
        public string  DepartmentType { get; set; }

        public bool ReceiveNewsWebRepository { get; set; }

    }
}




﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WebApp.Classes;
using System.IO;
using WebApp.Repository;

namespace WebApp.Entity
{


    public class DRTopic
    {
        private readonly string DirTopics = "Download\\WebRepository\\{0}";


        public DRTopic()
        {
            NotifyChange = false;
        }

        public int DRTopicId { get; set; }

        [StringLength(50)]
        public string Code { get; set; }
        [StringLength(20)]
        public string Revision { get; set; }
        [StringLength(200)]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Section")]
        public int DRSectionId { get; set; }
        public virtual DRSection DRSection { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        public bool? NotifyChange { get; set; }

        public DateTime TmstLastUpdResources { get; set; }

        [Display(Name = "Released Date")]
        public DateTime? ReleasedDate { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        public virtual ICollection<DRResource> Resources { get; set; }

        [NotMapped]
        public string ResourcesPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirTopics, this.DRTopicId));
            }
        }
        [NotMapped]
        public string TempPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirTopics, "TEMP"));
            }
        }
        [NotMapped]
        public string TopicInfo
        {
            get
            {
                string ret = this.Code;
                if (!string.IsNullOrWhiteSpace(this.Revision))
                {
                    ret = string.Format("{0} Rev.{1}", ret, this.Revision);
                }
                ret = string.Format("{0} {1}", ret, this.Title);
                return ret;
            }
        }

        [NotMapped]
        public string EmailBody { get; set; }       // Caricati solo a richiesta

    }


    public class DRResource
    {
        public int DRResourceId { get; set; }

        [Required]
        public int DRTopicId { get; set; }
        public virtual DRTopic Topic { get; set; }

        [Required]
        public Lookup.DRResourceType Type { get; set; }
        
        [StringLength(200)]
        public string Title { get; set; }
        public string Source { get; set; }

        public bool? Deleted { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        [NotMapped]
        public string TypeDeco
        {
            get {
                switch (this.Type)
                {
                    case Lookup.DRResourceType.file: return "FILE";
                    case Lookup.DRResourceType.attachment: return "ATTACHMENT";
                    case Lookup.DRResourceType.url: return "LINK";
                    default: return this.Type.ToString();
                }
            }
        }
        [NotMapped]
        public string ResourceClass
        {
            get
            {
                switch (this.Type)
                {
                    case Lookup.DRResourceType.file: return "glyphicon glyphicon-file";
                    case Lookup.DRResourceType.attachment: return "glyphicon glyphicon-paperclip";
                    case Lookup.DRResourceType.url: return "glyphicon glyphicon-link";
                    default: return "";
                }
            }
        }
        [NotMapped]
        public DateTime? TmstLastGet { get; set; }

        [NotMapped]
        public bool FlagDaLeggere
        {
            get
            {
                bool ret = true;
                if (this.TmstLastUpd != null )
                {
                    if (this.TmstLastGet != null && this.TmstLastGet > this.TmstLastUpd) 
                    {
                        ret = false;
                    }
                }
                return ret;
            }
        }
        [NotMapped]
        public string TmstLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
        [NotMapped]
        public string DataLastUpdDeco
        {
            get
            {
                string ret = "";
                if (this.TmstLastUpd != null)
                {
                    ret = ((DateTime)this.TmstLastUpd).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        //[NotMapped]
        //public string ResourceFullFileName { get; set; }
        [NotMapped]
        public string SourcePlayerVimeo
        {
            get
            {
                string ret = "";
                if (!String.IsNullOrWhiteSpace(this.Source) && this.Source.Contains("vimeo.com"))
                {
                    ret = this.Source.Replace("//vimeo.com/", "//player.vimeo.com/video/");
                }
                return ret;
            }
        }

    }

    public class DRTracking
    {
        public long DRTrackingId { get; set; }

        // Riferimento Estero: TOPIC/RESOURCE
        [StringLength(20)]
        public string ExtType { get; set; }
        public int ExtId { get; set; }

        [Required]
        //public Lookup.DRTrackingType Type { get; set; }
        [StringLength(20)]
        public string Operation { get; set; }

        [StringLength(1000)]
        public string Note { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
    }

    public class DRSection
    {
        [Required]
        public int DRSectionId { get; set; }
        public int? ParentId { get; set; }
        [StringLength(200)]
        public string Title { get; set; }
        public bool? Locked { get; set; }
        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }
        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }
        [NotMapped]
        public string icon {
            get
            {
                string ret = "";
                if (this.DRSectionId <= 0)
                {
                    ret = "fa fa-home";
                }
                if ((this.Locked ?? false))
                {
                    ret = "fa fa-ban";
                }
                return ret;
            }
        }
        [NotMapped]
        public bool Expanded { get; set; }
        [NotMapped]
        public bool Selected { get; set; }
        [NotMapped]
        public int cntTopics { get; set; }
        //[NotMapped]
        //public int cntResources { get; set; }
    }

    public class DRSectionPaths
    {
        [Required]
        public int DRSectionIdRef { get; set; }
        [Required]
        public int DRSectionId { get; set; }
        public string BreadCrumb { get; set; }
    }
}




﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using WebApp.Classes;

namespace WebApp.Entity
{
    public class Training
    {

        private readonly string DirAttachmentsAgenda = "Attachments\\Trainings\\{0}";


        public int TrainingId { get; set; }

        [StringLength(200)]
        [Required]
        public string Title { get; set; }

        [Required]
        public DateTime PeriodFrom { get; set; }
        [Required]
        public DateTime PeriodTo { get; set; }

        // Dove si tiene il corso (campo libero)
        [Required]
        [StringLength(200)]
        [Display(Name = "Location")]    
        public string Location { get; set; }        // Lookup su TrainingLocation

        // Nome dei relatori (campo libero)
        [Required]
        [StringLength(200)]
        public string Trainers { get; set; }

        [Required]
        public int TrainingTypeId { get; set; }
        public virtual TrainingType TrainingType { get; set; }

        public int? HotelId { get; set; }
        public virtual Hotel Hotel { get; set; }

        [Required]
        [StringLength(20)]
        public string Status { get; set; }

        [StringLength(4000)]
        [Display(Name = "Internal Note")]
        public string Note { get; set; }

        [StringLength(250)]
        public string Agenda { get; set; }    // Nome file Agenda Corso (in \Attachment)

        [Display(Name = "Reserved")]
        // NULLABLE per evitare il msg di DevExtreme di campo obbligatorio
        public bool Reserved { get; set; }     // true: solo utenti interni / false: per tutti

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [NotMapped]
        public string PeriodFromDeco
        {
            get { return this.PeriodFrom.ToString("dd/MM/yyyy"); }
        }

        [NotMapped]
        public string PeriodFromSort
        {
            get
            {
                return this.PeriodFrom.ToString("yyyyMMdd");
            }
        }

        [NotMapped]
        public string PeriodToDeco
        {
            get { return this.PeriodTo.ToString("dd/MM/yyyy"); }
        }

        [NotMapped]
        public string PeriodToSort
        {
            get
            {
                return this.PeriodTo.ToString("yyyyMMdd");
            }
        }

        [NotMapped]
        public string ReservedDesc
        {
            get { return (this.Reserved ? "Yes" : "No"); }

        }

        [NotMapped]
        public bool AgendaExist
        {
            get
            {
                string fullFileName = this.AgendaFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.AgendaFullFileName);
            }
        }

        [NotMapped]
        public string AgendaFileName
        {
            get
            {
                //if (string.IsNullOrWhiteSpace(this.Agenda))
                //{
                //    return "";
                //}
                //return this.Agenda;
                return string.Format("{0}", this.Agenda);
            }
        }
        [NotMapped]
        public string AgendaPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachmentsAgenda, this.TrainingId));
            }
        }
        [NotMapped]
        public string AgendaFullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.AgendaPathName, this.AgendaFileName);
            }
        }

        [NotMapped]
        public string StatusDeco
        {
            get
            {
                string deco = "";
                if (!String.IsNullOrWhiteSpace(this.Status))
                {
                    deco = this.Status;
                    if (this.Status.Equals(Lookup.Training_Status_CLOSED))
                        deco = "Closed for registration";
                }
                return deco;
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace WebApp.Entity
{
    public class Hotel
    {
        private readonly string DirAttachmentsHotel = "Attachments\\Hotels\\{0}";

        public int HotelId { get; set; }

        [Required]
        [StringLength(200)]
        [Display(Name = "Hotel")]
        public string Description { get; set; }

        [StringLength(200)]
        public string Address { get; set; }

        [StringLength(1000)]
        public string Details { get; set; }

        [StringLength(4000)]
        public string Note { get; set; }

        [StringLength(250)]
        [Display(Name = "CC Authorization")]
        public string CCAuthorization { get; set; }    // Nome file logico Modello CC Authorization  (il file fisico è in \Attachments\Hotel)

        [StringLength(200)]
        [Display(Name = "Confirmation Email Subject")]
        public string ConfirmationSubject { get; set; }
        [Display(Name = "Confirmation Email Message")]
        public string ConfirmationBody { get; set; }

        [Required]
        [StringLength(256)]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [StringLength(256)]
        public string UserLastUpd { get; set; }
        public DateTime? TmstLastUpd { get; set; }

        [NotMapped]
        public bool CCAuthorizationExist
        {
            get
            {
                string fullFileName = this.CCAuthorizationFileName;
                if (String.IsNullOrWhiteSpace(fullFileName))
                {
                    return false;
                }
                return System.IO.File.Exists(this.CCAuthorizationFullFileName);
            }
        }

        [NotMapped]
        public string CCAuthorizationFileName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.CCAuthorization))
                {
                    return "";
                }
                //return string.Format("{0}_{1}", this.HotelId, this.CCAuthorization);
                return string.Format("{0}", this.CCAuthorization);
            }
        }
        [NotMapped]
        public string CCAuthorizationPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format(DirAttachmentsHotel, this.HotelId));
            }
        }
        [NotMapped]
        public string CCAuthorizationFullFileName
        {
            get
            {
                return System.IO.Path.Combine(this.CCAuthorizationPathName, this.CCAuthorizationFileName);
            }
        }

        [NotMapped]
        public string AddressNoCRLF
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Address))
                {
                    return "";
                }
                return this.Address.Replace("\r", "/").Replace("\n", "/").Replace("//", "/").Replace("/", " / ");
            }
        }
        [NotMapped]
        public string DetailsNoCRLF
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Details))
                {
                    return "";
                }
                return this.Details.Replace("\r", "/").Replace("\n", "/").Replace("//", "/").Replace("/", " / ");
            }
        }

    }
}


﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using WebApp.Models;
using WebApp.Classes;
using WebApp.Repository;

namespace WebApp.Entity
{

    public class ProcessRequest
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]
        // vedi Lookup.ProcessRequest_Type_....
        public string ProcessType { get; set; }

        // Parametri in formato JSON {use 
        [StringLength(1000)]
        [Display(Name = "Parameters")]
        public string ProcessParms { get; set; }

        [StringLength(256)]
        public string UserInse { get; set; }
        public DateTime TmstInse { get; set; }

        [StringLength(10)]
        [Required]
        // vedi Lookup.ProcessRequest_Status_....
        public string Status { get; set; }
        public DateTime? TmstExec { get; set; }
        [StringLength(1000)]
        public string NoteExec { get; set; }

        [StringLength(200)]
        public string ExternalRif { get; set; } // Riferimento esterno da usare ad es. per il serial number dello strumento (per join !!)

        [StringLength(256)]
        public string FileNameDownload { get; set; }  // Nome file per Download 

        [NotMapped]
        public string TmstInseDeco
        {
            get { return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        [NotMapped]
        public string TmstInseSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMddHHmmss");
            }
        }

        [NotMapped]
        public string RequestTypeDeco
        {
            get
            {
                if (this.ProcessType.Equals(Lookup.ProcessRequest_Type_RnDExtractions)) return "Detail";
                if (this.ProcessType.Equals(Lookup.ProcessRequest_Type_RnDExtractionsSummary)) return "Summary";
                if (this.ProcessType.Equals(Lookup.ProcessRequest_Type_LogFileFluorescenze)) return Lookup.ProcessRequest_Type_LogFileFluorescenzeDeco;
                if (this.ProcessType.Equals(Lookup.ProcessRequest_Type_LogFileTemperature)) return Lookup.ProcessRequest_Type_LogFileTemperatureDeco;
                return this.ProcessType;
            }
        }

        [NotMapped]
        private ProcessRequestRnD _paramsRnD { get; set; }

        [NotMapped]
        public ProcessRequestRnD paramsRnD
        {
            get
            {
                if (this._paramsRnD == null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ProcessParms))
                    {
                        try
                        {
                            this._paramsRnD = JsonConvert.DeserializeObject<ProcessRequestRnD>(this.ProcessParms);
                            if (!String.IsNullOrWhiteSpace(this._paramsRnD.Instrument))
                            {
                                // singolo strumento o multi strumento 
                                if (this._paramsRnD.Instrument.IndexOf(",") == -1)
                                {
                                    this._paramsRnD.InstrumentData = (new InstrumentsBO()).GetInstrument(SerialNumber: this._paramsRnD.Instrument);
                                } else
                                {
                                    String[] strumenti = this._paramsRnD.Instrument.Split(',');
                                    string allInstrument = "";
                                    foreach (string item in strumenti)
                                    {
                                        InstrumentModel tmp = (new InstrumentsBO()).GetInstrument(SerialNumber: item);
                                        if (!string.IsNullOrWhiteSpace(allInstrument))
                                        {
                                            allInstrument += "\n";
                                        }
                                        allInstrument += tmp.SerialNumberInfo;
                                    }
                                    this._paramsRnD.InstrumentData = new InstrumentModel()
                                    {
                                        SerialNumber = allInstrument
                                    };
                                    
                                }
                                //if (data != null)
                                //{
                                //    this._paramsRnD.CompanyName = data.CompanyName;
                                //    this._paramsRnD.SiteDescription = data.SiteDescription;
                                //    this._paramsRnD.SiteCity = data.SiteCity;
                                //    this._paramsRnD.SiteCountry = data.SiteCountry;
                                //    this._paramsRnD.CommercialEntity = data.CommercialEntity;
                                //}
                            }
                        }
                        catch (Exception e)
                        {
                            string msg = String.Format("[{0}] - {1} / {2}", "infoRnD", e.Message, e.Source);
                        }
                    }
                }

                return this._paramsRnD;
            }
        }
        [NotMapped]
        private ProcessRequestInstrumentUpdate _paramsInstrumentUpdate { get; set; }

        [NotMapped]
        public ProcessRequestInstrumentUpdate paramsInstrumentUpdate
        {
            get
            {
                if (this._paramsInstrumentUpdate == null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ProcessParms))
                    {
                        try
                        {
                            this._paramsInstrumentUpdate = JsonConvert.DeserializeObject<ProcessRequestInstrumentUpdate>(this.ProcessParms);
                        }
                        catch (Exception e)
                        {
                            string msg = String.Format("[{0}] - {1} / {2}", "infoInstrumentUpdate", e.Message, e.Source);
                        }
                    }
                }

                return this._paramsInstrumentUpdate;
            }
        }

        [NotMapped]
        private ProcessRequestLogFileExtracData _paramsLogFileExtracData { get; set; }

        [NotMapped]
        public ProcessRequestLogFileExtracData paramsLogFileExtracData
        {
            get
            {
                if (this._paramsLogFileExtracData == null)
                {
                    if (!string.IsNullOrWhiteSpace(this.ProcessParms))
                    {
                        try
                        {
                            this._paramsLogFileExtracData = JsonConvert.DeserializeObject<ProcessRequestLogFileExtracData>(this.ProcessParms);
                        }
                        catch (Exception e)
                        {
                            string msg = String.Format("[{0}] - {1} / {2}", "infoLogFileExtracData", e.Message, e.Source);
                        }
                    }
                }

                return this._paramsLogFileExtracData;
            }
        }

        
    }

    public class ProcessRequestRnD
    {
        public string Instrument { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }

        public string DateFromDeco
        {
            get { return ((this.DateFrom == null) ? "" : ((DateTime)this.DateFrom).ToString("dd/MM/yyyy")); }
        }
        public string DateFromSort
        {
            get { return ((this.DateFrom == null) ? "" : ((DateTime)this.DateFrom).ToString("yyyyMM")); }
        }
        public string DateToDeco
        {
            get { return ((this.DateTo == null) ? "" : ((DateTime)this.DateTo).ToString("dd/MM/yyyy")); }
        }
        public string DateToSort
        {
            get { return ((this.DateTo == null) ? "" : ((DateTime)this.DateTo).ToString("yyyyMMdd")); }
        }
        public InstrumentModel InstrumentData { get; set; }
    }

    public class ProcessRequestInstrumentUpdate
    {
        public string SerialNumber { get; set; }
        public string SiteDescription { get; set; }
        public string SiteCity { get; set; }
        public string Version { get; set; }
        public string VersionDesc { get; set; }
        public string CommercialStatus { get; set; }
        public string Country { get; set; }
        public string CountryDesc { get; set; }
        public string NewSiteCode { get; set; }
        public string ExistingSiteCode { get; set; }
        public string Operation { get; set; }
    }

    public class ProcessRequestLogFileExtracData
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string DateFromDeco
        {
            get { return ((this.DateFrom == null) ? "" : ((DateTime)this.DateFrom).ToString("dd/MM/yyyy")); }
        }
        public string DateToDeco
        {
            get { return ((this.DateTo == null) ? "" : ((DateTime)this.DateTo).ToString("dd/MM/yyyy")); }
        }
    }


}




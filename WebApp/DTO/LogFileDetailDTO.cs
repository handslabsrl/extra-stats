﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class LogFileDetailDTO
    {
        //public LogFileDetailDTO()
        //{
        //    fluorescenceViewSel = Lookup.Claim_Fluorescence_View.Channel_Tm;
        //}
        public Claim claim { get; set; }
        public List<string> scripts { get; set; }
        public List<ClaimPressureSN> pressuresSNfromDB { get; set; }
        public List<ClaimPressureSNDTO> pressuresSN { get; set; }
        public List<ClaimVolumeDTO> volumeEluates { get; set; }
        public List<ClaimPressure> pressures { get; set; }
        public List<ClaimVolumeDTO> inventoryBlock { get; set; }

        public List<ClaimPressure> pressuresOffset { get; set; }
        public List<ClaimTemperature> temperatures { get; set; }
        public List<ClaimTemperature> coolBlockExt { get; set; }
        public List<ClaimDateRifDTO > dateRifList { get; set; }
        public ClaimTemperature firstTemperature { get; set; }
        public ClaimTemperature firstCoolBlockExt { get; set; }

        public List<ClaimFluoDTO>[] fluorescence { get; set; }

        public List<ClaimMessage> messages4tab { get; set; }

        [Display(Name = "Support")]
        public int? TKRequestId { get; set; }

        // 
        public string ChartTitle{ get; set; }
        public string ChartSubTitle { get; set; }
        public List<string> checks { get; set; }
        public bool ChartSerieLeak { get; set; }

        // Filtri 
        public int claimId { get; set; }
        public string scriptSel { get; set; }
        public string folderSel { get; set; }
        public string rowsList { get; set; }
        public string tab { get; set; }
        public DateTime? DateRif { get; set; }
        public int SessionNumber { get; set; }
        public string options { get; set; }     // per gestire EXPORT o altri richieste 
        public Lookup.Claim_Fluorescence_View fluorescenceViewSel { get; set; }

        public int maxSessionNumber { get; set; }
        public string imgLegenda { get; set; }

        [NotMapped]
        public string DateRifDeco
        {
            get {
                if (this.DateRif == null) return "";
                return ((DateTime)this.DateRif).ToString("dd/MM/yyyy");
            }
        }
        [NotMapped]
        public string DateRifDecoMDYY
        {
            get
            {
                if (this.DateRif == null) return "";
                return ((DateTime)this.DateRif).ToString("MM/dd/yyyy");
            }
        }

    }

}


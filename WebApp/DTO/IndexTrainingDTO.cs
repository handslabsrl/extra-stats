﻿
namespace WebApp.Entity
{
    public class IndexTrainingDTO
    {
        public int? trainingIdPreSel { get; set; }
        public bool bShowDeleted { get; set; }
        public bool bShowArchived { get; set; }
    }
}


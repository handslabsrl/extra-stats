﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class LogFileExtracDataRequestDTO
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string RequestType { get; set; }
        public int claimId { get; set; }
        public string manageError { get; set; }

    }

}


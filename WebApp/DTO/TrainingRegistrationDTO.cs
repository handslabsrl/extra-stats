﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    // Estende Model TrainingRegistration !! 
    public class TrainingRegistrationDTO : TrainingRegistration
    {
        public List<Country> countries { get; set; }
        public List<V_Training> trainings { get; set; }
        public List<TrainingTypeOfParticipant> typesOfParticipant { get; set; }
        public List<TrainingTypeOfParticipant> typesOfParticipantITA { get; set; }
        public List<TrainingJobPosition> jobPositions { get; set; }

    }

    // Estende Model TrainingRegistration !! 
    public class AddParticipantDLDTO : TrainingRegistration
    {
        public List<Country> countries { get; set; }
        public List<TrainingJobPosition> jobPositions { get; set; }
        public List<TrainingTypeOfParticipant> typesOfParticipant { get; set; }
    }
}


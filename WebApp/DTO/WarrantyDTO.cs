﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    // Estende Model WSPRequest !! 
    public class WSPRequestDTO : WSPRequest
    {
        public string CodArtDeco { get; set; }
        public string UserInse_Name { get; set; }
        public string OriginalPartDeco { get; set; }
        public string WarrantyStatusDeco { get; set; }
        public string ExchangeRequestDeco { get; set; }
        public string CompliantDeco { get; set; }
        public string WspActionDeco { get; set; }

        public string CompanyName { get; set; }
        public string SiteDescription { get; set; }
        public string SiteCity { get; set; }
        public string SiteCountry { get; set; }
        public string CommercialEntity { get; set; }
        public DateTime? Warranty_From { get; set; }
        public DateTime? Warranty_To { get; set; }
        public string ModelType { get; set; }

        public string WarrantyFromDeco
        {
            get { return ((this.Warranty_From == null) ? "" : ((DateTime)this.Warranty_From).ToString("dd/MM/yyyy")); }
        }
        public string WarrantyFromSort
        {
            get { return ((this.Warranty_From == null) ? "" : ((DateTime)this.Warranty_From).ToString("yyyyMM")); }
        }
        public string WarrantyToDeco
        {
            get { return ((this.Warranty_To == null) ? "" : ((DateTime)this.Warranty_To).ToString("dd/MM/yyyy")); }
        }
        public string WarrantyToSort
        {
            get { return ((this.Warranty_To == null) ? "" : ((DateTime)this.Warranty_To).ToString("yyyyMMdd")); }
        }
        public bool ExpiredWarranty
        {
            get { return ((this.Warranty_To == null) ? false : ((DateTime)this.Warranty_To) < DateTime.Now); }
        }
        public string WarrantyDeco
        {
            get { return ((this.Warranty_To == null) ? "" : (ExpiredWarranty ? Lookup.glb_NO : Lookup.glb_YES)); }
        }
        public bool ExpiredRequestWarranty
        {
            get { return ((this.TmstIssue == null|| this.Warranty_To == null) ? false : ((DateTime)this.Warranty_To) < this.TmstIssue); }
        }
        public string RequestWarrantyDeco
        {
            get { return ((this.TmstIssue == null) ? "" : (ExpiredRequestWarranty ? Lookup.glb_NO : Lookup.glb_YES)); }
        }
        public string OriginalPartInfo
        {
            get
            {
                string ret = "";
                if (!String.IsNullOrWhiteSpace(this.OriginalPart))
                {
                    ret = this.OriginalPartDeco;
                    if (this.OriginalPart.Equals(Lookup.Code_WSP_OriginalPart_NO) && !string.IsNullOrWhiteSpace(this.POReference))
                        ret += string.Format(" {0}", this.POReference);
                }
                return ret;
            }
        }
        public string ModelTypeDeco
        {
            get
            {
                string ret = this.ModelType;
                if (!string.IsNullOrWhiteSpace(this.ModelType))
                {
                    var item = Lookup.InstrumentModelTypeFilters.Find(x => x.id.Equals(this.ModelType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }

    }



    // Estende Model WSPRequest !! 
    public class WSPEditRequestDTO : WSPRequest
    {
        public InstrumentModel instrument { get; set; }
        public bool readOnly { get; set; }
        public string UserInse_Name { get; set; }
        public bool Management { get; set; }
        public List<LookUpString> listWarrantyStatus { get; set; }
        public List<LookUpString> listExchangeRequest { get; set; }
        public List<LookUpString> listCompliant { get; set; }
        public List<LookUpString> listWSPAction { get; set; }
        public Product CodArtProduct { get; set; }
        public List<LookUpString> listClosingOptions { get; set; }
        public string sessionCallerKey { get; set; }
        public bool OutOfWarranty { get; set; }
        public List<LookUpString> listOriginalPart { get; set; }

        // Dati per invio email 
        public int? EmailDLListId { get; set; }
        public string EmailSubject { get; set; }
        public string EmailBody { get; set; }
        public bool EmailCCPurchaseDepartment { get; set; }
        public bool EmailAttachSummary { get; set; }
    }


    public class WSPManageDTO 
    {
        public List<LookUpString> listWarrantyStatus { get; set; }
        public List<LookUpString> listExchangeRequest { get; set; }
        public List<LookUpString> listCompliant { get; set; }
        //public List<LookUpString> listShowClosed { get; set; }
        public List<LookUpString> listWSPAction { get; set; }
        public List<LookUpString> listOriginalPart { get; set; }
    }

    public class WSPIndexDTO
    {
        public List<LookUpString> listInstrumentFilters { get; set; }
    }

    public class WSPStepDTO
    {
        public string Step { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        //public string UserInse_Name { get; set; }
        //public DateTime? TmstInse { get; set; }
        public string Status { get; set; }
        public string StatusDeco { get; set; }

    }

    public class WSPInfoRGADTO
    {
        public string WarrantyCode { get; set; }
        public string DistributorName { get; set; }
        public string InstrumentType { get; set; }
        public string InstrumentSerialNumber { get; set; }
        public string SparePartDescription { get; set; }
        public string SparePartNumber { get; set; }
        public string QuantityOfItems { get; set; }
        public bool   DecontaminationRequired { get; set; }
        public string Name { get; set; }
    }

    public class WSPSummaryDTO
    {
        public string Date_Of_Issue { get; set; }
        public string Warranty_Ref_No { get; set; }
        public string Country { get; set; }
        public string Instrument_sn { get; set; }
        public string Invoice_Date { get; set; }
        public string PNo { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Notification_date_to_EGSPA { get; set; }
        public string End_of_Warranty_period { get; set; }
        public string Warranty { get; set; }
        public string Defective_ReportRow1 { get; set; }
    }


}


﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class DataMiningCreateDTO
    {
        public DataMining dataMining { get; set; }
        //public bool lockCountry { get; set; }
        //public bool lockCommerialeEntity { get; set; }
        public List<string> countriesLink { get; set; }
        public List<string> commercialEntitiesLink { get; set; }
    }

}


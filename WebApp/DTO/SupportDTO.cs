﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;
using WebApp.Classes;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class ClassificationExportDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string BreadCrumb { get; set; }
        public int ParentId { get; set; }
        public Lookup.Classification_RadioGroup? Instrument { get; set; }
        public Lookup.Classification_RadioGroup? OtherPlatform { get; set; }
        public Lookup.Classification_RadioGroup? Product { get; set; }
        public Lookup.Classification_RadioGroup? ClaimLogFile { get; set; }
        public Lookup.Classification_RadioGroup? AdditionalInfo { get; set; }
        public string AdditionalInfoTitle { get; set; }
        public Lookup.Classification_RadioGroup? InfoContact { get; set; }
        public Lookup.Classification_RadioGroup? Quantity { get; set; }
        public Lookup.Classification_RadioGroup? ToDoList { get; set; }
        public Lookup.Classification_RadioGroup? SupplierEngagement { get; set; }
        public Lookup.Classification_RadioGroup? LocationSGAT { get; set; }  // LocationId (tabella Site)
        public Lookup.Classification_RadioGroup? CustomerSGAT { get; set; }  // NON USATO 
        public Lookup.Classification_RadioGroup? ClosingInfo { get; set; }
        public string ClosingInfoTitle { get; set; }
        public bool Reserved { get; set; }  
        public bool Deleted { get; set; }
        public bool? NotifyAreaManager { get; set; } 
    }

    public class AddInfoExportDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string BreadCrumb { get; set; }
        public int ParentId { get; set; }
        public bool Reserved { get; set; }
        public bool Deleted { get; set; }
    }

    public class ClassificationAreaDTO
    {
        public int id { get; set; }
        public string text { get; set; }
        public int cntPending { get; set; }
        public string configId { get; set; }
        public TKSupportConfigModel configModel { get; set; }
    }

    public class IndexRequestDTO : TKRequest
    {
        public string City { get; set; }
        public string Country { get; set; }
        public string UserRegistration_Name { get; set; }
        public string UserRegistration_FirstCustomer { get; set; }
        public string UserInChargeOf_Name { get; set; }
        //public int CntNews { get; set; }
        public int CntNews_User2Support { get; set; }
        public int CntNews_Support2User { get; set; }
        public int CntNews_Support2CCA { get; set; }
        public int CntNews_CCA2Support { get; set; }
        public int Priority { get; set; }
        public DateTime? LastPublicEvt { get; set; }
        public string UserLinkommTracker_Name { get; set; }
        public string CommercialEntity { get; set; }
        public string UserInChargeOfSortPriority
        {
            get
            {
                return string.Format("{0}{1}", this.Priority, this.UserInChargeOf_Name);
            }
        }
        public string UserRegistrationSortPriority
        {
            get
            {
                return string.Format("{0}{1}", this.Priority, this.UserRegistration_Name);
            }
        }
        public string InfoLastAccess
        {
            get
            {
                string ret = "";
                if (this.LastPublicEvt != null && this.UserRegistration_LastAccess != null)
                {
                    if (this.UserRegistration_LastAccess > this.LastPublicEvt)
                    {
                        ret = string.Format("Read at {0}", ((DateTime)this.UserRegistration_LastAccess).ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                }
                return ret;
            }
        }
        public string ComplaintIncidentInfo
        {
            get
            {
                string ret = "";
                if (this.Complaint)
                {
                    ret = this.ComplaintIncidentDeco;
                    if (!string.IsNullOrWhiteSpace(this.ComplaintIncidentNote))
                    {
                        ret = string.Format("{0} - {1}", ret, this.ComplaintIncidentNote);
                    }
                }
                return ret;
            }
        }
        public string ComplaintDeco
        {
            get
            {
                return this.Complaint ? "Yes" : "No";
            }
        }
        public string PercToDoListColor
        {
            get
            {
                string ret = "red";
                if (this.PercToDoList != null)
                {
                    if (this.PercToDoList >= 100) return "green";
                    if (this.PercToDoList >= 75) return "#66b3ff";
                    if (this.PercToDoList >= 50) return "orange";
                    if (this.PercToDoList >= 25) return "magenta";
                    if (this.PercToDoList > 0) return "#ff0066";
                }
                return ret;
            }
        }


    }

    public class AddRequestNoteDTO
    {       
        public int TKRequestId { get; set; }
        public string Note { get; set; }
        public bool Reserved { get; set; }
        public bool isSupport { get; set; }
        public bool CCAreaManager { get; set; }
        public bool FollowRequest { get; set; }
        public string NoteType { get; set; }
    }

    //public class TkToDoItemDTO
    //{
    //    public long TKToDoItemId { get; set; }
    //    public string Title { get; set; }
    //    public int sequence { get; set; }
    //}

    public class TKEditRequestDTO : TKRequest
    {
        public string UserRegistration_Name { get; set; }
        public bool isEnabledNotifyAreaManager { get; set; }
        public List<LookUpString> ListCompaintStatus { get; set; }
        public int ComplaintClassificationRootId { get; set; }
        public List<LookUpString> listNoteType { get; set; }
        [Display(Name = "Note Type")]
        public string NoteType { get; set; }
        public string DraftNote { get; set; }
        public bool bAutoSaveDraft { get; set; }
        public string indexOrigin { get; set; }

        public TKSupportConfigModel ConfigModel { get; set; }

    }

    public class DetailRequestDTO : TKRequest
    {
        [Display(Name = "Classification")]
        public string ClassificationDeco { get; set; }
        public bool showInstrument { get; set; }
        public bool showOtherPlatform { get; set; }
        public bool showCodArt { get; set; }
        public bool showClaimLogFile { get; set; }
        public bool showAddInfo { get; set; }
        public bool showInfoContact { get; set; }
        public bool showQuantity { get; set; }
        public bool showLocation { get; set; }
        public string locationDeco { get; set; }
        public bool showCustomer { get; set; }
        public bool showClosingInfo { get; set; }
        [Display(Name = "Instrument")]
        public string InstrumentDeco { get; set; }
        [Display(Name = "Add.Info")]
        public string AddInfoDeco { get; set; }
        [Display(Name = "Closing Info")]
        public string ClosingInfoDeco { get; set; }
        public string UserRegistration_Name { get; set; }

        public TKSupportConfigModel ConfigModel { get; set; }

    }

    public class ClosingInfoExportDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public string BreadCrumb { get; set; }
        public int ParentId { get; set; }
        public bool Deleted { get; set; }
    }

    public class ViewActivityDTO : DetailRequestDTO
    {
        public List<WebApp.Entity.TKEventDTO> Activities { get; set; }
        public TKSupportConfigModel ConfigModel { get; set; }
    }


    public class StatisticsDTO
    {
        public int TKRequestId { get; set; }
        public string Status { get; set; }
        public string Code { get; set; }
        public string Object { get; set; }
        public string Description { get; set; }

        // Dati Registrazione 
        public string UserRegistration_Name { get; set; }

        public string AreaRegistration { get; set; }
        public string ClassificationRegistration { get; set; }
        public string SerialNumberRegistration { get; set; }
        public string OtherPlatformRegistration { get; set; }
        public string CodArtformRegistration { get; set; }
        public string CityRegistration { get; set; }
        public string CountryRegistration { get; set; }
        public string AddInfoRegistration { get; set; }
        public string AreaAddInfoRegistration { get; set; }

        // Dati chiusura 
        public string Area { get; set; }
        public string Classification { get; set; }
        public string SerialNumber { get; set; }
        public string OtherPlatform { get; set; }
        public string CodArt { get; set; }
        //public string CodArtDesc { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string AddInfo { get; set; }
        public string AreaAddInfo { get; set; }

        public int? MinuteInChargeOf { get; set; }
        public int? MinuteProcessing { get; set; }

        public DateTime? TmstInChargeOf { get; set; }
        public string UserInChargeOf_Name { get; set; }

        public DateTime? TmstClosed { get; set; }
        public string ClosingNote { get; set; }
        public string ClosingInfo { get; set; }
        public string UserClosed_Name { get; set; }

        public DateTime TmstInse { get; set; }

        // Complaint 
        public bool Complaint { get; set; }
        public string ComplaintStatus { get; set; }
        public string DefectObserved { get; set; }
        public string ConsequencesForEndUser { get; set; }
        public string ComplaintStatusNote { get; set; }
        public string ComplaintLotNumber { get; set; }
        public string ComplaintCustomer { get; set; }
        public string ComplaintCity { get; set; }
        public bool ComplaintIncident { get; set; }
        public string ComplaintIncidentNote { get; set; }
        public int? TKComplaintClassificationId { get; set; }
        public string ComplaintLinkommInfo { get; set; }

        public string ConfigId { get; set; }
        public TKSupportConfigModel ConfigModel { get; set; }
        public List<TKSupportConfigModel> TKSupportTypes { get; set; }

        public string DateOfTheRequest
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy");
            }
        }
        public string DateOfTheRequestSort
        {
            get
            {
                return this.TmstInse.ToString("yyyyMMdd");
            }
        }
        public string StatusDeco
        {
            get
            {
                TKRequest req = new TKRequest()
                {
                    Status = this.Status
                };
                return req.StatusDeco;
            }
        }
        public string TmstClosedDeco
        {
            get
            {
                if (this.TmstClosed != null)
                {
                    return ((DateTime)this.TmstClosed).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return "";
            }
        }
        public string TmstClosedSort
        {
            get
            {
                if (this.TmstClosed != null)
                {
                    return ((DateTime)this.TmstClosed).ToString("yyyyMMddHHmmss");
                }
                return "";
            }
        }
        public string TmstInChargeOfDeco
        {
            get
            {
                if (this.TmstInChargeOf != null)
                {
                    return ((DateTime)this.TmstInChargeOf).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return "";
            }
        }
        public string TmstInChargeOfSort
        {
            get
            {
                if (this.TmstInChargeOf != null)
                {
                    return ((DateTime)this.TmstInChargeOf).ToString("yyyyMMddHHmmss");
                }
                return "";
            }
        }
        public string ComplaintDeco
        {
            get
            {
                return this.Complaint ? "Yes" : "No";
            }
        }

        public string ComplaintIncidentDeco
        {
            get
            {
                return this.ComplaintIncident ? "YES" : "NO";
            }
        }

        public string ComplaintIncidentInfo
        {
            get
            {
                string ret = "";
                if (this.Complaint)
                {
                    ret = this.ComplaintIncidentDeco;
                    if (!string.IsNullOrWhiteSpace(this.ComplaintIncidentNote))
                    {
                        ret = string.Format("{0} - {1}", ret, this.ComplaintIncidentNote);
                    }
                }
                return ret;
            }
        }
        public string ComplaintClassificationDeco { get; set; }
        public string ComplaintStatusDeco { get; set; }


        //public string CodArtDeco
        //{
        //    get
        //    {
        //        if (!string.IsNullOrWhiteSpace(this.CodArt))
        //        {
        //            return string.Format("{0} {1}", this.CodArt, this.CodArtDesc);
        //        }
        //        return "";
        //    }
        //}

        //public string MinuteInChargeOfDeco
        //{
        //    get
        //    {
        //        if (this.MinuteInChargeOf != null && this.MinuteInChargeOf != 0)
        //        {
        //            int ore = ((int)this.MinuteInChargeOf / 60);
        //            int minuti = ((int)this.MinuteInChargeOf % 60);
        //            return string.Format("{0}hh {1:00}min", ore, minuti);
        //        }
        //        return "";
        //    }
        //}
        //public string MinuteProcessingDeco
        //{
        //    get
        //    {
        //        if (this.MinuteProcessing != null && this.MinuteProcessing != 0)
        //        {
        //            int ore = ((int)this.MinuteProcessing / 60);
        //            int minuti = ((int)this.MinuteProcessing % 60);
        //            return string.Format("{0}hh {1:00}min", ore, minuti);
        //        }
        //        return "";
        //    }
        //}
    }
    public class TKEventDTO : TKEvent
    {
        public string UserInse_Name { get; set; }
        public string UserInse_Role { get; set; }
        public List<TKAttachment> attachments { get; set; }

        public bool isNewNote { get; set; }

        public string icon
        {
            get
            {
                string iconCommenting = "fa fa-comments-o";
                string iconUser = "fa fa-user-circle";
                string iconSupport = "fa fa-users";
                string iconCCA = "fa fa-tags";
                string iconGeneric = "fa fa-info";
                switch (this.Operation)
                {
                    case Lookup.TKEvent_Operation_CREATE: return iconUser;
                    case Lookup.TKEvent_Operation_CHANGE_USER_REGISTRATION: return iconUser;
                    case Lookup.TKEvent_Operation_CLOSED: return iconSupport;
                    case Lookup.TKEvent_Operation_ASSIGN: return iconSupport;
                    case Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT: return iconUser;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT: return iconSupport;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER: return iconCommenting;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA: return iconCommenting;
                    case Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT: return iconCommenting;
                    case Lookup.TKEvent_Operation_ADDNOTE_CCA: return iconCCA;
                    case Lookup.TKEvent_Operation_SENDEMAIL: return iconSupport;
                    case Lookup.TKEvent_Operation_SUSPEND: return iconSupport;
                    case Lookup.TKEvent_Operation_DELETE: return "fa fa-trash-o";
                    case Lookup.TKEvent_Operation_REMOVEATTACH: return "fa fa-times";
                    case Lookup.TKEvent_Operation_ADDATTACH: return "fa fa-plus-square-o";
                    case Lookup.TKEvent_Operation_UPDATE: return "fa fa-pencil-square-o";
                    case Lookup.TKEvent_Operation_REOPEN: return "fa fa-flag-o";
                    default: return iconGeneric;
                }
            }
        }
        public string iconColor
        {
            get
            {
                string user = "green";
                string support = "blue";
                string cca = "red";
                switch (this.Operation)
                {
                    case Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT: return user;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT: return support;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER: return support;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA: return support;
                    case Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT: return cca;
                    case Lookup.TKEvent_Operation_ADDNOTE_CCA: return cca;
                    default:
                        switch (this.UserInse_Role)
                        {
                            case Lookup.Role_CustomerComplaintAnalyst: return cca;
                            case Lookup.Role_SupportAdministrator: return support;
                            case Lookup.Role_Support: return user;
                            default: return "gray";
                        }
                }
            }
        }
        public string iconColorTarget
        {
            get
            {
                string user = "green";
                string support = "blue";
                string cca = "red";
                switch (this.Operation)
                {
                    case Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT: return support;
                    //case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT: return support;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER: return user;
                    case Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA: return cca;
                    case Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT: return support;
                    //case Lookup.TKEvent_Operation_ADDNOTE_CCA: return cca;
                    default: return "";
                }
            }
        }
        public string initials
        {
            get
            {
                string ret = "?";
                if (!string.IsNullOrWhiteSpace(this.UserInse_Name))
                {
                    var items = this.UserInse_Name.Split(' ');
                    ret = items[0].Substring(0, 1).ToUpper();
                    if (items.Length > 1)
                    {
                        ret += items[1].Substring(0, 1).ToUpper();
                    }
                }
                return ret;
            }
        }
    }
    public class editClassificationDTO : TKClassification
    {
        public TKSupportConfigModel ConfigModel{ get; set; }
    }
    public class editAddInfoDTO : TKAddInfo
    {
        public TKSupportConfigModel ConfigModel { get; set; }
    }
    public class editClosingInfoDTO : TKClosingInfo
    {
        public TKSupportConfigModel ConfigModel { get; set; }
    }

    public class IndexClassificationDTO
    {
        public TKSupportConfigModel ConfigModel { get; set; }
    }

    public class IndexAddInfoDTO
    {
        public TKSupportConfigModel ConfigModel { get; set; }
        public string GroupId { get; set; }
        public string Title { get; set; }
    }

    public class IndexClosingInfoDTO
    {
        public TKSupportConfigModel ConfigModel { get; set; }
    }
}


﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class DLListDTO: DLList
    {
        public string IntUsers { get; set; }
        public string ExtUsers { get; set; }

        public string IntUsers2Export
        {
            get
            {
                return this.IntUsers.Replace("&lt;", "<").Replace("&gt;", ">").Replace(",", Environment.NewLine);
            }
        }
        public string ExtUsers2Export
        {
            get
            {
                return this.ExtUsers.Replace("&lt;", "<").Replace("&gt;", ">").Replace(",", Environment.NewLine);
            }
        }
    }


    public class DLMembersDTO
    {
        public int DLListId { get; set; }
        public string Email { get; set; }
        public string DLType { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
    }



    public class DLIndexDTO
    {
        public List<DLListDTO> liste { get; set; }
        public List<Code> tipiDL { get; set; }
        public string Title { get; set; }
        public string GroupId { get; set; }
        public bool  ExternalMembers { get; set; }
    }
}


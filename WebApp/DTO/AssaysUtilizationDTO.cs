﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class AssaysUtilizationDTO
    {
        //public AssaysUtilizationDTO()
        //{
        //    commercialEntity = new List<string>();
        //}

        public List<AssayUtilizationCounters> series { get; set; }
        public List<AssayUtilizationDetails> details { get; set; }
        public List<CountryModel> countries { get; set; }
        public List<RegionModel> regions { get; set; }
        public List<AreaModel> areas { get; set; }
        public List<CommStatusModel> commercialStatuses { get; set; }
        public List<CommercialEntityModel> commercialEntities{ get; set; }
        public List<LookUpString> modelTypes { get; set; }
        public List<PathogenModel> Pathogens { get; set; }
        public string Area { get; set; }        
        public string commercialStatus { get; set; }
        public List<string> countriesSel { get; set; }          // MultiSelezione
        public List<string> customersSel { get; set; }          // MultiSelezione
        public List<string> commercialEntitiesSel { get; set; } // MultiSelezione
        public List<string> regionsSel { get; set; } // MultiSelezione
        public List<string> citiesSel { get; set; } // MultiSelezione
        public List<string> siteCodesSel { get; set; } // MultiSelezione
        public List<string> instrumentsSel { get; set; } // MultiSelezione
        public List<string> modelTypesSel { get; set; } // MultiSelezione
        public bool? excludeSatSun { get; set; }
        public bool? excludeDismiss { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public DateTime? InstallDateFrom { get; set; }
        public DateTime? InstallDateTo { get; set; }
        public string summary { get; set; }
        public int? SampleType { get; set; }
        public string PathogenName { get; set; }
        public int cntInstruments { get; set; }
        public int CntRunAssayElite { get; set; }
        public int TotRun { get; set; }

        public bool lockRegion { get; set; }
        public bool lockArea { get; set; }
        public bool lockCountry { get; set; }
        public bool lockCommerialEntity { get; set; }
        public bool lockCustomer { get; set; }

        public DateTime tmstUpdCache{ get; set; }
    }
}


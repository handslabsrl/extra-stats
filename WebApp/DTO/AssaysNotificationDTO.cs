﻿using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Entity
{
    [TableName("v_Assay")]
    public class AssayDTO
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [NPoco.Column("Assay.AssayId")]
        public int AssayId { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [NPoco.Column("AssayVersion.AssayId")]
        public int AssayVersionId { get; set; }
        public String AssayCod { get; set; }
        [NPoco.Column("Assay.Attivo")]
        public bool AssayAttivo { get; set; }
        [NPoco.Column("Assay.Validato")]
        public bool AssayValidated { get; set; }
        public String Version { get; set; }
        public bool Obbligatorio { get; set; }
        [NPoco.Column("AssayVersion.Attivo")]
        public bool Attivo { get; set; }
        public String AssayFullName { get; set; }

    }

    public class CreateAssayDTO
    {
        [StringLength(250)]
        public String AssayCod { get; set; }
        public bool AssayAttivo { get; set; }
        public string Note { get; set; }
        public string Tipo { get; set; }
        public string AssayMadre { get; set; }
        public string AssayMadreVersione { get; set; }
        public bool AssayValidated { get; set; }

        public string Analyte { get; set; }
        public string Panel { get; set; }
        public string BusinessType { get; set; }

        public string InstrumentType { get; set; }

        [StringLength(250)]
        public String Version { get; set; }
        public bool Obbligatorio { get; set; }
        public bool VersionAttivo { get; set; }
        public Lookup.AssayVersion_StatusSend? VersionStatusSend { get; set; }
    }

    public class ManlevaDTO
    {
        //public string SerialNumber { get; set; }
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        //public string Site_Description { get; set; }
        //public string Site_City { get; set; }
        //public string Site_Country { get; set; }
        //public string CommercialEntity { get; set; }
        //public string Region { get; set; }
        //public string Area { get; set; }
        //public string Instrument_Model { get; set; }

        public string AssayCod { get; set; }

        public int LetterId { get; set; }
        public string FileName { get; set; }
        public string SignedFileName { get; set; }
        public string SignedUser { get; set; }
        public DateTime? SignedTmst { get; set; }

        public string SignedTmst_Deco
        {
            get
            {
                string ret = "";
                if (this.SignedTmst != null)
                {
                    ret = ((DateTime)this.SignedTmst).ToString("dd/MM/yyyy HH:mm:ss");
                }
                return ret;
            }
        }
        public string SignedTmst_Sort
        {
            get
            {
                string ret = "";
                if (this.SignedTmst != null)
                {
                    ret = ((DateTime)this.SignedTmst).ToString("yyyyMMddHHmmss");
                }
                return ret;
            }
        }
    }
    public class AssayLettersDTO
    {
        public int AssayId { get; set; }
        public Assay Assay { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;
using System.IO;

namespace WebApp.Entity
{
    public class CreateClaimDTO
    {
        private readonly string DirFilesByFTP = "Upload\\FTP";

        public CreateClaimDTO()
        {
            this.filesByFTP = new List<string>();
            //var fileEntries  = Directory.EnumerateFiles(this.FtpPathName, "exportlog*.zip", SearchOption.TopDirectoryOnly);
            //foreach (string item in fileEntries)
            //{
            //    filesByFTP.Add(Path.GetFileName(item));
            //}
        }
        public Claim claim { get; set; }
        public List<string> filesByFTP { get; set; }
        public string fileSelected { get; set; }

        public string FtpPathName
        {
            get
            {
                return System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirFilesByFTP);
            }
        }
    }
}

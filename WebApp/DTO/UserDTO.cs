﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class User
    {
        [Column("ID")]
        public string userId { get; set; }
        public string userName { get; set; }
        [Column("EMAIL")]
        public string userEmail { get; set; }
        public string ruoli { get; set; }
        public string locked { get; set; }
        public string fullName { get; set; }

        public string CommercialEntity { get; set; }
        public string Country { get; set; }
        public string Customer { get; set; }

        public string Department { get; set; }

        public string CompanyName { get; set; }

        public DateTime? TmstLastChangePwd { get; set; }
        public DateTime? TmstInse { get; set; }

        public DateTime? TmstLastActivity { get; set; }  // DA USER CFG 

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string userNameFull
        {
            get
            {
                string ret = this.userName;
                if (!string.IsNullOrWhiteSpace(this.fullName))
                {
                    ret += string.Format(" [{0}]", this.fullName);
                }
                return ret;
            }
        }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string DateInseDeco
        {
            get
            {
                if (this.TmstInse == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstInse).ToString("dd/MM/yyyy");
            }
        }
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string DateInseSort
        {
            get
            {
                if (this.TmstInse == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstInse).ToString("yyyyMMdd");
            }
        }
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string userNameAndEmail
        {
            get
            {
                string ret = this.userName;
                if (!string.IsNullOrWhiteSpace(this.fullName))
                {
                    ret = this.fullName;
                }
                ret += string.Format(" <{0}>", this.userEmail);
                return ret;
            }
        }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string TmstLastActivityDeco
        {
            get
            {
                if (this.TmstLastActivity == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstLastActivity).ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public string TmstLastActivitySort
        {
            get
            {
                if (this.TmstLastActivity == null)
                {
                    return "";
                }
                return ((DateTime)this.TmstLastActivity).ToString("yyyyMMddHHmmss");
            }
        }
    }

    public class Roles
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public int? Progr { get; set; }
    }


    public class UsersIndexDTO
    {
        public List<User> users { get; set; }
        public bool? showLockedUsers { get; set; }
    }

    // Estende USER
    public class UserEditDTO: User
    {
        // Attibuti USER
        //public string Country { get; set; }
        //public string Region { get; set; }
        public string Area { get; set; }
        //public string CommercialEntity { get; set; } 
        public string UserApproval1 { get; set; }
        public string UserApproval2 { get; set; }
        public int?  DepartmentId { get; set; }
        public bool? Master { get; set; }

        public List<String> UserRoles { get; set; }
 
        // elenchi per combo
        public List<CountryModel> countries { get; set; }
        public List<RegionModel> regions { get; set; }
        public List<AreaModel> areas { get; set; }
        public List<CommercialEntityModel> CommercialEntities { get; set; }
        public List<User> approvers { get; set; }
        public List<Department> departments { get; set; }
        public List<DRSection> DRSections { get; set; }
        public List<TKSupportConfigModel> TKSupportTypes { get; set; }

        public List<string> countriesLink { get; set; }
        public List<string> commercialEntitiesLink { get; set; }
        public List<string> customersLink { get; set; }
        public List<int> sectionsLink { get; set; }
        public List<string> regionsLink { get; set; }
        public List<string> tkSupportTypesLink { get; set; }

        public List<CustomerModel> customers { get; set; }

        // Elenco dei RUOLI 
        public List<Roles> roles { get; set; }
    }

    public class UsersForAuthor
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string LinkID { get; set; }
        public string CompanyName { get; set; }
        public string City { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }

        public bool Selected
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.LinkID);
            }
        }
        public String searchText
        {
            get
            {
                string ret = String.Format("{0} - {1}", this.userName, this.fullName).Trim();
                string cmp = String.Format("{0} - {1}", this.CompanyName, this.City).Trim();
                if (cmp.Length > 1)
                {
                    ret += string.Format(" - {0}", cmp);
                }
                if (!string.IsNullOrWhiteSpace(this.Department))
                {
                    ret += String.Format(" [{0}]", this.Department);
                }
                return ret;
            }
        }
        public String fullNameAndEmail
        {
            get
            {
                return String.Format("{0} - {1}", this.fullName, this.Email).Trim();
            }
        }

    }
    public class UsersbyLinkId
    {
        public string userId { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string Email { get; set; }
        public String fullNameAndEmail
        {
            get
            {
                return String.Format("{0} [{1}]", this.fullName, this.Email).Trim();
            }
        }
    }

}


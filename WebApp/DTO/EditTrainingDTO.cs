﻿using Microsoft.AspNetCore.Mvc.Rendering;
using NPoco;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Models;
using WebApp.Repository;

namespace WebApp.Entity
{
    // Estende Model TrainingRegistration !! 
    public class EditTrainingDTO : Training
    {
        public List<TrainingType> trainingTypes { get; set; }
        public List<Hotel> hotels { get; set; }
        public List<TrainingLocation> trainingLocations  { get; set; }
    }
}


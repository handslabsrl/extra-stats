﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;
using WebApp.Classes;
using System.ComponentModel.DataAnnotations;

namespace WebApp.Entity
{
    public class Index3PRequestDTO : TK3PRequest
    {
        public string UserInse_Name { get; set; }
        public string UserInse_CompanyName { get; set; }
        public string SWVersion_Desc { get; set; }
        public int cntIFU { get; set; }
        public int cntEvalForm { get; set; }
    }

    public class TK3PEditRequestDTO : TK3PRequest
    {
        public bool readOnly { get; set; }
        public bool evaluation { get; set; }
        public string UserRegistration_Name { get; set; }
        public bool CreateNewTicket { get; set; }
        public string CompEvalForm { get; set; }
    }

    public class TK3PEventDTO : TK3PEvent
    {
        public string UserInse_Name { get; set; }
    }
}


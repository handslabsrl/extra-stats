﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Repository;

namespace WebApp.Entity
{
    public class DocumentalRepositoryDTO
    {
        public string operation { get; set; }
        public int? DRSectionId { get; set; }
    }


    public class DRUserAuthorizationsDTO
    {
        //public string userId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string CompanyName { get; set; }
        public string City { get; set; }
        public string JobPosition { get; set; }
        //public string Affiliation { get; set; } 
        //public string Status { get; set; }
    }
    public class DRUserAuthorizationsForSessionDTO
    {
        public string UserName { get; set; }
        public string Authorized { get; set; }
    }

    public class Topic
    {
        public int TopicId { get; set; }
        public string Code { get; set; }
        public string Revision { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public string BreadCrumb { get; set; }
        public int SectionId { get; set; }
        public DateTime TmstLastUpdResources { get; set; }

        public string SummaryResources
        {
            get
            {
                return (new DocumentalRepositoryBO()).GetSummaryResources(this.TopicId);
            }
            set { }
        }

        public int? CntResourcesNew { get; set; }
        public bool bResourcesNew { 
            get
            {
                if (this.CntResourcesNew != null)
                {
                    return (this.CntResourcesNew > 0);
                }
                return false;
            }
        }

        public DateTime? ReleasedDate { get; set; }
        public string ReleasedDateDeco
        {
            get
            {
                string ret = "";
                if (this.ReleasedDate != null)
                {
                    ret = ((DateTime)this.ReleasedDate).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string ReleasedDateSort
        {
            get
            {
                string ret = "";
                if (this.ReleasedDate != null)
                {
                    ret = ((DateTime)this.ReleasedDate).ToString("yyyyMMdd");
                }
                return ret;
            }
        }

    }

    public class ResourceTracking
    {
        public string Tracking_Note { get; set; }
        public string operation { get; set; }
        public DateTime TmstInse { get; set; }
        public string FullName { get; set; }

        public int DRResourceId { get; set; }
        public bool? Deleted { get; set; }
        public Lookup.DRResourceType Resource_Type { get; set; }
        public string Resource_Title { get; set; }
        public string Resource_Source { get; set; }

        public int DRTopicId { get; set; }
        public string Code { get; set; }
        public string Revision { get; set; }
        public string Topic_Title { get; set; }
        public string Topic_Note { get; set; }

        public string BreadCrumb { get; set; }

        public string TypeDeco
        {
            get
            {
                DRResource dummy = new DRResource()
                {
                    Type = this.Resource_Type
                };
                return dummy.TypeDeco;
            }
        }
        public string ResourceClass
        {
            get
            {
                DRResource dummy = new DRResource()
                {
                    Type = this.Resource_Type
                };
                return dummy.ResourceClass;
            }
        }
        public string TmstInseDeco
        {
            get
            {
                return this.TmstInse.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public string TopicInfo
        {
            get
            {
                DRTopic dummy = new DRTopic()
                {
                    Code = this.Code,
                    Revision = this.Revision,
                    Title = this.Topic_Title
                };
                return dummy.TopicInfo;
            }
        }

        
        public string SectionAndTopicInfo
        {
            get
            {
                string ret = string.Format("[{0}] {1}", this.BreadCrumb, this.TopicInfo);
                return ret;
            }
        }

        public string ResourceInfo
        {
            get
            {
                string ret = string.Format("{0} {1}", this.TypeDeco, this.Resource_Title);
                if (!this.Resource_Source.ToUpper().Equals(this.Resource_Title.ToUpper()))
                {
                    {
                        ret = string.Format("{0} [{1}]", ret, this.Resource_Title);
                    }
                }
                if (this.Deleted != null && (bool)this.Deleted)
                {
                    ret = string.Format("{0} **DELETED**", ret);
                }
                return ret;
            }
        }
    }
    public class EmailTopic
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
    }
}


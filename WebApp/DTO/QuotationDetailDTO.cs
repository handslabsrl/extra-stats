﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class QuotationDetailDTO
    {

        public Quotation quotation { get; set; }
        public List<QuotationElitePCRKit> ElitePCRKits { get; set; }
        public List<QuotationElitePCRKit> Consumables { get; set; }
        public List<QuotationOpenPCRKit> OpenPCRKits { get; set; }
        public List<QuotationExtraction> Extractions { get; set; }
        public List<string> Elitekit2Add { get; set; }
        public List<string> Elitekit2Del { get; set; }
        public List<string> Openkit2Del { get; set; }
        public bool overflowExtraction { get; set; }
        public List<QuotationsTotal> Totals { get; set; }
        public bool bQManagerTopLevel { get; set; }
    }
}

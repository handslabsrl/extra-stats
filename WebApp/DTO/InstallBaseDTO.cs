﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Repository;
using WebApp.Models;

namespace WebApp.Entity
{
    public class InstallBaseIndexDTO
    {
        public List<LookUpString> mesi { get; set; }
        public string selMese { get; set; }
        public string datatype { get; set; }
        public bool bShowDismissed { get; set; }
    }

    public class InstrumentUpdateRequestDTO
    {
        public string SerialNumber { get; set; }
        public InstrumentModel InstrumentData { get; set; }
        public List<CommStatusModel> commercialStatuses { get; set; }
        //public List<CountryModel> countries { get; set; }
        public List<LookUpString> countries { get; set; }
        public List<LookUpString> versions { get; set; }
        public List<LookUpString> sites { get; set; }

        public string RequestSiteDescription { get; set; }
        public string RequestSiteCity { get; set; }
        public string RequestVersion { get; set; }
        public string RequestCommercialStatus { get; set; }
        public string RequestNewSiteCode { get; set; }
        public string RequestSiteCode { get; set; }
    }
}


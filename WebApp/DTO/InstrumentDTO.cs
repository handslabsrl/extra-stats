﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Models;

namespace WebApp.Entity
{
    public class bookingIndexDTO
    {
        public List<InstrumentModel> instruments { get; set; }
        public string UserNameFull { get; set; }

        public DateTime? DateFromExport { get; set; }
        public DateTime? DateToExport { get; set; }
        public string SerialNumberToExport { get; set; }
    }

    public class BookingDTO : Booking
    {
        public string UserInse_Name { get; set; }
        public string UserApproval_Name { get; set; }
        public bool locked { get; set; }
        public bool IsEditable { get; set; }
        public bool isApprover { get; set; }
        public string ClassName
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.Status) && (this.IsEditable || this.isApprover))
                {
                    if (this.Status.Equals(Lookup.Booking_Status_WAITING))
                        return "daConfermare";
                    if (this.Status.Equals(Lookup.Booking_Status_CONFIRMED))
                        return "confermata";
                }
                return "";
            }
        }
        public string SignLastUpdCurrent { get; set; }
        public string manageError { get; set; }
    }

    public class BookingExportDTO
    {
        public String Room { get; set; }
        public string SerialNumber { get; set; }
        public string InternalCode { get; set; }
        public string Locked { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public DateTime TmstFrom { get; set; }
        public DateTime TmstTo { get; set; }
        public string UserInse { get; set; }
    }
    public class MaintenanceDTO
    {
        // vuota s
    }
    public class UpdateCalibrationDTO
    {
        public string SerialNumber { get; set; }
        public DateTime NewDate { get; set; }
        public string MsgError { get; set; }
    }
    public class UpdateFibersDTO
    {
        public string SerialNumber { get; set; }
        public bool NewPack { get; set; }
        public int FibersNumber { get; set; }
        public string MsgError { get; set; }
    }
    public class RefurbishDTO
    {
        public string SerialNumberFilter { get; set; }
    }
    public class RefurbishRequestDTO: RefurbishRequest
    {
        public string ModelType { get; set; }
        public string CompanyName { get; set; }
        [Column("Site_City")]
        public string SiteCity { get; set; }
        [Column("Site_Description")]
        public string SiteDescription { get; set; }
        [Column("Site_Country")]
        public string SiteCountry { get; set; }
        public string CommercialEntity { get; set; }
        [Column("Commercial_Status")]
        public string CommercialStatus { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string CustomerCode { get; set; }
        [Column("Site_Code")]
        public string SiteCode { get; set; }

        public string UserInseName { get; set; }

        public decimal FinalCost
        {
            get
            {
                return (this.ProcessingCost ?? 0) + (this.SparePartsCost ?? 0) + (this.ResidualValue ?? 0);
            }
        }
        public decimal RefurbishCost
        {
            get
            {
                return (this.ProcessingCost ?? 0) + (this.SparePartsCost ?? 0);
            }
        }

        public string SerialNumberInfo
        {
            get
            {
                string ret = $"{SerialNumber}";
                if (!string.IsNullOrWhiteSpace(CompanyName) || !string.IsNullOrWhiteSpace(SiteDescription))
                {
                    ret += $" ({CompanyName} / {SiteDescription})";
                }
                return ret;
            }
        }
        public string ModelTypeDeco
        {
            get
            {
                string ret = this.ModelType;
                if (!string.IsNullOrWhiteSpace(this.ModelType))
                {
                    var item = Lookup.InstrumentModelTypeFilters.Find(x => x.id.Equals(this.ModelType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }
        public bool NeededByExpired
        {
            get
            {
                bool ret = false;
                if (this.NeededByDate != null && this.NeededByDate > DateTime.MinValue)
                {
                    ret = this.NeededByDate < DateTime.Now && (this.Status.Equals(Lookup.Refurbish_Status_PROCESSING) || this.Status.Equals(Lookup.Refurbish_Status_WAITING));
                }
                return ret;
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using System.Data.SqlClient;
using WebApp.Entity;
using WebApp.Classes;
using WebApp.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Data;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly ILogger _logger;
        private static string connectionString;
        private readonly WebAppDbContext _context;
        private readonly IEmailSender _emailSender;

        public UsersController(ILogger<UsersController> logger, WebAppDbContext context, IEmailSender emailSender)
        {
            _logger = logger;
            _context = context;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }
        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public IActionResult Index(UsersIndexDTO dto)
        {

            //UsersIndexDTO dto = new UsersIndexDTO() { };

            if (dto.showLockedUsers == null)
            {
                dto.showLockedUsers = false;
                string valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_UsersIndex_ShowLocked);
                bool bShow = false;
                if (bool.TryParse(valore, out bShow))
                {
                    dto.showLockedUsers = bShow;
                }
            } else
            {
                HttpContext.Session.SetString(Lookup.glb_sessionkey_UsersIndex_ShowLocked, dto.showLockedUsers.ToString());
            }

            dto.users = (new UsersBO()).Get(ExcludeLocked: (bool)!dto.showLockedUsers );
            List<CustomerModel> clienti = (new CustomersBO()).GetCustomers(); // per decodifica 
            // Lettura RUOLI (lista)
            foreach (var item in dto.users)
            {
                List<string> ruoli = (new UsersBO()).GetRoles(item.userId);
                string sep = "";
                foreach (var ruolo in ruoli)
                {
                    item.ruoli += sep + ruolo;
                    sep = ", ";
                }

                //UserCfg usercfg = _context.UserCfg.Find(item.userId);
                //if (usercfg != null)
                //{
                //    item.Country = usercfg.Country;
                //    item.CommercialEntity = usercfg.CommercialEntity;
                //}
                // Country configurate
                List<string> valori = (new UsersBO()).GetLinks(item.userId, Lookup.UserLinkType.Country);
                sep = "";
                item.Country = "";
                foreach (var valore in valori)
                {
                    item.Country += sep + valore;
                    sep = ", ";
                }
                // Commercial Entity configurate
                valori = (new UsersBO()).GetLinks(item.userId, Lookup.UserLinkType.CommercialEntity);
                sep = "";
                foreach (var valore in valori)
                {
                    item.CommercialEntity += sep + valore;
                    sep = ", ";
                }
                // Customer configurati
                valori = (new UsersBO()).GetLinks(item.userId, Lookup.UserLinkType.Customer);
                sep = "";
                foreach (var valore in valori)
                {
                    item.Customer += sep + clienti.Where(t => t.CustomerCode.Equals(valore)).Select(t => t.CompanyName).FirstOrDefault();
                    sep = ", ";
                }

            }

            return View(dto);
        }


        // GET: Users/ChangeStatus/5
        public IActionResult ChangeStatus(string id)
        {
            (new UsersBO()).ChangeStatus(id);
            return RedirectToAction("Index");
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(String id)
        {
            UserEditDTO dto = new UserEditDTO();
            User user = (new UsersBO()).Get(id).FirstOrDefault();
            PropertyCopier <User, UserEditDTO>.Copy(user , dto); // Copio proprietÓ con stesso nome
            UserCfg usercfg = _context.UserCfg.Find(id);
            if (usercfg != null)
            {
                PropertyCopier<UserCfg, UserEditDTO>.Copy(usercfg, dto);    // Copio proprietÓ con stesso nome
            }
            dto = loadUserEditDTO(id, dto);
            return View(dto);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditPost(String id, string selectedRoles)
        {
            User user = (new UsersBO()).Get(id).FirstOrDefault();
            UserCfg userCfg = _context.UserCfg.Find(id);

            UserEditDTO dto = new UserEditDTO();
            PropertyCopier<User, UserEditDTO>.Copy(user, dto); // Copio proprietÓ con stesso nome

            if (await TryUpdateModelAsync<UserEditDTO>(dto))
            {
                var context = new ValidationContext(dto, serviceProvider: null, items: null);
                var results = new List<ValidationResult>();
                var isValid = Validator.TryValidateObject(dto, context, results);
                if (!isValid)
                {
                    foreach (var validationResult in results)
                    {
                        ModelState.AddModelError("", validationResult.ErrorMessage);
                    }
                }
            }

            if (ModelState.IsValid)
            {
                if (String.IsNullOrWhiteSpace(dto.userEmail))
                {
                    ModelState.AddModelError("userEmail", "The User Email field is required");
                }
            }

            // AGGIORNAMENTO
            if (ModelState.IsValid)
            {
                using (var dbContextTransaction = _context.Database.BeginTransaction())
                {
                    try
                    {
                        // EMAIL 
                        if (!user.userEmail.Equals(dto.userEmail))
                        {
                            (new UsersBO()).setUserEmail(user.userName, dto.userEmail);
                        }

                        // ROLES
                        (new UsersBO()).RemoteAllRoles(id);
                        if (!String.IsNullOrWhiteSpace(selectedRoles))
                        {
                            foreach (string roleId in selectedRoles.Split('|'))
                            {
                                (new UsersBO()).InsertRoles(id, roleId);
                            }
                        }
                        // CONFIG
                        if (userCfg == null)
                        {
                            userCfg = new UserCfg()
                            {
                                UserID = id,
                                TmstInse = DateTime.Now
                            };
                            _context.UserCfg.Add(userCfg);
                            await _context.SaveChangesAsync();
                        }

                        bool bSendEmailConfirmCfg = false;
                        if (userCfg.TmstConfirmCfg == null && !String.IsNullOrWhiteSpace(selectedRoles))
                        {
                            bSendEmailConfirmCfg = true;
                            userCfg.TmstConfirmCfg = DateTime.Now;
                        }

                        PropertyCopier<UserEditDTO, UserCfg>.Copy(dto, userCfg); // Copio proprietÓ con stesso nome
                        userCfg.TmstLastUpd = DateTime.Now;
                        userCfg.UserLastUpd = User.Identity.Name;
                        //userCfg.DepartmentId = dto.DepartmentId;        // non viene ribaltato dalla COPIA
                        //userCfg.Country = ...country
                        //userCfg.CommercialEntity = ... commercial Entity 
                        _context.Update(userCfg);
                        await _context.SaveChangesAsync();

                        // Se indicata, la Country di default viene aggiunta anche nell'elenco delle country 
                        if (!string.IsNullOrWhiteSpace(dto.Country))
                        {
                            if (dto.countriesLink == null)
                            {
                                dto.countriesLink = new List<string>() { dto.Country };
                            } else
                            {
                                if (!dto.countriesLink.Contains(dto.Country)) {
                                    dto.countriesLink.Add(dto.Country);
                                }
                            }
                        }

                        // AGGIORNA ENTITA' CONFIGURATE (MULTISELEZIONE)
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.Country,  dto.countriesLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.CommercialEntity, dto.commercialEntitiesLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.Customer, dto.customersLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.Region, dto.regionsLink, User.Identity.Name);
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.TKSupportType, dto.tkSupportTypesLink, User.Identity.Name);

                        List<string> valori = new List<string>();
                        if (dto.sectionsLink != null)
                        {
                            dto.sectionsLink.ForEach(i => valori.Add(i.ToString()));
                        }
                        (new UsersBO()).UpdateLinks(id, Lookup.UserLinkType.DRSection, valori, User.Identity.Name);

                        // COMMIT !! 
                        dbContextTransaction.Commit();

                        // Invio Email 
                        if (bSendEmailConfirmCfg)
                        {
                            await _emailSender.SendEmailAsync(dto.userEmail, $"Welcome in {Lookup.GetAppTitle}",
                            String.Format("Dear {0},<br><br>You can now start to use {1} dashboard.<br><br>" +
                                          "If you have any questions, please contact us @ {2}<br><br>" +
                                          "Best regards.<br>{3}<br><br>" +
                                          "Note:<br>This is an automatically generated email, thus please do not reply to it.", 
                                          dto.userName, Lookup.GetAppTitle, Lookup.GetSupportEmail, Lookup.GetSupportSign), 
                                         _logger: _logger);
                        }
                    }
                    catch (Exception e)
                    {
                        dbContextTransaction.Rollback();
                        TempData["MsgToLayout"] = String.Format("Error in Update User Action: {0} {1} {2}", e.Message, e.Source, e.InnerException.ToString());
                    }
                    return RedirectToAction("Index");
                }
            }

            dto = loadUserEditDTO(id, dto);

            return View("Edit", dto);
        }

        // ************************************************************************
        // Gestione  Note RUOLI 
        // ************************************************************************
        public ActionResult ManageRoles()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public object GetRoles(DataSourceLoadOptions loadOptions)
        {
            var data = (new UsersBO()).GetAllRoles();
            return DataSourceLoader.Load(data, loadOptions);
        }

        // SALES - solo user validi (no locked e non deleted)
        public List<UsersForAuthor> GetAreaManagers(string country )
        {
            var ret = new List<UsersForAuthor>();
            if (string.IsNullOrWhiteSpace(country))
            {
                return ret;
            }
            List<object> args = new List<object>();
            var Sql = @"SELECT DISTINCT T1.USERID , T1.USERNAME , T1.FULLNAME ,  T1.COMPANYNAME , T1.CITY,  T1.EMAIL,
				                        (SELECT T2.COUNTRY  from V_COUNTRYSALES T2
					                        WHERE T2.USERID = T1.USERID
					                            AND T2.COUNTRY = @countryId
				                        ) AS LINKID
                            FROM V_COUNTRYSALES T1
                        ORDER BY T1.USERNAME";
            args.Add(new { countryId = country });
            try
            {
                using (IDatabase db = Connection)
                {
                    ret = db.Query<UsersForAuthor>(Sql, args.ToArray()).ToList();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("GetAreaManagers.get() - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }
            return ret;
        }
        [HttpGet]
        public IActionResult SetAreaManager(string country, string UserId, bool Selected)
        {
            string msg = "";
            try
            {
                if (Selected)
                {
                    (new UsersBO()).AddUserLink(UserId, Lookup.UserLinkType.AreaManager, country, User.Identity.Name);
                }
                else
                {
                    (new UsersBO()).RemoveUserLink(UserId, Lookup.UserLinkType.AreaManager, country);
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "SetAreaManager", e.Message, e.Source);
            }
            return Json(msg);
        }
        // Altro 
        [HttpPut]
        public IActionResult UpdateRoles(string key, string values)
        {
            Roles rec2Update = (new UsersBO()).GetRole(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.Progr = rec2Update.Progr ?? 0;
                rec2Update.Progr = rec2Update.Progr < 0 ? 0 : rec2Update.Progr = rec2Update.Progr;
                if (!string.IsNullOrWhiteSpace(rec2Update.Note))
                {
                    rec2Update.Note = rec2Update.Note.Length > 250 ? rec2Update.Note.Substring(0, 250) : rec2Update.Note;
                }
                List<object> args = new List<object>();
                var Sql = @"UPDATE ASPNETROLES 
                                   SET NOTE = @note, PROGR = @progr
                                 WHERE ID = @Id";
                args.Add(new { note = rec2Update.Note });
                args.Add(new { progr = rec2Update.Progr });
                args.Add(new { Id = rec2Update.Id });
                try
                {
                    using (IDatabase db = Connection)
                    {
                        db.Execute(Sql, args.ToArray());
                    }
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpGet]
        public object GetMasterUsers(DataSourceLoadOptions loadOptions)
        {
            List<User> data = (new UsersBO()).Get(OnlyMaster: true);
            return DataSourceLoader.Load(data, loadOptions);
        }

        // POST: Users/ImportFromMaster
        [HttpPost]
        public IActionResult ImportFromMaster(string userId, string masterName, string option)
        {
            string masterUserId = (new UsersBO()).GetUserId(masterName); 
            if (!string.IsNullOrWhiteSpace(masterUserId) && !userId.Equals(masterUserId))
            {
                List<object> args = new List<object>();
                args.Add(new { UserId = userId });
                args.Add(new { masterUserId = masterUserId });
                args.Add(new { UserUpd = User.Identity.Name });
                string sql = "";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        if (option.Equals(Lookup.User_CfgOpt_All) || option.Equals(Lookup.User_CfgOpt_Roles))
                        {
                            // ROLES
                            sql = @"DELETE ASPNETUSERROLES WHERE USERID = @UserId";
                            db.Execute(sql, args.ToArray());
                            sql = @"INSERT INTO ASPNETUSERROLES (USERID, ROLEID) 
                            SELECT @UserId, ROLEID
                              FROM ASPNETUSERROLES WHERE USERID = @masterUserId";
                            db.Execute(sql, args.ToArray());

                        }
                        if (option.Equals(Lookup.User_CfgOpt_All) || option.Equals(Lookup.User_CfgOpt_Configuration))
                        {
                            // LINK (escluso l'associazione come Manager Area di COUNTRY 
                            sql = @"DELETE USERLINKS WHERE USERID = @UserId";
                            db.Execute(sql, args.ToArray());
                            sql = @"INSERT INTO USERLINKS (USERID, TYPEID, LINKID, TMSTLASTUPD, USERLASTUPD) 
                            SELECT @UserId, TYPEID, LINKID, GETDATE(), @UserUpd
                              FROM USERLINKS WHERE USERID = @masterUserId 
                                 AND  (TYPEID <> @TypeidAreaManager)";
                            args.Add(new { TypeidAreaManager = Lookup.UserLinkType.AreaManager });
                            db.Execute(sql, args.ToArray());
                            // CFG
                            sql = @"UPDATE USR
                                SET COUNTRY = MST.COUNTRY,
                                    REGION = MST.REGION,
                                    AREA = MST.AREA,
                                    USERAPPROVAL1 = MST.USERAPPROVAL1,
                                    USERAPPROVAL2 = MST.USERAPPROVAL2,
                                    TMSTCONFIRMCFG = MST.TMSTCONFIRMCFG,
                                    COMMERCIALENTITY = MST.COMMERCIALENTITY,
                                    DEPARTMENTID = MST.DEPARTMENTID,
                                    CITY = MST.CITY,
                                    COMPANYNAME = MST.COMPANYNAME,
                                    USERLASTUPD = @UserUpd,
                                    TMSTLASTUPD = GETDATE()
                              FROM USERCFG USR
                              INNER JOIN USERCFG MST ON MST.USERID = @masterUserId
                                WHERE USR.USERID = @UserId";
                            db.Execute(sql, args.ToArray());
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("ImportFromMaster() - ERRORE {0} {1}", e.Message, e.Source));
                    _logger.LogError(string.Format("ImportFromMaster() - SQL {0}", sql));
                    throw;
                }
            }
            return RedirectToAction("Edit", new { id = userId });
        }
        private UserEditDTO loadUserEditDTO(String id, UserEditDTO dto)
        {
            dto.UserRoles = (new UsersBO()).GetRoles(id, "ID");

            dto.countries = (new CountriesBO()).GetCountries(allRegistry: true);
            dto.regions = (new RegionsBO()).GetRegions();
            dto.areas = (new AreasBO()).GetAreas();
            //dto.CommercialEntity = _context.CommercialEntity.ToList();
            dto.CommercialEntities = (new InstrumentsBO()).GetCommercialEntities();
            dto.approvers = (new UsersBO()).Get(filterRoleName: Lookup.Role_QuotationApproval);
            dto.roles = (new UsersBO()).GetAllRoles();
            dto.departments = _context.Departments.OrderBy(t => t.Description).ToList();
            dto.customers = (new CustomersBO()).GetCustomers();   // tutti i clienti presenti in anagrafica
            dto.DRSections = (new DocumentalRepositoryBO()).GetMainSections();
            dto.TKSupportTypes = Lookup.GetTKSupportConfigs();

            // dall'elenco delle country viene esclusa quella di default 
            dto.countriesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Country, ExcludeId: dto.Country);
            dto.commercialEntitiesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.CommercialEntity);
            dto.customersLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Customer);
            dto.regionsLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.Region);
            dto.tkSupportTypesLink = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.TKSupportType);

            dto.sectionsLink = new List<int>();
            var list = (new UsersBO()).GetLinks(id, Lookup.UserLinkType.DRSection);
            list.ForEach(i => dto.sectionsLink.Add(Int32.Parse(i)));

            return dto;

        }
    }
}




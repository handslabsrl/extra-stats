using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using WebApp.Repository;
using System.Collections;
using Microsoft.Extensions.Configuration;
using NPoco;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace WebApp.Controllers
{
    //[Authorize(Roles = "Open AP Third Party - Support Administrator,Open AP Third Party - Orders Department,Open AP Third Party - Research and Development,Open AP Third Party - Quality")]
    [Authorize]
    public class TK3PSupportController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private string connectionString;

        public TK3PSupportController(WebAppDbContext context, ILogger<TK3PSupportController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        [HttpGet]
        public IActionResult SaveManageFilters(bool showAllRequests)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_TK3PRequests_ShowAllRequests, showAllRequests.ToString());
            return Ok("OK");
        }

        // GET: TK3PSupport\Index
        public async Task<IActionResult> Index()
        {
            bool bShow = false;
            string valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TK3PRequests_ShowAllRequests);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowAllRequest"] = bShow;
            }
            return View();
        }

        [HttpGet]
        public object GetAP3Requests(DataSourceLoadOptions loadOptions, bool bGetAllRequests = false)
        {
            string UserIdOwner = User.Identity.Name;
            List<string> StatusIn = new List<string>();

            if (bGetAllRequests)
            {
                StatusIn.Add(Lookup.TK3PRequest_Status_PENDING);
                StatusIn.Add(Lookup.TK3PRequest_Status_REFUSED);
                StatusIn.Add(Lookup.TK3PRequest_Status_PROCESSING);
                StatusIn.Add(Lookup.TK3PRequest_Status_READY);
                StatusIn.Add(Lookup.TK3PRequest_Status_RELEASED);
                StatusIn.Add(Lookup.TK3PRequest_Status_CLOSED);
            } else
            {
                if (User.IsInRole(Lookup.Role_Support))  // Utente
                {
                    StatusIn.Add(Lookup.TK3PRequest_Status_PENDING);
                    StatusIn.Add(Lookup.TK3PRequest_Status_PROCESSING);
                    StatusIn.Add(Lookup.TK3PRequest_Status_READY);
                    StatusIn.Add(Lookup.TK3PRequest_Status_RELEASED);
                }
                if (User.IsInRole(Lookup.Role_OpenAP3P_SupportAdministrator))
                {
                    StatusIn.Add(Lookup.TK3PRequest_Status_PENDING);
                    //StatusIn.Add(Lookup.TK3PRequest_Status_REFUSED);
                    StatusIn.Add(Lookup.TK3PRequest_Status_PROCESSING);
                    StatusIn.Add(Lookup.TK3PRequest_Status_READY);
                    StatusIn.Add(Lookup.TK3PRequest_Status_RELEASED);
                }
                if (User.IsInRole(Lookup.Role_OpenAP3P_ResearchAndDevelopment))
                {
                    StatusIn.Add(Lookup.TK3PRequest_Status_PROCESSING);
                    UserIdOwner = "";
                }
                if (User.IsInRole(Lookup.Role_OpenAP3P_Quality))
                {
                    StatusIn.Add(Lookup.TK3PRequest_Status_READY);
                    UserIdOwner = "";
                }
                if (User.IsInRole(Lookup.Role_OpenAP3P_OrderDepartment))
                {
                    StatusIn.Add(Lookup.TK3PRequest_Status_RELEASED);
                }
            }
            if (User.IsInRole(Lookup.Role_OpenAP3P_OrderDepartment) || User.IsInRole(Lookup.Role_OpenAP3P_SupportAdministrator) ||
                User.IsInRole(Lookup.Role_OpenAP3P_Quality) || User.IsInRole(Lookup.Role_OpenAP3P_ResearchAndDevelopment))
            {
                UserIdOwner = "";
            }
            var data = (new Support3PBO()).GetRequests(UserIdOwner: UserIdOwner, StatusIn: StatusIn);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetAttachments(DataSourceLoadOptions loadOptions, int id, Lookup.TK3PKAttachType FileType)
        {
            List<TK3PAttachment> data = (new Support3PBO()).GetRequestAttachments(requestId: id, FileType: FileType);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetSWVersion(DataSourceLoadOptions loadOptions, int id)
        {
            string currVersion = "";
            if (id > 0)
            {
                TK3PRequest request = _context.TK3PRequests.Find(id);
                if (request != null)
                {
                    currVersion = request.SwVersion ?? "";
                }
            }
            // estraggo le versioni non annullate + quella della richiesta corrente (altrimenti non viene decodificata) 
            var data = _context.V_InGeniusSWVersions.OrderBy(t=>t.Descriz).Where(t=>string.IsNullOrWhiteSpace(t.Flag_Tratt) || t.Cod_Ele == currVersion).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }


        public ActionResult DownloadAttachment(int id)
        {
            string fileNameError = "DownloadAttachment_ERROR.txt";
            TK3PAttachment attach = _context.TK3PAttachments.Find(id);
            if (attach == null)
            {
                return NotFound();
            }
            TK3PRequest request = _context.TK3PRequests.Find(attach.TK3PRequestId);
            if (request == null)
            {
                return NotFound();
            }

            string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, attach.PhysicalFileName);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = attach.Source };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nFile Id: {0}", attach.TK3PAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nRequest Id: {0}", attach.TK3PRequestId);
                msg += String.Format("\nFileName: {0}", attach.PhysicalFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAttachment() function.");
                msg += String.Format("\nFile Id: {0}", attach.TK3PAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        public async Task<FileStreamResult> DownloadIFU(int id)
        {
            string fileNameError = "DownloadIFU_ERROR.txt";
            TK3PRequest request = _context.TK3PRequests.Find(id);
            if (request == null)
            {
                string msg = String.Format("Request {0} Not found", id);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            List<TK3PAttachment> files = _context.TK3PAttachments.Where(t => t.TK3PRequestId == id && t.Type == Lookup.TK3PKAttachType.IFU && t.Deleted == false).ToList();
            if (files.Count == 0)
            {
                string msg = String.Format("No files Found");
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            List<string> files2zip = new List<string>();
            foreach (var item in files)
            {
                string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, item.PhysicalFileName);
                files2zip.Add(fullFileName);
            }
            try
            {
                var outFileName = string.Format("IFU_{0}.zip", request.Code);
                if (!Directory.Exists(request.TempPathName))
                {
                    Directory.CreateDirectory(request.TempPathName);
                }
                //var ZipFileName = string.Format("{0}.zip", Lookup.GetValidFileName(User.Identity.Name));
                var ZipFileName = Utility.GetTempFileName(User.Identity.Name, "zip", request.TempPathName);
                var zipFullFileName = System.IO.Path.Combine(request.TempPathName, ZipFileName);
                if (System.IO.File.Exists(zipFullFileName))
                {
                    System.IO.File.Delete(zipFullFileName);
                }
                List<string> filesOK = ZipFileCreator.CreateZipFile(fileName: zipFullFileName,
                                                                    files: files2zip,
                                                                    strToMask: request.AttachmentsPathName,
                                                                    strToReplace: string.Format("<{0}>", id));
                if (filesOK.Count > 0)
                {
                    var stream = System.IO.File.OpenRead(zipFullFileName);
                    return new FileStreamResult(stream, "application/octet-stream") { FileDownloadName = outFileName };
                }
                string msg = String.Format("No files Found (CreateZipFile)");
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAllResourceTopic() function.");
                msg += String.Format("\nTopic Id: {0}", id);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        
        public  ActionResult DownloadCompEvalForm(int id)
        {
            string fileNameError = "DownloadCompEvalForm_ERROR.txt";
            TK3PRequest request = _context.TK3PRequests.Find(id);
            if (request == null)
            {
                string msg = String.Format("Request {0} Not found", id);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }

            TK3PAttachment attach = _context.TK3PAttachments.Where(t => t.TK3PRequestId == request.TK3PRequestId && t.Type == Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm).FirstOrDefault();
            if (attach == null)
            {
                string msg = String.Format("File not Found");
                msg += String.Format("\nRequest Id: {0}", request.TK3PRequestId);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }

            string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, attach.PhysicalFileName);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = attach.Source };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nFile Id: {0}", attach.TK3PAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nRequest Id: {0}", attach.TK3PRequestId);
                msg += String.Format("\nFileName: {0}", attach.PhysicalFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadTemplateCompEvalForm() function.");
                msg += String.Format("\nFile Id: {0}", attach.TK3PAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }
        public ActionResult DownloadAssayProtocol(int id, bool bCloseAfterDownload = false) 
        {
            string fileNameError = "DownloadAssayProtocol_ERROR.txt";
            TK3PRequest request = _context.TK3PRequests.Find(id);
            if (request == null)
            {
                string msg = String.Format("Request {0} Not found", id);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            if (!request.canDownloadAP)
            {
                string msg = "Operation not allowed because the request was been changed!";
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }

            string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, request.FileNameAP);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    if (request.UserInse.Equals(User.Identity.Name)) {
                        // Invio email solo al primo download da parte dell'utente
                        bool bSendEmail = (0 == _context.TK3PEvents.Count(t => t.TK3PRequestId == request.TK3PRequestId &&
                                                                         t.UserInse.Equals(User.Identity.Name) && t.Operation.Equals(Lookup.TK3PEvent_Operation_DOWNLOADAPFILE)));
                        TK3PEvent evento = new TK3PEvent()
                        {
                            Note = "Download Assay Protocol",
                            TK3PRequestId = request.TK3PRequestId,
                            Operation = Lookup.TK3PEvent_Operation_DOWNLOADAPFILE,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name
                        };
                        _context.TK3PEvents.Add(evento);

                        request.TmstLastUpd = DateTime.Now;
                        request.UserLastUpd = User.Identity.Name;
                        request.TmstLastDownload= DateTime.Now;
                        _context.Update(request);

                        _context.SaveChanges();

                        if (bSendEmail)
                        {
                            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK3P(request.TK3PRequestId, Lookup.Request_NotificationType.DownloadRequestAP3P, userName: User.Identity.Name);
                        }
                        if (bCloseAfterDownload)
                        {
                            SetStatusCloseRequest(id);
                        }
                    }
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = request.FileNameAP };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nRequest Id: {0}", request.TK3PRequestId);
                msg += String.Format("\nFileName: {0}", request.FileNameAP);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAssayProtocol() function.");
                msg += String.Format("\nRequest Id: {0}", request.TK3PRequestId);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }
        private void SetStatusCloseRequest(int id)
        {
            try
            {
                TK3PRequest request = _context.TK3PRequests.Find(id);
                if (request != null)
                {
                    TK3PEvent evento = new TK3PEvent()
                    {
                        Note = "Close request",
                        TK3PRequestId = id,
                        Operation = Lookup.TK3PEvent_Operation_CLOSE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.TK3PEvents.Add(evento);

                    request.Status = Lookup.TK3PRequest_Status_CLOSED;
                    request.TmstLastUpd = DateTime.Now;
                    request.UserLastUpd = User.Identity.Name;
                    _context.Update(request);

                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("CloseRequest() - {0}", e.Message));
            }
        }
        // GET:TK3PSUpport/SetReleaseDateJSON
        public IActionResult  SetReleaseDateJSON(int TK3PRequestId)
        {
            TK3PRequest rec2Update = _context.TK3PRequests.Find(TK3PRequestId);
            if (rec2Update == null)
            {
                return Json(string.Format("Request {0} not found!", TK3PRequestId));
            }
            if (!rec2Update.canSetRelease)
            {
                return Json("Operation not allowed because the request was been changed!");
            }
            rec2Update.Status = Lookup.TK3PRequest_Status_RELEASED;
            rec2Update.TmstRelease = DateTime.Now; 
            rec2Update.TmstLastUpd = DateTime.Now;
            rec2Update.UserLastUpd = User.Identity.Name;
            _context.Update(rec2Update);
            TK3PEvent evento = new TK3PEvent()
            {
                Note = "Set Release",
                TK3PRequestId = rec2Update.TK3PRequestId,
                Operation = Lookup.TK3PEvent_Operation_SETRELEASEDATE,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name
            };
            _context.TK3PEvents.Add(evento);
            _context.SaveChanges();
            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK3P(rec2Update.TK3PRequestId, Lookup.Request_NotificationType.ReleasedRequestAP3P, userName: User.Identity.Name);
            return Json("");
        }


        public ActionResult DownloadTemplateCompEvalForm()
        {
            string fileNameError = "DownloadTemplateCompEvalForm_ERROR.txt";

            TK3PRequest request = new TK3PRequest() { };
            string fullFileName = request.TemplateMod1923Fullname;
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = Path.GetFileName(fullFileName) };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nSource: {0}", fullFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadTemplateCompEvalForm() function.");
                msg += String.Format("\nSource: {0}", fullFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        public IActionResult Create()
        {
            TK3PRequest newReq = new TK3PRequest()
            {
                Status = Lookup.TK3PRequest_Status_DRAFT,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name,
                TmstLastUpd = DateTime.Now,
                UserLastUpd = User.Identity.Name
            };
            _context.TK3PRequests.Add(newReq);
            _context.SaveChanges();

            TK3PEditRequestDTO dto = new TK3PEditRequestDTO();
            PropertyCopier<TK3PRequest, TK3PEditRequestDTO>.Copy(newReq, dto); // Copio proprietÓ con stesso nome

            return View("EditRequest", dto);
        }

        public ActionResult UploadNewIFUFile(int TK3PRequestId)
        {
            try
            {
                TK3PRequest request = _context.TK3PRequests.Find(TK3PRequestId);
                if (request == null)
                {
                    throw new Exception(string.Format("Request {0} not found", TK3PRequestId));
                }
                var AttachmentsPathName = request.AttachmentsPathName;
                if (!Directory.Exists(AttachmentsPathName))
                {
                    Directory.CreateDirectory(AttachmentsPathName);
                }

                var myNewFile = Request.Form.Files["myNewFile"];
                string NewFullFileName = System.IO.Path.Combine(request.AttachmentsPathName, myNewFile.FileName);

                if (System.IO.File.Exists(NewFullFileName))
                {
                    throw new Exception(string.Format("File {0} already exists", myNewFile.FileName));
                }

                // Copia nuovo file 
                using (var fileStream = System.IO.File.Create(NewFullFileName))
                {
                    myNewFile.CopyTo(fileStream);
                }

                TK3PAttachment attachment = new TK3PAttachment()
                {
                    TK3PRequestId = TK3PRequestId,
                    Source = myNewFile.FileName,
                    Deleted = false,
                    Type = Lookup.TK3PKAttachType.IFU,
                    PhysicalFileName = myNewFile.FileName,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.TK3PAttachments.Add(attachment);

                string sAzione = string.Format("Add New FILE - Name: {0}", myNewFile.FileName);
                TK3PEvent evento = new TK3PEvent()
                {
                    Note = sAzione,
                    TK3PRequestId = TK3PRequestId,
                    Operation = Lookup.TK3PEvent_Operation_ADDATTACH,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TK3PEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return BadRequest(string.Format("{0}", e.Message));
            }
            return new EmptyResult();
        }

        [HttpGet]
        public IActionResult DeleteAttachment(int id)
        {
            string msg = "";
            try
            {
                TK3PAttachment attach2upd = _context.TK3PAttachments.Include(t => t.Request).Where(t => t.TK3PAttachmentId == id).FirstOrDefault();
                if (attach2upd != null)
                {
                    string FullFileName = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, attach2upd.Source);
                    if (System.IO.File.Exists(FullFileName))
                    {
                        string FileNameDeleted = string.Format("{0}_{1}_{2}", attach2upd.Source, attach2upd.TK3PAttachmentId, DateTime.Now.ToString("yyyyMMddHHmmss"));
                        string FullFileNameDeleted = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, FileNameDeleted);
                        System.IO.File.Move(FullFileName, FullFileNameDeleted);
                        attach2upd.Source = FileNameDeleted;
                    }
                    attach2upd.Deleted = true;
                    attach2upd.TmstLastUpd = DateTime.Now;
                    attach2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(attach2upd);
                    string sAzione = string.Format("remove {2} {0} - Name: {1}", id, attach2upd.Source, attach2upd.TypeDeco);
                    TK3PEvent evento = new TK3PEvent()
                    {
                        Note = sAzione,
                        TK3PRequestId = attach2upd.TK3PRequestId,
                        Operation = Lookup.TK3PEvent_Operation_REMOVEATTACH,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.TK3PEvents.Add(evento);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteAttachment", e.Message, e.Source);
            }
            return Json(msg);
        }

        [HttpGet]
        public IActionResult DeleteRequest(int Id)
        {
            string msg = "";
            TK3PRequest rec2Update = _context.TK3PRequests.Find(Id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.TK3PRequest_Status_DELETED;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                TK3PEvent evento = new TK3PEvent()
                {
                    Note = "Delete Request",
                    TK3PRequestId = rec2Update.TK3PRequestId,
                    Operation = Lookup.TK3PEvent_Operation_DELETE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TK3PEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteRequest", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public object GetTickets(DataSourceLoadOptions loadOptions)
        {
            var data = (new SupportBO()).GetRequests4AP3P();
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetRequestActivity(DataSourceLoadOptions loadOptions, int requestId)
        {
            var data = _context.TK3PEvents.Where(x => x.TK3PRequestId == requestId)
                                        .OrderByDescending(x => x.TmstInse).ToList();
            List<TK3PEventDTO> dto = new List<TK3PEventDTO>();
            foreach (var item in data)
            {
                TK3PEventDTO newItem = new TK3PEventDTO();
                PropertyCopier<TK3PEvent, TK3PEventDTO>.Copy(item, newItem); // Copio proprietÓ con stesso nome
                newItem.UserInse_Name = (new UsersBO()).GetFullName(newItem.UserInse);
                dto.Add(newItem);
            }
            return DataSourceLoader.Load(dto, loadOptions);
        }
        // GET: TK3PSupport/EditRequest/5
        public async Task<IActionResult> EditRequest(int id, bool readOnly = false, bool evaluation = false)
        {
            TK3PRequest request = _context.TK3PRequests.Where(x => x.TK3PRequestId== id).FirstOrDefault();
            if (request == null)
            {
                return NotFound();
            }
            TK3PEditRequestDTO dto = new TK3PEditRequestDTO();
            PropertyCopier<TK3PRequest, TK3PEditRequestDTO>.Copy(request, dto); // Copio proprietÓ con stesso nome

            dto.readOnly = readOnly;
            dto.evaluation = evaluation;
            dto.UserRegistration_Name = (new UsersBO()).GetFullName(dto.UserInse);

            TK3PAttachment attach = _context.TK3PAttachments.Where(t => t.TK3PRequestId == id && t.Deleted == false &&  t.Type == Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm).FirstOrDefault();
            if (attach != null)
            {
                dto.CompEvalForm = attach.Source;
            }
            return View(dto);
        }
        // POST: TK3PSupport/Edit
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRequest([Bind("TK3PRequestId,Status,SwVersion,CustomerSelection,Target,Manufacturer,PurchaseOrder,TicketId,CreateNewTicket")]TK3PEditRequestDTO dto,
                                         IFormFile newCompEvalForm)
        {
            bool bNewRequest = false;
            TK3PRequest rec2Update = _context.TK3PRequests.Find(dto.TK3PRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (!rec2Update.Status.Equals(dto.Status))
            {
                return BadRequest("Operation not allowed because the request was been changed!");
            }
            if (!CanEditRequest(rec2Update))
            {
                return BadRequest("Operation not allowed because the request was been changed!");
            }
            try
            {
                if (rec2Update.Status.Equals(Lookup.TK3PRequest_Status_DRAFT))
                {
                    rec2Update.Code = getNewCode();
                    rec2Update.Status = Lookup.WSPRequest_Status_PENDING;
                    rec2Update.TmstInse = DateTime.Now;
                    rec2Update.Status = Lookup.TK3PRequest_Status_PENDING;
                    bNewRequest = true;
                }

                rec2Update.SwVersion = dto.SwVersion;
                rec2Update.CustomerSelection = dto.CustomerSelection;
                rec2Update.Target = dto.Target;
                rec2Update.Manufacturer = dto.Manufacturer;
                rec2Update.PurchaseOrder = dto.PurchaseOrder;
                rec2Update.TicketId = dto.CreateNewTicket ? null : dto.TicketId; 
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.TK3PRequests.Update(rec2Update);
                _context.SaveChanges();

                if (bNewRequest)
                {
                    TK3PEvent evento = new TK3PEvent()
                    {
                        Note = "Create new request",
                        TK3PRequestId = rec2Update.TK3PRequestId,
                        Operation = Lookup.TK3PEvent_Operation_CREATE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.TK3PEvents.Add(evento);
                    _context.SaveChanges();
                }

                if (newCompEvalForm != null)
                {
                    SaveCompEvalForm(rec2Update, newCompEvalForm);
                }
                if (dto.CreateNewTicket)
                {
                    TK3PRequest rec2UpdateTicket = _context.TK3PRequests.Find(dto.TK3PRequestId);
                    rec2UpdateTicket.TicketId = CreateNewTicket(rec2UpdateTicket);
                    _context.TK3PRequests.Update(rec2UpdateTicket);
                    _context.SaveChanges();
                }
                if (bNewRequest)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK3P(rec2Update.TK3PRequestId, Lookup.Request_NotificationType.NewRequestAP3P, userName: User.Identity.Name);
                    TempData["MsgToNotify"] = string.Format("Operation successful - Insert request {0}", rec2Update.Code);
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return RedirectToAction("Index");
            //return View(TK3PRequest);
        }
        private int CreateNewTicket(TK3PRequest APrequest)
        {
            string descrizione = string.Format("Third Party Assay Protocols Request {0}", APrequest.Code);
            int classifId = Lookup.GetClassificationId4AP3P();
            TKRequest newRequest = new TKRequest()
            {
                Description = descrizione,
                DescriptionRegistration = descrizione,
                Object = descrizione,
                ObjectRegistration = descrizione,
                TKClassificationId = classifId,
                TKClassificationIdRegistration = classifId,
                Status = Lookup.TKRequest_Status_PENDING,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name,
                TmstLastUpd = DateTime.Now,
                UserLastUpd = User.Identity.Name,
                UserRegistration = APrequest.UserInse
            };
            _context.TKRequests.Add(newRequest);
            TKEvent evento = new TKEvent()
            {
                TKRequestId = newRequest.TKRequestId,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name,
                Note = string.Format("Create new request - AP rif: {0}", APrequest.Code),
                Operation = Lookup.TK3PEvent_Operation_CREATE
            };
            _context.TKEvents.Add(evento);
            _context.SaveChanges();

            return newRequest.TKRequestId;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetNewStatus(int TK3PRequestId,string Status, string newStatus, IFormFile APReadyFile)
        {
            TK3PRequest rec2Update = _context.TK3PRequests.Find(TK3PRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (!rec2Update.Status.Equals(Status) || !rec2Update.canEvaluate)
            {
                TempData["MsgToLayout"] = "Operation not allowed because the request was been changed!";
                //return BadRequest();
            }
            else
            {
                try
                {
                    rec2Update.Status = newStatus;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    rec2Update.UserLastUpd = User.Identity.Name;
                    _context.Update(rec2Update);
                    TK3PEvent evento = new TK3PEvent()
                    {
                        Note = string.Format("Set new Status: {0}", newStatus),
                        TK3PRequestId = rec2Update.TK3PRequestId,
                        Operation = Lookup.TK3PEvent_Operation_SETSTATUS,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.TK3PEvents.Add(evento);
                    _context.SaveChanges();

                    if (newStatus.Equals(Lookup.TK3PRequest_Status_READY) && APReadyFile != null)
                    {
                        SaveAPFile(TK3PRequestId, APReadyFile);
                    }

                    Lookup.Request_NotificationType notificationType = Lookup.Request_NotificationType.None;
                    if (newStatus.Equals(Lookup.TK3PRequest_Status_REFUSED))
                    {
                        notificationType = Lookup.Request_NotificationType.RefuseRequestAP3P;
                    }
                    if (newStatus.Equals(Lookup.TK3PRequest_Status_PROCESSING))
                    {
                        notificationType = Lookup.Request_NotificationType.ApproveRequestAP3P;
                    }
                    if (newStatus.Equals(Lookup.TK3PRequest_Status_READY))
                    {
                        notificationType = Lookup.Request_NotificationType.ReadyRequestAP3P;
                    }
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK3P(rec2Update.TK3PRequestId, notificationType, userName: User.Identity.Name);
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult UploadNewTemplate()
        {
            try
            {
                var myFile = Request.Form.Files["myNewTemplate"];
                TK3PRequest request = new TK3PRequest() { };
                if (!Directory.Exists(request.TemplateMod1923Path))
                {
                    Directory.CreateDirectory(request.TemplateMod1923Path);
                }
                string fullFileName = request.TemplateMod1923Fullname;
                if (System.IO.File.Exists(fullFileName))
                {
                    string NewFileName = string.Format("{0}_{1}", fullFileName, DateTime.Now.ToString("yyyyMMddHHmmss"));
                    System.IO.File.Move(fullFileName, NewFileName);
                }
                // Copia nuovo file 
                using (var fileStream = System.IO.File.Create(fullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                _logger.LogInformation(string.Format("UploadNewTemplate() - {0}", e.Message));
            }

            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult UploadNewAssayFile(int TK3PRequestId)
        {
            try
            {
                TK3PRequest request = _context.TK3PRequests.Find(TK3PRequestId);
                if (request == null || !request.canUploadAssay)
                {
                    return NotFound();
                }
                var myFile = Request.Form.Files["myAssayFile"];
                if (myFile != null)
                {
                    SaveAPFile(TK3PRequestId, myFile);
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK3P(request.TK3PRequestId, Lookup.Request_NotificationType.ReadyRequestAP3P, userName: User.Identity.Name);
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                _logger.LogInformation(string.Format("UploadNewAssayFile() - {0}", e.Message));
            }

            return new EmptyResult();
        }
        void SaveCompEvalForm(TK3PRequest request,  IFormFile file)
        {
            try
            {
                var AttachmentsPathName = request.AttachmentsPathName;
                if (!Directory.Exists(AttachmentsPathName))
                {
                    Directory.CreateDirectory(AttachmentsPathName);
                }

                string FileName = string.Format("{0}_{1}.{2}", request.MOD1923FileName, request.Code, Path.GetExtension(file.FileName).Replace(".",""));
                string FullFileName = System.IO.Path.Combine(request.AttachmentsPathName, FileName);
                if (System.IO.File.Exists(FullFileName))
                {
                    System.IO.File.Delete(FullFileName);
                }
                using (var fileStream = System.IO.File.Create(FullFileName))
                {
                    file.CopyTo(fileStream);
                }

                TK3PAttachment rec2Delete = _context.TK3PAttachments.Where(t => t.TK3PRequestId == request.TK3PRequestId && t.Type == Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm).FirstOrDefault();
                if (rec2Delete != null)
                {
                    _context.TK3PAttachments.Remove(rec2Delete);
                    _context.SaveChanges();
                }

                TK3PAttachment attachment = new TK3PAttachment()
                {
                    TK3PRequestId = request.TK3PRequestId,
                    Source = FileName,
                    Deleted = false,
                    Type = Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm,
                    PhysicalFileName = FileName,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.TK3PAttachments.Add(attachment);

                string sAzione = string.Format("Add New Assay Compatibility Evaluation Form - Name: {0}", file.FileName);
                TK3PEvent evento = new TK3PEvent()
                {
                    Note = sAzione,
                    TK3PRequestId = request.TK3PRequestId,
                    Operation = Lookup.TK3PEvent_Operation_ADDATTACH,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TK3PEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
            }
            Response.StatusCode = 200;
        }

        void SaveAPFile(int TK3PRequestId, IFormFile APFile)
        {
            try
            {

                TK3PRequest rec2Update = _context.TK3PRequests.Find(TK3PRequestId);
                if (rec2Update == null)
                {
                    Response.StatusCode = 500;
                    _logger.LogInformation(string.Format("SaveAPFile() / TK3PRequestId Not FOUND - {0}", TK3PRequestId));
                    return;
                }

                var APFilePathName = rec2Update.AttachmentsPathName;
                if (!Directory.Exists(APFilePathName))
                {
                    Directory.CreateDirectory(APFilePathName);
                }

                string FullFileName = System.IO.Path.Combine(APFilePathName, APFile.FileName);
                if (System.IO.File.Exists(FullFileName))
                {
                    string NewFileName = string.Format("{0}_{1}", FullFileName, DateTime.Now.ToString("yyyyMMddHHmmss"));
                    System.IO.File.Move(FullFileName, NewFileName);
                }
                using (var fileStream = System.IO.File.Create(FullFileName))
                {
                    APFile.CopyTo(fileStream);
                }

                rec2Update.FileNameAP = APFile.FileName;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.TmstRelease = null;
                rec2Update.Status = Lookup.TK3PRequest_Status_READY;
                _context.TK3PRequests.Update(rec2Update);

                string sAzione = string.Format("Upload AP File: - Name: {0}", APFile.FileName);
                TK3PEvent evento = new TK3PEvent()
                {
                    Note = sAzione,
                    TK3PRequestId = TK3PRequestId,
                    Operation = Lookup.TK3PEvent_Operation_UPLOADAPFILE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TK3PEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                _logger.LogInformation(string.Format("SaveAPFile() - TK3PRequestId: {0}, Error: {1}", TK3PRequestId, e.Message));
            }
            Response.StatusCode = 200;
        }

        private bool CanEditRequest(TK3PRequest request)
        {
            bool bCanEdit = false;

            if (request.UserInse.Equals(User.Identity.Name) && 
                (request.Status.Equals(Lookup.TK3PRequest_Status_DRAFT) || request.Status.Equals(Lookup.TK3PRequest_Status_PENDING))) {
                bCanEdit = true;
            }
            if (User.IsInRole(Lookup.Role_OpenAP3P_SupportAdministrator) && request.Status.Equals(Lookup.TK3PRequest_Status_PROCESSING))
            {
                bCanEdit = true;
            }
            return bCanEdit;
        }
        // GET: TK3PSupport/ReopenRequest/5
        public async Task<IActionResult> ReopenRequest(int id)
        {
            TK3PRequest request = _context.TK3PRequests.Where(x => x.TK3PRequestId == id).FirstOrDefault();
            if (request == null)
            {
                return NotFound();
            }
            if (!request.canReopen)
            {
                TempData["MsgToLayout"] = "Operation not allowed because the request was been changed!";
            }
            else
            {
                TK3PRequest newReq = new TK3PRequest()
                {
                    Status = Lookup.TK3PRequest_Status_PENDING,
                    Code = getNewCode(),
                    SwVersion = request.SwVersion,
                    CustomerSelection = request.CustomerSelection,
                    Target = request.Target,
                    Manufacturer = request.Manufacturer,
                    PurchaseOrder = request.PurchaseOrder,
                    TicketId = request.TicketId,
                    TmstInse = DateTime.Now,
                    UserInse = request.UserInse,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.TK3PRequests.Add(newReq);
                TK3PEvent evento = new TK3PEvent()
                {
                    Note = string.Format("Reopen request {0}", request.Code),
                    TK3PRequestId = newReq.TK3PRequestId,
                    Operation = Lookup.TK3PEvent_Operation_CREATE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TK3PEvents.Add(evento);
                _context.SaveChanges();

                // Copia MOD 19.23 
                TK3PAttachment attach = _context.TK3PAttachments.Where(t => t.TK3PRequestId == id && t.Type == Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm).FirstOrDefault();
                if (attach != null)
                {
                    string fullFileNameOrigin = System.IO.Path.Combine(request.AttachmentsPathName, attach.PhysicalFileName);
                    string newFileName = string.Format("{0}_{1}.{2}", newReq.MOD1923FileName, newReq.Code, Path.GetExtension(fullFileNameOrigin).Replace(".", ""));
                    string fullFileNameTarget = System.IO.Path.Combine(newReq.AttachmentsPathName, newFileName);
                    try
                    {
                        if (!Directory.Exists(newReq.AttachmentsPathName))
                        {
                            Directory.CreateDirectory(newReq.AttachmentsPathName);
                        }
                        if (System.IO.File.Exists(fullFileNameOrigin))
                        {
                            System.IO.File.Copy(fullFileNameOrigin, fullFileNameTarget);
                        }
                        TK3PAttachment newAttach = new TK3PAttachment()
                        {
                            TK3PRequestId = newReq.TK3PRequestId,
                            Source = newFileName,
                            Deleted = false,
                            Type = Lookup.TK3PKAttachType.AssayCompatibilityEvaluationForm,
                            PhysicalFileName = newFileName,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name,
                            TmstLastUpd = DateTime.Now,
                            UserLastUpd = User.Identity.Name
                        };
                        _context.TK3PAttachments.Add(newAttach);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        _logger.LogInformation(string.Format("ReopenRequest() - TK3PRequestId: {0}, Error: {1}", id, e.Message));
                    }
                }
            }
            return RedirectToAction("Index");
        }
        // GET: TK3PSupport/CloseRequest/5
        public async Task<IActionResult> CloseRequest(int id)
        {
            TK3PRequest request = _context.TK3PRequests.Where(x => x.TK3PRequestId == id).FirstOrDefault();
            if (request == null)
            {
                return NotFound();
            }
            if (!request.canClose)
            {
                TempData["MsgToLayout"] = "Operation not allowed because the request was been changed!";
            }
            else
            {
                SetStatusCloseRequest(id);
            }
            return RedirectToAction("Index");
        }
        private string getNewCode()
        {
            int maxProgCode = 0;
            TK3PRequest lastCode = _context.TK3PRequests.Where(c => c.Code.StartsWith("O")).OrderByDescending(c => c.Code).FirstOrDefault();
            if (lastCode != null)
            {
                maxProgCode = Int32.Parse(lastCode.Code.Substring(2));
            }
            return String.Format("O{0:D9}", ++maxProgCode);
        }
    }
}

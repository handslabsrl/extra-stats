using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using WebApp.Repository;
using System.Collections;
using Microsoft.Extensions.Configuration;
using NPoco;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Documental Repository,Documental Repository BackOffice")]
    public class DocumentalRepositoryController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private string connectionString;

        private readonly string DirFilesByFTP = "Upload\\FTP_WEBREPOSITORY";

        //private readonly string idCacheCreateRnDExtraction = "CreateRnDExtraction";

        public DocumentalRepositoryController(WebAppDbContext context, ILogger<DocumentalRepositoryController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public IActionResult Index(DocumentalRepositoryDTO dto)
        {
            return View(dto);
        }

        public IActionResult Manage(DocumentalRepositoryDTO dto)
        {
            if (!string.IsNullOrWhiteSpace(dto.operation))
            {
                //if (dto.operation.ToLower().Equals(("DuplicateSection").ToLower()) && dto.DRSectionId != null)
                if (string.Equals(dto.operation, "DuplicateSection", StringComparison.CurrentCultureIgnoreCase) && dto.DRSectionId != null)
                {
                    TempData["MsgToLayout"] = DuplicateSection((int)dto.DRSectionId);
                }
                if (string.Equals(dto.operation, "ExportUserAuthorizations", StringComparison.CurrentCultureIgnoreCase))
                {
                    return ExportUserAuthorizations();
                }

            }
            return View(dto);
        }

        public ActionResult ExportUserAuthorizations()
        {

            var allUsers = (new UsersBO()).Get(ExcludeDeleted: true, ExcludeLocked: true).OrderBy(o=>o.userName);

            List<DRUserAuthorizationsDTO> users = new List<DRUserAuthorizationsDTO>();
            foreach (var item in allUsers)
            {
                DRUserAuthorizationsDTO newRec = new DRUserAuthorizationsDTO();
                PropertyCopier<User, DRUserAuthorizationsDTO>.Copy(item, newRec); // Copio propriet� con stesso nome
                newRec.UserId = item.userName;
                newRec.Name = item.fullName;
                newRec.JobPosition = item.Department;
                users.Add(newRec);
            }

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("User Authorizations");
            workSheet.Cells[1, 1].LoadFromCollection(users, true);

            int firstSection = 6;   // Posizione partenza per aggiunta altre INFO 
            using (var range = workSheet.Cells[1, 1, 1, firstSection-1 ])
            {
                range.Style.Font.Bold = true;
            }

            List<DRSection> allSections = ((new DocumentalRepositoryBO()).GetSectionAuthorizables());
            int mypos = firstSection;
            foreach (var liv1 in allSections.Where(t=>t.ParentId is null).OrderBy(t=>t.Title))
            {
                List<DRUserAuthorizationsForSessionDTO> authsLiv1 = (new DocumentalRepositoryBO()).GetUserAuthorizationsForSession(liv1.DRSectionId)
                                                                                              .OrderBy(o=>o.UserName).ToList();
                var listWithoutCol = authsLiv1.Select(x => new { x.Authorized }).ToList();
                workSheet.Cells[1, mypos].LoadFromCollection(listWithoutCol, true);
                workSheet.Cells[1, mypos].Value = liv1.Title;

                using (var range = workSheet.Cells[1, mypos, 1, mypos])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Font.Size = 16;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                    range.Style.Font.Color.SetColor(Color.Yellow);
                }
                using (var range = workSheet.Cells[1, mypos, 1 + users.Count(), mypos])
                {
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                mypos++;

                foreach (var liv2 in allSections.Where(t => t.ParentId == liv1.DRSectionId).OrderBy(t => t.Title))
                {
                    List<DRUserAuthorizationsForSessionDTO> authsLiv2 = (new DocumentalRepositoryBO()).GetUserAuthorizationsForSession(liv2.DRSectionId)
                                                                                                   .OrderBy(o => o.UserName).ToList();
                    // Scorro authsLiv1 e se � Authorized, lascio il valore di LIV2, altrimenti forzo 'No' !!! ???
                    for (int i = 0; i < authsLiv1.Count; i++)
                    {
                        if (string.IsNullOrWhiteSpace(authsLiv1[i].Authorized) && !authsLiv1[i].Authorized.ToUpper().Equals(("Authorized").ToUpper()))
                        {
                            authsLiv2[i].Authorized = "No";
                        }
                    }
                    var listWithoutCol2 = authsLiv2.Select(x => new { x.Authorized }).ToList();
                    workSheet.Cells[1, mypos].LoadFromCollection(listWithoutCol2, true);
                    workSheet.Cells[1, mypos].Value = liv2.Title;

                    using (var range = workSheet.Cells[1, mypos, 1, mypos])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Font.Size = 12;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                        range.Style.Font.Color.SetColor(Color.White);
                    }
                    using (var range = workSheet.Cells[1, mypos, 1 + users.Count(), mypos])
                    {
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                    mypos++;
                }
            }

            using (var range = workSheet.Cells[2, firstSection, users.Count() + 1, firstSection + allSections.Count - 1])
            {
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            }
            for (int row = 2; row <= users.Count() + 1; row++)
            {
                for (int col = firstSection; col <= firstSection + allSections.Count -1; col++)
                {
                    object cellValue = workSheet.Cells[row, col].Text;
                    if (!string.IsNullOrWhiteSpace(cellValue.ToString()))
                    {
                        workSheet.Cells[row, col].Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        if (string.Compare(cellValue.ToString(), "Authorized", true) == 0) 
                        {
                            workSheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGreen);
                        }
                        else
                        {
                            workSheet.Cells[row, col].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                        }
                    }
                }
            }

            // AUTO FIT 
            for (int i = 1; i <= 99; i++)
            {
                workSheet.Column(i).AutoFit();
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", "UserAuthorizations.xlsx");
        }

        public IActionResult Journal(DocumentalRepositoryDTO dto)
        {
            return View(dto);
        }

        [HttpGet]
        public object GetTopicsUnlockedAuth(DataSourceLoadOptions loadOptions, int SectionId = 0, string FileName = "")
        {
            // Estra solo i TOPIC per le sezioni configurate 
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            var data = (new DocumentalRepositoryBO()).GetTopics(SectionId: SectionId, FileName: FileName, UserIdCfg: UserId, bGetResourcesNew: true, bOnlyAuthorized: true);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetTopicsToManage(DataSourceLoadOptions loadOptions, int SectionId = 0, string FileName = "", bool showSubSections = true)
        {
            // Estrae tutti i TOPIC, anche quelli LOCKED 
            var data = (new DocumentalRepositoryBO()).GetTopics(SectionId: SectionId, FileName: FileName, bAllTopics: true, showSubSections: showSubSections);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetTopicResources(DataSourceLoadOptions loadOptions, int id)
        {
            var data = (new DocumentalRepositoryBO()).GetResourcesWithLastGet(TopicId: id, UserName: User.Identity.Name);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetJournal(DataSourceLoadOptions loadOptions, int SectionId, string FileName, bool OnlyLastGet, bool showDeleted)
        {
            // Estrae tutti i TOPIC, anche quelli LOCKED 
            var data = (new DocumentalRepositoryBO()).getResourceTrackings(SectionId, FileName, OnlyLastGet, showDeleted);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetSectionsUnlocked(DataSourceLoadOptions loadOptions)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            //var data1 = (new DocumentalRepositoryBO()).GetSectionAuthorizables(userId: UserId, onlyAuthorized: true, bAddRoot: true, bOnlyActive: true, bGetSubSections: false);
            var data = (new DocumentalRepositoryBO()).GetUserSections(UserIdCfg: UserId);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetAllSections(DataSourceLoadOptions loadOptions, int excludeSectionId = 0, bool bExpandAll = false, bool bShowCounter = true, int SelectedId = 0)
        {
            var data = (new DocumentalRepositoryBO()).GetSections(bShowCounter: bShowCounter, excludeSectionId: excludeSectionId, bExpandAll: bExpandAll, SelectedId: SelectedId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetAllSectionAuthorizables(DataSourceLoadOptions loadOptions, string userId)
        {
            var data = (new DocumentalRepositoryBO()).GetSectionAuthorizables(userId);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetUsersForAuthor(DataSourceLoadOptions loadOptions, int sectionId = -1)
        {
            var data = (new DocumentalRepositoryBO()).GetUsersForAuthor(sectionId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        // OpenResource: legge i dati delle risorsa e imposta la lettura
        public JsonResult OpenResourceJSON(int ResourceId)
        {
            DRResource data = _context.DRResources.Find(ResourceId);
            if (data == null)
            {
                return Json("");
            }
            ImpostaLetturaRisorsa(ResourceId);
            return Json(data);
        }


        // GetTopicJSON
        public JsonResult GetTopicJSON(int TopicId, bool bGetEmailInfo = false)
        {
            DRTopic data = _context.DRTopics.Find(TopicId);
            if (data == null)
            {
                return Json("");
            }
            if (bGetEmailInfo)
            {
                data.EmailBody = getEmailBodyTopic(data);
            }
            return Json(data);
        }

        private string getEmailBodyTopic(DRTopic data)
        {
            EmailTemplate email = _context.EmailTemplates.Find(Lookup.Documental_Email_Notification);
            if (email == null)
            {   
                return String.Format("Email {0} Not Found", Lookup.Documental_Email_Notification);
            }
            string ret = email.Body;

            DRSection section = _context.DRSections.Find(data.DRSectionId);

            Dictionary<string, string> valori = new Dictionary<string, string>();
            valori.Add("$TITLE$", data.Title);
            valori.Add("$CODE$", data.Code);
            valori.Add("$REVISION$", data.Revision);
            valori.Add("$SECTION$", section.Title);
            string valore = "";
            foreach (var item in valori)
            {
                valore = "";
                if (!string.IsNullOrWhiteSpace(item.Value))
                {
                    valore = item.Value.Replace("\n\r", "<br/>").Replace("\n", "<br/>").Replace("\r", "<br/>");
                }
                if (!String.IsNullOrWhiteSpace(ret))
                {
                    ret = ret.Replace(item.Key, valore);
                }
            }
            return ret;
        }

        // GetResourceJSON
        public JsonResult GetResourceJSON(int ResourceId)
        {
            DRResource data = _context.DRResources.Find(ResourceId);
            if (data == null)
            {
                return Json("");
            }
            return Json(data);
        }


        private void ImpostaLetturaRisorsa(int ResourceId)
        {
            DRTracking newTrack = new DRTracking()
            {
                ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                ExtId = ResourceId,
                Operation = Lookup.DRTracking_Operation_GET,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name
            };
            _context.DRTrackings.Add(newTrack);
            _context.SaveChanges();
        }

        [HttpGet]
        public IActionResult SetLockSection(int SectionId)
        {
            IActionResult ret = Ok("OK");
            var section = _context.DRSections.Find(SectionId);
            if (section != null)
            {
                dynamic retExec = null;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        retExec = db.Fetch<dynamic>(";EXEC DRSetSectionLocked @id, @Locked", new { id = SectionId, Locked = !section.Locked });
                    }
                }
                catch (Exception e)
                {
                    var msgErr = string.Format("SetLockSection() - ERRORE {0} {1}", e.Message, e.Source);
                    _logger.LogError(msgErr);
                    ret = BadRequest(msgErr);
                }

            }
            return ret;
        }

        //[HttpGet]
        //public IActionResult DuplicateSection(int SectionId)
        //{
        //    string retDuplicate = "";
        //    var section = _context.DRSections.Find(SectionId);
        //    if (section!=null)
        //    {
        //        retDuplicate = runDuplicateSection(section, section.ParentId, true);
        //    }
        //    // Rigenera struttura percorsi (PATH) 
        //    string retLoad = LoadSectionPaths();
        //    return Json(string.Format("{0} {1}", retDuplicate, retLoad).Trim());
        //}

        private string DuplicateSection(int SectionId)
        {
            string retDuplicate = "";
            var section = _context.DRSections.Find(SectionId);
            if (section != null)
            {
                retDuplicate = runDuplicateSection(section, section.ParentId, true);
            }
            // Rigenera struttura percorsi (PATH) 
            string retLoad = LoadSectionPaths();
            return string.Format("{0} {1}", retDuplicate, retLoad).Trim();
        }


        private string runDuplicateSection(DRSection origSection, int? ParentId, bool bFirst)
        {
            string ret = "";

            try
            {
                DRSection newSection = new DRSection()
                {
                    Title = origSection.Title,
                    Locked = origSection.Locked,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name, 
                    ParentId = ParentId
                };
                if (bFirst)
                {
                    newSection.Title += " [COPY]";
                    if (newSection.Title.Length > 200)
                    {
                        newSection.Title = newSection.Title.Substring(0, 200);
                    }
                }            
                _context.DRSections.Add(newSection);
                _context.SaveChanges();

                // Duplica TOPIC
                List<DRTopic> topics = _context.DRTopics.Where(t => t.DRSectionId == origSection.DRSectionId).ToList();
                foreach(var origTopic in topics)
                {
                    DRTopic newTopic = new DRTopic()
                    {
                        Code = normalizeString(origTopic.Code),
                        Revision = origTopic.Revision,
                        Title = origTopic.Title,
                        DRSectionId = newSection.DRSectionId,
                        Note = origTopic.Note,
                        NotifyChange = origTopic.NotifyChange,
                        ReleasedDate = origTopic.ReleasedDate,
                        TmstLastUpdResources = DateTime.MinValue,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.DRTopics.Add(newTopic);
                    _context.SaveChanges();

                    // Duplicate RESOURCE !?!?!?
                    List<DRResource> resources = _context.DRResources.Where(t => t.DRTopicId == origTopic.DRTopicId).ToList();
                    foreach (var origResource in resources)
                    {
                        DRResource newResource = new DRResource()
                        {
                            DRTopicId = newTopic.DRTopicId,
                            Source = origResource.Source,
                            Title = origResource.Title,
                            Type = origResource.Type,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name,
                            TmstLastUpd = DateTime.Now,
                            UserLastUpd = User.Identity.Name,
                            Deleted = origResource.Deleted
                        };
                        _context.DRResources.Add(newResource);
                        _context.SaveChanges();
                    }
                    // COPIA TUTTI I FILE DA CARTELLA ORIGINALE 
                    if (Directory.Exists(origTopic.ResourcesPathName))
                    {
                        Directory.CreateDirectory(newTopic.ResourcesPathName);
                        foreach (var file in Directory.GetFiles(origTopic.ResourcesPathName))
                        {
                            System.IO.File.Copy(file, System.IO.Path.Combine(newTopic.ResourcesPathName, Path.GetFileName(file)));
                            //System.IO.File.Copy(file, Path.Combine(newTopic.ResourcesPathName, Path.GetFileName(file)));
                        }
                    }
                }

                // Duplica sotto sezioni
                List<DRSection> children = _context.DRSections.Where(t => t.ParentId == origSection.DRSectionId).ToList();
                children.ForEach(o => runDuplicateSection(o, newSection.DRSectionId, false));
            }
            catch (Exception e)
            {
                ret = String.Format("[{0}] - {1} / {2}", "runDuplicateSection", e.Message, e.Source);
            }
            return ret;
        }

        [HttpPost]
        public async Task<IActionResult> CreateSection(int ParentId, string Title)
        {
            string msg = "";
            try
            {
                Title = normalizeString(Title);
                if (Title.Length > 200 )
                {
                    Title = Title.Substring(0, 200);
                }
                DRSection section = new DRSection()
                {
                    Title = Title,
                    Locked = false,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                if (ParentId > 0)
                {
                    section.ParentId = ParentId;
                }
                _context.DRSections.Add(section);
                _context.SaveChanges();
                // Rigenera struttura percorsi (PATH) 
                msg = LoadSectionPaths();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "AddSection", e.Message, e.Source);
            }

            return Json(msg);
        }
        [HttpPost]
        public async Task<IActionResult> EditSection(int SectionId, string Title)
        {
            string msg = "";
            try
            {
                Title = normalizeString(Title);
                if (Title.Length > 200)
                {
                    Title = Title.Substring(0, 200);
                }
                DRSection section = _context.DRSections.Find(SectionId);
                if (section != null)
                {
                    section.Title = Title;
                    section.TmstLastUpd = DateTime.Now;
                    section.UserLastUpd = User.Identity.Name;
                    _context.Update(section);
                    await _context.SaveChangesAsync();
                }
                // Rigenera struttura percorsi (PATH) 
                msg = LoadSectionPaths();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "AddSection", e.Message, e.Source);
            }

            return Json(msg);
        }
        [HttpGet]
        public IActionResult MoveSection(int SectionId, int TargetParentId)
        {
            string msg = "";
            bool bSyncAuthor = false;
            try
            {
                DRSection section = _context.DRSections.Find(SectionId);
                if (section != null)
                {
                    if (TargetParentId > 0)
                    {
                        section.ParentId = TargetParentId;
                    } else
                    {
                        section.ParentId = null;
                    }
                    section.TmstLastUpd = DateTime.Now;
                    section.UserLastUpd = User.Identity.Name;
                    _context.Update(section);
                    _context.SaveChanges();
                    bSyncAuthor = true;
                }
                // Allineamento autorizzazioni utenti
                if (bSyncAuthor)
                {
                    (new UsersBO()).SyncSessionAuthorization();
                }
                // Rigenera struttura percorsi (PATH) 
                msg = LoadSectionPaths();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "MoveSection", e.Message, e.Source);
            }
            return Json(msg);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteSection(int SectionId)
        {
            string msg = "";
            try
            {
                DRSection section = _context.DRSections.Find(SectionId);
                if (section != null)
                {
                    List<Topic> data = (new DocumentalRepositoryBO()).GetTopics(SectionId: section.DRSectionId, bAllTopics: true);
                    List<string> path2Del = new List<string>();
                    List<int> topics2Del = new List<int>();
                    List<int> resources2Del = new List<int>();
                    List<int> sections2Del = new List<int>() { section.DRSectionId };
                    foreach (var item in data)
                    {
                        topics2Del.Add(item.TopicId);
                        path2Del.Add(_context.DRTopics.Find(item.TopicId).ResourcesPathName);
                        if (!sections2Del.Contains(item.SectionId))
                        {
                            sections2Del.Add(item.SectionId);
                        }
                    }
                    resources2Del = _context.DRResources.Where(p => topics2Del.Contains(p.DRTopicId)).Select(p => p.DRResourceId).ToList();

                    var sezioni = _context.DRSections.Where(p => sections2Del.Contains(p.DRSectionId)).ToList();
                    _context.RemoveRange(sezioni);
                    await _context.SaveChangesAsync();
                    // cancella Track e cartella risorse 
                    ClearTrackingAndDirectoryTopic(topics2Del, path2Del, resources2Del);
                }
                // Rigenera struttura percorsi (PATH) 
                msg = LoadSectionPaths();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteSection", e.Message, e.Source);
            }
            return Json(msg);
        }

        [HttpGet]
        public IActionResult DeleteTopic(int TopicId)
        {
            string msg = "";
            try
            {
                DRTopic rec2del = _context.DRTopics.Find(TopicId);
                if (rec2del != null)
                {
                    List<int> topics2Del = new List<int>() { TopicId };
                    List<int> resources2Del = _context.DRResources.Where(p => p.DRTopicId == TopicId).Select(p => p.DRResourceId).ToList();
                    List<string> path2Del = new List<string> { _context.DRTopics.Find(TopicId).ResourcesPathName };
                    _context.Remove(rec2del);
                    _context.SaveChanges();

                    // cancella Track e cartella risorse 
                    ClearTrackingAndDirectoryTopic(topics2Del, path2Del, resources2Del);
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteTopic", e.Message, e.Source);
            }
            return Json(msg);
        }

        
        [HttpGet]
        public IActionResult DeleteResource(int ResourceId)
        {
            string msg = "";
            try
            {
                DRResource resource2upd = _context.DRResources.Include(t => t.Topic).Where(t => t.DRResourceId == ResourceId).FirstOrDefault();
                if (resource2upd != null)
                {
                    if (resource2upd.Type == Lookup.DRResourceType.file || resource2upd.Type == Lookup.DRResourceType.attachment)
                    {
                        string FullFileName = System.IO.Path.Combine(resource2upd.Topic.ResourcesPathName, resource2upd.Source);
                        if (System.IO.File.Exists(FullFileName))
                        {
                            string FileNameDeleted = string.Format("{0}_{1}_{2}", resource2upd.Source, resource2upd.DRResourceId, DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string FullFileNameDeleted = System.IO.Path.Combine(resource2upd.Topic.ResourcesPathName, FileNameDeleted);
                            System.IO.File.Move(FullFileName, FullFileNameDeleted);
                            resource2upd.Source = FileNameDeleted;
                        }
                    }
                    resource2upd.Deleted = true;
                    resource2upd.TmstLastUpd = DateTime.Now;
                    resource2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(resource2upd);
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource2upd.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_DELETE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteResource", e.Message, e.Source);
            }
            return Json(msg);
        }

        private string LoadSectionPaths()
        {
            string msgErr = "";
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC DRLoadSectionPaths");
                }
            }
            catch (Exception e)
            {
                msgErr = String.Format("[{0}] - {1} / {2}", "LoadSectionPaths", e.Message, e.Source);
                _logger.LogError(msgErr);
            }
            return msgErr;
        }

        // GET: DocumentalRepository/EditTopic/5
        public async Task<IActionResult> EditTopic(int TopicId = 0, int SectionId = 0)
        {
            DRTopic dto = new DRTopic();
            dto.DRSectionId = SectionId;
            if (TopicId > 0)
            {
                dto = _context.DRTopics.Find(TopicId);
                if (dto == null)
                {
                    return BadRequest();
                }
            }
            return PartialView("_EditTopic", dto);
        }


        [HttpGet]
        public IActionResult EditResourceLink(int TopicId, string Link, string Title, int ResourceId)
        {
            string msg = "";
            try
            {
                DRTopic topic = _context.DRTopics.Find(TopicId);
                if (topic == null)
                {
                    throw new Exception(string.Format("Topic {0} not found", TopicId));
                }
                // se resourceId � valorizzato, devo fare una sostituzione 
                string SourcePrec = "";
                string TitlePrec = "";
                DRResource resource2upd = new DRResource();
                if (ResourceId != 0)
                {
                    resource2upd = _context.DRResources.Find(ResourceId);
                    if (resource2upd == null)
                    {
                        throw new Exception(string.Format("Resource {0} not found", ResourceId));
                    }
                    SourcePrec = resource2upd.Source;
                    TitlePrec = resource2upd.Title;
                }

                Title = normalizeString(Title);
                if (Link.Length > 200)
                {
                    Link = Link.Substring(0, 200);
                }
                if (String.IsNullOrWhiteSpace(Title))
                {
                    Title = Link;
                } else if (Title.Length > 200)
                {
                    Title = Title.Substring(0, 200);
                }


                // Aggiornamento/Inserimento 
                bool changeLink = false;
                if (ResourceId == 0)
                {
                    changeLink = true;
                    // NUOVA RISORSA 
                    DRResource resource = new DRResource()
                    {
                        DRTopicId = topic.DRTopicId,
                        Source = Link,
                        Title = Title,
                        Type = Lookup.DRResourceType.url,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        TmstLastUpd = DateTime.Now,
                        UserLastUpd = User.Identity.Name
                    };
                    _context.DRResources.Add(resource);
                    _context.SaveChanges();
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_PUT,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
                else
                {
                    // AGGIORNAMENTO RISORSA ESISTENTE
                    if (!resource2upd.Source.ToUpper().Equals(Link.ToUpper())) {
                        changeLink = true;
                        resource2upd.Source = Link;
                    }
                    resource2upd.Title = Title;
                    resource2upd.TmstLastUpd = DateTime.Now;
                    resource2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(resource2upd);
                    _context.SaveChanges();
                    if (changeLink)
                    {
                        DRTracking newTrack = new DRTracking()
                        {
                            ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                            ExtId = resource2upd.DRResourceId,
                            Operation = Lookup.DRTracking_Operation_UPDATE,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name,
                            Note = string.Format("Change Link from '{0}' to '{1}', Title from '{2}' to '{3}'", SourcePrec, Link, TitlePrec, Title)
                        };
                        _context.DRTrackings.Add(newTrack);
                        _context.SaveChanges();
                    }
                }
                // Aggiorna Data LastUpd Resource su topic (per ordinamento WebRepository)
                if (changeLink)
                {
                    topic.TmstLastUpdResources = DateTime.Now;
                    _context.Update(topic);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = string.Format("{0}", e.Message);
            }
            return Json(msg);
        }

        // GET: DocumentalRepository/EditResources/5
        public async Task<IActionResult> EditResources(int TopicId)
        {
            DRTopic dto = new DRTopic();
            dto = _context.DRTopics.Find(TopicId);
            if (dto == null)
            {
                return BadRequest();
            }
            return PartialView("_EditResources", dto);
        }

        private void ClearTrackingAndDirectoryTopic(List<int> topics2Del,  List<string> topicPaths, List<int> resources2Del)
        {
            foreach (var item in topicPaths)
            {
                try
                {
                    if (System.IO.Directory.Exists(item))
                    {
                        System.IO.Directory.Delete(item, true);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("Cancella cancellazione cartella {0} - Errore {1} {2}", item, e.Message, e.Source));
                }
            }

            var topics = _context.DRTrackings.Where(p => p.ExtType == Lookup.DRTracking_ExtType_TOPIC && topics2Del.Contains(p.ExtId));
            _context.DRTrackings.RemoveRange(topics);
            _context.SaveChanges();

            var resources = _context.DRTrackings.Where(p => p.ExtType == Lookup.DRTracking_ExtType_RESOURCE && resources2Del.Contains(p.ExtId));
            _context.DRTrackings.RemoveRange(resources);
            _context.SaveChanges();

        }

        [HttpPost]
        public IActionResult UpdateTopicJSON([FromBody]DRTopic DTO)
        {
            if (DTO.DRTopicId == 0)
            {
                DTO.TmstLastUpdResources = DateTime.MinValue;
                DTO.TmstInse = DateTime.Now;
                DTO.UserInse = User.Identity.Name;
                _context.Add(DTO);
            } else
            {
                DRTopic topic2Update = _context.DRTopics.Find(DTO.DRTopicId);
                if (topic2Update == null)
                {
                    return NotFound();
                }
                topic2Update.DRSectionId = DTO.DRSectionId;
                topic2Update.Code = normalizeString(DTO.Code);
                topic2Update.Revision = DTO.Revision;
                topic2Update.Title = normalizeString(DTO.Title);
                topic2Update.Note = DTO.Note;
                topic2Update.ReleasedDate = DTO.ReleasedDate;
                topic2Update.NotifyChange = DTO.NotifyChange;
                topic2Update.TmstLastUpd = DateTime.Now;
                topic2Update.UserLastUpd = User.Identity.Name;
                _context.Update(topic2Update);
            }
            _context.SaveChanges();
            return Json("");
        }

        [HttpGet]
        public IActionResult UpdateResourceTitle(int ResourceId, string Title)
        {
            string msg = "";
            try
            {
                DRResource resource2upd = _context.DRResources.Include(t => t.Topic).Where(t => t.DRResourceId == ResourceId).FirstOrDefault();
                if (resource2upd == null)
                {
                    return NotFound();
                }
                string infoUpdate = string.Format("Change Title from '{0}' to '{1}'", resource2upd.Title, Title);
                resource2upd.Title = normalizeString(Title);
                // LA MODIFICA del TITOLO non viene considerata una modifica del contenuto quindi il file non va segnalato come 'da leggere'
                // (Altrimenti andrebbe modificata anche la data sul TOPIC !! )
                //resource2upd.TmstLastUpd = DateTime.Now;  
                //resource2upd.UserLastUpd = User.Identity.Name;
                _context.Update(resource2upd);
                DRTracking newTrack = new DRTracking()
                {
                    ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                    ExtId = resource2upd.DRResourceId,
                    Operation = Lookup.DRTracking_Operation_UPDATE,
                    Note = infoUpdate,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.Add(newTrack);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "UpdateResourceTitle", e.Message, e.Source);
            }
            return Json(msg);
        }
        
        [HttpPost]
        public ActionResult UploadNewResource(int TopicId, Lookup.DRResourceType ResourceType, int ResourceId)
        {        
            try
            {
                DRTopic topic = _context.DRTopics.Find(TopicId);
                if (topic == null)
                {
                    throw new Exception(string.Format("Topic {0} not found", TopicId));
                }
                var resourcesPathName = topic.ResourcesPathName;
                if (!Directory.Exists(resourcesPathName))
                {
                    Directory.CreateDirectory(resourcesPathName);
                }

                var myFile = Request.Form.Files["myNewFileTopic"];
                string NewFullFileName = System.IO.Path.Combine(topic.ResourcesPathName, myFile.FileName);

                // se resourceId � valorizzato, devo fare una sostituzione 
                string ResourceFullFileName = "";
                DRResource resource2upd = new DRResource();
                if (ResourceId != 0)
                {
                    resource2upd = _context.DRResources.Find(ResourceId);
                    if (resource2upd == null)
                    {
                        throw new Exception(string.Format("Resource {0} not found", ResourceId));
                    }
                    if (!string.IsNullOrWhiteSpace(resource2upd.Source))
                    {
                        ResourceFullFileName = System.IO.Path.Combine(topic.ResourcesPathName, resource2upd.Source);
                    }
                }
                if (System.IO.File.Exists(NewFullFileName))
                {
                    // NON posso caricare un file che esiste gi� collegato ad un'altra risorsa
                    if (!ResourceFullFileName.ToUpper().Equals(NewFullFileName.ToUpper())) {
                        throw new Exception(string.Format("File {0} already exists", myFile.FileName));
                    }
                } 
                // Creo lo storico del file corrente 
                if (!string.IsNullOrWhiteSpace(ResourceFullFileName) && System.IO.File.Exists(ResourceFullFileName))
                {
                    System.IO.File.Move(ResourceFullFileName, string.Format("{0}_{1}_{2}", ResourceFullFileName, ResourceId, DateTime.Now.ToString("yyyyMMddHHmmss")));
                }

                // Copia nuovo file 
                using (var fileStream = System.IO.File.Create(NewFullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
                // Aggiornamento/Inserimento 
                if (ResourceId == 0)
                {
                    // NUOVA RISORSA 
                    DRResource resource = new DRResource()
                    {
                        DRTopicId = topic.DRTopicId,
                        Source = myFile.FileName,
                        Type = (Lookup.DRResourceType)ResourceType,
                        Title = myFile.FileName,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        TmstLastUpd = DateTime.Now,
                        UserLastUpd = User.Identity.Name
                    };
                    _context.DRResources.Add(resource);
                    _context.SaveChanges();
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_PUT,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
                else
                {
                    // RISORSA ESISTENTE
                    string FileNamePrec = resource2upd.Source;
                    resource2upd.Source = myFile.FileName;
                    if (resource2upd.Title == FileNamePrec)
                    {
                        resource2upd.Title = myFile.FileName;
                    }
                    resource2upd.TmstLastUpd = DateTime.Now;
                    resource2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(resource2upd);
                    _context.SaveChanges();
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource2upd.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_UPDATE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Change File from '{0}' to '{1}'", FileNamePrec, myFile.FileName)
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
                // Aggiorna Data LastUpd Resource su topic (per ordinamento WebRepository)
                topic.TmstLastUpdResources = DateTime.Now;
                _context.Update(topic);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return BadRequest(string.Format("{0}", e.Message));
                //return Json(String.Format("{0} / {1}", e.Message, e.Source));
            }
            return new EmptyResult();
        }

        [HttpPost]
        public IActionResult CopyFileFromFtp(int TopicId, string FileName, Lookup.DRResourceType ResourceType, int ResourceId)
        {
            List<string> arrRet = new List<string>();
            foreach (string myFile in FileName.Split(","))
            {
                string myRet = CopyFileFromFtpSingle(TopicId, myFile, ResourceType, ResourceId);
                if (string.IsNullOrWhiteSpace(myRet))
                {
                    arrRet.Add(string.Format("<span class='text-success'>File {0} moved!</span>", myFile));
                } else
                {
                    arrRet.Add(string.Format("<span class='text-danger'>{0}</span>", myRet));
                }
            }
            // return Json(string.Join(Environment.NewLine, arrRet));
            return Json(arrRet);
        }
        private string CopyFileFromFtpSingle(int TopicId, string FileName, Lookup.DRResourceType ResourceType, int ResourceId)
        {
            string ret = "";
            try
            {
                DRTopic topic = _context.DRTopics.Find(TopicId);
                if (topic == null)
                {
                    throw new Exception(string.Format("Topic {0} not found", TopicId));
                }
                var resourcesPathName = topic.ResourcesPathName;
                if (!Directory.Exists(resourcesPathName))
                {
                    Directory.CreateDirectory(resourcesPathName);
                }
                string FullFileNameFTP = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirFilesByFTP, FileName);
                if (!System.IO.File.Exists(FullFileNameFTP))
                {
                    throw new Exception(string.Format("File {0} not exists in FTP Area", FullFileNameFTP));
                }
                // se resourceId � valorizzato, devo fare una sostituzione 
                string ResourceFullFileName = "";
                DRResource resource2upd = new DRResource();
                if (ResourceId != 0)
                {
                    resource2upd = _context.DRResources.Find(ResourceId);
                    if (resource2upd == null)
                    {
                        throw new Exception(string.Format("Resource {0} not found", ResourceId));
                    }
                    if (!string.IsNullOrWhiteSpace(resource2upd.Source))
                    {
                        ResourceFullFileName = System.IO.Path.Combine(topic.ResourcesPathName, resource2upd.Source);
                    }
                }
                string NewFullFileName = System.IO.Path.Combine(topic.ResourcesPathName, FileName);
                if (System.IO.File.Exists(NewFullFileName))
                {
                    // NON posso caricare un file che esiste gi� collegato ad un'altra risorsa
                    if (!ResourceFullFileName.ToUpper().Equals(NewFullFileName.ToUpper()))
                    {
                        throw new Exception(string.Format("File {0} already exists", FileName));
                    }
                }
                // Creo lo storico del file corrente 
                if (!string.IsNullOrWhiteSpace(ResourceFullFileName) && System.IO.File.Exists(ResourceFullFileName))
                {
                    System.IO.File.Move(ResourceFullFileName, string.Format("{0}_{1}_{2}", ResourceFullFileName, ResourceId, DateTime.Now.ToString("yyyyMMddHHmmss")));
                }

                System.IO.File.Move(FullFileNameFTP, NewFullFileName);

                // Aggiornamento/Inserimento 
                if (ResourceId == 0)
                {
                    // NUOVA RISORSA 
                    DRResource resource = new DRResource()
                    {
                        DRTopicId = topic.DRTopicId,
                        Source = FileName,
                        Title = FileName,
                        Type = (Lookup.DRResourceType)ResourceType,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        TmstLastUpd = DateTime.Now,
                        UserLastUpd = User.Identity.Name
                    };
                    _context.DRResources.Add(resource);
                    _context.SaveChanges();
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_PUT,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
                else
                {
                    // RISORSA ESISTENTE
                    string FileNamePrec = resource2upd.Source;
                    resource2upd.Source = FileName;
                    if (resource2upd.Title == FileNamePrec)
                    {
                        resource2upd.Title = FileName;
                    }
                    resource2upd.TmstLastUpd = DateTime.Now;
                    resource2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(resource2upd);
                    _context.SaveChanges();
                    DRTracking newTrack = new DRTracking()
                    {
                        ExtType = Lookup.DRTracking_ExtType_RESOURCE,
                        ExtId = resource2upd.DRResourceId,
                        Operation = Lookup.DRTracking_Operation_UPDATE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Change File from '{0}' to '{1}'", FileNamePrec, FileName)
                    };
                    _context.DRTrackings.Add(newTrack);
                    _context.SaveChanges();
                }
                // Aggiorna Data LastUpd Resource su topic (per ordinamento WebRepository)
                topic.TmstLastUpdResources = DateTime.Now;
                _context.Update(topic);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return String.Format("{0}", e.Message);
            }

            return ret;
        }

        [HttpGet]
        public object GetFilesFromFTP(DataSourceLoadOptions loadOptions)
        {
            List<LookUpString> filesByFTP = new List<LookUpString>();
            if (Directory.Exists(DirFilesByFTP))
            {
                var fileEntries = Directory.EnumerateFiles(DirFilesByFTP, "*.*", SearchOption.TopDirectoryOnly);
                foreach (string item in fileEntries)
                {
                    filesByFTP.Add(new LookUpString() { id = Path.GetFileName(item) });
                }
            }
            return DataSourceLoader.Load(filesByFTP, loadOptions);
        }

        // GET:DocumentalRepository/GetResourcesJSON
        public async Task<IActionResult> GetResourcesJSON(int ResourceId)
        {
            DRResource data = _context.DRResources.Find(ResourceId);
            //if (data != null)
            //{
            //    data.ResourceFullFileName = (new DocumentalRepositoryBO()).ResourceFullFileName(ResourceId);
            //}
            return Json(data);
        }

        //public ActionResult DownloadResource(int id, string opt = "")
        //{
        //    string fileNameError = "DownloadRespource_ERROR.txt";
        //    DRResource resource = _context.DRResources.Find(id);
        //    if (resource == null )
        //    {
        //        return NotFound();
        //    }
        //    DRTopic topic = _context.DRTopics.Find(resource.DRTopicId);
        //    if (topic == null)
        //    {
        //        return NotFound();
        //    }

        //    string fullFileName = System.IO.Path.Combine(topic.ResourcesPathName, resource.Source);
        //    try
        //    {
        //        if (System.IO.File.Exists(fullFileName))
        //        {
        //            var fs = System.IO.File.ReadAllBytes(fullFileName);
        //            if (opt.ToLower().Equals("read"))
        //            {
        //                ImpostaLetturaRisorsa(id);
        //            }
        //            return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = resource.Source };
        //        }
        //        string msg = String.Format("File not Found");
        //        msg += String.Format("\nFile Id: {0}", resource.DRResourceId);
        //        msg += String.Format("\nFileName: {0}", resource.Source);
        //        msg += String.Format("\nTitle: {0}", resource.Title);
        //        msg += String.Format("\nTitle: {0}", resource.Title);
        //        msg += String.Format("\nTopic Id: {0}", resource.DRTopicId);
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = String.Format("Error in DownloadResource() function.");
        //        msg += String.Format("\nFile Id: {0}", resource.DRResourceId);
        //        msg += String.Format("\nTopic Id: {0}", resource.DRTopicId);
        //        msg += String.Format("\nMessage: {0}", e.Message);
        //        msg += String.Format("\nSource: {0}", e.Source);
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //        //string error = String.Format("{0} / {1}", e.Message, e.Source);
        //        //_logger.LogError(String.Format("{0} -  {1}", "DownloadResource()", error));
        //        //return BadRequest(error);
        //    }
        //}

        //public ActionResult DownloadAllResourceTopic(int id)
        //{
        //    string fileNameError = "DownloadAllResource_ERROR.txt";
        //    DRTopic topic = _context.DRTopics.Find(id);
        //    if (topic == null)
        //    {
        //        string msg = String.Format("Topic {0} Not found", id);
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //    }
        //    List<DRResource> resources = _context.DRResources.Where(t => t.DRTopicId == id && t.Deleted == false && 
        //                                                                (t.Type == Lookup.DRResourceType.file || t.Type == Lookup.DRResourceType.attachment))
        //                                                    .ToList();
        //    List<string> files = new List<string>();
        //    List<DRResource> resourcesFound = new List<DRResource>();
        //    foreach (var item in resources)
        //    {
        //        string fullFileName = System.IO.Path.Combine(topic.ResourcesPathName, item.Source);
        //        if (System.IO.File.Exists(fullFileName))
        //        {
        //            files.Add(fullFileName);
        //            resourcesFound.Add(item);
        //        }
        //    }
        //    if (files.Count == 0)
        //    {
        //        string msg = String.Format("No files Found");
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //    }
        //    try
        //    {
        //        var zipFileName = Lookup.GetValidFileName(string.Format("{0}_{1}_{2}.zip", topic.Code, topic.Revision, topic.Title));
        //        var zipFullFileName = System.IO.Path.Combine(topic.ResourcesPathName, zipFileName);
        //        if (System.IO.File.Exists(zipFullFileName))
        //        {
        //            System.IO.File.Delete(zipFullFileName);
        //        }
        //        List<string> filesOK = ZipFileCreator.CreateZipFile(zipFullFileName, files);
        //        if (filesOK.Count > 0)
        //        {
        //            var fs = System.IO.File.ReadAllBytes(zipFullFileName);
        //            foreach (var item in resourcesFound)
        //            {
        //                ImpostaLetturaRisorsa(item.DRResourceId);
        //            }
        //            System.IO.File.Delete(zipFullFileName);
        //            return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = zipFileName };
        //        }
        //        string msg = String.Format("No files Found (CreateZipFile)");
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //    }
        //    catch (Exception e)
        //    {
        //        string msg = String.Format("Error in DownloadAllResourceTopic() function.");
        //        msg += String.Format("\nTopic Id: {0}", id);
        //        msg += String.Format("\nMessage: {0}", e.Message);
        //        msg += String.Format("\nSource: {0}", e.Source);
        //        byte[] bytes = Encoding.ASCII.GetBytes(msg);
        //        return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
        //        //string error = String.Format("{0} / {1}", e.Message, e.Source);
        //        //_logger.LogError(String.Format("{0} -  {1}", "DownloadAllResourceTopic()", error));
        //        //return BadRequest(error);
        //    }
        //}

        public async Task<FileStreamResult> DownloadResource(int id, string opt = "")
        {
            string fileNameError = "DownloadRespource_ERROR.txt";
            DRResource resource = _context.DRResources.Find(id);
            if (resource == null)
            {
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(string.Format("Resource {0} not found", id))), "application/octet-stream");
            }
            DRTopic topic = _context.DRTopics.Find(resource.DRTopicId);
            if (topic == null)
            {
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(string.Format("Topic {0} not found", resource.DRTopicId))), "application/octet-stream");
            }

            string fullFileName = System.IO.Path.Combine(topic.ResourcesPathName, resource.Source);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var stream = System.IO.File.OpenRead(fullFileName);
                    if (opt.ToLower().Equals("read"))
                    {
                        ImpostaLetturaRisorsa(id);
                    }
                    return new FileStreamResult(stream, "application/octet-stream") { FileDownloadName = resource.Source };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nFile Id: {0}", resource.DRResourceId);
                msg += String.Format("\nFileName: {0}", resource.Source);
                msg += String.Format("\nTitle: {0}", resource.Title);
                msg += String.Format("\nTitle: {0}", resource.Title);
                msg += String.Format("\nTopic Id: {0}", resource.DRTopicId);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadResource() function.");
                msg += String.Format("\nFile Id: {0}", resource.DRResourceId);
                msg += String.Format("\nTopic Id: {0}", resource.DRTopicId);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        public async Task<FileStreamResult> DownloadAllResourceTopic(int id)
        {
            string fileNameError = "DownloadAllResource_ERROR.txt";
            DRTopic topic = _context.DRTopics.Find(id);
            if (topic == null)
            {
                string msg = String.Format("Topic {0} Not found", id);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            List<DRResource> resources = _context.DRResources.Where(t => t.DRTopicId == id && t.Deleted == false &&
                                                                        (t.Type == Lookup.DRResourceType.file || t.Type == Lookup.DRResourceType.attachment))
                                                            .ToList();
            List<string> files = new List<string>();
            List<DRResource> resourcesFound = new List<DRResource>();
            foreach (var item in resources)
            {
                string fullFileName = System.IO.Path.Combine(topic.ResourcesPathName, item.Source);
                //if (System.IO.File.Exists(fullFileName))
                //{
                files.Add(fullFileName);
                resourcesFound.Add(item);
                //}
            }
            if (files.Count == 0)
            {
                string msg = String.Format("No files Found");
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            try
            {
                var outFileName = string.Format("{0}_{1}_{2}.zip", topic.Code, topic.Revision, topic.Title);
                if (!Directory.Exists(topic.TempPathName))
                {
                    Directory.CreateDirectory(topic.TempPathName);
                }
                //var ZipFileName = string.Format("{0}.zip", Lookup.GetValidFileName(User.Identity.Name));
                var ZipFileName =   Utility.GetTempFileName(User.Identity.Name, "zip", topic.TempPathName);
                var zipFullFileName = System.IO.Path.Combine(topic.TempPathName, ZipFileName);
                if (System.IO.File.Exists(zipFullFileName))
                {
                    System.IO.File.Delete(zipFullFileName);
                }
                List<string> filesOK = ZipFileCreator.CreateZipFile(fileName: zipFullFileName,
                                                                    files: files,
                                                                    strToMask: topic.ResourcesPathName,
                                                                    strToReplace: string.Format("<{0}>", id));
                if (filesOK.Count > 0)
                {
                    foreach (var item in resourcesFound)
                    {
                        string fullFileName = System.IO.Path.Combine(topic.ResourcesPathName, item.Source);
                        foreach (var file in filesOK)
                        {
                            if (fullFileName.Equals(file))
                            {
                                ImpostaLetturaRisorsa(item.DRResourceId);
                            }
                        }
                    }
                    var stream = System.IO.File.OpenRead(zipFullFileName);
                    return new FileStreamResult(stream, "application/octet-stream") { FileDownloadName = outFileName };
                }
                string msg = String.Format("No files Found (CreateZipFile)");
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAllResourceTopic() function.");
                msg += String.Format("\nTopic Id: {0}", id);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(msg ?? "")), "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        [HttpPost]
        public IActionResult SendEmailTopic([FromBody]EmailTemplate DTO)
        {
            string msgException = "";
            string msgSendEmail = "[OK]";

            DRTopic topic = _context.DRTopics.Find((int)DTO.IdRif);
            if (topic == null)
            {
                return BadRequest(string.Format("SendEmailTopic() - Topic {0} not found", DTO.IdRif));
            }

            List<EmailTopic> destinatari = (new DocumentalRepositoryBO()).GetEmailNotificaTopic(topic);
            if (destinatari != null && destinatari.Count > 0)
            {
                var subDestinatari = destinatari
                                        .Select((x, i) => new { Index = i, Value = x })
                                        .GroupBy(x => x.Index / 50)
                                        .Select(x => x.Select(v => v.Value).ToList())
                                        .ToList();
                foreach(var item in subDestinatari)
                {
                    string BCC = string.Join (",", item.Select(p=>p.Email).ToArray());
                    var retSend = _emailSender.SendEmailAsync("NONE", DTO.Subject, DTO.Body, emailBcc: BCC, context: "REPOSITORY", _logger: _logger);
                    if (retSend.IsFaulted)
                    {
                        msgSendEmail = "*** ERROR SendEmailAsync() ****";
                        if (retSend.Exception != null || retSend.Exception.InnerException != null)
                        {
                            msgException = retSend.Exception.InnerException.Message;
                        }
                    }
                }
            }

            if (!msgSendEmail.Equals("[OK]"))
            {
                return BadRequest(string.Format("{0} - Exception Message: {1}", msgSendEmail, msgException));
            }

            return Json("");
        }

        [AllowAnonymous]
        // <site>/DocumentalRepository/DownloadDoc?ResourceId=3021&Hashfile=9E35E4ADDF19C839A4C49763ADEA2F8AA6B2B7817B808F19B75C281C50C9DA53
        public ActionResult DownloadDoc(long ResourceId, string HashFile)
        {
            string msg = "";
            DRResource data = _context.DRResources.Include(t => t.Topic).Where(t => t.DRResourceId == ResourceId).FirstOrDefault();
            if (data == null)
            {
                msg = String.Format("Document '{0}' not found / NTFND !!", ResourceId);
                _logger.LogError(msg);
                return NotFound(msg);
            }

            DRSection section = _context.DRSections.Find(data.Topic.DRSectionId);
            if (section == null)
            {
                msg = String.Format("Document '{0}' not found / SECERR !!", ResourceId);
                _logger.LogError(string.Format("{0} - Section {1} Not found", msg, data.Topic.DRSectionId));
                return NotFound(msg);

            }
            if (section.Locked == true)
            {
                msg = String.Format("Document '{0}' not found / SECLOCK !!", ResourceId);
                _logger.LogError(string.Format("{0} - Section {1} LOCKED", msg, data.Topic.DRSectionId));
                return NotFound(msg);
            }
            if (data.Type == Lookup.DRResourceType.file || data.Type == Lookup.DRResourceType.attachment)
            {
                string FullFileName = System.IO.Path.Combine(data.Topic.ResourcesPathName, data.Source);
                if (!System.IO.File.Exists(FullFileName))
                {
                    msg = String.Format("Document '{0}' not found / FILENTFND !!", ResourceId);
                    _logger.LogError(string.Format("{0} - File {1} not Found", msg, FullFileName));
                    return NotFound(msg);
                }
                byte[] mySha256 = GetHashSha256(FullFileName);
                StringBuilder stringBuilder = new StringBuilder();
                foreach (byte b in mySha256)
                    stringBuilder.AppendFormat("{0:X2}", b);
                string hashString = stringBuilder.ToString();
                if (string.IsNullOrWhiteSpace(HashFile) || !HashFile.ToUpper().Equals(hashString.ToUpper())) {
                    msg = String.Format("Document '{0}' not found / CHECKERR !!", ResourceId);
                    _logger.LogError(string.Format("{0} - Hash not corresponding to {1}", msg, hashString));
                    return NotFound(msg);
                }
                try
                {
                    var fs = System.IO.File.ReadAllBytes(FullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = data.Source };
                }
                catch (Exception e)
                {
                    msg = String.Format("DownloadDoc - Error {0} / {1}", e.Message, e.Source);
                    _logger.LogError(msg);
                    return BadRequest(msg);
                }
            }
            msg = String.Format("Document '{0}' not found / TYPENTFND !!", ResourceId);
            _logger.LogError(string.Format("{0} - Unhandled Type {1}", msg, data.TypeDeco));
            return NotFound(msg);
        }

        // Compute the file's hash.
        private byte[] GetHashSha256(string filename)
        {
            // The cryptographic service provider.
            SHA256 Sha256 = SHA256.Create();
            using (FileStream stream = System.IO.File.OpenRead(filename))
            {
                return Sha256.ComputeHash(stream);
            }
        }

        [HttpGet]
        public IActionResult SetUserAuthorizationOnSection(int SectionId, string UserId, bool Selected)
        {
            string msg = "";
            try
            {
                if (Selected)
                {
                    (new UsersBO()).AddUserLink(UserId, Lookup.UserLinkType.DRSection, SectionId.ToString(), User.Identity.Name);
                } else
                {
                    (new UsersBO()).RemoveUserLink(UserId, Lookup.UserLinkType.DRSection, SectionId.ToString());
                }                
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "SetUserAuthorizationOnSection", e.Message, e.Source);
            }
            return Json(msg);
        }

        private string normalizeString(string source)
        {
            string ret = source;
            if (!string.IsNullOrWhiteSpace(ret))
            {
                ret = ret.Replace("\n\r", " ").Replace("\n", " ").Replace("\r", " ").Trim() ;
            }
            return ret;
        }
    }
}

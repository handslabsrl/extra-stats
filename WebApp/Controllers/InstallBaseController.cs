using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using WebApp.Repository;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Localization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;
using System.Data.SqlClient;
using NPoco;
using System.IO;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using Microsoft.Extensions.Configuration;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Roles = "ELITe Instruments")]
    public class InstallBaseController : Controller
    {

        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        //private readonly IHtmlLocalizer<QuotationsController> _localizer;
        private string connectionString;
        private readonly string DirInstallBase = "Download\\InstallBase";

        //public InstallBaseController(WebAppDbContext context, ILogger<InstallBaseController> logger, IHtmlLocalizer<QuotationsController> localizer)
        public InstallBaseController(WebAppDbContext context, ILogger<InstallBaseController> logger)
        {
            _context = context;
            _logger = logger;
            //_localizer = localizer;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }

        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object GetDataImport(DataSourceLoadOptions loadOptions, string referencePeriod = null, bool showDismissed = false)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            var data = (new InstallBaseBO()).GetDataImport(referencePeriod, UserId, showDismissed);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object GetInstallBase(DataSourceLoadOptions loadOptions)
        {
            var data = (new InstallBaseBO()).GetInstallBase();
            return DataSourceLoader.Load(data, loadOptions);
        }

        // GET: Quotations
        public async Task<IActionResult> Index(string referencePeriod, string datatype, bool bShowDismissed = false)
        {
            InstallBaseIndexDTO dto = new InstallBaseIndexDTO();
            dto.selMese = referencePeriod ?? string.Format("{0}{1}", DateTime.Now.Year.ToString("d4"), DateTime.Now.Month.ToString("d2"));
            // caricamento Array Mesi: da gennaio 2018 al mese corrente !!
            dto.mesi = new List<LookUpString>();
            DateTime currdate = DateTime.Now;
            DateTime enddate = new DateTime(2018, 1, 1);
            CultureInfo ci = new CultureInfo("en-US");
            while (currdate >= enddate)
            {
                LookUpString item = new LookUpString();
                item.id = string.Format("{0}{1}", currdate.Year.ToString("d4"), currdate.Month.ToString("d2"));
                item.descrizione = currdate.ToString("MMMM yyyy", ci);
                dto.mesi.Add(item);
                currdate = currdate.AddMonths(-1);
            }
            dto.datatype = datatype;
            dto.bShowDismissed = bShowDismissed;
            return View("index", dto);
        }

        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file, string dataRif, string serialNumber, string dataType, 
                                                    string fileVersion, bool bShowDismissed, string modelType)
        {
            if (file != null && file.Length > 0) {
                string path = Path.Combine(Directory.GetCurrentDirectory(), string.Format("Upload\\{0}", serialNumber));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filename = Path.Combine(path, file.FileName);
                //using (var stream = new FileStream(filename, FileMode.Create))
                //{
                //    await file.CopyToAsync(stream);
                //}
                using (FileStream fs = System.IO.File.Create(filename))
                {
                    file.CopyTo(fs);
                }
                // Annulla eventuali file in stato waiting, per stesso periodo, lo stesso strumento, stesso tipo  (TRANNE CHE INSTALLBASE)
                if (!dataType.Equals(Lookup.FileDataType_InstallBase)) { 
                    var file2del = _context.UploadFiles.Where(t => t.SerialNumber == serialNumber && t.FileType == dataType && t.ReferencePeriod == dataRif && t.Status == "WAIT").ToList();
                    foreach (var item in file2del)
                    {
                        item.Status = "DELETE";
                        item.UserDelete = User.Identity.Name;
                        item.TmstDelete = DateTime.Now;
                        _context.Update(item);
                        _context.SaveChanges();
                    }
                }
                // Aggiunta nuovo file 
                UploadFile newRec = new UploadFile()
                {
                    FileName = file.FileName,
                    FileType = dataType, 
                    ReferencePeriod = dataRif,
                    SerialNumber = serialNumber,
                    Status = "WAIT",
                    UserUpload = User.Identity.Name,
                    TmstUpload = DateTime.Now, 
                    RevisionExtra = fileVersion, 
                    ModelType = modelType
                };
                _context.Add(newRec);
                _context.SaveChanges();
            }
            return RedirectToAction("Index", new { referencePeriod = dataRif, datatype = dataType, bShowDismissed=bShowDismissed } );

        }


        [HttpPost]
        public async Task<IActionResult> RemoveFile(int id, string referencePeriod, bool bShowDismissed)
        {
            UploadFile file2del = _context.UploadFiles.Find(id);
            if (file2del != null)
            {
                if (file2del.Status.Equals("WAIT"))
                {
                    file2del.Status = "DELETE";
                    file2del.UserDelete = User.Identity.Name;
                    file2del.TmstDelete = DateTime.Now;
                };
                _context.Update(file2del);
                _context.SaveChanges();
            }

            return RedirectToAction("Index", new { referencePeriod = referencePeriod, datatype=file2del?.FileType, bShowDismissed=bShowDismissed });
        }

        [HttpPost]
        public ActionResult GetSerialNumberJson(string id)
        {
            string userId = (new UsersBO()).GetUserId(User.Identity.Name);
            var data = (new InstallBaseBO()).GetSerialNumberEnabled(id, userId);
            string serialNumber = "";
            string modelType = "";
            string modelTypeDeco = "";
            if (!string.IsNullOrWhiteSpace(data)) {
                serialNumber = data;
                if (serialNumber.StartsWith("*"))
                {
                    serialNumber = "*";
                }
                // in caso in cui siano stati trovati pi� serial number, recupero il ModelType dal primo modello 
                InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(data.Replace("*", ""));
                if (instrument != null)
                {
                    modelType = instrument.ModelType;
                    modelTypeDeco = instrument.ModelTypeDeco;
                }
            }
            //return Json(data);
            return Json(new { serialNumber, modelType, modelTypeDeco  });
        }

        [HttpPost]
        public ActionResult GetSerialNumberInfoJson(string id)
        {
            var data = (new InstrumentsBO()).GetInstruments(SerialNumber: id);
            return Json(data);
        }

        [HttpPost]
        public async Task<IActionResult> SetReloadFile(int id, string referencePeriod, bool bShowDismissed)
        {
            UploadFile file2upd = _context.UploadFiles.Find(id);
            if (file2upd != null)
            {
                file2upd.Status = "RELOAD";
                file2upd.UserDelete = User.Identity.Name;
                file2upd.TmstDelete = DateTime.Now;
                _context.Update(file2upd);
                _context.SaveChanges();
            }
            return RedirectToAction("Index", new { referencePeriod = referencePeriod, datatype = file2upd?.FileType, bShowDismissed=bShowDismissed });
        }

        // POST: InstallBase/setInstrumentCommand/5
        public JsonResult setInstrumentCommand(string SerialNumber, string Command,  DateTime DateRif)
        {
            (new InstrumentsBO()).SetCommand(SerialNumber: SerialNumber, Command: Command, DateRif: DateRif);
            return Json("");
        }

        // POST: InstallBase/setInstrumentCommand/5
        public JsonResult InsertExportRequest(string requestType)
        {
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirInstallBase);
            string fullPathName = System.IO.Path.Combine(relativePath, String.Format("{0}.req", requestType.Replace(" ", String.Empty)));
            if (!Directory.Exists(relativePath))
            {
                Directory.CreateDirectory(relativePath);
            }
            try
            {
                string email = (new UsersBO()).GetEmail(User.Identity.Name);
                string[] lines = { User.Identity.Name, email };
                System.IO.File.WriteAllLines(fullPathName, lines);
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadFile.Get()", error));
                return Json(error);
            }
            return Json("");
        }

        // **************************************************************
        // INSTRUMENT UPDATE REQUEST
        // **************************************************************
        public ActionResult InstrumentUpdateRequest(InstrumentUpdateRequestDTO dto)
        {
            dto.InstrumentData = (new InstrumentsBO()).GetInstrument(SerialNumber: dto.SerialNumber);
            dto.commercialStatuses = (new CommercialStatusBO()).Get(enabledISR: true);
            //dto.countries = (new CountriesBO()).GetCountries(allRegistry: true);
            dto.countries = (new UtilBO()).GetStdTables(CodTab: Lookup.StdTab_COUNTRIES, bExcludeDeleted: true);
            dto.sites= (new UtilBO()).GetSGATSites(cliente: dto.InstrumentData.CustomerCode);
            List<LookUpString> allversions = (new UtilBO()).GetStdTables(CodTab: Lookup.StdTab_VERSIONS, bExcludeDeleted: true);
            string[] arr = { dto.InstrumentData.ModelType, Lookup.Instrument_ModelType_All };
            dto.versions = (from t in allversions where !string.IsNullOrWhiteSpace(t.attr1) && arr.Contains(t.attr1) select t).ToList();
            return View(dto);
        }

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object GetInstrumentUpdateRequests(DataSourceLoadOptions loadOptions, string SerialNumber)
        {
            var data = _context.ProcessRequests.Where(t => t.ExternalRif.Equals(SerialNumber) && t.ProcessType == Lookup.ProcessRequest_Type_InstrumentUpdate).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpPost]
        public IActionResult CreateInstrumentUpdateRequestJSON([FromBody]ProcessRequestInstrumentUpdate DTO)
        {
            string msgErr = (new SGATUtilBO(_context)).UpdateInstrumentInformations(DTO);
            if (String.IsNullOrWhiteSpace(msgErr))
            {
                ProcessRequest newRequest = new ProcessRequest()
                {
                    ProcessType = Lookup.ProcessRequest_Type_InstrumentUpdate,
                    ProcessParms = JsonConvert.SerializeObject(new { DTO.SiteDescription, DTO.SiteCity, DTO.VersionDesc, DTO.CommercialStatus, DTO.CountryDesc, DTO.NewSiteCode}),
                    ExternalRif = DTO.SerialNumber,
                    Status = Lookup.ProcessRequest_Status_READY,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ProcessRequests.Add(newRequest);
                _context.SaveChanges();
            }

            return Json(msgErr);
        }

        [HttpGet]
        public IActionResult DeleteInstrumentUpdateRequest(int Id)
        {
            ProcessRequest rec2Delete = _context.ProcessRequests.Find(Id);
            if (rec2Delete != null)
            {
                if (rec2Delete.Status.Equals(Lookup.ProcessRequest_Status_NEW))
                {
                    _context.ProcessRequests.Remove(rec2Delete);
                    _context.SaveChanges();
                }
            }
            return Json("");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPoco;
using WebApp.Classes;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Repository;

namespace WebApp.Controllers
{
    [Authorize(Roles = "ELITe Instruments,Assays Notification")]
    public class AssaysNotificationController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private static string connectionString;

        private readonly string DirManleva = "Download\\Letters\\Manleva";

        public AssaysNotificationController(WebAppDbContext context, ILogger<AssaysNotificationController> logger)
        {
            _context = context;
            _logger = logger;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult AssayLetters(int AssayId)
        {
            AssayLettersDTO dto = new AssayLettersDTO()
            {
                AssayId = AssayId, 
                Assay = _context.Assay.Find(AssayId)
            };
            return View(dto);
        }
        [HttpGet]
        public object GetAllAssays(DataSourceLoadOptions loadOptions)
        {
            //var data = _context.Assay.ToList();
            var data = (new AssaysBO()).GetAllAssay();

            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            var assayConManleva = (new AssaysBO()).GetLettersByAssay(AssayId: 0, UserId: UserId, letterType: Lookup.Letter_MANLEVA);

            foreach (var item in data)
            {
                if ((bool)item.Validated == false)
                {
                    var myAssay = assayConManleva.Where(t => t.AssayCod.Equals(item.AssayCod));
                    // lettere ancora da creare 
                    item.StatoManlevaCalc = Lookup.Assay_StatoManleva.DaCreare;
                    if (myAssay.Count() > 0)
                    {
                        if (myAssay.Where(t=>t.SignedFileName == null).Count() > 0)
                        {
                            // Almento uno da firmare
                            item.StatoManlevaCalc = Lookup.Assay_StatoManleva.Creata;
                        }
                        else
                        {
                            // tutte firmate 
                            item.StatoManlevaCalc = Lookup.Assay_StatoManleva.Firmata;
                        }
                    }
                }
            }
            /*  select AssayVersion.Version as AssayVersionMax, Assay.* from Assay
                LEFT JOIN AssayVersion on Assay.AssayID = AssayVersion.AssayId
                WHERe AssayVersionID = (SELECT MAX(B.AssayVersionID) from AssayVersion B WHERE B.AssayId = Assay.AssayID)
                OR AssayVersionID  IS NULL
                order by AssayVersionID
             */
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetAssayVersions(DataSourceLoadOptions loadOptions, int id)
        {
            var data = _context.AssayVersion.Where(t => t.AssayId == id).OrderBy(t=>t.AssayVersionID).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }
        // GET: AssaysNotification/Create/5
        public async Task<IActionResult> Create()
        {
            CreateAssayDTO dto = new CreateAssayDTO();
            return PartialView("_Create", dto);
        }

        // GET: AssaysNotification/EditAssay/5
        public async Task<IActionResult> EditAssay(int AssayId = 0)
        {
            Assay dto = new Assay();
            if (AssayId > 0)
            {
                dto = _context.Assay.Find(AssayId);
                if (dto == null)
                {
                    return BadRequest();
                }
            }
            return PartialView("_EditAssay", dto);
        }

        // GET: AssaysNotification/EditVersion/5
        public async Task<IActionResult> EditVersion(int AssayId = 0, int AssayVersionID = 0)
        {
            bool bCanDelete = false;
            AssayVersion dto = new AssayVersion();
            dto.AssayId = AssayId;
            if (AssayVersionID > 0)
            {
                dto = _context.AssayVersion.Find(AssayVersionID);
                if (dto == null)
                {
                    return BadRequest();
                }
                // non posso cancellare l'ultimo elemento di version !!! 
                bCanDelete = _context.AssayVersion.Where(t => t.AssayId == dto.AssayId).Count() > 1;
            }
            ViewData["CanDeleteVersion"] = bCanDelete;
            return PartialView("_EditVersion", dto);
        }

        [HttpPost]
        public IActionResult CreateAssayJSON([FromBody]CreateAssayDTO DTO)
        {
            Assay assay = new Assay()
            {
                AssayCod = DTO.AssayCod,
                InstrumentType = DTO.InstrumentType, 
                Note = DTO.Note,
                Tipo = DTO.Tipo,
                AssayMadre = DTO.AssayMadre,
                AssayMadreVersione = DTO.AssayMadreVersione,
                Attivo = DTO.AssayAttivo,
                Validated = DTO.AssayValidated,
                StatoManleva = (int)Lookup.Assay_StatoManleva.DaCreare,
                Analyte = DTO.Analyte,
                Panel = DTO.Panel,
                BusinessType = DTO.BusinessType,
                TmstLastUpd = DateTime.Now,
                UserLastUpd = User.Identity.Name
            };            
            _context.Assay.Add(assay);
            AssayVersion version = new AssayVersion()
            {
                AssayId = assay.AssayID,
                Version = DTO.Version,
                Attivo = DTO.VersionAttivo,
                Obbligatorio = DTO.Obbligatorio,
                StatusSend = DTO.VersionStatusSend,
                TmstLastUpd = DateTime.Now,
                UserLastUpd = User.Identity.Name
            };
            _context.AssayVersion.Add(version);
            _context.SaveChanges();
            if (assay.Attivo && version.Attivo)
            {
                NotifyNewAssayVersion(version.AssayVersionID);
            }
            return Json("");
        }
        [HttpPost]
        public IActionResult UpdateAssayJSON([FromBody]Assay DTO)
        {
            Assay assay2Update = _context.Assay.Find(DTO.AssayID);
            if (assay2Update == null)
            {
                return NotFound();
            }
            assay2Update.AssayCod = DTO.AssayCod;
            assay2Update.InstrumentType = DTO.InstrumentType;
            assay2Update.Note = DTO.Note;
            assay2Update.Tipo = DTO.Tipo;
            assay2Update.AssayMadre= DTO.AssayMadre;
            assay2Update.AssayMadreVersione = DTO.AssayMadreVersione;
            assay2Update.Attivo = DTO.Attivo;
            assay2Update.Validated = DTO.Validated;
            assay2Update.Analyte= DTO.Analyte;
            assay2Update.Panel = DTO.Panel;
            assay2Update.BusinessType= DTO.BusinessType;
            assay2Update.TmstLastUpd = DateTime.Now;
            assay2Update.UserLastUpd = User.Identity.Name;
            _context.Update(assay2Update);
            _context.SaveChanges();
            return Json("");
        }
        [HttpGet]
        public IActionResult DeleteAssay(int AssayId)
        {
            string msg = "";
            try
            {
                Assay rec2del = _context.Assay.Find(AssayId);
                if (rec2del != null)
                {
                    _context.Remove(rec2del);
                    _context.SaveChanges();

                    string sql = @"UPDATE GAssayPrograms SET DashboardAssayID = NULL, DashboardAssayVersionID = NULL
                                        WHERE DashboardAssayID = @AssayId";
                    List<object> args = new List<object>();
                    args.Add(new { AssayId = AssayId });
                    using (IDatabase db = Connection)
                    {
                        try
                        {
                            db.Execute(sql, args.ToArray());
                        }
                        catch (Exception e)
                        {
                            msg = string.Format("DeleteAssay/Update GAssayPrograms() - Error {0} {1}", e.Message, e.Source);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteAssay", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpPost]
        public IActionResult UpdateVersionJSON([FromBody]AssayVersion DTO)
        {
            bool bNotificaVersione = false;
            // la modifica o l'aggiunta di una versione NON azzera il flag di generazione della lettera di manleva
            //bool bResetLetteraManleva = false;
            if (DTO.AssayVersionID == 0)
            {
                DTO.TmstLastUpd = DateTime.Now;
                DTO.UserLastUpd  = User.Identity.Name;
                _context.AssayVersion.Add(DTO);
                Assay assay = _context.Assay.Find(DTO.AssayId);
                bNotificaVersione = (DTO.Attivo && assay.Attivo);
                //bResetLetteraManleva = true;
            }
            else
            {
                AssayVersion version2Update = _context.AssayVersion.Find(DTO.AssayVersionID);
                if (version2Update == null)
                {
                    return NotFound();
                }
                Assay assay = _context.Assay.Find(DTO.AssayId);
                if (assay.Attivo == true && version2Update.Attivo == false && DTO.Attivo == true )
                {
                    bNotificaVersione = true;
                }
                version2Update.Version = DTO.Version;
                version2Update.Attivo = DTO.Attivo;
                version2Update.Obbligatorio = DTO.Obbligatorio;
                version2Update.StatusSend = DTO.StatusSend;
                version2Update.TmstLastUpd = DateTime.Now;
                version2Update.UserLastUpd = User.Identity.Name;
                _context.Update(version2Update);
            }

            // la modifica o l'aggiunta di una versione NON azzera il flag di generazione della lettera di manleva
            //Assay assay2update = _context.Assay.Find(DTO.AssayId);
            //assay2update.StatoManleva = Lookup.Assay_StatoManleva.DaCreare;
            //assay2update.TmstLastUpd = DateTime.Now;
            //assay2update.UserLastUpd = User.Identity.Name;
            //_context.Update(assay2update);

            _context.SaveChanges();

            if (bNotificaVersione)
            {
                NotifyNewAssayVersion(DTO.AssayVersionID);
            }
            //if (bResetLetteraManleva)
            //{
            //    resetLetteraManleva(DTO.AssayId);
            //}
            return Json("");
        }
        [HttpGet]
        public IActionResult DeleteVersion(int VersionId)
        {
            string msg = "";
            try
            {
                AssayVersion rec2del = _context.AssayVersion.Find(VersionId);
                if (rec2del != null)
                {
                    _context.Remove(rec2del);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteVersion", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public IActionResult CreateRequestExportAssayJSON(string mode)
        {
            string msg = "";
            try
            {
                ProcessRequest newRequest = new ProcessRequest()
                {
                    ProcessType = Lookup.ProcessRequest_Type_AssaysNotification,
                    ProcessParms = JsonConvert.SerializeObject(new { mode = mode }),
                    Status = Lookup.ProcessRequest_Status_NEW,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.ProcessRequests.Add(newRequest);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "CreateRequestExportAssayJSON", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public object GetLettersByAssay(DataSourceLoadOptions loadOptions, int AssayId)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            var data = (new AssaysBO()).GetLettersByAssay(AssayId, UserId, letterType: Lookup.Letter_MANLEVA);
            return DataSourceLoader.Load(data, loadOptions);
        }

        public ActionResult DownloadManleva(int Id, string opt  = "")
        {
            Letter data = _context.Letters.Find(Id);
            if (data == null)
            {
                return NotFound(string.Format("Letter {0} not found", Id));
            }
            string fileName = data.FileName;
            string fileDownloadName = fileName;
            if (!string.IsNullOrWhiteSpace(opt) && opt.ToUpper().Equals("SIGNED"))
            {
                fileName = data.SignedFileName;
                fileDownloadName = string.Format("Signed_{0}", data.FileName);
            }
            if (string.IsNullOrWhiteSpace(fileName))
            {
                return BadRequest("File Name Missing");
            }
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirManleva);
            string filePathName = System.IO.Path.Combine(relativePath, fileName);
            _logger.LogInformation(string.Format("Download - Path: >{0}<", filePathName));
            if (!System.IO.File.Exists(filePathName))
            {
                return BadRequest(String.Format("File {0} not found!!", filePathName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(filePathName);
                return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", fileDownloadName);
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadManleva.Get()", error));
                return BadRequest(error);
            }
        }
        [HttpPost]
        public ActionResult UploadSignedLetter(int LetterId)
        {
            try
            {
                Letter lettera = _context.Letters.Find(LetterId);
                if (lettera == null)
                {
                    throw new Exception(string.Format("Letter {0} not found", LetterId));
                }
                string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirManleva);
                if (!Directory.Exists(relativePath))
                {
                    Directory.CreateDirectory(relativePath);
                }
                var myFile = Request.Form.Files["myNewFile"];
                var fileName = string.Format("Signed_{0}_{1}_{2}", LetterId, DateTime.Now.ToString("yyyyMMdd-HHmmss"), myFile.FileName);
                string NewFullFileName = System.IO.Path.Combine(relativePath, fileName);
                using (var fileStream = System.IO.File.Create(NewFullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
                lettera.SignedFileName = fileName;
                lettera.SignedUser = User.Identity.Name;
                lettera.SignedTmst = DateTime.Now;
                _context.Letters.Update(lettera);
                _context.SaveChanges();

                // Verifica Stato Manleva
                // lo stato della lettera viene calcolato a runtime in base alla abilitazioni dell'utente 
                //if (lettera.LetterType.ToUpper().Equals(Lookup.Letter_MANLEVA)) {
                //    AggiornaStatoManleva(lettera.LetterId);
                //}

            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return BadRequest(string.Format("{0}", e.Message));
                //return Json(String.Format("{0} / {1}", e.Message, e.Source));
            }
            return new EmptyResult();
        }

        private void NotifyNewAssayVersion(int AssayVersionID)
        {
            ProcessRequest newRequest = new ProcessRequest()
            {
                ProcessType = Lookup.ProcessRequest_Type_NewAssayVersion,
                ProcessParms = JsonConvert.SerializeObject(new { AssayVersionId = AssayVersionID }),
                Status = Lookup.ProcessRequest_Status_NEW,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name
            };
            _context.ProcessRequests.Add(newRequest);
            _context.SaveChanges();
        }
        //private void resetLetteraManleva(int AssayId)
        //{
        //    Assay assay = _context.Assay.Find(AssayId);
        //    if (assay == null)
        //        return;
        //    assay.StatoManleva = Lookup.Assay_StatoManleva.DaCreare;
        //    _context.Assay.Update(assay);
        //    _context.SaveChanges();

        //    List<LetterAssay> lettere = (from s in _context.LetterAssays
        //                                 join sa in _context.Letters
        //                                   on s.LetterId equals sa.LetterId
        //                                 where sa.LetterType == Lookup.Letter_MANLEVA
        //                                    && s.Disabled == false
        //                                    && s.AssayCod == assay.AssayCod
        //                                 select s).ToList();
        //    if (lettere.Count > 0)
        //    {
        //        foreach (var item in lettere)
        //        {
        //            item.Disabled = true;
        //            _context.LetterAssays.Update(item);
        //        }
        //        _context.SaveChanges();
        //    }
        //}
        // per ogni ASSAY legato alla lettera in questione, verifica se tutte le lettere in cui compare sono firmate 
        //private void AggiornaStatoManleva(int LetterId)
        //{
        //    List<LetterAssay> assays = _context.LetterAssays.Where(t => t.LetterId == LetterId && t.Disabled == false).ToList();

        //    foreach(var item in assays)
        //    {
        //        List<Letter> lettereDaFirmare = (from s in _context.Letters
        //                                        join sa in _context.LetterAssays
        //                                        on s.LetterId equals sa.LetterId
        //                                    where s.LetterType == Lookup.Letter_MANLEVA
        //                                        && sa.Disabled == false
        //                                        && sa.AssayCod == item.AssayCod
        //                                        && string.IsNullOrWhiteSpace(s.SignedFileName)
        //                                    select s).ToList();
        //        if (lettereDaFirmare.Count == 0)
        //        {
        //            Assay rec2Update = _context.Assay.Where(t => t.AssayCod == item.AssayCod).FirstOrDefault();
        //            if (rec2Update != null)
        //            {
        //                rec2Update.StatoManleva = Lookup.Assay_StatoManleva.Firmata;
        //                _context.Assay.Update(rec2Update);
        //                _context.SaveChanges();
        //            }
        //        }
        //    }
        //}

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApp.Data;
using WebApp.Classes;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Drawing;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;
using WebApp.Services;
using Microsoft.DotNet.PlatformAbstractions;
using System.Reflection;
using Microsoft.AspNetCore.Identity;
using WebApp.Models;
using WebApp.Repository;
using System.Net;
using Newtonsoft.Json;

namespace WebApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly IEmailSender _emailSender;
        private readonly SignInManager<ApplicationUser> _signInManager;

        //public string AssemblyVersion { get; set; }

        //public void OnGet()
        //{
        //    AssemblyVersion = typeof(RuntimeEnvironment).GetTypeInfo()
        //        .Assembly.GetCustomAttribute<AssemblyFileVersionAttribute>().Version;

        //    AssemblyVersion = @Microsoft.Extensions.PlatformAbstractions.PlatformServices.Default.Application.ApplicationVersion;
        //}

        private class VOLOS_listofsn_IN
        {
            public string cod_soc { get; set; }
            public string cod_fil { get; set; }
            public string cod_clifor { get; set; }
            public string cod_sito { get; set; }
            public string id_mac { get; set; }
            public string sn_mac { get; set; }
            public string cod_dmg { get; set; }
            public string dt_inst_eff { get; set; }
            public string cod_cfs { get; set; }
            public string cod_sito_new { get; set; }
            public string rag_sociale { get; set; }
            public string indirizzo { get; set; }
            public string localita { get; set; }
            public string cod_naz { get; set; }
        }
        private class VOLOS_params 
        {
            public List<VOLOS_listofsn_IN> listofsn { get; set; }
        }
        private class VOLOS_listofsn_OUT
        {
            public string guid { get; set; }
            public string id_mac { get; set; }
            public string sn_mac { get; set; }
            public string cod_clifor { get; set; }
            public string cod_sito { get; set; }
            public string esito { get; set; }
            public string cod_sito_new { get; set; }
            public string anomalia { get; set; }
        }
        private class VOLOS_response
        {
            public string esito { get; set; }
            public string exception { get; set; }
            public string listofsn { get; set; }
        }


        public HomeController(WebAppDbContext context, IEmailSender emailSender, SignInManager<ApplicationUser> signInManager)
        {
            _context = context;
            _emailSender = emailSender;
            _signInManager = signInManager;
        }

        
        public IActionResult Index(string command)
        {

            if (!User.Identity.IsAuthenticated)
           {
                return View("../Account/Login");
            }

            //Hangfire.BackgroundJob.Enqueue(() => Console.WriteLine("Hello world from Hangfire!"));

            if (!String.IsNullOrWhiteSpace(command) && command.ToUpper().Equals("INIT"))
            {
                HttpContext.Session.Remove(Lookup.idCache_AssaysUtilizationFilters);
                HttpContext.Session.Remove(Lookup.idCache_DailyExecutionsFilters);
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }

            // Forza cambio password !!!! 
            if (!string.IsNullOrWhiteSpace(HttpContext.Session.GetString(Lookup.SpecialMode_ChangePassword))) {
                TempData["MsgToLayout"] = String.Format("Your password has expired and must be changed");
                return RedirectToAction("ChangePassword", "Manage");
            }

            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            var UserRoles = (new UsersBO()).GetRoles(UserId);
            if (UserRoles.Count == 1 && User.IsInRole(Lookup.Role_DocumentalRepository) && string.IsNullOrWhiteSpace(command))
            {
                return RedirectToAction("Index", "DocumentalRepository");
            }

            // Inizializza filtri 
            //HttpContext.Session.SetInt32(Lookup.glb_sessionkey_ClienteId, 0);
            //HttpContext.Session.SetInt32(Lookup.glb_sessionkey_DettaglioContrattoId, 0);
            //HttpContext.Session.SetInt32(Lookup.glb_sessionkey_ContrattoId, 0);
            //HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, String.Empty);

            //if (!String.IsNullOrWhiteSpace(command))
            //{
            //    switch (command.ToLower())
            //    {
            //        case "clienti_commerciale":
            //            HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, Lookup.glb_const_AreaCommerciale);
            //            return RedirectToAction("Index", "Cliente");
            //        case "contratti_commerciale":
            //            HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, Lookup.glb_const_AreaCommerciale);
            //            return RedirectToAction("Index", "Contratto");
            //        case "ordini_commerciale":
            //            HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, Lookup.glb_const_AreaCommerciale);
            //            return RedirectToAction("Index", "Ordine");
            //        case "clienti": // TUTTI !!! 
            //            return RedirectToAction("Index", "Cliente");
            //        case "clienti_sportello":
            //            // Visualizzare TUTTI i clienti !!! 
            //            HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, Lookup.glb_const_AreaSportello);
            //            return RedirectToAction("Index", "Cliente");
            //        case "ordini_sportello":
            //            HttpContext.Session.SetString(Lookup.glb_sessionkey_TipoArea, Lookup.glb_const_AreaSportello);
            //            return RedirectToAction("Index", "Sportello");
            //    }
            //}

            TempData["cmdToExecute"] = command;
            TempData["wmessages"] = _context.WMessages.Where(t => !t.Deleted && t.TmstFrom.Value.Date <= DateTime.Now.Date && t.TmstTo.Value.Date >= DateTime.Now.Date).OrderBy(t => t.TmstTo).ToList();
            return View();


        }

        public IActionResult InstallBase()
        {
            return View();
        }
        public IActionResult LogFiles()
        {
            return View();
        }
        public IActionResult AssaysList()
        {
            return View();
        }

        public IActionResult About(string cmd = "")
        {
            ViewData["Message"] = "Your application description page.";
            //List<IdentityUserRole<string>> listUserRoles = _context.UserRoles.ToList();
            //var xxx = _context.UserRoles;
            //DirectoryInfo outputDir = new DirectoryInfo(@"c:\temp\SampleApp");
            //string output = RunSample1(outputDir);
            //return File(RunSample1(),
            //            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
            //            string.Format("Test_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
            //SendMail("CIAO");

            //ViewData["FieldsList"] = new string[] { "Hello", "World", "foo", "Bar" };

            if (!string.IsNullOrWhiteSpace(cmd) && cmd.ToUpper() == "VOLOS")
            {
                try
                {
                    ;
                    VOLOS_params VOLOSIN = new VOLOS_params()
                    {
                        listofsn = new List<VOLOS_listofsn_IN>()
                    };
                    VOLOS_listofsn_IN item = new VOLOS_listofsn_IN()
                    {
                        cod_soc = "ELGRP",
                        cod_fil = "EGITA",
                        cod_clifor = "C00001",
                        cod_sito = "1",
                        id_mac = "2072101B0841E ",
                        sn_mac = "ABCGD",
                        cod_dmg = "PSS",
                        dt_inst_eff = "2018-03-12",
                        cod_cfs = "GUI 011",
                        cod_sito_new = "",
                        rag_sociale = "ELITechGroup Spa - Torino (Magazzino)",
                        indirizzo = "Via Nole 51",
                        localita = "Torino",
                        cod_naz = " IT"
                    };
                    VOLOSIN.listofsn.Add(item);

                    var dataString = Newtonsoft.Json.JsonConvert.SerializeObject(VOLOSIN);
                    string myUrl = "https://evoelitechtest.volos.it/api/configurazione/InfoFromDashboard";

                    using (WebClient webClient = new WebClient())
                    {
                        //webClient.BaseAddress = myUrl;
                        webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                        webClient.Headers.Add("authorization", "VolosApiAuth 6TzaM58IlSeLmwcbvxLjTg==");
                        var retString = webClient.UploadString(new Uri(myUrl), "POST", dataString);
                        VOLOS_response response = Newtonsoft.Json.JsonConvert.DeserializeObject<VOLOS_response>(retString);
                        List<VOLOS_listofsn_OUT> listofsn_OUT = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VOLOS_listofsn_OUT>>(response.listofsn);
                    }
                    //var json = webClient.DownloadString("City/CityGetForDDL");
                    //var list = JsonConvert.DeserializeObject<List<CityInfo>>(json);
                    //return list.ToList();
                }
                catch (WebException ex)
                {
                    throw ex;
                }
            }

            if (!string.IsNullOrWhiteSpace(cmd) && cmd.ToUpper() == "SEND")
            {
                //_emailSender.SendEmailAsync("dariomillone@yahoo.it", "Email Test", String.Format("Email di prova per verifica della configurazione."));
                //ViewData["Message"] = string.Format("Email Inviata!");
                ViewData["Message"] = provaEMAIL();
            }


            if (!string.IsNullOrWhiteSpace(cmd) && cmd.ToUpper() == "TKSUPPORTCONFIG")
            {
                //using (StreamReader r = new StreamReader("TKSupportConfig.json"))
                //{
                //    string json = r.ReadToEnd();
                //    var xxxx = JsonConvert.DeserializeObject<List<TKSupportConfigModel>>(json);
                //}
                var xxxx = Lookup.GetTKSupportConfig("ITA");
            }


            return View();


        }
        public IActionResult TestEmail(string cmd = "")
        {
            if (!string.IsNullOrWhiteSpace(cmd))
            {
                if (cmd.ToUpper() == "SEND") {
                    _emailSender.SendEmailAsync("dariomillone@yahoo.it", "Email Test", String.Format("Email di prova per verifica della configurazione."));
                    ViewData["Message"] = string.Format("Email Inviata!");
                }
            }

            return View();
        }

        private string provaEMAIL()
        {

            try
            {
                string FromAddress = "noreply_eg_cc@elitechgroupglobal.com";
                string nameSender = "PROVA";
                string ToAddress = "dariomillone@yahoo.it";
                string SmtpServer = "smtp.office365.com";
                int SmtpPortNumber = 587;
                string User = "noreply_eg_cc@elitechgroupglobal.com";
                string Password = "Qualityisvital!";

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress(nameSender, FromAddress));
                InternetAddressList listTo = new InternetAddressList();
                listTo.Add(new MailboxAddress("prova", ToAddress));
                mimeMessage.To.AddRange(listTo);
                mimeMessage.Subject = "PROVA EMAIL";
                var builder = new BodyBuilder();
                // Set the plain-text version of the message text
                builder.HtmlBody = "PROVA EMAIL! CIAO";
                mimeMessage.Body = builder.ToMessageBody();
                foreach (var body in mimeMessage.BodyParts.OfType<TextPart>())
                {
                    body.ContentTransferEncoding = ContentEncoding.Default;
                    //body.ContentTransferEncoding = ContentEncoding.UUEncode;
                }

                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.CheckCertificateRevocation = false;
                    client.Connect(SmtpServer, SmtpPortNumber, SecureSocketOptions.Auto);
                    client.Authenticate(User, Password);
                    client.Send(mimeMessage);
                    client.Disconnect(true);
                }
            }
            catch (Exception e)
            {
                return string.Format("error {0} {1}", e.Source, e.Message);
            }

            return "OK";
        }

        public IActionResult TestInstrumentsBooking()
        {

            return View();
        }

        public IActionResult TestSignalR()
        {

            return View();
        }

        private void SendMail(string mailbody)
        {
            ////From Address  
            //string FromAddress = "dariomillone@yahoo.it";
            //string FromAdressTitle = "Email from ASP.NET Core 1.1";
            ////To Address  
            //string ToAddress = "dariomillone@yahoo.it";
            //string ToAdressTitle = "Microsoft ASP.NET Core";
            //string Subject = "Hello World - Sending email using ASP.NET Core 1.1";
            //string BodyContent = "ASP.NET Core was previously called ASP.NET 5. It was renamed in January 2016. It supports cross-platform frameworks ( Windows, Linux, Mac ) for building modern cloud-based internet-connected applications like IOT, web apps, and mobile back-end.";
            ////Smtp Server  
            //string SmtpServer = "smtp.mail.yahoo.com";
            ////Smtp Port Number  
            //int SmtpPortNumber = 465;

            //var mimeMessage = new MimeMessage();
            //mimeMessage.From.Add(new MailboxAddress
            //                        (FromAdressTitle,
            //                         FromAddress
            //                         ));
            //mimeMessage.To.Add(new MailboxAddress
            //                         (ToAdressTitle,
            //                         ToAddress
            //                         ));
            //mimeMessage.Subject = Subject; //Subject  
            //mimeMessage.Body = new TextPart("plain")
            //{
            //    Text = BodyContent
            //};

            //using (var client = new SmtpClient())
            //{
            //    client.Connect(SmtpServer, SmtpPortNumber, true);
            //    client.Authenticate(
            //        "dariomillone@yahoo.it",
            //        "h3rb4l1f3h24"
            //        );
            //    client.Send(mimeMessage);
            //    client.Disconnect(true);
            //}

            _emailSender.SendEmailAsync("dariomillone@yahoo.it", "PROVA_OK/1", "TESTO FORMATTATO DA FUORI <b>CIAO</b>");
            _emailSender.SendEmailAsync("dariomillone@yahoo.it", "PROVA_OK/2", "TESTO NON FORMATTATO");

            //using (var client = new SmtpClient())
            //{

            //    client.Connect(SmtpServer, SmtpPortNumber, true);
            //    // Note: only needed if the SMTP server requires authentication  
            //    // Error 5.5.1 Authentication   
            //    client.Authenticate("dariomillone@yahoo.it", "h3rb4l1f3h24");
            //    client.Send(mimeMessage);
            //    client.Disconnect(true);

            //}
        }  
        
        //public static string RunSample1(DirectoryInfo outputDir)
        public Stream RunSample1()
        {
            //FileInfo newFile = new FileInfo(outputDir.FullName + @"\ProvaDario.xlsx");
            //if (newFile.Exists)
            //{
            //    newFile.Delete();  // ensures we create a new workbook
            //    newFile = new FileInfo(outputDir.FullName + @"\ProvaDario.xlsx");
            //}

            //using (ExcelPackage package = new ExcelPackage(newFile))
            //{

            // List<ClaimExportDTO> data = new List<ClaimExportDTO>();
            //foreach (var item in claims)
            //{
            //    ClaimExportDTO exp = new ClaimExportDTO
            //    {
            //        AnalysisID = item.ClaimID,
            //        CCAuthor = item.CCAuthor,
            //        AnalysisDate = item.ClaimDate != null ? item.ClaimDate.Value.ToString("dd-MM-yyyy") : "",
            //        ReportedBy = item.ReportedBy,
            //        WSReference = item.WSReference,
            //        Country = item.Countries.Description,
            //        Status = item.Status,
            //        Description = item.Notes,
            //        UserInse = item.UserInse,
            //        TmstInse = item.TmstInse != null ? item.TmstInse.Value.ToString("dd/MM/yyyy HH:mm:ss") : "",
            //        UserAggi = item.UserAggi,
            //        TmstAggi = item.TmstAggi != null ? item.TmstAggi.Value.ToString("dd/MM/yyyy HH:mm:ss") : ""
            //    };
            //    data.Add(exp);
            //}

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Inventory");

            //Add the headers
            workSheet.Cells[1, 1].Value = "ID";
            workSheet.Cells[1, 2].Value = "Product";
            workSheet.Cells[1, 3].Value = "Quantity";
            workSheet.Cells[1, 4].Value = "Price";
            workSheet.Cells[1, 5].Value = "Value";

            //Add some items...
            workSheet.Cells["A2"].Value = 12001;
            workSheet.Cells["B2"].Value = "Nails";
            workSheet.Cells["C2"].Value = 37;
            workSheet.Cells["D2"].Value = 3.99;

            workSheet.Cells["A3"].Value = 12002;
            workSheet.Cells["B3"].Value = "Hammer";
            workSheet.Cells["C3"].Value = 5;
            workSheet.Cells["D3"].Value = 12.10;

            workSheet.Cells["A4"].Value = 12003;
            workSheet.Cells["B4"].Value = "Saw";
            workSheet.Cells["C4"].Value = 12;
            workSheet.Cells["D4"].Value = 15.37;

            //Add a formula for the value-column
            workSheet.Cells["E2:E4"].Formula = "C2*D2";

            //Ok now format the values;
            using (var range = workSheet.Cells[1, 1, 1, 5])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
                range.Style.Font.Color.SetColor(Color.White);
            }

            workSheet.Cells["A5:E5"].Style.Border.Top.Style = ExcelBorderStyle.Thin;
            workSheet.Cells["A5:E5"].Style.Font.Bold = true;

            workSheet.Cells[5, 3, 5, 5].Formula = string.Format("SUBTOTAL(9,{0})", new ExcelAddress(2, 3, 4, 3).Address);
            workSheet.Cells["C2:C5"].Style.Numberformat.Format = "#,##0";
            workSheet.Cells["D2:E5"].Style.Numberformat.Format = "#,##0.00";

            //Create an autofilter for the range
            workSheet.Cells["A1:E4"].AutoFilter = true;

            workSheet.Cells["A2:A4"].Style.Numberformat.Format = "@";   //Format as text

            //There is actually no need to calculate, Excel will do it for you, but in some cases it might be useful. 
            //For example if you link to this workbook from another workbook or you will open the workbook in a program that hasn't a calculation engine or 
            //you want to use the result of a formula in your program.
            workSheet.Calculate();

            workSheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells

            // lets set the header text 
            workSheet.HeaderFooter.OddHeader.CenteredText = "&24&U&\"Arial,Regular Bold\" Inventory";
            // add the page number to the footer plus the total number of pages
            workSheet.HeaderFooter.OddFooter.RightAlignedText =
                string.Format("Page {0} of {1}", ExcelHeaderFooter.PageNumber, ExcelHeaderFooter.NumberOfPages);
            // add the sheet name to the footer
            workSheet.HeaderFooter.OddFooter.CenteredText = ExcelHeaderFooter.SheetName;
            // add the file path to the footer
            workSheet.HeaderFooter.OddFooter.LeftAlignedText = ExcelHeaderFooter.FilePath + ExcelHeaderFooter.FileName;

            workSheet.PrinterSettings.RepeatRows = workSheet.Cells["1:2"];
            workSheet.PrinterSettings.RepeatColumns = workSheet.Cells["A:G"];

            // Change the sheet view to show it in page layout mode
            workSheet.View.PageLayoutView = false;

            //// set some document properties
            //package.Workbook.Properties.Title = "Invertory";
            //package.Workbook.Properties.Author = "Jan Källman";
            //package.Workbook.Properties.Comments = "This sample demonstrates how to create an Excel 2007 workbook using EPPlus";

            //// set some extended property values
            //package.Workbook.Properties.Company = "AdventureWorks Inc.";

            //// set some custom property values
            //package.Workbook.Properties.SetCustomPropertyValue("Checked by", "Jan Källman");
            //package.Workbook.Properties.SetCustomPropertyValue("AssemblyName", "EPPlus");
            //// save our new workbook and we are done!
            //package.Save();

            //workSheet.Cells[1, 1].LoadFromCollection(data, true);
            //for (int i = 1; i <= 15; i++)
            //{
            //    workSheet.Column(i).AutoFit();
            //}

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return stream;            //return newFile.FullName;
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Draft(int id)
        {

            //string sURL;
            ////sURL = "https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/421458006&width=480&height=360";
            //sURL = "https://vimeo.com/api/oembed.json?url=https%3A//vimeo.com/showcase/ingenius-consumables";
            
            //WebRequest wrGETURL;
            //wrGETURL = WebRequest.Create(sURL);
            ////WebProxy myProxy = new WebProxy("myproxy", 80);
            ////myProxy.BypassProxyOnLocal = true;
            ////wrGETURL.Proxy = WebProxy.GetDefaultProxy();
            //Stream objStream;
            //objStream = wrGETURL.GetResponse().GetResponseStream();
            //StreamReader objReader = new StreamReader(objStream);
            //string sLine = "";
            //int i = 0;
            //string ret = "";
            //while (sLine != null)
            //{
            //    i++;
            //    sLine = objReader.ReadLine();
            //    ret += sLine;
            //}
            //ViewData["provaURL"] = ret;
            return View(string.Format("Draft{0}", id));
        }


        public IActionResult Error()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateSnapshotJPG(int ClaimId, string Note, string image64, string SnapshotType)
        {
            string msg = "";
            //try
            //{
            //    Claim claim = _context.Claims.Find(ClaimId);
            //    if (claim == null)
            //    {
            //        return NotFound();
            //    }
            //    if (!String.IsNullOrWhiteSpace(Note) && Note.Length > 1000)
            //    {
            //        Note = Note.Substring(0, 1000);
            //    }

            //    string dirClaim = System.IO.Path.GetDirectoryName(string.Format("{0}\\{1}", DirUploadClaim, claim.FileName));
            //    string relativePath = string.Format("{0}\\Snapshots", dirClaim);
            //    string fullDirPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), relativePath);
            //    if (!Directory.Exists(fullDirPath))
            //    {
            //        Directory.CreateDirectory(fullDirPath);
            //    }
            //    // Salvataggio file IMG
            //    string fileName = string.Format("S{0}_{1}_{2}.svg", claim.ClaimId, SnapshotType, DateTime.Now.ToString("yyyyMMddHHmmss"));        // system.IO.Path.GetRandomFileName()
            //    string fullFileName = string.Format("{0}\\{1}", fullDirPath, fileName);

            //    string myImagebase64 = image64;
            //    byte[] contents = Convert.FromBase64String(myImagebase64);
            //    System.IO.File.WriteAllText(fullFileName, @"<?xml version=""1.0"" standalone=""no""?><!DOCTYPE svg PUBLIC ""-//W3C//DTD SVG 1.1//EN"" ""http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"">");
            //    using (var stream = new FileStream(fullFileName, FileMode.Append))
            //    {
            //        stream.Write(contents, 0, contents.Length);
            //    }

            //    ClaimSnapshot snapshot = new ClaimSnapshot()
            //    {
            //        Claim = claim,
            //        FileName = string.Format("{0}\\{1}", relativePath, fileName),
            //        tipo = SnapshotType,
            //        Note = Note,
            //        TmstInse = DateTime.Now,
            //        UserInse = User.Identity.Name
            //    };
            //    _context.ClaimSnapshots.Add(snapshot);
            //    ClaimTrack newTrack = new ClaimTrack()
            //    {
            //        Claim = claim,
            //        Description = "Snapshot Created",
            //        Note = Note,
            //        TmstInse = DateTime.Now,
            //        UserInse = User.Identity.Name
            //    };
            //    _context.ClaimTracks.Add(newTrack);
            //    _context.SaveChanges();

            //}
            //catch (Exception e)
            //{
            //    msg = String.Format("[{0}] - {1} / {2}", "CreateChartSnapshot", e.Message, e.Source);
            //}

            return Json(msg);
        }
    }
}

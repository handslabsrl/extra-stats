using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using WebApp.Models;
using WebApp.Repository;
using System.Collections;
using Microsoft.Extensions.Configuration;
using NPoco;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using OfficeOpenXml;
using OfficeOpenXml.Style;


namespace WebApp.Controllers
{
    //[Authorize(Roles = "Documental Repository,Documental Repository BackOffice")]
    [Authorize]
    public class TKRegistryController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private string connectionString;

        //private readonly string DirFilesByFTP = "Upload\\FTP_WEBREPOSITORY";

        //private readonly string idCacheCreateRnDExtraction = "CreateRnDExtraction";

        public TKRegistryController(WebAppDbContext context, ILogger<TKRegistryController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // *********************************************************************************************************************
        // CLASSIFICATION 
        // *********************************************************************************************************************
        public IActionResult IndexClassification(string configId)
        {
            IndexClassificationDTO dto = new IndexClassificationDTO();
            try
            {
                dto.ConfigModel = Lookup.GetTKSupportConfig(configId);
            }
            catch (Exception e)
            {

                TempData["MsgToLayout"] = String.Format("Config Type Not Found (id {0})", configId);
                return RedirectToAction("Index", "Home");
            }
            return View(dto);
        }
        // GET: TKRegistry/EditClassification/5
        public async Task<IActionResult> EditClassification(int ClassificationId = 0, int ParentId = 0, string ConfigId = "")
        {
            editClassificationDTO dto = new editClassificationDTO();
            dto.TKClassificationId = ClassificationId;
            dto.ConfigId = ConfigId;
            if (ParentId != 0)
            {
                dto.ParentId = ParentId;
            }
            if (ClassificationId > 0)
            {
                var Classification =  _context.TKClassifications.Find(ClassificationId);
                if (Classification == null)
                {
                    return BadRequest();
                }
                PropertyCopier<TKClassification, editClassificationDTO>.Copy(Classification, dto); // Copio propriet� con stesso nome
            }
            dto.ConfigModel = Lookup.GetTKSupportConfig(dto.ConfigId);
            return PartialView("_EditClassification", dto);
        }
        [HttpPost]
        public IActionResult UpdateClassificationJSON([FromBody]TKClassification DTO)
        {
            // Forzatura Riservato = true se lo � anche il PARENT !! 
            if (DTO.ParentId > 0)
            {
                TKClassification parent = _context.TKClassifications.Find(DTO.ParentId);
                if (parent != null && parent.Reserved == true)
                {
                    DTO.Reserved = true;
                }
            }

            if (DTO.TKClassificationId == 0)
            {
                if (DTO.ParentId <= 0)
                {
                    DTO.ParentId = null;
                }
                DTO.TmstInse = DateTime.Now;
                DTO.UserInse = User.Identity.Name;
                _context.Add(DTO);
            }
            else
            {
                TKClassification data2Update = _context.TKClassifications.Find(DTO.TKClassificationId);
                if (data2Update == null)
                {
                    return NotFound();
                }
                if (DTO.ParentId != 0)
                {
                    data2Update.ParentId = DTO.ParentId;
                }
                data2Update.Code = DTO.Code;
                data2Update.Title = DTO.Title;
                data2Update.Reserved = DTO.Reserved;
                data2Update.Instrument = DTO.Instrument;
                data2Update.OtherPlatform = DTO.OtherPlatform;
                data2Update.CodArt = DTO.CodArt;
                data2Update.AddInfo = DTO.AddInfo;
                data2Update.TKAddInfoId = (DTO.AddInfo == Lookup.Classification_RadioGroup.No ? null : DTO.TKAddInfoId);
                data2Update.ClosingInfo = DTO.ClosingInfo;
                data2Update.TKClosingInfoId = (DTO.ClosingInfo == Lookup.Classification_RadioGroup.No ? null : DTO.TKClosingInfoId);
                data2Update.ClaimLogFile = DTO.ClaimLogFile;
                data2Update.NotifyAreaManager = DTO.NotifyAreaManager;
                data2Update.InfoContact = DTO.InfoContact;
                data2Update.Quantity= DTO.Quantity;
                data2Update.ToDoList= DTO.ToDoList;
                data2Update.SupplierEngagement= DTO.SupplierEngagement;
                data2Update.LocationSGAT= DTO.LocationSGAT;
                data2Update.CustomerSGAT= DTO.CustomerSGAT;
                data2Update.TmstLastUpd = DateTime.Now;
                data2Update.UserLastUpd = User.Identity.Name;
                _context.Update(data2Update);
            }
            _context.SaveChanges();
            ReloadClassificationPaths();  /// ?? da spostare in jobConsole
            return Json("");
        }

        public ActionResult ExportClassification(string id)
        {

            var dati = (new SupportBO()).GetClassificationsForExport(configId:id);

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("RequestClassifications");
            workSheet.Cells[1, 1].LoadFromCollection(dati, true);
            // personalizzazione CAPTION (Parto dal fondo , da destra, perch� vengono cancellate le colonne !!)
            TKSupportConfigModel configModel = Lookup.GetTKSupportConfig(id);
            for (int i = 22; i > 1; i--)
            {
                string titolo = workSheet.Cells[1, i].Value.ToString();
                string titoloConfig = titolo;
                foreach (var prop in configModel.GetType().GetProperties())
                {
                    if (prop.Name.Equals(titolo))
                    {
                        titoloConfig = string.Format("{0}", prop.GetValue(configModel, null));
                    }

                }
                if (titolo.Equals("AdditionalInfoTitle"))
                {
                    titoloConfig = configModel.AdditionalInfo;
                    if (!string.IsNullOrWhiteSpace(titoloConfig)) titoloConfig += " Title";
                }
                if (titolo.Equals("ClosingInfoTitle"))
                {
                    titoloConfig = configModel.ClosingInfo;
                    if (!string.IsNullOrWhiteSpace(titoloConfig)) titoloConfig += " Title";
                }
                if (!string.IsNullOrWhiteSpace(titoloConfig))
                {
                    workSheet.Cells[1, i].Value = titoloConfig;
                } else
                {
                    workSheet.DeleteColumn(i);
                }
            }
            for (int i = 1; i < 25; i++)
            {
                workSheet.Column(i).AutoFit();
            }
            //using (var range = workSheet.Cells[posData, 1, posData, nColumns])
            //{
            //    range.Style.Font.Bold = true;
            //    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
            //    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            //    range.Style.Font.Color.SetColor(Color.White);
            //}
            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("Classification_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
        }

        [HttpGet]
        public object GetClassifications(DataSourceLoadOptions loadOptions, int excludeClassificationId = 0, bool bExpandAll = false, 
                                                          bool bHideRoot = false, bool bLoadBreadCrumb = false, string configId = "")
        {
            var data = (new SupportBO()).GetClassifications(excludeClassificationId: excludeClassificationId, bExpandAll: bExpandAll, 
                                                            bHideRoot: bHideRoot, bLoadBreadCrumb: bLoadBreadCrumb, configId: configId );
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public async Task<IActionResult> DeleteClassification(int ClassificationId)
        {
            IActionResult ret = Ok("OK");
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC TKSetClassificationDeleted @id, @Deleted", new { id = ClassificationId, Deleted = true });
                }
            }
            catch (Exception e)
            {
                var msgErr = string.Format("DeleteClassification() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.LogError(msgErr);
                ret = BadRequest(msgErr);
            }
            return ret;
        }


        [HttpGet]
        public async Task<IActionResult> ApplyConfiguration(int ClassificationId)
        {
            IActionResult ret = Ok("OK");
            List<object> args = new List<object>();
            args.Clear();
            string Sql = @"UPDATE A 
                            SET A.INSTRUMENT = RIF.INSTRUMENT, A.CLAIMLOGFILE  = RIF.CLAIMLOGFILE,
                                A.ADDINFO = RIF.ADDINFO, A.TKADDINFOID = RIF.TKADDINFOID, 
                                A.CLOSINGINFO = RIF.CLOSINGINFO, A.TKCLOSINGINFOID = RIF.TKCLOSINGINFOID,
                                A.OTHERPLATFORM  = RIF.OTHERPLATFORM, A.CODART = RIF.CODART,
                                A.INFOCONTACT = RIF.INFOCONTACT, A.QUANTITY = RIF.QUANTITY, 
                                A.TODOLIST = RIF.TODOLIST, A.SUPPLIERENGAGEMENT = RIF.SUPPLIERENGAGEMENT, 
                                A.LOCATIONSGAT = RIF.LOCATIONSGA, A.CUSTOMERSGAT = RIF.CUSTOMERSGAT
                            FROM TKCLASSIFICATIONS A
                            INNER JOIN TKCLASSIFICATIONS RIF
                            ON RIF.TKCLASSIFICATIONID = @ClassificationId 
                            WHERE A.TKCLASSIFICATIONID IN (SELECT TKCLASSIFICATIONID FROM TKCLASSIFICATIONPATHS 
                                                            WHERE TKCLASSIFICATIONIDREF = @ClassificationId 
                                                            AND TKCLASSIFICATIONIDREF != TKCLASSIFICATIONID)";
            args.Add(new { ClassificationId = ClassificationId });
            try
            {
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                var msgErr = string.Format("ApplyConfiguration() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.LogError(msgErr);
                ret = BadRequest(msgErr);
            }
            return ret;
        }

        

        //[HttpGet]
        //public IActionResult SetReservedClassification(int ClassificationId)
        //{
        //    IActionResult ret = Ok("");
        //    var classification = _context.TKClassifications.Find(ClassificationId);
        //    if (classification != null)
        //    {
        //        if (classification.ParentId != null)
        //        {
        //            var parent = _context.TKClassifications.Find(classification.ParentId);
        //            if (parent != null)
        //            {
        //                if (classification.Reserved == true && parent.Reserved == true)
        //                {
        //                    //return BadRequest("Invalid Operation for selected classification!");
        //                    return Ok(string.Format("Invalid Operation for element '{0}'.", classification.Title));
        //                }
        //            }
        //        }

        //        dynamic retExec = null;
        //        try
        //        {
        //            using (IDatabase db = Connection)
        //            {
        //                retExec = db.Fetch<dynamic>(";EXEC TKSetClassificationReserved @id, @Reserved", new { id = ClassificationId, Reserved = !classification.Reserved });
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            var msgErr = string.Format("SetReservedClassification() - ERRORE {0} {1}", e.Message, e.Source);
        //            _logger.LogError(msgErr);
        //            ret = BadRequest(msgErr);
        //        }

        //    }
        //    return ret;
        //}

        [HttpGet]
        public IActionResult MoveClassification(int ClassificationId, int TargetParentId)
        {
            string msg = "";
            try
            {
                TKClassification Classification = _context.TKClassifications.Find(ClassificationId);
                if (Classification != null)
                {
                    if (TargetParentId > 0)
                    {
                        Classification.ParentId = TargetParentId;
                    }
                    else
                    {
                        Classification.ParentId = null;
                    }
                    Classification.TmstLastUpd = DateTime.Now;
                    Classification.UserLastUpd = User.Identity.Name;
                    _context.Update(Classification);
                    _context.SaveChanges();
                }
                // Rigenera Alberto percorsi 
                msg = ReloadClassificationPaths();  /// ?? da spostare in jobConsole
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "MoveClassification", e.Message, e.Source);
            }
            return Json(msg);
        }

        private string ReloadClassificationPaths()
        {
            string msgErr = "";
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC TKLoadClassificationPaths");
                }
            }
            catch (Exception e)
            {
                msgErr = String.Format("[{0}] - {1} / {2}", "ReloadClassificationPaths", e.Message, e.Source);
                _logger.LogError(msgErr);
            }
            return msgErr;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetClassificationBreadCrumb(int TKClassificationId)
        {
            string ret = "";
            var path = _context.TKClassificationPaths.Where(x => x.TKClassificationId == TKClassificationId && x.TKClassificationIdRef == x.TKClassificationId).FirstOrDefault();
            return Ok(path.BreadCrumb);
        }
        public JsonResult GetClassificationJSON(int TKClassificationId)
        {
            TKClassification data = _context.TKClassifications.Where(x => x.TKClassificationId == TKClassificationId).FirstOrDefault();
            return Json(data);
        }
        // *********************************************************************************************************************
        // ADDITIONAL INFORMATION 
        // *********************************************************************************************************************
        public IActionResult IndexAddInfo(string configId, string groupId)
        {
            IndexAddInfoDTO dto = new IndexAddInfoDTO();
            try
            {
                dto.ConfigModel = Lookup.GetTKSupportConfig(configId);
                dto.GroupId = groupId;
                dto.Title = "Additional Informations";
                if (groupId.Equals(Lookup.TKSupport_Config_GroupId_TODOLIST))
                {
                    dto.Title = "ToDo List Templates";
                }
            }
            catch (Exception e)
            {

                TempData["MsgToLayout"] = String.Format("Config Type Not Found (id {0})", configId);
                return RedirectToAction("Index", "Home");
            }
            return View(dto);
        }
        [HttpGet]
        public async Task<IActionResult> DeleteAddInfo(int AddInfoId)
        {
            IActionResult ret = Ok("OK");
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC TKSetAddInfoDeleted @id, @Deleted", new { id = AddInfoId, Deleted = true });
                }
            }
            catch (Exception e)
            {
                var msgErr = string.Format("DeleteAddInfo() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.LogError(msgErr);
                ret = BadRequest(msgErr);
            }
            return ret;
        }
        public IActionResult SetReservedAddInfo(int AddInfoId)
        {
            IActionResult ret = Ok("");
            var AddInfo = _context.TKAddInfos.Find(AddInfoId);
            if (AddInfo != null)
            {
                if (AddInfo.ParentId != null)
                {
                    var parent = _context.TKAddInfos.Find(AddInfo.ParentId);
                    if (parent != null)
                    {
                        if (AddInfo.Reserved == true && parent.Reserved == true)
                        {
                            //return BadRequest("Invalid Operation for selected AddInfo!");
                            return Ok(string.Format("Invalid Operation for element '{0}'.", AddInfo.Title));
                        }
                    }
                }

                dynamic retExec = null;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        retExec = db.Fetch<dynamic>(";EXEC TKSetAddInfoReserved @id, @Reserved", new { id = AddInfoId, Reserved = !AddInfo.Reserved });
                    }
                }
                catch (Exception e)
                {
                    var msgErr = string.Format("SetReservedAddInfo() - ERRORE {0} {1}", e.Message, e.Source);
                    _logger.LogError(msgErr);
                    ret = BadRequest(msgErr);
                }

            }
            return ret;
        }
        [HttpGet]
        public IActionResult MoveAddInfo(int AddInfoId, int TargetParentId)
        {
            string msg = "";
            try
            {
                TKAddInfo AddInfo = _context.TKAddInfos.Find(AddInfoId);
                if (AddInfo != null)
                {
                    if (TargetParentId > 0)
                    {
                        AddInfo.ParentId = TargetParentId;
                    }
                    else
                    {
                        AddInfo.ParentId = null;
                    }
                    AddInfo.TmstLastUpd = DateTime.Now;
                    AddInfo.UserLastUpd = User.Identity.Name;
                    _context.Update(AddInfo);
                    _context.SaveChanges();
                }
                // Rigenera Alberto percorsi 
                msg = ReloadAddInfoPaths();  // Inserimento Richiesta per State Machine 
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "MoveAddInfo", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public IActionResult CanMoveAddInfo()
        {
            string msg = "";
            try
            {
                if (_context.ProcessRequests.Where(t => t.ProcessType.Equals(Lookup.ProcessRequest_Type_ReloadAddInfoPaths) && t.Status.Equals(Lookup.ProcessRequest_Status_NEW)).Count() > 0)
                {
                    msg = "Synchronization process in progress; try again later!";
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "CanMoveAddInfo", e.Message, e.Source);
            }
            return Json(msg);
        }
        private string ReloadAddInfoPaths()
        {
            string msgErr = "";
            ProcessRequest newRequest = new ProcessRequest()
            {
                ProcessType = Lookup.ProcessRequest_Type_ReloadAddInfoPaths,
                ProcessParms = null,
                Status = Lookup.ProcessRequest_Status_NEW,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name
            };
            if (_context.ProcessRequests.Where(t => t.ProcessType.Equals(newRequest.ProcessType) && t.Status.Equals(Lookup.ProcessRequest_Status_NEW)).Count() == 0)
            {
                _context.ProcessRequests.Add(newRequest);
                _context.SaveChanges();
            }

            //dynamic retExec = null;
            //try
            //{
            //    using (IDatabase db = Connection)
            //    {
            //        retExec = db.Fetch<dynamic>(";EXEC TKLoadAddInfoPaths");
            //    }
            //}
            //catch (Exception e)
            //{
            //    msgErr = String.Format("[{0}] - {1} / {2}", "ReloadAddInfoPaths", e.Message, e.Source);
            //    _logger.LogError(msgErr);
            //}

            return msgErr;
        }
        // GET: TKRegistry/EditAddInfo/5
        public async Task<IActionResult> EditAddInfo(int AddInfoId = 0, int ParentId = 0, string ConfigId = "", string groupId = "")
        {
            editAddInfoDTO dto = new editAddInfoDTO();
            dto.TKAddInfoId = AddInfoId;
            dto.ConfigId = ConfigId;
            dto.GroupId = groupId;
            if (ParentId != 0)
            {
                dto.ParentId = ParentId;
            }
            if (AddInfoId > 0)
            {
                TKAddInfo addinfo = _context.TKAddInfos.Find(AddInfoId);
                if (addinfo == null)
                {
                    return BadRequest();
                }
                PropertyCopier<TKAddInfo, editAddInfoDTO>.Copy(addinfo, dto); // Copio propriet� con stesso nome
            }
            dto.ConfigModel = Lookup.GetTKSupportConfig(dto.ConfigId);
            return PartialView("_EditAddInfo", dto);
        }
        public ActionResult ExportAddInfo(string id)
        {
            ExcelPackage excel = new ExcelPackage();

            var roots = _context.TKAddInfos.Where(x => x.ParentId == null && x.ConfigId.Equals(id)).ToList();
            foreach(var item in roots)
            {
                try
                {
                    var dati = (new SupportBO()).GetAddInfosForExport(item.TKAddInfoId);
                    var workSheet = excel.Workbook.Worksheets.Add(item.Title);
                    workSheet.Cells[1, 1].LoadFromCollection(dati, true);
                    // AUTO FIT Parto dal fondo (da destra) perch� vengono cancellate le colonne !!! 
                    for (int i = 1; i <= 20; i++)
                    {
                        workSheet.Column(i).AutoFit();
                    }
                } 
                catch (Exception e)
                {
                    // N.A.
                }
            }
            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("AdditionalInfo_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
        }
        [HttpGet]
        public object GetAllAddInfos(DataSourceLoadOptions loadOptions, int excludeAddInfoId = 0, bool bExpandAll = false, int relativeRootId = 0, 
                                                      bool bHideRoot = false, bool bLoadBreadCrumb = false, string configId = "", string groupId = "")
        {
            var data = (new SupportBO()).GetPublicAddInfos(excludeAddInfoId: excludeAddInfoId, bExpandAll: bExpandAll, relativeRootId: relativeRootId, 
                                                           bHideRoot: bHideRoot, bLoadBreadCrumb: bLoadBreadCrumb, configId: configId, groupId: groupId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public IActionResult UpdateAddInfoJSON([FromBody]TKAddInfo DTO)
        {
            if (DTO.TKAddInfoId == 0)
            {
                if (DTO.ParentId > 0)
                {
                    TKAddInfo parent = _context.TKAddInfos.Find(DTO.ParentId);
                    if (parent != null)
                    {
                        DTO.Reserved = parent.Reserved;
                    }
                }
                else
                {
                    DTO.ParentId = null;
                }
                DTO.TmstInse = DateTime.Now;
                DTO.UserInse = User.Identity.Name;
                _context.Add(DTO);
            }
            else
            {
                TKAddInfo data2Update = _context.TKAddInfos.Find(DTO.TKAddInfoId);
                if (data2Update == null)
                {
                    return NotFound();
                }
                if (DTO.ParentId != 0)
                {
                    data2Update.ParentId = DTO.ParentId;
                }
                data2Update.Code = DTO.Code;
                data2Update.Title = DTO.Title;
                data2Update.Color = DTO.Color;
                data2Update.Reserved = false;
                data2Update.Deleted = false;
                data2Update.TmstLastUpd = DateTime.Now;
                data2Update.UserLastUpd = User.Identity.Name;
                _context.Update(data2Update);
            }
            _context.SaveChanges();
            ReloadAddInfoPaths();  // Inserimento Richiesta per State Machine 
            return Json("");
        }
        [HttpGet]
        public object GetAddInfoRoots(DataSourceLoadOptions loadOptions, string configId, string groupId)
        {
            var data = _context.TKAddInfos.Where(x => x.Deleted == false && x.Reserved == false && x.ParentId == null && 
                                                 x.ConfigId.Equals(configId) && x.GroupId.Equals(groupId)).ToList();
            return DataSourceLoader.Load(data, loadOptions);

        }
        [HttpGet]
        public async Task<IActionResult> GetAddInfoJSON(int TKAddInfoId)
        {
            TKAddInfo data = _context.TKAddInfos.Find(TKAddInfoId);
            return Json(data);
        }

        // *********************************************************************************************************************
        // CLOSING INFORMATION 
        // *********************************************************************************************************************
        public IActionResult IndexClosingInfo(string configId)
        {
            IndexClosingInfoDTO dto = new IndexClosingInfoDTO();
            try
            {
                dto.ConfigModel = Lookup.GetTKSupportConfig(configId);
            }
            catch (Exception e)
            {

                TempData["MsgToLayout"] = String.Format("Config Type Not Found (id {0})", configId);
                return RedirectToAction("Index", "Home");
            }
            return View(dto);
        }
        [HttpGet]
        public async Task<IActionResult> DeleteClosingInfo(int ClosingInfoId)
        {
            IActionResult ret = Ok("OK");
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC TKSetClosingInfoDeleted @id, @Deleted", new { id = ClosingInfoId, Deleted = true });
                }
            }
            catch (Exception e)
            {
                var msgErr = string.Format("DeleteClosingInfo() - ERRORE {0} {1}", e.Message, e.Source);
                _logger.LogError(msgErr);
                ret = BadRequest(msgErr);
            }
            return ret;
        }
        private string ReloadClosingInfoPaths()
        {
            string msgErr = "";
            dynamic retExec = null;
            try
            {
                using (IDatabase db = Connection)
                {
                    retExec = db.Fetch<dynamic>(";EXEC TKLoadClosingInfoPaths");
                }
            }
            catch (Exception e)
            {
                msgErr = String.Format("[{0}] - {1} / {2}", "ReloadClosingInfoPaths", e.Message, e.Source);
                _logger.LogError(msgErr);
            }
            return msgErr;
        }

        // GET: TKRegistry/EditClosingInfo/5
        public async Task<IActionResult> EditClosingInfo(int ClosingInfoId = 0, int ParentId = 0, string ConfigId = "")
        {
            editClosingInfoDTO dto = new editClosingInfoDTO();
            dto.TKClosingInfoId = ClosingInfoId;
            dto.ConfigId = ConfigId;
            if (ParentId != 0)
            {
                dto.ParentId = ParentId;
            }
            if (ClosingInfoId > 0)
            {
                TKClosingInfo closingInfo = _context.TKClosingInfos.Find(ClosingInfoId);
                if (closingInfo  == null)
                {
                    return BadRequest();
                }
                PropertyCopier<TKClosingInfo, editClosingInfoDTO>.Copy(closingInfo, dto); // Copio propriet� con stesso nome
            }

            dto.ConfigModel = Lookup.GetTKSupportConfig(dto.ConfigId);
            return PartialView("_EditClosingInfo", dto);
        }
        public ActionResult ExportClosingInfo(string id)
        {
            ExcelPackage excel = new ExcelPackage();

            var roots = _context.TKClosingInfos.Where(x => x.ParentId == null && x.ConfigId.Equals(id) ).ToList();
            foreach (var item in roots)
            {
                try { 
                    var dati = (new SupportBO()).GetClosingInfosForExport(item.TKClosingInfoId);
                    var workSheet = excel.Workbook.Worksheets.Add(item.Title);
                    workSheet.Cells[1, 1].LoadFromCollection(dati, true);
                    // AUTO FIT Parto dal fondo (da destra) perch� vengono cancellate le colonne !!! 
                    for (int i = 1; i <= 20; i++)
                    {
                        workSheet.Column(i).AutoFit();
                    }
                }
                catch (Exception e)
                {
                    // N.A.
                }
            }
            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("ClosingInfo_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
        }
        [HttpGet]
        public object GetAllClosingInfos(DataSourceLoadOptions loadOptions, int excludeClosingInfoId = 0, bool bExpandAll = false, int relativeRootId = 0,
                                                      bool bHideRoot = false, bool bLoadBreadCrumb = false, string configId = "")
        {
            var data = (new SupportBO()).GetPublicClosingInfos(excludeClosingInfoId: excludeClosingInfoId, bExpandAll: bExpandAll, relativeRootId: relativeRootId, 
                                                               bHideRoot: bHideRoot, bLoadBreadCrumb: bLoadBreadCrumb, configId: configId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public IActionResult UpdateClosingInfoJSON([FromBody]TKClosingInfo DTO)
        {
            if (DTO.TKClosingInfoId == 0)
            {
                if (DTO.ParentId < 0)
                {
                    DTO.ParentId = null;
                }
                DTO.TmstInse = DateTime.Now;
                DTO.UserInse = User.Identity.Name;
                _context.Add(DTO);
            }
            else
            {
                TKClosingInfo data2Update = _context.TKClosingInfos.Find(DTO.TKClosingInfoId);
                if (data2Update == null)
                {
                    return NotFound();
                }
                if (DTO.ParentId != 0)
                {
                    data2Update.ParentId = DTO.ParentId;
                }
                data2Update.Code = DTO.Code;
                data2Update.Title = DTO.Title;
                data2Update.Deleted = false;
                data2Update.TmstLastUpd = DateTime.Now;
                data2Update.UserLastUpd = User.Identity.Name;
                _context.Update(data2Update);
            }
            _context.SaveChanges();
            ReloadClosingInfoPaths();
            return Json("");
        }


        [HttpGet]
        public object GetClosingInfoRoots(DataSourceLoadOptions loadOptions, string configId)
        {
            var data = _context.TKClosingInfos.Where(x => x.Deleted == false && x.ParentId == null && x.ConfigId.Equals(configId)).ToList();
            return DataSourceLoader.Load(data, loadOptions);

        }
    }
}

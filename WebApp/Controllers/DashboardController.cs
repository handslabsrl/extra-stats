using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using DevExtreme.AspNet.Data;
using WebApp.Entity;
using WebApp.Repository;
using WebApp.Data;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using System.Drawing;
using System.Reflection;
using WebApp.Models;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using WebApp.Classes;

namespace WebApp.Controllers
{
    [Authorize(Roles = "ELITe Instruments")]
    public class DashboardController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        //private readonly IStringLocalizer<DashboardController> _localizer;

        //public DashboardController(WebAppDbContext context, ILogger<DashboardController> logger, IStringLocalizer<DashboardController> localizer)
        public DashboardController(WebAppDbContext context, ILogger<DashboardController> logger)
        {
            _context = context;
            _logger = logger;
            //_localizer = localizer;
        }


        public IActionResult Test(DailyExecutionsDTO dto)
        {
            //List<ExecutionModel> ret = (new ExecutionsBO()).Get();
            //List<DailyExecutionsDetail> prova = new List<DailyExecutionsDetail>();
            //prova.Add(new DailyExecutionsDetail { StartRunDateRif = "01/01/2018", num_test = 1120, process_pcr = 1120, process_extr = 576 });
            //prova.Add(new DailyExecutionsDetail { StartRunDateRif = "02/01/2018", num_test = 768, process_pcr = 1120, process_extr = 576 });
            //prova.Add(new DailyExecutionsDetail { StartRunDateRif = "03/01/2018", num_test = 1024, process_pcr = 1000, process_extr = 1020 });
            //prova.Add(new DailyExecutionsDetail { StartRunDateRif = "04/01/2018", num_test = 400, process_pcr = 356, process_extr = 399 });
            //prova.Add(new DailyExecutionsDetail { StartRunDateRif = "05/01/2018", num_test = 900, process_pcr = 768, process_extr = 35 });
            try
            {
                //dto.dati = (new ExecutionsBO()).Get(DataDa: new DateTime(2018, 1, 1), DataA: DateTime.Now);
                //dto.dati = (new ExecutionsBO()).Get(DataDa: DateTime.Now.AddDays(-31), DataA: DateTime.Now);
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("{0} - {1} / {2}", "ExecutionsBO.Get()", e.Message, e.Source));
            }
            return View(dto);
        }

        public IActionResult Index()
        {
            //var Sql = "";
            //List<object> args = new List<object>();
            //var ret = new List<Executions>();
            ////string cnnStr = ConfigurationManager.AppSettings["cnnStr"];
            ////Sql = @"SELECT COUNT(*) FROM DATAMAIN";
            //Sql = @"SELECT STARTRUNTIME, COUNT(*) AS NUM_TEST,
            //        SUM(PROCESS_PCR) AS process_pcr, SUM(process_extr) AS process_extr
            //        FROM (
            //            SELECT STARTRUNTIME, IIF(PROCESS IN (0, 1) , 1, 0) AS PROCESS_PCR, IIF(PROCESS IN (1, 2) , 1, 0) AS process_extr 
            //            FROM DATAMAIN
            //            INNER JOIN INSTRUMENTS 
            //              ON DATAMAIN.SERIALNUMBER = INSTRUMENTS.SERIALNUMBER
            //            INNER JOIN GASSAYPROGRAMS 
            //              ON DATAMAIN.SERIALNUMBER = GASSAYPROGRAMS.SERIAL
            //              AND DATAMAIN.GENERALASSAYID = GASSAYPROGRAMS.ASSAYID
            //            WHERE 1 = 1
            //            AND   DATAMAIN.SERIALNUMBER = '2071603B0069E'
            //            AND INSTRUMENTS.CUSTOMERCODE = 'C00325' 
            //            AND INSTRUMENTS.SITE_CODE = '000'
            //            AND INSTRUMENTS.SITE_COUNTRY = 'ITALY'
            //            AND GASSAYPROGRAMS.ASSAYID = 18
            //            ) AS TMP
            //            GROUP BY STARTRUNTIME";
            //// --WHERE STARTRUNTIME > '2016-12-09' AND STARTRUNTIME < '2017-01-01'
            ////args.Add(new { articolo = String.Format("%{0}%", term.ToUpper().Trim()) });
            ////args.Add(new { idCliente = idCliente });
            ////args.Add(new { idFornitore = idFornitore });
            ////args.Add(new { statoCreato = Lookup.stato_CREATO });
            ////args.Add(new { statoAnnullato = Lookup.stato_ANNULLATO });

            ////var nCnt = 0;
            //try
            //{
            //    using (IDatabase db = Connection)
            //    {
            //        //nCnt = db.ExecuteScalar<int>(Sql, args.ToArray());
            //        ret = db.Query<Executions>(Sql, args.ToArray()).ToList();
            //    }
            //}
            //catch (Exception e)
            //{
            //    //logger.Error(String.Format("{0} - {1} / {2}", "getArticoliJson", e.Message, e.Source));
            //}


            return View();
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult DailyExecutions(DailyExecutionsDTO dto, string opt)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            if (!string.IsNullOrWhiteSpace(UserId))
            {
                if (dto.countriesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                    if (valori.Count == 1)
                    {
                        dto.countriesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.customersSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                    if (valori.Count == 1)
                    {
                        dto.customersSel = new List<string> { valori[0] };
                    }
                }
                if (dto.commercialEntitiesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                    if (valori.Count == 1)
                    {
                        dto.commercialEntitiesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.regionsSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Region);
                    if (valori.Count == 1)
                    {
                        dto.regionsSel = new List<string> { valori[0] };
                    }
                }

                UserCfg userCfg = _context.UserCfg.Find(UserId);
                if (userCfg != null)
                {
                    if (!String.IsNullOrWhiteSpace(userCfg.Area))
                    {
                        dto.Area = userCfg.Area;
                        dto.lockArea = true;
                    }
                }
            }
            //_logger.LogInformation("DailyExecutions");
            // nel caso di EXPORT recupero gli ultimi filtri dalla sessione in modo da avere esattamente gli stessi filtri 
            // usati per l'ultima ricerca; attenzione: la cache dura 30minuti !! 
            // valori possibili per <opt>
            // null/''      - indica l'applicazione dei filtri di ricerca
            // init         - prima esecuzione (serve per condividere i filtri tra DailyExecution e AssayUtilization)
            // detail       - esportazione Analitycs Detail
            // counters     - esportazione ChartData Total
            // detailshort  - esportazione Analitycs Detail (Ridotto)
            // detailmkt    - MKT Extraction
            if (!String.IsNullOrWhiteSpace(opt))
            {
                try
                {
                    if (opt.ToLower().Equals("init"))
                    {
                        // recupero i filtri dalla sessione !!! 
                        if (HttpContext.Session.GetString(Lookup.idCache_DailyExecutionsFilters) != null)
                        {
                            dto = JsonConvert.DeserializeObject<DailyExecutionsDTO>(HttpContext.Session.GetString(Lookup.idCache_DailyExecutionsFilters));
                        }
                        if (HttpContext.Session.GetString(Lookup.idCache_AssaysUtilizationFilters) != null)
                        {
                            try
                            {
                                AssaysUtilizationDTO dtoAssaysCache = new AssaysUtilizationDTO();
                                dtoAssaysCache = JsonConvert.DeserializeObject<AssaysUtilizationDTO>(HttpContext.Session.GetString(Lookup.idCache_AssaysUtilizationFilters));
                                if (dto.tmstUpdCache == null || dtoAssaysCache.tmstUpdCache > dto.tmstUpdCache)
                                {
                                    PropertyCopier<AssaysUtilizationDTO, DailyExecutionsDTO>.Copy(dtoAssaysCache, dto); // Copio propriet� con stesso nome
                                }
                            }
                            catch (Exception e)
                            {
                                _logger.LogError(String.Format("{0} - {1} / {2}", "DailyExecutions.RestoreCache idCache_AssaysUtilizationFilters", e.Message, e.Source));
                            }

                        }
                    }
                    else
                    {
                        dto = JsonConvert.DeserializeObject<DailyExecutionsDTO>(HttpContext.Session.GetString(Lookup.idCache_DailyExecutionsFilters));
                    }
                }
                catch
                {
                    // Nessuna azione 
                }
            }

            if (dto.DateFrom == DateTime.MinValue)
            {
                dto.DateFrom = DateTime.Now.AddDays(-31);
            }
            if (dto.DateTo == DateTime.MinValue)
            {
                dto.DateTo = DateTime.Now;
            }

            ExecutionsBO.getType tipoEstrazione = ExecutionsBO.getType.series;
            bool bUseHangFire = false;
            if (!String.IsNullOrWhiteSpace(opt))
            {
                switch (opt.ToLower())
                {
                    case "detail": tipoEstrazione = ExecutionsBO.getType.exportDetail; break;
                    case "detailshort": tipoEstrazione = ExecutionsBO.getType.exportDetailShort;  break;
                    case "detailmkt": tipoEstrazione = ExecutionsBO.getType.exportMKTExtraction; break;
                }
            }

            // ESTRAZIONE DATI 
            GetDailyExecutionsDTO filters = new GetDailyExecutionsDTO()
            {
                dto = dto,
                userId = UserId,
                tipo = ((bUseHangFire) ? ExecutionsBO.getType.series : tipoEstrazione),
                opt = opt
            };
            ExecutionModel ext = (new ExecutionsBO()).GetDailyExecutions(filters);
            if (!String.IsNullOrWhiteSpace(opt))
            {
                if (opt.ToLower().Equals("counters")) {
                    //return File(ExportDailyExecutions<ExecutionCounters>(ext, opt),
                    //            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                    //            string.Format("DailyExecutions_ ChartDataTotal_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    var stream = ExportDailyExecutions<ExecutionCounters>(ext, opt);
                    return new FileStreamResult(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml")
                    {
                        FileDownloadName = string.Format("DailyExecutions_ ChartDataTotal_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss"))
                    };
                }
                if (!bUseHangFire)
                {
                    switch (opt.ToLower())
                    {
                        case "detail":
                            return File(ExportDailyExecutions<ExecutionDetail>(ext, opt),
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("DailyExecutions_AnalyticsDetail_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                        case "detailshort":
                            return File(ExportDailyExecutions<ExecutionDetailShort>(ext, opt),
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("DailyExecutions_AnalyticsDetail (Short)_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                        case "detailmkt":
                            return File(ExportDailyExecutions<ExecutionDetailMKT>(ext, opt),
                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                                string.Format("DailyExecutions_MKT Extraction_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    }
                } else
                {
                    if (tipoEstrazione!= ExecutionsBO.getType.exportSeries)
                    {
                        filters.tipo = tipoEstrazione;
                        string jobId = "";
                        //jobId = Hangfire.BackgroundJob.Enqueue(() => this.DailyExecutionsFAF(filters, null));
                        int lastJobId = (new UtilBO()).GetLastHangFireJobId();
                        if (lastJobId == 0)
                        {
                            jobId = Hangfire.BackgroundJob.Enqueue(() => this.DailyExecutionsFAF(filters, null));
                        }
                        else
                        {
                            jobId = Hangfire.BackgroundJob.ContinueJobWith(lastJobId.ToString(), () => this.DailyExecutionsFAF(filters, null));
                        }
                        TempData["MsgToNotify"] = string.Format("Scheduled Process {0}", jobId);
                    }
                }
            }

            // Salvataggio filtri di ricerca
            dto.tmstUpdCache = DateTime.Now;
            HttpContext.Session.SetString(Lookup.idCache_DailyExecutionsFilters, JsonConvert.SerializeObject(dto));
            dto.regions = (new RegionsBO()).GetRegions(UserIdCfg: UserId);
            dto.areas = (new AreasBO()).GetAreas(UserIdCfg: UserId);
            dto.commercialStatuses = (new CommercialStatusBO()).Get();
            dto.modelTypes = Lookup.InstrumentModelType_EliteInstruments;

            //String UserIdLink = "";
            //UserIdLink = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country).Count() > 0 ? UserId : "");
            dto.countries = (new CountriesBO()).GetCountries(UserIdCfg: UserId);
            dto.lockCountry = false;
            if (dto.countries.Count() == 1)
            {
                dto.lockCountry = true;
                dto.countriesSel = new List<string>() { dto.countries.FirstOrDefault().Country };
            }

            dto.commercialEntities = (new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId);
            dto.lockCommerialEntity = false;
            if (dto.commercialEntities.Count() == 1)
            {
                dto.lockCommerialEntity = true;
                dto.commercialEntitiesSel = new List<string>() { dto.commercialEntities.FirstOrDefault().CommercialEntity };
            }
            if (dto.regions.Count() == 1)
            {
                dto.lockRegion = true;
                dto.regionsSel = new List<string>() { dto.regions.FirstOrDefault().Region };
            }

            dto.lockCustomer = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer)).Count() == 1;

            // Serie e risultati 
            dto.series = ext.dailyExecutions;
            dto.summary = ext.filtersDesc;
            dto.cntInstruments = ext.cntInstruments;
            return View(dto);
        }


        // elenco DailyExecutionsFAF - Fire And Forget
        public async Task<IActionResult> DailyExecutionsFAF(GetDailyExecutionsDTO filters, Hangfire.Server.PerformContext context)
        {
            try
            {
                var jobId = context.BackgroundJob.Id;
                ExecutionModel ext = (new ExecutionsBO()).GetDailyExecutions(filters);
                Stream FileOut = null;
                switch (filters.opt.ToLower())
                {
                    case "detail":
                        FileOut = ExportDailyExecutions<ExecutionDetail>(ext, filters.opt);
                        //return File(ExportDailyExecutions<ExecutionDetail>(ext, filters.opt),
                        //    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                        //    string.Format("DailyExecutions_AnalyticsDetail_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                        break;
                    case "detailshort":
                        FileOut = ExportDailyExecutions<ExecutionDetailShort>(ext, filters.opt);
                        //return File(ExportDailyExecutions<ExecutionDetailShort>(ext, filters.opt),
                        //    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                        //    string.Format("DailyExecutions_AnalyticsDetail (Short)_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                        break;
                    case "detailmkt":
                        FileOut= ExportDailyExecutions<ExecutionDetailMKT>(ext, filters.opt);
                        //return File(ExportDailyExecutions<ExecutionDetailMKT>(ext, filters.opt),
                        //    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                        //    string.Format("DailyExecutions_MKT Extraction_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                        break;
                }
                if (FileOut != null)
                {
                    //throw new Exception(string.Format("PROVA"));
                    string FullFileName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "PROVA.xlsx");
                    using (var fileStream = System.IO.File.Create(FullFileName))
                    {
                        FileOut.CopyTo(fileStream);
                    }
                }

                /*
                 * salvare il file in una cartella specifica (export)
                 * registrazione export su tabella ExportJobs (???) -> user, data, nome file, cartellla, data scarico ? 
                 * inviare email al richiedente con il link per il download (DA SVILUPPARE !!!)
                 * signalR per notificare che il file � pronto
                 * -> verifica streaming via signalR del file 
                 * servizio per download del file ? 
                 * pagina per consultazione richiete 
                 */

            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();

        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult AssaysUtilization(AssaysUtilizationDTO dto, string opt)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            if (!string.IsNullOrWhiteSpace(UserId))
            {
                if (dto.countriesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Country);
                    if (valori.Count == 1)
                    {
                        dto.countriesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.customersSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer);
                    if (valori.Count == 1)
                    {
                        dto.customersSel = new List<string> { valori[0] };
                    }
                }
                if (dto.commercialEntitiesSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.CommercialEntity);
                    if (valori.Count == 1)
                    {
                        dto.commercialEntitiesSel = new List<string> { valori[0] };
                    }
                }
                if (dto.regionsSel == null)
                {
                    List<string> valori = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Region);
                    if (valori.Count == 1)
                    {
                        dto.regionsSel = new List<string> { valori[0] };
                    }
                }

                UserCfg userCfg = _context.UserCfg.Find(UserId);
                if (userCfg != null)
                {
                    if (!String.IsNullOrWhiteSpace(userCfg.Area))
                    {
                        dto.Area = userCfg.Area;
                        dto.lockArea = true;
                    }
                }
            }

            // nel caso di EXPORT recupero gli ultimi filtri dalla sessione in modo da avere esattamente gli stessi filtri 
            // usati per l'ultima ricerca; attenzione: la cache dura 30minuti !! 
            // valori possibili per <opt>
            // null/''  - indica l'applicazione dei filtri di ricerca
            // init     - prima esecuzione (serve per condividere i filtri tra DailyExecution e AssayUtilization)
            // export   - esportazione dat
            if (!String.IsNullOrWhiteSpace(opt))
            {
                try
                {
                    if (opt.ToLower().Equals("init"))
                    {
                        // recupero i filtri dalla sessione !!! 
                        if (HttpContext.Session.GetString(Lookup.idCache_AssaysUtilizationFilters) != null)
                        {
                            dto = JsonConvert.DeserializeObject<AssaysUtilizationDTO>(HttpContext.Session.GetString(Lookup.idCache_AssaysUtilizationFilters));
                        }
                        if (HttpContext.Session.GetString(Lookup.idCache_DailyExecutionsFilters) != null)
                        {
                            try
                            {
                                DailyExecutionsDTO dtoDailyCache = new DailyExecutionsDTO();
                                dtoDailyCache = JsonConvert.DeserializeObject<DailyExecutionsDTO>(HttpContext.Session.GetString(Lookup.idCache_DailyExecutionsFilters));
                                if (dto.tmstUpdCache == null || dtoDailyCache.tmstUpdCache > dto.tmstUpdCache)
                                {
                                    PropertyCopier<DailyExecutionsDTO, AssaysUtilizationDTO>.Copy(dtoDailyCache, dto); // Copio propriet� con stesso nome
                                }
                            }
                            catch (Exception e)
                            {
                                _logger.LogError(String.Format("{0} - {1} / {2}", "AssaysUtilization.RestoreCache idCache_DailyExecutionsFilters()", e.Message, e.Source));
                            }
                        }
                    }
                    else
                    {
                        dto = JsonConvert.DeserializeObject<AssaysUtilizationDTO>(HttpContext.Session.GetString(Lookup.idCache_AssaysUtilizationFilters));
                    }
                }
                catch
                {
                    // Nessuna azione 
                }
            }

            if (dto.DateFrom == DateTime.MinValue)
            {
                dto.DateFrom = DateTime.Now.AddDays(-31);
            }
            if (dto.DateTo == DateTime.MinValue)
            {
                dto.DateTo = DateTime.Now;
            }

            // ESTRAZIONE DATI 
            AssayUtilizationModel ext = (new ExecutionsBO()).GetAssaysUtilization(dto: dto, UserId: UserId);

            if (!String.IsNullOrWhiteSpace(opt) && opt.ToLower().Equals("export"))
            {
                return File(ExportAssaysUtilization(ext),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
                            string.Format("ReportableResult_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
            }

            // Salvataggio filtri 
            dto.tmstUpdCache = DateTime.Now;
            HttpContext.Session.SetString(Lookup.idCache_AssaysUtilizationFilters, JsonConvert.SerializeObject(dto));

            dto.regions = (new RegionsBO()).GetRegions(UserIdCfg: UserId);
            dto.areas = (new AreasBO()).GetAreas(UserIdCfg: UserId);
            dto.commercialStatuses = (new CommercialStatusBO()).Get();
            dto.modelTypes= Lookup.InstrumentModelType_EliteInstruments;
            dto.Pathogens = (new AssaysBO()).GetPathogens();

            dto.countries = (new CountriesBO()).GetCountries(UserIdCfg: UserId);
            dto.lockCountry = false;
            if (dto.countries.Count() == 1)
            {
                dto.lockCountry = true;
                dto.countriesSel = new List<string>() { dto.countries.FirstOrDefault().Country };
            }

            dto.commercialEntities = (new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId);
            dto.lockCommerialEntity = false;
            if (dto.commercialEntities.Count() == 1)
            {
                dto.lockCommerialEntity = true;
                dto.commercialEntitiesSel = new List<string>() { dto.commercialEntities.FirstOrDefault().CommercialEntity };
            }
            if (dto.regions.Count() == 1)
            {
                dto.lockRegion = true;
                dto.regionsSel = new List<string>() { dto.regions.FirstOrDefault().Region };
            }

            dto.lockCustomer = ((new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.Customer)).Count() == 1;

            // Series 
            dto.series = ext.AssaysUtilization;
            dto.details = ext.AssayUtilizationDetails;
            dto.summary = ext.filtersDesc;
            dto.CntRunAssayElite = ext.CntRunAssayElite;
            dto.TotRun = ext.TotRun;
            dto.cntInstruments = ext.cntInstruments;
            return View(dto);
        }

        ////[ValidateAntiForgeryToken]
        //public ActionResult Test2(DailyExecutionsDTO dto, string opt)
        //{

        //    if (dto.DateFrom == DateTime.MinValue)
        //    {
        //        dto.DateFrom = DateTime.Now.AddDays(-31);
        //    }
        //    if (dto.DateTo == DateTime.MinValue)
        //    {
        //        dto.DateTo = DateTime.Now;
        //    }

        //    dto.countries = (new CountriesBO()).GetCountries();

        //    return View(dto);

        //}

        public Stream ExportDailyExecutions<T>(ExecutionModel dati, string opt)
        {

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("DailyExecutions");

            int posData = 1;
            int nRows = 0;
            int nColumns = (typeof(T)).GetProperties().Length;

            // CARICAMENTO DATI
            List<string> fldDateTime = new List<string>();
            List<string> fld2Remove = new List<string>();
            int numColDeleted = 0;
            bool addTotals = false;
            if (opt.ToLower().Equals("counters"))
            {
                // CONTATORI 
                workSheet.Cells[posData, 1].LoadFromCollection(dati.dailyExecutions, true);
                nRows = dati.dailyExecutions.Count;
                fldDateTime = new List<string> { "StartRunTime" };
                fld2Remove = new List<string> { "StartRunDateRif" };
                addTotals = true;
                if (nRows > 0)
                {
                    using (var range = workSheet.Cells[posData + 1, 3, posData + nRows, 7])
                    {
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    }
                }
            } else {

                // DETTAGLIO  RIDOTTO o ESTESO 
                switch (opt.ToLower())
                {
                    case "detailshort":
                        workSheet.Cells[posData, 1].LoadFromCollection(dati.exportDetailShorts, true);
                        nRows = dati.exportDetailShorts.Count;
                        break;
                    case "detailmkt":
                        workSheet.Cells[posData, 1].LoadFromCollection(dati.exportDetailMKT, true);
                        nRows = dati.exportDetailMKT.Count;
                        break;
                    default:
                        workSheet.Cells[posData, 1].LoadFromCollection(dati.exportDetails, true);
                        nRows = dati.exportDetails.Count;
                        break;
                }

                fldDateTime = new List<string> { "ApprovalDateTime", "StartRunTime", "EndRunTime", "AbortTime", "Installation_Date", "RegisteredDate", 
                                                 "ExtractionCassetteExpiryDate", "ReagentExpiryDate", "IcExpiryDate", "CalibratorExpiryDate",
                                                 "ControlExpiryDate" , "ExtractionData"};
                fld2Remove = new List<string> { "RunStatus", "SampleTypes", "Process", "Melt", "ApprovalUserRole" };
            }

            // Formattazioni colonne Date/DateTime e cancellazione colonne aggiuntive (usate per deco)
            for (int i = 0; i < fldDateTime.Count; i++)
            {
                fldDateTime[i] = fldDateTime[i].ToLower();
                fldDateTime[i] = fldDateTime[i].Replace("_", " ");      // il carattere _ diventa " "
            }
            for (int i = 0; i < fld2Remove.Count; i++)
            {
                fld2Remove[i] = fld2Remove[i].ToLower();
                fld2Remove[i] = fld2Remove[i].Replace("_", " ");      // il carattere _ diventa " "
            }
            // Parto dal fondo perch� vengono cancellate le colonne !!! 
            for (int i = nColumns; i >= 1; i--)
            {
                var nome = workSheet.Cells[posData, i].Value.ToString();
                if (fldDateTime.Contains(nome.ToLower()))
                {
                    workSheet.Cells[posData, i, posData + nRows, i].Style.Numberformat.Format = "yyyy-MM-dd HH:mm";
                }
                if (fld2Remove.Contains(nome.ToLower()))
                {
                    workSheet.DeleteColumn(i);
                    numColDeleted++;
                }
            }

            // Filtri
            posData = 0;
            if (dati.filters != null)
            {
                // Inserisco tante righe vuote quanti sono i filtri da visualizzare + 1 riga vuota 
                workSheet.InsertRow(1, dati.filters.Count + 1);
                foreach (var f in dati.filters)
                {
                    posData++;
                    workSheet.Cells[posData, 1].Value = string.Format("{0}:", f.Key);
                    workSheet.Cells[posData, 2].Value = f.Value;
                }
                using (var range = workSheet.Cells[1, 1, posData, 2])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
            }

            // Instruments of numbers
            posData++;
            workSheet.InsertRow(posData, 1);
            workSheet.Cells[posData, 1].Value = "Number of Instruments";
            workSheet.Cells[posData, 2].Value = dati.cntInstruments;
            using (var range = workSheet.Cells[posData, 1, posData, 2])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                range.Style.Font.Color.SetColor(Color.White);
            }

            // aggiungo 1 righe vuote
            posData++;
            workSheet.InsertRow(posData, 1);

            // I dati iniziato 2 righe dopo !!!! 
            posData++;
            posData++;

            //Aggiunta TOTALE, MEDIA e MEDIA per STRUMENTO(attenzione: il numero della colonna � fisso !!!!) 
            if (addTotals && nRows > 0)
            {
                // Metto i TOTALI in testa e sposto i dati in basso 
                //var posTotals = posData + nRows + 2; // lascio una riga vuota per evitare che il filtro li prenda in considerazione
                var posTotals = posData;
                workSheet.InsertRow(posData, 5);
                posData  = posData + 5;

                workSheet.Cells[posTotals, 1].Value = "Total";
                workSheet.Cells[posTotals + 1, 1].Value = "Total Instr.Average";
                workSheet.Cells[posTotals + 2, 1].Value = "Daily Instr.Average";
                for (int i = 2; i <= 7; i++)
                {
                    workSheet.Cells[posTotals - 1, i].Value = workSheet.Cells[posData, i].Value;
                }
                using (var range = workSheet.Cells[posTotals-1, 2, posTotals-1, 7])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Font.Color.SetColor(Color.Black);
                    //range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //range.Style.Fill.BackgroundColor.SetColor(Color.Gray);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                for (int i = 2; i <= 7; i++)
                {
                    workSheet.Cells[posTotals, i].Formula = string.Format("Sum({0})", new ExcelAddress(posData + 1, i, posData + nRows, i).Address);
                    workSheet.Cells[posTotals + 1, i].Formula = string.Format("Average({0})", new ExcelAddress(posData + 1, i, posData + nRows, i).Address);
                    workSheet.Cells[posTotals + 2, i].Formula = string.Format("{0}/{1}", workSheet.Cells[posTotals + 1, i].Address, dati.cntInstruments);
                }
                using (var range = workSheet.Cells[posTotals, 2, posTotals + 2, 7])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Numberformat.Format = "#,##0";
                }
                using (var range = workSheet.Cells[posTotals, 1, posTotals + 2, 1])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Font.Color.SetColor(Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                }
                //using (var range = workSheet.Cells[posTotals + 2, 1, posTotals + 2, 4])
                //{
                //    range.Style.Font.Bold = true;
                //    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                //    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                //    //range.Style.Numberformat.Format = "#,##0.0";
                //}
                // 1 cifra decimale per AVG
                using (var range = workSheet.Cells[posTotals + 1, 2, posTotals + 2, 7])
                {
                    range.Style.Numberformat.Format = "0.0";
                }
            }

            // Filtro automatico !!! 
            nColumns = nColumns - numColDeleted;
            workSheet.Cells[posData, 1, posData + nRows, nColumns].AutoFilter = true;

            // AUTO FIT Parto dal fondo (da destra) perch� vengono cancellate le colonne !!! 
            // SOLO PER I CONTATORI !!! 
            if (opt.Equals("counters"))
            {
                for (int i = 1; i <= nColumns; i++)
                {
                    workSheet.Column(i).AutoFit();
                }

            }
            using (var range = workSheet.Cells[posData, 1, posData, nColumns])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                range.Style.Font.Color.SetColor(Color.White);
            }

            //// AGGIUNTA ULTERIORE SHEET 
            //workSheet = excel.Workbook.Worksheets.Add("PROVA");

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return stream;            //return newFile.FullName;
        }

        public Stream ExportAssaysUtilization(AssayUtilizationModel dati)
        {

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("AssaysUtilization");

            int posData = 1;
            int nRows = 0;
            int nColumns = 18;   // (typeof(T)).GetProperties().Length;

            // CARICAMENTO DATI
            workSheet.Cells[1, 1].LoadFromCollection(dati.AssayUtilizationDetails, true);
            nRows = dati.AssayUtilizationDetails.Count;

            // Aggiunta intestazione (2 righe)
            workSheet.InsertRow(posData, 1);
            int idxCol = 1;
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "N.Test";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "N.Sample";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "N.Calibrator";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "N.Control";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            workSheet.Cells[1, idxCol++].Value = "";
            //workSheet.Cells[1, idxCol++].Value = "";
            // Unione celle per titolo 
            workSheet.Cells[1, 2, 1, 5].Merge = true;
            workSheet.Cells[1, 2, 1, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Cells[1, 6, 1, 9].Merge = true;
            workSheet.Cells[1, 6, 1, 9].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Cells[1, 10, 1, 13].Merge = true;
            workSheet.Cells[1, 10, 1, 13].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Cells[1, 14, 1, 17].Merge = true;
            workSheet.Cells[1, 14, 1, 17].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            idxCol = 1;
            workSheet.Cells[2, idxCol++].Value = "Pathogen";
            workSheet.Cells[2, idxCol++].Value = "Valid";  // run
            workSheet.Cells[2, idxCol++].Value = "Invalid";
            workSheet.Cells[2, idxCol++].Value = "Error";
            workSheet.Cells[2, idxCol++].Value = "Total";
            workSheet.Cells[2, idxCol++].Value = "Valid"; // Sample
            workSheet.Cells[2, idxCol++].Value = "Invalid";
            workSheet.Cells[2, idxCol++].Value = "Error";
            workSheet.Cells[2, idxCol++].Value = "Total";
            workSheet.Cells[2, idxCol++].Value = "Valid"; // Calibrator
            workSheet.Cells[2, idxCol++].Value = "Invalid";
            workSheet.Cells[2, idxCol++].Value = "Error";
            workSheet.Cells[2, idxCol++].Value = "Total";
            workSheet.Cells[2, idxCol++].Value = "Valid"; // Control
            workSheet.Cells[2, idxCol++].Value = "Invalid";
            workSheet.Cells[2, idxCol++].Value = "Error";
            workSheet.Cells[2, idxCol++].Value = "Total";
            workSheet.Cells[2, idxCol++].Value = "Aborted";
            //workSheet.Cells[2, idxCol++].Value = "Error";


            // Impostazione STILE INTESTAZIONE
            using (var range = workSheet.Cells[1, 2, 1, nColumns-1])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                range.Style.Font.Color.SetColor(Color.White);
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            }
            using (var range = workSheet.Cells[2, 1, 2, nColumns])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                range.Style.Font.Color.SetColor(Color.White);
                range.Style.Border.Top.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Left.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
            }

            //// Rimuovo la colonna 'S' (ERROR)
            //workSheet.DeleteColumn(19);
            //nColumns--;


            // Filtri
            posData = 0;
            if (dati.filters != null)
            {
                // Inserisco tante righe vuote quanti sono i filtri da visualizzare + 1 riga vuota 
                workSheet.InsertRow(1, dati.filters.Count + 1);
                foreach (var f in dati.filters)
                {
                    posData++;
                    workSheet.Cells[posData, 1].Value = string.Format("{0}:", f.Key);
                    workSheet.Cells[posData, 2].Value = f.Value;
                }
                using (var range = workSheet.Cells[1, 1, posData, 2])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.Blue);
                    range.Style.Font.Color.SetColor(Color.White);
                }
            }

            // aggiungo una riga vuota
            posData++;
            workSheet.InsertRow(posData, 1);

            // Filtro automatico !!! 
            posData = posData+3;  // Posizionamento 
            workSheet.Cells[posData, 1, posData + nRows, nColumns].AutoFilter = true;

            // Aggiunta TOTALE
            if (nRows > 0)
            {
                var posTotals = posData + nRows + 2; // lascio una riga vuota per evitare che il filtro li prenda inconsiderazione
                workSheet.Cells[posTotals, 1].Value = "Total";
                for (int i = 2; i <= nColumns; i++)
                {
                    workSheet.Cells[posTotals, i].Formula = string.Format("Sum({0})", new ExcelAddress(posData + 1, i, posData + nRows, i).Address);
                }
                using (var range = workSheet.Cells[posTotals, 2, posTotals, nColumns])
                {
                    range.Style.Font.Bold = true;
                    range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.Style.Numberformat.Format = "#,##0";
                }
                // Aggiungo colore ai soli SUBTOTAL: GREEN per VALID, RED per ERROR, ORANGE per INVALID 
                for (int i = 2 ; i <= nColumns - 2; i++)
                {
                    Color newColor = Color.Empty;
                    var testo = workSheet.Cells[posData, i].Value.ToString();
                    if (testo.ToLower().Equals("valid"))
                        newColor = Color.Green;
                    if (testo.ToLower().Equals("invalid"))
                        newColor = Color.DarkOrange;
                    if (testo.ToLower().Equals("error"))
                        newColor = Color.Red;

                    if (newColor != Color.Empty)
                    {
                        using (var range = workSheet.Cells[posData + 1, i, posTotals, i])
                        {
                            range.Style.Font.Color.SetColor(newColor);
                        }
                    }
                }
            }

            // AUTO FIT Parto dal fondo (da destra) perch� vengono cancellate le colonne !!! 
            for (int i = 1; i <= nColumns; i++)
            {
                workSheet.Column(i).AutoFit();
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return stream;            //return newFile.FullName;
        }

        //[HttpGet]
        //[Route("api/[controller]/[action]")]
        //public object GetDailyExecutions(DataSourceLoadOptionsExtraStats loadOptions)
        //{
        //    //ExecutionModel ret = (new ExecutionsBO()).GetDailyExecutions();
        //    //return DataSourceLoader.Load(ret.dailyExecutions, loadOptions);
        //    return BadRequest();
        //}

        //[HttpGet]
        //[Route("api/[controller]/[action]")]
        //public object GetDailyExecutionsHomePage(DataSourceLoadOptionsExtraStats loadOptions)
        //{
        //    // ??? PARAMETRI ??? 
        //    DailyExecutionsDTO dto = new DailyExecutionsDTO()
        //    {
        //        DateFrom = DateTime.Now.AddMonths(-1),
        //        DateTo = DateTime.Now
        //    };
        //    ExecutionModel ret = (new ExecutionsBO()).GetDailyExecutions(dto: dto);
        //    return DataSourceLoader.Load(ret.dailyExecutions, loadOptions);
        //}

    }
}
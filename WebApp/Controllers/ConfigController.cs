﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NPoco;
using System.Data.SqlClient;
using WebApp.Entity;
using WebApp.Classes;
using WebApp.Repository;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Data;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ConfigController : Controller
    {
        private readonly ILogger _logger;
        private static string connectionString;
        private readonly WebAppDbContext _context;
        private readonly IEmailSender _emailSender;

        public ConfigController(ILogger<UsersController> logger, WebAppDbContext context, IEmailSender emailSender)
        {
            _logger = logger;
            _context = context;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }
        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // ************************************************************************
        // Gestione User Job Position (Department)
        // ************************************************************************
        public ActionResult ManageUserJobPositions()
        {
            return View();
        }

        [HttpGet]
        public object GetUserJobPositions(DataSourceLoadOptions loadOptions)
        {
            var data = _context.Departments.OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public ActionResult DeleteUserJobPositions(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                // ?? CONTROLLO SE USATA ??? 
                int id2Del = Int32.Parse(id);
                Department rec2Del = _context.Departments.Where(t => t.DepartmentId == id2Del).FirstOrDefault();
                if (rec2Del != null)
                {
                    if (_context.UserCfg.Where(t => t.DepartmentId != null && t.DepartmentId == rec2Del.DepartmentId).Count() > 0)
                    {
                        ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                    }
                    else
                    {
                        try
                        {
                            _context.Departments.Remove(rec2Del);
                            _context.SaveChanges();
                        }
                        catch (Exception e)
                        {
                            var sqlex = e.InnerException as SqlException;
                            if (sqlex != null && sqlex.Number == 547)
                            {
                                ret = String.Format("Cannot delete '{0}' because it is used.", rec2Del.Description);
                            }
                            else
                            {
                                ret = String.Format("Delete '{0}' - Error: {1} - {2}", rec2Del.Description, e.Message, e.InnerException.ToString());
                            }
                            break;
                        }
                    }
                }
            }
            return Json(ret);
        }

        [HttpPost]
        public ActionResult CreateUserJobPosition(string Description)
        {
            var ret = "";
            string newDesc = Description;
            if (!String.IsNullOrWhiteSpace(newDesc) && newDesc.Length > 100)
            {
                newDesc = newDesc.Substring(0, 100);
            }

            Department newRec = new Department()
            {
                Description = newDesc,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            try
            {
                _context.Departments.Add(newRec);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                ret = String.Format("Create User Job Position - Error: {0} - {1}", e.Message, e.InnerException.ToString());
            }
            return Json(ret);
        }

        [HttpPut]
        public IActionResult UpdateUserJobPosition(int key, string values)
        {
            Department rec2Update = _context.Departments.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione MESSAGE/WARNING
        // ************************************************************************
        public ActionResult ManageWMessages()
        {
            return View();
        }

        [HttpGet]
        public object GetWMessages(DataSourceLoadOptions loadOptions, bool showExpired)
        {
            var data = _context.WMessages.Where(t => !t.Deleted).OrderBy(t => t.TmstFrom).ThenBy(t=>t.TmstTo).ToList();
            if (!showExpired)
            {
                data = data.Where(t => t.TmstTo.Value.Date >= DateTime.Now.Date).ToList();
            }
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPost]
        public IActionResult InsertWMessage(int key, string values)
        {
            WMessage rec2Insert = new WMessage();
            try
            {
                JsonConvert.PopulateObject(values, rec2Insert);
                if (!TryValidateModel(rec2Insert))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                if (string.IsNullOrWhiteSpace(rec2Insert.Note))
                {
                    return BadRequest(string.Format("The Note field is required."));
                }
                if (rec2Insert.Note.Length > 500)
                {
                    return BadRequest(string.Format("The field Title must be a string with a maximum length of 500."));
                }
                if (rec2Insert.TmstFrom  == null || rec2Insert.TmstFrom == DateTime.MinValue)
                {
                    return BadRequest(string.Format("The Start Date field is required."));
                }
                if (rec2Insert.TmstTo == null || rec2Insert.TmstTo == DateTime.MinValue)
                {
                    return BadRequest(string.Format("The Expiration Date field is required."));
                }
                if (rec2Insert.TmstFrom > rec2Insert.TmstTo) { 
                    return BadRequest(string.Format("Expiration Date must be after Start Date."));
                }
                rec2Insert.UserInse = User.Identity.Name;
                rec2Insert.TmstInse= DateTime.Now;
                rec2Insert.UserLastUpd = User.Identity.Name;
                rec2Insert.TmstLastUpd = DateTime.Now;
                _context.WMessages.Add(rec2Insert);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        [HttpPut]
        public IActionResult UpdateWMessage(int key, string values)
        {
            WMessage rec2Update = _context.WMessages.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                if (string.IsNullOrWhiteSpace(rec2Update.Note))
                {
                    return BadRequest(string.Format("The Note field is required."));
                }
                if (rec2Update.Note.Length > 500)
                {
                    return BadRequest(string.Format("The field Title must be a string with a maximum length of 500.")); 
                }
                if (rec2Update.TmstFrom == null || rec2Update.TmstFrom == DateTime.MinValue)
                {
                    return BadRequest(string.Format("The Start Date field is required."));
                }
                if (rec2Update.TmstTo == null || rec2Update.TmstTo == DateTime.MinValue)
                {
                    return BadRequest(string.Format("The Expiration Date field is required."));
                }
                if (rec2Update.TmstFrom > rec2Update.TmstTo)
                {
                    return BadRequest(string.Format("Expiration Date must be after Start Date."));
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.WMessages.Update(rec2Update);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteWMessage(int key)
        {
            var rec2Del = _context.WMessages.Find(key);
            if (rec2Del != null)
            {
                rec2Del.Deleted = true;
                rec2Del.UserLastUpd = User.Identity.Name;
                rec2Del.TmstLastUpd = DateTime.Now;
                _context.WMessages.Update(rec2Del);
                _context.SaveChanges();
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione Versione ISR 
        // ************************************************************************
        public ActionResult ManageVersionISR()
        {
            return View();
        }

        [HttpGet]
        public object GetVersionISRs(DataSourceLoadOptions loadOptions)
        {
            var data = (new UtilBO()).GetStdTables(CodTab: Lookup.StdTab_VERSIONS, bExcludeDeleted: true);
            return DataSourceLoader.Load(data, loadOptions);
        }


        [HttpPut]
        public IActionResult UpdateVersionISR(int key, string values)
        {
            try
            {
                LookUpString newVal = JsonConvert.DeserializeObject<LookUpString>(values);
                var Sql = @"UPDATE stdtables  SET MODELTYPE = @attr1
                                 WHERE ID = @key";
                List<object> args = new List<object>();
                args.Add(new { key = key });
                args.Add(new { attr1 = newVal.attr1});
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        // ************************************************************************
        // Gestione Commercial Status (ISR)
        // ************************************************************************
        public ActionResult ManageCommercialStatusISR()
        {
            try
            {
                var Sql = @"INSERT INTO TabGenConfigs (IdTab, DescTab, EnabledISR, TmstInse, UserInse, TmstLastUpd, UserLastUpd)
                                SELECT DISTINCT @idTab as IdTab, UPPER(COMMERCIAL_STATUS) as DescTab, 0 as enabledIsr, 
                                       getDate() as TmstInse, @idUser as UserInse,  getDate() as TmstLastUpd, @idUser as UserLastUpd
                                  FROM INSTRUMENTS 
                                 WHERE COALESCE(COMMERCIAL_STATUS, '') <> ''
                                 AND UPPER(MODELTYPE) IN(@ModelTypeEliteInstruments)
                                 AND UPPER(COMMERCIAL_STATUS) NOT IN (SELECT UPPER(DescTab) FROM TABGENCONFIGS WHERE IdTab = @idTab)";
                List<object> args = new List<object>();
                args.Add(new { idTab = Lookup.TabGenConfig_CommercialStatusISR });
                // solo strumenti ELITE	
                args.Add(new { ModelTypeEliteInstruments = Lookup.InstrumentModelType_EliteInstruments.Select(el => el.id.ToUpper()).ToList() });
                args.Add(new { idUser = User.Identity.Name });
                using (IDatabase db = Connection)
                {
                    db.Execute(Sql, args.ToArray());
                }
            }
            catch (Exception e)
            {
                TempData["MsgToLayout"] = String.Format("Error in init ManageCommercialStatusISR(): {0} {1}", e.Message, e.Source);
            }
            return View();
        }

        [HttpGet]
        public object GetCommercialStatusISR(DataSourceLoadOptions loadOptions)
        {
            var data = _context.TabGenConfigs.Where(t => t.IdTab.Equals(Lookup.TabGenConfig_CommercialStatusISR)).OrderBy(t => t.DescTab).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpPut]
        public IActionResult UpdateCommercialStatusISR(int key, string values)
        {
            TabGenConfig rec2Update = _context.TabGenConfigs.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

    }
}




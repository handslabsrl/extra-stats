using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Models;
using WebApp.Classes;
using System.IO;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using DevExtreme.AspNet.Data;
using WebApp.Entity;
using WebApp.Repository;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApp.Controllers
{
    [Authorize]
    public class UtilsController : Controller
    {
        private readonly ILogger _logger;
        private readonly WebAppDbContext _context;

        public UtilsController(ILogger<UtilsController> logger, WebAppDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        // ************************************************************************
        // Gestione Functions Documentation
        // ************************************************************************

        public IActionResult FunctionsDocumentation()
        {
            return View();
        }

        public ActionResult ShowFunctionPdf(string functionName)
        {
            FunctionDocumentation item = new FunctionDocumentation() { functionName = functionName };
            string fileNamePdf = item.getFileNamePdf;
            if (!item.existsPdf)
            {
                return NotFound(String.Format("Function Documentation '{0}' not found !!", item.functionName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(item.getFullFileNamePdf);
                //    //FileStream fs = new FileStream(filePathName, FileMode.Open, FileAccess.Read, FileShare.Read);
                //    return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml");
                return new FileContentResult(fs, "application/pdf");
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "Utils.ShowFunctionPdf()", error));
                return BadRequest(error);
            }
        }

        [AllowAnonymous]
        public ActionResult DownloadDoc(string fileName)
        {
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Download\\Docs");
            string fullFileName = System.IO.Path.Combine(relativePath, fileName);

            if (!System.IO.File.Exists(fullFileName))
            {
                return NotFound(String.Format("Document '{0}' not found !!", fileName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(fullFileName);
                return new FileContentResult(fs, "application/pdf");
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadDoc)", error));
                return BadRequest(error);
            }
        }

        [HttpGet]
        public ActionResult GetFunctionNames(DataSourceLoadOptions loadOptions)
        {
            List<FunctionDocumentation> items = new List<FunctionDocumentation>();
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DailyExecution });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_AssayUtilization });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_UserRoles });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DataImport });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DataMining });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_LogFiles }); 
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_UserManual });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_RnDExtractions });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_DocumentalRepository });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Support_YourRequests });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Support_Manage});
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Support_Complaint });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Support_3PRequests });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Support_Statistics });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Warrenty });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Warrenty_Manage });
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Instruments_Booking});
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Maintenance }); 
            items.Add(new FunctionDocumentation() { functionName = Lookup.Function_Documentation_Refurbish }); 
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(items.OrderBy(t=>t.functionName), loadOptions)), "application/json");
        }

        [HttpPost]
        public ActionResult UploadPdf(string functionName)
        {
            try
            {
                FunctionDocumentation item = new FunctionDocumentation() { functionName = functionName };
                if (item == null)
                {
                    Response.StatusCode = 500;
                }
                var myFile = Request.Form.Files["myFile"];
                var fullFileName = item.getFullFileNamePdf;
                if (!Directory.Exists(Path.GetDirectoryName(fullFileName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fullFileName));
                }
                using (var fileStream = System.IO.File.Create(fullFileName))
                {
                    myFile.CopyTo(fileStream);
                }
            }
            catch
            {
                Response.StatusCode = 400;
            }

            return new EmptyResult();
        }

        // POST: Utils/deleteFilePdf/5
        public JsonResult deleteFilePdf(string id)
        {
            string ret = "";
            FunctionDocumentation item = new FunctionDocumentation() { functionName = id };
            string fileNamePdf = item.getFileNamePdf;
            if (!item.existsPdf)
            {
                ret = String.Format("File '{0}' not found !!", fileNamePdf);
            } else
            {
                try
                {
                    System.IO.File.Delete(item.getFullFileNamePdf);
                }
                catch (Exception e)
                {
                    ret = String.Format("[deleteFilePdf] - {0} / {1}", e.Message, e.Source);
                }
            }
            return Json(ret);
        }

        // ************************************************************************
        // Gestione Distribution List
        // ************************************************************************
        [HttpGet]
        public object GetDistributionLists(DataSourceLoadOptions loadOptions, string DLType = "")
        {
            var data = _context.DLLists.Where(t => !t.Deleted && (string.IsNullOrWhiteSpace(DLType) || t.DLType.Equals(DLType))).OrderBy(t => t.Description).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        public ActionResult ManageDistributionLists(string groupId)
        {
            DLIndexDTO dto = new DLIndexDTO() { };
            dto.GroupId = groupId;
            if (string.IsNullOrWhiteSpace(dto.GroupId))
            {
                dto.GroupId = HttpContext.Session.GetString(Lookup.glb_sessionkey_ManageDL_GroupId);
                if (string.IsNullOrWhiteSpace(dto.GroupId))
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            HttpContext.Session.SetString(Lookup.glb_sessionkey_ManageDL_GroupId, dto.GroupId);

            dto.Title = "Manage Distribution Lists";
            dto.ExternalMembers = true;
            if (dto.GroupId.Equals(Lookup.Code_DL_Group_ListType)) {
                dto.Title = "Manage Groups";
                dto.ExternalMembers = false;
            }
            dto.tipiDL = _context.Codes.Where(t => t.GroupId.Equals(dto.GroupId)).OrderBy(t => t.Sequence).ToList();
            HashSet<string> elencoTipi = dto.tipiDL.Select(t => t.CodeId).ToHashSet();
            List<DLList> elenco = _context.DLLists.Where(t => t.Deleted == false && elencoTipi.Contains(t.DLType)).ToList();
            
            // Lettura RUOLI (lista)
            dto.liste = new List<DLListDTO>();
            foreach (var item in elenco)
            {
                DLListDTO newRec = new DLListDTO();
                PropertyCopier<DLList, DLListDTO>.Copy(item, newRec); // Copia delle proprietÓ con stesso nome

                List<DLMember> membri = _context.DLMembers.Where(x => x.DLListId == item.DLListId).ToList();
                List<string> intUsers = new List<string>();
                List<string> extUsers = new List<string>();
                foreach (var membro in membri)
                {
                    if (!string.IsNullOrWhiteSpace(membro.UserId))
                    {
                        User user = (new UsersBO()).Get(id: membro.UserId, ExcludeLocked: true).FirstOrDefault();
                        if (user != null)
                        {
                            intUsers.Add(string.Format("{0} &lt;{1}&gt;", user.fullName, user.userEmail));
                        }

                    } else
                    {
                        extUsers.Add(string.Format("{0} &lt;{1}&gt;", membro.ExtName, membro.ExtEmail));
                    }
                }
                newRec.IntUsers = String.Join(",", (intUsers.OrderBy(q => q).ToList()));
                newRec.ExtUsers = String.Join(",", (extUsers.OrderBy(q => q).ToList()));

                newRec.DLTypeDesc = (new UtilBO()).GetCodeDescription(newRec.DLType);

                dto.liste.Add(newRec);
            }
            return View(dto);
        }

        public IActionResult CreateDistributionList(string Description, string DLType)
        {
            DLList newDL = new DLList()
            {
                Description = Description.Replace("\r", string.Empty).Replace("\n", string.Empty),
                DLType = DLType,
                Deleted = false,
                UserLastUpd = User.Identity.Name,
                TmstLastUpd = DateTime.Now
            };
            _context.DLLists.Add(newDL);
            _context.SaveChanges();
            return RedirectToAction("EditDistributionList", new { id = newDL.DLListId });
        }
        [HttpPost]
        public ActionResult DeleteDistributionList(string chiavi)
        {
            var ret = "";
            var items = chiavi.Split('|');
            foreach (string id in items)
            {
                DLList rec2Del = _context.DLLists.Where(t => t.DLListId.ToString() == id).FirstOrDefault();
                if (rec2Del != null)
                {
                    try
                    {
                        rec2Del.Deleted = true;
                        rec2Del.UserLastUpd = User.Identity.Name;
                        rec2Del.TmstLastUpd = DateTime.Now;
                        _context.DLLists.Update(rec2Del);
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        ret = String.Format("DeleteDistributionList '{0}' - Error: {1} - {2}", rec2Del.DLListId, e.Message, e.InnerException.ToString());
                        break;
                    }
                }
            }
            return Json(ret);
        }
        // GET: Utils/EditDistributionList/5
        public async Task<IActionResult> EditDistributionList(int id)
        {
            var dl = _context.DLLists.Find(id);
            if (dl == null)
            {
                return NotFound();
            }
            dl.DLTypeDesc = (new UtilBO()).GetCodeDescription(dl.DLType);

            
            string groupIp = _context.Codes.Find(dl.DLType).GroupId;
            dl.ExternalMembers = groupIp.Equals(Lookup.Code_DL_Group_ListType) ? false : true;

            return View(dl);
        }

        //// POST: Utils/EditDistributionList/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditDistributionListPost([Bind("DLListId, Description")] DLList dto)
        {
            DLList rec2Update = _context.DLLists.Find(dto.DLListId);
            if (rec2Update == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    rec2Update.Description = dto.Description;
                    //rec2Update.DLType = dto.DLType;
                    rec2Update.UserLastUpd = User.Identity.Name;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    _context.DLLists.Update(rec2Update);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("EditDistributionList - Error {0} {1}", e.Message, e.Source));
                    throw;
                }
                return RedirectToAction("ManageDistributionLists");
            }
            return View(dto);
        }
        //[HttpPut]
        //public IActionResult DLListUpdateMember(string key, string values)
        //{
        //    //TrainingJobPosition rec2Update = _context.TrainingJobPositions.Find(key);
        //    //try
        //    //{
        //    //    JsonConvert.PopulateObject(values, rec2Update);
        //    //    if (!TryValidateModel(rec2Update))
        //    //    {
        //    //        string messages = string.Join(" ", ModelState.Values
        //    //                            .SelectMany(x => x.Errors)
        //    //                            .Select(x => x.ErrorMessage));
        //    //        return BadRequest(messages);
        //    //    }

        //    //    if (!key.Equals(rec2Update.Id))
        //    //    {
        //    //        if (!String.IsNullOrWhiteSpace(rec2Update.Id) && rec2Update.Id.Length > 100)
        //    //        {
        //    //            rec2Update.Id = rec2Update.Id.Substring(0, 100);
        //    //        }
        //    //        if (String.IsNullOrWhiteSpace(rec2Update.Id))
        //    //        {
        //    //            string messages = String.Format("The Job Position field is required");
        //    //            return BadRequest(messages);
        //    //        }

        //    //        if (JobPositionExists(rec2Update.Id))
        //    //        {
        //    //            string messages = String.Format("Job Position already exists");
        //    //            return BadRequest(messages);
        //    //        }

        //    //        // Modifica Chiave JOBPosition
        //    //        List<object> args = new List<object>();
        //    //        var Sql = @"UPDATE TrainingJobPositions 
        //    //                       SET ID = @newID,
        //    //                           TmstLastUpd = CONVERT(datetime, @TmstLastUpd, 103),
        //    //                           UserLastUpd = @UserLastUpd
        //    //                     WHERE ID = @oldID";
        //    //        var SqlRegistration = @"UPDATE TrainingRegistrations 
        //    //                                   SET JobPosition = @newID
        //    //                                 WHERE JobPosition = @oldID";
        //    //        args.Add(new { newID = rec2Update.Id });
        //    //        args.Add(new { oldID = key });
        //    //        args.Add(new { TmstLastUpd = DateTime.Now });
        //    //        args.Add(new { UserLastUpd = User.Identity.Name });

        //    //        try
        //    //        {
        //    //            using (IDatabase db = Connection)
        //    //            {
        //    //                db.Execute(Sql, args.ToArray());
        //    //                db.Execute(SqlRegistration, args.ToArray());
        //    //            }
        //    //        }
        //    //        catch (Exception e)
        //    //        {
        //    //            return BadRequest(e.Message);
        //    //        }
        //    //    }
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    return BadRequest(e.Message);
        //    //}
        //    return Ok();
        //}
        //[HttpPut]
        //public IActionResult DDLListInsertMember(string key, string values)
        //{
        //    // ???? 
        //    return Ok();
        //}

        //[HttpPut]
        //public IActionResult DLListDeleteMember(string key)
        //{
        //    // ???? 
        //    return Ok();
        //}
        [HttpPost]
        public IActionResult DLListInsertMember(int key, string values)
        {
            DLMember rec2Insert = new DLMember();
            try
            {
                JsonConvert.PopulateObject(values, rec2Insert);
                if (!TryValidateModel(rec2Insert))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                bool bOk = false;
                // combinazioni valide: solo USERID, solo EMAIL oppure EMAIL + NAME 
                if (!string.IsNullOrEmpty(rec2Insert.UserId) && string.IsNullOrEmpty(rec2Insert.ExtEmail) && string.IsNullOrEmpty(rec2Insert.ExtName))
                {
                    bOk = true;
                }
                if (!string.IsNullOrEmpty(rec2Insert.ExtEmail) && string.IsNullOrEmpty(rec2Insert.UserId))
                {
                    if (!Utility.isEmailValid(rec2Insert.ExtEmail))
                    {
                        return BadRequest(string.Format("The address {0} is not valid.", rec2Insert.ExtEmail));
                    }
                    bOk = true;
                }
                if (!bOk)
                {
                    return BadRequest("Use Internal or External Information");
                }
                rec2Insert.UserLastUpd = User.Identity.Name;
                rec2Insert.TmstLastUpd = DateTime.Now;
                _context.DLMembers.Add(rec2Insert);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpDelete]
        public IActionResult DLListDeleteMember(int key)
        {
            var rec2Del = _context.DLMembers.Find(key);
            if (rec2Del != null )
            {
                _context.DLMembers.Remove(rec2Del);
                _context.SaveChanges();
            }
            return Ok();
        }

        [HttpGet]
        public object GetMembers(DataSourceLoadOptions loadOptions, int DLListId)
        {
            var data = _context.DLMembers.Where(t=>t.DLListId == DLListId).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetUsers(DataSourceLoadOptions loadOptions)
        {
            var data = (new UsersBO()).Get(ExcludeLocked: true);
            return DataSourceLoader.Load(data, loadOptions);
        }


        [HttpGet]
        public object GetCodes(DataSourceLoadOptions loadOptions, string GroupId)
        {
            var data = _context.Codes.Where(t => string.IsNullOrWhiteSpace(GroupId) || t.GroupId.Equals(GroupId)).OrderBy(t => t.Sequence).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        // GET:Utils/SetLastActivityJSON
        [HttpGet]
        public IActionResult SetLastActivityJSON()
        {
            try
            {
                (new UsersBO()).SetLastActivity(User.Identity.Name);
            }
            catch (Exception e)
            {
                // ??? 
            }
            return Json("");
        }

        [HttpGet]
        public object GetStdTabItems(DataSourceLoadOptions loadOptions, string CodTab)
        {
            var data = (new UtilBO()).GetStdTables(CodTab: CodTab);
            return DataSourceLoader.Load(data, loadOptions);
        }
    }
}


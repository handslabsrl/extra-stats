﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using WebApp.Classes;
using WebApp.Controllers;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Models;
using WebApp.Repository;
using WebApp.Services;
using NPoco;
using System.Data.SqlClient;
//using Microsoft.AspNetCore.Mvc.NewtonsoftJson;

namespace ELITeBoard.Controllers
{
    [Authorize(Roles = "Calendar Administrator,Admin,ELITe Instruments,Refurbish Administrator,Refurbish Center,Refurbish Finance,Refurbish ReadOnly")]
    public class InstrumentsController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private static string connectionString;

       public InstrumentsController(WebAppDbContext context, ILogger<TRegistryController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            //_localizer = localizer;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }

        }
        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }
        public IActionResult Index()
        {
            return View("index");
        }
        public IActionResult Maintenance()
        {
            MaintenanceDTO dto = new MaintenanceDTO();
            return View(dto);
        }
        [HttpGet]
        public object GetInstruments(DataSourceLoadOptions loadOptions, bool GetAllModelTypes = false)
        {
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(ExcludeDeleted: true, ExcludeDismiss: true, GetAllModelTypes: GetAllModelTypes);
            return DataSourceLoader.Load(instruments, loadOptions);
        }
        [HttpGet]
        public object GetOwnerGroups(DataSourceLoadOptions loadOptions)
        {
            var tipiDL = _context.Codes.Where(t => t.GroupId.Equals(Lookup.Code_DL_Group_ListType)).OrderBy(t => t.Sequence).ToList();
            HashSet<string> elencoTipi = tipiDL.Select(t => t.CodeId).ToHashSet();
            List<DLList> dls = _context.DLLists.Where(t => t.Deleted == false && elencoTipi.Contains(t.DLType)).ToList();
            return DataSourceLoader.Load(dls, loadOptions);
        }        
        [HttpPut]
        public IActionResult UpdateInstrument(string key, string values)
        {
            Instrument rec2Update = _context.Instruments.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        // GET: Instruments\Booking
        public IActionResult Booking(bookingIndexDTO dto) 
        {
            if (dto.DateFromExport != null && dto.DateToExport != null)
            {
                return ExportTimeLine((DateTime)dto.DateFromExport, (DateTime)dto.DateToExport, dto.SerialNumberToExport);
            }
                 
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(ExcludeDeleted: true, ExcludeDismiss: true);
            dto.instruments = instruments.Where(t => t.Booking == true).ToList();
            dto.UserNameFull = (new UsersBO()).GetFullName(User.Identity.Name);
            return View(dto);
        }

        public ActionResult ExportTimeLine(DateTime DateFromExport, DateTime DateToExport, string SerialNumberToExport)
        {

            List<BookingDTO> data = (new InstrumentsBO()).GetBookings(DateFromExport, DateToExport, userName: User.Identity.Name, SerialNumberList: SerialNumberToExport);
            if (data.Count() == 0)
            {
                return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(string.Format("No Data Found"))), "application/octet-stream") { FileDownloadName = "TimeLine_Error.txt" };
            }

            List<BookingExportDTO> bookings = new List<BookingExportDTO>();
            foreach (var item in data)
            {
                BookingExportDTO newRec = new BookingExportDTO()
                {
                    SerialNumber = item.SerialNumber,
                    Note = item.Note,
                    Title = item.Title,
                    UserInse = item.UserInse,
                    TmstFrom = item.TmstFrom.AddHours(1),   // Sul DB è il dato è salvato con riferimento a UTC 
                    TmstTo = item.TmstTo.AddHours(1)        // Sul DB è il dato è salvato con riferimento a UTC 
                };
                bookings.Add(newRec);
            }
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(ExcludeDeleted: true, ExcludeDismiss: true);
            foreach(var item in instruments)
            {
                foreach(var booking in bookings) {
                    if (booking.SerialNumber.Equals(item.SerialNumber))
                    {
                        booking.Room = item.SiteDescription;
                        booking.InternalCode = item.Machine_Description;
                        booking.Locked = (bool)item.Locked ? "Locked" : "";
                    }
                }
            }
            bookings = bookings.Where(t => t.Room != null).OrderBy(t => t.Room).ThenBy(t=>t.SerialNumber).ToList();

            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("TimeLine");
            workSheet.Cells[1, 1].LoadFromCollection(bookings, true);

            // Intestazione 
            using (var range = workSheet.Cells[1, 1, 1, 9])
            {
                range.Style.Font.Bold = true;
                range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
            }
            workSheet.Cells[1, 1].Value = "Room/Site";
            workSheet.Cells[1, 2].Value = "Serial Number";
            workSheet.Cells[1, 3].Value = "Internal Code";
            workSheet.Cells[1, 4].Value = "Locked";
            workSheet.Cells[1, 5].Value = "Title";
            workSheet.Cells[1, 6].Value = "Note";
            workSheet.Cells[1, 7].Value = "From";
            workSheet.Cells[1, 8].Value = "To";
            workSheet.Cells[1, 9].Value = "Inserted by";

            // Formattazione Colonne DATA/ORA
            using (var range = workSheet.Cells[2, 7, bookings.Count() + 1, 8])
            {
                range.Style.Numberformat.Format = "dd-MM-yyyy HH:mm";
            }

            // AUTO FIT 
            for (int i = 1; i <= 9; i++)
            {
                if (i==5 || i == 6)
                {
                    workSheet.Column(i).Style.WrapText = true;
                    workSheet.Column(i).Width= 80;
                } else
                {
                    workSheet.Column(i).AutoFit();
                }
            }

            var stream = new MemoryStream();
            excel.SaveAs(stream);
            stream.Position = 0;

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", "TimeLine.xlsx");
        }

        [HttpGet]
        public ActionResult GetInstrumentsBooking(DataSourceLoadOptions loadOptions)
        {
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(ExcludeDeleted: true, ExcludeDismiss: true);
            instruments = instruments.Where(t => t.Booking == true).ToList();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(instruments, loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetInstruments4Maintenance(DataSourceLoadOptions loadOptions)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(UserIdCfg: UserIdCfg,
                                                                                     ExcludeDeleted: true,
                                                                                     ExcludeDismiss: true,
                                                                                     MaintenanceInfo: true);
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(instruments, loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetMaintenance(DataSourceLoadOptions loadOptions, string serialNumber, string maintenanceType, bool showAllRecords)
        {
            string UserName = "";
            if (!showAllRecords)
            {
                UserName = User.Identity.Name;
            }
            List<MaintenanceModel> maintenance = (new InstrumentsBO()).GetMaintenance(SerialNumber: serialNumber, MaintenanceType: maintenanceType, UserNameInse: UserName);
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(maintenance, loadOptions)), "application/json");
        }

        [HttpGet]
        public IActionResult getBookingsJSON(DateTime From, DateTime To)
        {
            List<BookingDTO> data = (new InstrumentsBO()).GetBookings(From, To, userName: User.Identity.Name);
            return Content(JsonConvert.SerializeObject(data), "application/json");
        }


        [HttpGet]
        public IActionResult DeleteBooking(int bookingId, string signLastUpd)
        {
            string msg = "";
            bool bSendEmail = true;
            try
            {
                Booking rec2del = _context.Bookings.Find(bookingId);
                if (rec2del != null)
                {
                    if (!signLastUpd.Equals(rec2del.SignLastUpd))
                    {
                        return Json("Operation not allowed because the booking was been changed!");
                    }
                    // Notifica Cancellazione, solo se non faccio parte del gruppo degli approvatori
                    InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(rec2del.SerialNumber);
                    List<DLMembersDTO> users = new List<DLMembersDTO>();
                    if (instrument.OwnerGroupId != null)
                    {
                        users = (new UtilBO()).GetDLUsers((int)instrument.OwnerGroupId);
                        foreach (var user in users)
                        {
                            if (user.UserName.Equals(User.Identity.Name))
                            {
                                bSendEmail = false;
                            }
                        }
                        if (bSendEmail)
                        {
                            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationBooking(bookingId,
                                                                    NotificationType: Lookup.Request_NotificationType.BookingDeleteReservation,
                                                                    userName: User.Identity.Name);
                        }
                    }
                    rec2del.Status = Lookup.Booking_Status_DELETED;
                    rec2del.UserLastUpd = User.Identity.Name;
                    rec2del.TmstLastUpd = DateTime.Now;
                    _context.Bookings.Update(rec2del);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteBooking", e.Message, e.Source);
            }
            return Json(msg);
        }

        [HttpPost]
        public IActionResult ManageBookingJSON([FromBody]BookingDTO dto)
        {
            dto.manageError = "";

            dto.TmstFrom = dto.TmstFrom.Date +
                            new TimeSpan(dto.TmstFrom.TimeOfDay.Hours, (dto.TmstFrom.TimeOfDay.Minutes < 30 ? 0 : dto.TmstFrom.TimeOfDay.Minutes), 0);
            dto.TmstTo = dto.TmstTo.Date +
                            new TimeSpan(dto.TmstTo.TimeOfDay.Hours, (dto.TmstTo.TimeOfDay.Minutes < 30 ? 0 : dto.TmstTo.TimeOfDay.Minutes), 0);

            // CONTROLLO SOVRAPPOSIZIONE 
            // per lo stesso strumento, non deve esistere un'altra prenotazione nello stesso periodo (escluso se stesso)
            int nCnt = (new InstrumentsBO()).GetBookings(From: dto.TmstFrom, To: dto.TmstTo, SerialNumber: dto.SerialNumber,
                                                         BookingIdExclude: dto.BookingId, checkOverlap: true).Count();
            if (nCnt > 0)
            {
                dto.manageError = "There are other reservations for the same period.";
            }
            // STATO PRENOTAZIOnE 
            bool bSendNotifica = true;
            bool bNewBooking = false;
            string status = Lookup.Booking_Status_WAITING;

            // Verifica OWNER 
            InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(dto.SerialNumber);
            if (instrument == null)
            {
                dto.manageError = "Instrument not found.";
            }
            if (string.IsNullOrWhiteSpace(dto.manageError)) {
                if (instrument != null && instrument.OwnerGroupId != null)
                {
                    List<DLMembersDTO> users = (new UtilBO()).GetDLUsers((int)instrument.OwnerGroupId);
                    foreach (var user in users)
                    {
                        if (user.UserName.Equals(User.Identity.Name))
                        {
                            status = Lookup.Booking_Status_CONFIRMED;
                        }
                    }
                }
                if ((bool)instrument.Locked)
                {
                    dto.manageError = "Instrument Locked";
                }
            }
            if (string.IsNullOrWhiteSpace(dto.manageError))
            {
                try
                {
                    if (dto.BookingId == 0)
                    {
                        bNewBooking = true;
                        Booking rec2insert = new Booking()
                        {
                            SerialNumber = dto.SerialNumber,
                            Title = dto.Title, 
                            TmstFrom = dto.TmstFrom,
                            TmstTo = dto.TmstTo,
                            EntireDay = dto.EntireDay,
                            Note = dto.Note,
                            Status = status,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name,
                            TmstLastUpd = DateTime.Now,
                            UserLastUpd = User.Identity.Name
                        };
                        if (status.Equals(Lookup.Booking_Status_CONFIRMED))
                        {
                            rec2insert.TmstApproval = DateTime.Now;
                            rec2insert.UserApproval = User.Identity.Name;
                            bSendNotifica = false;
                        }
                        _context.Bookings.Add(rec2insert);
                        _context.SaveChanges();

                        dto.BookingId = rec2insert.BookingId;

                    }
                    else
                    {
                        // EDIT 
                        Booking ret2Update = _context.Bookings.Find(dto.BookingId);
                        if (ret2Update == null)
                        {
                            dto.manageError = string.Format("Booking {0} not found", dto.BookingId);
                        }
                        else
                        {
                            if (!ret2Update.SignLastUpd.Equals(dto.SignLastUpdCurrent)) {
                                dto.manageError = "Operation not allowed because the booking was been changed!";
                            }
                            else
                            {
                                ret2Update.Status = status;
                                ret2Update.SerialNumber = dto.SerialNumber;
                                ret2Update.Title = dto.Title;
                                ret2Update.TmstFrom = dto.TmstFrom;
                                ret2Update.TmstTo = dto.TmstTo;
                                ret2Update.EntireDay = dto.EntireDay;
                                ret2Update.Note = dto.Note;
                                ret2Update.TmstLastUpd = DateTime.Now;
                                ret2Update.UserLastUpd = User.Identity.Name;
                                ret2Update.TmstApproval = null;
                                ret2Update.UserApproval = null;
                                if (status.Equals(Lookup.Booking_Status_CONFIRMED))
                                {
                                    ret2Update.TmstApproval = DateTime.Now;
                                    ret2Update.UserApproval = User.Identity.Name;
                                    bSendNotifica = false;
                                }
                                _context.Bookings.Update(ret2Update);
                                _context.SaveChanges();
                            }
                        }
                    }
                    if (string.IsNullOrWhiteSpace(dto.manageError))
                    {
                        // Aggiorna ITEM restituito
                        dto = (new InstrumentsBO()).GetBookings(userName: User.Identity.Name, BookingId: dto.BookingId).FirstOrDefault();
                        if (bSendNotifica) 
                        {
                            Lookup.Request_NotificationType notificationType = Lookup.Request_NotificationType.BookingModifyReservation;
                            if (bNewBooking) notificationType = Lookup.Request_NotificationType.BookingNewReservation;
                            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationBooking(dto.BookingId,
                                                                 NotificationType: notificationType,
                                                                 userName: User.Identity.Name);
                        }
                    }
                }
                catch (Exception e)
                {
                    dto.manageError = String.Format("[{0}] - {1} / {2}", "ManageBookingJSON", e.Message, e.Source);
                }
            }
            return Json(dto);
        }
        [HttpPost]
        public IActionResult MoveBookingJSON([FromBody]BookingDTO dto)
        {
            dto.manageError = "";

            // CONTROLLO SOVRAPPOSIZIONE 
            // per lo stesso strumento, non deve esistere un'altra prenotazione nello stesso periodo (escluso se stesso)
            int nCnt = (new InstrumentsBO()).GetBookings(From: dto.TmstFrom, To: dto.TmstTo, SerialNumber: dto.SerialNumber,
                                                         BookingIdExclude: dto.BookingId, checkOverlap: true).Count();
            if (nCnt > 0)
            {
                dto.manageError = "There are other reservations for the same period.";
            }
            // STATO PRENOTAZIOnE 
            string status = Lookup.Booking_Status_WAITING;

            // Verifica OWNER 
            InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(dto.SerialNumber);
            if (instrument != null && instrument.OwnerGroupId != null)
            {
                List<DLMembersDTO> users = (new UtilBO()).GetDLUsers((int)instrument.OwnerGroupId);
                foreach (var user in users)
                {
                    if (user.UserName.Equals(User.Identity.Name))
                    {
                        status = Lookup.Booking_Status_CONFIRMED;
                    }
                }
            }

            if (string.IsNullOrWhiteSpace(dto.manageError))
            {
                bool bSendNotifica = true;
                try
                {
                    Booking ret2Update = _context.Bookings.Find(dto.BookingId);
                    if (ret2Update == null)
                    {
                        dto.manageError = string.Format("Booking {0} not found", dto.BookingId);
                    }
                    else
                    {
                        if (!ret2Update.SignLastUpd.Equals(dto.SignLastUpdCurrent))
                        {
                            dto.manageError = "Operation not allowed because the booking was been changed!";
                        }
                        else
                        {
                            ret2Update.Status = status;
                            ret2Update.SerialNumber = dto.SerialNumber;
                            ret2Update.TmstFrom = dto.TmstFrom;
                            ret2Update.TmstTo = dto.TmstTo;
                            ret2Update.TmstLastUpd = DateTime.Now;
                            ret2Update.UserLastUpd = User.Identity.Name;
                            ret2Update.TmstApproval = null;
                            ret2Update.UserApproval = null;
                            if (status.Equals(Lookup.Booking_Status_CONFIRMED))
                            {
                                ret2Update.TmstApproval = DateTime.Now;
                                ret2Update.UserApproval = User.Identity.Name;
                                bSendNotifica = false;
                            } 
                            _context.Bookings.Update(ret2Update);
                            _context.SaveChanges();

                            // Aggiorna ITEM restituito
                            dto = (new InstrumentsBO()).GetBookings(userName: User.Identity.Name, BookingId: dto.BookingId).FirstOrDefault();

                            if (bSendNotifica)
                            {
                                (new NotificationBO(_context, _emailSender, _logger)).SendNotificationBooking(dto.BookingId,
                                                                                                     NotificationType: Lookup.Request_NotificationType.BookingModifyReservation,
                                                                                                     userName: User.Identity.Name);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    dto.manageError = String.Format("[{0}] - {1} / {2}", "MoveBookingJSON", e.Message, e.Source);
                }
            }
            return Json(dto);
        }

        // GET:Trainings/GetEmailInfoJSON
        public async Task<IActionResult> GetEmailInfoJSON(long TrainingRegistrationId = 0, string EmailTemplateId = "", long TrainingId = 0)
        {
            EmailTemplate data = (new NotificationBO(_context, _emailSender, _logger)).GetEmailInfoGeneric(templateId: EmailTemplateId, userName: User.Identity.Name);
            return Json(data);
        }
        [HttpPost]
        public IActionResult SendEmailToOwnerGroup([FromBody]EmailTemplate DTO)
        {
            string emailOwnerGroup = "";
            try
            {
                InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(DTO.IdRifCode);
                if (instrument != null)
                {
                    if (instrument.OwnerGroupId != null && instrument.OwnerGroupId > 0)
                    {
                        emailOwnerGroup = (new UtilBO()).GetDLEmails(DLListId: (int)instrument.OwnerGroupId);
                    }
                }
                if (!string.IsNullOrWhiteSpace(emailOwnerGroup))
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotification(label: "SendEmailToOwnerGroup", 
                                                                                        to: emailOwnerGroup, subject: DTO.Subject,
                                                                                        body: DTO.Body, context: "BOOKING");
                }
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("[SendEmailToOwnerGroup] {0} - Exception Message: {1}", e.Message, e.Source));
            }
            return Json("");
        }
        [HttpPost]
        public IActionResult UpdateCalibrationJSON([FromBody]UpdateCalibrationDTO dto)
        {
            dto.MsgError = "";

            InstrumentModel instrument = (new InstrumentsBO()).GetInstruments(SerialNumber: dto.SerialNumber, MaintenanceInfo: true).FirstOrDefault();
            int GGNext = (instrument.Calibration_GGNext ?? Lookup.GetInstrumentCalibrationGGNext());

            List<object> args = new List<object>();
            args.Add(new { serialNumber = dto.SerialNumber });
            args.Add(new { userInse = User.Identity.Name });
            args.Add(new { tmstInse = DateTime.Now });
            args.Add(new { maintenanceType = Lookup.Instrument_MaintenanceType_Calibration });
            args.Add(new { tmstLast = dto.NewDate.Date });
            args.Add(new { tmstNext = dto.NewDate.AddDays(GGNext).Date }); 

            string sqlIns = @"INSERT INTO MAINTENANCE 
                                          (SerialNumber, TmstInse, UserInse, LastRec, MaintenanceType, TmstLastCalibration, TmstNextCalibration)
                                 VALUES (@serialNumber, @tmstInse, @userInse, 1, @maintenanceType, @tmstLast, @tmstNext)";
            string sqlUpd = @"UPDATE MAINTENANCE SET LastRec = 0 
                                    WHERE SERIALNUMBER = @serialNumber 
                                      AND LastRec = 1 
                                      AND MaintenanceType = @maintenanceType";
            using (IDatabase db = Connection)
            {
                try
                {
                    db.BeginTransaction();
                    db.Execute(sqlUpd, args.ToArray());
                    db.Execute(sqlIns, args.ToArray());
                    db.CompleteTransaction();
                }
                catch (Exception e)
                {
                    db.AbortTransaction();
                    dto.MsgError = string.Format("UpdateCalibrationJSON() - Error {0} {1}", e.Message, e.Source);
                }
            }
            return Json(dto);
        }
        [HttpPost]
        public IActionResult UpdateFibersJSON([FromBody]UpdateFibersDTO dto)
        {
            dto.MsgError = "";

            InstrumentModel instrument = (new InstrumentsBO()).GetInstruments(SerialNumber: dto.SerialNumber, MaintenanceInfo: true).FirstOrDefault();

            List<object> args = new List<object>();
            args.Add(new { serialNumber = dto.SerialNumber });
            args.Add(new { userInse = User.Identity.Name });
            args.Add(new { tmstInse = DateTime.Now });
            args.Add(new { maintenanceType = Lookup.Instrument_MaintenanceType_Pack });
            args.Add(new { fibersNumber = dto.FibersNumber });
            args.Add(new { fibersNewPack = dto.NewPack });

            string sqlIns = @"INSERT INTO MAINTENANCE 
                                          (SerialNumber, TmstInse, UserInse, LastRec, MaintenanceType, FibersNumber, FibersNewPack)
                                 VALUES (@serialNumber, @tmstInse, @userInse, 1, @maintenanceType, @fibersNumber, @fibersNewPack)";
            string sqlUpd = @"UPDATE MAINTENANCE SET LastRec = 0 
                                    WHERE SERIALNUMBER = @serialNumber 
                                      AND LastRec = 1 
                                      AND MaintenanceType = @maintenanceType";
            using (IDatabase db = Connection)
            {
                try
                {
                    db.BeginTransaction();
                    db.Execute(sqlUpd, args.ToArray());
                    db.Execute(sqlIns, args.ToArray());
                    db.CompleteTransaction();
                }
                catch (Exception e)
                {
                    db.AbortTransaction();
                    dto.MsgError = string.Format("UpdateFibersJSON() - Error {0} {1}", e.Message, e.Source);
                }
            }
            return Json(dto);
        }
        public IActionResult Refurbish(string SerialNumber = "")
        {
            RefurbishDTO dto = new RefurbishDTO();
            dto.SerialNumberFilter = SerialNumber;
            return View(dto);
        }
        [HttpGet]
        public ActionResult GetRefurbishRequests(DataSourceLoadOptions loadOptions, string StatusIn, bool OnlyNeededByExpired)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<RefurbishRequestDTO> requests = (new InstrumentsBO()).GetRefurbishRequests(UserIdCfg: UserIdCfg, StatusIn: StatusIn);
            if (OnlyNeededByExpired)
            {
                requests = requests.Where(t => t.NeededByExpired).ToList();
            }
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(requests, loadOptions)), "application/json");
        }
        [HttpPut]
        public IActionResult UpdateRefurbish(int key, string values)
        {
            RefurbishRequest rec2Update = _context.RefurbishRequests.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;

                if (!rec2Update.Status.Equals(Lookup.Refurbish_Status_CLOSED) && !rec2Update.Status.Equals(Lookup.Refurbish_Status_READY))
                {
                    string newStatus = Lookup.Refurbish_Status_WAITING;
                    if (rec2Update.ProcessingStartDate != null && rec2Update.ProcessingStartDate != DateTime.MinValue)
                    {
                        newStatus = Lookup.Refurbish_Status_PROCESSING;
                    }
                    if (rec2Update.ProcessingEndDate != null && rec2Update.ProcessingEndDate != DateTime.MinValue)
                    {
                        if (!rec2Update.Status.Equals(Lookup.Refurbish_Status_PROCESSING))
                        {
                            return BadRequest(String.Format("Invalid Operation on request {0}", rec2Update.SerialNumber));
                        }
                        if (string.IsNullOrWhiteSpace(rec2Update.SGATWONumber))
                        {
                            return BadRequest(String.Format("Field 'SGAT WO Number' is mandatory to complete request {0}", rec2Update.SerialNumber));
                        }
                        newStatus = Lookup.Refurbish_Status_COMPLETE;
                    }
                    if (rec2Update.Status != newStatus)
                    {
                        RefurbishEvent evento = new RefurbishEvent()
                        {
                            Note = string.Format("Change Status from {0} to {1}", rec2Update.Status, newStatus) ,
                            RefurbishRequestId = rec2Update.RefurbishRequestId,
                            Operation = Lookup.Refurbish_Operation_CHANGESTATUS,
                            TmstInse = DateTime.Now,
                            UserInse = User.Identity.Name
                        };
                        _context.RefurbishEvents.Add(evento);
                        rec2Update.Status = newStatus;
                    }
                }
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        [HttpGet]
        public IActionResult DeleteRefurbishRequest(int id)
        {
            string msg = "";
            RefurbishRequest rec2Update = _context.RefurbishRequests.Find(id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.Refurbish_Status_DELETED;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                RefurbishEvent evento = new RefurbishEvent()
                {
                    Note = "Delete Request",
                    RefurbishRequestId = id, 
                    Operation = Lookup.Refurbish_Operation_DELETE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.RefurbishEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteRefurbishRequest", e.Message, e.Source);
            }
            return Json(msg);
        }
        
        [HttpGet]
        public IActionResult ReadyRefurbishRequest(int id)
        {
            string msg = "";
            RefurbishRequest rec2Update = _context.RefurbishRequests.Find(id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.Refurbish_Status_READY;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                RefurbishEvent evento = new RefurbishEvent()
                {
                    Note = "Confirm READY Request",
                    RefurbishRequestId = id,
                    Operation = Lookup.Refurbish_Operation_READY,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.RefurbishEvents.Add(evento);
                _context.SaveChanges();

                (new NotificationBO(_context, _emailSender, _logger)).SendNotificationRefurbish(rec2Update.RefurbishRequestId, NotificationType: Lookup.Request_NotificationType.RefurbishReadyToShipped, userName: User.Identity.Name);

            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "ReadyRefurbishRequest", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public IActionResult RestoreRefurbishRequest(int id)
        {
            string msg = "";

            RefurbishRequest rec2Update = _context.RefurbishRequests.Find(id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                string prevStatus = rec2Update.Status;
                if (prevStatus.Equals(Lookup.Refurbish_Status_CLOSED))
                {
                    rec2Update.Status = Lookup.Refurbish_Status_READY;
                }
                if (prevStatus.Equals(Lookup.Refurbish_Status_READY))
                {
                    rec2Update.Status = Lookup.Refurbish_Status_COMPLETE;
                }
                if (prevStatus.Equals(Lookup.Refurbish_Status_COMPLETE))
                {
                    rec2Update.Status = Lookup.Refurbish_Status_PROCESSING;
                    rec2Update.ProcessingEndDate = null;
                }
                if (prevStatus.Equals(Lookup.Refurbish_Status_PROCESSING))
                {
                    rec2Update.Status = Lookup.Refurbish_Status_WAITING;
                    rec2Update.ProcessingStartDate = null;
                }
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                RefurbishEvent evento = new RefurbishEvent()
                {
                    Note = string.Format("Reopen Request from {0} to {1}", prevStatus, rec2Update.Status),
                    RefurbishRequestId = id,
                    Operation = Lookup.Refurbish_Operation_REOPEN,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.RefurbishEvents.Add(evento);
                _context.SaveChanges();
                
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "RestoreRefurbishRequest", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public IActionResult CloseRefurbishRequest(int id)
        {
            string msg = "";
            RefurbishRequest rec2Update = _context.RefurbishRequests.Find(id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.Refurbish_Status_CLOSED;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                RefurbishEvent evento = new RefurbishEvent()
                {
                    Note = "Close Request",
                    RefurbishRequestId = id,
                    Operation = Lookup.Refurbish_Operation_CLOSE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.RefurbishEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "CloseRefurbishRequest", e.Message, e.Source);
            }
            return Json(msg);
        }


        public JsonResult createRefurbishRequest(string serialNumber)
        {
            string msg = "";
            HashSet<string> StatusNotIn = new HashSet<string>() { Lookup.Refurbish_Status_CLOSED, Lookup.Refurbish_Status_DELETED } ;
            int count = _context.RefurbishRequests.Where(t => t.SerialNumber.Equals(serialNumber) && !StatusNotIn.Contains(t.Status)).Count();
            if (count > 0)
            {
                msg = "There is already a request for this Instrument!";
            }
            if (string.IsNullOrWhiteSpace(msg))
            {
                try
                {
                    RefurbishRequest newRec = new RefurbishRequest()
                    {
                        SerialNumber = serialNumber,
                        Status = Lookup.Refurbish_Status_WAITING,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.RefurbishRequests.Add(newRec);
                    RefurbishEvent evento = new RefurbishEvent()
                    {
                        Note = "Create  Request",
                        RefurbishRequestId = newRec.RefurbishRequestId,
                        Operation = Lookup.Refurbish_Operation_CREATE,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.RefurbishEvents.Add(evento);
                    _context.SaveChanges();

                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationRefurbish(newRec.RefurbishRequestId, Lookup.Request_NotificationType.RefurbishCreate, userName: User.Identity.Name);

                }
                 catch (Exception e)
                {
                    msg = string.Format("createRefurbishRequest - ERROR {0} {1}", e.Message, e.Source);
                }
            }
            return Json(msg); 
        }
        [HttpGet]
        public ActionResult GetInstruments4Refurbish(DataSourceLoadOptions loadOptions)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(UserIdCfg: UserIdCfg,
                                                                                     ExcludeDeleted: true,
                                                                                     ExcludeDismiss: true);
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(instruments, loadOptions)), "application/json");
        }
        // SERVICE CENTER
        public IActionResult ServiceCenters()
        {
            return View();
        }
        [HttpGet]
        public object GetServiceCenters(DataSourceLoadOptions loadOptions)
        {
            List<ServiceCenter> serviceCenters = _context.ServiceCenters.Where(t=>t.Deleted == false).ToList();
            return DataSourceLoader.Load(serviceCenters, loadOptions);
        }
        [HttpPost]
        public IActionResult InsertServiceCenter(int key, string values)
        {
            ServiceCenter rec2Insert = new ServiceCenter();
            try
            {
                JsonConvert.PopulateObject(values, rec2Insert);
                if (!TryValidateModel(rec2Insert))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                if (string.IsNullOrWhiteSpace(rec2Insert.ServiceCenterCode))
                {
                    return BadRequest(string.Format("The Service Center Code field is required."));
                }
                if (!string.IsNullOrWhiteSpace(rec2Insert.Note) && rec2Insert.Note.Length > 500)
                {
                    return BadRequest(string.Format("The field Title must be a string with a maximum length of 500."));
                }
                if (!string.IsNullOrWhiteSpace(rec2Insert.Contacts) && rec2Insert.Contacts.Length > 2000)
                {
                    return BadRequest(string.Format("The field Contacts must be a string with a maximum length of 2000."));
                }
                if (GetServiceCenterByCode(Code: rec2Insert.ServiceCenterCode) != null)
                {
                    return BadRequest(string.Format("Service Center Code {0} already exists.", rec2Insert.ServiceCenterCode));
                }
                rec2Insert.UserInse = User.Identity.Name;
                rec2Insert.TmstInse = DateTime.Now;
                rec2Insert.UserLastUpd = User.Identity.Name;
                rec2Insert.TmstLastUpd = DateTime.Now;
                _context.ServiceCenters.Add(rec2Insert);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        [HttpPut]
        public IActionResult UpdateServiceCenter(int key, string values)
        {
            ServiceCenter rec2Update = _context.ServiceCenters.Find(key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                if (string.IsNullOrWhiteSpace(rec2Update.ServiceCenterCode))
                {
                    return BadRequest(string.Format("The Service Center Code field is required."));
                }
                if (!string.IsNullOrWhiteSpace(rec2Update.Note) && rec2Update.Note.Length > 500)
                {
                    return BadRequest(string.Format("The field Title must be a string with a maximum length of 500."));
                }
                if (!string.IsNullOrWhiteSpace(rec2Update.Contacts) && rec2Update.Contacts.Length > 2000)
                {
                    return BadRequest(string.Format("The field Contacts must be a string with a maximum length of 2000."));
                }
                if (GetServiceCenterByCode(Code: rec2Update.ServiceCenterCode, excludeId: rec2Update.ServiceCenterId) != null)
                {
                    return BadRequest(string.Format("Service Center Code {0} already exists.", rec2Update.ServiceCenterCode));
                }
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.ServiceCenters.Update(rec2Update);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }

        [HttpDelete]
        public IActionResult DeleteServiceCenter(int key)
        {
            ServiceCenter rec2Del = _context.ServiceCenters.Find(key);
            if (rec2Del != null)
            {
                rec2Del.Deleted = true;
                rec2Del.UserLastUpd = User.Identity.Name;
                rec2Del.TmstLastUpd = DateTime.Now;
                _context.ServiceCenters.Update(rec2Del);
                _context.SaveChanges();
            }
            return Ok();
        }

        private ServiceCenter GetServiceCenterByCode(string Code, int excludeId = 0)
        {
            ServiceCenter ret = _context.ServiceCenters.Where(t => t.ServiceCenterCode.ToUpper().Equals(Code.ToUpper()) && !t.Deleted && t.ServiceCenterId != excludeId).FirstOrDefault();
            return ret;
        }
    }
}
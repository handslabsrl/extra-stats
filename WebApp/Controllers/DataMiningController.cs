using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApp.Repository;
using WebApp.Entity;
using WebApp.Data;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using WebApp.Classes;
using System.Data.SqlClient;
using NPoco;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize(Roles = "ELITe Instruments")]
    public class DataMiningController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;

        private readonly string DirExport = "Download\\DataMining";

        public DataMiningController(WebAppDbContext context, ILogger<DataMiningController> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }


        // GET: DataMining/Create
        public IActionResult Create()
        {
            DataMiningCreateDTO dto = new DataMiningCreateDTO();
            dto.dataMining = new DataMining();
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            //if (!string.IsNullOrWhiteSpace(UserId))
            //{
            //    UserCfg userCfg = _context.UserCfg.Find(UserId);
            //    dto.dataMining.Country = userCfg.Country?.ToUpper();
            //    dto.dataMining.CommercialEntity = userCfg.CommercialEntity?.ToUpper();
            //    dto.lockCountry = !string.IsNullOrWhiteSpace(userCfg.Country);
            //    dto.lockCommerialeEntity = !string.IsNullOrWhiteSpace(userCfg.CommercialEntity);
            //}
            //string countriesCfg = "";
            List<CountryModel> countries = (new CountriesBO()).GetCountries(UserId);  
            if (countries.Count() == 0)
            {
                countries = (new CountriesBO()).GetCountries();
            }
            if (countries.Count() == 1)
            {
                dto.countriesLink = new List<string> { countries.Select(t => t.Country).FirstOrDefault() };
            }
            List<CommercialEntityModel> commercialEntities = (new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId);
            if (commercialEntities.Count() == 0)
            {
                commercialEntities = (new InstrumentsBO()).GetCommercialEntities();
            }
            if (commercialEntities.Count() == 1)
            {
                dto.commercialEntitiesLink = new List<string> { commercialEntities.Select(t => t.CommercialEntity).FirstOrDefault() };
            }
            return View(dto);
        }

        // POST: DataMining/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("ExtractionType,DateFrom,DateTo,DateUpToDate,VersionExtra,Country,CommercialEntity")] DataMining dataMining)
        public async Task<IActionResult> Create(DataMiningCreateDTO dto)
        {
            if (ModelState.IsValid)
            {
                dto.dataMining.UserInse = User.Identity.Name;
                dto.dataMining.TmstInse = DateTime.Now;
                dto.dataMining.Status = Lookup.DataMining_Status_NEW;
                if (dto.countriesLink != null)
                {
                    dto.dataMining.Country = string.Join(", ", dto.countriesLink);
                }
                if (dto.commercialEntitiesLink != null)
                {
                    dto.dataMining.CommercialEntity = string.Join(", ", dto.commercialEntitiesLink);
                }
                dto.dataMining.ModelType = dto.dataMining.ModelType;
                _context.Add(dto.dataMining);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            //string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            //if (!string.IsNullOrWhiteSpace(UserId))
            //{
            //    UserCfg userCfg = _context.UserCfg.Find(UserId);
            //    dto.lockCountry = !string.IsNullOrWhiteSpace(userCfg.Country);
            //    dto.lockCommerialeEntity = !string.IsNullOrWhiteSpace(userCfg.CommercialEntity);
            //}
            return View(dto);
        }

        [HttpGet]
        public ActionResult GetCommercialEntity(DataSourceLoadOptions loadOptions, string Country = null)
        {
            //string commercialDefault = "";
            string CountrySel = Country;
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            if (string.IsNullOrWhiteSpace(CountrySel))
            {
                List<CountryModel> countriesCfg = (new CountriesBO()).GetCountries(UserId);
                CountrySel = string.Join (",", countriesCfg.Select(t => t.Country).ToList());
            }
            List<CommercialEntityModel> dati = (new InstrumentsBO()).GetCommercialEntities(Country: CountrySel, UserIdCfg: UserId);
            if (dati.Count == 0)
            {
                // Se non ci sono configurazioni specifiche, prelevo TUTTE le Commercial Entity censite 
                if (((new InstrumentsBO()).GetCommercialEntities(UserIdCfg: UserId)).Count == 0)
                {
                    dati = (new InstrumentsBO()).GetCommercialEntities(Country: CountrySel);
                }
            }
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(dati, loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetCommercialStatus(DataSourceLoadOptions loadOptions)
        {
            List<CommStatusModel> dati = (new CommercialStatusBO()).Get();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(dati, loadOptions)), "application/json");
        }

        [HttpGet]
        public ActionResult GetCountry(DataSourceLoadOptions loadOptions)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
            List<CountryModel> dati = (new CountriesBO()).GetCountries(UserId);
            if (dati.Count() == 0)
            {
                dati = (new CountriesBO()).GetCountries();
            }
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(dati, loadOptions)), "application/json");
        }

        // GET: LogFIles/Delete
        public IActionResult Delete(int DataMiningId)
        {
            DataMining rec2Delete = _context.DataMining.Find(DataMiningId);
            if (rec2Delete != null)
            {
                _context.DataMining.Remove(rec2Delete);
                _context.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        public ActionResult DownloadFile(int Id)
        {
            DataMining data = _context.DataMining.Find(Id);
            if (data == null)
            {
                return NotFound(string.Format("DataMining {0} not found", Id));
            }
            if (string.IsNullOrWhiteSpace(data.FileName))
            {
                return BadRequest("File Name Missing");
            }
            string relativePath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), DirExport);
            string fullPathName = System.IO.Path.Combine(relativePath, data.TmstInse.ToString("yyyyMM"));
            string filePathName = System.IO.Path.Combine(fullPathName, data.FileName);
            _logger.LogInformation(string.Format("Download - Path: >{0}<", filePathName));
            if (!System.IO.File.Exists(filePathName))
            {
                return BadRequest(String.Format("File {0} not found!!", filePathName));
            }
            try
            {
                var fs = System.IO.File.ReadAllBytes(filePathName);
                //FileStream fs = new FileStream(filePathName, FileMode.Open, FileAccess.Read, FileShare.Read);
                return File(fs, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml", data.FileName);
            }
            catch (Exception e)
            {
                string error = String.Format("{0} / {1}", e.Message, e.Source);
                _logger.LogError(String.Format("{0} -  {1}", "DownloadFile.Get()" , error));
                return BadRequest(error);
            }
        }

        ///// <summary>
        ///// File.ReadAllBytes alternative to avoid read and/or write locking
        ///// </summary>
        //private byte[] ReadAllBytes2(string filePath, FileAccess = FileAccess.Read, FileShare shareMode = FileShare.ReadWrite)
        //{
        //    using (var fs = new FileStream(filePath, FileMode.Open, fileAccess, shareMode))
        //    {
        //        using (var ms = new MemoryStream())
        //        {
        //            fs.CopyTo(ms);
        //            return ms.ToArray();
        //        }
        //    }
        //}

        [HttpGet]
        [Route("api/[controller]/[action]")]
        public object Get(DataSourceLoadOptions loadOptions)
        {
            var data = _context.DataMining.Where(t => t.UserInse.Equals(User.Identity.Name)).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }
    }
}

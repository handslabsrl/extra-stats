using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebApp.Repository;
using WebApp.Models;
using WebApp.Classes;
using Microsoft.AspNetCore.Http;

namespace WebApp.Controllers
{
    public class CustomersController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult getCities(DataSourceLoadOptions loadOptions, string Country = null)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<CityModel> cities = (new CitiesBO()).GetCities(Country: Country, UserIdCfg: UserIdCfg);
            //loadOptions.Take = 999;
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(cities, loadOptions)), "application/json");
        }
        [HttpGet]
        public ActionResult getCustomers(DataSourceLoadOptions loadOptions, string Country = null, string City = null)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<CustomerModel> clienti = (new CustomersBO()).GetCustomers(Country: Country, City : City, UserIdCfg: UserIdCfg);
            //loadOptions.Take = 999;
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(clienti, loadOptions)), "application/json");
        }
        [HttpGet]
        public ActionResult GetSites(DataSourceLoadOptions loadOptions, string CustomerCode = null)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<SiteModel> sites = (new SitesBO()).GetSites(CustomerCode: CustomerCode, UserIdCfg: UserIdCfg);
            //loadOptions.Take = 999;
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(sites, loadOptions)), "application/json");
        }
        [HttpGet]
        public ActionResult GetInstruments(DataSourceLoadOptions loadOptions, string CustomerCode = null, string SiteCode = null, 
                                                                              string Country = null, string City = null, string CommercialEntity = null,
                                                                              string Region = null, string Area = null, bool ExcludeDeleted = false, 
                                                                              bool ExcludeDismiss = false, string modelType = "", bool getAllModelTypes = false)
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(CustomerCode: CustomerCode, SiteCode: SiteCode, Country: Country, City: City,
                                                                                     CommercialEntity: CommercialEntity, Region: Region, Area: Area, UserIdCfg: UserIdCfg,
                                                                                     ExcludeDeleted: ExcludeDeleted, ExcludeDismiss: ExcludeDismiss, 
                                                                                     modelType: modelType, GetAllModelTypes: getAllModelTypes);
            //loadOptions.Take = 999;
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(instruments, loadOptions)), "application/json");
        }
        [HttpGet]
        public ActionResult GetPCRTarget(DataSourceLoadOptions loadOptions, string CustomerCode = null, string SiteCode = null, string Country = null, 
                                                                            string PCRTargetType = null)
        {
            List<PCRTargetModel> pathogens = (new PCRTargetBO()).Get(CustomerCode : CustomerCode, SiteCode : SiteCode, Country: Country, PCRTargetType: PCRTargetType);
            //loadOptions.Take = 999; 
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(pathogens, loadOptions)), "application/json");
        }

        
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Classes;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;
using System.ComponentModel.DataAnnotations;
using WebApp.Services;
using WebApp.Repository;
using System.Collections;
using Microsoft.Extensions.Configuration;
using NPoco;
using System.Data.SqlClient;
using System.Text;
using System.Security.Cryptography;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using WebApp.Models;

namespace WebApp.Controllers
{
    [Authorize]
    public class TKSupportController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;
        private string connectionString;

        //private readonly string DirFilesByFTP = "Upload\\FTP_WEBREPOSITORY";

        private readonly string idCacheEditRequestOrigin = "idCacheEditRequestOrigin";

        public TKSupportController(WebAppDbContext context, ILogger<TKSupportController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }

        // GET: TKSupport/EditTopic/5
        public async Task<IActionResult> WizardNewRequest()
        {
            return PartialView("_WizardNewRequest");
        }


        // GET: TKSupport/ViewActivity/
        public async Task<IActionResult> ViewActivity(int TKRequestId)
        {
            TKRequest request = _context.TKRequests.Include(x => x.Classification).Include(x => x.AddInfo).Where(x => x.TKRequestId == TKRequestId).FirstOrDefault();
            if (request == null)
            {
                return NotFound();
            }

            ViewActivityDTO dto = new ViewActivityDTO();

            DetailRequestDTO dtoDetail = GetDetailRequetDTO(request);
            PropertyCopier<DetailRequestDTO, ViewActivityDTO>.Copy(dtoDetail, dto); // Copio propriet� con stesso nome

            bool bEstraiTutto = User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_SupportViewer) || User.IsInRole(Lookup.Role_CustomerComplaintAnalyst);
            var data = _context.TKEvents.Where(x => x.TKRequestId == TKRequestId && (x.Reserved == false || bEstraiTutto) && !x.Operation.Equals(Lookup.TKEvent_Operation_NOTE_DRAFT))
                            .OrderByDescending(x => x.TmstInse).ToList();
            dto.Activities = new List<TKEventDTO>();
            foreach (var item in data)
            {
                TKEventDTO newItem = new TKEventDTO();
                PropertyCopier<TKEvent, TKEventDTO>.Copy(item, newItem); // Copio propriet� con stesso nome
                newItem.UserInse_Name = (new UsersBO()).GetFullName(newItem.UserInse);
                dto.Activities.Add(newItem);
            }

            return PartialView("_ViewActivity", dto);
        }


        [HttpGet]
        public object GetClassificationsForNewRequestJSON(int ParendId)
        {
            var data = _context.TKClassifications
                               .Where(t => ((t.ParentId == null && ParendId == 0) || t.ParentId == ParendId) && t.Reserved == false && t.Deleted == false)
                               .OrderBy(t=> t.Title)
                               .ToList();
            if (ParendId == 0)
            {
                string UserId = (new UsersBO()).GetUserId(User.Identity.Name);
                List<string> tkSupportTypesLink = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.TKSupportType);
                data = (from t in data where tkSupportTypesLink.Contains(t.ConfigId) select t).ToList();
            }
            return Json(data);
        }

        // GET: TKSupport/Create
        public IActionResult Create(int Id)
        {
            TKRequest newRequest = new TKRequest()
            {
                TKClassificationId = Id,
                TKClassificationIdRegistration = Id,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name,
                UserRegistration= User.Identity.Name,
                Status = Lookup.TKRequest_Status_DRAFT,
                PercToDoList = 0
            };

            //var indexName = HttpContext.Session.GetString(Lookup.glb_sessionkey_Requests_IndexOrigin);
            //newRequest.Complaint = string.Compare(indexName, Lookup.TKRequest_ActionIndex_Complaint) == 0;
            newRequest.Complaint = false;

            _context.TKRequests.Add(newRequest);
            _context.SaveChanges();

            // Cancellazione REQUEST in DRAFT pi� vecchie di 15gg !!!  DA SPOSTARE ??? 
            // ??? CANCELLARE ANCHE I FILE ??? 
            //_context.TKRequests.Where(t => t.UserInse.Equals(User.Identity.Name) && t.Status.Equals(Lookup.TKRequest_Status_DRAFT) 
            //            && t.TmstInse < DateTime.Now.AddDays(-15)).ToList().ForEach(r => _context.TKRequests.Remove(r));
            //_context.SaveChanges();

            HttpContext.Session.SetString(Lookup.glb_sessionkey_Requests_IndexOrigin, Lookup.TKRequest_ActionIndex_YourRequests);
            return RedirectToAction("EditRequest", new { id = newRequest.TKRequestId });
        }

        // GET: TKSupport/Detail/5
        public async Task<IActionResult> DetailRequestPartial(int id)
        {
            return RedirectToAction("DetailRequest", "TKSupport", new { id = id, asPartial = true});
        }

        // GET: TKSupport/Detail/5
        public async Task<IActionResult> DetailRequest(int id, string msgErr, bool asPartial = false)
        {
            TKRequest request = _context.TKRequests.Include(x => x.Classification).Include(x => x.AddInfo).Where(x => x.TKRequestId == id).FirstOrDefault();
            if (request == null)
            {
                return NotFound();
            }

            DetailRequestDTO dto = GetDetailRequetDTO(request);

            // Traccia lettura da parte del richiedente 
            SetLastAccess(request);

            TempData["MsgToLayout"] = msgErr;
            
            if (asPartial)
            {
                TempData["asPartial"] = true;
                return PartialView("DetailRequest", dto);
            }
            return View("DetailRequest", dto);
        }

        // GET: TKSupport/AssignToMe/5
        public async Task<IActionResult> AssignToMe(int id)
        {
            TKRequest rec2Update = _context.TKRequests.Include(x => x.Classification).Where(x => x.TKRequestId == id).FirstOrDefault();
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (rec2Update.Status.Equals(Lookup.TKRequest_Status_PENDING) || rec2Update.Status.Equals(Lookup.TKRequest_Status_REOPENED))
            {
                try
                {
                    rec2Update.Status = Lookup.TKRequest_Status_PROCESSING;
                    rec2Update.UserInChargeOf = User.Identity.Name;
                    if (rec2Update.TmstInChargeOf == null)  // solo la prima ASSEGNAZIONE !!! 
                    {
                        rec2Update.TmstInChargeOf = DateTime .Now;
                    }
                    if (rec2Update.Complaint && !string.IsNullOrWhiteSpace(rec2Update.ComplaintStatus) && !string.IsNullOrWhiteSpace(rec2Update.ComplaintLinkommInfo))
                    {
                        rec2Update.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                    }
                    rec2Update.UserLastUpd = User.Identity.Name;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    _context.Update(rec2Update);
                    TKEvent evento = new TKEvent()
                    {
                        Note = string.Format("Assign to {0}", User.Identity.Name),
                        TKRequestId = rec2Update.TKRequestId,
                        Operation = Lookup.TKEvent_Operation_ASSIGN,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Reserved = true
                    };
                    _context.TKEvents.Add(evento);
                    await _context.SaveChangesAsync();

                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, Lookup.Request_NotificationType.TakingCharge, userName: User.Identity.Name);
                }
                catch (Exception e)
                {
                    return BadRequest(String.Format("[{0}] - {1} / {2}", "AssignToMe", e.Message, e.Source));
                }
                return RedirectToAction("EditRequest", new { id = id });
            }
            // La richiesta presa in carico non � pi� in uno stato valido; attivo la consultazione della richiesta 
            return RedirectToAction("DetailRequest", new { id = id });
        }
        // GET: TKSupport/AssignToMeAsUserRegistration/5
        public async Task<IActionResult> AssignToMeAsUserRegistration(int id)
        {
            TKRequest rec2Update = _context.TKRequests.Include(x => x.Classification).Where(x => x.TKRequestId == id).FirstOrDefault();
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                string prevUserRegitration = (new UsersBO()).GetFullName(rec2Update.UserRegistration);
                string email = (new UsersBO()).GetEmail(rec2Update.UserRegistration);
                if (!string.IsNullOrWhiteSpace(email))
                {
                    if (string.IsNullOrWhiteSpace(rec2Update.EmailsKeepInformed) || !rec2Update.EmailsKeepInformed.Contains(email))
                    {
                        if (!string.IsNullOrWhiteSpace(rec2Update.EmailsKeepInformed))
                        {
                            rec2Update.EmailsKeepInformed += ",";
                        }
                        rec2Update.EmailsKeepInformed += email;
                    }
                }
                rec2Update.UserRegistration = User.Identity.Name;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Update(rec2Update);
                TKEvent evento = new TKEvent()
                {
                    Note = string.Format("Change User Registration (previous User {0})", prevUserRegitration),
                    TKRequestId = rec2Update.TKRequestId,
                    Operation = Lookup.TKEvent_Operation_CHANGE_USER_REGISTRATION,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    Reserved = true
                };
                _context.TKEvents.Add(evento);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(String.Format("[{0}] - {1} / {2}", "AssignToMeAsUserRegistration", e.Message, e.Source));
            }
            return RedirectToAction("IndexYourRequests");
        }

        // GET: TKSupport/goBackToIndex/5
        public async Task<IActionResult> goBackToIndex()
        {
            var indexName = HttpContext.Session.GetString(Lookup.glb_sessionkey_Requests_IndexOrigin);
            if (!string.IsNullOrWhiteSpace(indexName))
            {
                if (indexName.Equals(Lookup.TKRequest_ActionIndex_YourRequests))
                {
                    return RedirectToAction("IndexYourRequests");
                }
                if (indexName.Equals(Lookup.TKRequest_ActionIndex_Manage))
                {
                    return RedirectToAction("Manage");
                }
                if (indexName.Equals(Lookup.TKRequest_ActionIndex_Complaint))
                {
                    return RedirectToAction("Complaint");
                }
            }
            return RedirectToAction("Index", "Home");
        }

        // GET: TKSupport/EditRequest/5
        public async Task<IActionResult> EditRequest(int id)
        {
            var indexOrigin = HttpContext.Session.GetString(Lookup.glb_sessionkey_Requests_IndexOrigin);
            if (string.IsNullOrWhiteSpace(indexOrigin)) return RedirectToAction("DetailRequest", new { id = id });  // ??? sessione persa !! 
            
            TKRequest request = _context.TKRequests.Include(x => x.Classification).Where(x => x.TKRequestId == id).FirstOrDefault();
            if (request == null) return NotFound();
            if (!CanEditRequest(request)) return RedirectToAction("DetailRequest", new { id = id });

            // Traccia lettura da parte del richiedente 
            SetLastAccess(request);

            //TKEditRequestDTO dto = (TKEditRequestDTO)request;
            TKEditRequestDTO dto = new TKEditRequestDTO();
            PropertyCopier<TKRequest, TKEditRequestDTO>.Copy(request, dto); // Copio propriet� con stesso nome
            dto.UserRegistration_Name = (new UsersBO()).GetFullName(dto.UserRegistration);

            // Configurazione
            dto.ConfigModel = Lookup.GetTKSupportConfig(dto.Classification.ConfigId);

            dto.isEnabledNotifyAreaManager = false;
            if ((new SupportBO()).isNotifyToAreaManager(request.TKRequestId))
            {
                if (!request.Status.Equals(Lookup.TKRequest_Status_DRAFT) && !string.IsNullOrWhiteSpace((new SupportBO()).GetEmailAreaManagers(request.UserRegistration)))
                {
                    dto.isEnabledNotifyAreaManager = true;
                }
            }

            dto.ListCompaintStatus = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_COMPLAINT_STATUS)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            
            if (!User.IsInRole(Lookup.Role_SupportAdministrator) && !User.IsInRole(Lookup.Role_CustomerComplaintAnalyst)) {
                // per l'utente finale, una richiesta di Reclamo in PROCESSING deve risultare gi� UNDER INVESTIGATION 
                if (dto.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && dto.Complaint)
                {
                    dto.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                }
            }
            dto.ComplaintClassificationRootId = _context.TKAddInfos.Where(t => t.Code.Equals("COMPLAINT_ROOT")).FirstOrDefault().TKAddInfoId;

            // TO DO LIST 
            List<TKToDoItem> todoitems = _context.TKToDoItems.Where(t => t.TKRequestId == id).OrderBy(t=>t.sequence).ToList();
            dto.ToDoItemsJSON = JsonConvert.SerializeObject(todoitems);


            // Gestione Tipo Nota 
            dto.bAutoSaveDraft = false;
            dto.listNoteType = new List<LookUpString>();
            dto.indexOrigin = indexOrigin;
            switch (dto.indexOrigin)
            {
                case Lookup.TKRequest_ActionIndex_YourRequests:
                    dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT });
                    break;
                case Lookup.TKRequest_ActionIndex_Manage:
                    dto.bAutoSaveDraft = true;
                    dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT });
                    dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER });
                    if (request.Complaint)
                    {
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA });
                    }
                    break;
                case Lookup.TKRequest_ActionIndex_Complaint:
                    if (User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))  // cca
                    {
                        dto.bAutoSaveDraft = true;
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT });
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_CCA });
                    }
                    else if (User.IsInRole(Lookup.Role_SupportAdministrator))  // Support 
                    {
                        dto.bAutoSaveDraft = true;
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT });
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER });
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA });
                    }
                    else // user
                    {
                        dto.listNoteType.Add(new LookUpString() { id = Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT });
                    }
                    break;
            }
            foreach(LookUpString item in dto.listNoteType)
            {
                item.descrizione = item.id.Replace("ADD NOTE -", "").Trim();
            }

            if (dto.bAutoSaveDraft )
            {
                TKEvent draft = _context.TKEvents.Where(t => t.TKRequestId == id && t.UserInse.Equals(User.Identity.Name) && t.Operation.Equals(Lookup.TKEvent_Operation_NOTE_DRAFT)).FirstOrDefault();
                if (draft != null)
                {
                    //dto.DraftNote = System.Net.WebUtility.HtmlEncode(draft.Note);
                    dto.DraftNote = draft.Note;
                    if (!string.IsNullOrWhiteSpace(dto.DraftNote))
                    {
                        dto.DraftNote = dto.DraftNote.Replace("'", "&rsquo;");
                        dto.DraftNote = dto.DraftNote.Replace(Environment.NewLine, "<br>");
                    }
                }
            }
            return View(dto);
        }

        // POST: TKSupport/Edit
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditRequest([Bind("TKRequestId,Status,TKClassificationId,Code,Object,Description,SerialNumber,OtherPlatform,CodArt," +
                                                            "TKAddInfoId,EmailsKeepInformed,InfoContact,Quantity,LocationId,ClosingNote,TKClosingInfoId," +
                                                            "Complaint,DefectObserved,ConsequencesForEndUser,ComplaintStatus,ComplaintStatusNote," +
                                                            "ComplaintLotNumber,ComplaintCustomer,ComplaintCity,ComplaintIncident,ComplaintIncidentNote," +
                                                            "TKComplaintClassificationId,ComplaintLinkommInfo," + 
                                                            "ToDoItemsJSON,EmailSubject,EmailBody")] TKRequest dto)
        {
            bool bTrackUpdateRequest = false;
            bool bCloseRequest = false;

            TKRequest rec2Update = _context.TKRequests.Find(dto.TKRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }

            if (!CanEditRequest(rec2Update))
            {
                return RedirectToAction("DetailRequest", new { id = dto.TKRequestId, msgErr = $"Operation not allowed because the request was been changed!" });
            }


            // Configurazione
            TKClassification classif = _context.TKClassifications.Find(dto.TKClassificationId);
            if (classif == null) {
                return BadRequest(string.Format("Configuration {0} not found", dto.TKClassificationId));
            }
            TKSupportConfigModel configModel = Lookup.GetTKSupportConfig(classif.ConfigId);

            // Elaborazione 
            if (rec2Update.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && User.IsInRole(Lookup.Role_SupportAdministrator))
            {
                if (String.Compare(rec2Update.Code, dto.Code) != 0 ||
                    String.Compare(rec2Update.Description, dto.Description) != 0 ||
                    String.Compare(rec2Update.Object, dto.Object) != 0 ||
                    String.Compare(rec2Update.EmailsKeepInformed, dto.EmailsKeepInformed) != 0 ||
                    rec2Update.TKClassificationId.Equals(dto.TKClassificationId) == false ||
                    String.Compare(rec2Update.SerialNumber, dto.SerialNumber) != 0 ||
                    String.Compare(rec2Update.OtherPlatform, dto.OtherPlatform) != 0 ||
                    String.Compare(rec2Update.CodArt, dto.CodArt) != 0 ||
                    String.Compare(rec2Update.InfoContact, dto.InfoContact) != 0 ||
                    rec2Update.Quantity != dto.Quantity ||
                    rec2Update.LocationId.Equals(dto.LocationId) == false || 
                    rec2Update.Complaint != dto.Complaint || 
                    rec2Update.TKAddInfoId.Equals(dto.TKAddInfoId) == false )
                {
                    bTrackUpdateRequest = true;
                }
            }

            // se DRAFT --> creazione nuovo ticket 
            bool bCreateRequest = rec2Update.Status.Equals(Lookup.TKRequest_Status_DRAFT);
            // se DRAFT o PENDING allineo anche i dati della prima registrazione 
            bool bUpdateRegistrationInfo = (rec2Update.Status.Equals(Lookup.TKRequest_Status_DRAFT) || rec2Update.Status.Equals(Lookup.TKRequest_Status_PENDING));

            bool bChangeStatus = String.Compare(rec2Update.Status, dto.Status) != 0;
            bool bChangeComplaintStatus = false;
            bool bNotifyForwardToCCA = false;
            bool bNotifyComplaintReject = false;
            bool bTrackChangeStatus = false;
            bool bNotifyRequestData = false;
            bool bSendEngagementMail = false;
            EmailTemplate EngagementEmail = new EmailTemplate();

            try
            {
                rec2Update.Code = dto.Code;
                rec2Update.TKClassificationId = dto.TKClassificationId;
                rec2Update.Description = dto.Description;
                rec2Update.Object = dto.Object;
                rec2Update.EmailsKeepInformed = dto.EmailsKeepInformed;
                rec2Update.SerialNumber = dto.SerialNumber;
                rec2Update.OtherPlatform = dto.OtherPlatform;
                rec2Update.CodArt = dto.CodArt;
                rec2Update.TKAddInfoId = dto.TKAddInfoId;
                rec2Update.InfoContact = dto.InfoContact;
                rec2Update.Quantity = dto.Quantity;
                rec2Update.LocationId = dto.LocationId;
                // COMPLAINT 
                rec2Update.Complaint = dto.Complaint;
                if (rec2Update.Complaint)
                {
                    rec2Update.DefectObserved = dto.DefectObserved;
                    rec2Update.ConsequencesForEndUser = dto.ConsequencesForEndUser;
                    if (String.Compare(rec2Update.ComplaintStatus, dto.ComplaintStatus) != 0)
                    {
                        // GESTiONE CAMBIO STATO RECLAMO 
                        bChangeComplaintStatus = true;
                        rec2Update.ComplaintStatus = dto.ComplaintStatus;
                        rec2Update.ComplaintStatusNote = dto.ComplaintStatusNote;

                        if (dto.ComplaintStatus.Equals(Lookup.Code_Complaint_Status_REJECT))
                        {
                            rec2Update.Complaint = false;
                            rec2Update.Status = Lookup.TKRequest_Status_PROCESSING;
                            bNotifyComplaintReject = true;
                        }
                        else
                        {
                            if (dto.ComplaintStatus.Equals(Lookup.Code_Complaint_Status_FORWARD_TO_CCA))
                            {
                                bNotifyForwardToCCA = true;
                            }
                            if (dto.ComplaintStatus.Equals(Lookup.Code_Complaint_Status_REQUEST_DATA))
                            {
                                bNotifyRequestData = true;
                            }
                            // Quando cambio lo status del reclamo, la richiesta passa in PROCESSING o UNDER INVESTIGATION (se � presente il riferimento  linkomm)
                            if (rec2Update.Status.Equals(Lookup.TKRequest_Status_PENDING))
                            {
                                rec2Update.Status = Lookup.TKRequest_Status_PROCESSING;
                                bTrackChangeStatus = true;
                            }
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(dto.ComplaintLinkommInfo) && rec2Update.Status.Equals(Lookup.TKRequest_Status_PROCESSING))
                    {
                        rec2Update.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                        bTrackChangeStatus = true;
                    }
                    rec2Update.ComplaintLotNumber = dto.ComplaintLotNumber;
                    rec2Update.ComplaintCustomer = dto.ComplaintCustomer;
                    rec2Update.ComplaintCity = dto.ComplaintCity;
                    rec2Update.ComplaintIncident = dto.ComplaintIncident;
                    rec2Update.ComplaintIncidentNote = dto.ComplaintIncidentNote;
                    rec2Update.ComplaintIncidentNote = dto.ComplaintIncidentNote;
                    rec2Update.TKComplaintClassificationId = dto.TKComplaintClassificationId;
                    if (string.IsNullOrWhiteSpace(rec2Update.ComplaintLinkommInfo) && !string.IsNullOrWhiteSpace(dto.ComplaintLinkommInfo))
                    {
                        rec2Update.UserLinkommTracker = User.Identity.Name;
                    }
                    rec2Update.ComplaintLinkommInfo = dto.ComplaintLinkommInfo;
                }
                // Aggiornamento dati relativi alla prima registazione 
                if (bUpdateRegistrationInfo)
                {
                    rec2Update.CodeRegistration = dto.Code;
                    rec2Update.TKClassificationIdRegistration = dto.TKClassificationId;
                    rec2Update.DescriptionRegistration = dto.Description;
                    rec2Update.ObjectRegistration = dto.Object;
                    rec2Update.SerialNumberRegistration = dto.SerialNumber;
                    rec2Update.OtherPlatformRegistration = dto.OtherPlatform;
                    rec2Update.CodArtRegistration = dto.CodArt;
                    rec2Update.TKAddInfoIdRegistration = dto.TKAddInfoId;
                    rec2Update.InfoContactRegistration = dto.InfoContact;
                    rec2Update.QuantityRegistration = dto.Quantity;
                    rec2Update.LocationIdRegistration = dto.LocationId;
                }
                if (bCreateRequest)
                {
                    rec2Update.UserRegistration = User.Identity.Name;
                    rec2Update.Status = Lookup.TKRequest_Status_PENDING;
                }
                if (bChangeStatus && dto.Status.Equals(Lookup.TKRequest_Status_CLOSED))
                {
                    bCloseRequest = true;
                    rec2Update.StanbyMinutes = GetSuspendTime(dto.TKRequestId);
                    rec2Update.Status = Lookup.TKRequest_Status_CLOSED;
                    rec2Update.ClosingNote = dto.ClosingNote;
                    rec2Update.TKClosingInfoId = dto.TKClosingInfoId;
                    rec2Update.UserClosed = User.Identity.Name;
                    rec2Update.TmstClosed = DateTime.Now;
                }

                // TODOLIST
                if (!dto.ToDoItemsJSON.Equals("SKIP"))
                {
                    rec2Update.PercToDoList = ManageToDoList(dto.TKRequestId, dto.ToDoItemsJSON);
                }

                // Invio email 
                if (!configModel.SupplierEngagement.Equals(Lookup.Classification_RadioGroup.No))
                {
                    bSendEngagementMail = !string.IsNullOrWhiteSpace(dto.EmailSubject) && !string.IsNullOrWhiteSpace(dto.EmailBody);
                    if (bSendEngagementMail)
                    {
                        EngagementEmail.To = (new InstrumentsBO()).GetSupplierEmails(dto.SerialNumber);
                        EngagementEmail.CC = (new UsersBO()).GetEmail(User.Identity.Name);
                        EngagementEmail.ReplyTo = EngagementEmail.CC;
                        EngagementEmail.Subject = dto.EmailSubject;
                        EngagementEmail.Body = dto.EmailBody;
                        bSendEngagementMail = (!String.IsNullOrWhiteSpace(EngagementEmail.To));
                        if (!bSendEngagementMail)
                        {
                            TempData["MsgToLayout"] = string.Format("No Supplier found for instrument {0}.", dto.SerialNumber);
                        }
                    }
                }

                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Update(rec2Update);
                await _context.SaveChangesAsync();

                if (bCreateRequest || bTrackUpdateRequest)
                {
                    TKEvent evento = new TKEvent()
                    {
                        TKRequestId = rec2Update.TKRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    if (bCreateRequest)
                    {
                        evento.Note = string.Format("Create new {0}", rec2Update.RequestType);
                        evento.Operation = Lookup.TKEvent_Operation_CREATE;
                        evento.TmstInse = rec2Update.TmstInse;
                    }
                    if (bTrackUpdateRequest)
                    {
                        evento.Note = string.Format("Update {0}", rec2Update.RequestType);
                        evento.Operation = Lookup.TKEvent_Operation_UPDATE;
                        evento.Reserved = (User.IsInRole(Lookup.Role_SupportAdministrator)); // evento RISERVATO se l'operazione � fatta dal supporto
                    }
                    _context.TKEvents.Add(evento);
                }
                // CLOSE 
                if (bCloseRequest)
                {
                    TKEvent evento = new TKEvent()
                    {
                        TKRequestId = rec2Update.TKRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Close {0}", rec2Update.RequestType),
                        Operation = Lookup.TKEvent_Operation_CLOSED,
                        Reserved = true
                    };
                    _context.TKEvents.Add(evento);
                }
                if (bTrackChangeStatus)
                {
                    TKEvent evento = new TKEvent()
                    {
                        TKRequestId = rec2Update.TKRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Change Status to {0}", rec2Update.Status),
                        Operation = Lookup.TKEvent_Operation_ADDNOTE_INFO,
                        Reserved = true
                    };
                    _context.TKEvents.Add(evento);
                }
                if (bChangeComplaintStatus)
                {
                    string decoAction = _context.Codes.Find(rec2Update.ComplaintStatus).Description;
                    TKEvent evento = new TKEvent()
                    {
                        TKRequestId = rec2Update.TKRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Set Complaint Action: {0} - Notes: {1}", decoAction, rec2Update.ComplaintStatusNote),
                        Operation = Lookup.TKEvent_Operation_ADDNOTE_INFO,
                        Reserved = true
                    };
                    _context.TKEvents.Add(evento);
                }
                await _context.SaveChangesAsync();

                if (bCreateRequest) 
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, 
                                                                                             Lookup.Request_NotificationType.NewRequest, 
                                                                                             userName: User.Identity.Name, 
                                                                                             note: dto.Description, 
                                                                                             title: dto.Object);
                    TempData["MsgToLayout"] = string.Format("New {0} {1} has been created successfully.", rec2Update.RequestType, rec2Update.TKRequestId);
                }
                if (bCloseRequest)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, Lookup.Request_NotificationType.CloseRequest, 
                                                                                    userName: User.Identity.Name, note: rec2Update.ClosingNote, title: null);
                }
                if (bNotifyComplaintReject)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, Lookup.Request_NotificationType.ComplaintReject,
                                                                                    userName: User.Identity.Name, note: rec2Update.ComplaintStatusNote);
                }
                if (bNotifyForwardToCCA)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, Lookup.Request_NotificationType.ComplaintForwardToCCA,
                                                                                    userName: User.Identity.Name);
                }
                if (bNotifyRequestData)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId, Lookup.Request_NotificationType.ComplaintRequestData,
                                                                                    userName: User.Identity.Name);
                }
                if (bSendEngagementMail)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(rec2Update.TKRequestId,
                                                                                            NotificationType: Lookup.Request_NotificationType.EngagementSupplier,
                                                                                            userName: User.Identity.Name,
                                                                                            extTo: EngagementEmail.To,
                                                                                            extCC: EngagementEmail.CC,
                                                                                            extReplyTo: EngagementEmail.ReplyTo,
                                                                                            title: EngagementEmail.Subject,
                                                                                            note: EngagementEmail.Body);
                    TKEvent evento = new TKEvent()
                    {
                        Note = string.Format("[Engagement Email] To: {0}", EngagementEmail.To),
                        TKRequestId = rec2Update.TKRequestId,
                        Operation = Lookup.TKEvent_Operation_ENGAGEMENTSUPPLIER,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Reserved = true
                    };
                    _context.TKEvents.Add(evento);
                    await _context.SaveChangesAsync();
                }

                if (!bCreateRequest && !bCloseRequest)
                {
                    TempData["MsgToNotify"] = string.Format("Operation successful");
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            if (dto.Complaint && bCreateRequest)
            {
                //HttpContext.Session.SetString(Lookup.glb_sessionkey_Requests_IndexOrigin, Lookup.TKRequest_ActionIndex_Complaint);
            }
            return RedirectToAction("goBackToIndex");
            //return View(tkrequest);
        }
        private int ManageToDoList(int TKRequestId, string ToDoItemsJSON)
        {
            int PercToDoList = 0;
            List<TKToDoItem> todoitems = JsonConvert.DeserializeObject<List<TKToDoItem>>(ToDoItemsJSON);

            try
            {
                foreach (var some in _context.TKToDoItems.Where(t => t.TKRequestId == TKRequestId).ToList())
                {
                    some.sequence = -1;
                }
                _context.SaveChanges();

                int nCompleted = 0;
                foreach (var item in todoitems)
                {
                    if (item.TKToDoItemId > 0)
                    {
                        TKToDoItem rec2update = _context.TKToDoItems.Find(item.TKToDoItemId);
                        if (rec2update  != null)
                        {
                            rec2update.sequence = item.sequence;
                            rec2update.Title = item.Title;
                            rec2update.TmstLastUpd = DateTime.Now;
                            rec2update.UserLastUpd = User.Identity.Name;
                            if (!string.IsNullOrWhiteSpace(item.UserComplete))
                            {
                                nCompleted++;
                                if (!string.IsNullOrWhiteSpace(item.statusNewItem))
                                {
                                    rec2update.TmstComplete = DateTime.Now;
                                    rec2update.UserComplete = User.Identity.Name;
                                }
                            } else
                            {
                                rec2update.TmstComplete = null;
                                rec2update.UserComplete = null;
                            }
                            _context.TKToDoItems.Update(rec2update);
                            _context.SaveChanges();
                        }
                    } else
                    {
                        TKToDoItem rec2Insert = new TKToDoItem()
                        {
                            TKRequestId = TKRequestId,
                            sequence = item.sequence,
                            Title = item.Title,
                            TmstLastUpd = DateTime.Now,
                            UserLastUpd = User.Identity.Name
                        };
                        if (!string.IsNullOrWhiteSpace(item.UserComplete))
                        {
                            nCompleted++;
                            rec2Insert.TmstComplete = DateTime.Now;
                            rec2Insert.UserComplete = User.Identity.Name;
                        }
                        _context.TKToDoItems.Add(rec2Insert);
                        _context.SaveChanges();
                    }
                }
                // Cancellazione Items non aggiornati (sequence < 0)
                _context.TKToDoItems.RemoveRange(_context.TKToDoItems.Where(t => t.TKRequestId == TKRequestId && t.sequence < 0));
                _context.SaveChanges();

                if (todoitems.Count > 0)
                {
                    if (nCompleted > 0)
                    {
                        // Arrotondamento al 5 superiore 
                        double perc = (nCompleted * 100 / todoitems.Count);
                        PercToDoList = (int)Math.Round(perc / 5 , MidpointRounding.AwayFromZero) * 5;
                        if (PercToDoList < perc)
                        {
                            PercToDoList = PercToDoList + 5;
                        }
                        if (PercToDoList > 100) PercToDoList = 100;
                        if (PercToDoList == 100 && nCompleted < todoitems.Count) PercToDoList = 99;
                    }
                }

            } 
            catch(Exception e)
            {
                // 
            }
            return PercToDoList;
        }
        private int GetSuspendTime(int TKRequestId)
        {
            int ret = 0;
            List<string> operationIn = new List<string>() { Lookup.TKEvent_Operation_SUSPEND, Lookup.TKEvent_Operation_CLOSED, Lookup.TKEvent_Operation_REOPEN};
            DateTime startStandby = DateTime.MinValue;
            var eventi = _context.TKEvents.Where(t => t.TKRequestId == TKRequestId && operationIn.Contains(t.Operation))
                                          .OrderBy(t => t.TmstInse).ThenBy(t=>t.TKRequestId).ToList();
            foreach(var item in eventi)
            {
                if (item.Operation.Equals(Lookup.TKEvent_Operation_SUSPEND) || item.Operation.Equals(Lookup.TKEvent_Operation_CLOSED))
                {
                    startStandby = item.TmstInse;
                } else
                {
                    // reopen !
                    if (startStandby != DateTime.MinValue)
                    {
                        TimeSpan ts = item.TmstInse - startStandby;
                        if (ts.TotalMinutes > 0)
                        {
                            ret += (int)ts.TotalMinutes;
                        }
                        startStandby = DateTime.MinValue;
                    }
                }
            }
            return ret;
        }
        [HttpPost]
        public IActionResult AddRequestNoteJSON([FromBody]AddRequestNoteDTO DTO)
        {
            //_logger.LogInformation(string.Format("AddRequestNoteJSON() / START - RequestId: {0},  note; {1:200} ", DTO.TKRequestId, DTO.Note));

            //???? REWORK NOTE - DA RIVEDERE ????
            //bool bOnlyCCA = User.IsInRole(Lookup.Role_CustomerComplaintAnalyst) && !User.IsInRole(Lookup.Role_SupportAdministrator);
            //if (bOnlyCCA)
            //{
            //    DTO.Reserved = true;
            //}
            DTO.Reserved = true;  // visibile solo da SUPPORT E CCA 
            if (DTO.NoteType.Equals(Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT) || DTO.NoteType.Equals(Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER))
            {
                DTO.Reserved = false;
            }
            TKEvent newEvent = new TKEvent()
            {
                TKRequestId = DTO.TKRequestId,
                Operation = DTO.NoteType,
                Reserved = DTO.Reserved,
                Note = DTO.Note,
                TmstInse = DateTime.Now,
                UserInse = User.Identity.Name
            };
            _context.TKEvents.Add(newEvent);
            _context.SaveChanges();

            // svuota bozza  (non viene cancellata, per riusare l'ID!!) 
            TKEvent draftEvent = GetUserDraftEventForRequest(DTO.TKRequestId);
            draftEvent.Note = "";
            draftEvent.TmstInse = DateTime.Now;
            _context.TKEvents.Update(draftEvent);
            _context.SaveChanges();

            // pubblicazione allegati collegati alla nota Bozza
            var attach2publish = _context.TKAttachments.Where(t => t.TKRequestId == DTO.TKRequestId && t.Deleted == false && t.TKEventId == draftEvent.TKEventId).ToList();
            foreach (var attach in attach2publish)
            {
                attach.TKEventId = newEvent.TKEventId;
                attach.Draft = false;
                attach.TmstInse = DateTime.Now;
                attach.TmstLastUpd= DateTime.Now;
                attach.Reserved = DTO.Reserved;
                _context.TKAttachments.Update(attach);
           
                //string sNoteEvento = sNoteEvento = string.Format("Add New {1} about Note - Name: {0}", attach.Source, attach.TypeDeco);
                //TKEvent evento = new TKEvent()
                //{
                //    Note = sNoteEvento,
                //    TKRequestId = DTO.TKRequestId,
                //    Operation = Lookup.TKEvent_Operation_ADDATTACH,
                //    TmstInse = DateTime.Now,
                //    UserInse = User.Identity.Name,
                //    Reserved = DTO.Reserved // stessa visibilit� della NOTA
                //};
                //_context.TKEvents.Add(evento);
                _context.SaveChanges();
            }

            if (DTO.FollowRequest) {
                string email = (new UsersBO()).GetEmail(User.Identity.Name);
                TKRequest request = _context.TKRequests.Find(DTO.TKRequestId);
                if (!string.IsNullOrWhiteSpace(email) && request != null)
                {
                    bool bAddEmailCC= true;
                    if (!string.IsNullOrWhiteSpace(request.EmailsKeepInformed))
                    {
                        foreach (var item in request.EmailsKeepInformed.Split(','))
                        {
                            if (item.ToUpper().Equals(email.ToUpper())) {
                                bAddEmailCC = false;
                                break;
                            }
                        }
                    }
                    if (bAddEmailCC)
                    {
                        if (!string.IsNullOrWhiteSpace(request.EmailsKeepInformed))
                        {
                            request.EmailsKeepInformed += ",";
                        }
                        request.EmailsKeepInformed += email;
                        if (request.EmailsKeepInformed.Length <= 1000)  // per evitare errori !!! 
                        {
                            _context.TKRequests.Update(request);
                            _context.SaveChanges();
                        }
                    }
                }
            }


            // Add Note 
            // ???? REWORK NOTE - DA RIVEDERE ????
            Lookup.Request_NotificationType notificationType = Lookup.Request_NotificationType.None;
            if (DTO.isSupport)
            {
                if (DTO.Reserved)
                {
                    notificationType = Lookup.Request_NotificationType.AddReservedNote;
                }
                else
                {
                    notificationType = Lookup.Request_NotificationType.AddPublicNote;
                    if (DTO.CCAreaManager)
                    {
                        notificationType = Lookup.Request_NotificationType.AddPublicNoteCCAreaManager;
                    }
                }
            }
            else
            {
                // Nota aggiunta dall'utente che ha inserito la richiesta 
                notificationType = Lookup.Request_NotificationType.AddReporterNote;
            }
            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(DTO.TKRequestId, notificationType, userName: User.Identity.Name, note: DTO.Note);

            _logger.LogInformation(string.Format("AddRequestNoteJSON() / END - RequestId: {0} ", DTO.TKRequestId));
            return Json("");
        }

        [HttpPost]
        public IActionResult AddRequestDraftJSON([FromBody]AddRequestNoteDTO DTO)
        {
            //_logger.LogInformation(string.Format("AddRequestDraftJSON() / START - RequestId: {0},  note; {1:200} ", DTO.TKRequestId, DTO.Note));
            long EventId = 0;
            // se esiste, aggiorno la nota DRAFT per la request e per l'utente
            TKEvent draft = GetUserDraftEventForRequest(DTO.TKRequestId);
            if (draft == null)
            {
                TKEvent newDraft = new TKEvent()
                {
                    TKRequestId = DTO.TKRequestId,
                    Operation = Lookup.TKEvent_Operation_NOTE_DRAFT,
                    Reserved = true, 
                    Note = DTO.Note,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TKEvents.Add(newDraft);
                _context.SaveChanges();
                EventId = newDraft.TKEventId;
            } else
            {
                draft.Note = DTO.Note;
                draft.TmstInse = DateTime.Now;
                _context.TKEvents.Update(draft);
                _context.SaveChanges();
                EventId = draft.TKEventId;
            }

            //_logger.LogInformation(string.Format("AddRequestDraftJSON() / END - RequestId: {0} ", DTO.TKRequestId));
            return Json("");
        }

        [HttpGet]
        public object GetRequestAttachments(DataSourceLoadOptions loadOptions, int requestId, bool draft = false)
        {
            bool bSupport = User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_SupportViewer) || User.IsInRole(Lookup.Role_CustomerComplaintAnalyst);

            var data = (new SupportBO()).GetRequestAttachments(requestId: requestId, draft: draft, userInseDraft: User.Identity.Name, bOnlyPublic: !bSupport);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetRequestActivity(DataSourceLoadOptions loadOptions, int requestId, bool showAllItems = false)
        {
            bool bEstraiTutto = User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_SupportViewer) || User.IsInRole(Lookup.Role_CustomerComplaintAnalyst);
            HashSet<string> tipiNote = new HashSet<string>();
            if (User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_SupportViewer))  // SUPPORT
            {
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_SUPPORT);
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER);
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT);
            }
            if (User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))
            {
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_CCA);
            }
            if (User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_SupportViewer) || User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))
            {
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_CCA2SUPPORT);
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2CCA);
            }
            if (tipiNote.Count == 0)  // UTENTE (non Admin, non CCA!!) 
            {
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_SUPPORT2USER);
                tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_USER2SUPPORT);
            }
            // sempre visibili x tutti gli utenti 
            tipiNote.Add(Lookup.TKEvent_Operation_ADDNOTE_INFO);
            tipiNote.Add(Lookup.TKEvent_Operation_REOPEN);
            
            var data = _context.TKEvents.Where(x => x.TKRequestId == requestId && 
                                        (showAllItems || tipiNote.Contains(x.Operation)) && 
                                        (x.Reserved == false || bEstraiTutto) && !x.Operation.Equals(Lookup.TKEvent_Operation_NOTE_DRAFT))
                                        .OrderByDescending(x=>x.TmstInse).ToList();

            string usersSupport = (new UsersBO()).GetUsersInRole(roles: new[] { Lookup.Role_SupportAdministrator});
            string usersCCA = (new UsersBO()).GetUsersInRole(roles: new[] { Lookup.Role_CustomerComplaintAnalyst});

            long? lastTKEventIdRead = _context.TKEventReads.Where(t => t.TKRequestId == requestId && t.UserInse.Equals(User.Identity.Name)).Max(t => (long?)t.TKEventId);
            if (lastTKEventIdRead == null) lastTKEventIdRead = 0;
            List<TKEventDTO> dto = new List<TKEventDTO>();
            foreach(var item in data)
            {
                TKEventDTO newItem = new TKEventDTO();
                PropertyCopier<TKEvent, TKEventDTO>.Copy(item, newItem); // Copio propriet� con stesso nome
                newItem.UserInse_Name = (new UsersBO()).GetFullName(newItem.UserInse);

                // determino il ruolo dell'utente che ha inserito la nota (approssimativo!!) 
                newItem.UserInse_Role = Lookup.Role_Support;
                if (usersCCA.Contains(item.UserInse))
                {
                    newItem.UserInse_Role = Lookup.Role_CustomerComplaintAnalyst;
                } else
                {
                    if (usersSupport.Contains(item.UserInse))
                    {
                        newItem.UserInse_Role = Lookup.Role_SupportAdministrator;
                    }
                }

                newItem.isNewNote = false;
                if (tipiNote.Contains(newItem.Operation) && newItem.TKEventId > lastTKEventIdRead && !newItem.UserInse.Equals(User.Identity.Name))
                {
                    newItem.isNewNote = true;
                }


                // estrazione allegati specifici della nota 
                newItem.attachments = _context.TKAttachments.Where(t => t.TKRequestId == requestId && t.Deleted == false && t.TKEventId == newItem.TKEventId).ToList();
                dto.Add(newItem);
            }
            return DataSourceLoader.Load(dto, loadOptions);
        }

        // GET: TKSupport\IndexYourRequests
        public async Task<IActionResult> IndexYourRequests(int ClassificationAreaId = 0)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_Requests_IndexOrigin, Lookup.TKRequest_ActionIndex_YourRequests);
            ViewData["TabItemIndex"] = 0;
            var valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_IndexRequests_TabItemIndex);
            if (valore != null)
            {
                int valOut = 0;
                if (Int32.TryParse(valore, out valOut))
                {
                    ViewData["TabItemIndex"] = valOut;
                }
            }

            int CurrentArea = ClassificationAreaId;
            if (ClassificationAreaId == 0)
            {
                var area = HttpContext.Session.GetString(Lookup.glb_sessionkey_IndexRequests_Area);
                if (area != null)
                {
                    int valOut = 0;
                    if (Int32.TryParse(area, out valOut))
                    {
                        CurrentArea = valOut;
                    }
                }
            }
            if (CurrentArea == 0)
            {
                var firstArea = (new SupportBO()).GetClassificationAreas(context: Lookup.TKRequest_ActionIndex_YourRequests, userName: User.Identity.Name).FirstOrDefault();
                if (firstArea != null) CurrentArea = firstArea.id;
            }
            ViewData["CurrentArea"] = CurrentArea;
            return View();
        }

        // GET: TKSupport\Complaint
        public async Task<IActionResult> Complaint(int ClassificationAreaId = 0)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_Requests_IndexOrigin, Lookup.TKRequest_ActionIndex_Complaint);
            ViewData["TabItemIndex"] = 0;
            var valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_Complaint_TabItemIndex);
            if (valore != null)
            {
                int valOut = 0;
                if (Int32.TryParse(valore, out valOut))
                {
                    ViewData["TabItemIndex"] = valOut;
                }
            }

            int CurrentArea = ClassificationAreaId;
            if (ClassificationAreaId == 0)
            {
                var area = HttpContext.Session.GetString(Lookup.glb_sessionkey_Complaints_Area);
                if (area != null)
                {
                    int valOut = 0;
                    if (Int32.TryParse(area, out valOut))
                    {
                        CurrentArea = valOut;
                    }
                }
            }
            if (CurrentArea == 0)
            {
                var firstArea = (new SupportBO()).GetClassificationAreas(context: Lookup.TKRequest_ActionIndex_Complaint, userName: User.Identity.Name).FirstOrDefault();
                if (firstArea != null) CurrentArea = firstArea.id;

            }
            ViewData["CurrentArea"] = CurrentArea;
            bool bShow = false;
            valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TKManage_ShowClosed);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowClosed"] = bShow;
            }
            valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TKManage_ShowOnlyRequestData);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowOnlyRequestData"] = bShow;
            }
            return View();
        }

        // GET: TKSupport\Manage
        [Authorize(Roles = "Support Administrator, Support Viewer")]
        public async Task<IActionResult> Manage()
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_Requests_IndexOrigin, Lookup.TKRequest_ActionIndex_Manage);
            ViewData["TabItemIndex"] = 0;
            var valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_IndexRequests_TabItemIndex);
            if (valore != null)
            {
                int valOut = 0;
                if (Int32.TryParse(valore, out valOut))
                {
                    ViewData["TabItemIndex"] = valOut;
                }
            }

            int CurrentArea = 0;
            var area = HttpContext.Session.GetString(Lookup.glb_sessionkey_IndexRequests_Area);
            if (area != null)
            {
                int valOut = 0;
                if (Int32.TryParse(area, out valOut))
                {
                    CurrentArea = valOut;
                }
            }
            if (CurrentArea == 0)
            {
                var firstArea = (new SupportBO()).GetClassificationAreas(context: Lookup.TKRequest_ActionIndex_Manage, userName: User.Identity.Name).FirstOrDefault();
                if (firstArea != null) CurrentArea = firstArea.id;
            }
            ViewData["CurrentArea"] = CurrentArea;

            //bool showSendEmailAreaManager = false;
            //var radice = _context.TKClassifications.Find(CurrentArea);
            //if (radice != null)
            //{
            //    showSendEmailAreaManager = (radice.NotifyAreaManager ?? false);
            //}
            //ViewData["bShowSendEmailAreaManager"] = showSendEmailAreaManager;

            bool bShow = false;
            valore = HttpContext.Session.GetString(Lookup.glb_sessionkey_TKManage_ShowClosed);
            if (bool.TryParse(valore, out bShow))
            {
                ViewData["bShowClosed"] = bShow;
            }
            return View();
        }

        [HttpGet]
        public IActionResult SaveManageFilters(bool showClosed, bool showOnlyRequestData)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_TKManage_ShowClosed, showClosed.ToString());
            HttpContext.Session.SetString(Lookup.glb_sessionkey_TKManage_ShowOnlyRequestData, showOnlyRequestData.ToString());
            return Ok("OK");
        }

        // GET: TKSupport\Statistics
        [Authorize(Roles = "Support Administrator")]
        public async Task<IActionResult> Statistics(string configId)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);

            StatisticsDTO dto = new StatisticsDTO()
            {
                ConfigId = configId
            };

            List<string> tkSupportTypesLink = (new UsersBO()).GetLinks(UserId, Lookup.UserLinkType.TKSupportType);
            List<TKSupportConfigModel> allTKSupportTypes = Lookup.GetTKSupportConfigs();
            if (tkSupportTypesLink.Count > 0)
            {
                dto.TKSupportTypes = (from t in allTKSupportTypes where tkSupportTypesLink.Contains(t.ID) select t).ToList();
                if (string.IsNullOrWhiteSpace(dto.ConfigId))
                {
                    dto.ConfigId = dto.TKSupportTypes.FirstOrDefault().ID;
                }
                dto.ConfigModel = Lookup.GetTKSupportConfig(dto.ConfigId);
            }
            if (dto.TKSupportTypes == null || dto.TKSupportTypes.Count == 0) {
                TempData["MsgToLayout"] = "Configuration Missing!!!";
                return RedirectToAction("Index", "Home");
            }
            return View(dto);
        }

        [HttpGet]
        public object GetRequests(DataSourceLoadOptions loadOptions, int ClassificationAreaId, Lookup.GetRequestsType getType, bool bGetClosed = false, bool bGetOnlyRequestData = false)
        {
            string UserId = (new UsersBO()).GetUserId(User.Identity.Name);

            Lookup.GetRequestsType getTypeBO = getType;
            if (getTypeBO == Lookup.GetRequestsType.ManageComplaint && !User.IsInRole(Lookup.Role_SupportAdministrator) && User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))
            {
                getTypeBO = Lookup.GetRequestsType.onlyCCA;
            }

            List<IndexRequestDTO> data = (new SupportBO()).GetRequests(ClassificationAreaId: ClassificationAreaId, UserName: User.Identity.Name, 
                                                                UserId: UserId, getType: getTypeBO, bGetClosed: bGetClosed, bGetOnlyRequestData: bGetOnlyRequestData);

            Dictionary<string, string> commercialEntityUserInse = new Dictionary<string, string>();
            foreach (var item in data)
            {
                if (getType == Lookup.GetRequestsType.YourRequests)
                {
                    if (item.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && item.Complaint)
                    {
                        item.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                    }
                }
                if (string.IsNullOrWhiteSpace(item.CommercialEntity))
                {
                    if (commercialEntityUserInse.Count(t=>t.Key.Equals(item.UserInse)) == 0) {
                        commercialEntityUserInse.Add(item.UserInse, (new UsersBO()).GetLinks(item.UserInse, Lookup.UserLinkType.CommercialEntity, bValueList:true).FirstOrDefault());
                    }
                    item.CommercialEntity = commercialEntityUserInse[item.UserInse];
                }
            }
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetStatistics(DataSourceLoadOptions loadOptions, int ClassificationId, DateTime DateFrom, DateTime DateTo, string ConfigId)
        {
            var data = (new SupportBO()).GetStatistics(ClassificationId, DateFrom, DateTo, ConfigId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        [HttpGet]
        public object GetClassificationAreas(DataSourceLoadOptions loadOptions, string context)
        {
            var data = (new SupportBO()).GetClassificationAreas(context: context, userName: User.Identity.Name);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public IActionResult SaveIndexParams(int tabItemIndex, int Area)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_IndexRequests_Area, Area.ToString());
            HttpContext.Session.SetString(Lookup.glb_sessionkey_IndexRequests_TabItemIndex, tabItemIndex.ToString());
            return Ok("OK");
        }
        [HttpGet]
        public IActionResult SaveIndexComplaintParams(int tabItemIndex, int Area)
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_Complaints_Area, Area.ToString());
            HttpContext.Session.SetString(Lookup.glb_sessionkey_Complaint_TabItemIndex, tabItemIndex.ToString());
            return Ok("OK");
        }
        [HttpPost]
        public ActionResult UploadNewFile(int TKRequestId, bool bAboutDraftNote = false)
        {
            try
            {
                TKRequest request = _context.TKRequests.Find(TKRequestId);
                if (request == null) throw new Exception(string.Format("TKRequests {0} not found", TKRequestId));

                var AttachmentsPathName = request.AttachmentsPathName;
                if (!Directory.Exists(AttachmentsPathName)) Directory.CreateDirectory(AttachmentsPathName);

                var myNewFile = Request.Form.Files["myNewFile"];
                string NewFullFileName = System.IO.Path.Combine(request.AttachmentsPathName, myNewFile.FileName);

                if (myNewFile.FileName.ToLower().Contains("exportlog_") == true)
                {
                    throw new Exception(string.Format("The file cannot be uploaded because it is an instrument LogFile. " +
                                                      "This file shall be uploaded in the 'LogFile' button in the 'ELITe Instruments' section. " +
                                                      "In the ticket it is to be reported only the Claim ID of the uploaded LogFile. " +
                                                      "Watch video VT S019 for details."));
                }

                if (System.IO.File.Exists(NewFullFileName)) throw new Exception(string.Format("File {0} already exists", myNewFile.FileName));

                // Copia nuovo file 
                using (var fileStream = System.IO.File.Create(NewFullFileName))
                {
                    myNewFile.CopyTo(fileStream);
                }

                long DraftTKEventId = bAboutDraftNote ? GetUserDraftEventForRequest(TKRequestId).TKEventId : 0;

                TKAttachment attachment = new TKAttachment()
                {
                    TKRequestId = TKRequestId,
                    Source = myNewFile.FileName,
                    Deleted = false,
                    Type = Lookup.TKAttachType.file,
                    PhysicalFileName = myNewFile.FileName,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name,
                    Draft = bAboutDraftNote, 
                    TKEventId = DraftTKEventId,
                    Reserved = false
                };
                _context.TKAttachments.Add(attachment);
                _context.SaveChanges();

                string sNoteEvento = string.Format("Add New FILE - Name: {0}", myNewFile.FileName);
                if (!bAboutDraftNote)
                {
                    TKEvent evento = new TKEvent()
                    {
                        Note = sNoteEvento,
                        TKRequestId = TKRequestId,
                        Operation = Lookup.TKEvent_Operation_ADDATTACH,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Reserved = (User.IsInRole(Lookup.Role_SupportAdministrator))  // evento RISERVATO se l'operazione � fatta dal supporto
                    };
                    _context.TKEvents.Add(evento);
                    _context.SaveChanges();

                    if (request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && !User.IsInRole(Lookup.Role_SupportAdministrator))
                    {
                        (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(request.TKRequestId, Lookup.Request_NotificationType.ChangeAttach, 
                                                                                        userName: User.Identity.Name, note: sNoteEvento);
                    }
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return BadRequest(string.Format("{0}", e.Message));
            }
            return new EmptyResult();
        }
        [HttpGet]
        public ActionResult AddNewLink(int TKRequestId, string Link, bool bAboutDraftNote = false ) 
        {
            string msgErr = "";
            try
            {
                TKRequest request = _context.TKRequests.Find(TKRequestId);
                if (request == null)throw new Exception(string.Format("TKRequests {0} not found", TKRequestId));

                long DraftTKEventId = bAboutDraftNote ? GetUserDraftEventForRequest(TKRequestId).TKEventId : 0;

                TKAttachment attachment = new TKAttachment()
                {
                    TKRequestId = TKRequestId,
                    Source = Link,
                    Deleted = false,
                    Type = Lookup.TKAttachType.url,
                    PhysicalFileName = null,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name,
                    Draft = bAboutDraftNote,
                    TKEventId = DraftTKEventId,
                    Reserved = false
                };
                _context.TKAttachments.Add(attachment);
                _context.SaveChanges();

                string sNoteEvento = string.Format("Add New LINK - Name: {0}", Link);
                if (!bAboutDraftNote)
                {
                    TKEvent evento = new TKEvent()
                    {
                        Note = sNoteEvento,
                        TKRequestId = TKRequestId,
                        Operation = Lookup.TKEvent_Operation_ADDATTACH,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Reserved = (User.IsInRole(Lookup.Role_SupportAdministrator))    // evento RISERVATO se l'operazione � fatta dal supporto
                    };
                    _context.TKEvents.Add(evento);
                    _context.SaveChanges();

                    if (request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && !User.IsInRole(Lookup.Role_SupportAdministrator))
                    {
                        (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(request.TKRequestId, Lookup.Request_NotificationType.ChangeAttach, 
                                                                                        userName: User.Identity.Name, note: sNoteEvento);
                    }
                }
            }
            catch (Exception e)
            {
                msgErr = string.Format("{0}", e.Message);
            }
            return Json(msgErr);
        }
        [HttpGet]
        public IActionResult MarkAllAsRead (int TKRequestId)
        {
            try
            {
                long? maxId = _context.TKEvents.Where(x => x.TKRequestId == TKRequestId).Max(t => (long?)t.TKEventId);

                TKEventRead item = new TKEventRead()
                {
                    TKRequestId = TKRequestId,
                    UserInse = User.Identity.Name,
                    TmstInse = DateTime.Now, 
                    TKEventId = (maxId ?? 0)
                };
                _context.TKEventReads.Add(item);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                _logger.LogError(String.Format("[{0}] - {1} / {2}", "MarkAllAsRead", e.Message, e.Source));
            }
            return Json("");
        }
        [HttpGet]
        public IActionResult DeleteAttachment(int TKAttachmentId)
        {
            string msg = "";
            try
            {
                TKAttachment attach2upd = _context.TKAttachments.Include(t => t.Request).Where(t => t.TKAttachmentId == TKAttachmentId).FirstOrDefault();
                if (attach2upd != null)
                {
                    if (attach2upd.Type == Lookup.TKAttachType.file)
                    {
                        string FullFileName = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, attach2upd.Source);
                        if (System.IO.File.Exists(FullFileName))
                        {
                            string FileNameDeleted = string.Format("{0}_{1}_{2}", attach2upd.Source, attach2upd.TKAttachmentId, DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string FullFileNameDeleted = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, FileNameDeleted);
                            System.IO.File.Move(FullFileName, FullFileNameDeleted);
                            //attach2upd.Source = FileNameDeleted;
                            attach2upd.PhysicalFileName = FileNameDeleted;
                        }
                    }
                    attach2upd.Deleted = true;
                    attach2upd.TmstLastUpd = DateTime.Now;
                    attach2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(attach2upd);
                    string sAzione = string.Format("remove {2} {0} - Name: {1}", TKAttachmentId, attach2upd.Source, attach2upd.TypeDeco);
                    TKEvent evento = new TKEvent()
                    {
                        Note = sAzione,
                        TKRequestId = attach2upd.TKRequestId,
                        Operation = Lookup.TKEvent_Operation_REMOVEATTACH,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Reserved = (User.IsInRole(Lookup.Role_SupportAdministrator))    // evento RISERVATO se l'operazione � fatta dal supporto
                    };
                    _context.TKEvents.Add(evento);
                    _context.SaveChanges();

                    TKRequest request = _context.TKRequests.Find(attach2upd.TKRequestId);
                    if (request != null)
                    {
                        if (request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && !User.IsInRole(Lookup.Role_SupportAdministrator))
                        {
                            (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(request.TKRequestId, Lookup.Request_NotificationType.ChangeAttach, 
                                                                                            userName: User.Identity.Name, note: sAzione);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteAttachment", e.Message, e.Source);
            }
            return Json(msg);
        }

        public ActionResult DownloadAttachment(int id)
        {
            string fileNameError = "DownloadAttachment_ERROR.txt";
            TKAttachment attach = _context.TKAttachments.Find(id);
            if (attach == null)
            {
                return NotFound();
            }
            TKRequest request = _context.TKRequests.Find(attach.TKRequestId);
            if (request == null)
            {
                return NotFound();
            }

            string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, attach.Source);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = attach.Source };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nFile Id: {0}", attach.TKAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nRequest Id: {0}", attach.TKRequestId);
                msg += String.Format("\nFileName: {0}", attach.PhysicalFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAttachment() function.");
                msg += String.Format("\nFile Id: {0}", attach.TKAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }
        
        // OpenResource: legge i dati delle risorsa e imposta la lettura
        public JsonResult GetAttachmentJSON(int id)
        {
            TKAttachment data = _context.TKAttachments.Find(id);
            if (data == null)
            {
                return Json("");
            }
            return Json(data);
        }

        [HttpGet]
        public IActionResult DeleteRequest(int Id)
        {
            string msg = "";
            TKRequest rec2Update = _context.TKRequests.Find(Id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.TKRequest_Status_DELETED;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.Update(rec2Update);
                TKEvent evento = new TKEvent()
                {
                    Note = string.Format("Delete {0}", rec2Update.RequestType),
                    TKRequestId = rec2Update.TKRequestId,
                    Operation = Lookup.TKEvent_Operation_DELETE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    Reserved = (User.IsInRole(Lookup.Role_SupportAdministrator))        // evento RISERVATO se l'operazione � fatta dal supporto
                };
                _context.TKEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteRequest", e.Message, e.Source);
            }
            return Json(msg);
        }

        private bool CanEditRequest(TKRequest request)
        {
            var LastAction = HttpContext.Session.GetString(Lookup.glb_sessionkey_Requests_IndexOrigin);
            var bCanEdit = false;
            if (!string.IsNullOrWhiteSpace(LastAction))
            {
                if (LastAction.Equals(Lookup.TKRequest_ActionIndex_YourRequests))
                {
                    // Stato DRAFT oppure PENGING e l'utente deve essere il proprietario 
                    if (request.Status.Equals(Lookup.TKRequest_Status_DRAFT) || request.Status.Equals(Lookup.TKRequest_Status_PENDING))
                    {
                        bCanEdit = string.IsNullOrWhiteSpace(request.UserRegistration) || request.UserRegistration.Equals(User.Identity.Name);
                    } else
                    {
                        bCanEdit = request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) || request.Status.Equals(Lookup.TKRequest_Status_REOPENED);
                    }
                }
                if (LastAction.Equals(Lookup.TKRequest_ActionIndex_Manage))
                {
                    // stato PROCESSING e l'utente deve essere un SupportAdministrator
                    bCanEdit = request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && User.IsInRole(Lookup.Role_SupportAdministrator);
                }
                if (LastAction.Equals(Lookup.TKRequest_ActionIndex_Complaint))
                {
                    if (User.IsInRole(Lookup.Role_SupportAdministrator) || User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))
                    {
                        bCanEdit = request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) || request.Status.Equals(Lookup.TKRequest_Status_PENDING) || request.Status.Equals(Lookup.TKRequest_Status_UNDER_INVESTIGATION);
                    }
                    else
                    {
                        // Stato DRAFT oppure PENGING e l'utente deve essere il proprietario 
                        if (request.Status.Equals(Lookup.TKRequest_Status_DRAFT) || request.Status.Equals(Lookup.TKRequest_Status_PENDING))
                        {
                            bCanEdit = string.IsNullOrWhiteSpace(request.UserRegistration) || request.UserRegistration.Equals(User.Identity.Name);
                        }
                        else
                        {
                            bCanEdit = request.Status.Equals(Lookup.TKRequest_Status_PROCESSING) || request.Status.Equals(Lookup.TKRequest_Status_REOPENED) || request.Status.Equals(Lookup.TKRequest_Status_UNDER_INVESTIGATION);
                        }
                    }
                }
            }
            return bCanEdit;
        }

        [HttpGet]
        [AllowAnonymous]
        public object getUsers2Assign(DataSourceLoadOptions loadOptions, int TKRequestId)
        {
            List<User> data = new List<User>();
            string currentUserInChargeOf = "";
            if (TKRequestId > 0)
            {
                currentUserInChargeOf = _context.TKRequests.Find(TKRequestId).UserInChargeOf;
            }
            data = (new UsersBO()).Get(filterRoleName: Lookup.Role_SupportAdministrator, ExcludeLocked: true)
                        .Where(t => string.IsNullOrWhiteSpace(currentUserInChargeOf) || !t.userName.Equals(currentUserInChargeOf)).ToList();
            return DataSourceLoader.Load(data, loadOptions);
        }

        // GET: TKSupport/AssignRequest
        public IActionResult AssignRequest(int TKRequestId, string userName, string NoteAssign)
        {
            TKRequest rec2Update = _context.TKRequests.Find(TKRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (!rec2Update.Status.Equals(Lookup.TKRequest_Status_PROCESSING)) 
            {
                return RedirectToAction("DetailRequest", new { id = TKRequestId, msgErr = $"Operation not allowed because the request was been changed!" });
            }

            try
            {
                rec2Update.UserInChargeOf = userName;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Update(rec2Update);

                TKEvent evento = new TKEvent()
                {
                    TKRequestId = rec2Update.TKRequestId,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    Operation = Lookup.TKEvent_Operation_ASSIGN,
                    Note = string.Format("Assign to {0}", userName),
                    Reserved = true
                };
                if (!string.IsNullOrWhiteSpace(NoteAssign))
                {
                    evento.Note += string.Format(" - Note: {0}", NoteAssign);
                }
                _context.TKEvents.Add(evento);
                _context.SaveChanges();

                // Invia Notifica assegnazione
                (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(TKRequestId, Lookup.Request_NotificationType.AssignRequest, 
                                                                                userName: User.Identity.Name, note: NoteAssign);

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return RedirectToAction("Manage");
        }

        // GET: TKSupport/SuspendRequest
        public IActionResult SuspendRequest(int TKRequestId,string Note)
        {
            TKRequest rec2Update = _context.TKRequests.Find(TKRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (!rec2Update.Status.Equals(Lookup.TKRequest_Status_PROCESSING))
            {
                return RedirectToAction("DetailRequest", new { id = TKRequestId, msgErr = $"Operation not allowed because the request was been changed!" });
            }

            try
            {
                rec2Update.Status = Lookup.TKRequest_Status_STANDBY;
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Update(rec2Update);

                TKEvent evento = new TKEvent()
                {
                    TKRequestId = rec2Update.TKRequestId,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    Operation = Lookup.TKEvent_Operation_SUSPEND,
                    Note = Note,
                    Reserved = false
                };
                _context.TKEvents.Add(evento);
                _context.SaveChanges();

                // Invia Notifica SOSPENSIONE
                (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(TKRequestId, Lookup.Request_NotificationType.SupendRequest, 
                                                                                userName: User.Identity.Name, note: Note);

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return RedirectToAction("Manage");
        }

        // GET: TKSupport/ReopenRequest
        public IActionResult ReopenRequest(int TKRequestId, string Note)
        {
            bool bSendEmail = false;
            TKRequest rec2Update = _context.TKRequests.Find(TKRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }
            if (rec2Update.Status.Equals(Lookup.TKRequest_Status_STANDBY))
            {
                rec2Update.Status = Lookup.TKRequest_Status_PROCESSING;
                // se RECLAMO, potrebbe tornare in stato UNDER_INVESTIGATION !! 
                if (rec2Update.Complaint && !string.IsNullOrWhiteSpace(rec2Update.ComplaintStatus) && !string.IsNullOrWhiteSpace(rec2Update.ComplaintLinkommInfo))
                {
                    rec2Update.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                }
            }
            else if (rec2Update.Status.Equals(Lookup.TKRequest_Status_CLOSED))
            {
                bSendEmail = true;
                rec2Update.Status = Lookup.TKRequest_Status_REOPENED;
                rec2Update.UserInChargeOf = null;
                rec2Update.TmstClosed = null;
                rec2Update.UserClosed = null;
                rec2Update.StanbyMinutes = 0;
            }
            else { 
                return RedirectToAction("DetailRequest", new { id = TKRequestId, msgErr = $"Operation not allowed because the request was been changed!" });
            }

            try
            {
                rec2Update.UserLastUpd = User.Identity.Name;
                rec2Update.TmstLastUpd = DateTime.Now;
                _context.Update(rec2Update);

                TKEvent evento = new TKEvent()
                {
                    TKRequestId = rec2Update.TKRequestId,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    Operation = Lookup.TKEvent_Operation_REOPEN,
                    Note = Note,    
                    Reserved = false
                };
                _context.TKEvents.Add(evento);
                _context.SaveChanges();

                // Invia Notifica RIAPERTURA
                if (bSendEmail)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(TKRequestId, Lookup.Request_NotificationType.ReopenRequest, userName: User.Identity.Name, note: Note);
                    //(new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(TKRequestId, Lookup.Request_NotificationType.ReopenRequest, userName: User.Identity.Name);
                }

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return RedirectToAction("goBackToIndex");
        }

        private void SetLastAccess(TKRequest rec2Update)
        {
            
            if (!rec2Update.Status.Equals(Lookup.TKRequest_Status_DRAFT) && !rec2Update.Status.Equals(Lookup.TKRequest_Status_PENDING) && rec2Update.UserRegistration.Equals(User.Identity.Name))
            {
                rec2Update.UserRegistration_LastAccess = DateTime.Now;
                _context.Update(rec2Update);
                _context.SaveChanges();
            }
        }

        //[HttpGet]
        //public object GetUsersRegistration(DataSourceLoadOptions loadOptions, string currentUserRegistration)
        //{
        //    List<User> data = new List<User>();
        //    if (!string.IsNullOrWhiteSpace(currentUserRegistration))
        //    {
        //        data.Add(new Entity.User { userName = currentUserRegistration });
        //        return DataSourceLoader.Load(data, loadOptions);
        //    }
        //    data = (new UsersBO()).Get(ExcludeLocked: true).ToList();
        //    return DataSourceLoader.Load(data, loadOptions);
        //}

        [HttpGet]
        public ActionResult GetProducts(DataSourceLoadOptions loadOptions)
        {
            List<Product> produtcs = (new SupportBO()).GetProducts();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(produtcs, loadOptions)), "application/json");
        }
        // GET:Trainings/GetEmailInfoJSON
        public async Task<IActionResult> GetEmailInfoJSON(int TKRequestId, string EmailTemplateId)
        {
            EmailTemplate data = (new NotificationBO(_context, _emailSender, _logger)).GetEmailInfoTK(TKRequestId: TKRequestId,
                                                                                              templateId: EmailTemplateId,
                                                                                              userName: User.Identity.Name);
            return Json(data);
        }
        [HttpPost]
        public IActionResult SendAreaManagerEmail([FromBody]EmailTemplate DTO)
        {
            string msgException = "";
            string msgSendEmail = "[OK]";
            TKRequest request = _context.TKRequests.FirstOrDefault(t => t.TKRequestId == DTO.IdRif);
            if (request == null)
            {
                return NotFound();
            }
            try
            {
                string msgErr = (new NotificationBO(_context, _emailSender, _logger)).SendNotificationTK(request.TKRequestId, Lookup.Request_NotificationType.AreaManager, 
                                                                                                userName: User.Identity.Name,
                                                                                                note: DTO.Area, title: DTO.Subject, bTrack: true);
                if (!string.IsNullOrWhiteSpace(msgErr)) 
                {
                    return BadRequest(string.Format("[SendAreaManagerEmail] {0}", msgErr));
                }
            }
            catch (Exception e)
            {
                return BadRequest(string.Format("[SendAreaManagerEmail] {0} - Exception Message: {1}", e.Message, e.Source));
            }
            return Json("");
        }
        // Ricerca e creazione della NOTA bozza per l'utente sulla richiesta
        private TKEvent GetUserDraftEventForRequest(int TKRequestId)
        {
            TKEvent draftEvent = _context.TKEvents.Where(t => t.TKRequestId == TKRequestId && t.UserInse.Equals(User.Identity.Name) &&
                                                         t.Operation.Equals(Lookup.TKEvent_Operation_NOTE_DRAFT)).FirstOrDefault();
            if (draftEvent == null)
            {
                draftEvent = new TKEvent()
                {
                    TKRequestId = TKRequestId,
                    Operation = Lookup.TKEvent_Operation_NOTE_DRAFT,
                    Reserved = true,
                    Note = null,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.TKEvents.Add(draftEvent);
                _context.SaveChanges();
            }
            return draftEvent;
        }
        private DetailRequestDTO GetDetailRequetDTO(TKRequest request)
        {
            DetailRequestDTO dto = new DetailRequestDTO();
            PropertyCopier<TKRequest, DetailRequestDTO>.Copy(request, dto); // Copio propriet� con stesso nome
            dto.UserRegistration_Name = (new UsersBO()).GetFullName(dto.UserRegistration);

            var classifPath = _context.TKClassificationPaths.Where(x => x.TKClassificationId == request.TKClassificationId && x.TKClassificationIdRef == x.TKClassificationId).FirstOrDefault();
            dto.ClassificationDeco = (classifPath != null ? classifPath.BreadCrumb : request.Classification.TitleFull);
            if (request.Classification.Instrument != Lookup.Classification_RadioGroup.No)
            {
                dto.showInstrument = true;
                if (!string.IsNullOrWhiteSpace(request.SerialNumber))
                {
                    var instrument = (new InstrumentsBO()).GetInstruments(SerialNumber: request.SerialNumber).FirstOrDefault();
                    dto.InstrumentDeco = (instrument != null ? instrument.SerialNumberInfo : request.SerialNumber);
                }
            }
            if (request.Classification.OtherPlatform != Lookup.Classification_RadioGroup.No)
            {
                dto.showOtherPlatform = true;
            }
            if (request.Classification.CodArt != Lookup.Classification_RadioGroup.No)
            {
                dto.showCodArt = true;
            }
            if (request.Classification.ClaimLogFile != Lookup.Classification_RadioGroup.No)
            {
                dto.showClaimLogFile = true;
            }
            if (request.Classification.AddInfo != Lookup.Classification_RadioGroup.No)
            {
                dto.showAddInfo = true;
                if (request.TKAddInfoId != null)
                {
                    var AddInfoPath = _context.TKAddInfoPaths.Where(x => x.TKAddInfoIdRef == request.TKAddInfoId && x.TKAddInfoIdRef == x.TKAddInfoId).FirstOrDefault();
                    dto.AddInfoDeco = (AddInfoPath != null ? AddInfoPath.BreadCrumb : request.AddInfo.TitleFull);
                }
            }
            dto.showInfoContact = (request.Classification.InfoContact != Lookup.Classification_RadioGroup.No);
            dto.showQuantity = (request.Classification.Quantity != Lookup.Classification_RadioGroup.No);
            if (request.Classification.LocationSGAT != Lookup.Classification_RadioGroup.No)
            {
                dto.showLocation = true;
                if (request.LocationId != null)
                {
                    Site location = _context.Sites.Find(request.LocationId);
                    dto.locationDeco = location.DescriptionFull;
                }
            }
            if (request.Classification.CustomerSGAT != Lookup.Classification_RadioGroup.No)
            {
                dto.showCustomer= true;
            }

            if (request.Classification.ClosingInfo != Lookup.Classification_RadioGroup.No)
            {
                dto.showClosingInfo = true;
                if (request.TKClosingInfoId != null)
                {
                    var ClosingInfoPath = _context.TKClosingInfoPaths.Where(x => x.TKClosingInfoIdRef == request.TKClosingInfoId && x.TKClosingInfoIdRef == x.TKClosingInfoId).FirstOrDefault();
                    dto.ClosingInfoDeco = (ClosingInfoPath != null ? ClosingInfoPath.BreadCrumb : request.ClosingInfo.TitleFull);
                }
            }
            if (!User.IsInRole(Lookup.Role_SupportAdministrator) && !User.IsInRole(Lookup.Role_CustomerComplaintAnalyst))
            {
                // per l'utente finale, una richiesta di Reclamo in PROCESSING deve risultare gi� UNDER INVESTIGATION 
                if (dto.Status.Equals(Lookup.TKRequest_Status_PROCESSING) && dto.Complaint)
                {
                    dto.Status = Lookup.TKRequest_Status_UNDER_INVESTIGATION;
                }
            }
            if (request.TKComplaintClassificationId != null)
            {
                var AddInfoPath = _context.TKAddInfoPaths.Where(x => x.TKAddInfoIdRef == request.TKComplaintClassificationId && x.TKAddInfoIdRef == x.TKAddInfoId).FirstOrDefault();
                dto.ComplaintClassificationDeco = (AddInfoPath != null ? AddInfoPath.BreadCrumb : request.AddInfo.TitleFull);
            }
            if (!string.IsNullOrWhiteSpace(request.ComplaintStatus))
            {
                dto.ComplaintStatusDeco = _context.Codes.Find(request.ComplaintStatus).Description;
            }

            dto.ConfigModel = Lookup.GetTKSupportConfig(request.Classification.ConfigId);

            // TO DO LIST 
            List<TKToDoItem> todoitems = _context.TKToDoItems.Where(t => t.TKRequestId == request.TKRequestId).OrderBy(t => t.sequence).ToList();
            dto.ToDoItemsJSON = JsonConvert.SerializeObject(todoitems);

            return dto;
        }

        
        [HttpGet]
        [AllowAnonymous]
        public object GetTemplateToDoList(DataSourceLoadOptions loadOptions, string ConfigId, int TKRequestId)
        {
            List<string> data = (new SupportBO()).GetTemplateToDoList(ConfigId, TKRequestId);
            return DataSourceLoader.Load(data, loadOptions);
        }

        // LOCATION (SITE)
        public object GetLocationSites(DataSourceLoadOptions loadOptions, string codNaz = "", string serialNumber = "")
        {
            List<Site> data = _context.Sites.Where(t => (string.IsNullOrWhiteSpace(codNaz) || t.Cod_Naz.Equals(codNaz)) && !t.Flag_Tratt.Equals("A"))
                                   .OrderBy(t => t.Rag_Sociale).ThenBy(t => t.Indirizzo).ToList();
            if (!string.IsNullOrWhiteSpace(serialNumber))
            {
                InstrumentModel instr = (new InstrumentsBO()).GetInstrument(serialNumber);
                if (instr == null)
                {
                    data = new List<Site>();
                } else
                {
                    data = data.Where(t => t.Cod_CliFor.Equals(instr.CustomerCode) && t.Cod_Sito.Equals(instr.SiteCode))
                        .OrderBy(t => t.Rag_Sociale).ThenBy(t => t.Indirizzo).ToList();
                }
            }
            return DataSourceLoader.Load(data, loadOptions);
        }

        public JsonResult RequestEventsJSON(int WSPRequestId, string Operation = "")
        {
            IEnumerable<TKEvent> dati = _context.TKEvents.Where(t => t.TKRequestId == WSPRequestId && (string.IsNullOrWhiteSpace(Operation) || t.Operation.Equals(Operation)))
                                                           .OrderBy(t => t.TKEventId).ToList();
            if (dati == null)
            {
                return Json("");
            }
            return Json(dati);
        }

    }
}

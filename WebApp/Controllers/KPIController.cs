﻿using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NPoco;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Data;
using WebApp.Entity;
using WebApp.Models;
using WebApp.Repository;
using WebApp.Services;

namespace WebApp.Controllers
{
    [Authorize(Roles = "Key Performance Indicators")]
    public class KPIController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ISmsSender _smsSender;
        private readonly ILogger _logger;
        private readonly WebAppDbContext _context;
        private string connectionString;

        public KPIController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ISmsSender smsSender,
            ILogger<AccountController> logger,
            WebAppDbContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _smsSender = smsSender;
            _logger = logger;
            _context = context;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                connectionString = Lookup.getConnectionString;
            }
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.SqlServer2012, SqlClientFactory.Instance);
            }
        }


        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> LoadPKIPartial(string code)
        {
            return PartialView(code);
        }

        [HttpGet]
        public object GetSample2(DataSourceLoadOptions loadOptions)
        {
            return DataSourceLoader.Load(SampleData.Sales, loadOptions);
        }
        [HttpGet]
        public object GetKPI1_Actions(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI1> rows = new List<ModelKPI1>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT Num as KPI_NUM, 
                                        ActionType AS KPI1_Type, 
                                        CAST(Data_Apertura as date) as OpeningDate, 
                                        CAST(Data_chiusura_azioni as date) as ActionsClosureDate, 
                                        CAST(Data_Chiusura_Efficacia as date) as EffectivenessClosureDate, 
                                        ActionStatus as KPI1_Status, 
                                        1 AS KPI1_Count
                                    from QA_ACTIONS
                                    WHERE 1 = 1";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                //string Sql = $@"Select N, Data_Apertura,Data_chiusura_azioni, Data_Chiusura_Efficacia,  tipo, stato, Count(*) as cnt FROM ({innerSql}) TMP";
                //Sql += @" GROUP BY N, Data_Apertura,Data_chiusura_azioni, Data_Chiusura_Efficacia,  tipo, stato";
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI1>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI1_Actions - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI6_Actions(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI6> rows = new List<ModelKPI6>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT Num   as KPI_NUM, 
                                           ActionType AS KPI6_Type,
                                           CAST(Data_Apertura as date) as OpeningDate, 
                                           CAST(Data_chiusura_azioni as date) as ClosingDateOfActions, 
                                           Datediff(day, CAST(Data_Apertura as date), CAST(Data_chiusura_azioni as date) ) as ImplementationDays
                                      from QA_Actions
                                      WHERE 1 = 1
                                      AND Data_chiusura_azioni IS NOT NULL ";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                //string Sql = $@"SELECT DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA) as Data_Apertura, 
                //                       Tipo, GGDURATA, COUNT(*) as cnt FROM ({innerSql}) TMP";
                // Sql += @" GROUP BY DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA), Tipo, GGDURATA";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI6>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI6_Actions - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI7_Actions(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI7> rows = new List<ModelKPI7>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM, 
                                            ActionType AS KPI7_Type,
                                            CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                            CAST(DATA_CHIUSURA_AZIONI AS DATE) AS ClosingDateOfActions, 
		                                    CAST(Data_Implementazione_1 AS DATE) AS ImplementationDateOfACAP, 
                                            DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), CAST(DATA_CHIUSURA_AZIONI AS DATE)) -
		                                    DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), CAST(Data_Implementazione_1 AS DATE)) AS DelayDays,
		                                    PROCESSO_VMP AS KPI7_ProcessoVpm, 
                                            SOTTOPROCESSO AS KPI7_SOTTOPROCESSO
                                        FROM QA_Actions
                                        WHERE 1 = 1
                                        AND DATA_CHIUSURA_AZIONI IS NOT NULL
	                                    AND Data_Implementazione_1 IS NOT NULL";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = $@"SELECT * FROM ({innerSql}) TMP WHERE DelayDays > 0";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI7>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI7_Actions - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI13_Change(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI13> rows = new List<ModelKPI13>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM, 
                                            CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                            CAST(DATA_CHIUSURA AS DATE) AS ClosingDate, 
                                            DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), CAST(DATA_CHIUSURA AS DATE)) AS ImplementationDays
                                    FROM QA_Change
                                    WHERE DATA_CHIUSURA IS NOT NULL";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI13>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI13_Change - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI14_Change(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI14> rows = new List<ModelKPI14>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                        CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
            			                COALESCE(CAST(Data_chiusura AS DATE), CAST(getDate() as DATE)) AS ClosingDate,
			                            CAST(Data_Implementazione_CC AS DATE) AS ImplementationDateOfCC
                                    FROM QA_Change
                                    WHERE 1 = 1
		                            AND Data_Implementazione_CC IS NOT NULL";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = $@"SELECT TMP.*, 
                                       DATEDIFF(DAY, OpeningDate, ClosingDate) -  DATEDIFF(DAY, OpeningDate, ImplementationDateOfCC) AS DelayDays
                                  FROM ({innerSql}) TMP";
                            Sql += @" WHERE (DATEDIFF(DAY, OpeningDate, ClosingDate) -  DATEDIFF(DAY, OpeningDate, ImplementationDateOfCC) ) > 0";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI14>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI14_Change - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI17_NonCompliant(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI17> rows = new List<ModelKPI17>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM as KPI_NUM, 
                                            CAST(Data_apertura as date) as OpeningDate, 
                                            CAST(Data_chiusura as date) as ClosingDate, 
                                            CAST(Data_chiusura_indagine as date) as ClosingDateOfInvestigation, 
                                            STATUS AS KPI17_Stato, 
		                                    1 as KPI17_Count
                                        from QA_UNCOMPLIANCE
	                                    WHERE 1 = 1";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                //string Sql = $@"SELECT DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA) as Data_Apertura, 
                //                       Tipo, GGDURATA, COUNT(*) as cnt FROM ({innerSql}) TMP";
                // Sql += @" GROUP BY DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA), Tipo, GGDURATA";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI17>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI17_NonCompliant - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI25_NonCompliant(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI25> rows = new List<ModelKPI25>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM as KPI_NUM, 
                                            CAST(Data_apertura as date) as OpeningDate, 
                                            IIF(Upper(Apertura_RAC) = 'SI', 1, 0) as KPI25_Count
                                        from QA_UNCOMPLIANCE
                                      WHERE 1 = 1";
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI25>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI25_NonCompliant - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        
        [HttpGet]
        public object GetKPI26_NonCompliant(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI26> rows = new List<ModelKPI26>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM as KPI_NUM, 
                                           --Classificazione_Evento as Classification,
                                           Rilevata_su as DetectOn,
                                           CAST(Data_apertura as date) as OpeningDate, 
                                           CAST(Data_Chiusura as date) as ClosingDate, 
                                           DATEDIFF(DAY, CAST(Data_Apertura AS DATE), CAST(Data_Chiusura AS DATE)) AS ClosingDays
                                        from QA_UNCOMPLIANCE
                                      WHERE DATA_CHIUSURA IS NOT NULL";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                //string Sql = $@"SELECT DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA) as Data_Apertura, 
                //                       Tipo, GGDURATA, COUNT(*) as cnt FROM ({innerSql}) TMP";
                // Sql += @" GROUP BY DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA), Tipo, GGDURATA";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI26>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI26_NonCompliant - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI29_NonCompliant(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI29> rows = new List<ModelKPI29>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM as KPI_NUM, 
                                            CAST(Data_apertura as date) as OpeningDate, 
                                            CAST(Data_chiusura as date) as ClosingDate,
                                            Codice_Nome as Code, 
                                            STATUS AS KPI29_Stato, 
		                                    1 as KPI29_Count
                                        from QA_UNCOMPLIANCE
	                                    WHERE LEFT(Codice_Nome, 4) IN ('962-', '971-', '972-' , '964-' , '910-')";
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI29>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI29_NonCompliant - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI33_Claims(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI33> rows = new List<ModelKPI33>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                        CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                        IIF(Upper(Apertura_RAC) = 'SI', 1, 0) as KPI33_Count
                                FROM QA_Claim";
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI33>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI33_Claims - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI34_Claims(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI34> rows = new List<ModelKPI34>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                        CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                        IIF(Upper(Comunicazione_Cliente) = 'SI', 1, 0) as KPI34_Count,
                                        CLASSIFICAZIONE AS Classification
                                FROM QA_Claim";
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI34>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI34_Claims - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI35_Claims(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI35> rows = new List<ModelKPI35>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
			                    COALESCE(CAST(Data_chiusura AS DATE), CAST(getDate() as DATE)) AS ClosingDate,
			                    DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), COALESCE(CAST(Data_chiusura AS DATE), CAST(getDate() as DATE))) AS OpeningDays, 
                                CLASSIFICAZIONE AS Classification
                        FROM QA_Claim";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI35>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI35_Claims - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI36_Claims(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI36> rows = new List<ModelKPI36>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                           CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                           CAST(Data_chiusura AS DATE) AS ClosingDate,
                                           DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), CAST(Data_chiusura AS DATE)) AS ClosingDays, 
                                           CLASSIFICAZIONE AS Classification
                                    FROM QA_Claim
                                    WHERE DATA_CHIUSURA IS NOT NULL";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI36>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI36_Claims - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        [HttpGet]
        public object GetKPI37_Claims(DataSourceLoadOptions loadOptions, string jsonParams)
        {
            List<ModelKPI37> rows = new List<ModelKPI37>();
            List<object> args = new List<object>();

            JsonParamsGetKPI param = null;
            if (!string.IsNullOrEmpty(jsonParams))
            {
                param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
            }

            if (param != null && param.Execute)
            {
                //string innerSql = @"SELECT TOP(85) PERCENT NUM AS KPI_NUM,  
                //                CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                //       CAST(Data_Chiusura_Indagine AS DATE) AS ClosingDateOfInvestigation,
                //       DATEDIFF(DAY, CAST(Data_Apertura AS DATE), CAST(Data_Chiusura_Indagine AS DATE)) AS ClosingDays, 
                //                CLASSIFICAZIONE AS Classification
                //        FROM QA_Claim
                //        where Classificazione_Evento like '%performance%'
                //        AND Data_Chiusura_Indagine  IS NOT NULL
                //        ORDER BY ClosingDays DESC";
                
                string innerSql = @"SELECT NUM AS KPI_NUM,  
                                CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
                                CAST(Data_Chiusura_Indagine AS DATE) AS ClosingDateOfInvestigation,
                                CAST(Data_chiusura AS DATE) AS ClosingDate, 
                                Datediff(day, CAST(Data_Apertura as date), CAST(Data_Chiusura_Indagine as date) ) as ClosingDays,
                                CLASSIFICAZIONE AS Classification, 
                                IIF (Data_Chiusura_Indagine is null, 0, 1) as KPI37_CountClosed,
                                1 as KPI37_Count
                            FROM QA_Claim
                                where Classificazione_Evento like '%performance%'
                        ORDER BY DATA_APERTURA";
                //if (param.DateFrom != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) >= CONVERT(datetime, @dataDa, 103)";
                //    args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
                //}
                //if (param.DateTo != null)
                //{
                //    innerSql += " AND CONVERT(date, DATA_APERTURA ) <= CONVERT(datetime, @dataA, 103)";
                //    args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
                //}
                string Sql = innerSql;
                try
                {
                    using (IDatabase db = Connection)
                    {
                        rows = db.Query<ModelKPI37>(Sql, args.ToArray()).ToList();
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetKPI37_Claims - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return DataSourceLoader.Load(rows, loadOptions);
        }
        //[HttpGet]
        //public object GetKPI10(DataSourceLoadOptions loadOptions, string jsonParams)
        //{
        //    List<ModelKPI10> rows = new List<ModelKPI10>();
        //    List<object> args = new List<object>();

        //    JsonParamsGetKPI param = null;
        //    if (!string.IsNullOrEmpty(jsonParams))
        //    {
        //        param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
        //    }

        //    if (param != null && param.Execute)
        //    {
        //        string innerSql = @"SELECT NUM as KPI10_N, 
        //                                CAST(DATA_APERTURA AS DATE) AS KPI10_DATA_APERTURA, 
        //                                CAST(DATA_CHIUSURA AS DATE) AS KPI10_DATA_CHIUSURA, 
        //                          CAST(DATA_NON_APPROVAZIONE AS DATE) AS KPI10_DATA_NON_APPROVAZIONE, 
        //                          CASE 
        //                           WHEN CC_NON_APPROVATO ='Si' THEN '**NOT APPROVED**'
        //                           ELSE '**APPROVED**'
        //                          END AS KPI10_NOT_APPROVED, 
        //                          CASE 
        //                           WHEN DATA_CHIUSURA IS NULL THEN '**OPEN**'
        //                           ELSE '**CLOSE**'
        //                          END AS KPI10_STATUS 
        //                            FROM QA_Change
        //                            WHERE 1 = 1";
        //        if (param.DateFrom != null)
        //        {
        //            innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
        //            args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
        //        }
        //        if (param.DateTo != null)
        //        {
        //            innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
        //            args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
        //        }
        //        string Sql = innerSql;
        //        //string Sql = $@"SELECT DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA) as Data_Apertura, 
        //        //                       Tipo, GGDURATA, COUNT(*) as cnt FROM ({innerSql}) TMP";
        //        // Sql += @" GROUP BY DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA), Tipo, GGDURATA";
        //        try
        //        {
        //            using (IDatabase db = Connection)
        //            {
        //                rows = db.Query<ModelKPI10>(Sql, args.ToArray()).ToList();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.LogError(string.Format("GetKPI10 - ERRORE {0} {1}", e.Message, e.Source));
        //            throw;
        //        }
        //    }
        //    return DataSourceLoader.Load(rows, loadOptions);
        //}

        //[HttpGet]
        //public object GetKPI31(DataSourceLoadOptions loadOptions, string jsonParams)
        //{
        //    List<ModelKPI31> rows = new List<ModelKPI31>();
        //    List<object> args = new List<object>();

        //    JsonParamsGetKPI param = null;
        //    if (!string.IsNullOrEmpty(jsonParams))
        //    {
        //        param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
        //    }

        //    if (param != null && param.Execute)
        //    {
        //        string innerSql = @"SELECT CAST(Data_Apertura as date) as Data_Apertura, 
        //                            Classificazione, 
        //                            Classificazione_causa_primaria as  Causa_Primaria, 
        //                            Cliente, Citt as citta,
        //                            Responsabilit_accertata as Responsabilita_accertata, 
        //                            Fornitore, Processo_VMP, Sottoprocesso_VMP, Stato as Nazione, 
        //                            COUNT(*) as cnt 
        //                            from QA_Claim 
        //                              WHERE 1 = 1";
        //        if (param.DateFrom != null)
        //        {
        //            innerSql += " AND CONVERT(date, Data_Apertura ) >= CONVERT(datetime, @dataDa, 103)";
        //            args.Add(new { dataDa = ((DateTime)param.DateFrom).ToString("dd/MM/yyyy") });
        //        }
        //        if (param.DateTo != null)
        //        {
        //            innerSql += " AND CONVERT(date, Data_Apertura ) <= CONVERT(datetime, @dataA, 103)";
        //            args.Add(new { dataA = ((DateTime)param.DateTo).ToString("dd/MM/yyyy") });
        //        }
        //        innerSql += @" GROUP by CAST(Data_Apertura as date),  Classificazione, Classificazione_causa_primaria, Cliente, Citt, Stato, 
        //                                Responsabilit_accertata , Fornitore, Processo_VMP, Sottoprocesso_VMP";
        //        string Sql = innerSql;
        //        //string Sql = $@"SELECT DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA) as Data_Apertura, 
        //        //                       Tipo, GGDURATA, COUNT(*) as cnt FROM ({innerSql}) TMP";
        //        // Sql += @" GROUP BY DATEADD(DD, -(DAY(DATA_APERTURA) - 1), DATA_APERTURA), Tipo, GGDURATA";
        //        try
        //        {
        //            using (IDatabase db = Connection)
        //            {
        //                rows = db.Query<ModelKPI31>(Sql, args.ToArray()).ToList();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.LogError(string.Format("GetKPI31 - ERRORE {0} {1}", e.Message, e.Source));
        //            throw;
        //        }
        //    }
        //    return DataSourceLoader.Load(rows, loadOptions);
        //}
        // POST: InstallBase/setInstrumentCommand/5
        //public JsonResult GetDataPost([FromBody] string jsonParams)
        public JsonResult GetDataPost([FromBody] JsonParamsGetKPI param)

        {
            if (param != null && param.Execute)
            {
                // CANCELLAZIONE RICHIESTE PRECEDENTI 
                var SqlDel = @"DELETE FROM QA_ITEMS where TmstInse < DATEADD(hour, -1, getdate())";
                try
                {
                    using (IDatabase db = Connection)
                    {
                        db.Execute(SqlDel);
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetDataPost/DELETE - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }


                // INSERIMENTO NUOVE CHIAVI 
                string idRichiesta = string.Format("{0}_{1}", DateTime.Now.AddDays(-1).ToString("yyyyMMdd_HHmmss"), User.Identity.Name);
                string sqlIns = @"INSERT INTO QA_ITEMS (idRichiesta, idItem, TmstInse) VALUES (@idRichiesta, @idItem, GetDate())";
                var newArray = param.idItems.Distinct().ToArray();
                foreach (var item in newArray)
                {
                    var args1 = new List<object>();
                    args1.Add(new { idRichiesta = idRichiesta });
                    args1.Add(new { idItem = item });
                    using (IDatabase db = Connection)
                    {
                        try
                        {
                            db.Execute(sqlIns, args1.ToArray());
                        }
                        catch (Exception e)
                        {
                            _logger.LogError(string.Format("GetDataPost/INSERT - ERRORE {0} {1}", e.Message, e.Source));
                            throw;
                        }
                    }
                }

                string sql = "";
                List<object> args = new List<object>();
                args.Add(new { IdRichiesta = idRichiesta });
                try
                {
                    // CAPA
                    if (param.category.Equals(Lookup.KPI_Category_ACTIONS)) {
                       sql = @"SELECT QALIST.* 
                                    FROM QA_Actions QALIST
                                    INNER JOIN QA_ITEMS ON IDITEM = QALIST.NUM
                                    WHERE QA_ITEMS.IDRICHIESTA = @IdRichiesta ";
                        using (IDatabase db = Connection)
                        {
                            List<ModelActionsData> rows = new List<ModelActionsData>();
                            rows = db.Query<ModelActionsData>(sql, args.ToArray()).ToList();
                            return Json(new { rows = rows });
                        }
                    }
                    // CHANGE 
                    if (param.category.Equals(Lookup.KPI_Category_CHANGE))
                    {
                        sql = @"SELECT QALIST.* 
                                    FROM  QA_CHANGE QALIST
                                    INNER JOIN QA_ITEMS ON IDITEM = QALIST.NUM
                                    WHERE QA_ITEMS.IDRICHIESTA = @IdRichiesta ";
                        using (IDatabase db = Connection)
                        {
                            List<ModelChangeControlData> rows = new List<ModelChangeControlData>();
                            rows = db.Query<ModelChangeControlData>(sql, args.ToArray()).ToList();
                            return Json(new { rows = rows });
                        }
                    }
                    // CLAIM
                    if (param.category.Equals(Lookup.KPI_Category_CLAIM))
                    {
                        sql = @"SELECT QALIST.* 
                                    FROM  QA_Claim QALIST
                                    INNER JOIN QA_ITEMS ON IDITEM = QALIST.NUM
                                    WHERE QA_ITEMS.IDRICHIESTA = @IdRichiesta ";
                        using (IDatabase db = Connection)
                        {
                            List<ModelClaimData> rows = new List<ModelClaimData>();
                            rows = db.Query<ModelClaimData>(sql, args.ToArray()).ToList();
                            return Json(new { rows = rows });
                        }
                    }
                    // UNCOMPLIANCE
                    if (param.category.Equals(Lookup.KPI_Category_UNCOMPLIANCE))
                    {
                        sql = @"SELECT QALIST.* 
                                    FROM  QA_UNCOMPLIANCE QALIST
                                    INNER JOIN QA_ITEMS ON IDITEM = QALIST.NUM
                                    WHERE QA_ITEMS.IDRICHIESTA = @IdRichiesta ";
                        using (IDatabase db = Connection)
                        {
                            List<ModelUncomplianceData> rows = new List<ModelUncomplianceData>();
                            rows = db.Query<ModelUncomplianceData>(sql, args.ToArray()).ToList();
                            return Json(new { rows = rows });
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("GetDataPost - ERRORE {0} {1}", e.Message, e.Source));
                    throw;
                }
            }
            return Json(new { rows = new List<object>() });
        }
        //[HttpGet]
        //public object GetData(DataSourceLoadOptions loadOptions, string jsonParams)
        //{
        //    List<ModelKPI35> rows = new List<ModelKPI35>();
        //    List<object> args = new List<object>();

        //    JsonParamsGetKPI param = null;
        //    if (!string.IsNullOrEmpty(jsonParams))
        //    {
        //        param = JsonConvert.DeserializeObject<JsonParamsGetKPI>(jsonParams);
        //    }
        //    if (param != null && param.Execute)
        //    {
        //        string innerSql = @"SELECT NUM AS KPI_NUM,  
        //                        CAST(DATA_APERTURA AS DATE) AS OpeningDate, 
        //               COALESCE(CAST(Data_chiusura AS DATE), CAST(getDate() as DATE)) AS ClosingDate,
        //               DATEDIFF(DAY, CAST(DATA_APERTURA AS DATE), COALESCE(CAST(Data_chiusura AS DATE), CAST(getDate() as DATE))) AS OpeningDays, 
        //                        CLASSIFICAZIONE AS Classification
        //                FROM QA_Claim";
        //        string Sql = innerSql;
        //        try
        //        {
        //            using (IDatabase db = Connection)
        //            {
        //                rows = db.Query<ModelKPI35>(Sql, args.ToArray()).ToList();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            _logger.LogError(string.Format("GetData - ERRORE {0} {1}", e.Message, e.Source));
        //            throw;
        //        }
        //    }
        //    return DataSourceLoader.Load(rows, loadOptions);
        //}
    }
}



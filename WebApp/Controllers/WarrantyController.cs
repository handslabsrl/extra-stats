﻿using System.Collections.Generic;
using System.Linq;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApp.Data;
using Newtonsoft.Json;
using WebApp.Services;
using WebApp.Repository;
using WebApp.Models;
using WebApp.Classes;
using System.Threading.Tasks;
using WebApp.Entity;
using System;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.IO;
using OfficeOpenXml;
using iTextSharp.text.pdf;
using System.Collections;
using OfficeOpenXml.Style;
using System.Drawing;

namespace ELITeBoard.Controllers
{

    [Authorize(Roles = "Warranty,Warranty Administrator,Warranty BackOffice")]
    public class WarrantyController : Controller
    {
        private readonly WebAppDbContext _context;
        private readonly ILogger _logger;
        private readonly IEmailSender _emailSender;

        public WarrantyController(WebAppDbContext context, ILogger<WarrantyController> logger, IEmailSender emailSender)
        {
            _context = context;
            _logger = logger;
            _emailSender = emailSender;
        }


        public IActionResult WSPIndex()
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex, Lookup.glb_sessionkey_WSPRequests_Index);

            WSPIndexDTO dto = new WSPIndexDTO();

            // Warranty Status
            dto.listInstrumentFilters = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_InstrumentFilters)).OrderBy(x => x.Sequence)
                                .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();

            return View(dto);
        }

        public IActionResult WSPManage()
        {
            HttpContext.Session.SetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex, Lookup.glb_sessionkey_WSPRequests_Manage);

            WSPManageDTO dto = new WSPManageDTO();

            // Warranty Status
            dto.listWarrantyStatus = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_WarrantyStatus)).OrderBy(x => x.Sequence)
                                .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listWarrantyStatus.Add(new LookUpString { id = Lookup.Code_WSP_NONE_id, descrizione = Lookup.Code_WSP_NONE_desc });
            dto.listWarrantyStatus.Add(new LookUpString { id = Lookup.Code_WSP_ANY_id, descrizione = Lookup.Code_WSP_ANY_desc });
            // Exchange Request
            dto.listExchangeRequest = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_ExchangeRequest)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listExchangeRequest.Add(new LookUpString { id = Lookup.Code_WSP_NONE_id, descrizione = Lookup.Code_WSP_NONE_desc });
            dto.listExchangeRequest.Add(new LookUpString { id = Lookup.Code_WSP_ANY_id, descrizione = Lookup.Code_WSP_ANY_desc });
            // Compliant/Conformità -> Shipment of the new part from supplier 
            dto.listCompliant = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Compliant)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            //// ShowClosed
            //dto.listShowClosed = new List<LookUpString>{
            //            new LookUpString() { id = Lookup.glb_YES, descrizione = "YES" },
            //        };
            // WspAction
            dto.listWSPAction = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Action)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listWSPAction.Add(new LookUpString { id = Lookup.Code_WSP_NONE_id, descrizione = Lookup.Code_WSP_NONE_desc });
            dto.listWSPAction.Add(new LookUpString { id = Lookup.Code_WSP_ANY_id, descrizione = Lookup.Code_WSP_ANY_desc });
            // Original Part
            dto.listOriginalPart = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_OriginalPart)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();

            return View(dto);
        }


        public ActionResult GetInstruments4Warranty(DataSourceLoadOptions loadOptions, string WSPFilter = "")
        {
            string UserIdCfg = (new UsersBO()).GetUserId(User.Identity.Name);
            List<InstrumentModel> instruments = (new InstrumentsBO()).GetInstruments(UserIdCfg: UserIdCfg, 
                                                                                     ExcludeDeleted: true, 
                                                                                     ExcludeDismiss: true, 
                                                                                     CountWSPRequests: true, 
                                                                                     WSPFilter: WSPFilter);
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(instruments, loadOptions)), "application/json");
        }

        [HttpGet]
        public object GetInstrumentRequests4WSPIndex(DataSourceLoadOptions loadOptions, string SerialNumber)
        {
            var data = (new WarrantyBO()).GetInstrumentRequests4WSPIndex(SerialNumber: SerialNumber);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetRequests(DataSourceLoadOptions loadOptions, string WarrantyStatus = "", string ExchangeRequest = "",
                                                                     string Compliant = "", bool ShowClosed = false, string WSPAction = "")
        {
            var data = (new WarrantyBO()).getRequests(WarrantyStatus: WarrantyStatus, ExchangeRequest: ExchangeRequest, Compliant: Compliant, 
                                                      ShowClosed: ShowClosed, WSPAction: WSPAction);
            return DataSourceLoader.Load(data, loadOptions);
        }
        [HttpGet]
        public object GetRequestSteps(DataSourceLoadOptions loadOptions, int WSPRequestId)
        {
            var data = (new WarrantyBO()).GetRequestSteps(WSPRequestId: WSPRequestId);
            return DataSourceLoader.Load(data, loadOptions);
        }
        // GET: Training/TrainingLog/5
        public ActionResult RequestEvents(int WSPRequestId)
        {
            IEnumerable<WSPEvent> dati = _context.WSPEvents.Where(t => t.WSPRequestId == WSPRequestId)
                                                           .OrderBy(t => t.WSPEventId).ToList();
            return PartialView("_RequestEvents", dati);
        }
        public JsonResult RequestEventsJSON(int WSPRequestId, string Operation = "")
        {
            IEnumerable<WSPEvent> dati = _context.WSPEvents.Where(t => t.WSPRequestId == WSPRequestId && (string.IsNullOrWhiteSpace(Operation) || t.Operation.Equals(Operation)))
                                                           .OrderBy(t => t.WSPEventId).ToList();
            if (dati == null)
            {
                return Json("");
            }
            return Json(dati);
        }


        // GET: Warranty/WSPEditRequest
        [HttpGet]
        public async Task<IActionResult> WSPEditRequest(int WSPRequestId = 0, string SerialNumber = "", string sessionCallerKey = "")
        {
            if (!string.IsNullOrWhiteSpace(sessionCallerKey))
            {
                HttpContext.Session.SetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex, sessionCallerKey);
            }

            if (WSPRequestId == 0) {
                if (string.IsNullOrWhiteSpace(SerialNumber))
                {
                    return NotFound("SerialNumber/RequestId missing!!!");
                }
                WSPRequest newReq = new WSPRequest()
                {
                    SerialNumber = SerialNumber,
                    Code = Lookup.WSPRequest_Code_NEW,
                    Status = Lookup.WSPRequest_Status_DRAFT,
                    NumberOfParts = 1,
                    TmstIssue = DateTime.Now,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.WSPRequests.Add(newReq);
                _context.SaveChanges();
                WSPRequestId = newReq.WSPRequestId;
            }

            // preparazione DTO
            WSPRequest request = _context.WSPRequests.Find(WSPRequestId);
            WSPEditRequestDTO dto = new WSPEditRequestDTO();
            PropertyCopier<WSPRequest, WSPEditRequestDTO>.Copy(request, dto); // Copio proprietà con stesso nome

            dto.instrument = (new InstrumentsBO()).GetInstrument(SerialNumber: request.SerialNumber);
            dto.UserInse_Name = (new UsersBO()).GetFullName(dto.UserInse);

            dto.Management = (sessionCallerKey.Equals(Lookup.glb_sessionkey_WSPRequests_Manage));
            dto.listWarrantyStatus = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_WarrantyStatus)).OrderBy(x => x.Sequence)
                                .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listExchangeRequest = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_ExchangeRequest)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listCompliant = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Compliant)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listWSPAction = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Action)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listClosingOptions = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_ClosingOptions) && x.Sequence >= 0).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.CodArtProduct = (new WarrantyBO()).GetSparePart(dto.CodArt);
            dto.listOriginalPart = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_OriginalPart)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.sessionCallerKey = sessionCallerKey;

            dto.OutOfWarranty = false;
            if (dto.instrument.Warranty_To != null && dto.TmstIssue != null)
            {
                dto.OutOfWarranty = dto.instrument.Warranty_To < dto.TmstIssue;
            }

            return View(dto);
        }
        // POST: Warranty/WSPEditRequest
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("WSPEditRequest")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> WSPEditRequestPost([Bind("WSPRequestId,TmstIssue,CodArt,NumberOfParts,Notes,Decontamined,LogFileId,TicketId," +
                                                                  "OriginalPart, POReference," + 
                                                                  "WarrantyStatus,WarrantyStatusNote," +
                                                                  "ExchangeRequest,ExchangeRequestNote,Compliant,CompliantNote,WspAction,WspActionNote," +
                                                                  "CarrierNote,ShippingNote,InvoiceNUmber,InvoiceDate,ClosingOption," +
                                                                  "EmailDLListId,EmailSubject,EmailBody,EmailCCPurchaseDepartment,EmailAttachSummary,sessionCallerKey")] WSPEditRequestDTO data)
        {
            bool bNewRequest = false;
            bool bSetWarrantyStatus = false;
            bool SetExchangeRequest = false;
            bool SetCompliance = false;  // Shipment of the new part from supplier 
            bool SetAction = false;
            bool goBackIndex = false;
            bool SendEngagementEmail = false;
            EmailTemplate EngagementEmail = new EmailTemplate();

            Lookup.Request_NotificationType notificationType = Lookup.Request_NotificationType.None;
            string notificationCC = "";

            WSPRequest rec2Update = _context.WSPRequests.Find(data.WSPRequestId);
            if (rec2Update == null)
            {
                return NotFound();
            }

            if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_DRAFT))
            {
                int maxProgCode = 0;
                WSPRequest lastCode = _context.WSPRequests.Where(c => c.Code.StartsWith("W")).OrderByDescending(c => c.Code).FirstOrDefault();
                if (lastCode != null)
                {
                    maxProgCode = Int32.Parse(lastCode.Code.Substring(2));
                }
                rec2Update.Code = String.Format("W{0:D9}", ++maxProgCode);
                rec2Update.Status = Lookup.WSPRequest_Status_PENDING;
                rec2Update.TmstInse = DateTime.Now;
                bNewRequest = true;
            }

            try
            {
                if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_PENDING) || rec2Update.Status.Equals(Lookup.WSPRequest_Status_PROCESSING))
                {
                    rec2Update.CodArt = data.CodArt;
                    rec2Update.NumberOfParts = data.NumberOfParts;
                    rec2Update.TmstIssue = data.TmstIssue;
                    rec2Update.Notes = data.Notes;
                    rec2Update.Decontamined = data.Decontamined;
                    rec2Update.LogFileId = data.LogFileId;
                    rec2Update.TicketId = data.TicketId;
                    rec2Update.OriginalPart = data.OriginalPart;
                    rec2Update.POReference = string.IsNullOrWhiteSpace(data.OriginalPart) ? null : ((data.OriginalPart.Equals(Lookup.Code_WSP_OriginalPart_NO) ? data.POReference : null));
                    rec2Update.UserLastUpd = User.Identity.Name;
                    rec2Update.TmstLastUpd = DateTime.Now;
                    if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_PENDING))
                    {
                        goBackIndex = true;
                        rec2Update.WarrantyStatus = data.WarrantyStatus;
                        rec2Update.WarrantyStatusNote = string.IsNullOrWhiteSpace(rec2Update.WarrantyStatus) ? null : data.WarrantyStatusNote;
                        if (!string.IsNullOrWhiteSpace(data.WarrantyStatus)){
                            bSetWarrantyStatus = true;
                            if (data.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_WARRANTY)) 
                            {
                                rec2Update.Status = Lookup.WSPRequest_Status_CLOSED;
                                rec2Update.ClosingOption = Lookup.Code_WSP_ClosingOptions_NO_WARRANTY;
                            } 
                            else if (data.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT))
                            {
                                rec2Update.Status = Lookup.WSPRequest_Status_CLOSED;
                                rec2Update.ClosingOption = Lookup.Code_WSP_ClosingOptions_NO_REPLACEMENT;
                            } else
                            {
                                rec2Update.Status = Lookup.WSPRequest_Status_PROCESSING;
                                //goBackIndex = false;
                            }
                        }
                    }
                    if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_PROCESSING))
                    {
                        SetExchangeRequest = String.Compare(rec2Update.ExchangeRequest, data.ExchangeRequest) != 0;
                        SetCompliance= String.Compare(rec2Update.Compliant, data.Compliant) != 0;
                        SetAction = String.Compare(rec2Update.WspAction, data.WspAction) != 0;
                        // Richiesta SOSTITUZIONE -> se 'RICHIESTA' salvo tutti gli altri campi, altrimenti li ripulisco perchè non sono digitabili
                        rec2Update.ExchangeRequest = data.ExchangeRequest;
                        rec2Update.ExchangeRequestNote = string.IsNullOrWhiteSpace(rec2Update.ExchangeRequest) ? null : data.ExchangeRequestNote;
                        if (!string.IsNullOrWhiteSpace(rec2Update.ExchangeRequest) && rec2Update.ExchangeRequest == Lookup.Code_WSP_ExchangeRequest_RequestToTheProvider) {
                            rec2Update.WspAction = data.WspAction;
                            rec2Update.WspActionNote = string.IsNullOrWhiteSpace(rec2Update.WspAction) ? null : data.WspActionNote;
                            // ACTION -> se 'SEND' salvo tutti gli altri campi, altrimenti li ripulisco perchè non sono digitabili
                            if (!string.IsNullOrWhiteSpace(rec2Update.WspAction) && rec2Update.WspAction == Lookup.Code_WSP_Action_Send)
                            {
                                rec2Update.CarrierNote = data.CarrierNote;
                                rec2Update.ShippingNote = data.ShippingNote;
                            } else
                            {
                                rec2Update.CarrierNote = null;
                                rec2Update.ShippingNote = null;
                            }
                            // COMPIANT/PART REPLACEMENT -> se 'YES' salvo tutti gli altri campi, altrimenti li ripulisco perchè non sono digitabili
                            rec2Update.Compliant = data.Compliant;
                            rec2Update.CompliantNote= string.IsNullOrWhiteSpace(rec2Update.Compliant) ? null : data.CompliantNote;
                            if (!string.IsNullOrWhiteSpace(rec2Update.Compliant) && rec2Update.Compliant == Lookup.Code_WSP_Compliant_YES)
                            {
                                rec2Update.InvoiceNUmber = data.InvoiceNUmber;
                                rec2Update.InvoiceDate = data.InvoiceDate;
                            } else
                            {
                                rec2Update.InvoiceNUmber = null;
                                rec2Update.InvoiceDate = null;
                            }
                            // Invio Email 
                            if (data.EmailDLListId != null)
                            {
                                EngagementEmail.To = (new UtilBO()).GetDLEmails(DLListId: (int)data.EmailDLListId);
                                if (data.EmailCCPurchaseDepartment)
                                {
                                    EngagementEmail.CC = (new UtilBO()).GetDLEmails(DLType: Lookup.Code_DL_ListType_PurchaseDepartments);
                                }
                                if (data.EmailAttachSummary )
                                {
                                    string summaryPathName = CreateWSPSummary(data.WSPRequestId);
                                    if (!string.IsNullOrWhiteSpace(summaryPathName))
                                    {
                                        string[] attach = null;
                                        Array.Resize(ref attach, 1);
                                        attach[0] = summaryPathName; 
                                        EngagementEmail.attach = attach;
                                    }
                                }
                                EngagementEmail.Subject = data.EmailSubject;
                                EngagementEmail.Body = data.EmailBody;
                                SendEngagementEmail = (!String.IsNullOrWhiteSpace(EngagementEmail.To));
                            }

                        } else
                        {
                            rec2Update.Compliant = null;
                            rec2Update.CompliantNote = null;
                            rec2Update.WspAction = null;
                            rec2Update.WspActionNote = null;
                            rec2Update.CarrierNote = null;
                            rec2Update.ShippingNote = null;
                            rec2Update.InvoiceNUmber = null;
                            rec2Update.InvoiceDate = null;
                            if (!string.IsNullOrWhiteSpace(rec2Update.ExchangeRequest) && rec2Update.ExchangeRequest == Lookup.Code_WSP_ExchangeRequest_Unnecessary)
                            {
                                rec2Update.Status = Lookup.WSPRequest_Status_CLOSED;
                                rec2Update.ClosingOption = Lookup.Code_WSP_ClosingOptions_REPLACEMENT_NOT_REQUESTED;
                                notificationType = Lookup.Request_NotificationType.CloseRequest;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(data.ClosingOption))
                        {
                            rec2Update.Status = Lookup.WSPRequest_Status_CLOSED;
                            rec2Update.ClosingOption = data.ClosingOption;
                            notificationType = Lookup.Request_NotificationType.CloseRequest;
                        }
                    }
                    _context.WSPRequests.Update(rec2Update);
                    await _context.SaveChangesAsync();

                    if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_CLOSED))
                    {
                        goBackIndex = true;
                    }
                }
                if (bNewRequest)
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = "Create new request",
                        Operation = Lookup.WSPEvent_Operation_CREATE,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();

                    notificationType = Lookup.Request_NotificationType.NewRequest;

                    TempData["MsgToNotify"] = string.Format("Create new request {0}", rec2Update.Code);
                    goBackIndex = true;
                }
                if (bSetWarrantyStatus)
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Set Warranty Status: {0}", (new UtilBO()).GetCodeDescription(data.WarrantyStatus)),
                        Operation = Lookup.WSPEvent_Operation_SetWarrantyStatus,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();

                    notificationType = Lookup.Request_NotificationType.SetWarrantyStatus;
                    if (data.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_WITH_REPLACEMENT) || 
                        data.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT))
                    {
                        notificationCC = (new UtilBO()).GetDLEmails(DLType: Lookup.Code_DL_ListType_OrderDepartments);
                    }
                }
                if (SetExchangeRequest)
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Set Exchange Request: {0}", (new UtilBO()).GetCodeDescription(data.ExchangeRequest)),
                        Operation = Lookup.WSPEvent_Operation_SetExchangeRequest,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();
                }
                if (SetCompliance)
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Shipment of the new part from supplier: {0}", (new UtilBO()).GetCodeDescription(data.Compliant)),
                        Operation = Lookup.WSPEvent_Operation_SetCompliance,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();
                }
                if (SetAction)
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Set Action: {0}", (new UtilBO()).GetCodeDescription(data.WspAction)),
                        Operation = Lookup.WSPEvent_Operation_SetAction,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();
                }
                if (SendEngagementEmail)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationWSP(data.WSPRequestId, 
                                                                                     NotificationType: Lookup.Request_NotificationType.EngagementSupplier, 
                                                                                     userName: User.Identity.Name, 
                                                                                     extTo: EngagementEmail.To, 
                                                                                     extCC: EngagementEmail.CC, 
                                                                                     title: EngagementEmail.Subject, 
                                                                                     note: EngagementEmail.Body, 
                                                                                     attachments: EngagementEmail.attach, 
                                                                                     deleteAttachments: true);
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Operation = Lookup.WSPEvent_Operation_EngagementEmail
                    };
                    evento.Note = string.Format("[Engagement Email] To: {0}", EngagementEmail.To);
                    if (!string.IsNullOrWhiteSpace(EngagementEmail.CC))
                    {
                        evento.Note += string.Format(" / CC: {0}", EngagementEmail.CC);
                    }
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();
                }
                if (!string.IsNullOrWhiteSpace(data.ClosingOption))
                {
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = rec2Update.WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Close Request - {0}", (new UtilBO()).GetCodeDescription(data.ClosingOption)),
                        Operation = Lookup.WSPEvent_Operation_CLOSED,
                    };
                    _context.WSPEvents.Add(evento);
                    await _context.SaveChangesAsync();
                    goBackIndex = true; // in ogni caso, quando chiudo, torno sull'elenco 
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            if (notificationType != Lookup.Request_NotificationType.None)
            {
                bool bSendEmail = true;
                // per l'utente, le richieste in PROCESSING e IN_WARRANTY_NO_REPLACEMENT, oppure COMPLIANT = YES, sono già considerate CHIUSE, 
                // quindi non invio la email 
                if (notificationType == Lookup.Request_NotificationType.CloseRequest) {
                    if ((!string.IsNullOrWhiteSpace(data.WarrantyStatus) && data.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT)) ||
                        (!string.IsNullOrWhiteSpace(data.Compliant) && data.Compliant.Equals(Lookup.Code_WSP_Compliant_YES)))
                    {
                        bSendEmail = false;
                    }
                }
                if (bSendEmail)
                {
                    (new NotificationBO(_context, _emailSender, _logger)).SendNotificationWSP(data.WSPRequestId, 
                                                                                     NotificationType: notificationType, 
                                                                                     userName: User.Identity.Name, 
                                                                                     extCC: notificationCC);
                }                           
            }


            goBackIndex = true;     // per ora torna SEMPRE su index !!! 
            if (goBackIndex)
            {
                return RedirectToAction("goBackToIndex", new { sessionCallerKey = data.sessionCallerKey } );
            }
            //WSPEditRequestDTO dto = FillWSPEditRequestDTO(data.WSPRequestId, sessionCallerKey);
            return RedirectToAction("WSPEditRequest", new { WSPRequestId = data.WSPRequestId, sessionCallerKey = data.sessionCallerKey });
        }

        private string CreateWSPSummary(int WSPRequestId)
        {
            try
            {
                WSPRequestDTO request = (new WarrantyBO()).getRequests(WSPRequestId: WSPRequestId).FirstOrDefault();
                if (request == null)
                {
                    _logger.LogWarning(string.Format("Request {0} not found", WSPRequestId));
                    return null;
                }

                // Copia template 
                string templateFullPathName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format("Attachments\\WSPRequest\\WSPSummary_Template.pdf"));
                string FullPathName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format("Attachments\\WSPRequest\\TEMP\\WPRSummary_{0}.pdf", request.Code));

                if (!System.IO.File.Exists(templateFullPathName))
                {
                    _logger.LogWarning(string.Format("Template {0} not found", templateFullPathName));
                    return null;
                }
                if (!Directory.Exists(Path.GetDirectoryName(FullPathName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(FullPathName));
                }
                if (System.IO.File.Exists(FullPathName))
                {
                    System.IO.File.Delete(FullPathName);
                }
                System.IO.File.Copy(templateFullPathName, FullPathName, true);

                WSPSummaryDTO dati = new WSPSummaryDTO()
                {
                    Date_Of_Issue = DateTime.Now.ToString("dd/MM/yyyy"),
                    Warranty_Ref_No = request.Code,
                    Country = request.SiteCountry,
                    Instrument_sn = request.SerialNumber,
                    Invoice_Date = request.WarrantyFromDeco,
                    PNo = request.CodArt,
                    Description = request.CodArtDeco,
                    Qty = request.NumberOfParts.ToString(),
                    Notification_date_to_EGSPA = request.DataInseDeco,
                    End_of_Warranty_period = request.WarrantyToDeco,
                    Warranty = request.WarrantyDeco,
                    Defective_ReportRow1 = request.Notes
                    //  UserName =  (new UsersBO()).GetFullName(User.Identity.Name)
                };


                using (Stream pdfInputStream = new FileStream(path: templateFullPathName, mode: FileMode.Open))
                using (Stream resultPDFOutputStream = new FileStream(path: FullPathName, mode: FileMode.Create))
                using (Stream resultPDFStream = FillFormSummary(pdfInputStream, dati))
                {
                    // set the position of the stream to 0 to avoid corrupted PDF. 
                    resultPDFStream.Position = 0;
                    //return File(resultPDFOutputStream, "application/octet-stream", string.Format("RGA_{0}.pdf", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                    resultPDFStream.CopyTo(resultPDFOutputStream);
                }

                try
                {
                    if (System.IO.File.Exists(FullPathName))
                    {
                        //var fs = System.IO.File.ReadAllBytes(FullPathName);
                        return FullPathName;
                    }
                }
                catch (Exception e)
                {
                    _logger.LogError(string.Format("CreateWSPSummary - ERRORE {0} {1}", e.Message, e.Source));
                    return null;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(string.Format("CreateWSPSummary - ERRORE {0} {1}", e.Message, e.Source));
                return null;
            }

            return null;
        }

        //private string CreateWSPSummaryXLSX(int WSPRequestId)
        //{
        //    string FullPathName = "";
        //    try
        //    {
        //        WSPRequestDTO request = (new WarrantyBO()).getRequests(WSPRequestId: WSPRequestId).FirstOrDefault();
        //        if (request == null)
        //        {
        //            _logger.LogWarning(string.Format("Request {0} not found", WSPRequestId));
        //            return null;
        //        }

        //        // Copia template 
        //        string templateFullPathName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format("Attachments\\WSPRequest\\WSPSummary_Template.xlsx"));
        //        FullPathName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), string.Format("Attachments\\WSPRequest\\TEMP\\WPRSummary_{0}.xlsx", request.Code));

        //        if (!System.IO.File.Exists(templateFullPathName))
        //        {
        //            _logger.LogWarning(string.Format("Template {0} not found", templateFullPathName));
        //            return null;
        //        }
        //        if (!Directory.Exists(Path.GetDirectoryName(FullPathName)))
        //        {
        //            Directory.CreateDirectory(Path.GetDirectoryName(FullPathName));
        //        }
        //        System.IO.File.Copy(templateFullPathName, FullPathName, true);

        //        var data2Replace = new[]
        //            {  new { bookmark = "{Date}", value = DateTime.Now.ToString("dd/MM/yyyy")
        //            }, new { bookmark = "{Code}", value = request.Code
        //            }, new { bookmark = "{Country}", value = request.SiteCountry,
        //            }, new { bookmark = "{SerialNumber}", value = request.SerialNumber
        //            }, new { bookmark = "{WarrantyFrom}", value = request.WarrantyFromDeco
        //            }, new { bookmark = "{PartNumber}", value = request.CodArt
        //            }, new { bookmark = "{DescriptionPartNumber}", value = request.CodArtDeco
        //            }, new { bookmark = "{NumberOfParts}", value = request.NumberOfParts.ToString()
        //            }, new { bookmark = "{DataInse}", value = request.DataInseDeco
        //            }, new { bookmark = "{WarrantyTo}", value = request.WarrantyToDeco
        //            }, new { bookmark = "{Warranty}", value = request.WarrantyDeco
        //            }, new { bookmark = "{Description}", value = request.Notes
        //            }, new { bookmark = "{UserName}", value = (new UsersBO()).GetFullName(User.Identity.Name)
        //            }
        //        };

        //        using (var excel = new ExcelPackage(new System.IO.FileInfo(FullPathName)))
        //        {
        //            ExcelWorksheet ws = excel.Workbook.Worksheets[1];
        //            foreach (var item in data2Replace)
        //            {
        //                string valore = item.value ?? "";
        //                for (int row = 1; row <= 100; row++)
        //                {
        //                    for (int col = 1; col <= 20; col++)
        //                    {
        //                        if (ws.Cells[row, col].Text.Equals(item.bookmark))
        //                        {
        //                            ws.Cells[row, col].Value = valore;
        //                        }
        //                    }
        //                }
        //            }
        //            excel.Save();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        _logger.LogError(string.Format("CreateWSPSummary - ERRORE {0} {1}", e.Message, e.Source));
        //        return null;
        //    }

        //    return FullPathName;
        //}

        // GET: Warranty/WSPDetailRequest
        public async Task<IActionResult> WSPDetailRequest(int WSPRequestId, string sessionCallerKey = "")
        {
            if (!string.IsNullOrWhiteSpace(sessionCallerKey))
            {
                HttpContext.Session.SetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex, sessionCallerKey);
            }

            WSPRequest request = _context.WSPRequests.Find(WSPRequestId);
            WSPEditRequestDTO dto = new WSPEditRequestDTO();
            PropertyCopier<WSPRequest, WSPEditRequestDTO>.Copy(request, dto); // Copio proprietà con stesso nome
            dto.instrument = (new InstrumentsBO()).GetInstrument(SerialNumber: request.SerialNumber);
            dto.UserInse_Name = (new UsersBO()).GetFullName(dto.UserInse);
            dto.readOnly = true;

            var indexName = HttpContext.Session.GetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex);
            if (!string.IsNullOrWhiteSpace(indexName))
            {
                dto.Management = (indexName.Equals(Lookup.glb_sessionkey_WSPRequests_Manage));
            }
            dto.listWarrantyStatus = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_WarrantyStatus)).OrderBy(x => x.Sequence)
                                .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listExchangeRequest = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_ExchangeRequest)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listCompliant = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Compliant)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.listWSPAction = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_Action)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.CodArtProduct = (new WarrantyBO()).GetSparePart(dto.CodArt);
            dto.listOriginalPart = _context.Codes.Where(x => x.GroupId.Equals(Lookup.Code_WSP_OriginalPart)).OrderBy(x => x.Sequence)
                    .Select(x => new LookUpString { id = x.CodeId, descrizione = x.Description }).ToList();
            dto.sessionCallerKey = sessionCallerKey;

            dto.OutOfWarranty = false;
            if (dto.instrument.Warranty_To != null && dto.TmstIssue != null)
            {
                dto.OutOfWarranty = dto.instrument.Warranty_To < dto.TmstIssue;
            }

            return View("WSPEditRequest", dto);
        }
        // GET: Warranty/goBackToIndex/5
        public async Task<IActionResult> goBackToIndex(string sessionCallerKey = "")
        {
            var indexName = sessionCallerKey;
            if (string.IsNullOrWhiteSpace(indexName))
            {
                indexName = HttpContext.Session.GetString(Lookup.glb_sessionkey_WSPRequests_LastActionIndex);
            }
            if (!string.IsNullOrWhiteSpace(indexName))
            {
                if (indexName.Equals(Lookup.glb_sessionkey_WSPRequests_Index))
                {
                    return RedirectToAction("WSPIndex");
                }
                if (indexName.Equals(Lookup.glb_sessionkey_WSPRequests_Manage))
                {
                    return RedirectToAction("WSPManage");
                }
            }
            return RedirectToAction("Index", "Home");
        }
        [HttpGet]
        public ActionResult GetSpareParts(DataSourceLoadOptions loadOptions)
        {
            List<Product> produtcs = (new WarrantyBO()).GetSpareParts();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(produtcs, loadOptions)), "application/json");
        }
        [HttpGet]
        public ActionResult GetAllSpareParts(DataSourceLoadOptions loadOptions)
        {
            List<Product> produtcs = (new WarrantyBO()).GetSpareParts(excludeOffLine: false);
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(produtcs, loadOptions)), "application/json");
        }

        [HttpGet]
        public object GetRequestAttachments(DataSourceLoadOptions loadOptions, int requestId)
        {
            var data = (new WarrantyBO()).GetRequestAttachments(requestId: requestId);
            return DataSourceLoader.Load(data, loadOptions);
        }
        
        // OpenResource: legge i dati delle risorsa e imposta la lettura
        public JsonResult GetAttachmentJSON(int id)
        {
            WSPAttachment data = _context.WSPAttachments.Find(id);
            if (data == null)
            {
                return Json("");
            }
            return Json(data);
        }
        [HttpGet]
        public IActionResult DeleteRequest(int Id)
        {
            string msg = "";
            WSPRequest rec2Update = _context.WSPRequests.Find(Id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                rec2Update.Status = Lookup.WSPRequest_Status_DELETED;
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.WSPRequests.Update(rec2Update);
                WSPEvent evento = new WSPEvent()
                {
                    Note = "Delete Request",
                    WSPRequestId = rec2Update.WSPRequestId,
                    Operation = Lookup.WSPEvent_Operation_DELETE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.WSPEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteRequest", e.Message, e.Source);
            }
            return Json(msg);
        }
        [HttpGet]
        public IActionResult RestoreRequest(int Id)
        {
            string msg = "";
            WSPRequest rec2Update = _context.WSPRequests.Find(Id);
            if (rec2Update == null)
            {
                return NotFound();
            }
            try
            {
                if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_CLOSED))
                {
                    if (rec2Update.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_WARRANTY) ||
                        rec2Update.WarrantyStatus.Equals(Lookup.Code_WSP_WarrantyStatus_NO_REPLACEMENT))
                    {
                        rec2Update.Status = Lookup.WSPRequest_Status_PENDING;
                    } else
                    {
                        rec2Update.Status = Lookup.WSPRequest_Status_PROCESSING;
                    }
                }
                else
                {
                    rec2Update.Status = Lookup.WSPRequest_Status_PENDING;
                }

                // Se ripristino in PENDING, azzero tutti i campi inseriti (tranne i riferimenti alla fattura)
                if (rec2Update.Status.Equals(Lookup.WSPRequest_Status_PENDING))
                {
                    rec2Update.WarrantyStatus = null;
                    rec2Update.WarrantyStatusNote = null;
                    rec2Update.ExchangeRequest = null;
                    rec2Update.ExchangeRequestNote = null;
                    rec2Update.Compliant = null;
                    rec2Update.CompliantNote = null;
                    rec2Update.WspAction = null;
                    rec2Update.WspActionNote = null;
                    rec2Update.CarrierNote = null;
                    rec2Update.ShippingNote = null;
                }
                rec2Update.ClosingOption = null; 
                rec2Update.TmstLastUpd = DateTime.Now;
                rec2Update.UserLastUpd = User.Identity.Name;
                _context.WSPRequests.Update(rec2Update);
                WSPEvent evento = new WSPEvent()
                {
                    Note = "Restore Request",
                    WSPRequestId = rec2Update.WSPRequestId,
                    Operation = Lookup.WSPEvent_Operation_RESTORE,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.WSPEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "RestoreRequest", e.Message, e.Source);
            }
            return Json(msg);
        }

        [HttpPost]
        public ActionResult UploadNewFile(int WSPRequestId)
        {
            try
            {
                WSPRequest request = _context.WSPRequests.Find(WSPRequestId);
                if (request == null)
                {
                    throw new Exception(string.Format("Request {0} not found", WSPRequestId));
                }
                var AttachmentsPathName = request.AttachmentsPathName;
                if (!Directory.Exists(AttachmentsPathName))
                {
                    Directory.CreateDirectory(AttachmentsPathName);
                }

                var myNewFile = Request.Form.Files["myNewFile"];
                string NewFullFileName = System.IO.Path.Combine(request.AttachmentsPathName, myNewFile.FileName);

                if (System.IO.File.Exists(NewFullFileName))
                {
                    throw new Exception(string.Format("File {0} already exists", myNewFile.FileName));
                }

                // Copia nuovo file 
                using (var fileStream = System.IO.File.Create(NewFullFileName))
                {
                    myNewFile.CopyTo(fileStream);
                }

                WSPAttachment attachment = new WSPAttachment()
                {
                    WSPRequestId = WSPRequestId,
                    Source = myNewFile.FileName,
                    Deleted = false,
                    Type = Lookup.WSPAttachType.file,
                    PhysicalFileName = myNewFile.FileName,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.WSPAttachments.Add(attachment);

                string sAzione = string.Format("Add New FILE - Name: {0}", myNewFile.FileName);
                WSPEvent evento = new WSPEvent()
                {
                    Note = sAzione,
                    WSPRequestId = WSPRequestId,
                    Operation = Lookup.WSPEvent_Operation_ADDATTACH,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.WSPEvents.Add(evento);
                _context.SaveChanges();

            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return BadRequest(string.Format("{0}", e.Message));
            }
            return new EmptyResult();
        }
        [HttpGet]
        public ActionResult AddNewLink(int WSPRequestId, string Link)
        {
            string msgErr = "";
            try
            {
                WSPRequest request = _context.WSPRequests.Find(WSPRequestId);
                if (request == null)
                {
                    throw new Exception(string.Format("Request {0} not found", WSPRequestId));
                }
                WSPAttachment attachment = new WSPAttachment()
                {
                    WSPRequestId = WSPRequestId,
                    Source = Link,
                    Deleted = false,
                    Type = Lookup.WSPAttachType.url,
                    PhysicalFileName = null,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name,
                    TmstLastUpd = DateTime.Now,
                    UserLastUpd = User.Identity.Name
                };
                _context.WSPAttachments.Add(attachment);

                string sAzione = string.Format("Add New LINK - Name: {0}", Link);
                WSPEvent evento = new WSPEvent()
                {
                    Note = sAzione,
                    WSPRequestId = WSPRequestId,
                    Operation = Lookup.WSPEvent_Operation_ADDATTACH,
                    TmstInse = DateTime.Now,
                    UserInse = User.Identity.Name
                };
                _context.WSPEvents.Add(evento);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                msgErr = string.Format("{0}", e.Message);
            }
            return Json(msgErr);
        }
        [HttpGet]
        public IActionResult DeleteAttachment(int WSPAttachmentId)
        {
            string msg = "";
            try
            {
                WSPAttachment attach2upd = _context.WSPAttachments.Include(t => t.Request).Where(t => t.WSPAttachmentId == WSPAttachmentId).FirstOrDefault();
                if (attach2upd != null)
                {
                    if (attach2upd.Type == Lookup.WSPAttachType.file)
                    {
                        string FullFileName = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, attach2upd.Source);
                        if (System.IO.File.Exists(FullFileName))
                        {
                            string FileNameDeleted = string.Format("{0}_{1}_{2}", attach2upd.Source, attach2upd.WSPAttachmentId, DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string FullFileNameDeleted = System.IO.Path.Combine(attach2upd.Request.AttachmentsPathName, FileNameDeleted);
                            System.IO.File.Move(FullFileName, FullFileNameDeleted);
                            attach2upd.Source = FileNameDeleted;
                        }
                    }
                    attach2upd.Deleted = true;
                    attach2upd.TmstLastUpd = DateTime.Now;
                    attach2upd.UserLastUpd = User.Identity.Name;
                    _context.Update(attach2upd);
                    string sAzione = string.Format("remove {2} {0} - Name: {1}", WSPAttachmentId, attach2upd.Source, attach2upd.TypeDeco);
                    WSPEvent evento = new WSPEvent()
                    {
                        Note = sAzione,
                        WSPRequestId = attach2upd.WSPRequestId,
                        Operation = Lookup.WSPEvent_Operation_REMOVEATTACH,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name
                    };
                    _context.WSPEvents.Add(evento);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "DeleteAttachment", e.Message, e.Source);
            }
            return Json(msg);
        }
        public ActionResult DownloadRGA(int id)
        {
            string fileNameError = "DownloadRGA_ERROR.txt";
            WSPRequest request = _context.WSPRequests.Find(id);
            if (request == null)
            {
                return NotFound();
            }
            InstrumentModel instrument = (new InstrumentsBO()).GetInstrument(SerialNumber: request.SerialNumber);

            string FileName = System.IO.Path.Combine(Directory.GetCurrentDirectory(), request.RGATemplateName);
            string RGAPathName = request.RGAPathName;
            if (!Directory.Exists(RGAPathName))
            {
                Directory.CreateDirectory(RGAPathName);
            }
            string FileNameOut = System.IO.Path.Combine(request.RGAPathName, "Decontamination Certificate.pdf");
            if (System.IO.File.Exists(FileNameOut))
            {
                System.IO.File.Delete(FileNameOut);
            }

            WSPInfoRGADTO dati = new WSPInfoRGADTO()
            {
                WarrantyCode = request.Code,
                InstrumentSerialNumber = request.SerialNumber,
                InstrumentType = instrument.Instrument_Model,
                DistributorName = instrument.CommercialEntity,
                QuantityOfItems = string.Format("{0}", request.NumberOfParts),
                SparePartNumber = request.CodArt,
                SparePartDescription = (new WarrantyBO()).GetSparePart(request.CodArt).Description,
                Name = (new UsersBO()).GetFullName(request.UserInse), 
                DecontaminationRequired = request.Decontamined
            };

            using (Stream pdfInputStream = new FileStream(path: FileName, mode: FileMode.Open))
            using (Stream resultPDFOutputStream = new FileStream(path: FileNameOut, mode: FileMode.Create))
            using (Stream resultPDFStream = FillFormRGA(pdfInputStream, dati))
            {
                // set the position of the stream to 0 to avoid corrupted PDF. 
                resultPDFStream.Position = 0;
                //return File(resultPDFOutputStream, "application/octet-stream", string.Format("RGA_{0}.pdf", DateTime.Now.ToString("yyyyMMdd-HHmmss")));
                resultPDFStream.CopyTo(resultPDFOutputStream);
            }

            try
            {
                if (System.IO.File.Exists(FileNameOut))
                {
                    var fs = System.IO.File.ReadAllBytes(FileNameOut);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = "DecontaminationCertificate.pdf" };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nRequest Id: {0}", request.WSPRequestId);
                //msg += String.Format("\nSource: {0}", attach.Source);
                //msg += String.Format("\nRequest Id: {0}", attach.WSPRequestId);
                //msg += String.Format("\nFileName: {0}", attach.PhysicalFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadRGA() function.");
                msg += String.Format("\nRequest Id: {0}", request.WSPRequestId);
                //msg += String.Format("\nSource: {0}", attach.Source);
                //msg += String.Format("\nMessage: {0}", e.Message);
                //msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }

            //FileInfo template = new FileInfo(FileName);
            //ExcelPackage pck = new ExcelPackage(template);
            //    var stream = new MemoryStream();
            //pck.SaveAs(stream);
            //stream.Position = 0;
            //return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
            //                     string.Format("RGA_{0}.xlsx", DateTime.Now.ToString("yyyyMMdd-HHmmss")));


            //Stream pdfStream = new FileStream(path: FileName, mode: FileMode.Open);
            //ICollection fieldNames = GetFormFields(pdfStream);
            //foreach (var fieldName in fieldNames)
            //{
            //    _output.WriteLine(fieldName.ToString());
            //}
            //Stream outStream = new MemoryStream();
            //PdfStamper pdfStamper = null;

            //PdfReader pdfReader = new PdfReader(pdfStream);
            //pdfStamper = new PdfStamper(pdfReader, outStream);
            //AcroFields form = pdfStamper.AcroFields;
            //form.SetField("Given Name Text Box", "Dario !! ");
            ////form.SetField(SampleFormFieldNames.LastName, model.LastName);
            ////form.SetField(SampleFormFieldNames.IAmAwesomeCheck, model.AwesomeCheck ? "Yes" : "Off");
            ////// set this if you want the result PDF to not be editable. 
            //pdfStamper.FormFlattening = true;
            //outStream.Position = 0;
            //pdfStream.Close();

            //Stream pdfStream = new FileStream(path: FileName, mode: FileMode.Open);
            //ICollection fieldNames = GetFormFields(pdfStream);
            //foreach (var fieldName in fieldNames)
            //{
            //    //_output.WriteLine(fieldName.ToString());
            //}
        }
        public ActionResult DownloadAttachment(int id)
        {
            string fileNameError = "DownloadAttachment_ERROR.txt";
            WSPAttachment attach = _context.WSPAttachments.Find(id);
            if (attach == null)
            {
                return NotFound();
            }
            WSPRequest request = _context.WSPRequests.Find(attach.WSPRequestId);
            if (request == null)
            {
                return NotFound();
            }

            string fullFileName = System.IO.Path.Combine(request.AttachmentsPathName, attach.Source);
            try
            {
                if (System.IO.File.Exists(fullFileName))
                {
                    var fs = System.IO.File.ReadAllBytes(fullFileName);
                    return new FileContentResult(fs, "application/octet-stream") { FileDownloadName = attach.Source };
                }
                string msg = String.Format("File not Found");
                msg += String.Format("\nFile Id: {0}", attach.WSPAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nRequest Id: {0}", attach.WSPRequestId);
                msg += String.Format("\nFileName: {0}", attach.PhysicalFileName);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
            catch (Exception e)
            {
                string msg = String.Format("Error in DownloadAttachment() function.");
                msg += String.Format("\nFile Id: {0}", attach.WSPAttachmentId);
                msg += String.Format("\nSource: {0}", attach.Source);
                msg += String.Format("\nMessage: {0}", e.Message);
                msg += String.Format("\nSource: {0}", e.Source);
                byte[] bytes = Encoding.ASCII.GetBytes(msg);
                return new FileContentResult(bytes, "application/octet-stream") { FileDownloadName = fileNameError };
            }
        }

        // ************************************************************************
        // Gestione SPARE PARTS
        // ************************************************************************
        public ActionResult ManageSpareParts()
        {
            return View();
        }
        [HttpPut]
        public IActionResult UpdateSparePart(string key, string values)
        {
            Product rec2Update = (new WarrantyBO()).GetSparePart(CodArt: key);
            try
            {
                JsonConvert.PopulateObject(values, rec2Update);
                if (!TryValidateModel(rec2Update))
                {
                    string messages = string.Join(" ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                    return BadRequest(messages);
                }
                (new WarrantyBO()).UpdateSparePart(data: rec2Update, User: User.Identity.Name);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok();
        }
        [HttpGet]
        public IActionResult GetSparePartJSON(string CodArt)
        {
            Product ret = (new WarrantyBO()).GetSparePart(CodArt);
            return Json(ret);
        }
        // GET:Trainings/GetEmailInfoJSON
        public async Task<IActionResult> GetEmailInfoJSON(int WSPRequestId, string EmailTemplateId)
        {
            EmailTemplate data = (new NotificationBO(_context, _emailSender, _logger)).GetEmailInfoWSP(WSPRequestId: WSPRequestId, 
                                                                                              templateId: EmailTemplateId, 
                                                                                              userName: User.Identity.Name);
            return Json(data);
        }
        [HttpGet]
        public ActionResult GetEngagementEmail(DataSourceLoadOptions loadOptions, int WSPRequestId)
        {
            IEnumerable<WSPEvent> dati = _context.WSPEvents.Where(t => t.WSPRequestId == WSPRequestId && t.Operation.Equals(Lookup.WSPEvent_Operation_EngagementEmail))
                                               .OrderBy(t => t.WSPEventId).ToList();
            return Content(JsonConvert.SerializeObject(DataSourceLoader.Load(dati, loadOptions)), "application/json");
        }

        //  DA SPOSTARE 
        public ICollection GetFormFields(Stream pdfStream)
        {
            PdfReader reader = null;
            try
            {
                PdfReader pdfReader = new PdfReader(pdfStream);
                AcroFields acroFields = pdfReader.AcroFields;
                return acroFields.Fields.Keys;
            }
            finally
            {
                reader?.Close();
            }
        }
        public Stream FillFormRGA(Stream inputStream, WSPInfoRGADTO model)
        {
            Stream outStream = new MemoryStream();
            PdfReader pdfReader = null;
            PdfStamper pdfStamper = null;
            Stream inStream = null;

           //var xxxx = GetFormFields(inputStream);
            try
            {
                pdfReader = new PdfReader(inputStream);
                pdfStamper = new PdfStamper(pdfReader, outStream);
                AcroFields form = pdfStamper.AcroFields;
                form.SetField("Return Authorization Number If available", model.WarrantyCode);
                form.SetField("Name SubsidiaryDistributor", model.DistributorName);
                form.SetField("Instrument Type", model.InstrumentType);
                form.SetField("Instrument Serial Number", model.InstrumentSerialNumber);
                form.SetField("Spare Part Description", model.SparePartDescription);
                form.SetField("Spare Part Number", model.SparePartNumber);
                form.SetField("Quantity of Items", model.QuantityOfItems);
                form.SetField("Name", model.Name);
                form.SetField("Testo1", model.DecontaminationRequired ? "" : "X");
                form.SetField("Testo2", model.DecontaminationRequired ? "X" : "");
                // set this if you want the result PDF to not be editable. 
                pdfStamper.FormFlattening = true;
                return outStream;
            }
            finally
            {
                pdfStamper?.Close();
                pdfReader?.Close();
                inStream?.Close();
            }
        }
        public Stream FillFormSummary(Stream inputStream, WSPSummaryDTO model)
        {
            Stream outStream = new MemoryStream();
            PdfReader pdfReader = null;
            PdfStamper pdfStamper = null;
            Stream inStream = null;

            //var xxxx = GetFormFields(inputStream);
            try
            {
                pdfReader = new PdfReader(inputStream);
                pdfStamper = new PdfStamper(pdfReader, outStream);
                AcroFields form = pdfStamper.AcroFields;
                form.SetField("Date of issue", model.Date_Of_Issue);
                form.SetField("Warranty Ref No", model.Warranty_Ref_No);
                form.SetField("Country", model.Country);
                form.SetField("Instrument sn", model.Instrument_sn);
                form.SetField("Invoice Date", model.Invoice_Date);
                form.SetField("PNo", model.PNo);
                form.SetField("Description", model.Description);
                form.SetField("Qty", model.Qty);
                form.SetField("Notification date to EGSPA", model.Notification_date_to_EGSPA);
                form.SetField("End of Warranty period", model.End_of_Warranty_period);
                form.SetField("Warranty", model.Warranty);
                form.SetField("Defective ReportRow1", model.Defective_ReportRow1);
                // set this if you want the result PDF to not be editable. 
                pdfStamper.FormFlattening = true;
                return outStream;
            }
            finally
            {
                pdfStamper?.Close();
                pdfReader?.Close();
                inStream?.Close();
            }
        }
        // GET:Warranty/GetRequestJSON
        public async Task<IActionResult> GetRequestJSON(int WSPRequestId)
        {
            WSPRequest data = _context.WSPRequests.Find(WSPRequestId);
            return Json(data);
        }
        [HttpGet]
        public IActionResult UpdateConsignmentNote(int WSPRequestId, string Note)
        {
            string msg = "";
            try
            {
                WSPRequest rec2update = _context.WSPRequests.Find(WSPRequestId);
                if (rec2update == null)
                {
                    return NotFound();
                }
                if (String.Compare(rec2update.ReturnedPartNote, Note) != 0)
                {
                    rec2update.ReturnedPartNote= (!string.IsNullOrWhiteSpace(Note) ? Note : null);
                    _context.WSPRequests.Update(rec2update);
                    WSPEvent evento = new WSPEvent()
                    {
                        WSPRequestId = WSPRequestId,
                        TmstInse = DateTime.Now,
                        UserInse = User.Identity.Name,
                        Note = string.Format("Set Consignment Note: {0}", Note),
                        Operation = Lookup.WSPEvent_Operation_SETCONSIGNMENTNOTE,
                    };
                    _context.WSPEvents.Add(evento);
                    _context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                msg = String.Format("[{0}] - {1} / {2}", "UpdateConsignmentNote", e.Message, e.Source);
            }
            return Json(msg);
        }

    }
}
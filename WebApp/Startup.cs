using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using NLog.Web;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;
using WebApp.Repository;
using System;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Identity;
//using Hangfire;
//using Hangfire.SqlServer;
using System.Collections.Generic;
//using Hangfire.Dashboard;
using WebApp.Classes;

namespace WebApp
    {
    public class Startup
        {
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }			
        /* public Startup(IHostingEnvironment env)
            {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json" , optional: false , reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json" , optional: true)
                .AddJsonFile($"appsettings_user.json" , optional: false , reloadOnChange: true);

            if (env.IsDevelopment())
                {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
                }
            //env.ConfigureNLog("nlog.config");
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            }
        public IConfigurationRoot Configuration { get; } */

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
            {
            // Add framework services.
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<WebAppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IHttpContextAccessor , HttpContextAccessor>();

            services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();
            //services.ConfigureApplicationCookie(options => options.LoginPath = "/Account/LogIn");

            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            //services.AddHangfire(configuration => configuration
            //        .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            //        .UseSimpleAssemblyNameTypeSerializer()
            //        .UseRecommendedSerializerSettings()
            //        .UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"), new SqlServerStorageOptions
            //        {
            //            CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
            //            SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
            //            QueuePollInterval = TimeSpan.Zero,
            //            UseRecommendedIsolationLevel = true,
            //            DisableGlobalLocks = true
            //        }));

            //// Add the processing server as IHostedService
            //services.AddHangfireServer();


            services.AddMvc().AddJsonOptions(options => options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.DefaultContractResolver())
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization();

            //services.AddMvc()
            //        .AddViewLocalization(
            //            LanguageViewLocationExpanderFormat.Suffix,
            //        opts => { opts.ResourcesPath = "Resources"; })
            //        .AddDataAnnotationsLocalization();
            services.AddDistributedMemoryCache();
            //services.AddSession();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
            });

            services.AddSingleton<IConfiguration>(Configuration);

            // Configure Identity
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                //options.Password.RequireDigit = false;
                //options.Password.RequiredLength = 6;
                //options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 12;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                // Lockout settings
                //options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                //options.Lockout.MaxFailedAccessAttempts = 10;
                // Cookie settings
                //options.Cookies.ApplicationCookie.ExpireTimeSpan = TimeSpan.FromDays(150);
                //options.Cookies.ApplicationCookie.LoginPath = "/Account/LogIn";
                //options.Cookies.ApplicationCookie.LogoutPath = "/Account/LogOut";
                // User settings
                options.User.RequireUniqueEmail = true;
            });

            // Add application services.
            services.AddTransient<IEmailSender , AuthMessageSender>();
            services.AddTransient<ISmsSender , AuthMessageSender>();
            // 
            //services.AddTransient<ExecutionsBO>();
            //
            // abilitazione TempData
            services.AddSingleton<ITempDataProvider, CookieTempDataProvider>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app , IHostingEnvironment env , ILoggerFactory loggerFactory)
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerfactory, IHostingEnvironment env)
        {
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddConsole(includeScopes: true)
            //             .AddDebug();
            loggerfactory.AddDebug();
            loggerfactory.AddNLog();
            env.ConfigureNLog("nlog.config");

            if (env.IsDevelopment())
                {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
                }
            else
                {
                app.UseExceptionHandler("/Home/Error");
                }
            //app.AddNLogWeb();
            app.UseStaticFiles();

            app.UseAuthentication();


            //// HANGFIRE
            //app.UseHangfireDashboard("/hangfire", new DashboardOptions
            //{
            //    Authorization = new[] { new MyAuthorizationFilter() }
            //});

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715
            // IMPORTANT: This session call MUST go before UseMvc()
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default" ,
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            }
        }

        //public class MyAuthorizationFilter : IDashboardAuthorizationFilter
        //{
        //    public bool Authorize(DashboardContext context)
        //    {
        //        bool ret = false;   
        //        var httpContext = context.GetHttpContext();
        //        // Allow all authenticated users to see the Dashboard (potentially dangerous).
        //        if (httpContext.User.Identity.IsAuthenticated)
        //        {
        //            // soluzione PROVVISORIA
        //            ret = httpContext.User.IsInRole(Lookup.Role_Admin);
        //            //if (httpContext.User.Identity.Name.ToLower().Equals("d.millone") || httpContext.User.Identity.Name.ToLower().Equals("handslab"))
        //            //{
        //            //    ret = true;
        //            //}
        //        }
        //        return ret;          
        //    }
        //}
}

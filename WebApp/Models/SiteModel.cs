﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class SiteModel
    {
        [Column("Site_Code")]
        public string SiteCode { get; set; }
        [Column("Site_Description")]
        public string SiteDescription { get; set; }
    }
}


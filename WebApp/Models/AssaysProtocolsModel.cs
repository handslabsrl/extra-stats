﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{

    public class AssaysProtocolsDetails
    {
        public string assay_name { get; set; }
        public string pathogen_name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Customer { get; set; }
        public string Site { get; set; }
        public string SerialNumber { get; set; }
        public string AssayProtocolStatus { get; set; }
        [NotMapped]
        public string ImportDate
        {
            get
            {
                string ret = "";
                if (!string.IsNullOrWhiteSpace(this.FileName)) {
                    try
                    {
                        var parts = this.FileName.Split('_');
                        DateTime dt = DateTime.ParseExact(parts[parts.Length - 1].Substring(0, 6), "ddMMyy", null);
                        ret = dt.ToString("dd/MM/yyyy");
                    }
                    catch (Exception e) {
                        // nessuna gestione
                    }
                };
                return ret;
            }
        }
        public string CommercialEntity { get; set; }
        public string CommercialStatus { get; set; }
        public string FileName { get; set; }
        [NotMapped]
        public string ImportDateSort
        {
            get
            {
                string ret = "";
                if (!string.IsNullOrWhiteSpace(this.FileName))
                {
                    try
                    {
                        var parts = this.FileName.Split('_');
                        DateTime dt = DateTime.ParseExact(parts[parts.Length - 1].Substring(0, 6), "ddMMyy", null);
                        ret = dt.ToString("yyyyMMdd");
                    }
                    catch (Exception e)
                    {
                        // nessuna gestione
                    }
                };
                return ret;
            }
        }

    }

    public class AssaysProtocolsModel
    {
        public AssaysProtocolsModel()
        {
            this.filters = new Dictionary<string, string>();
        }
        public Dictionary<string, string> filters { get; set; }
        public List<AssaysProtocolsDetails> AssaysProtocolsDetails { get; set; } 
        
        // Formattazione filtri per presentazione sopra al grafico
        public string filtersDesc
        {
            get
            {
                string ret = "";
                string parziale = "";
                string filtro = "";
                if (this.filters != null)
                {
                    foreach (var f in this.filters)
                    {
                        if (ret.Length > 0) { ret += ", "; }
                        filtro = string.Format("{0}: <b>{1}</b>", f.Key, f.Value);
                        if ((parziale.Length + filtro.Length) > 175)
                        {
                            ret += "\n";
                            parziale = "";
                        }
                        ret += filtro;
                        parziale += filtro;
                    }
                }
                return ret;
            }
        }
    }
}

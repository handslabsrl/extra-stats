﻿using NPoco;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class CityModel
    {
        [Column("Site_City")]
        public string City { get; set; }
        [Column("Site_Country")]
        public string Country { get; set; }
    }
}


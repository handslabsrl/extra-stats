﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Models
{
    public class UploadFilesModel
    {
        public string SerialNumber { get; set; }
        public string CompanyName { get; set; }
        public string Site_Description { get; set; }
        public string Site_City { get; set; }
        public string Site_Country { get; set; }
        public string CommercialEntity { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string Commercial_Status { get; set; }
        public string Instrument_Model { get; set; }
        public string Instrument_ModelType { get; set; }
        public DateTime? Installation_Date { get; set; }
        // Info file DATA
        public string id_data { get; set; }
        public string status_data { get; set; }
        public string filename_data { get; set; }
        public string tmstupload_data { get; set; }
        public string userupload_data { get; set; }
        public string tmstprocess_data { get; set; }
        public string note_data { get; set; }
        public string revExtra_data { get; set; }
        // Info file ASSAYSLIST
        public string id_assayslist { get; set; }
        public string status_assayslist { get; set; }
        public string filename_assayslist { get; set; }
        public string tmstupload_assayslist { get; set; }
        public string userupload_assayslist { get; set; }
        public string tmstprocess_assayslis { get; set; }
        public string note_assayslist { get; set; }
        public string revExtra_assayslist { get; set; }
        // Info file INSTALLBASE
        // NOTA: questi file NON sono legati a nessuno STRUMENTO !!!! 
        public string id_installbase { get; set; }
        public string status_installbase { get; set; }
        public string filename_installbase { get; set; }
        public string tmstupload_installbase { get; set; }
        public string userupload_installbase { get; set; }
        public string tmstprocess_installbase { get; set; }
        public string note_installbase { get; set; }
        public string revExtra_installbase { get; set; }

        public string Installation_DateDeco
        {
            get
            {
                string ret = "";
                if (this.Installation_Date != null)
                {
                    ret = ((DateTime)this.Installation_Date).ToString("dd/MM/yyyy");
                }
                return ret;
            }
        }
        public string Installation_DateSort
        {
            get
            {
                string ret = "";
                if (this.Installation_Date != null)
                {
                    ret = ((DateTime)this.Installation_Date).ToString("yyyyMMdd");
                }
                return ret;
            }
        }

        public string Instrument_ModelDeco
        {
            get
            {
                string ret = this.Instrument_ModelType;
                if (!string.IsNullOrWhiteSpace(this.Instrument_ModelType))
                {
                    var item = Lookup.InstrumentModelTypeFilters.Find(x => x.id.Equals(this.Instrument_ModelType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }


    }
}


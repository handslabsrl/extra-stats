﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;
using WebApp.Entity;

namespace WebApp.Models
{
    public class InstrumentModel: Instrument
    {
        public string CompanyName { get; set; }
        [Column("Site_Description")]
        public string SiteDescription { get; set; }
        public string SiteCity { get; set; }
        public string SiteCountry { get; set; }
        public string CommercialEntity { get; set; }
        public string CommercialStatus { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string CustomerCode { get; set; }
        [Column("Site_Code")]
        public string SiteCode{ get; set; }
        [Column("Id_Mac")]
        public string IdMac{ get; set; }

        public DateTime? Installation_Date { get; set; }

        public string SerialNumberInfo
        {
            get
            {
                string ret = $"{SerialNumber}";
                if (!string.IsNullOrWhiteSpace(CompanyName) || !string.IsNullOrWhiteSpace(SiteDescription)) {
                    ret += $" ({CompanyName} / {SiteDescription})";
                }
                if (!string.IsNullOrWhiteSpace(ModelTypeDeco))
                {
                    ret += $" [{ModelTypeDeco}]";
                }
                return ret;
            }
        }
        public string SerialNumberInfo2
        {
            get
            {
                string ret = $"{SerialNumber}";
                if (!string.IsNullOrWhiteSpace(Machine_Description))
                {
                    ret += $" [{Machine_Description}]";
                }
                if (!string.IsNullOrWhiteSpace(CompanyName) || !string.IsNullOrWhiteSpace(SiteDescription))
                {
                    ret += $" ({CompanyName} / {SiteDescription})";
                }
                return ret;
            }
        }
        public string SerialNumberInfo3
        {
            get
            {
                string ret = $"{SerialNumber}";
                if (!string.IsNullOrWhiteSpace(Machine_Description))
                {
                    ret += $" [{Machine_Description}]";
                }
                return ret;
            }
        }
        public string Command { get; set; }
        public DateTime? CommandDateRif { get; set; }
        public string CommandInfo
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.Command))
                {
                    return "";
                }
                string ret = this.Command;
                if (this.Command.Equals("DELETE"))
                {
                    ret = string.Format("Deleted");
                }
                if (this.Command.Equals("DISMISS") && this.CommandDateRif != null)
                {
                    ret = string.Format("Dismissed from {0}", ((DateTime)this.CommandDateRif).ToString("dd/MM/yyyy"));
                }
                if (this.Command.Equals("RESET") && this.CommandDateRif != null)
                {
                    ret = string.Format("Reset at {0}", ((DateTime)this.CommandDateRif).ToString("dd/MM/yyyy"));
                }
                return ret;
            }
        }

        public string Instrument_Model { get; set; }
        public string Machine_Description { get; set; }
        public string ModelType { get; set; }
        public DateTime? Supplier_Invoice_Date { get; set; }
        public DateTime? Warranty_From { get; set; }
        public DateTime? Warranty_To { get; set; }
        public DateTime? movement_date_invoice_asset { get; set; }
        public int? Warranty_Duration { get; set; }
        public string WarrantyType { get; set; }

        public string WarrantyFromDeco
        {
            get { return ((this.Warranty_From == null) ? "" : ((DateTime)this.Warranty_From).ToString("dd/MM/yyyy")); }
        }
        public string WarrantyFromSort
        {
            get { return ((this.Warranty_From == null) ? "" : ((DateTime)this.Warranty_From).ToString("yyyyMM")); }
        }
        public string WarrantyToDeco
        {
            get { return ((this.Warranty_To == null) ? "" : ((DateTime)this.Warranty_To).ToString("dd/MM/yyyy")); }
        }
        public string WarrantyToSort
        {
            get { return ((this.Warranty_To == null) ? "" : ((DateTime)this.Warranty_To).ToString("yyyyMMdd")); }
        }
        public bool ExpiredWarranty
        {
            get { return ((this.Warranty_To == null) ? false : ((DateTime)this.Warranty_To) < DateTime.Now); }
        }
        public string WarrantyDeco
        {
            get { return ((this.Warranty_To == null) ? "" : (ExpiredWarranty ? Lookup.glb_NO : Lookup.glb_YES)); }
        }
        public int? OpenWSPRequests { get; set; }
        public string OpenWSPRequestsDeco
        {
            get { return ((this.OpenWSPRequests == null || (int)this.OpenWSPRequests == 0) ? "" : ((int)this.OpenWSPRequests).ToString()); }
        }
        public string ModelTypeDeco
        {
            get {
                string ret = this.ModelType;
                if (!string.IsNullOrWhiteSpace(this.ModelType))
                {
                    var item = Lookup.InstrumentModelTypeFilters.Find(x => x.id.Equals(this.ModelType));
                    if (item != null)
                    {
                        ret = item.descrizione;
                    }
                }
                return ret;
            }
        }
        public string ContactPerson { get; set; }
        //public int? OwnerGroupId { get; set; }  // Referente/Proprietario strumento -> id tabella DLLists

        // MAINTENANCE
        public DateTime? TmstInseMaintenance { get; set; }
        public string  UserInseMaintenance { get; set; }
        public string UserInseMaintenance_Name { get; set; }
        public DateTime? Dt_Last_Maintenance { get; set; }
        public DateTime? Dt_Next_Maintenance { get; set; }
        public DateTime? TmstLastCalibration { get; set; }
        public DateTime? TmstNextCalibration { get; set; }
        public int? FibersNumber { get; set; }
        public bool showHistoryMaintenance { get; set; }
        public string LastMaintenanceDeco
        {
            get { return ((this.Dt_Last_Maintenance == null) ? "" : ((DateTime)this.Dt_Last_Maintenance).ToString("dd/MM/yyyy")); }
        }
        public string LastMaintenancSort
        {
            get { return ((this.Dt_Last_Maintenance == null) ? "" : ((DateTime)this.Dt_Last_Maintenance).ToString("yyyyMMdd")); }
        }
        public string NextMaintenanceDeco
        {
            get { return ((this.Dt_Next_Maintenance == null) ? "" : ((DateTime)this.Dt_Next_Maintenance).ToString("dd/MM/yyyy")); }
        }
        public string NextMaintenanceSort
        {
            get { return ((this.Dt_Next_Maintenance == null) ? "" : ((DateTime)this.Dt_Next_Maintenance).ToString("yyyyMMdd")); }
        }
        public string LastCalibrationDeco
        {
            get { return ((this.TmstLastCalibration == null) ? "" : ((DateTime)this.TmstLastCalibration).ToString("dd/MM/yyyy")); }
        }
        public string LastCalibrationSort
        {
            get { return ((this.TmstLastCalibration == null) ? "" : ((DateTime)this.TmstLastCalibration).ToString("yyyyMMdd")); }
        }
        public string NextCalibrationDeco
        {
            get { return ((this.TmstNextCalibration == null) ? "" : ((DateTime)this.TmstNextCalibration).ToString("dd/MM/yyyy")); }
        }
        public string NextCalibrationSort
        {
            get { return ((this.TmstNextCalibration == null) ? "" : ((DateTime)this.TmstNextCalibration).ToString("yyyyMMdd")); }
        }
        public int? GGNextMaintenance
        {
            get
            {
                if (this.Dt_Next_Maintenance is null)
                {
                    return null;
                }
                System.TimeSpan diff = (((DateTime)Dt_Next_Maintenance).Date).Subtract(DateTime.Now.Date);
                return diff.Days;
            }
        }
        public string ClassNextMaintenance
        {
            get {
                int? giorni = this.GGNextMaintenance;
                if (giorni is null)
                {
                    return "Missing"; // Manutenzione mancante 
                }
                if (giorni <= 0)
                {
                    return "Alert";  // Manutenzione scaduta
                }
                if (giorni < (this.Maintenance_WarningAfterGG ?? Lookup.GetInstrumentMaintenanceWarningAfterGG()))
                {
                    return "Warning";  // Manutenzione in scadenza 
                }
                return "Valid";  // Manutenzione valida 
            }
        }
        public int? GGNextCalibration
        {
            get
            {
                if (this.TmstNextCalibration is null)
                {
                    return null;
                }
                System.TimeSpan diff = (((DateTime)TmstNextCalibration).Date).Subtract(DateTime.Now.Date);

                return diff.Days;
            }
        }
        public string ClassNextCalibration
        {
            get
            {
                int? giorni = this.GGNextCalibration;
                if (giorni is null)
                {
                    return "Missing"; // Calibrazione mancante 
                }
                if (giorni <= 0)
                {
                    return "Alert";  // Calibrazione scaduta
                }
                if (giorni < (this.Calibration_WarningAfterGG ?? Lookup.GetInstrumentCalibrationWarningAfterGG()))
                {
                    return "Warning";  // Calibrazione in scadenza 
                }
                return "Valid";  // Calibrazione valida 
            }
        }
        public decimal? FibersNumberPerc
        {
            get
            {
                if (this.FibersNumber is null)
                {
                    return null;
                }
                int myFibersPack = this.Fibers_Default ?? Lookup.GetInstrumentFibersDefault();
                decimal ret = (decimal)(this.FibersNumber * 100) / myFibersPack;
                return Math.Round(ret, 2);
            }
        }
        public string ClassFibersNumberPerc
        {
            get
            {
                decimal? myPerc = this.FibersNumberPerc;
                if (myPerc is null)
                {
                    return "Missing"; // Numero Fibre Mancanti 
                }
                if (myPerc <= 0)
                {
                    return "Alert";  // Numero Fibre da sostituire 
                }
                if (myPerc <= (this.Fibers_WarningAfterPerc ?? Lookup.GetInstrumentFibersWarningAfterPerc()))
                {
                    return "Warning";  // Numero Fibre sopra soglia 
                }
                return "Valid";  // Numero Fibre valide 
            }
        }

    }

}


﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class TKSupportConfigModel
    {
        //public TKSupportConfigModel() {
        //    Instrument = "";
        //    OtherPlatform = "";
        //    Product = "";
        //    ClaimLogFile = "";
        //    AdditionalInfo = "";
        //    ClosingInfo = "";
        //    InfoContact = "";
        //    ToDoList = "";
        //    Quantity = "";
        //    SupplierEngagement = "";
        //    LocationSGAT = "";
        //    CustomerSGAT = "";
        //}

        public string ID { get; set; }
        public string Description { get; set; }
        public string ClassificationClass { get; set; }
        public bool HasComplaint { get; set; }
        public string Instrument { get; set; }
        public bool AllModelTypes { get; set; }
        public string OtherPlatform { get; set; }
        public string Product { get; set; }
        public string ClaimLogFile { get; set; }
        public string AdditionalInfo { get; set; }
        public string ClosingInfo { get; set; }
        public string InfoContact { get; set; }
        public string Quantity { get; set; }
        public string ToDoList { get; set; }
        public string SupplierEngagement { get; set; }
        public string CustomerSGAT { get; set; }
        public string LocationSGAT { get; set; }
        public string LocationSGATCodNaz { get; set; }

        public string Header(string value)
        {
            var regex = new Regex(Regex.Escape(" "));
            var newText = regex.Replace(string.Format("{0}", value), "<br>", 1);
            return newText;
            //return string.Format("{0}", value).Replace(;
        }
    }
}


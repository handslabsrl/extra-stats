﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Models
{
    public class MaintenanceModel
    {
        public DateTime? TmstInseMaintenance { get; set; }
        public string  UserInseMaintenance { get; set; }
        public string UserInseMaintenance_Name { get; set; }
        public DateTime? TmstLastCalibration { get; set; }
        public DateTime? TmstNextCalibration { get; set; }
        public int? FibersNumber { get; set; }
        public bool? FibersNewPack { get; set; }
        public string TmstInseMaintenanceDeco
        {
            get { return ((this.TmstInseMaintenance == null) ? "" : ((DateTime)this.TmstInseMaintenance).ToString("dd/MM/yyyy HH:mm:ss")); }
        }
        public string LastCalibrationDeco
        {
            get { return ((this.TmstLastCalibration == null) ? "" : ((DateTime)this.TmstLastCalibration).ToString("dd/MM/yyyy")); }
        }
        public string NextCalibrationDeco
        {
            get { return ((this.TmstNextCalibration == null) ? "" : ((DateTime)this.TmstNextCalibration).ToString("dd/MM/yyyy")); }
        }
        public string FibersNewPackDeco
        {
            get {
                if (this.FibersNewPack != null && this.FibersNewPack == true)
                {
                    return "New Pack";
                }
                return "";
            }
        }

        
    }
}


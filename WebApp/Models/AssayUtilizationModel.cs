﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Models
{
    public class AssayUtilizationCounters
    {
        public string pathogen_name { get; set; }
        public int num_test { get; set; }
    }

    public class AssayUtilizationDetails
    {
        public string pathogen_name { get; set; }
        //public string assay_type { get; set; }
        //public int total { get { return this.num_test + this.error; }}
        public int num_test_v { get; set; }
        public int num_test_i { get; set; }
        public int num_test_e { get; set; }
        public int num_test { get; set; }
        public int num_sample_v { get; set; }
        public int num_sample_i { get; set; }
        public int num_sample_e { get; set; }
        public int num_sample { get; set; }
        public int num_calibrator_v { get; set; }
        public int num_calibrator_i { get; set; }
        public int num_calibrator_e { get; set; }
        public int num_calibrator { get; set; }
        public int num_control_v { get; set; }
        public int num_control_i { get; set; }
        public int num_control_e { get; set; }
        public int num_control { get; set; }
        public int invalid { get; set; }        // sottoinsieme dei RUN
        //public int error { get; set; }
    }

    public class RunAssaysCounters    {
        public int num_test_elite { get; set; }
        public int tot_test { get; set; }
    }


    public class AssayUtilizationModel
    {
        public AssayUtilizationModel()
        {
            this.filters = new Dictionary<string, string>();
        }
        public Dictionary<string, string> filters { get; set; }
        public List<AssayUtilizationCounters> AssaysUtilization { get; set; }
        public List<AssayUtilizationDetails> AssayUtilizationDetails { get; set; }
        public int CntRunAssayElite { get; set; }
        public int TotRun { get; set; }
        public int cntInstruments { get; set; }

        // Formattazione filtri per presentazione sopra al grafico
        public string filtersDesc
        {
            get
            {
                string ret = "";
                string parziale = "";
                string filtro = "";
                if (this.filters != null)
                {
                    foreach (var f in this.filters)
                    {
                        if (ret.Length > 0) { ret += ", "; }
                        filtro = string.Format("{0}: <b>{1}</b>", f.Key, f.Value);
                        if ((parziale.Length + filtro.Length) > 175)
                        {
                            ret += "\n";
                            parziale = "";
                        }
                        ret += filtro;
                        parziale += filtro;
                    }
                }
                return ret;
            }
        }
    }
}

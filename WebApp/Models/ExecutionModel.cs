﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Classes;

namespace WebApp.Models
{
    public class ExecutionCounters
    {
        //[Column("StartRunTime")]
        public string StartRunDateRif
        {
            get { return this.StartRunTime.ToString().Substring(0, 10); }
        }
        public string StartRunTime { get; set; }
        public int num_test { get; set; }
        public int process_pcr { get; set; }
        public int process_extr { get; set; }
        public int invalid { get; set; }
        public int error { get; set; }
        public int aborted { get; set; }
    }

    public class ExecutionDetail
    {
        public string RunStatus { get; set; }
        public string RunStatusDecode
        {
            get
            {
                switch (RunStatus)
                {
                    case "V": return "VALID";
                    case "I": return "INVALID";
                    case "E": return "ERROR";
                    case "A": return "ABORTED";
                    default: return RunStatus.ToString();
                }
            }
            set
            {

            }
        }
        public int? RunSetupID { get; set; }
        public int? TrackNumber { get; set; }
        public int? GeneralAssayID { get; set; }
        public string AssayName { get; set; }
        public int? SampleTypes { get; set; }
        public string SampleTypesDecode
        {
            get
            {
                //switch (SampleTypes)
                //{
                //    case 0: return "Sample";
                //    case 1: return "Calibrator";
                //    case 2: return "Control";
                //    default: return SampleTypes.ToString();
                //}
                return Lookup.SampleTypesDecode(this.SampleTypes);
            }
        }
        public string SampleMatrixName { get; set; }
        public int Process { get; set; }
        public string ProcessDecode
        {
            get
            {
                switch (Process)
                {
                    case 0: return "PCR";
                    case 1: return "Extraction PCR";
                    case 2: return "Extraction";
                    default: return Process.ToString();
                }
            }
            set
            {

            }
        }
        public int PositionToPlaceSample { get; set; }
        public int Sonication { get; set; }
        public int? SonicationOnTime { get; set; }
        public int? SonicationOffTime { get; set; }
        public int? SonicationCycle { get; set; }
        public int? Melt { get; set; }
        public string MeltDecode
        {
            get
            {
                switch (Melt)
                {
                    case 0: return "No Melting";
                    case 1: return "Melting";
                    default: return Melt.ToString();
                }
            }
            set
            {

            }
        }
        public int? RefEluteTrack { get; set; }
        public int? DilutionFactor { get; set; }
        public string SerialNumber { get; set; }
        public string PCRReactionCassetteSerialNumber { get; set; }
        public string ExtractionCassetteLot { get; set; }
        public DateTime? ExtractionCassetteExpiryDate { get; set; }
        public string ExtractionCassetteSerialNumber { get; set; }
        public string ReagentLot { get; set; }
        public DateTime? ReagentExpiryDate { get; set; }
        public int? ReagentTubeSize { get; set; }
        public string ICLot { get; set; }
        public DateTime? IcExpiryDate { get; set; }
        public int? IcTubeSize { get; set; }
        public string CalibratorLot { get; set; }
        public DateTime? CalibratorExpiryDate { get; set; }
        public string ControlLot { get; set; }
        public DateTime? ControlExpiryDate { get; set; }
        public int? MeasuredCalibrationResultID { get; set; }
        public int? MeasuredControlResultID { get; set; }
        public string AssayDetail { get; set; }
        public string AssayDetailDecode { get; set; }
        public int? LISUploadSelected { get; set; }
        public int? LISStatusv { get; set; }
        public string ApprovalUserName { get; set; }
        public int? ApprovalUserRole { get; set; }
        public string ApprovalUserRoleDecode
        {
            get
            {
                switch (ApprovalUserRole)
                {
                    case 0: return "Service";
                    case 1: return "Admin";
                    case 2: return "Analyst";
                    default: return ApprovalUserRole.ToString();
                }
            }
            set
            {

            }
        }
        public DateTime? ApprovalDateTime { get; set; }
        public int? ApprovalStatus { get; set; }
        public string ApprovalStatusDecode { get; set; }
        public string AssayDetailForCopiesPerMl { get; set; }
        public string AssayDetailForGeqPerMl { get; set; }
        public string AssayDetailForIuPerMl { get; set; }
        public string AssayDetailShortening { get; set; }
        public string AssayDetailShorteningForCopiesPerMl { get; set; }
        public string AssayDetailShorteningForGeqPerMl { get; set; }
        public string AssayDetailShorteningForIuPerMl { get; set; }
        public string AssayDetailShorteningUnit { get; set; }
        public string RunName { get; set; }
        public string UserName { get; set; }
        public DateTime? StartRunTime { get; set; }
        public DateTime? EndRunTime { get; set; }
        public DateTime? AbortTime { get; set; }
        public int? ExtractionInputVolume { get; set; }
        public int? ExtractedEluteVolume { get; set; }
        public string InventoryBlockConfirmationMethod { get; set; }
        public int? InventoryBlockNumber { get; set; }
        public string InventoryBlockName { get; set; }
        public string InventoryBlockBarcode { get; set; }

        // INSTRUMENTS 
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string Site_Code { get; set; }
        public string Site_Description { get; set; }
        public string Site_Address { get; set; }
        public string Site_City { get; set; }
        public string Site_Country { get; set; }
        public DateTime? Installation_Date { get; set; }
        public string Commercial_Status { get; set; }
        public string Finance_Status { get; set; }
        public string REGION { get; set; }
        public string AREA { get; set; }

        // ASSAY 
        public int IVDCleared { get; set; }
        public int CtsAndTmOnly { get; set; }
        public int? SampleTypes_assay { get; set; }
        public string Pathogen_TargetName { get; set; }
        public int ExtractionInputVolume_assay { get; set; }
        public int ExtractedEluteVolume_assay { get; set; }
        public int ResultsScaling { get; set; }
        public int SonicationOnTime_assay { get; set; }
        public int SonicationOffTime_assay { get; set; }
        public int SonicationCycle_assay { get; set; }
        public string ExtractionCassetteID { get; set; }
        public string PCRCassetteID { get; set; }
        public int PCRInputEluateVolume { get; set; }
        public int PreCycleStepNumber { get; set; }
        public int PCRAmplificationCycles { get; set; }
        public int PCRAmplificationSteps { get; set; }
        public int MeltRequired { get; set; }
        public int MeltingOptional { get; set; }
        public int StartRampTemperature { get; set; }
        public int EndRampTemperature { get; set; }
        public double RampRate { get; set; }
        public int UpperTolerance { get; set; }
        public int LowerTolerance { get; set; }
        public int OffSet { get; set; }
        public double ConversionFactorIU { get; set; }
        public string UserName_assay { get; set; }
        public DateTime RegisteredDate { get; set; }
        public int SampleMatrix_Id { get; set; }
        public string AssayParameterToolVersion { get; set; }
        public string Serial { get; set; }
        public int AssayId { get; set; }

        // Dati EXTRA 
        public DateTime? ExtractionData { get; set; }
        public string RevisionExtra { get; set; }
    }

    public class ExecutionDetailShort
    {
        public string RunStatus { get; set; }
        public string RunStatusDecode
        {
            get
            {
                switch (RunStatus)
                {
                    case "V": return "VALID";
                    case "I": return "INVALID";
                    case "E": return "ERROR";
                    case "A": return "ABORTED";
                    default: return RunStatus.ToString();
                }
            }
            set
            {

            }
        }
        public int? RunSetupID { get; set; }
        public int? TrackNumber { get; set; }
        public int? GeneralAssayID { get; set; }
        public string AssayName { get; set; }
        public int? SampleTypes { get; set; }
        public string SampleTypesDecode
        {
            get
            {
                //switch (SampleTypes)
                //{
                //    case 0: return "Sample";
                //    case 1: return "Calibrator";
                //    case 2: return "Control";
                //    default: return SampleTypes.ToString();
                //}
                return Lookup.SampleTypesDecode(this.SampleTypes);
            }
        }
        public string SampleMatrixName { get; set; }
        public int Process { get; set; }
        public string ProcessDecode
        {
            get
            {
                switch (Process)
                {
                    case 0: return "PCR";
                    case 1: return "Extraction PCR";
                    case 2: return "Extraction";
                    default: return Process.ToString();
                }
            }
            set
            {

            }
        }
        public int PositionToPlaceSample { get; set; }
        public int Sonication { get; set; }
        public int? Melt { get; set; }
        public string MeltDecode
        {
            get
            {
                switch (Melt)
                {
                    case 0: return "No Melting";
                    case 1: return "Melting";
                    default: return Melt.ToString();
                }
            }
            set
            {

            }
        }
        public int? RefEluteTrack { get; set; }
        public int? DilutionFactor { get; set; }
        public string SerialNumber { get; set; }
        public string PCRReactionCassetteSerialNumber { get; set; }
        public string ExtractionCassetteLot { get; set; }
        public DateTime? ExtractionCassetteExpiryDate { get; set; }
        public string ReagentLot { get; set; }
        public DateTime? ReagentExpiryDate { get; set; }
        public string ICLot { get; set; }
        public DateTime? IcExpiryDate { get; set; }
        public string CalibratorLot { get; set; }
        public DateTime? CalibratorExpiryDate { get; set; }
        public string ControlLot { get; set; }
        public DateTime? ControlExpiryDate { get; set; }
        public int? MeasuredCalibrationResultID { get; set; }
        public int? MeasuredControlResultID { get; set; }
        public string AssayDetail { get; set; }
        public string AssayDetailDecode { get; set; }
        public DateTime? ApprovalDateTime { get; set; }
        public int? ApprovalStatus { get; set; }
        public string ApprovalStatusDecode { get; set; }
        public string AssayDetailShorteningForIuPerMl { get; set; }
        public string AssayDetailShorteningUnit { get; set; }
        public string RunName { get; set; }
        public string UserName { get; set; }
        public DateTime? StartRunTime { get; set; }
        public DateTime? EndRunTime { get; set; }
        public DateTime? AbortTime { get; set; }
        public int? ExtractionInputVolume { get; set; }
        public int? ExtractedEluteVolume { get; set; }

        // INSTRUMENTS 
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string Site_Code { get; set; }
        public string Site_Description { get; set; }
        public string Site_Address { get; set; }
        public string Site_City { get; set; }
        public string Site_Country { get; set; }
        public DateTime? Installation_Date { get; set; }
        public string Commercial_Status { get; set; }
        public string Finance_Status { get; set; }

        // ASSAY 
        public string Pathogen_TargetName { get; set; }
        public int ExtractionInputVolume_assay { get; set; }
        public int ExtractedEluteVolume_assay { get; set; }
        public string ExtractionCassetteID { get; set; }
        public string UserName_assay { get; set; }
        public DateTime RegisteredDate { get; set; }
        public string AssayParameterToolVersion { get; set; }
        public string Serial { get; set; }
        public int AssayId { get; set; }

        // Dati EXTRA 
        public DateTime? ExtractionData { get; set; }
        public string RevisionExtra { get; set; }

    }

    public class ExecutionDetailMKT
    {
        public string RunStatus { get; set; }  // ??? 
        public string RunStatusDecode
        {
            get
            {
                switch (RunStatus)
                {
                    case "V": return "VALID";
                    case "I": return "INVALID";
                    case "E": return "ERROR";
                    case "A": return "ABORTED";
                    default: return RunStatus.ToString();
                }
            }
            set
            {

            }
        }
        public string AssayName { get; set; }
        public int? SampleTypes { get; set; }
        public string SampleTypesDecode
        {
            get
            {
                //switch (SampleTypes)
                //{
                //    case 0: return "Sample";
                //    case 1: return "Calibrator";
                //    case 2: return "Control";
                //    default: return SampleTypes.ToString();
                //}
                return Lookup.SampleTypesDecode(this.SampleTypes);
            }
        }
        public string SampleMatrixName { get; set; }
        public int Process { get; set; }
        public string ProcessDecode
        {
            get
            {
                switch (Process)
                {
                    case 0: return "PCR";
                    case 1: return "Extraction PCR";
                    case 2: return "Extraction";
                    default: return Process.ToString();
                }
            }
            set
            {

            }
        }
        public string SerialNumber { get; set; }
        public string AssayDetailDecode { get; set; }
        public DateTime? ApprovalDateTime { get; set; }
        public string ApprovalStatusDecode { get; set; }
        public DateTime? StartRunTime { get; set; }
        public DateTime? EndRunTime { get; set; }
        public DateTime? AbortTime { get; set; }

        // INSTRUMENTS 
        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string Site_Code { get; set; }
        public string Site_Description { get; set; }
        public string Site_City { get; set; }
        public string Site_Country { get; set; }
        public DateTime? Installation_Date { get; set; }
        public string Commercial_Status { get; set; }
        public string Finance_Status { get; set; }

        // ASSAY 
        public string Pathogen_TargetName { get; set; }

        // Dati EXTRA 
        public DateTime? ExtractionData { get; set; }
    }

    public class ExecutionModel
    {
        public ExecutionModel()
        {
            this.filters = new Dictionary<string, string>();
        }

        //public string  summary{ get; set; }
        public Dictionary<string, string> filters { get; set; }
        public List<ExecutionCounters> dailyExecutions { get; set; }
        public List<ExecutionDetail> exportDetails { get; set; }
        public List<ExecutionDetailShort> exportDetailShorts { get; set; }
        public List<ExecutionDetailMKT> exportDetailMKT { get; set; }
        public int cntInstruments { get; set; }
        // Formattazione filtri per presentazione sopra al grafico
        public string filtersDesc
        {
            get
            {
                string ret = "";
                string parziale = "";
                string filtro = "";
                if (this.filters != null)
                {
                    foreach (var f in this.filters)
                    {
                        if (ret.Length > 0) { ret += ", "; }
                        filtro = string.Format("{0}: <b>{1}</b>", f.Key, f.Value);
                        if ((parziale.Length + filtro.Length) > 175)
                        {
                            ret += "\n";
                            parziale = "";
                        }
                        ret += filtro;
                        parziale += filtro;
                    }
                }
                return ret;
            }
        }
    }
}

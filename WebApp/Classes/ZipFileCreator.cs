﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
//using System.IO.Compression.FileSystem;

namespace WebApp.Classes
{
    public static class ZipFileCreator
    {
        /// <summary>
        /// Create a ZIP file of the files provided.
        /// </summary>
        /// <param name="fileName">The full path and name to store the ZIP file at.</param>
        /// <param name="files">The list of files to be added.</param>
        public static List<string> CreateZipFile(string fileName, List<string> files, string strToMask, string strToReplace)
        {
            // Create and open a new ZIP file
            List<string> filesOk = new List<string>();
            string scartati = "";

            var zip = ZipFile.Open(fileName, ZipArchiveMode.Create);
            foreach (var file in files)
            {
                // Add the entry for each file
                try
                {
                    FileInfo newFile = new FileInfo(file);
                    if (newFile.Length < 307200000)  // 300 MB
                    {
                        zip.CreateEntryFromFile(file, Path.GetFileName(file), CompressionLevel.Optimal);
                        filesOk.Add(file);
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(scartati))
                            scartati += "\n";
                        scartati += System.IO.Path.GetFileName(file) + " -> too big (> 300MB)";

                    }
                }
                catch (Exception e)
                {
                    if (!string.IsNullOrWhiteSpace(scartati))
                        scartati += "\n";
                    scartati += System.IO.Path.GetFileName(file) + " -> " + e.Message;
                }
            }
            if (!string.IsNullOrWhiteSpace(scartati) && filesOk.Count > 0)
            {
                if (!string.IsNullOrWhiteSpace(strToMask))
                {
                    scartati = scartati.Replace(strToMask, strToReplace);
                }
                var note = zip.CreateEntry("_skipped_files_and_errors_.txt");
                using (var entryStream = note.Open())
                using (var streamWriter = new StreamWriter(entryStream))
                {
                    streamWriter.Write(scartati);
                }
            }
            // Dispose of the object when we are done
            zip.Dispose();
            return filesOk;
        }
    }
}

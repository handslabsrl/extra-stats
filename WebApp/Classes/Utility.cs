﻿using System;
using System.Collections.Generic;
//using System.IO;
//using System.Reflection;
//using System.Security.Cryptography;
//using System.Text;

namespace WebApp.Classes
{
    public static class Utility
    {
        public static bool isEmailValid(string emailAddress)
        {
            try
            {
                System.Net.Mail.MailAddress m = new System.Net.Mail.MailAddress(emailAddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public static string removeCRLF(string source)
        {
            string ret = source;
            if (!string.IsNullOrWhiteSpace(ret))
            {
                ret = ret.Replace("\n\r", " ").Replace("\n", " ").Replace("\r", " ").Trim();
            }
            return ret;
        }
        public static string GetTempFileName(string root, string extention, string path = null)
        {
            var candidate = new List<string>();
            candidate.Add(string.Format("{0}.{1}", Lookup.GetValidFileName(root), Lookup.GetValidFileName(extention)));
            for (int i = 1; i <= 99; i++)
            {
                candidate.Add(string.Format("{0}_{2}.{1}", Lookup.GetValidFileName(root), Lookup.GetValidFileName(extention), i));
            }
            // se è stato passato il path, ciclo su tutti i CANDIDATE, se esite provo di cancellarlo; Se non esiste è il file da assegnare 
            if (!string.IsNullOrWhiteSpace(path))
            {
                for (int i = 0; i <= candidate.Count; i++)
                {
                    string filePathName = System.IO.Path.Combine(path, candidate[i]);
                    if (System.IO.File.Exists(filePathName))
                    {
                        try
                        {
                            System.IO.File.Delete(filePathName);
                        }
                        catch (Exception e) { }
                    }
                    if (!System.IO.File.Exists(filePathName))
                    {
                        return candidate[i];
                    }
                }
            }
            return candidate[0];
        }
   
        //public static string EncryptString(string data, string sKey = "")
        //{
        //    if (string.IsNullOrWhiteSpace(sKey))
        //    {
        //        sKey = Lookup.secretKey;
        //    }
        //    MemoryStream msOutput = new MemoryStream();

        //    DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
        //    DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
        //    DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
        //    ICryptoTransform desencrypt = DES.CreateEncryptor();
        //    CryptoStream cryptostream = new CryptoStream(msOutput,
        //        desencrypt,
        //        CryptoStreamMode.Write);

        //    StreamWriter sw = new StreamWriter(cryptostream);
        //    sw.WriteLine(data);
        //    sw.Close();
        //    cryptostream.Close();

        //    return Convert.ToBase64String(msOutput.ToArray());
        //}

        //public static string DecryptString(string data, string sKey = "")
        //{
        //    if (string.IsNullOrWhiteSpace(sKey))
        //    {
        //        sKey = Lookup.secretKey;
        //    }

        //    DESCryptoServiceProvider DES = new DESCryptoServiceProvider();
        //    DES.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
        //    DES.IV = ASCIIEncoding.ASCII.GetBytes(sKey);

        //    MemoryStream ms = new MemoryStream(Convert.FromBase64String(data));
        //    CryptoStream encStream = new CryptoStream(ms, DES.CreateDecryptor(), CryptoStreamMode.Read);
        //    StreamReader sr = new StreamReader(encStream);

        //    string val = sr.ReadLine();

        //    sr.Close();
        //    encStream.Close();
        //    ms.Close();

        //    return val;
        //}
    }
}



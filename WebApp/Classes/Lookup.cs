﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Configuration.Xml;
using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Entity;
using WebApp.Repository;
using System.Reflection;
using System.Text.RegularExpressions;
using WebApp.Models;
using Newtonsoft.Json;

namespace WebApp.Classes
{
    public static class Lookup
    {

        public static string secretKey = "Elite2022!";   // 8 byte -> 64bit


        // ***************************************************************
        // RUOLI / SICUREZZA 
        // DASHBOARD (accesso completo oppure limitato a area/regione di competenza)
        // ***************************************************************
        public const string Role_Admin = "Admin";
        public const string Role_ELITeInstruments = "ELITe Instruments";
        public const string Role_AssaysNotification = "Assays Notification";
        public const string Role_LogFilesSupervisor = "Log Files Supervisor";
        public const string Role_LettersAssays = "Letters Assays";
        public const string Role_Quotation = "Quotation";
        public const string Role_QuotationApproval = "Quotation Approval";
        public const string Role_QuotationBackOffice = "Quotation BackOffice";
        public const string Role_QuotationManager = "Quotation Manager";
        public const string Role_TrainingBackOffice = "Training BackOffice";
        public const string Role_TrainingNotification = "Training Notification";
        public const string Role_DocumentalRepository = "Documental Repository";
        public const string Role_DocumentalRepositoryBackOffice = "Documental Repository BackOffice";
        public const string Role_SupportAdministrator = "Support Administrator";
        public const string Role_SupportConfiguration = "Support Configuration";
        public const string Role_SupportViewer = "Support Viewer";
        public const string Role_OpenAP3P_SupportAdministrator = "Third Party Assay Protocols - Support Administrator";
        public const string Role_OpenAP3P_OrderDepartment = "Third Party Assay Protocols - Orders Department";
        public const string Role_OpenAP3P_ResearchAndDevelopment = "Third Party Assay Protocols - Research and Development";
        public const string Role_OpenAP3P_Quality = "Third Party Assay Protocols - Quality";
        public const string Role_CustomerComplaintAnalyst = "Customer Complaint Analyst";
        public const string Role_Support = "Support";
        public const string Role_Warranty = "Warranty";
        public const string Role_WarrantyAdministrator = "Warranty Administrator";
        public const string Role_WarrantyBackOffice = "Warranty BackOffice";
        public const string Role_KPI = "Key Performance Indicators";
        public const string Role_CalendarAdministrator = "Calendar Administrator";
        public const string Role_RefurbishAdministrator = "Refurbish Administrator";
        public const string Role_RefurbishFinance = "Refurbish Finance";
        public const string Role_RefurbishCenter = "Refurbish Center";
        public const string Role_RefurbishReadOnly = "Refurbish ReadOnly";

        // UserLink 
        public enum UserLinkType : int
        {
            Country = 0,
            CommercialEntity = 1,
            Customer = 2,
            DRSection = 3,
            Region = 4,
            AreaManager = 5,
            TKSupportType = 6 
        }

        public static List<LookUpString> listDepartmentType = new List<LookUpString>{
                    new LookUpString() { descrizione = ""},
                    new LookUpString() { descrizione = "SUPPORT" },
                    new LookUpString() { descrizione = "SALES AREA" }
        };

        public const string User_CfgOpt_All = "Roles&Configuration";
        public const string User_CfgOpt_Roles = "Only Roles";
        public const string User_CfgOpt_Configuration = "Only Configuration";

        public const string glb_sessionkey_UsersIndex_ShowLocked = "_UsersIndex_ShowLocked";

        // ***************************************************************
        // CLAIM 
        // ***************************************************************
        // Status 
        public const string Claim_Status_NEW = "NEW";
        public const string Claim_Status_COMPLETED = "COMPLETED";
        public const string Claim_Status_CLOSED = "CLOSED";
        public const string Claim_Status_ERROR = "ERROR";

        // Status DECO
        public const string Claim_StatusDeco_NEW = "Processing";
        public const string Claim_StatusDeco_COMPLETED = "Process complete";
        public const string Claim_StatusDeco_CLOSED = "Closed";
        public const string Claim_StatusDeco_ERROR = "ERROR";

        // Folder
        public const string Claim_Folder_Fisics = "FISICS";
        public const string Claim_Folder_Operation = "OPERATION";
        public const string Claim_Folder_System = "SYSTEM";
        public const string Claim_Folder_Debug = "DEBUG";

        // View Rows 
        public const string Claim_RowsList_None = "NONE";
        public const string Claim_RowsList_Group1 = "GROUP1";

        public enum Claim_Fluorescence_View : int
        {
            Track_Cycle = 0,
            Track_Tm,
            Track_RawData,
            Channel_Cycle,
            Channel_Tm,
            Channel_RawData
        }

        public static List<LookUpString> listClaimFluorescenceView = new List<LookUpString>{
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Track_Cycle, descrizione = "Track - Cycle" },
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Track_Tm, descrizione = "Track - Tm" },
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Track_RawData, descrizione = "Track - RawData" },
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Channel_Cycle, descrizione = "Channel - Cycle" },
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Channel_Tm, descrizione = "Channel - Tm" },
            new LookUpString() { idNum = (int)Lookup.Claim_Fluorescence_View.Channel_RawData, descrizione = "Channel - RawData" }
        };


        // personalizzazione SCRIPT 
        public static List<LookUpString> LogFiles_ScriptDecode = new List<LookUpString>{
                    new LookUpString() { id = "SampleDisp.scr", descrizione = "Sample (SampleDisp.scr)"},
                    new LookUpString() { id = "ICDisp.scr", descrizione = "IC (ICDisp.scr)"},
                    new LookUpString() { id = "Extraction.scr", descrizione = "Extraction 12Nozzle (Extraction.scr)"},
                    new LookUpString() { id = "PCRReagentDisp.scr", descrizione = "MMix (PCRReagentDisp.scr)"},
                    new LookUpString() { id = "DNASampleDisp.scr", descrizione = "Eluate (DNASampleDisp.scr)"}
        };

        // Tipi di registrazione della Temperatura
        public const string Claim_TempRecType_Temp = "T"; 
        public const string Claim_TempRecType_CoolBlock = "C";

        // ***************************************************************
        // PROCESS REQUEST
        // ***************************************************************
        // Status
        public const string ProcessRequest_Status_NEW = "NEW";
        public const string ProcessRequest_Status_READY = "READY";
        public const string ProcessRequest_Status_ERROR = "ERROR";

        public const string ProcessRequest_Create_DETAIL = "DETAIL";
        public const string ProcessRequest_Create_SUMMARY = "SUMMARY";
        public const string ProcessRequest_Create_BOTH = "BOTH";

        public const string ProcessRequest_Type_DeleteClaim = "DELETE_CLAIM";
        public const string ProcessRequest_Type_RnDExtractions = "R&D_EXTRACTION";
        public const string ProcessRequest_Type_RnDExtractionsSummary = "R&D_EXTRACTION_SUMMARY";
        public const string ProcessRequest_Type_NotifyClaimAssign = "NOTIFY_CLAIM_ASSIGN";
        public const string ProcessRequest_Type_RegenerateCertificate = "REGENERATE_CERTIFICATE";
        public const string ProcessRequest_Type_NewAssayVersion = "NEW_ASSAY_VERSION";
        public const string ProcessRequest_Type_AssaysNotification = "ASSAYS_NOTIFICATION";
        public const string ProcessRequest_Type_ReloadAddInfoPaths = "RELOAD_ADDINFO_PATHS";
        public const string ProcessRequest_Type_InstrumentUpdate = "INSTRUMENT_UPDATE";
        public const string ProcessRequest_Type_LogFileTemperature = "LOGFLE_TEMP";
        public const string ProcessRequest_Type_LogFileFluorescenze = "LOGFLE_FLUO";
        public const string ProcessRequest_Type_LogFileTemperatureDeco = "Temperatures (logfile)";
        public const string ProcessRequest_Type_LogFileFluorescenzeDeco = "Fluorescence (logfile)";

        // ***************************************************************
        // DATA MINING 
        // ***************************************************************
        // Status
        public const string DataMining_Status_NEW = "NEW";
        public const string DataMining_Status_READY = "READY";
        public const string DataMining_Status_ERROR = "ERROR";

        public const string DataMining_Type_RunningRate = "RUNNING RATE";
        public const string DataMining_Type_RunningRateDesc = "Cost Per Reportable Result";
        public const string DataMining_Type_RunsByType = "RUNS BY TYPE";
        public const string DataMining_Type_RunsByTypeDesc = "Data Mining - Runs by Type";
        public const string DataMining_Type_RunningByComponent = "RUNNING RATE COM";
        public const string DataMining_Type_RunningByComponentDesc = "Billing Each Component";

        // ***************************************************************
        // ASSAY/ASSAY VERSIONE 
        // ***************************************************************
        public enum AssayVersion_StatusSend : int
        {
            T0 = 0,
            T1,
            T2
        }
        public static List<LookUpString> listAssayVersionStatusSend = new List<LookUpString>{
            new LookUpString() { idNum = (int)Lookup.AssayVersion_StatusSend.T0, descrizione = "T0" },
            new LookUpString() { idNum = (int)Lookup.AssayVersion_StatusSend.T1, descrizione = "T1" },
            new LookUpString() { idNum = (int)Lookup.AssayVersion_StatusSend.T2, descrizione = "T2" },
        };

        public enum Assay_StatoManleva : int
        {
            DaCreare = 0,
            Creata,
            Firmata
        }

        public const string Letter_MANLEVA = "MANLEVA";


        // ***************************************************************
        // QUOTATIONS 
        // ***************************************************************
        // Target Type
        public const string TargetType_STD = "STD";
        public const string TargetType_OPEN = "OPEN";
        public const string TargetTypeDesc_STD = "Elite PCR Kit";
        public const string TargetTypeDesc_OPEN = "Open PCR Kits";

        public const string FileDataType_InstallBase = "INSTALLBASE";
        public const string FileDataType_Data = "DATA";
        public const string FileDataType_AssaysList = "ASSAYSLIST";

        //public const string RoleId_QuotationApproval = "QUOTATION_APPROVAL";
        //public const string RoleId_LogFiles = "LOG_FILES";
        public static string SampleTypesDecode(int? SampleTypes)
        {
            if (SampleTypes == null)
            {
                return "";
            }
            else
            {
                switch (SampleTypes)
                {
                    case 0: return "Sample";
                    case 1: return "Calibrator";
                    case 2: return "Control";
                    default: return SampleTypes.ToString();
                }
            }
        }

        public enum QStatus : int
        {
            //[Display(Name = "DRAFT")]
            draft = 0,
            waiting = 1,        // Waiting Approval
            approved = 2,
            deleted = 9
        }

        public enum ElitePCRKitType : int
        {
            Assay = 0,
            Control,
            Standard,
            Consumable = 9
        }
        public enum ExtractionType : int
        {
            singleplex = 0,
            noExtraction,
            multiplex
        }
        public enum AnalysisType : int
        {
            none = 0,
            qualitative,
            quantitative
        }
        public enum OpenPCRKitType : int
        {
            Assay = 0,
            Control
        }
        public enum CustomerType : int
        {
            PUBLIC = 0,
            PRIVATE = 1,
            DISTRIBUTOR = 2,
            OTHER = 3
        }
        public enum SaleType : int
        {
            TENDER = 0,
            DIRECT_SALE
        }
        public enum ContractType : int
        {
            [Display(Name = "Reagent Rental")]
            REAGENT_RENTAL = 0,
            [Display(Name = "Cash Sales (Direct Sale)")]
            SALE,
            [Display(Name = "ASR only")]
            OTHER
        }
        public enum OpenPCRKitsGGExpiry : int
        {
            NA,
            GG15 = 15,
            GG20 = 20,
            GG25 = 25,
            GG30 = 30,
            GG45 = 45,
            GG60 = 60,
            GG90 = 90
        }
        public enum OpenPCRKitControl : int
        {
            included = 0,
            not_included = 1
        }

        public static List<LookUpString> listElitePCRKitType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Assay, descrizione = Lookup.ElitePCRKitType.Assay.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Control, descrizione = Lookup.ElitePCRKitType.Control.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ElitePCRKitType.Standard, descrizione = Lookup.ElitePCRKitType.Standard.ToString() }
        };

        public static List<LookUpString> listExtractionType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.noExtraction, descrizione = Lookup.ExtractionType.noExtraction.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.singleplex, descrizione = Lookup.ExtractionType.singleplex.ToString() },
                    new LookUpString() { idNum = (int)Lookup.ExtractionType.multiplex, descrizione = Lookup.ExtractionType.multiplex.ToString() }
        };

        public static List<LookUpString> listAnalysisType = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.none, descrizione = Lookup.AnalysisType.none.ToString() },
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.qualitative, descrizione = Lookup.AnalysisType.qualitative.ToString() },
                    new LookUpString() { idNum = (int)Lookup.AnalysisType.quantitative, descrizione = Lookup.AnalysisType.quantitative.ToString() }
        };
        public static List<LookUpString> listOpenPCRKitsGGExpiry = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.NA, descrizione = "n.a." },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG15, descrizione = "15" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG20, descrizione = "20" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG25, descrizione = "25" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG30, descrizione = "30" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG45, descrizione = "45" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG60, descrizione = "60" },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitsGGExpiry.GG90, descrizione = "90" }
        };
        public static List<LookUpString> listOpenPCRKitControl = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitControl.included, descrizione = Lookup.OpenPCRKitControl.included.ToString().Replace("_", " ") },
                    new LookUpString() { idNum = (int)Lookup.OpenPCRKitControl.not_included, descrizione = Lookup.OpenPCRKitControl.not_included.ToString().Replace("_", " ") }
        };


        // ***************************************************************
        // TRAINING
        // ***************************************************************
        // Status 
        public const string Training_Status_NEW = "New";
        public const string Training_Status_PUBLISHED = "Published";
        public const string Training_Status_CLOSED = "Closed";
        public const string Training_Status_ARCHIVED = "Archived";
        public const string Training_Status_DELETED = "Deleted";

        public enum TrainigVISA : int
        {
            yes_with_letter = 0,
            yes_no_letter,
            no
        }

        public static List<LookUpString> listTrainigVISA = new List<LookUpString>{
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.yes_with_letter, descrizione = "I need the VISA, please send me an Invitation letter" },
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.yes_no_letter, descrizione = "I need the VISA but I do not need the Invitation letter" },
                    new LookUpString() { idNum = (int)Lookup.TrainigVISA.no, descrizione = "I do not need the VISA" }
        };

        public const string TrainingRegistration_Status_NEW = "New";
        public const string TrainingRegistration_Status_ACCEPTED = "Accepted";
        public const string TrainingRegistration_Status_REFUSED = "Refused";

        public static List<LookUpString> listRegistrationStatus = new List<LookUpString>{
                    new LookUpString() { id = TrainingRegistration_Status_NEW, descrizione = TrainingRegistration_Status_NEW },
                    new LookUpString() { id = TrainingRegistration_Status_ACCEPTED, descrizione = TrainingRegistration_Status_ACCEPTED },
                    new LookUpString() { id = TrainingRegistration_Status_REFUSED, descrizione = TrainingRegistration_Status_REFUSED}
        };

        public static List<LookUpString> listRegistrationTitle = new List<LookUpString>{
                    new LookUpString() { id = "Mr.", descrizione = "Mr." },
                    new LookUpString() { id = "Mrs.", descrizione = "Mrs." },
                    new LookUpString() { id = "Ms.", descrizione = "Ms." },
                    new LookUpString() { id = "Miss", descrizione = "Miss" }
        };

        public const string TrainingRegistration_Accomodation_NOT_REQUIRED = "Not Required";
        public const string TrainingRegistration_Accomodation_REQUIRED = "Required";
        public const string TrainingRegistration_Accomodation_AWAITING = "Awaiting";
        public const string TrainingRegistration_Accomodation_CONFIRMED = "Confirmed";
        public const string TrainingRegistration_Accomodation_COMPLETED = "Completed";

        public const string TrainingRegistration_VISA_NOT_REQUIRED = "Not Required";
        //public const string TrainingRegistration_VISA_NOT_NECESSARY = "Not Necessary"; 
        public const string TrainingRegistration_VISA_REQUIRED = "Required";
        public const string TrainingRegistration_VISA_NOT_SENT = "Not Sent";
        public const string TrainingRegistration_VISA_SENT = "Completed";

        public const string glb_sessionkey_TrainingIndex_ShowDeleted = "_TrainingIndex_ShowDeleted";
        public const string glb_sessionkey_TrainingIndex_ShowArchived = "_TrainingIndex_ShowArchived";

        public const string Training_Email_NewRegistration = "_NEWREG";
        public const string Training_Email_NotifyNewRegistration = "_NOTIFY_NEWREG";
        public const string Training_Email_ConfRegistration = "_CONF_REG";
        public const string Training_Email_RefuseRegistration = "_REJECT_REG";
        public const string Training_Email_ConfHotel = "_CONF_HOTEL";
        public const string Training_Email_InvitationLetter = "_INVITATION_LETTER";
        public const string Training_Email_HOTEL = "***HOTEL***";  // da Hotel
        public const string Training_Email_RemoveRegistration = "_REMOVE_REG";

        // ***************************************************************
        // DOCUMENTAL REPOSITORY 
        // ***************************************************************
        public enum DRResourceType : int
        {
            file = 0,
            attachment,
            url
        }
        public const string DRTracking_Operation_GET = "GET";
        public const string DRTracking_Operation_PUT = "PUT";
        public const string DRTracking_Operation_DELETE = "DELETE";
        public const string DRTracking_Operation_UPDATE = "UPDATE";

        public const string DRTracking_ExtType_TOPIC = "TOPIC";
        public const string DRTracking_ExtType_RESOURCE = "RESOURCE";

        public const string Documental_Email_Notification = "_DOCUMENT NOTIFICATION";

        //public enum DRTrackingType: int
        //{
        //    add = 0,
        //    update,
        //    remove, 
        //    read
        //}

        // ***************************************************************
        // SUPPORT / TICKETING 
        // ***************************************************************
        // Config
        //public const string TKSupport_Config_STD = "STD";
        //public const string TKSupport_Config_ITA = "ITA";
        //public const string TKSupport_Config_CONS = "CONS";
        
            
        public const string TKSupport_Config_GroupId_ADDINFO = "ADDINFO";
        public const string TKSupport_Config_GroupId_TODOLIST = "TODOLIST";

        public enum TKAttachType : int
        {
            file = 0,
            url
        }
        public enum Classification_RadioGroup : int
        {
            No = 0,
            Optional,
            Mandatory
        }

        //public enum Classification_Instrument : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}
        //public enum Classification_AddInfo : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}
        //public enum Classification_ClosingInfo : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}
        //public enum XClassification_ClaimLogFile : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}
        //public enum Classification_OtherPlatform : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}
        //public enum Classification_CodArt : int
        //{
        //    No = 0,
        //    Optional,
        //    Mandatory
        //}

        // Status 
        public const string TKRequest_Status_DRAFT = "DRAFT";
        public const string TKRequest_Status_PENDING = "PENDING";
        public const string TKRequest_Status_REOPENED = "REOPENED";
        public const string TKRequest_Status_PROCESSING = "PROCESSING";
        public const string TKRequest_Status_CLOSED = "CLOSED";
        public const string TKRequest_Status_STANDBY = "STANDBY";
        public const string TKRequest_Status_DELETED = "DELETED";
        public const string TKRequest_Status_UNDER_INVESTIGATION = "UNDER_INVESTIGATION";

        // Status DECO
        public const string TKRequest_StatusDeco_DRAFT = "DRAFT";
        public const string TKRequest_StatusDeco_PENDING = "Pending";
        public const string TKRequest_StatusDeco_REOPENED = "Reopened";
        public const string TKRequest_StatusDeco_PROCESSING = "Processing";
        public const string TKRequest_StatusDeco_CLOSED = "Closed";
        public const string TKRequest_StatusDeco_STANDBY = "StandBy";
        public const string TKRequest_StatusDeco_DELETED = "Deleted";
        public const string TKRequest_StatusDeco_UNDER_INVESTIGATION = "Under Investigation";

        public const string glb_sessionkey_Requests_IndexOrigin = "_RequestsIndex_IndexOrigin";
        public const string glb_sessionkey_IndexRequests_TabItemIndex = "_RequestsIndex_TabItemIndex";
        public const string glb_sessionkey_IndexRequests_Area = "_RequestsIndex_Area";
        public const string glb_sessionkey_Complaint_TabItemIndex = "_Complaintx_TabItemIndex";
        public const string glb_sessionkey_Complaints_Area = "_Complaint_Area";

        public const string TKRequest_ActionIndex_YourRequests = "IndexYourRequests";
        public const string TKRequest_ActionIndex_Manage = "Manage";
        public const string TKRequest_ActionIndex_Complaint = "Complaint";

        public const string TKEvent_Operation_CREATE = "CREATE";
        public const string TKEvent_Operation_UPDATE = "UPDATE";
        public const string TKEvent_Operation_REOPEN = "REOPEN";
        public const string TKEvent_Operation_DELETE = "DELETE";
        public const string TKEvent_Operation_ASSIGN = "ASSIGN";
        public const string TKEvent_Operation_CHANGE_USER_REGISTRATION = "CHANGE USER REGISTRATION";
        public const string TKEvent_Operation_SUSPEND = "SUSPEND";
        public const string TKEvent_Operation_CLOSED = "CLOSED";
        public const string TKEvent_Operation_ADDATTACH = "ADD ATTACH";
        public const string TKEvent_Operation_REMOVEATTACH = "REMOVE ATTACH";
        public const string TKEvent_Operation_SENDEMAIL = "SEND EMAIL";
        public const string TKEvent_Operation_NOTE_DRAFT = "NOTE - Draft";
        public const string TKEvent_Operation_ADDNOTE_INFO = "ADD NOTE";  
        public const string TKEvent_Operation_ADDNOTE_USER2SUPPORT = "ADD NOTE - User To Support Team";
        public const string TKEvent_Operation_ADDNOTE_SUPPORT = "ADD NOTE - Reserved on Support Team";
        public const string TKEvent_Operation_ADDNOTE_SUPPORT2USER = "ADD NOTE - Support Team To User";
        public const string TKEvent_Operation_ADDNOTE_SUPPORT2CCA = "ADD NOTE - Support Team To CCA Team";
        public const string TKEvent_Operation_ADDNOTE_CCA2SUPPORT = "ADD NOTE - CCA Team To Support Team";
        public const string TKEvent_Operation_ADDNOTE_CCA = "ADD NOTE - Reserved on CCA Team";
        public const string TKEvent_Operation_ENGAGEMENTSUPPLIER = "ENGAGEMENT_SUPPLIER";

        public enum GetRequestsType : int
        {
            YourRequests = 0, 
            Manage, 
            Statistics, 
            //YourComplaint,
            ManageComplaint,
            onlyCCA
        }

        public enum Request_NotificationType : int
        {
            // Sono volutamente generici e non declinati per Modulo!!! 
            None = 0,  // Default 
            NewRequest,
            NewRequestAP3P,
            RefuseRequestAP3P,
            ApproveRequestAP3P,
            ReadyRequestAP3P,
            ReleasedRequestAP3P,
            DownloadRequestAP3P, 
            CloseRequest,
            AssignRequest,
            SupendRequest,
            AddReservedNote,
            AddPublicNote,
            AddPublicNoteCCAreaManager,
            AddReporterNote,
            ChangeAttach,
            TakingCharge,
            ReopenRequest, 
            AreaManager, 
            SetWarrantyStatus,
            EngagementSupplier, 
            BookingNewReservation,
            BookingModifyReservation,
            BookingDeleteReservation,
            ComplaintReject,
            ComplaintForwardToCCA, 
            ComplaintRequestData, 
            RefurbishCreate, 
            RefurbishReadyToShipped
        }

        public const string Support_Email_NewRequest = "_NEW_REQUEST";
        public const string Support_Email_CloseRequest = "_CLOSE_REQUEST";
        public const string Support_Email_AssignRequest = "_ASSIGN_REQUEST";
        public const string Support_Email_NoteToReporter = "_NOTE_TO_REPORTER";
        public const string Support_Email_NoteToSupport = "_NOTE_TO_SUPPORT";
        public const string Support_Email_TakingCharge = "_TAKING_CHARGE";
        public const string Support_Email_ReopenRequest = "_REOPEN_REQUEST";
        public const string Support_Email_ComplaintForwardToCCA = "_FORWARD_TO_CCA";
        public const string Support_Email_ComplaintReject = "_COMPLAINT_REJECT";
        public const string Support_Email_ComplaintRequestData = "_COMPLAINT_REQUEST_DATA";

        public const string Support_Email_NewRequestAP3P = "_NEW_OPEN_AP_REQUEST";
        public const string Support_Email_RefuseRequestAP3P = "_REFUSE_OPEN_AP_REQUEST";
        public const string Support_Email_ApproveRequestAP3P = "_APPROVE_OPEN_AP_REQUEST";
        public const string Support_Email_ReadyRequestAP3P = "_READY_OPEN_AP_REQUEST";
        public const string Support_Email_ReleasedRequestAP3P = "_RELEASED_OPEN_AP_REQUEST";
        public const string Support_Email_DownloadRequestAP3P = "_DOWNLOAD_OPEN_AP_REQUEST";

        public const string glb_sessionkey_TKManage_ShowClosed = "_TKManage_ShowClosed";
        public const string glb_sessionkey_TKManage_ShowOnlyRequestData = "_TKManage_ShowOnlyRequestData";

        // ***************************************************************
        // SUPPORT AP THIRD PARTY (3P)
        // ***************************************************************
        public enum TK3PKAttachType : int
        {
            IFU = 0,
            AssayCompatibilityEvaluationForm
        }

        // Status
        public const string TK3PRequest_Status_DRAFT = "DRAFT";
        public const string TK3PRequest_Status_PENDING = "PENDING";
        public const string TK3PRequest_Status_REFUSED = "REFUSED";
        public const string TK3PRequest_Status_PROCESSING = "PROCESSING";
        public const string TK3PRequest_Status_READY = "READY";
        public const string TK3PRequest_Status_RELEASED = "RELEASED";
        public const string TK3PRequest_Status_CLOSED = "CLOSED";
        public const string TK3PRequest_Status_DELETED = "DELETED";

        // Status DECO
        public const string TK3PRequest_StatusDeco_DRAFT = "DRAFT";
        public const string TK3PRequest_StatusDeco_PENDING = "Pending";
        public const string TK3PRequest_StatusDeco_REFUSED = "Refused";
        public const string TK3PRequest_StatusDeco_PROCESSING = "To be created";
        public const string TK3PRequest_StatusDeco_READY = "Ready to be Released";
        public const string TK3PRequest_StatusDeco_RELEASED = "Released";
        public const string TK3PRequest_StatusDeco_CLOSED = "Closed";
        public const string TK3PRequest_StatusDeco_DELETED = "DELETED";

        public const string glb_sessionkey_TK3PRequests_ShowAllRequests = "_TK3PRequests_ShowAllRequests";

        public const string TK3PEvent_Operation_CREATE = "CREATE";
        public const string TK3PEvent_Operation_DELETE = "DELETE";
        public const string TK3PEvent_Operation_ADDATTACH = "ADD ATTACH";
        public const string TK3PEvent_Operation_REMOVEATTACH = "REMOVE ATTACH";
        public const string TK3PEvent_Operation_SETSTATUS = "SET STATUS";
        public const string TK3PEvent_Operation_UPLOADAPFILE = "UPLOAD AP FILE";
        public const string TK3PEvent_Operation_SETRELEASEDATE = "SET RELEASE DATE";
        public const string TK3PEvent_Operation_DOWNLOADAPFILE = "DOWNLOAD AP FILE";
        public const string TK3PEvent_Operation_CLOSE = "CLOSE";
        //public const string TK3PEvent_Operation_UPDATE = "UPDATE";
        //public const string TK3PEvent_Operation_REOPEN = "REOPEN";
        //public const string TK3PEvent_Operation_ASSIGN = "ASSIGN";
        //public const string TK3PEvent_Operation_CHANGE_USER_REGISTRATION = "CHANGE USER REGISTRATION";
        //public const string TK3PEvent_Operation_SUSPEND = "SUSPEND";
        //public const string TK3PEvent_Operation_CLOSED = "CLOSED";
        //public const string TK3PEvent_Operation_ADDNOTE = "ADD NOTE";


        // ***************************************************************
        // WARRANTY SPARE PART (WSP)
        // ***************************************************************

        public const string WSPRequest_Code_NEW = "<new>";

        // Status 
        public const string WSPRequest_Status_DRAFT = "DRAFT";
        public const string WSPRequest_Status_PENDING = "PENDING";
        public const string WSPRequest_Status_PROCESSING = "PROCESSING";
        public const string WSPRequest_Status_CLOSED = "CLOSED";
        public const string WSPRequest_Status_DELETED = "DELETED";

        // Operation
        public const string WSPEvent_Operation_CREATE = "CREATE";
        public const string WSPEvent_Operation_DELETE = "DELETE";
        public const string WSPEvent_Operation_ASSIGN = "ASSIGN";
        public const string WSPEvent_Operation_CLOSED = "CLOSED";
        public const string WSPEvent_Operation_RESTORE = "RESTORE";
        public const string WSPEvent_Operation_ADDATTACH = "ADD ATTACH";
        public const string WSPEvent_Operation_REMOVEATTACH = "REMOVE ATTACH";
        public const string WSPEvent_Operation_SETCONSIGNMENTNOTE = "SET CONSIGNMENT NOTE";

        public const string WSPEvent_Operation_SetWarrantyStatus = "SET_WARRANTY_STATUS";
        public const string WSPEvent_Operation_SetExchangeRequest = "SET_EXCHANGE_REQUEST";
        public const string WSPEvent_Operation_SetCompliance = "SHIPMENT_OF_THE_NEW_PART_FROM_SUPPLIER";   // Shipment of the new part from supplier 
        public const string WSPEvent_Operation_SetAction = "SET_ACTION";  // Defective part management 
        public const string WSPEvent_Operation_SetInvoiceInfo = "SET_INVOICE_INFORMATION";
        public const string WSPEvent_Operation_EngagementEmail = "ENGAGEMENT_EMAIL";

        public const string Warranty_Email_NewRequest = "_NEW_WARRANTY_REQUEST";
        public const string Warranty_Email_SetWarrantyStatus = "_SET_WARRANTY_STATUS_{0}";   // DA COMPLETARE con il CODICE DELLO STATUS !!! 
        public const string Warranty_Email_CloseRequest = "_CLOSE_WARRANTY_REQUEST";

        public enum WSPAttachType : int
        {
            file = 0,
            url
        }

        public const string glb_sessionkey_WSPRequests_LastActionIndex = "_WSPRequestsIndex_LastIndexName";
        public const string glb_sessionkey_WSPRequests_Index = "_WSPRequests_Index";
        public const string glb_sessionkey_WSPRequests_Manage = "_WSPRequests_Manage";

        // ***************************************************************
        // BOOKING
        // ***************************************************************
        // Status
        public const string Booking_Status_WAITING = "WAITING";
        public const string Booking_Status_CONFIRMED = "CONFIRMED";
        public const string Booking_Status_DELETED = "DELETED";

        // Status DECO
        public const string Booking_StatusDeco_WAINING = "To Be Approved";
        public const string Booking_StatusDeco_CONFIRMED = "Confirmed";
        public const string Booking_StatusDeco_DELETED = "Deleted";

        public const string Booking_Email_NewReservation = "_NEW_RESERVATION";
        public const string Booking_Email_ModifyReservation = "_MODIFY_RESERVATION";
        public const string Booking_Email_DeleteReservation = "_DELETE_RESERVATION";

        // ***************************************************************
        // KPI - KEY PERFORMANCE INDICATORS
        // ***************************************************************
        public const string KPI_Category_ACTIONS = "CORRECTIVE AND PREVENTIVE ACTIONS";
        public const string KPI_Category_CHANGE = "CHANGE CONTROL (CC)";
        public const string KPI_Category_CLAIM = "NON-COMPLIANT";
        public const string KPI_Category_UNCOMPLIANCE = "NON CONFORMITIES (NC)";

        // ***************************************************************
        // EMAIL TEMPLATE 
        // ***************************************************************
        public const string EmailTemplate_Area_TRAINING = "TRAINING";
        public const string EmailTemplate_Area_WEBREPOSITORY = "WEBREPOSITORY";
        public const string EmailTemplate_Area_SUPPORT = "SUPPORT";
        public const string EmailTemplate_Area_SUPPORT_OPENAP = "SUPPORTOPENAP3P"; 
        public const string EmailTemplate_Area_WARRANTY = "WARRANTY";
        public const string EmailTemplate_Area_ASSAYNOTIFICATION = "ASSAYNOTIFICATION";
        public const string EmailTemplate_Area_BOOKING = "BOOKING"; 
        public const string EmailTemplate_Area_REFURBISH = "REFURBISH"; 
        public const string EmailTemplate_Area_GENERIC = "GENERIC";

        // ***************************************************************
        // CODES 
        // ***************************************************************
        public const string Code_WSP_WarrantyStatus = "WSP_WARRANTYSTATUS"; 
        public const string Code_WSP_ExchangeRequest = "WSP_EXCHANGEREQUEST";
        public const string Code_WSP_Compliant = "WSP_COMPLIANT";
        public const string Code_WSP_Action = "WSP_ACTION"; 
        public const string Code_WSP_InstrumentFilters = "WSP_INSTR_FILTERS";
        public const string Code_WSP_ClosingOptions = "WSP_CLOSING_OPTIONS";
        public const string Code_WSP_OriginalPart = "WSP_ORIGINAL_PART";
        public const string Code_DL_ListType = "DL_LIST_TYPE";
        public const string Code_DL_Group_ListType = "DL_GROUP_LIST_TYPE";
        public const string Code_COMPLAINT_STATUS = "COMPLAINT_STATUS";

        // Valori specifici 
        public const string Code_WSP_NONE_id = "*NONE*"; 
        public const string Code_WSP_NONE_desc = "(NO VALUE)"; 
        public const string Code_WSP_ANY_id = "*ANY*"; 
        public const string Code_WSP_ANY_desc = "(ANY VALUE)"; 

        public const string Code_WSP_WarrantyStatus_NO_WARRANTY = "WST000";
        public const string Code_WSP_WarrantyStatus_WITH_REPLACEMENT = "WST001";
        public const string Code_WSP_WarrantyStatus_NO_REPLACEMENT = "WST002";

        public const string Code_WSP_ExchangeRequest_Unnecessary = "WEX000";
        public const string Code_WSP_ExchangeRequest_RequestToTheProvider = "WEX001";

        public const string Code_WSP_Action_Send = "WACT001";

        public const string Code_WSP_Compliant_YES = "WCMP001";
        public const string Code_WSP_OriginalPart_YES = "WORIGYES";
        public const string Code_WSP_OriginalPart_NO = "WORIGNO";

        public const string Code_WSP_InstrumentFilters_WithOpenRequests = "WINST01";
        public const string Code_WSP_InstrumentFilters_WithCloseRequests = "WINST02";
        public const string Code_WSP_InstrumentFilters_WithAtLeast1Request = "WINST03";
        
        // valori specifici per tracciare la modalità della chiusura (valore riepilogativo)
        public const string Code_WSP_ClosingOptions_NO_WARRANTY = "WCLOPT90";       
        public const string Code_WSP_ClosingOptions_NO_REPLACEMENT = "WCLOPT91";    
        public const string Code_WSP_ClosingOptions_REPLACEMENT_NOT_REQUESTED = "WCLOPT92";

        // Distribution List - List TYPE 
        public const string Code_DL_ListType_Supplier = "DLTYPE01";
        public const string Code_DL_ListType_OrderDepartments = "DLTYPE02";
        public const string Code_DL_ListType_PurchaseDepartments = "DLTYPE03";
        public const string Code_DL_ListType_AssaysNotification = "DLTYPE04";
        public const string Code_DL_ListType_TrainingAdministrationOffice = "DLTYPE05";

        public const string glb_sessionkey_ManageDL_GroupId = "_ManageDL_GroupId";

        // valori specifici per tracciare lo STATUS dei Reclami (COMPLAINT)
        public const string Code_Complaint_Status_REJECT = "CMPSTATUS1";
        public const string Code_Complaint_Status_FORWARD_TO_CCA = "CMPSTATUS2";
        public const string Code_Complaint_Status_OPEN_ON_LINKOMM = "CMPSTATUS3";
        public const string Code_Complaint_Status_REQUEST_DATA = "CMPSTATUS4";

        // ***************************************************************
        // REFURBISH
        // ***************************************************************

        // Status 
        public const string Refurbish_Status_WAITING = "WAITING";
        public const string Refurbish_Status_PROCESSING = "PROCESSING";
        public const string Refurbish_Status_COMPLETE = "COMPLETE";
        public const string Refurbish_Status_READY = "READY";
        public const string Refurbish_Status_CLOSED = "CLOSED";
        public const string Refurbish_Status_DELETED = "DELETED";

        public const string Refurbish_StatusDeco_WAITING = "Waiting";
        public const string Refurbish_StatusDeco_PROCESSING = "Processing";
        public const string Refurbish_StatusDeco_COMPLETE = "Complete";
        public const string Refurbish_StatusDeco_READY = "Ready to be Shipped";
        public const string Refurbish_StatusDeco_CLOSED = "Closed";

        public static List<LookUpString> ListRefurbishStatus = new List<LookUpString>{
                    new LookUpString() { id = Lookup.Refurbish_Status_WAITING, descrizione = Refurbish_StatusDeco_WAITING },
                    new LookUpString() { id = Lookup.Refurbish_Status_PROCESSING, descrizione = Refurbish_StatusDeco_PROCESSING },
                    new LookUpString() { id = Lookup.Refurbish_Status_COMPLETE, descrizione = Refurbish_StatusDeco_COMPLETE },
                    new LookUpString() { id = Lookup.Refurbish_Status_READY, descrizione = Refurbish_StatusDeco_READY },
                    new LookUpString() { id = Lookup.Refurbish_Status_CLOSED, descrizione = Refurbish_StatusDeco_CLOSED }
        };

        public const string Refurbish_Operation_CREATE = "CREATE";
        public const string Refurbish_Operation_DELETE = "DELETE";
        public const string Refurbish_Operation_CHANGESTATUS = "CHANGESTATUS";
        public const string Refurbish_Operation_REOPEN = "REOPEN";
        public const string Refurbish_Operation_CLOSE = "CLOSE";
        public const string Refurbish_Operation_READY = "READY";

        public const string Refurbish_Email_NewRequest = "_REFURBISH_NEW_REQUEST";
        public const string Refurbish_Email_ReadyToBeShipped = "_REFURBISH_READY";

        // ***************************************************************
        // StdTables
        // ***************************************************************
        public const string StdTab_Assay_Analyte = "ASSAY_ANALYTE";
        public const string StdTab_Assay_Panel = "ASSAY_PANEL";
        public const string StdTab_Assay_BusinessType = "ASSAY_BUSINESS_TYPE";

        public const string StdTab_VERSIONS = "CFS";
        public const string StdTab_COUNTRIES = "NAZ";

        // ***************************************************************
        // TabGenConfig
        // ***************************************************************
        public const string TabGenConfig_CommercialStatusISR = "COMMERCIALSTATUSISR";

        // ***************************************************************
        // GENERAL
        // ***************************************************************
        public const string Function_Documentation_DailyExecution = "Daily Execution";
        public const string Function_Documentation_AssayUtilization = "Assay Utilization";
        public const string Function_Documentation_UserRoles = "User Roles";
        public const string Function_Documentation_DataImport = "Data Import";
        public const string Function_Documentation_DataMining = "Data Mining";
        public const string Function_Documentation_LogFiles = "Log Files";
        public const string Function_Documentation_UserManual = "User Manual";
        public const string Function_Documentation_RnDExtractions = "RnD Analysis";
        public const string Function_Documentation_DocumentalRepository = "Documental Repository";
        public const string Function_Documentation_Support_YourRequests = "Support/Status of your Tickets";
        public const string Function_Documentation_Support_Manage = "Support/Manage Tickets";
        public const string Function_Documentation_Support_Complaint = "Support/Manage Complaint";
        public const string Function_Documentation_Support_3PRequests = "Support/Third Party Assay Protocols";
        public const string Function_Documentation_Support_Statistics = "Support/Statistics";
        public const string Function_Documentation_Warrenty = "Warrenty";
        public const string Function_Documentation_Warrenty_Manage = "Warranty Management";
        public const string Function_Documentation_Instruments_Booking = "Instruments Booking";
        public const string Function_Documentation_Maintenance = "Instruments Maintenance";
        public const string Function_Documentation_Refurbish = "Refurbish Instruments";

        public static string idCache_DailyExecutionsFilters = "DailyExecutionsFilters";
        public static string idCache_AssaysUtilizationFilters = "AssaysUtilizationFilters";

        public const int Instrument_ModelTypeKey_InGenius = 542118061;  // univoci generati casualmente
        public const int Instrument_ModelTypeKey_BeGenius = 324302932; // univoci generati casualmente
        public const int Instrument_ModelTypeKey_Other = 841400913; // univoci generati casualmente
        public const string Instrument_ModelType_InGenius = "INGENIUS";
        public const string Instrument_ModelType_BeGenius = "BEGENIUS";
        public const string Instrument_ModelType_Other = "OTHER";
        public const string Instrument_ModelType_All = "*ALL";
        public const string Instrument_ModelTypeDeco_InGenius = "ELITe InGenius";
        public const string Instrument_ModelTypeDeco_BeGenius = "ELITe BeGenius";
        public const string Instrument_ModelTypeDeco_Other = "Other Instr.";
        public const string Instrument_ModelTypeDeco_All = "All Models";

        public const string Instrument_SGAT_NewSitePrefix = "dhb";
        public const string Instrument_InstrumentUpdateRequest_OptCurrentSite = "Update Current Site";
        public const string Instrument_InstrumentUpdateRequest_OptChangeSite = "Change Site";
        public const string Instrument_InstrumentUpdateRequest_OptNewSite = "Create new Site";

        public const string Instrument_MaintenanceType_Calibration = "Calibration";
        public const string Instrument_MaintenanceType_Pack = "Pack";

        public static List<LookUpString> InstrumentModelType_EliteInstruments = new List<LookUpString>{
                    new LookUpString() { id = Instrument_ModelType_InGenius, descrizione = Instrument_ModelTypeDeco_InGenius, idNum = Instrument_ModelTypeKey_InGenius },
                    new LookUpString() { id = Instrument_ModelType_BeGenius, descrizione = Instrument_ModelTypeDeco_BeGenius, idNum = Instrument_ModelTypeKey_BeGenius }
        };
        public static List<LookUpString> InstrumentModelTypeFilters = new List<LookUpString>{
                    new LookUpString() { id = Instrument_ModelType_InGenius, descrizione = Instrument_ModelTypeDeco_InGenius, idNum = Instrument_ModelTypeKey_InGenius },
                    new LookUpString() { id = Instrument_ModelType_BeGenius, descrizione = Instrument_ModelTypeDeco_BeGenius, idNum = Instrument_ModelTypeKey_BeGenius },
                    new LookUpString() { id = Instrument_ModelType_Other, descrizione = Instrument_ModelTypeDeco_Other, idNum = Instrument_ModelTypeKey_Other }
        };
        public static List<LookUpString> InstrumentModelTypeFiltersWithAll = new List<LookUpString>{
                    new LookUpString() { id = Instrument_ModelType_InGenius, descrizione = Instrument_ModelTypeDeco_InGenius, idNum = Instrument_ModelTypeKey_InGenius },
                    new LookUpString() { id = Instrument_ModelType_BeGenius, descrizione = Instrument_ModelTypeDeco_BeGenius, idNum = Instrument_ModelTypeKey_BeGenius },
                    new LookUpString() { id = Instrument_ModelType_All, descrizione = Instrument_ModelTypeDeco_All, idNum = 0 }
        };
        public static string GetInstrumentModelTypeId(int? idNum)
        {
            string ret = "";
            if (idNum != null)
            {
                ret = InstrumentModelTypeFilters.Where(t => t.idNum == idNum).Select(t => t.id).FirstOrDefault();
            }
            return (ret ?? "UNDEFINED");
        }
        public static string GetInstrumentModelDescbyCode(string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
            {
                return InstrumentModelTypeFilters.Where(t => t.id.Equals(code)).Select(t => t.descrizione).FirstOrDefault();
            }
            return code;
        }
        public static string GetInstrumentModelTypeDesc(int? idNum)
        {
            string ret = "";
            if (idNum != null)
            {
                ret = InstrumentModelTypeFilters.Where(t => t.idNum == idNum).Select(t => t.descrizione).FirstOrDefault();
            }
            return (ret ?? "UNDEFINED");
        }

        public static string SpecialMode_ChangePassword = "ChangePassword";

        public const string glb_YES = "YES";
        public const string glb_NO = "NO";

        private static string cache_connectionString = "";
        private static string cache_AppTitle = "";
        private static string cache_CompanyName = "";
        private static string cache_SupportEmail = "";
        private static string cache_SupportSign = "";
        private static bool? cache_ComplaintActive = null;
        private static bool? cache_OpenAP3PActive = null;
        private static bool? cache_InstallBaseISRActive = null;
        private static List<TKSupportConfigModel> cache_TKSupportConfig = null;
        //private static string cache_CommercialEntityForInstrumentUpdate = "";
        public static string getConnectionString
        {
            get
            {   
                // x ottimizzare gli accessi, viene letto solo la prima volta
                if (String.IsNullOrWhiteSpace(cache_connectionString))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_connectionString = configurationBuilder.Build().GetConnectionString("NPOCOConnection");
                }
                return cache_connectionString;
            }
        }

        public static string GetConfigAdminEmail
        {
            get
            {   
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("Config:Admin").Value;
            }
        }

        public static string GetEnvironment
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("Config:Environment").Value;
            }
        }
        public static string GetAppTitle
        {
            get
            {
                if (string.IsNullOrWhiteSpace(cache_AppTitle))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_AppTitle =  configurationBuilder.Build().GetSection("Config:AppTitle").Value;
                }
                return cache_AppTitle;
            }
        }
        public static string GetCompanyName
        {
            get
            {
                if (string.IsNullOrWhiteSpace(cache_CompanyName))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_CompanyName = configurationBuilder.Build().GetSection("Config:CompanyName").Value;
                }
                return cache_CompanyName;
            }
        }
        public static string GetSupportEmail
        {
            get
            {
                if (string.IsNullOrWhiteSpace(cache_SupportEmail))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_SupportEmail = configurationBuilder.Build().GetSection("Config:SupportEmail").Value;
                }
                return cache_SupportEmail;
            }
        }
        public static string GetSupportSign
        {
            get
            {
                if (string.IsNullOrWhiteSpace(cache_SupportSign))
                {
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    cache_SupportSign = configurationBuilder.Build().GetSection("Config:SupportSign").Value;
                }
                return cache_SupportSign;
            }
        }
        public static string GetCommercialEntityForInstrumentUpdate
        {
            get
            {
                //if (string.IsNullOrWhiteSpace(cache_CommercialEntityForInstrumentUpdate))
                //{
                var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("DataImport:CommercialEntity4InstrumentUpdate").Value;
                //cache_CommercialEntityForInstrumentUpdate = configurationBuilder.Build().GetSection("DataImport:CommercialEntity4InstrumentUpdate").Value;
            //}
            //    return cache_CommercialEntityForInstrumentUpdate;
            }
        }


        //public static string getSupportEmail
        //{
        //    get
        //    {
        //        var configurationBuilder = new ConfigurationBuilder()
        //        .SetBasePath(Directory.GetCurrentDirectory())
        //        .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
        //        return configurationBuilder.Build().GetSection("EmailCfg:EmailNotifySUPPORT").Value;
        //    }
        //}

        //public static string GetHTMLPrivacy
        //{
        //    get
        //    {
        //        var configurationBuilder = new ConfigurationBuilder()
        //            .SetBasePath(Directory.GetCurrentDirectory())
        //            .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
        //        string html = "<p>" + configurationBuilder.Build().GetSection("html:Privacy1").Value + "</p>" +
        //                      "<p>" + configurationBuilder.Build().GetSection("html:Privacy2").Value + "</p>" +
        //                      "<p>" + configurationBuilder.Build().GetSection("html:Privacy3").Value + "</p>";
        //        return html;
        //    }
        //}

        public static int GetClassificationId4AP3P()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
            int ret = 0;
            try
            {
                ret = Int32.Parse(configurationBuilder.Build().GetSection("AP3P:ClassificationIdForSupport").Value);
            }
            catch (Exception e) { }
            return ret;
        }

        private static string cache_version = "";
        public static string getVersion
        {
            get
            {
                // x ottimizzare gli accessi, viene letto solo la prima volta
                if (String.IsNullOrWhiteSpace(cache_version))
                {
                    List<WebApp.Entity.Version> dati = (new VersionsBO()).Get();
                    //string WebApp = dati.Where(t => t.Code.Equals("WEBAPP")).Select(t => t.Description).FirstOrDefault();
                    //string StateMachine = dati.Where(t => t.Code.Equals("STATEMACHINE")).Select(t => t.Description).FirstOrDefault();
                    Assembly asm = Assembly.GetEntryAssembly();
                    FileInfo fi = new FileInfo(asm.Location);
                    DateTime myBuildDate = fi.LastWriteTime;
                    string build = String.Format("{0}.{1}.{2}.{3}", myBuildDate.Year - 2018, myBuildDate.DayOfYear, myBuildDate.Hour, myBuildDate.Minute);
                    cache_version = String.Format("WebApp {0} Build {1} - {2} {3}", "2.04.02", build, 
                                                                dati.Select(t => t.Code).FirstOrDefault(), 
                                                                dati.Select(t => t.Description).FirstOrDefault());
                }
                return cache_version;
            }
        }
		
        public static string GetValidFileName(string fileName)
        {
            // remove any invalid character from the filename.
            String ret = Regex.Replace(fileName.Trim(), "[^A-Za-z0-9_. ]+", "");
            return ret.Replace(" ", String.Empty);
        }

        // Parametri default per MAINTENANCE 
        private static int cache_Maintenance_WarningAfterGG = 0;
        private static int cache_Calibration_GGNext = 0;
        private static int cache_Calibration_WarningAfterGG = 0;
        private static int cache_Fibers_Default = 0;
        private static decimal cache_Fibers_WarningAfterPerc = 0;

        public static int GetInstrumentMaintenanceWarningAfterGG()
        {
            if (cache_Maintenance_WarningAfterGG == 0)
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                try
                {
                    cache_Maintenance_WarningAfterGG = Int32.Parse(configurationBuilder.Build().GetSection("Instruments:Maintenance_WarningAfterGG").Value);
                }
                catch (Exception e) { }
            }
            return cache_Maintenance_WarningAfterGG;
        }
        public static int GetInstrumentCalibrationGGNext()
        {
            if (cache_Calibration_GGNext == 0)
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                try
                {
                    cache_Calibration_GGNext = Int32.Parse(configurationBuilder.Build().GetSection("Instruments:Calibration_GGNext").Value);
                }
                catch (Exception e) { }
            }
            return cache_Calibration_GGNext;
        }
        public static int GetInstrumentCalibrationWarningAfterGG()
        {
            if (cache_Calibration_WarningAfterGG == 0)
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                try
                {
                    cache_Calibration_WarningAfterGG = Int32.Parse(configurationBuilder.Build().GetSection("Instruments:Calibration_WarningAfterGG").Value);
                }
                catch (Exception e) { }
            }
            return cache_Calibration_WarningAfterGG;
        }
        public static int GetInstrumentFibersDefault()
        {
            if (cache_Fibers_Default == 0)
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                try
                {
                    cache_Fibers_Default = Int32.Parse(configurationBuilder.Build().GetSection("Instruments:Fibers_Default").Value);
                }
                catch (Exception e) { }
            }
            return cache_Fibers_Default;
        }
        public static decimal GetInstrumentFibersWarningAfterPerc()
        {
            if (cache_Fibers_WarningAfterPerc == 0)
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                try
                {
                    cache_Fibers_WarningAfterPerc = decimal.Parse(configurationBuilder.Build().GetSection("Instruments:Fibers_WarningAfterPerc").Value);
                }
                catch (Exception e) { }
            }
            return cache_Fibers_WarningAfterPerc;
        }
        public static string GetInfoFromDashboardURL
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("InfoFromDashboard:Url").Value;
            }
        }
        public static string GetInfoFromDashboardAuthorization
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("InfoFromDashboard:Authorization").Value;
            }
        }
        public static string GetInfoFromDashboardCompany
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("InfoFromDashboard:Company").Value;
            }
        }
        public static string GetInfoFromDashboardBranch
        {
            get
            {
                var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                return configurationBuilder.Build().GetSection("InfoFromDashboard:Branch").Value;
            }
        }
        public static bool GetComplaintActive
        {
            get
            {
                if (cache_ComplaintActive == null)
                {
                    cache_ComplaintActive = false;
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    string valore = configurationBuilder.Build().GetSection("Config:Complaint").Value;
                    try
                    {
                        cache_ComplaintActive = bool.Parse(valore);
                    }
                    catch (Exception e) { }
                }
                return (bool)cache_ComplaintActive;
            }
        }
        public static bool GetOpenAP3PActive
        {
            get
            {
                if (cache_OpenAP3PActive == null)
                {
                    cache_OpenAP3PActive = false;
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    string valore = configurationBuilder.Build().GetSection("Config:OpenAP3P").Value;
                    try
                    {
                        cache_OpenAP3PActive = bool.Parse(valore);
                    }
                    catch (Exception e) { }
                }
                return (bool)cache_OpenAP3PActive;
            }
        }
        public static bool GetInstallBaseISRActive
        {
            get
            {
                // leggo sempre per avere il dato aggiornato
                //if (cache_InstallBaseISRActive == null)
                //{
                    cache_InstallBaseISRActive = false;
                    var configurationBuilder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings_user.json", optional: false, reloadOnChange: true);
                    string valore = configurationBuilder.Build().GetSection("Config:InstallBaseISR").Value;
                    try
                    {
                        cache_InstallBaseISRActive = bool.Parse(valore);
                    }
                    catch (Exception e) { }
                //}
                return (bool)cache_InstallBaseISRActive;
            }
        }
        public static TKSupportConfigModel GetTKSupportConfig(string id)
        {
            GetTKSupportConfigs();
            TKSupportConfigModel ret = cache_TKSupportConfig.Find(t => t.ID.Equals(id));
            return ret;
        }
        public static List<TKSupportConfigModel> GetTKSupportConfigs()
        {
            if (cache_TKSupportConfig == null)
            {
                try
                {
                    using (StreamReader r = new StreamReader("TKSupportConfig.json"))
                    {
                        string json = r.ReadToEnd();
                        cache_TKSupportConfig = JsonConvert.DeserializeObject<List<TKSupportConfigModel>>(json);
                    }

                }
                catch (Exception e) { }
            }
            return cache_TKSupportConfig;
        }

    }
}





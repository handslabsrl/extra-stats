﻿#region Using
using System;
using System.ComponentModel;
using System.Data.SqlClient;
#endregion

namespace EDFS.Db
{
    public class SGAT
    {

        public string CustomerCode { get; set; }
        public string CompanyName { get; set; }
        public string Site_Code { get; set; }
        public string Site_Description { get; set; }
        public string Site_Address { get; set; }
        public string Site_City  { get; set; }
        public string Site_Country { get; set; }
        public string SerialNumber { get; set; }
        public DateTime? Installation_Date { get; set; }
        public string Commercial_Status { get; set; }
        public string Finance_Status { get; set; }
        public static string GetGeneralExtraction()
        {
            string sql="";
            return sql;
        }
        public static string GetGeneralExtraction(DateTime startDate, DateTime endDate)
        {
            string sql="";
            return sql;
        }

    }
    public class SGATCollection : BindingList<SGAT>
    {

    }
}

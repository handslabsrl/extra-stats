-- DROP TABLE ZZ_list_actions_CAPA
IF NOT EXISTS (
    select * from sysobjects where name='ZZ_list_actions_CAPA' and xtype='U'
) CREATE TABLE ZZ_list_actions_CAPA (
    [N] NVARCHAR(50),
    [Tipo] NVARCHAR(50),
    [Data_apertura] NVARCHAR(50),
    [Aperta_da] NVARCHAR(50),
    [Area] NVARCHAR(50),
    [Riferimento_a] NVARCHAR(50),
    [N_1] NVARCHAR(50),
    [Descrizione_prodotto] NVARCHAR(250),
    [Oggetto_del_cambiamento_Correzione] NVARCHAR(250),
    [Esito_indagine_Causa_primaria] NVARCHAR(4000),
    [Descrizione] NVARCHAR(4000),
    [Aree_d_impatto] NVARCHAR(50),
    [a_cambiamento_avvenuto_Valutazione_rischio_Costi_Benefici] NVARCHAR(4000),
    [Data_approvazione] NVARCHAR(250),
    [Azione] NVARCHAR(4000),
    [Allegati] NVARCHAR(MAX),
    [Funzioni_coinvolte] NVARCHAR(50),
    [Data_approvazione_1] NVARCHAR(250),
    [Data_di_implementazione] NVARCHAR(250),
    [Data_completamento] NVARCHAR(250),
    [DMRI_Prodotto] NVARCHAR(50),
    [DMRI_Prodotto_testo] NVARCHAR(500),
    [Comunicazione_al_cliente] NVARCHAR(50),
    [Comunicazione_al_cliente_testo] NVARCHAR(500),
    [Impatto_sul_prodotto_immesso_in_commercio] NVARCHAR(50),
    [Impatto_sul_prodotto_immesso_in_commercio_testo] NVARCHAR(2000),
    [Valutazione_dei_requisiti_regolatori] NVARCHAR(2000),
    [Processo_VMP] NVARCHAR(250),
    [Sottoprocesso] NVARCHAR(250),
    [Risk_Assessment] NVARCHAR(500),
    [Notifica_Autorit] NVARCHAR(50),
    [Descrizione_Notifica_Autorit] NVARCHAR(500),
    [Notifica_Virtual_Manufacturer] NVARCHAR(50),
    [Descrizione_Notifica_Virtual_Manufacturer] NVARCHAR(500),
    [Project_leader] NVARCHAR(50),
    [Modifica_analisi_del_rischio_prodotto] NVARCHAR(50),
    [Interventi_analisi_del_rischio] NVARCHAR(2000),
    [Termine_valutazione_efficacia] NVARCHAR(50),
    [Modalit_verifica_efficacia] NVARCHAR(4000),
    [Efficacia_verificata_da] NVARCHAR(50),
    [Esito_valutazione_efficacia] NVARCHAR(50),
    [Ulteriori_attivit] NVARCHAR(4000),
    [Termine_valutazione] NVARCHAR(4000),
    [Efficacia_dopo_proroga] NVARCHAR(50),
    [Data_di_implementazione_1] NVARCHAR(50),
    [Data_chiusura_azioni] NVARCHAR(50),
    [Data_chiusura_efficacia] NVARCHAR(50)
);
INSERT INTO ZZ_list_actions_CAPA VALUES
    (N'19-28',N'Correttiva','2019-09-27 00:00:00',N'Stefania Brun',N'Sistema qualità',N'Non conformità',NULL,N'AR14/Mi-01 Significant changes were implemented without prior approval of the Notified Body',N'Documentazione SGQ',N'The product change was notified to DEKRA after its implementation according to the procedure for Change Control (PR05,05 "Gestione delle Modifiche" Ed 03, Rev 04) which placed the notification of the significant changes to the NB (DEKRA) after its actual implementation. This inappropriate process was mainly due to a misinterpretation of DEKRA directions received over the years: EGSPA understood that, thanks to its Full Quality Assurance System, implementing changes without waiting for a "go/no go" decision by DEKRA was allowed and that the changes were checked, if needed, during the next on-site audit.',N'Understood the substance of misinterpretation, taken in consideration the 98/79/EC (Annex IV #3.4, 4.4, 4.5), the Guidance NBOG rev 04 and the last page of the DEKRA Notice of Change Form (with the table of NB decision) the the following action will be part of the corrective plan:
-	above mentioned procedure PR05,05 will be changed in order to clearly define the sequence of the process steps. Specifically: any substantial change has to be notified to DEKRA, to the Competent Authority and to the Virtual Manufacturer before its implementation and, in case of products included in Annex II List B, change implementation depends on DEKRA decision. 
-	similar update will be done with the  PR 14,01  Preventive and Corrective Actions since currently there are NO clear directions in that procedure about the timeline of changes notification to DEKRA, Competent Authority and Virtual Manufacturer . 
-	other procedures, including but not limited to those regarding Product Development, Product Validation, Product Manufacturing will be reviewed in order to assess the need of any update and/or clarification 
-	training about change management and change notification will be given to the all dept involved by this matter including but not limited to QARA, R&D, Manufacturing, Purchase, Product Validation, Technical Support.',NULL,N'Positive since this action allows to comply with IVDD regulation prescription regarding notification of substantial change to Notified Body and Competent Authority 
Positive since this action allows to comply with IVDD regulation prescription regarding notification of substantial change to Notified Body and Competent Authority 
Positive since this action allows to comply with IVDD regulation prescription regarding notification of substantial change to Notified Body and Competent Authority. Negative because it necessary to plan additional work to manage the substantial changes (preliminary notification to Notify Body and Competent Authority, submission of documents to the Notify Body, if required and communication to Notify Body, Competent Authority and Virtual Manufacturer about the change conclusion). Moreover it is necessary to forecast longer time for closure of RAC / RAP / CC related to product included in Annex II List B, due to possible Notify Body approval  (if document submission is requested).
',N'27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
24/10/2019
24/10/2019
27/09/2019
27/09/2019
27/09/2019',N'To review  PR05,05 Change Control, PR14,01 Preventive and Corrective Actions in order to specify that any substantial change has to be notified to DEKRA, to the Competent Authority and to the Virtual Manufacturer  before its implementation and in case of products included in Annex II List B, change implementation depends on DEKRA decision. 
Release the modified and approved documents
Verify which substantial changes (from CC/RAC/RAP/projects) related to products included in Annex II List B are ongoing and send to Dekra the Notice of Change before the change implementation. About Competent Authority, preliminary notification will be sent starting from new CC/RAC/RAP/project opened after new PR05,05 revision approval.
Training including but not limited to QARA, R&D, Manufacturing, Purchase, Product Validation, Technical Support.
Check the RAC / RAP / CC projects and extensions used before the NC and make a preliminary notification to DEKRA, if necessary.
Each change must be communicate to SGQ in the p',N'PR05,05_05, IO05,05_00 in uso al 06/12/19, formazione del 6/12/2019. PR14,01 non necessita di essere aggiornata. - rac19-28_messa_in_uso_pr0505_05_e_io0505_00_rac19-28_rap18-19_cc19-4_mod1802_del_061219.msg
Notifica preliminare delle modifiche identificate come significative a DEKRA.  - rac19-28_elitechgroup_spa_notice_of_change.msg
MOD18,02 del 06/12/2019 - rac19-28_2019-12_6_formazione_pr05,05_05_processogestionenotificacambiamenti.pptx
MOD18,02 del 06/12/2019 - rac19-28_2019-12_6_formazione_pr05,05_05_processogestionenotificacambiamenti_1.pptx
MOD04,15_05, MOD04,18_05 IN USO AL 16/12/2019. MOD18,02 DEL 06/12/2019 - rac19-28_messa_in_uso_liste_di_riscontro_rac19-28_mod1802_06122019.msg
PR04,01_03_PR04,04_02 - rac19-28_messa_in_uso_pr0401_03_pr0404_02_rac1819_rac19-28.msg
MOD04,25_03, MOD04,26_03 IN USO AL 16/12/2019. MOD18,02 DEL 06/12/2019  - rac19-28_messa_in_uso_liste_di_riscontro_rac19-28_mod1802_06122019_1.msg
 PR04,01_03_PR04,04_02  - rac19-28_messa_in_uso_pr0401_03_pr0404_02_rac1819_rac19-28_1.msg
PR05,05_05, IO05,05_00 in uso al 06/12/19, formazione del 6/12/2019. PR14,01 non necessita di essere aggiornata.  - rac19-28_messa_in_uso_pr0505_05_e_io0505_00_rac19-28_rap18-19_cc19-4_mod1802_del_061219_1.msg',NULL,N'27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019
27/09/2019',N'02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
02/12/2019
27/09/2019',N'06/12/2019
06/12/2019
29/11/2019
06/12/2019
06/12/2019
30/03/2020
30/03/2020
27/09/2019',N'No',N'No since change affect procedure which doesn''t impact on DMRI',N'No',N'No',N'No',N'No',N'No since the change allows to comply with IVDD regulation prescription ',N'GM - Gestione delle modifiche',N'GM15 - Valutazione  notifica Autorità Competente / Organismo Notificato',N'GM15',N'No',N'No since the change doesn''t need to be notified ',N'No',N'no impact',N'Roberta  Paviolo',N'No',NULL,'2020-03-31 00:00:00',N'Verification of Effectiveness, as per Corrective Action procedure in place, will be performed through evaluation of at least 3 appropriate evidences. In this case evidences will be represented by next 3 Notices of Change which EGSPA is going to performed to DEKRA within the due timeline (6 months).  
In date 29-11-2019 were notify this changes:
1.	Request of Preventive Action RAP 19-13: update of the IFU of the product «CMV ELITe Standard» (code STD015PLD). 
2.	Change Control CC19-10: change in the packaging and labelling of the product «CMV ELITe Standard» (code STD015PLD).
3.	Project SCP2018-009: extension of the intended use of the product «CMV ELITe MGB® Kit» (code RTK015PLD) with the new matrices Bronchial Aspirate and Broncho Alveolar Lavage in association with the automatic integrated system «ELITe InGenius®» (code INT030).  
DEKRA approves the change in data 27/02/2020. 
From settember to march have been no other changes.
The procedure PR05,05 "Managment of change" and all forms associated are in use from december/2019, the other procedure have been aligned in march/2020 (training in form MOD18,02).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-12-02 00:00:00','2020-03-30 00:00:00','2020-03-30 00:00:00'),
    (N'20-7',N'Preventiva','2020-03-04 00:00:00',N'Anna Capizzi',N'Ricerca e sviluppo',NULL,NULL,NULL,N'Documentazione SGQ',N'In seguito all''analisi statistica dei dati relativi ai test di Controllo Qualità e dei test di Stabilità eseguiti su quattro Lotti Pilota emerge la necessità di modificare i criteri di accettazione per il rilascio dei lotti di produzione in quanto gli attuali criteri di CQ risultano troppo permissivi.',N'Si procederà con l''adeguamento con i nuovi criteri di accettazione di CQ nel modulo di Verifica tecnica (MOD10,12_RTS140ING-CTR140ING)',NULL,N'Impatto positivo, apportando nuovi criteri di CQ più stringenti si eviterà il rilascio come conformi lotti di prodotto al limite delle prestazioni.
Impatto positivo, apportando nuovi criteri di CQ più stringenti si eviterà il rilascio come conformi lotti di prodotto al limite delle prestazioni.
Impatto positivo, apportando nuovi criteri di CQ più stringenti si eviterà il rilascio come conformi lotti di prodotto al limite delle prestazioni.',N'26/03/2020
04/03/2020
04/03/2020',N'aggiornamento MOD10,12_RTS140ING-CTR140ING
messa in uso moduli
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'mod1012-rts140ing-ctr140ing rev01 - rap20-7_messa_in_uso_mod1012-rts140ing-ctr140ing_rap20-7.msg
la modifica è significativa, la notifica, però, non è da effettuare perchè il prodotto è ancora in fase di progettazione, quindi non registrato presso le autorità comptenti. - rac20-7_mod05,36__significativa.pdf',NULL,N'26/03/2020
26/03/2020
26/03/2020',N'27/03/2020
03/04/2020',N'27/03/2020
27/03/2020
26/03/2020',N'No',N'nessun impatto',N'No',N'non necessaria, la modifica è interna',N'No',N'nessun impatto, i lotti rilasciati sono conformi anche con criteri più stringenti',N'Criteri di CQ più stringenti prmettono di individuare anche lotti al limite delle prestazioni',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.8 - Fase di Sviluppo: Reportistica',N'nessun impatto su RA R&D1.8',N'No',N'vedere MOD05,36. Il prodotto non è ancora stato commercializzato',N'No',N'nessun virtual manufacturer',NULL,N'Si',N'valutare','2020-07-30 00:00:00',N'chiusura progetto.
Market Release del 31/007/2020, data di immissione in commercio del 29/07/2020',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-04-03 00:00:00','2020-03-27 00:00:00','2020-08-27 00:00:00'),
    (N'20-9',N'Correttiva','2020-03-16 00:00:00',N'Anna Capizzi',N'Ricerca e sviluppo',N'Non conformità',N'20-20',N' plasmide pHCV GenScript',N'Materia prima',N'L''ordine è stato effettuato senza seguire la procedura perchè la sequenza non è stata copiata dalla specifica di riferimento. ',N'eseguite le seguenti azioni:
- effettuare nuova formazione ai tecnici R&D sottolineando l''importanza di creare la specifica prototipo prima di effettuare un''ordine cGMP
- la sequenza plasmidica da ordinare andrà copiata dalla specifica prototipo',NULL,N'Positivo in quanto si riduce il rischio di ordinare, in fase di sviluppo, plasmidi contenenti sequenze errate.
Il cambiamento permette di ridurre gli errori in fase di ordine al fornitore del plasmide.
Nessun impatto.
Impatto positivo, i controlli inseriti riducono la causa di eventuali nc relative all''ordine di plasmidi con sequenze errate e di utilizzo per lo sviluppo dei lotti prototipo.
Impatto positivo, i controlli inseriti riducono la causa di eventuali nc relative all''ordine di plasmidi con sequenze errate e di utilizzo per lo sviluppo dei lotti prototipo.
Impatto positivo, i controlli inseriti riducono la causa di eventuali nc relative all''ordine di plasmidi con sequenze errate e di utilizzo per lo sviluppo dei lotti prototipo.
Impatto positivo, i controlli inseriti riducono la causa di eventuali nc relative all''ordine di plasmidi con sequenze errate e di utilizzo per lo sviluppo dei lotti prototipo.',N'27/03/2020
24/03/2020
26/03/2020
17/03/2020
17/03/2020
17/03/2020
17/03/2020',N'Formazione ai tecnici R&D convolti nell''ordine dei plasmidi in fase di sviluppo per la stesusura della specfica prima dell''effettuazione dell''ordine.
Formazione al personale acquisti per l''effettuazione dell''ordine solo in presenza della specifica prototipo da cui copiare la sequenza plasmidica per effettuare l''ordine al fornitore. 
Verifica ed approvazione dei risk assessment A2 e del VMP
Verifica ed approvazione dei risk assessment R5 e del VMP
Messa in uso specifica
Aggiornamento, in collaborazione con il responsabile diprocesso, dei risk assessment A2 e VMP
La modifica non è significativia in quanto introduce controlli documentali di processo, non modifica il processo in se di controllo al ricevmmento ed in accettazione.',N'Risk assessment A2 aggiornato il 26/05/2020 - rac20-9_mod09,27_01_a2_identificazionematerialistrumentiserviziinfluentiqualitaprodotto_rac20-9.pdf
VMP acquisti aggiornato il 26/05/2020 - rac20-9_mod09,30_01_a_gestione_acquisti_rac20-9.pdf
951-RTS601ING REV00 - rac20-9_messa_in_uso_specificihe_hcv_e_specifiche.msg
RA A2 aggiornato il 26/05/2020 - rac20-9_mod09,27_01_a2_identificazionematerialistrumentiserviziinfluentiqualitaprodotto_rac20-9_1.pdf
VMP acquisti aggiornato il 26/05/2020 - rac20-9_mod09,30_01_a_gestione_acquisti_rac20-9_1.pdf
modifica non significativa - rac20-9_mod05,36_non_significativa.pdf',NULL,N'27/03/2020
30/03/2020
24/03/2020
17/03/2020
17/03/2020
17/03/2020',N'15/04/2020
03/06/2020',N'24/02/2021
26/05/2020
01/02/2021
26/05/2020
17/03/2020',N'No',N'Nessun impatto sul DMRI.',N'No',N' Il prodotto è in fase di sviluppo, non è commercializzato.',N'No',N' Il prodotto è in fase di sviluppo, non è commercializzato.',N' Nessun impatto regolatorio, il prodotto è in fase di sviluppo, non è commercializzato.',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nel RA A2, introdurre la misura di controllo sull''effettuazione dell''ordine solo a seguito di consegna della specifica prototipo.',N'No',N'Veder MOD05,36. ',N'No',N'Nessun virtual manufacturer',N'Anna Capizzi',N'No',NULL,'2021-07-30 00:00:00',N'VALUTARE ASSENZA NC CON UGUALE CAUSA PRIMARIA',NULL,NULL,NULL,NULL,NULL,'2020-06-03 00:00:00','2021-02-24 00:00:00',NULL),
    (N'18-17',N'Correttiva','2018-06-04 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'La verifica dei documenti non risulta essere efficace.',N'In uscita dal risk assessment si richiedeva, almeno per i moduli contenenti criteri di accettazione (moduli di produzione e cq), la dimostrazione dell''avvenuta verifica, per esempio attraverso una prova (validazione) scritta prima dell''utilizzo. 
La verifica dei documenti verrà eseguita attraverso simulazioni di utilizzo le cui registrazioni verranno allegate al layout firmato, o verifiche puntali nei casi di modifiche puntuali (valutate con gli impatti della modifica). Gli errori che potrebbero non essere individuati durante la simulazione verranno corretti con data e firma (se non influenti sul prodotto) oppure come di consueto con l''apertura della NC. Le modalità di verifica e correzione verranno riportate nella PR05,01 "Gestione dei documenti".',NULL,N'Una più accurata verifica dei documenti, attraverso delle simulazioni di compilazione, dovrebbero mettere in luce gli eventuali errori prima dell''utilizzo del documento, riducendo la successiva gestione degli stessi attraverso la NC. La correzione degli eventuali errori, non influenti sulla qualità del prodotto, individuati durante l''utilizzo riduce i tempi di gestione di NC la cui causa primaria potrebbe essere errore di digitazione/distrazione. La cronologia delle revisioni nei moduli di produzione ottimizza l''individuazione dell''ulitima modifica apportata.
Una più accurata verifica dei documenti, attraverso delle simulazioni di compilazione, dovrebbero mettere in luce gli eventuali errori prima dell''utilizzo del documento, riducendo la successiva gestione degli stessi attraverso la NC. La correzione degli eventuali errori, non influenti sulla qualità del prodotto, individuati durante l''utilizzo riduce i tempi di gestione di NC la cui causa primaria potrebbe essere errore di digitazione/distrazione. L',N'17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018',N'Gestione delle registrazioni delle simulazioni da ri-verificare ed applicare le eventuali correzioni al layout del documento.
inserimento nei moduli di produzione della tabella con la cronologia delle revisioni.
Formazione del personale sulla nuova metodica di verifica dei documenti attraverso una simulazione della produzione di un lotto. 
Formazione sulla metodica di correzione che, nell''ottica di un controllo più più efficace attraverso le simulazioni, dovrebbe ridursi nell''arco del tempo (dall''inizio delle simulazioni).
Valutazione sulla scelta di un referente in produzione per la gestione dei documenti draft da verificare (esempio: assegnazione della simulazione al personale PT/PCT, verifica degli errori individuati da PT/PCT ed interfaccia con il DS ecc).
Aggiornamento della PR05,01 specificando:
- le modalità di verifica attraverso la simulazioni di utilizzo , o verifiche puntali nei casi di modifiche puntuali da valutare con gli impatti della modifica
- definizione dei type error
- la p',N'flusso - pr0501_03_draft_diagarammiflusso_ff_06-2018_post_riunione.pptx
formazione PR05,01, simulazioni - formazionepr05,01_03_25-09-18.pptx
formazione verifica documenti tramite simulazione - formazionepr0501_03_25-09-18.pdf
formazione sulla metodica di correzione - formazionepr0501_03_25-09-18_1.pdf
bozza diagramma flusso - rac18-17_bozza_diagramma_flusso.pdf
PR05,01 rev03 - rac18-17_messa_in_uso_pr0501_rev03_rac18-17.msg
 PR05,01 rev03  - rac18-17_messa_in_uso_pr0501_rev03_rac18-17.msg
 PR05,01 rev03  - rac18-17_messa_in_uso_pr0501_rev03_rac18-17_1.msg
Gestione correzione typing error di documenti in uso (SPECxxx) - rac18-17_sgq_formazione_pr0501_04_gestione_documenti.msg
Gestione correzione typing error di documenti in uso (SPECxxx) - rac18-17_sgq_formazione_pr0501_04_gestione_documenti_1.msg
Formazione, MOD18,02 del 19/06/2020 - rac18-17_formazione_pr05,01_04_typig_error_1.pdf
Formazione, MOD18,02 del 19/06/2020 - rac18-17_formazione_pr05,01_04_typig_error_2.pdf
esempio modulo produzione con tabella cornologia revisioni e data messa in uso in ultima pagina - mod962-ctr02_01.pdf
inserita la cronologia delle revisioni - formazionepr0501_03_25-09-18_2.pdf',NULL,N'17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018',N'03/09/2018',N'28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
19/06/2020
19/06/2020
19/06/2020
19/06/2020',N'No',N'non necessario',N'No',N'non necessaria',N'No',N'nessuno',N'nessun impatto regolatorio',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC3 - Verifica documenti',N'DC3. Il risk assessment è da aggiornare  a seguito di valutazione dell''efficacia di questa azione corretiva.',N'No',N'non necessaria',N'No',N'non necessaria',N'Roberta  Paviolo',N'No',NULL,'2019-12-30 00:00:00',N'03/09/2019: vedere REPORT "RAC18-17_TIPOLOGIA CORREZIONI_RaccoltaDati_Report_4-9-19".
Il numero di NC è  stato costante nel 2017/2018 anche dopo l''introduzione delle simulazioni. Il numero di NC è bruscamente sceso nel 2019 sicuramente grazie ad un''ampio numero di documenti corretti negli anni precedenti, quindi il processo introdotto di correzione dei typing error è sicuramente funzionale. La RAC18-17 sta avendo efficacia positiva. Dai dati raccolti emerge che le NC si sono dimezzate
Il numero di correzioni effettuate è invece sempre allineato, ossia circa ¼ rispetto ai moduli messi in uso (stessa situazione riscontrata al momento dell''apertura della RAC). La tipologia di correzioni non è significativa. L''arco di tempo preso in analisi (circa un anno) per valutare se le simulazioni d''uso sono una metodica efficace è troppo breve. I dati verranno ancora raccolti, almeno per un altro anno.

29/03/2020: vedere REPORT "RAC18-17_TIPOLOGIA CORREZIONI_RaccoltaDati_Report_29-09-20".
Nel corso di 2 anni le correzioni dei documenti hanno subito un decremento (dal 25% al 14%), dimostrando l''efficacia dell''introduzione delle simulazioni sui moduli prima della loro messa in uso. Questo decremento, come era stato previsto dal precedente report, è stato messo in evidenza ad una distanza di tempo maggiore, rispetto alla prima analisi che comprendeva solo un anno, grazie all''aumentare dei moduli messi in uso previa simulazione di utilizzo. Le correzioni con impatto sul prodotto che necessitano l''apertura di NC si sono dimezzate dopo la gestione dei typing error e poi mantenute più o meno costanti nel corso di 2 anni (28/09/2018-03/09/2019 4 NC, 04/09/2019-29/09/2020 3 NC.

Il risk assessment DC3 è aggiornato al 29/09/2020 con un profilo di rischio di nessun intervento.

17/11/2020: dall''applicazione del nuovo flusso di correzione delle specifiche (06/2020) ad oggi è emerso che le specifiche subiscono un basso numero di correzioni e che il flusso, nei 5 mesi, in cui è stato testato è risultato efficace.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-09-03 00:00:00','2020-06-19 00:00:00','2020-09-29 00:00:00'),
    (N'19-18',N'Correttiva','2019-06-25 00:00:00',N'Federica Farinazzo',N'Sistema qualità',N'Reclamo',N'18-28',N'Prodotti  OEM Fast Track: RTS548ING',N'Manuale di istruzioni per l''uso',N'Il falso positivo RSV è causato da un CrossTalk tra i canali RSV, FluA e FluB. Dai test eseguiti in sede, il cross-talk ha una componente strumento-dipendente.Lo strumento del cliente è utilizzato solo per Demo e potrebbe necessitare una nuova taratura.',N'Messa a punto di una procedura Service" per la riduzione del rischio CrossTalk  tra i diversi canali ottici',NULL,N'Nessun impatto se non in termini di attività
Analisi dei dati raccolti dal Service InGenius al fine di valutare con loro l''opportunità di impostare una manutenzione delle ottiche che eviterebbe il cross-talk.
Nessun impatto se non in termini di attivtà.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto
Pianificazione di uno studio sulla misurazione dell''intensità della fluorescenza delle ottiche sullo strumento InGenius ',N'26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019',N'Revisione IFU di RTS548ING.
Benché non sono presenti evidenze sugli altri prodotti OEM Fast Track, valutare se necessario l''inserimento di una frase generica anche per gli altri prodotti. In tal caso allegare l''elenco delle IFU da modificare.
In base ai dati raccolti ed alle decisioni sull''integrazione delle attività di manutenzione eventuale aggiornamento delle IFU dei prodotti multiplex.
nessuna attività
Messa in uso IFU di RTS548ING ed eventuali altri prodotti, come da elenco fornito.
In base ai dati raccolti ed alle decisioni sull''integrazione delle attività di manutenzione eventuale aggiornamento della documentazione degli FTP dei prodottimultiplex ed eventuale aggiornamento dell''analisi dei rsichi del prodotto InGenius.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
nessauna attività',NULL,NULL,N'26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019',N'05/09/2019
05/09/2019
26/07/2019
05/09/2019
05/09/2019
05/09/2019
26/07/2019',N'26/07/2019
26/07/2019',N'No',N'Nessun impatto sui DMRI di prodotto',N'No',N'nono necessaria',N'No',N'Nessun impatto sul prodotto immesso in commercio',N'-',N'ST - Elenco famiglie Strumenti',N'ST37 - Elite InGenius (STRxx)',N'si valuterà in base all''implementazione della manutenzione delle ottiche',N'No',N'vedere MOD05,36',N'No',N'nessun Virtual manufacturer',NULL,N'Si',N'valutare la''ggiornamento dell''analisi del rischio',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-09-05 00:00:00',NULL,NULL),
    (N'18-1',N'Correttiva','2018-01-10 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-132',N'955-RNAEV-PLD',N'Documentazione SGQ',N'I risultati ottenuti nel test CQ e, contestualmente, nella ripetizione hanno messo in luce alcune criticità relative ai criteri di accettazione e di selezione del campione stabiliti nel precedente modulo di verifica tecnica (MOD10,12-RNAEV-10DiluizioneRNA_00del 23/07/2015). Le criticità sono collegate alla variabilità di quantificazione dell'' RNA in base al lotto di mix utilizzato, inoltre i criteri di CQ sono stati validati con un unico lotto di RNA EV il quale corrisponde allo stesso lotto fornitore in test.',N'Modifica dei criteri di controllo qualità
',NULL,N'1)	Modifica del criterio di accettazione del campione RNA-EV di riferimento: da 3/3 replicati 300?Qty?3000  a 3/3 replicati Qty?100;
2)	Calcolo del coefficiente di variazione percentuale (CV%) per determinare quale campione di RNA-EV in analisi (PLD6-PLD1) risulta più simile in termini di quantificazione al campione di RNA-EV di riferimento;
3)	Calcolo del volume in µL (V) del campione da utilizzare secondo la legge di diluizione: 
Qty media riferimento x V riferimento =  Qty media analisi x V analisi quindi V analisi = (Qty media riferimento x V riferimento) / Qty media analisi

Verifica dei criteri studiati utilizzando i dati di CQ eseguiti.
1)	Modifica del criterio di accettazione del campione RNA-EV di riferimento: da 3/3 replicati 300?Qty?3000  a 3/3 replicati Qty?100;
2)	Calcolo del coefficiente di variazione percentuale (CV%) per determinare quale campione di RNA-EV in analisi (PLD6-PLD1) risulta più simile in termini di quantificazione al campione di RNA-EV di riferimento;
3)	Calcolo del v',N'10/01/2018
10/01/2018',N'vedi RAC18-01_RNA-EV_Action_Plan_Modello.xlsx in G:OPERATIONSCAPA
vedi RAC18-01_RNA-EV_Action_Plan_Modello.xlsx in G:OPERATIONSCAPA',N'MOD10,12-RNAEV-diluizione RNA_01_prova_dati - mod10,12-rnaev-diluizionerna_01_prova_dati.xlsx
Test confronto vecchia e nuova modalità calcolo volumi - mod10,12-rts076pld_02.xlsx
Action plan completato - rac18-01_rna-ev_action_plan.xlsx
MOD10,12-RNEEV-10-DILUIZIONERNA_REV01 - rac18-1_messa_in_uso_mod1012-rnaev-10-diluizionerna_rac18-1.msg
action plan completato - rac18-01_rna-ev_action_plan_1.xlsx
Test confronto vecchia e nuova procedura calcolo volumi - mod10,12-rts076pld_02_1.xlsx',NULL,N'10/01/2018
06/03/2018',N'31/01/2018
06/03/2018',N'06/03/2018
06/03/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'-',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',NULL,N'No',N'-',N'No',N'-',NULL,N'No',NULL,'2018-10-30 00:00:00',N'Nessuna NC con uguale causa primaria.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-03-06 00:00:00','2018-03-06 00:00:00','2018-10-30 00:00:00'),
    (N'19-1',N'Correttiva','2019-01-10 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',NULL,N'RTS120ING',N'Assay Protocol',N'Problematica Ct early: PSS ha implementato la possibilità di accettare campioni con bassi Ct solo per i modelli 1 e 2 , non è stato ancora pianificata l''implementazione per il modello 12. 
Problematica TM: I dati di Tm in uscita dal progetto sono statisticametne solidi, ma probabilmetne non è stato tenuta in debita considerazione  la variabilità strumentale a livello degli utilizzari. 
Problematica relativa alla Discrepanza tra AP e IFU: l''errore di scrittura dell''AP non è stato intercettato nel controllo in quanto non eseguito sulle lingue diverse dall''inglese.',N'Problematica Ct early: Si chiede la modifica dell''Assay Protocol al fine di ridurre la frequenza di campioni con errore legato al Ct early abbassando il limite di calcolo dei Ct. 
Problematica TM: Si chiede di ricalcolare il range per MTB tenendo conto dei dati segnalati dai clienti.
Problematica relativa alla Discrepanza tra AP e IFU: si chiede di correggere i nomi dell''AP come da IFU e di implementare la verifica degli output in tutte le lingue previste dalla gui nel nel MOD04,30.',NULL,N'L''implemtentazione della verifica e della registrazione nelle lingue della GUI rispetto all''IFU può avere impatti in termini di tempo delle attività, ma garantisce il rilascio di un AP disponibile per tutte le lingue della GUI.
La modifica andrà a mitigare la problematica del Ct early pertanto saranno ridotte le possibilità di segnalazioni relative alla problematica.
Sarà necessario iil coordinamento per la messa in uso del nuovo AP
La modifica del solo AP per i clienti porterebbe ad avere diomogeneità rispetto all''utilizzo del prodotto a livello di CQ, benchè la problematica possa non presentarsi andrebbe valutata la stessa modifica per i criteri di CQ. In particolare è necessario modificare il criterio per MTB inserendo in Verifica tecnica il limite superiore Tm a 69,5°C
La modifica andrà a mitigare la problematica del Ct early pertanto saranno ridotte le possibilità di segnalazioni relative alla problematica, sarà necessario valutare se tale modifica è da notificare.
La modifica andr',N'08/02/2019
08/02/2019
10/01/2019
08/02/2019
08/02/2019
08/02/2019
08/02/2019
08/02/2019',N'Rianalizzare i dati di Tm.
Modifica dell''AP per il prodotto RTS120ING per Tm /nomi e Ct early.
Inserimento nel MOD04,30 di una verifica degli output nelle lingue.
Analizzare i dati di validazione e i dati forniti dai clienti con il nuovo range di Tm.
messa in uso nuovo AP
Aggiornamento MOD10,12-RTS120ING-CTR120ING_00
Aggiornamento assay presso i clienti, come da pianificazione
Interfacciamento con PSS per l''indagine.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Controllo del nuovo AP e stesura delle TAB
Inserimento nellla IO04,08 della verifica degli output nelle lingue sul  MOD04,30
Informazione degli specialist per aggiornamento in seguito a segnalazione da parte del cliente di dati inconclusivi.
Individuazione dei clienti che acquistano il prodotto, pianificazione ed aggiornamento AP presso gli stessi',N'ASSAY PROTOCOL IN USO AL  8/2/2019 - rac19-1_messa_in_uso_ifu_rts120ing_-_rac19-01;_r19-07.msg
IFU rev01 - rac19-1_messa_in_uso_ifu_rts120ing_-_rac19-01;_r19-07_1.msg
mod10,12-rts120ing-ctr120ing REV01 - rac19-1_meesa_in_uso_mod1012-rts120ing-ctr120ing_rac19-1_rac19-6_1.msg
La modifica NON è significativa, pertanto non è necessario effettuare notifiche presso le autorità competenti. - rac19-1_mod05,36_non_significativa.pdf
FTP RTS120ING aggiornato - rac19-1_section_1_product_technical_file_index_01.docx
TAB P24 revAB IN USO 11/02/19 - rac19-1__messa_in_uso_tab.msg
Invio TAB P24  - rac19-1_elite_ingenius_tab_p24_-_rev_ab_-_product_mdr_mtb_elite_mgb_notice.msg
Analisi dati CQ specifiche AP vs CQ - rts120ing_monitoraggio_cqvsap.pdf
MOD10,12-RTS120ING-CTR120ING_rev01 - rac19-1_meesa_in_uso_mod1012-rts120ing-ctr120ing_rac19-1_rac19-6.msg
IO04,08_01 - rapc19-1_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919.msg',NULL,N'08/02/2019
08/02/2019
08/02/2019
08/02/2019
08/02/2019
08/02/2019',N'11/03/2019',N'10/09/2019
11/03/2019
11/03/2019
05/08/2019
11/02/2019
13/09/2019
11/02/2019',N'No',N'Non necessario alcun aggiornamento',N'No',N'TAB P24 ',N'No',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.2 - Fase di Pianificazione: Definizione requisiti',N'RA R&D1.2 non impattato',N'No',N'non significativa vedere MOD05,36',N'No',N'non necessaria',NULL,N'No',NULL,'2021-01-31 00:00:00',N'Assenza reclami con uguale causa primaria.
A 01/2020, sul lotto U1019AN non ci sono stati reclami.
A 05/2020 è stato prodotto un altro lotto che non presenta reclami: U0420-014; il lotto U1019AN ha un reclamo R20-15 (gestito con la RAC20-8), non è relativo a questa azione correttiva. Si attende un terzo lotto.
A 11/2020 il lotto U1120-012 è conforme. Il reclamo 20-72 e 20-17 riguardano l''interfacciamento con il LIS e il R20-65 pè gestito attraverso la RAC20-35 (problematica di lettura del datamatrix presente sui tubi interni).
A 01/2021 non ci sono stati reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-03-11 00:00:00','2019-09-13 00:00:00','2021-01-13 00:00:00'),
    (N'19-22',N'Preventiva','2019-07-24 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',N'19-60',N'RTS507ING',N'Progetto',N'Allo scopo di prevenire NON conformità sia nell''uso corrente del prodotto da parte dei clienti (reclami) che nella verifica al CQ dei lotti di produzione, si ritiene opportuno introdurre delle modifiche al prodotto per migliorarne e/o irrobustirne le prestazioni adottando un approccio simile a quanto già eseguito nella RAC 19-2 per il prodotto MV2 ELITe MGB  ref RTS532ING',N'Incremento della fluorescenza finale del controllo interno e di un target d''interesse (HSV1) e migliorare l''efficienza di reazione in termini di Ct attraverso la modifica del ciclo termico, degli Assay parameter per il calcolo del Ct. 
Tra i possibili impatti/rischi da considerare è l''insorgenza di falsi positivi o negativi  ins eguito alla modifica del cross talk per il controllo interno e HSV1. 
',NULL,N'Una modifica del ciclo termico a livello di temperatura di annealing/extensionavrebbe un forte impatto sulle prestazioni dei prodotti e potrebbe richiedere una rivalidazione. Modifiche della fase di trascrizione inversa o denaturazione (già effettuate da FTD per i suoi kit corrispondenti agli OEM EG SpA) possono essere ritenute accettabili senza necessità di una rivalidazione.
Una modifica della cross talk compensation può portare a risultati "falsi positivi" e deve essere valutata sui dati pregressi per evitare una rivalidazione.
In base alle modifiche apportate al ciclo termico, il cambiamento può richiedere una rivalidazione. Modifiche minime (vedi R&D) possono essere ritenute accettabili.
Il miglioramento delle prestazioni del prodotto ha impatto positivo sulle prestazioni dei lotti, riducendo il rischio di Non conformità.
Impatto in termini di attività
La modifica può impattare i prodotti e richiedere una rivalidazione, in base ai risultati dei test con i vari cicli termici, in questo cas',N'10/09/2019
10/09/2019
10/09/2019
10/09/2019
10/09/2019
10/09/2019',N'Valutazione degli effetti di una modifica del ciclo termico nella fase di trascrizione inversa o denaturazione.
Valutazione degli effetti di una modifica della crosstalk compensation per migliorare le fluorescenze dell'' IC500 e HSV1.
Modifica / verifica AP cliente e AP CQ.
Valutazione della modifica dell''AP per i parametri della dell''Assay Cross-talk Matrix per ottimizzare la fluorescenza del canale IC.
Verifica AP.
Stesura TAB.
Verifica sperimentale delle prestazioni dei nuovi AP, per sensibilità e specifità. Report sulle attività svolte.
Individuazione dei clienti che utilizzano il prodotto, pianficazione ed aggiornamento AP presso gli stessi.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Aggiornamento FTP e valutazione aggiornamento analisi dei rischi
messa in uso AP ',N'AP in uso il 24/10/2019 - rac19-22_ap_rts507ing_rap19-22_e_rts523ing_rac19-2.msg
AP in uso il 24/10/2019 - rac19-22_ap_rts507ing_rap19-22_e_rts523ing_rac19-2_1.msg
TAB P04 - Rev AC -  Product MV ELITe MGB Panel notice, INVIATA IL 18/10/2019 - rac19-22_invio_elite_ingenius_-tab_p04_-_rev_ac_-_meningitis_viral_elite_mgb_panel_notice_of_change.msg
La modifica non è significativa, pertanto non è necessarrio effettuare alcuna notifica. - rap19-22_mod05,36_non_significativa.pdf
 AP in uso il 24/10/2019  - rac19-22_ap_rts507ing_rap19-22_e_rts523ing_rac19-2_2.msg',NULL,N'10/09/2019
10/09/2019
24/07/2019
10/09/2019
10/09/2019
10/09/2019
10/09/2019',N'11/10/2019',N'24/10/2019
24/10/2019
08/10/2019
13/01/2021
10/09/2019
13/01/2021
24/10/2019',N'No',N'nessun impatto',N'Si',N'da esuguire',N'No',N'.',N'La sicurezza del prodotto deve rimanere invariata',N'ALTRO - Altro',N'ALTRO - ALTRO',N'da valutare',N'No',N'vedere MOD05,36',N'No',N'non necessario',NULL,N'Si',N'da valutare','2021-01-13 00:00:00',N'Gennaio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-10-11 00:00:00','2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'17-14',N'Preventiva','2017-05-29 00:00:00',N'Roberta  Paviolo',N'Ricerca e sviluppo',NULL,NULL,NULL,NULL,N'Probabile dimerizzazione causata da una parziale attivazione dell''enzima dovuta ai cicli di scongelamento.',N'Diminuizione dei cicli di scongelamento ed aggiornamento del IFU.',NULL,N'verificare il comportamento nei prossimi lotti e nelle prossime stabilità. Aggiornare l''IFU.
verificare il comportamento nei prossimi lotti e nelle prossime stabilità. Aggiornare l''IFU.
messa in uso IFU',N'30/05/2017
30/05/2017
30/05/2017',N'verificare il comportamento nei prossimi lotti e nelle prossime stabilità.
aggiornare IFU
messa in uso IFU',N'elenco 3 stabilità a 12 mesi + 1 a 18 mesi - rap17-14_allegato_1_elencostabilita.docx
messa in uso IFU RTS200ING rev01 - rap17-14_messa_in_uso_ifu_rts200ing.pdf',NULL,N'30/05/2017
30/05/2017
30/05/2017',N'12/11/2017
30/05/2017
27/06/2017',N'12/01/2018
19/06/2017
26/06/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'ALTRO - Altro',NULL,N'no',N'No',NULL,N'No',NULL,N'Salvatore Patanè',N'Si',N'verificare nel documento DDC.1432 r0 CRE ELITe MGB Kit code RTS200ING CRE-ELITe Positive Control se è specificato il numero di cicli di conge-sconge','2018-05-30 00:00:00',N'Conformità delle stabilità dei lotto a -20°C per 18 mesi:
STB2017-091, U0316AO, CONFORME
STB2018-007, U0416AU, CONFORME
STB2018-029, U0616BC, CONFORME',N'Stefania Brun',N'Positivo',NULL,NULL,NULL,'2017-11-12 00:00:00','2018-01-12 00:00:00','2018-05-09 00:00:00'),
    (N'18-24',N'Preventiva','2018-07-05 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'L''uso nelle carte di controllo Ingenius di Controlli Positivi a concentrazione significativamente diversa da quella prevista potrebbe generare sia carte di controllo inaccurate sia sedute erroneamente invalide',N'Adeguamento delle modalità di controllo qualità utilizzando la curva standard di riferimento per la quantificazione del CTR.',NULL,N'Modifica delle modalità di esecuzione dei CQ dei Controlli positivi. Miglioramento della valutazione del lotto e unformità indipendentemente dalla pianificazione di produzione.
Non vi sono impatti sul processo. 
Non vi sono impatti sul processo. La modifica è migliorativa per la valutazione del prodotto.',N'05/07/2018
05/07/2018
05/07/2018',N'Modifica del MOD10,18 indicando i moduli per i CQ quantitativi anche per i Controlli Positivi. Formazione ai QCT.
Valutazione degli impatti sul prodotto
Messa in uso dei documenti',NULL,NULL,N'05/07/2018
05/07/2018
05/07/2018',N'13/07/2018',N'05/07/2018
05/07/2018
05/07/2018',N'No',N'na',N'No',N'na',N'No',N'na',N'na',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'na',N'No',N'na',NULL,N'No',NULL,'2020-07-05 00:00:00',N'Assenza di reclaimi causati da deriva del lotto di CTR a distanza di 24 mesi dalla chiusura delle azioni.
14/07/2020: l''adeguamento delle modalità di controllo qualità utilizzando la curva standard di riferimento per la quantificazione del CTR NON ha mostrato dei lotti con delle derive della curva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-07-13 00:00:00','2018-07-05 00:00:00','2020-07-14 00:00:00'),
    (N'20-39',N'Preventiva','2020-10-23 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'L''attività di monitoraggio dei lotti è una fase utile al fine di prevenire non conformità e derive di processo. E'' pertanto utile inserire dei criteri per il monitoraggio quando possibile.',N'Ampliamento dei criteri di CQ al fine di adeguare entrambe le mix ai criteri degli AP ed inserire dei criteri di monitoraggio più stringenti e in grado di valutare eventuali derive.',NULL,N'La modifica permette di avere dei criteri di CQ omogenei rispetto ai criteri di AP, e alle due mix in test, e di avere criteri più stringenti di monitoraggio per valutare la produzione di ogni lotto di prodotto ed eventuali derive.
I criteri di accettazione del controllo qualità vengono allineati all''AP in uso presso il cliente, ossia ampliati rispetto ai criteri di CQ attuali. Ai criteri di CQ sono stati aggiunti dei criteri di monitoraggio più stringenti, rispetto ai nuovi criteri di CQ, al fine di monitorare eventuali derive (valori di ct differenti rispetto a valori ottenuti con i lotti precedenti).  
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'05/11/2020
05/11/2020',N'Analisi dei dati dei lotti
Studio di criteri di monitoraggio (vedi allegato Statistica e nuovi criteri)
modifica del MOD10-12-RTS120ING
formazione
valutazione degli impatti della modifica
Valutazione dell''idoneità dei nuovi criteri
messa in uso dei documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'statistica e nuovi criteri - criteri_new_23-10-2020.xlsx
Nuovi Criteri RAP20-39 per MOD10,12 rev02 - rap20-39_nuovi-criteri-monitoraggio.pdf
messa_in_uso_mod1012-rts120ing-ctr120ing_02 - rap20-39_messa_in_uso_mod1012-rts120ing-ctr120ing_02_mod1802_25112020.msg
messa_in_uso_mod1012-rts120ing-ctr120ing_02 - rap20-39_messa_in_uso_mod1012-rts120ing-ctr120ing_02_mod1802_25112020_1.msg
messa_in_uso_mod1012-rts120ing-ctr120ing_02 - rap20-39_messa_in_uso_mod1012-rts120ing-ctr120ing_02_mod1802_25112020_2.msg
Essendo i documenti di CQ strettamente confidenziali, la notifica del cambiamento deve essere effettuata solo alle autorità competenti che hanno richiesto in passato documenti di CQ. RTS120ING è registrato in diversi paesi (come da MOD05,29_Stato_Registrazioni_Prodotti_IVD), ma in nessuno aono stati consegnati i documenti di CQ, pertanto non si ritiene necessario effettuare la notifica. - rap20-39_mod05,36_01_significativa.xlsx',NULL,N'23/10/2020
05/11/2020
05/11/2020',N'13/11/2020',N'26/11/2020
02/12/2020
02/12/2020
12/03/2021',N'No',N'nessun impatto sul DMRI, non cambiano i titoli dei documenti, nè ne vengono aggiunti',N'No',N'non necessaria, modifica interna',N'No',N'nessun impatto sul prodotto immesso in commercio',N'La modifica dei requisiti di CQ mira a migliorare il monitoraggio dei lotti ed individuare tempestivamente le eventuali derive. inoltre allinea i requisiti di CQ, con quelli indicati negli Assay Protocol. il prodotto non subisce modifiche alle performance dichiarate.',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'da verificare su quale ra ha impatto la modifica',N'Si',N'vedere MOD05,36. Essendo i documenti di CQ strettamente confidenziali, effettuare la notifica del cambiamento solo alle autorità competenti che hanno richiesto in passato tali documenti, altrimenti si ritiene non necessario effettuare la notifica.',N'No',N'nessun virtual manufacturer coinvolto',NULL,N'No',NULL,'2021-06-30 00:00:00',N'Verificare, dai dati di monitoraggio, la presenza di eventuali derive (attendere almeno 3 lotti dalla messa in uso dei moduli di CQ 2/12/2020)',NULL,NULL,NULL,NULL,NULL,'2020-11-13 00:00:00','2021-03-12 00:00:00',NULL),
    (N'20-25',N'Correttiva','2020-08-21 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-113',N'Prodotti ad RNA',N'Documentazione SGQ',N'Durante l''indagine per la NC19-113 relativa al prodotto RTSG07PLD190-TAQ è emerso che i tubi utilizzati per effettuare le diluizione dell''RNA  erano inadeguati; gli stessi tubi sono stati utilizzati per l''esecuzione del CQ del lotto in oggetto. I tubi utilizzati presentano un grado di purezza "Sterile" a differenza dei tubi eppendorff che hanno caratteristiche DNAsi ed RNasi free. Le minori prestazioni hanno diminuito l''efficienza di retrotrascrizione e amplificazione dell''RNA, in particolare evidenziabile nel campione di RNA a più basso titolo (0,4 ng/reazione).
Le istruzioni di Controllo qualità non danno rilievo al tipo di plastica da utilizzare per la preparazione dei campioni ad RNA, richiedono un aggiornamento.',N'Prevedere sui moduli di CQ dei prodotti one-step, l''indicazione dei tubi per RNA da utilizzare per le diluizioni dei campioni ad RNA',NULL,N'Eventuali errori di selezione dei tubi possono essere intercettati in fase preanalitica e di registrazione dei materiali 
Eventuali errori di selezione dei tubi possono essere intercettati in fase preanalitica e di registrazione dei materiali 
solo in termini di attività
solo in termini di attività',N'21/08/2020
21/08/2020
26/08/2020
26/08/2020',N'Modifica documenti e formazione (vedi allegato). Verificare la necessità di tale implementazione anche sui moduli dei nuovi progetti in corso.
verificare se necessario integrare il RA CQ5 con l''identificazione dei materiali di consumo da utilizzare
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'elenco_doc - elenco_doc.xlsx
IO10,06rev06 e allegato, MOD10,13-xx e MOD957-xx; MOD18,02 del 10/11/2020 - rac20-25_messa_in_uso_doc_cq_mod1802_10112020.msg
IO10,06rev06 e allegato, MOD10,13-xx e MOD957-xx; MOD18,02 del 10/11/2020 - rac20-25_messa_in_uso_doc_cq_mod1802_10112020_1.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-25_mod05,36__non_significativa.pdf
Tutti i risk assessment di CQ verrano presi in analisi come da RAP21-3 - rac20-25_raaggiornatirap21-3.txt',NULL,N'21/08/2020
26/08/2020
26/08/2020',N'31/10/2020
13/11/2020',N'25/11/2020
28/04/2021
25/11/2020
26/08/2020',N'Si',N'Aggiornare DMRI di Entero, WNV, P210, P190 e HEV',N'No',N'non necessaria',N'No',N'nessun impatto. ',N'La registrazione sui documenti di CQ dei prodotti one-step, dei tubi per RNA da utilizzare per le diluizioni dei campioni ad RNA, migliora la tracciabilità dei materiali utilizzati e riduce la possibilità di avere altre nc con uguale causa primaria.',N'CQ - Controllo Qualità prodotti',N'CQ5 - Identificazione campioni / reagenti / prodotti accessori / documenti',N'verificare se necessario integrare il RA CQ5 con l''identificazione dei materiali di consumo da utilizzare',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufdacturer',NULL,N'No',NULL,'2021-05-31 00:00:00',N'Verificare che l''indicazione sulla documentazione CQ dei tubi da utilizzare sia sufficiente ad eliminare la causa primaria della nc (verificare 3 lotti di prodotto)
Al 28/04/2021 sono ancora in corso di aggiornamento i DMRI. è stata aperta la NC21-48 in fase di analisi',NULL,NULL,NULL,NULL,NULL,'2020-11-13 00:00:00','2021-04-28 00:00:00',NULL),
    (N'19-26',N'Correttiva','2019-09-20 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',N'19-72',N'955-  Oligonucleotidi',N'Documentazione SGQ
Processo di produzione',N'Il personale di produzione non ha seguito le istruzioni previste dal modulo MOD09,017 Scheda preparazione sonde monoreagente, rev.03 per il calcolo del volume iniziale che richiede di inserire il "Volume dichiarato" sul certificato di analisi e non quello misurato. Nonostante non sia stata seguita la procedura correttamente, il lotto risulta conforme. 
',N'Eseguita formazione al personale di produzione in data 24/07/2019. Panificato un aggiornamento del modulo al fine di migliorare il flusso di lavoro. In particolare è necessario modificare i moduli di preparazione degli oligo nella sezione relativa alla preparazione del controcampione al fine di rendere più comprensibile la procedura. La procedura era stata impostata in modo tale da aspirare un volume esatto come volume iniziale per il calcolo e la diluizione. Attualmente si è verificato abbastanza frequentemente una differenza tra il volume dichiarato dal fornitore e quello realmente ricevuto; è pertanto necessario adattare la procedura a questa eventualità.
Si chiede inoltre di modificare la sezione relativa a lettura e calcoli in modo tale che il calcolo sia eseguito in un unico step conservando i decimali, come nel calcolo eseguito dal SW "Gestione oligo", codice SW5. Questo modifica permette di avere i risultati allineati tra software e verifica del secondo operatore, segnalazione già presente nell''analisi dei dati fatta per la NC18-52, dalla quale non si era ritenuto necessario aprire una richiesta di azione correttiva.',NULL,N'La modifica ha impatto positivo in quanto renderà più flessibili le procedure adottate per la produzione dei semilavorati "oligo", prevedendo una gestione più dettagliata e precisa dei vari casi che si possono presentare (come definito dall''analisi dello storico) delle differenze fra il volume dichiarato dal fornitore e quello realmente ricevuto. Inoltre, lo svolgimento dei calcoli in un unico step, mantenendo lo stesso numero di cifre decimali, permetterà un corretto confronto tra il risultato dei calcoli svolti dall''operatore ed il risultato rilasciato dal SW "Gestione Oligo". 
Nessun impatto sul processo ma solo in termini di attività
Nessun impatto se non in termini di attività.
Nessun impatto sul processo ma solo in termini di attività',N'23/09/2019
23/09/2019
23/09/2019',N'Simulazione dei nuovi moduli 
Modifica dei moduli
MOD09,013-Scheda preparazione sonde trireagente (TaqMan)
MOD09,017-Scheda preparazione sonde monoreagente (Pleiadi e TaqMan)
MOD09,018-SCHEDA DILUIZIONE PRIMER
MOD09,019-SCHEDA DILUIZIONE SONDE
MOD09,011-Scheda preparazione primer liofilizzati
Formazione
Messa in uso dei moduli di produzione
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'Messa in uso moduli oligo, MOD18,02 del 25/11/2019 - rac19-26_messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119.msg
Messa in uso moduli oligo, MOD18,02 del 25/11/2019 - rac19-26_messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119_1.msg
Messa in uso moduli oligo - rac19-26_messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119_2.msg
La modifica non è significativa, pertanto non è da notificare - rac19-26_mod05,36_non_significativa.pdf',NULL,N'23/09/2019
20/09/2019
23/09/2019
23/09/2019',N'29/11/2019
15/11/2019
13/12/2019
15/11/2019',N'09/12/2019
09/12/2019
09/12/2019
23/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.4 - Calcolo delle quantità
P1.9 - Misurazione e trasferimento materiali e semilavorati',N'P1.9',N'No',N'vedere MOD05,36',N'No',N'vedere MOD05,36',N'Federica Farinazzo',N'No',NULL,'2020-12-30 00:00:00',N'VALUTAZIONE EFFICACIA: verificare assenza NC con causa primaria errore nel calcolo del volume iniziale per procedura non applicata , aggiornamento del risk assemsnet P1.4 riportando la probabilità di accadimento dell''evento a livello basso (vedere anche RAC19-38). A 08-2020 è presente la NC20-36 relativa ad un non corretto utilizzo del MOD09,017, la formazione aggiuntiva svolta MOD18,02 del 8/07/2020 è sufficiente (anche alla luce del nuovo personale di produzione), a 01/2021 non sono presenti NC imputabili al calcolo del volume iniziale, è pertanto possibile chiudere l''azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-12-13 00:00:00','2019-12-09 00:00:00','2021-01-13 00:00:00'),
    (N'21-10',N'Correttiva','2021-03-18 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'21-16',N'Modifica degli AP per soglie Tm Threshold katG e rpoB e del modello interpretativo 12 del prodotto RTS120ING - MDR/MTB ELITe MGB Kit',N'Assay Protocol
Modello interpretativo ',N'L''assenza di amplificazione del gene rpoB da parte di tutte le quattro sonde specifiche, con un risultato di resistenza genotipica positiva alla Rifampicina, era già emersa con il reclamo 20-15 e, dal momento che il gene è essenziale e non può non essere rilevato almeno da una delle quattro sonde, la causa è imputabile ad un''inibizione dell''amplificazione del gene rpoB. E'' stata inserita la mitigazione nell''IFU del prodotto (RAC20-8) che sottolinea che, in caso di rpoB non riconosciuto con tutte le quattro sonde, il campione debba essere ritestato se positivo per il DNA dell''MTB complex.
Il risultato ottenuto dal centro rientra in un caso di inibizione dell''amplificazione del gene rpoB.
La causa del risultato di resistenza genotipica positiva all''Isoniazide del gene katG in presenza di amplificazione valida del target ma in assenza di un valore di Tm rilevato è imputabile al valore della Tm del picco della curva di dissociazione che è presente ma si trova sotto il valore della Threshold impostata a 50. La Tm, non venendo rilevata, ricade automaticamente al di fuori del range di sensibilità all''Isoniazide (70°C-80°C) anche se graficamente ricade nel range che identifica la presenza del gene katG wild type e quindi di sensibilità. 

Alla base dei risultati discrepanti vi è un modello interpretativo (12) che considera come criterio di positività alle resistenze agli antibiotici la completa assenza di un valore di Tm che, oltre ad essere indice di una mutazione sul gene, può però essere anche la conseguenza di un''inibizione della reazione di amplificazione o di un valore di Tm del picco della curva di dissociazione che si trova al di sotto della Threshold di Tm impostata.
L''inibizione in un campione della reazione di amplificazione del gene rpoB (che è la reazione più sensibile) è indice anche di parziali inibizioni delle reazione del gene inhA o del gene katG che generano curve di amplificazione con fluorescenze più basse e, potenzialmente, conseguenti Tm sotto soglia. 
La possibilità che un risultato non corretto possa essere rilasciato in caso di inibizione della sola reazione di amplificazione del gene katG o del gene inhA viene esclusa a priori poichè la reazione di rpoB è molto più sensibile all''inibizione di quella katG o inhA: non risultano casi di inibizione di katG o inhA senza inibizione di rpoB.
Il limite del modello può portare, con una bassa frequenza, a falsi positivi per le resistenze agli antibiotici.

La causa primaria è imputabile al limite del modello interpretativo 12 del prodotto MDR-MTB che, nel caso di inibizione della reazione di amplificazione del gene rpoB con tutte le quattro sonde, deve prevedere che il risultato sia "campione non valido" senza restituire ulteriori risultati sulle resistenze. ',N'1. Soluzione a breve termine: 
- modifica degli AP abbassando la Threshold della Tm dal valore 50 a 30 con rianalisi dei dati di validazione per diminuire le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica all''Isoniazide del gene katG
- in concomitanza, valutare un''eventuale modifica degli AP abbassando la Thereshold della Tm anche per le quattro sonde rpoB con rianalisi dei dati di validazione per diminuire le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica alla Rifampicina del gene rpoB
2. Soluzione a lungo termine:
- aggiornamento del modello interpretativo 12 che deve prevedere che, in caso di rpoB non riconosciuto per tutte le quattro sonde, il risultato sia "campione non valido" senza restituire ulteriori risultati sulle resistenze. PSS potrà rilasciarlo solo con nuova versione del Software InGenius e con tempistiche legate a rilascio avvenuto del BeGenius.',NULL,N'Impatto positivo: la modifica del modello interpretativo permetterebbe di non avere più risultati errati "RESISTANCE POSITIVE" in caso di assenza di Tm per tutti e 4 i target rpoB.  Valutare a modifica avvenuta gli eventuali impatti sugli Assay Protocol di RTS120ING
Nessun impatto, se non in termini di attività
Nessun impatto, se non in termini di attività
Nessun impatto se non in termine di attività
Impatto positivo: 
- la modifica sugli AP permetterà di ridurre le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica all''Isoniazide del gene katG e sulla resistenza genotipica alla Rifampicina del gene rpoB
- la modifica del modello interpretativo permetterebbe di non avere più risultati errati "RESISTANCE POSITIVE" in caso di assenza di Tm per tutti e 4 i target rpoB.  
Impatto positivo: 
- la modifica sugli AP permetterà di ridurre le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica all''Isoniazide del ',N'12/04/2021
25/03/2021
25/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021',N'Aggiornamento del modello interpretativo 12 che deve prevedere che, in caso di rpoB non riconosciuto per tutte e quattro le sonde, il risultato sia "campione non valido" senza restituire ulteriori risultati sulle resistenze. Esiste già un modello proposto che attualmente è allegato al Mantis 441 che deve essere revisionato e riallegato sia al Mantis che qui sulla RAC.
Valutare la diminuzione della Threshold della Tm di KatG dal valore 50 a 30 con rianalisi dei dati di validazione per diminuire le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica all''Isoniazide del gene katG. (rianalisi da allegare). In concomitanza, valutare un''eventuale diminuzione della Thereshold della Tm anche per le quattro sonde rpoB con rianalisi dei dati di validazione per diminuire le possibilità che possa essere rilasciato un risultato non corretto sulla resistenza genotipica alla Rifampicina del gene rpoB.  (rianalisi da allegare).
- aggiornare AP dei samples con le modifiche sopr',NULL,NULL,N'26/03/2021
25/03/2021
25/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021',N'30/04/2021
14/05/2021',N'25/03/2021',N'No',N'nessun impatto sul dmri',N'Si',N'attraverso TAB',N'No',N'nessun impatto',N'L''aggiornamento del modello interpretativo 12 da parte di PSS permetterebbe',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',N'da valutare',N'No',N'la modifica degli AP non è considerata significativa',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-14 00:00:00',NULL,NULL),
    (N'20-24',N'Correttiva','2020-08-04 00:00:00',N'Stefania Brun',N'Ordini',N'Non conformità',N'20-62',N'mancata verifica dello stato regolatorio dei prodotti prima di evasione dell''ordine',N'Documentazione SGQ',N'La consultazione di un file esterno al sistema gestionale Navision da parte dell''ufficio ordini in fase di inserimento ordini per verificare lo stato regolatorio del prodotto presso il paese di destinazione non è di facile integrazione nell''attività di order processing soprattutto nel periodo attuale che ha visto un aumento del carico di lavoro a seguito dell''emergenza Covid',N'Attivare la funzione "Item registration" presente nel core model di Nav2018, descritto al paragrafo 2.7 del documento di Requisiti.
Integrare le informazioni contenute nel modulo MOD05,29 Stato registrazioni prodotti IVD all''interno delle anagrafiche clienti
Aggiornare la procedura degli ordini indicando questa modalità operativa',NULL,N'Positivo in quanto l''attivazione della funzionalità di Nav permette di rendere più veloce il controllo dello stato di registrazione del prodotto presso il paese di destinazione
Positivo in quanto l''attivazione della funzionalità di Nav permette di rendere più veloce il controllo dello stato di registrazione del prodotto presso il paese di destinazione
La funzionalità è già presente all''interno del core model di Nav 2018 pertanto non richiede attività aggiuntiva da parte di ITM
La funzionalità è già presente all''interno del core model di Nav 2018 pertanto non richiede attività aggiuntiva da parte di ITM
La funzionalità è già presente all''interno del core model di Nav 2018 pertanto non richiede attività aggiuntiva da parte di ITM
Positivo in quanto l''utilizzo di questa funzionalità permette di rendere più automatico il processo di verifica dello stato di registrazione 
Positivo in quanto l''utilizzo di questa funzionalità permette di rendere più automatico il processo di verifica ',N'06/08/2020
06/08/2020
04/08/2020
04/08/2020
04/08/2020',N'Eseguire il test di verifica della funzionalità "Product status registration" in ambiente di test 
Aggiornare la procedura degli ordini PR03,01 indicando questa modalità operativa
Supervisione dell''attività di test
Valutazione del caricamento massivo delle info presenti sul MOD05,29. Necessarie i seguenti dati: codice cliente, codice articolo e data
formazione del personale utilizzatore
Upload (da valutare se in modalità automatica o manuale) su Nav delle informazioni presenti nel MOD05,29 dando priorità ai clienti dei paesi MDSAP (Canada, Brasile, Australia, Giappone e USA)
Aggiornare il RA SP8 con le nuove modalità di controllo e il VMP
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica',N'la modifica non è significativa, pertanto non è da notificare. - rac20-23_mod05,36__non_significativa_1.pdf',NULL,N'06/08/2020
06/08/2020
06/08/2020
06/08/2020
06/08/2020
06/08/2020
06/08/2020
06/08/2020',N'04/09/2020
14/09/2020
04/09/2020
04/09/2020
14/09/2020
14/09/2020
14/09/2020',N'26/08/2020',N'No',N'No in quanto non ha impatti sul DMRI',N'No',N'No in quanto è una funzionalità usata nel processo ordini',N'No',N'No in quanto è una funzionalità usata nel processo ordini',N'E'' una funzionalità presente all''interno del gestionale Nav 2018 usata nel processo ordini che permetterà di essere più confidenti rispetto ai requisiti regolatori dei paesi extra europei nel momento in cui viene processato un ordine',N'SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'SP8 - Ordine da EGSpA',N'il RA SP8 è da aggiornare con le nuove modalità di controllo',N'No',N'No in quanto è una funzionalità usata nel processo ordini',N'No',N'Tale modifica non coinvolge aspetti relativi a prodotti ',N'Stefania Brun',N'No',NULL,NULL,N'Verificare a seguito della messa in uso della funzionalità che non ci siano NC imputabili alla spedizione di prodotti privi della registrazione presso la locale CA senza etichetta RUO o IUO',NULL,NULL,NULL,NULL,NULL,'2020-09-14 00:00:00',NULL,NULL),
    (N'21-4',N'Correttiva','2021-01-28 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Non conformità',N'21-11',N'RTS180ING ',N'Assay Protocol',N'In base all''indagine eseguita è stata identificata come causa della non conformità una contaminazione in sede di preparazione della premix. Data la sensibilità elevata del sistema per il controllo interno, dovuta sia al fatto che l''amplicone è corto sia per il canale in cui viene letto, resta necessario valutare la necessità di spostare i range attualmente previsti per la validità del controllo interno',N'Si chiede la modifica del criterio di validità del controllo negativo dell''assay protocol (oggi ct>40) e della verifica tecnica di CQ. Tale modifica richiede la rivalutazione del cutoff per i campioni validi (oggi Ct minore di 40)                    ',NULL,N'Nessun impatto sul processo ma solo in termini di attività
I criteri di CQ risulterebbero allineati a quelli previsti dall''AP garantendo una valutazione delle performance sulla base dell''uso previsto
Nessun impatto sul processo ma solo in termini di attività
Non ci sono impatti a livello di performance di prodotto: data l''elevata sensibilità del controllo interno si ritiene opportuno introdurre un cutoff nel criterio di CQ relativo al IC e di rivalutare i dati del cutoff dell''IC che permette di definire l''idoneità del campione, al fine di inserire un ct limite in linea con i dati ottenuti durante la validazione del prodotto.
La modifica non impatta sulle performance del prodotto.
La modifica non impatta sulle performance del prodotto.
Non ci sono impatti a livello di performance di prodotto: data l''elevata sensibilità del controllo interno si ritiene opportuno introdurre un cutoff nel criterio di CQ relativo al IC e di rivalutare i dati del cutoff dell''IC che permette di definire l''idoneità ',N'28/01/2021
28/01/2021
28/01/2021
28/01/2021
28/01/2021
28/01/2021
09/03/2021',N'Installazione della nuova revisione dell''AP su Ingenius dei clienti
Modifica della Verifica tecnica con i nuovi requisiti
Formazione
Esecuzione del CQ sul lotto NC U0121-082 con i nuovi criteri
invio di un nuovo campione in CQ del lotto U0121-082 con riferimento alla NC21-11 e alla RAC21-4
Controllo dell''AP del CTR negativo e dei campioni
Messa in uso dei moduli di CQ
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  
Analisi dei dati di validazione
Modifica dell''AP del CTR negativo e dei campioni',N'tab_p44revaa_product_sars-cov-2_plus_elite_mgb - rac21-4_tab_p44revaa_product_sars-cov-2_plus_elite_mgb.msg
mod1012-ctr180ing_e_rts180ing - rac21-4_messa_in_uso_mod1012-ctr180ing_e_rts180ing.msg
ap_sars-cov-2_plus_rts180ing - rac21-4_ap_sars-cov-2_plus_rts180ing.msg
mod1012-ctr180ing_e_rts180ing_01 - rac21-4_messa_in_uso_mod1012-ctr180ing_e_rts180ing_1.msg
ap_sars-cov-2_plus_rts180ing_1 - rac21-4_ap_sars-cov-2_plus_rts180ing_1.msg
La modifica apportata riguarda i criteri di CQ, tuttavia essendo informazioni riservate, la notifica verrà fatta presso le autorità competenti solo dei paesi che hanno richiesto in fase di registrazione questo tipo di documenti. A marzo/2021 il prodotto non risulta registrato presso altri stati. - rac21-4_mod05,36__significativa.pdf',NULL,N'28/01/2021
28/01/2021
02/02/2021
28/01/2021
28/01/2021
28/01/2021
09/03/2021',N'15/02/2021',N'16/03/2021
10/03/2021
10/03/2021
08/03/2021
10/03/2021
28/01/2021
08/03/2021',N'No',N'nessun impatto',N'No',N'comunicazione tramite TAB',N'No',N'nessun impatto',N'La modifica non impatta sulle performance del prodotto.',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',N'nessun impatto su RA AP3',N'Si',N'veder MOD05,36. La modifica apportata riguarda i criteri di CQ, tuttavia essendo informazioni riservate, la notifica verrà fatta presso le autorità competenti solo dei paesi che hanno richiesto in fase di registrazione questo tipo di documenti. A marzo/2021 il prodotto non risulta registrato presso altri stati. la modifica degli AP non è da notificare.',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-15 00:00:00','2021-03-16 00:00:00',NULL),
    (N'18-39',N'Correttiva','2018-12-19 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',N'Non conformità',N'18-138',N'NC CEISO',N'Documentazione SGQ',N'Mancata esplicitazione nella PR02,01 della qualifica degli auditor interni reispetto ai requisiti regolatori. ',N'Aggiornamento della procedura PR02,01 e raccolta sistematica delle evidenze relative alla formazione sui regolamenti. 
Verificare lo stato di formazione degli auditor interni e valutarne l''idoneità.',NULL,N'Impatto positivo in quanto si da evidenza che il personale che conduce audit interni è formato rispetto ai regolamenti extra UE.
Impatto positivo in quanto si da evidenza che il personale che conduce audit interni è formato rispetto ai regolamenti extra UE.
Impatto positivo in quanto si da evidenza che il personale che conduce audit interni è formato rispetto ai regolamenti extra UE.',N'19/12/2018
19/12/2018
19/12/2018',N'Verificare lo stato di formazione degli auditor interni e valutarne l''idoneità.
Aggiornamento della procedura PR02,01 e del programma di audit MOD02,01.
Raccolta sistematica delle evidenze relative alla formazione sui regolamenti. ',N'Auditor Interni Fazari, Paviolo, Brun, Catalano, formati secondo i regolamenti MDSAP - mod02,01_00_programma_audit_interni_e_fornitori_2019_1.pdf
programma audit del 15/02/2019 - mod02,01_00_programma_audit_interni_e_fornitori_2019.pdf
MESSA IN USO PR02,01_03, MOD02,02_02 il 5/3/19(MOD18,02 del 5/03/19) - rac18-39_messa_in_uso_pr0201_03_mod0202_02_rap18-22_rac1839_mod1802_del_50319.msg
MOD18,02 212/06/19 IVDR2017/746 (Docente Fazari), MOD18,02 06/05/2019 MDSAP (Docente Fazari),  MOD18,02 28/03/19 IVDR2017/746 (Docente Ceiso), MOD18,02 28/03/19 IVDR2017/746 (Docente Ceiso), MOD18,02 17/01/18 IVDR2017/746 (Docente Ceiso) - rac18-39_elencoformazioneregolamenti.docx',NULL,N'19/12/2018
19/12/2018
19/12/2018',N'15/03/2019
15/03/2019
15/03/2019',N'15/02/2019
05/03/2019
12/06/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'-',N'AU - Gestione audit interni ed esterni',N'AU1 - Programmazione di audit interni e audit a fornitori',N'RA AU1',N'No',N'non necessaria',N'No',N'non necessaria',NULL,N'No',NULL,'2019-09-30 00:00:00',N'Come da Report DEKRA del 19/09/2019, numero Report identification: 2235223-AR14-R0 DRAFT, non ci sono state NC imputabili alla formazione degli auditor interni sui regolamenti dei paesi aderenti al MDSAP. ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-03-15 00:00:00','2019-06-12 00:00:00','2020-01-07 00:00:00'),
    (N'18-16',N'Preventiva','2018-06-04 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,NULL,N'Durante la modifica dei documenti di CQ dei plasmidi, non è stato valutato correttamente l''impatto che la mancata verifica della diluizione delle 10 copie potesse avere sul CQ del BANG07.',N'Si chiede di modificare la Scheda appunti di CQ del prodotto BANG07 prevedendo la preparazione della diluizone 10  copie da parte di QCT durante il CQ stesso.',NULL,N'La procedura richiesta nella modifica è già utilizzata per tutti i prodotti Real Time qualitativi, per cui tale procedura è già validata.
Durante la preparazione del CTRG07 non sarà necessario eseguire anche la preparazione del punto 10. 
Nessun impatto sul processo di R&D
Nessun impatto sul sistema gestione qualità.',N'04/06/2018
13/06/2018
14/06/2018
14/06/2018',N'Modifica del modulo MOD10,13-PCR-ALert-onco-3, MOD09,23 e MOD10,18
nessuna attività a carico della produzione
nessuna attività a carico di R&D
Messa in uso documenti',N'mod10,13-pcralert-onco3 rev02, MOD18,,02 del 5/06/18 - rap18-16_messa_in_uso_mod1013-pcr-alert-onco-3_rev02_rap18-16.oft
messa in uso MOD10,13-pcr-alert-onco-3_rev02 - rap18-16_messa_in_uso_mod1013-pcr-alert-onco-3_rev02_rap18-16_1.oft',NULL,N'04/06/2018
14/06/2018
14/06/2018
14/06/2018',N'15/06/2018
14/06/2018
14/06/2018
14/06/2018',N'14/06/2018
14/06/2018
14/06/2018
14/06/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'GM - Gestione delle modifiche',NULL,N'GM5 "Verifica/individuazione  degli impatti che la modifica ha sul processo/prodotto (CC - RAC/RAP) ed approvazione degli stessi"',N'No',NULL,N'No',NULL,N'Federica Farinazzo',N'No',NULL,'2019-01-14 00:00:00',N'Il nuovo lotto di CTRG07, U0319BL, non ha dato NC. Essendo la procedura, richiesta nella modifica, già utilizzata per tutti i prodotti Real Time qualitativi ed essendo una procedura validata si reputa non necessario attendere i 3 lotti. ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-06-15 00:00:00','2018-06-14 00:00:00','2019-03-25 00:00:00'),
    (N'18-7',N'Correttiva','2018-02-19 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Reclamo',N'18-20',N'Reclamo registrato su db Lupin rif. Reclamo n°19-18 (data 15/02/2018)',NULL,N'1. Ai fini dell''indagine è stato ripetuto il test di controllo qualità sul lotto che ha confermato i risultati ottenuti con il primo test eseguito, i risultati ottenuti rientrano nei limiti stabiliti per il fattore V mutato.
2. Rianalizzati i dati ottenuti in fase di sviluppo e validazione. I primi compresa la media delle 3 deviazioni standard della temperatura di melting per il Fattore V mutato rientrano nei requisiti stabiliti, i dati di validazione rientrano anch''essi ma la media delle 3 deviazioni standard della temperatura di melting per il Fattore V mutato ricade sul limite superiore (68)
3. Recupero dei dati ottenuti dagli altri utilizzatori del prodotto RTSD00ING - Coagulation - ELITe MGB KIT per confermare ipotesi della variabilità strumentale 
In base ai risultati dell''indagine la causa primaria è attribuibile all''incompletezza dell''analisi statistica dei dati di validazione.',N'Ridefinizione del valore superiore della temperatura di melting per il Fattore V mutato',NULL,N'Valutazione della fattibilità circa ''innalzamento del limite del valore superiore della temperatura di melting per il Fattore V mutato 
Valutazione della fattibilità circa ''innalzamento del limite del valore superiore della temperatura di melting per il Fattore V mutato 
Valutazione della fattibilità circa ''innalzamento del limite del valore superiore della temperatura di melting per il Fattore V mutato 
Valutazione della fattibilità circa ''innalzamento del limite del valore superiore della temperatura di melting per il Fattore V mutato 
Modifica moduli CQ MOD10,12- RTSD00ING-CTRD00ING  "Verifica Tecnica". 30/05/2018 Annex to complaint #18-20: non è necessario modificare i requisiti di CQ.
Notifica all''autorità competente italiana e verifica necessità di notifica per CA extra-UE secondo i regolamenti nazionali. Ritiro del lotto non si applica in quanto come evidenziato nell''indagine il lotto è conforme ai requisiti
Notifica all''autorità competente italiana e verifica necessità di notific',N'19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018',N'Modifica requisiti dell''Assay protocol del prodotto RTSD00ING
Modifica dell''Assay protocol CQ per aggiornamento requisito
Aggiornamento IFU per requisito e rinforzo indicazione che in caso di approvazione del risultato l''utilizzatore deve visionare anche il grafico 
Rivalutare l''appropriatezza delle tecniche statistiche utilizzate in fase di sviluppo e validazione 
Notifica all''autorità competente italiana dell''evento
Stesura nota informativa da inviare ai clienti del prodotto RTSD00ING
Aggiornamento analisi dei rischi del prodotto sulla base dei dati raccolti per rivalutare la probabilità dell''evento (falso negativo)
Notifica all''autorità competente Italiana della chiusura delle attività.
Aggiornare procedura PR04,04 "Gestione della validazione di prodotto"
Definire piano per aggiornamento della versione modificata dell''Assay protocol presso i clienti ed eseguire update sugli strumenti installati',N'Assay - rac18-7_messa_in_uso_ifu_rtsd00ing_e_assay.pdf
Annex to complaint #18-20: NON necessaria la modifica dei requisiti di CQ - 2018-05-30_egspa_investigation_complaint18-20.pdf
IFU RTSD00ING rev01 - rac18-7_messa_in_uso_ifu_rtsd00ing_e_assay_1.pdf
ALLEGATO 5 "RAPPORTO INIZIALE EVENTO" - 2018-03-15_elitech_rapporto_iniziale_evento.pdf
nota informativa clienti (FSN18/01-ITA) - 2018-03-15_fsn_18-01_coagulation_ita.pdf
field safety notice (FSN15-01-EN) - 2018-03-15_elitech_fsn_18-01_coagulation_en.pdf
Annex to complaint #18-20: non è necessario aggiornare l''analisi dei rischi - 2018-05-30_egspa_investigation_complaint18-20_1.pdf
PR04,04 rev01 - rac18-7_messa_in_uso_pr04,04_rev01_1.pdf
Lista clienti lotto - rtsd00ing_lotti_distribuiti_bis.xlsx
Lista clienti aggiornata al 5-giu-2018 - rac18-7_elenco_clienti_aggiornato_al_5-giu-2018.pdf
TAB coagulation firmata il 8/6/2018 - 2018-06-08_tab-coagulation.pdf
Nota informativa clienti 14/06/2018 - nota_info_coagulazione_20180614.pdf
PR04,04 rev01 - rac18-7_messa_in_uso_pr04,04_rev01.pdf
2018-06-08_ALLEGATO 2 "RAPPORTO FINALE" - 2018-06-08_elitech_rapporto_chiusura_azioni.pdf',NULL,N'19/02/2018
19/02/2018
13/03/2018
19/02/2018
19/02/2018
19/02/2018
19/02/2018
08/06/2018
19/02/2018
19/02/2018',N'30/03/2018
30/03/2018
30/03/2018
30/03/2018
30/03/2018
16/03/2018
27/04/2018
15/06/2018
30/03/2018
30/03/2018',N'06/06/2018
30/05/2018
06/06/2018
08/06/2018
13/03/2018
15/03/2018
30/05/2018
08/06/2018
08/06/2018
08/06/2018',N'No',NULL,N'Si',N'Stesura nota informativa da inviare ai clienti, utilizzatori del prodotto RTDS00ING',N'Si',N'L''assay protocol del prodotto RTSD00ING nella nuova revisione dovrà essere sostituito presso i clienti InGenius del prodotto RTSD00ING. All''estero le ICO e i distributori saranno informati sulla nuova revisione dell''assay protocol e sui tempi per l''aggiornamento attraverso una TAB',NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'Si',NULL,'2019-01-08 00:00:00',N'Dal 8/6/18, data di implementazione della misura correttiva, al 31/12/2018, non ci sonostati reclami la cui causa primaria erariconducibile a questa RAC.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-06-15 00:00:00','2018-06-08 00:00:00','2019-01-08 00:00:00'),
    (N'18-6',N'Preventiva','2018-02-02 00:00:00',N'Stefania Brun',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'L''attività di assistenza di 1° livello è una tra le fonti di informazione da cui può originare un reclamo',N'Modificare la PR14,02 "GESTIONE DEI RECLAMI E DELLE SEGNALAZIONI" rev. 02 al fine di definire tra gli input che possono dare origine ad un reclamo l''attività di assistenza di 1° livello',NULL,N'Positivi in quanto viene dettagliata l''interazione tra l''attività di supporto e il monitoraggio delle prestazioni dei prodotti in quanto la prima è un per la rilevazione dei reclami 
Positivi in quanto previene la possibilità che non venga intercettato un potenziale reclamo. Aggiornamento e validazione del permesso di scrittura della prima parte del reclamo per i livelli di "specialist" e "operatore base". 
Positivi in quanto previene la possibilità che non venga intercettato un potenziale reclamo. Aggiornamento e validazione del permesso di scrittura della prima parte del reclamo per i livelli di "specialist" e "operatore base". 
Positivi in quanto previene la possibilità che non venga intercettato un potenziale reclamo ed evita ritardi nell''esecuzione delle azioni successive ',N'02/02/2018
02/02/2018
02/02/2018
02/02/2018',N'verifica della PR14,02
Inserimento del permesso di scrittura per la prima parte del reclamo (fino al campo "Reclamo fondato") per il livello di "Operatore base" e "Specialist", validazione delle modifiche. Inserimento di tutto il personale che si occupa di reclami in "Gestione utenti".
revisione della PR14,02
verifica della PR14,02',N'piano di validazione (cartaceo firmato) - allegato_4_al_piano_di_validazione_mod05,23_00_piano_validazione_sw31_2018-01-10.pdf
report di validazione (cartaceo firmato) - allegato_9_al_rapporto_di_validazione_mod05,24_00_rapporto_validazione_sw31_2018-01-24.pdf
pr14,02_rev03_MOD18,02 del 23/05/18 - rap18-6_messa_in_uso_pr14,02_03.pdf',NULL,N'02/02/2018
02/02/2018
02/02/2018
02/02/2018',N'15/06/2018
28/02/2018
15/06/2018
15/06/2018',N'04/06/2018
28/02/2018
04/06/2018
04/06/2018',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'La procedura non ha impatto sui requisiti regolatori, ma tratta la gestione interna dei reclami',N'ASA - Assistenza applicativa prodotti',N'ASA1 - Gestione reclamo',N'il risk assessment è da aggiornare',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'Stefania Brun',N'No',NULL,'2020-09-30 00:00:00',N'Verificare nell''arco di 12 mesi i reclami aperti secondo la nuova procedura. Aggiornare il risk assessment valutando se i reclami gestiti secondo la nuova procedura, hanno una ridotta probabilità di avere tempi di presa in carico da parte del secondo livello di assistenza (GATL e GSTL) non allineati.
02/2020 l''apertura dei reclami di performance da parte di CCA è bloccata a causa di assenza della funzione. Con questa modalità si presume una ridurre dei tempi. la valutazione dell''efficacia è rimandata a 09/2020.
',NULL,NULL,NULL,NULL,NULL,'2018-06-15 00:00:00','2018-06-04 00:00:00',NULL),
    (N'20-30',N'Correttiva','2020-09-16 00:00:00',N'Lucia  Liofante',N'Controllo qualità',N'Non conformità',N'20-77',N'962-RTS170ING',N'Documentazione SGQ',N'Il ridotto numero di run eseguite sullo strumento ha determinato un basso valore di deviazione standard per la Qty del Positive Control (PC) e di conseguenza un intervallo "media Qty +/-3SD" piuttosto stretto.
La Conformità del lotto di Mix in test è confermata dell''analisi dei Ct del PC: il PC "failed" ha il medesimo Ct di un PC risultato "passed" (lotto CQ U0620-047).
Si evidenzia che nelle prime fasi di utilizzo del prodotto in modalità quantitativa il cliente potrà verosimilmente riscontrare lo stesso tipo di inconveniente.',N'Modifica dei moduli MOD10,12 di CQ collegati al prodotto SARS-CoV-2 ELITe MGB Kit con eliminazione degli Shortened Results come criterio di accettazione (come evidenziato nell''indagine sulla NC, non sono significativi per la valutazione delle performance del lotto).',NULL,N'Impatto positivo:  si correggerà presso il cliente il manifestarsi di risultati "failed" sul Positive Control motivati da scarsità di dati.
Impatto positivo:  si correggerà presso il cliente il manifestarsi di risultati "failed" sul Positive Control motivati da scarsità di dati.
Impatto positivo: il manifestarsi di un risultato "failed" non pregiudicherà la conformità del lotto
Nessun impatto a parte le azioni
Nessun impatto se non in termini di azioni.
Nessun impatto se non in termini di azioni.
Nessun impatto a parte le azioni',N'16/09/2020
16/09/2020
16/09/2020
28/09/2020
16/09/2020
16/09/2020
16/09/2020',N'nessun attività a carico
-
Modifica verifiche tecniche MOD10,12-RTS170ING, MOD10,12-STD170ING, MOD10,12-CTR170ING e relativa formazione : eliminazione criterio PC rispetto al calcolo eseguito per le carte di controllo (deviazioni standard tra lotti)
Modifica relative schede appunti: inserimento indicazione per cancellazione Control Chart del controllo positivo
Determinare un range di accettazione per la Control Chart del Positive Control tale da limitare il rischio del manifestarsi di risultati "failed"
Messa in uso nuova revisione moduli
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica
Monitorare l''implementazione del ticket 450 su Mantis, riguardante la possibilità di caricare su InGenius 4 valori predefiniti per la control chart (= già requisito per il software del BeGenius)',N'MOD10,13-ctr170ing rev02 - cc20-65_messa_in_uso_mod1013-ctr170ing_2.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - cc20-65_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_2.msg
MOD10,13-ctr170ing rev02 - cc20-65_messa_in_uso_mod1013-ctr170ing_3.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - cc20-65_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_3.msg',NULL,N'16/09/2020
16/09/2020
16/09/2020
16/09/2020
16/09/2020',N'15/10/2020',N'16/09/2020
06/11/2020
06/11/2020
06/11/2020',N'No',N'nessun impatto',N'No',N'nessun impatto in quanto il prodotto std170ing non è ancora in commercio',N'No',N'nessun impatto in quanto il prodotto std170ing non è ancora in commercio',N'da valutare',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'a fine anno valutare l''impatto sul RA R&D1.1',N'No',N'il prodotto STD170ING non è ancora immesso in commercio, pertanto non è necessario effettuare alcuna notifica',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-15 00:00:00',NULL,NULL),
    (N'20-38',N'Correttiva','2020-10-22 00:00:00',N'Renata  Catalano',N'Sistema qualità',N'Non conformità',N'20-97',N'SARS-CoV-2 ELITe MGB Kit RTS170ING-96',N'Etichette',N'L''indagine condotta ha rilevato la presenza dell''errore (codice a barre lineare inferiore disallineato) già a livello del template presente su Navision. Come da "Istruzione operativa creazione etichette prodotti", IO04,06 Rev.03, il template in questione è stato creato nel corso del CC20-47 ed è stato approvato mediante MOD04,13 "Check list approvazione etichette". Nel modulo è presente fra le informazioni da verificare contenute nell''etichetta il "Codice a barre lineare" del quale occorre verificare le seguenti informazioni: identificativo dell''azienda e codice prodotto (informazioni presenti nel codice a barre lineare superiore) e lotto e scadenza (informazioni presenti nel codice a barre lineare inferiore). Sebbene queste informazioni siano presenti in due codici a barre differenti la verifica è unica. Nel caso del MOD04,13 relativo al prodotto RTS170ING-96, QM ha effettuato unicamente il controllo del codice a barre superiore registrando poi come conforme "C" la verifica effettuata sul parametro "Codice a barre lineare". Il disallineamento del codice a barre inferiore generato a livello della creazione del template, non è così stato intercettato in fase di verifica e approvazione dell''etichetta. La non conformità  generata è dunque imputabile alla mancata richiesta da parte del MOD04,13 di verificare entrambi i codici a barre lineari. ',N'La presente RAC richiede una correzione ed un azione correttiva. La correzione consiste nel modificare il template dell''etichetta esterna del prodotto SARS-CoV-2 ELITe MGB Kit, codice RTS170ING-96 allineando la posizione del codice a barre inferiore e aggiornare il MOD04,13 relativo. La modifica del template è già stata eseguita da ITT. L''azione correttiva prevede la modifica del MOD04,13 sdoppiando la voce "Codice a barre lineare" in modo da verificare separatamente il codice a barre superiore e inferiore',NULL,N'Impatto positivo. La verifica di entrambi i codici a barre lineari nel MOD04,13 consentirà di garantire un controllo più efficace e completo degli stessi
Impatto positivo. La verifica di entrambi i codici a barre lineari nel MOD04,13 consentirà di garantire un controllo più efficace e completo degli stessi
Impatto positivo. La verifica di entrambi i codici a barre lineari nel MOD04,13 consentirà di garantire un controllo più efficace e completo degli stessi',N'06/11/2020
06/11/2020
06/11/2020',N'Aggiornare il MOD04,13 relativo al prodotto SARS-CoV-2 ELITe MGB Kit, codice RTS170ING-96, per approvare il template modificato
Aggiornare il modello relativo al MOD04,13 sdoppiando la voce "Codice a barre lineare" in modo da verificare separatamente il codice a barre superiore e inferiore. Mettere in uso il nuovo modello
Valutare la significatività della modifica ed effettuare l''eventuale notifica preliminare ',N'mod04,13 rev04 - rac20-38_messa_in_uso_mod0413_rev04.msg
 non necessaria, il codice a barre è utilizzato eventualmente a fini logistici, non riguarda il labelling del prodotto - rac20-38_mod05,36_01_non_significativa.pdf',NULL,N'06/11/2020
06/11/2020
06/11/2020',N'16/11/2020',N'05/03/2021
12/11/2020
09/12/2020',N'No',N'nessun impatto',N'No',N'non necessaria, le informazioni regolatorie riportate sull''etichetta (es. ref, lotto e scadenza) sono leggibili visivamente, il codice a barre lineare sull''etichetta esterna è utilizzato eventualmente per fini logistici.',N'No',N'nessun impatto',N'Le informazioni regolatorie riportate sull''etichetta (es. ref, lotto e scadenza) sono leggibili visivamente, il codice a barre lineare sull''etichetta esterna è utilizzato eventualmente per fini logistici, ma non influisce sull''identificazione del prodotto.',N'ET - Gestione Etichette',N'ET3 - Approvazione dei contenuti dell''etichetta',N'nessun impatto sul risk assessment ET3',N'No',N'vedere MOD05,36. non necessaria, il codice a barre è utilizzato eventualmente a fini logistici, non riguarda il labelling del prodotto',N'No',N'nessun virtual manufacturer',N'Stefania Brun',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-11-16 00:00:00','2021-03-05 00:00:00',NULL),
    (N'19-36',N'Preventiva','2019-11-18 00:00:00',N'Silvia Costa',N'Validazioni',NULL,NULL,NULL,N'Manuale di istruzioni per l''uso',N'Passaggio dalla versione software di InGEnius 1.2 a 1.3. I test svolti con la 1.2 erano risultati conformi.',N'Aggiornamento dell''IFU di ENTEROVIRUS ELITe MGB Kit per il calcolo dei volumi dei tre componenti da utilizzare per preparare la miscela completa di reazione (fino a 5 campioni +1 e da 6 a 12 +2, come HEV) ed adeguamento del numero totale di reazioni che è possibile effettuare in associazione ad InGenius (da 96 a 92).
Tale aggiornamento sarà valido solo nel periodo di tempo che intercorre fino alla manutenzione di prodotto (prevista nel 2021) che risolverà tale problematica.',NULL,N'La riduzione del numero di reazioni permette l''utilizzo del kit in associazione allo strumento ELITe InGenius  anche per sessioni da 6 campioni in su. E'' necessario ri-conteggiare i kit da offrire in gara per il periodo di tempo che intercorre fino alla manutenzione di prodotto (prevista nel 2021) che risolverà tale problematica.
La riduzione del numero di reazioni permette l''utilizzo del kit in associazione allo strumento ELITe InGenius  anche per sessioni da 6 campioni in su. E'' necessario ri-conteggiare i kit da offrire in gara per il periodo di tempo che intercorre fino alla manutenzione di prodotto (prevista nel 2021) che risolverà tale problematica.
Per le gare in essere potrebbe essere necessario fornire dei kit in omaggio. Per il periodo di tempo che intercorre fino alla manutenzione di prodotto (prevista nel 2021), che risolverà tale problematica, verranno conteggiati dafli specialist nuovamente i kit da offrire in gara.
La riduzione del numero di reazioni deve essere comunicata propriamente',N'02/12/2019
02/12/2019
02/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019',N'Revisionare IFU e aggiornare TAB.
Conteggio reazioni da offrire in gara
verificare le gare attivie per il prodotto RTS076PLD  e verificare l''impatto del ri-conteggio del numero di reazioni offerte
verificare le brochure e presentazioni.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Revisione IFU
messa in uso IFU
Aggiornamento IFU e revisione TAB
aggiornamento listini',N'sch_mRTS076PLD_rev04 - rap19-36_ifu_rts076pld_ctr076pld.msg
TAB P22 - Rev AB  - rap19-36_elite_ingenius_-tab_p22_-_rev_ab_-__product_ev_elite_mgb_notice_signed.pdf
 sch_mRTS076PLD_rev04  - rap19-36_ifu_rts076pld_ctr076pld_1.msg
 sch_mRTS076PLD_rev04  - rap19-36_ifu_rts076pld_ctr076pld_3.msg
TAB P22 - Rev AB - rap19-36_elite_ingenius_-tab_p22_-_rev_ab_-__product_ev_elite_mgb_notice_signed_1.pdf
Per le gare in essere verranno dati degli omaggi per compensare le insufficienze, laddove necessario. - rap19-36_omaggi_per_compensare_le_insufficienze_delle_gare_in_essere.msg
Notifica effetuata - rap19-36_notification_of_change_-rts076pld-ctr076pld_-_rap19-36.msg
la modifica è significativa, pertanto da notificare nei paesi in cui il prodotto è registrato. - rap19-36_mod05,36__significativa.pdf
 sch_mRTS076PLD_rev04  - rap19-36_ifu_rts076pld_ctr076pld_2.msg',NULL,N'02/12/2019
02/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019
17/12/2019',N'17/12/2019',N'06/04/2020
06/04/2020
17/12/2019
17/12/2019
26/02/2020',N'No',N'nessun impatto',N'No',N'atraverso l''avvertenza dell''IFU',N'No',N'nessun impatto',N'le performance del prodotto non variano, ma varia il ri-conteggio del numero di reazioni.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto',N'Si',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2021-06-30 00:00:00',N'Monitoraggio dell''assenza di reclami da 03/2020 alla manutanzione di prodotto prevista entro 12/2021.',NULL,NULL,NULL,NULL,NULL,'2019-12-17 00:00:00',NULL,NULL),
    (N'20-2',N'Correttiva','2020-02-04 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'19-122',N'RTSD00ING',N'Progetto',N'Dall''analisi dei database dei due strumenti si conferma un''interpretazione non corretta del genotipo del FV e dell''MTHFR sui campioni di sangue intero, oggetto del reclamo, con il prodotto RTSD00ING. 
Di seguito i dettagli:
- Caso 1: sullo strumento B0048 il campione viene rilevato omozigote wild type per il FV, nonostante siano evidenti nei grafici delle temperature di melting  entrambi i picchi di Tm (wt e Mut), perchè il valore dell''altezza del picco di Tm per l''allele Mut del FV è al di sotto della soglia di Threshold impostata (Tm Cut Off Threshold=50) nell''Assay protocol.
- Caso 2: sullo strumento B00254 i 5 campioni vengono rilevati omozigoti mutati per l''MTHFR, nonostante siano evidenti nel grafico delle temperature di melting entrambi i picchi di Tm (wt e Mut), perchè il valore dell''altezza del picco di Tm per l''allele wt dell''MTHFR  è al di sotto della soglia di Threshold impostata (Tm Cut Off Threshold=20) nell''Assay protocol.
La causa primaria potrebbe essere imputabile ad una combinazione di fattori quali la variabilità inter-strumento della lettura di fluorescenza acquisita  e le variabilità minime di performance tra i lotti del prodotto RTSD00ING che possono aver contribuito a rendere  le impostazioni dell''Assay protocol in uso troppo stringenti.',N'Azioni a breve termine: 
1. Sorveglianza post market: raccolta e analisi dei dati (circa 7000 campioni) di più centri utilizzatori del prodotto (Teramo, Taranto, Cile e Francia) per verificare l''eventuale incidenza di interpretazioni non corrette dei risultati.
2. Valutazione delle modifiche nell''Assay Protocol dei parametri di Tm Cut Off Threshold e Tm Auto Threshold per il FV, l'' MTHFR e il FII per rendere rilevabili i picchi risultati sotto soglia. 
3. Rianalisi dei dati discordanti emersi dal reclamo (ed eventualmente dall''analisi di sorveglianza post-market) con i nuovi parametri per valutare l''efficacia delle modifiche introdotte.
4. Rianalisi dei dati di validazione con i nuovi parametri per valutare l''impatto delle modifiche apportate.',NULL,N'La modifica delle impostazioni dell''AP può avere impatto positivo a fronte della ri- valutazione dei campioni (clinici e simulati) testati durante la Fase di Verifica e di Validazione del prodotto al fine di confermare la corretta genotipizzazione anche con la nuova impostazione dell''AP. 
Sarà necessario validare internamente che l''abbassamento della soglia di rilevazione consenta effettivamente di ridurre il rischio di risultati discrepanti o invalidi, evitando tuttavia che tale abbassamento generi la rilevazione di segnali NON-specifici (background).
La modifica delle impostazioni dell''AP può avere impatto positivo a fronte della ri- valutazione dei campioni (clinici e simulati) testati durante la Fase di Verifica e di Validazione del prodotto al fine di confermare la corretta genotipizzazione anche con la nuova impostazione dell''AP. 
Sarà necessario validare internamente che l''abbassamento della soglia di rilevazione consenta effettivamente di ridurre il rischio di risultati discrepanti o invalidi,',N'04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020',N'Rianalisi dei risultati discordanti provenienti dai reclami e dall''analisi di sorveglianza post-market per quanto riguarda il Coagulation ELITe MGB Kit per valutare le modifiche nell''Assay Protocol dei parametri di Tm Cut Off Threshold e Tm Auto Threshold per il FV, l'' MTHFR e il FII per rendere rilevabili i picchi risultati sotto soglia. 
Ri-analisi delle sedute su campioni clinici e campioni simulati effettuate durante la Fase di Verifica e di Validazione del prodotto, al fine di valutar l''impatto delle modifiche introdotte sui dati pregressi
creazione assay protocol
Redazione di una nota tecnica per tutti gli utilizzatori del prodotto Coagulation
messa in uso assay protocol
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Redazione TAB
Installazione nuova versione AP presso clienti Coagulation
revisione assay protocol e TAB',N'Annex to RAC20-2 - Improved Assay Protocol for Coagulation ELITe MGB Kit - rac20-2_annex_to_rac20-2_report.pdf
Annex to RAC20-2 - Improved Assay Protocol for Coagulation ELITe MGB Kit  - rac20-2_annex_to_rac20-2_report_1.pdf
ELITe InGenius -TAB P12- Rev AE - Product Coagulation ELITe MGB notice_in uso - rac20-2_messa_in_uso_tab.msg
ELITe InGenius -TAB P12- Rev AE - Product Coagulation ELITe MGB notice - rac20-2_elite_ingenius_-tab_p12-_rev_ae_-_product_coagulation_elite_mgb_notice.pdf
Ad agosto 2020permangano AP obsoleti presso i clienti attivi (vedere stato allegato). ATTENZIONE: questi dati non sono correllati ad un''analisi sull''utilizzo del prodotto da parte del cliente, potrebbero anche essere stati installati presso il cliente esclusivamente per attività di DEMO (vedere anche RAC20-16) - rac20-2_stato_aggiornamento_assay_protocol_ce-ivd_08-2020.docx
nota tecnita IT - rac20-2_2020-02-25_advisory_notice_coagulation_it.pdf
nota tecnicca EN - rac20-2_2020-02-25_avisory_notice_20-01_en_coagulation_kit.pdf
AP_04 - rac20-2_messa_in_suo_nuova_revisione_ap_coagulation_04.msg
AP_04__analizza solo FV e FII - rac20-2_messa_in_suo_nuova_revisione_ap_coagulation_04_analizza_solo_fv_e_fii.msg
AP_04 - rac20-2_messa_in_suo_nuova_revisione_ap_coagulation_04_1.msg
AP_04_analizza solo FV e FII - rac20-2_messa_in_suo_nuova_revisione_ap_coagulation_04_analizza_solo_fv_e_fii_1.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-2_non_significativa.pdf',NULL,N'04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020
04/02/2020',N'18/02/2020
18/02/2020
18/02/2020',N'02/03/2020
02/03/2020
01/04/2020
26/02/2020
01/04/2020
04/02/2020
28/02/2020
13/01/2021
28/02/2020',N'No',N'No in quanto il DMRI non contiene la revisione dell''assay protocol',N'Si',N'Si dovrà essere redatta una TAB per informare i clienti sul rilascio della nuova versione dell''AP',N'Si',N'Si in quanto la nuova versione dell''AP deve essere sostituita presso i clienti per evitare di incorrere in interpretazioni non corrette del genotipo del FV e dell''MTHFR ',N'La modifica delle impostazioni dell'' AP a fronte dell''esito positivo della ri-analisi dei campioni (clinici e simulati) testati durante la Fase di Verifica e di Validazione del prodotto non modifica il profilo di rischio del prodotto nè le sue prestazioni',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'Da valutare nell''ambito del riesame periodico del RA per quanto concerne la componente probabilità dell''evento',N'No',N'vedere MOD05,36. La notifica non è prevista per la modifica delle impostazioni dell''AP',N'No',N'Non applicabile in quanto non è un prodotto OEM',N'Lucia  Liofante',N'No',NULL,'2020-09-18 00:00:00',N'Verifica dell''efficacia della modifica introdotta attraverso sorveglianza post market e assenza di reclami inerenti errata.
Ad agosto 2020 permangano AP obsoleti presso i clienti attivi (vedere stato allegato). ATTENZIONE: questi dati non sono correlati ad un''analisi sull''utilizzo del prodotto da parte del cliente, potrebbero anche essere stati installati presso il cliente esclusivamente per attività di DEMO, inoltre non sono tracciate le attività a carico dei distributori (vedere RAC20-16 relativa alla gestione dell''aggiornamento degli AP presso il cliente e del monitoraggio di tale attività). Non sono presenti reclami con uguale causa primaria (r20-16 e r20-19 collegati alla rac20-10; r20-20 altra causa primaria)
Gennaio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-02-18 00:00:00','2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'18-21',N'Preventiva','2018-06-20 00:00:00',N'Stefania Brun',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Fino a fine 2018 per poter commercializzare in Canada era richiesto il certificato CAN/CSA Iso 13485:2003 dal 2019 il certificato non sarà più valido se non attraverso la sottomissione ad audit MDSAP',N'Per il dettaglio dei documenti QMS da aggiornare si veda piano azioni della Gantt',NULL,N'Positivo in quanto il superamento dell''audit MDSAP consentirà di rispondere ai nuovi requisiti relativi al certificato QMS per il Canada e quindi poter commercializzare i prodotti
Positivo in quanto il superamento dell''audit MDSAP consentirà di rispondere ai nuovi requisiti relativi al certificato QMS per il Canada e quindi poter commercializzare i prodotti
L''impatto in termini ore lavoro è gravoso come si evince dalla Gantt allegata.
L''impatto in termini ore lavoro è gravoso come si evince dalla Gantt allegata.',N'22/06/2018
22/06/2018
05/07/2018
05/07/2018',N'Richiesta quotation per audit MDSAP a Dekra
Valutare con Direzione in base al business attualmente presente in Canada e ad una valutazione costi-benefici se effettuare l''audit MDSAP entro il 2018 o nel primo trimestre 2019 
Per il dettaglio dei documenti QMS da aggiornare si veda piano azioni della Gantt
Dalla Gantt risulta che :
1.le procedure PR14,02 "Gestione dei reclami" e PR13,01 "Gestione delle non conformità" non descrivono in maniera esauriente la modalità impiegata per l''analisi dei dati e non contengono  il collegamento con la  procedura di analisi statistica PR24,01.
2. La procedura PR13,01 "Gestione delle non conformità" non prevede una classificazione in termini di gravità delle NC',N'DEKRA quotation MDSAP - med-qca-18-312_quotation_mdsap_elitechgroup_10oct2018.pdf
Accordo con DEKRA sulla pianificazione audit il week 37 2019 - rap18-21_written_letter_mdsap_audit.pdf
Gant chart - gantt_chart.xlsx
aggiornamento scheda rilavorazione con particolare attenzione a tempistiche e condizioni di rilavorazione che non devono introdurre ulteriori rischi. - rap18-21__messa_in_uso_mod0908_rev01_rap18-21.msg
messa in uso PR14,03 vigilanza rev03  e documenti collegati - rap18-21_messa_in_uso_pr1403_rev03_vigilanza_e_documenti_correlati_rap18-21_mod1802_del_3718.msg
PR10,01_02 - rap18-21_messa_in_uso_pr1001_e_allegato_rap18-21_cc18-21_cc12-17_rac18-26.msg
QM-EGSPA_REV06 - rap18-21_messa_in_uso_manuale_qualita_rev06.pdf
LR05,01_XXXrev02 - rap18-21_messa_in_uso_requisiti_essenziali_rap18-21.msg
PR05,02 rev03 - rap18-21_messa_in_uso_pr0502_03_rap18-21_rac17-30_mod1802_del_29072019.msg
PR05,05 rev04_MOD05,36 CTT2 - rap18-21_messa_in_uso_pr0505_04_e_mod0536_ctt2_rap18-19_rap18-21_1.msg
PR14,01_04, IO04,02_03 E ALLEGATO, IO14,07_01, IO14,08_01, IO14,09_00, IO14,10_00 - rap18-21_messa_in_uso_pr1403_rev03_vigilanza_e_documenti_correlati_rap18-21_mod1802_del_3718.msg
IO04,05-02 - rap18-21_messa_in_uso_io0405_02_rap18-21_rap18-19_archiviato_mod0405.msg
PR13,01_02 e PR14,01_02 in uso al 10/09/2019, MOD18,02 del 11/09/2019 - rap18-21_mess_in_uso_pr1401_02_e_pr1301_02rap18-21_rap18-19_rap17-34.msg
PR09,01_03 e documenti collegati, MOD18,02 del 3/9/19 - rac18-21_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20.msg
PR06,01 E ALLEGATI rev 02 - rap18-21_messa_in_uso_pr06,01_02_rap18-21.msg
ALL-2_PR08,01_05 - rap18-21_messa_in_uso_all-2_pr0801_05.msg
PR14,02_04_MOD18,02 del 12/09/19 - rap18-21_messa_in_uso_pr14,02_04_rap18-21_mod1802_120919.msg
PR22,01_02_MOD18,02 del 16/09/2019 - rap18-21_messa_in_uso_pr22,01_02_rap18-21_rap17-32_mod1802_del_160919.msg
PR10,02_07_MOD18,02 del 12/09/2019 - rap18-21_messa_in_suo_pr10,02_07_rap18-21_cc19-33_mod1802_del_12092019.msg
PR005,06_03_ALL-1_PR05,06_04_MOD18,02 del 10/09/19 - rap18-21_messa_in_uso_pr05,06_sorveglianza_regolatoria_rap1821_mod1802_100919.msg
PR24,01_01_MOD18,02 del 12/09/19 - rap18-21_messa_in_uso_pr24,01_01_rap18-21_mod1802_del_100919.msg
PR14,02_04 - rap18-21_messa_in_uso_pr14,02_04_rap18-21_mod1802_120919_1.msg
PR13_01_02 - rap18-21_messa_in_uso_pr14,01_02_e_pr13,01_02rap18-21_rap18-19_rap17-34.msg',NULL,N'20/06/2018
20/06/2018
05/07/2018',N'20/06/2018
18/06/2019',N'20/06/2018
21/12/2018
10/09/2019',N'No',N'nessun impatto sui DMRI',N'No',N'non necessaria comunicazione al cliente',N'No',N'nessun impatto sui prodotti immessi in commercio',N'Adeguamento del sistema gestion equalità ai regolamenti dei paesi MDSAP (vedere GANTT)',N'ALTRO - Altro',N'ALTRO - ALTRO',N'impattati i risjk assessment GM15 "VALUTAZIONE SOTTOMISSIONE AUTORITA'' COMPETENTE", valutazione impatto sul sottoprocesso P1.6" Etichttatura materiali e compoenti", PR14 "Rilavorazione Prodotto finito"',N'No',N'comunicazione al Canada per dichiarazione di DEKRA sulla pianificazione dell''audit MDSAP',N'No',N'non necessaria notifica',N'Stefania Brun',N'No',NULL,'2019-12-31 00:00:00',N'Audit MDSAP con esito positivo.
Rilasciato certificato DEKRA n°2235223 in data 31/01/2020, valido fino a 07/01/2023',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-06-18 00:00:00',NULL,'2020-02-14 00:00:00'),
    (N'20-45',N'Preventiva','2020-12-03 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Reclamo',N'20-79',N'RTK402ING - High Risk HPV ELITe Panel',N'Assay Protocol',N'Mancata impostazione del Cut off per il controllo interno negli assay protocol rilasciati per il prodotto RTK402ING, come previsto per tutti i prodotti ELITe',N'Definizione del valore di Cut off dell''IC per il prodotto RTK402ING High Risk HPV ELITe Panel e modifica dei relativi assay protocol ',NULL,N'Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività, l''introduzione del Cut off permette di definire l''idoneità del campione
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Impatto positivo poichè l''impostazione del valore di Cut off per il controllo interno permette di definire l'' idoneità del campione
Impatto positivo poichè l''impostazione del valore di Cut of',N'03/12/2020
03/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
03/12/2020
03/12/2020
10/12/2020
10/12/2020',N'Modifica degli assay protocol a seguito di verifica del valore di Cut off del Controllo Interno dopo riesame dei dati di validazione da parte del reparto validazioni
Revisionare IFU per modifica strumento amplificazione (da ABI 7500 Fast Dx a ABI 7500)
Riesame dei dati di validazione per definizione del valore di Cut off del CI
Controllo degli AP a modifica apportata
Aggiornamento IFU per modifica strumento amplificazione (da ABI 7500 Fast Dx a ABI 7500)
Messa in uso degli assay protocol modificati e delle IFU aggiornate
Notifica al fornitore con proposta di modifica degli AP
Aggiornare gli assay protocol modificati presso i clienti
Aggiornare TAB degli assay protocol Open e Closed per il prodotto RTK402ING',N'MESSA IN USO IFU RTK402ING REV01 E AP - racp20-45__ifu_high_risk_hpv_elite_mgb_panel.msg
MESSA IN USO IFU RTK402ING REV01 E AP - racp20-45__ifu_high_risk_hpv_elite_mgb_panel_3.msg
TAB - tab_eliteboard_documental_repository.msg
TAB P43 Rev. AA   - rap20-45_tab_p43_rev._aa.msg
MESSA IN USO IFU RTK402ING REV01 E AP - racp20-45__ifu_high_risk_hpv_elite_mgb_panel_1.msg
MESSA IN USO IFU RTK402ING REV01 E AP - racp20-45__ifu_high_risk_hpv_elite_mgb_panel_2.msg',NULL,N'03/12/2020
15/12/2020
15/12/2020
08/02/2021
03/12/2020
03/12/2020
10/12/2020
10/12/2020',N'15/12/2020
16/12/2020
18/12/2020
18/12/2020
18/12/2020',N'12/03/2021
12/03/2021
12/03/2021
12/03/2021
12/03/2021
12/03/2021
12/03/2021
15/03/2021',N'No',N'No in quanto il prodotto è fabbricato da Osang',N'No',N'Si ',N'No',N'nessun impatto',N'L''introduzione del Cut off permette di definire l''idoneità del campione, non apporta nè riduce i rischi legati alla sicurezza del prodotto.',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'valutare l''impatto sul RA AP1 a fine anno',N'No',N'vedere MOD05,36. La modifica di un AP non è da notificare ',N'No',N'No in quanto il prodotto non prevede un virtual manufacturer',N'Maria Galluzzo',N'Si',NULL,NULL,N'valutazione efficacia: assenza di reclami con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2020-12-18 00:00:00',NULL,NULL),
    (N'20-43',N'Correttiva','2020-11-19 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-106',N'Prodotti controllati con sistemi di Estrazione',N'Documentazione SGQ',N'Mancata valutazione delle aree d''impatto, in particolare per il CQ',N'La presente RAC richiede una correzione.
Si chiede l''inserimento in procedura di un documento dedicato alla dispensazione di volumi più piccoli sotto i 15mL. Inoltre dato che saranno disponibili due versioni di plasma e sangue uno con EDTA e uno con ACD (come fornito dalla banca del sangue), si chiede di identificare i nomi dei campioni con il tipo di anticoagulante. Inoltre si chiede di distinguere la fase del processo aggiungendo i codici che identificano la dispensazione (957) e la preparazione nel caso di aggiunta di EDTA (955). Si allega per chiarezza il file che riassume le modifiche richieste.

Per il sotto-processo GM5 "Verifica/individuazione degli impatti che la modifica ha sul processo/prodotto (CC - RAC/RAP) ed approvazione degli stessi" si prevede già una probabilità alta di mancata individuazione degli impatti, come emerso dall''analisi annuale 2019 del Validation Master Plan, mitigata da un''alta rilevabilità di questo evento, in quanto, come nel caso di questa RAC, l''integrazioni degli impatti è sempre individuata altrimenti non sarebbe possibile portare a termine il cambiamento. Si raccomanda, comunque, per la gestione dei change control, in particolare quelli che trattano l''esclusivo aggiornamento di un documento, l''individuazione di tutti i reparti utilizzatori con  la rispettiva area d''impatto. ',NULL,N'Impatto positivo: miglior distinzione del tipo di campione data la necessità di utilizzare plasma e sangue con EDTA o con ACD.  Infatti i nuovi prodotti HBV, HIV, HCV richiedono l''uso di plasma ACD mentre gli altri plasma o sangue con EDTA.
Nessun impatto solo modifica del nome
Impatto positivo: miglior distinzione del tipo di campione data la necessità di utilizzare plasma e sangue con EDTA o con ACD. 
La RAC richiede una correzione, ma  non si ritiene necessario intraprendere alcuna azione correttiva in quanto il risk assessment GM5  "Verifica/individuazione degli impatti che la modifica ha sul processo/prodotto (CC - RAC/RAP) ed approvazione degli stessi" già prevede una probabilità alta di accadimento di una mancata/scorretta valutazione degli impatti. Il sotto-processo è monitorato attraverso l''analisi annuale del VMP: incrementi di NC  imputabili al sotto-processo GM5 verranno trattate in taòle sede.
La RAC richiede una correzione, ma  non si ritiene necessario intraprendere alcuna azione ',N'09/12/2020
09/12/2020
09/12/2020
09/12/2020
09/12/2020
09/12/2020',N'Modifica della IO10,02
Nuovo modulo  di dispensazione
MOdifica MOD04-Emo-EDTA 
MOD04-Disp.Matrici
Modifica MOD10,13-XXX (utilizzo di plasma o sangue ACD o PLSMA)

approvazione modifica e Specifica
Modifica SPEC 950-192 e SPEC950-193 
messa in uso documenti
modifica del DMRI di HBV. ci sarebbe da aggiornare anche il DMRI di EXTR01, ma vista l''imminente dismissione (CC20-67) non si ritiene prioritario svolgerla.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
nessuna azione
approvazione modifica e Specifica',N'Modifiche - matrici_emo.docx
IO10,02_05 - rac20-43_messa_in_uso_io1002_05.msg
mod_dispensazioni_matrici - rac20-43_messa_in_uso_mod_dispensazioni_matrici.msg
spec950-132, spec950-193 rev02 - rac20-43_messa_in_uso_specificihe_hcv_e_specifiche.msg
mod_dispensazioni_matrici - rac20-43_messa_in_uso_mod_dispensazioni_matrici_1.msg
IO10,02_05 - rac20-43_messa_in_uso_io1002_05_1.msg
spec950-132, spec950-193 rev02 - rac20-43_messa_in_uso_specificihe_hcv_e_specifiche_1.msg
spec950-132, spec950-193 rev02 - rac20-43_messa_in_uso_specificihe_hcv_e_specifiche.msg
DMRI2020-003_RTK602ING_HBV_REV01 DEL 7-4-21 - rac20-43_dmri2020-003_rtk602ing_hbv_7-4-21.txt
la modifica non è significativa, pertanto non è da notificare. - rac20-43_mod05,36_01_non_significativa.pdf',NULL,N'09/12/2020
09/12/2020
09/12/2020
09/12/2020
26/11/2020
09/12/2020
09/12/2020',N'18/12/2020',N'24/02/2021
24/02/2021
09/12/2020
12/04/2021
09/12/2020
09/12/2020
24/02/2021',N'Si',N'modifica del DMRI di HBV.',N'No',N'non necessaria,  trattasi di una modifica interna',N'No',N'nessun impatto',N'Nessun impatto a livello regolatorio, si migliora l''identificazione dei campioni in associazione all''anticoagulante ',N'GM - Gestione delle modifiche',N'GM5 - Verifica/individuazione ed approvazione  degli impatti',N'monitorare a fine anno quanti eventi hanno avuto impatto sul RA GM5 per stabilire se necessario introdurre ulteriori controlli',N'No',N'vedere MOD05,35. Non necessaria in quanto trattasi di una modifica interna.',N'No',N'-',NULL,N'No',NULL,NULL,N'VALUTAZOINE EFFICACIA: verificare assenza nc con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2020-12-18 00:00:00','2021-04-12 00:00:00',NULL),
    (N'19-12',N'Correttiva','2019-04-19 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-41',N'RTS400ING',N'Documentazione SGQ',N'Il problema della curva con la doppia rampa e il falso negativo per il plasmide di CT è causato da un problema di interpretazione errata delle curve da parte del SW e non ha alcun impatto sui campioni clinici positivi per la Chlamydia trachomatis, in quanto in natura CT presenta sempre circa 9 copie di plasmide e 1 copia di cromosoma che sono amplificate insieme.
Il test di QC prevede l''amplificazione separata dei due target di CT (plasmide e cromosoma) per verificare l''efficienza delle singole PCR e in queste condizioni la reazione per il plasmide può generare curve di difficile interpretazione per il SW. R&D ha simulato la situazione della CT naturale nel test di QC (50.000 copie plasmide + 5.000 copie cromosoma) eseguendo 12 reazioni su 4 ELITe InGenius per un totale di 48 replicati: tutti i replicati presentano una curva che è la risultante della somma delle fluorescenze delle due PCR, ha una forma normale senza doppia rampa e non genera problemi con il software ELITe InGenius.
',N'Si chiede la modifica delle modalità di CQ al fine di verificare le prestazioni dei due target di CT (plasmide e cromosoma) insieme al fine di simulare il comportamento nei campioni reali.
Si chiede inoltre l''accettazione in deroga del lotto dal momento che la non conformità rilevata non inficia le prestazioni del prodotto in condizioni reali.',NULL,N'Le specifiche di CQ risulterebbero in linea con le specifiche del prodotto, non causando false non conformità
La modifica non impatta sulle prestazioni del prodotto.
Nessun impatto
La modifica non impatta sulle prestazioni del prodotto. Le specifiche di CQ risulterebbero in linea con le specifiche del prodotto, non causando false non conformità
La modifica non impatta sulle prestazioni del prodotto. Le specifiche di CQ risulterebbero in linea con le specifiche del prodotto, non causando false non conformità
La modifica non impatta sulle prestazioni del prodotto. Le specifiche di CQ risulterebbero in linea con le specifiche del prodotto, non causando false non conformità',N'19/04/2019
19/04/2019
30/04/2019
19/04/2019
19/04/2019
16/05/2019',N'Modifica dei moduli MOD10,12-RTS400ING e MOD10,13-RTS400ING.
Test su campioni clinici per l''accettazione in deroga del lotto e valutazione delle nuove modalità di CQ. (vedi Test allegati)
Confezionamento del lotto accettato inderoga
Report di accettazione in deroga del lotto U0419AT.
Messa in uso dei documenti di CQ.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'Test su campioni clinici per l''accettazione in concessione del lotto e valutazione delle nuove modalità di CQ - rac19-12_reportaccettazioneconcessione_u0419at_nc19-41.pdf
messa in uso moduli cq - rac19-12_messa_in_uso_moduli_cq_rts400ing_rac19-12.msg
MOD962-RTS400ING_U0419AT_19/04/19 - rac19-12_confezionamentolottoaccettatoinderoga_u0419at.pdf
messa in uso moduli - rac19-12_messa_in_uso_moduli_cq_rts400ing_rac19-12_1.msg
LOTTO U0419AT ccettato in concessione - rac19-12_reportaccettazioneconcessione_u0419at_nc19-41.pdf
La modifica è significativa, la notifica deve essere svolta esclusivamente alle autorità competenti che lo richiedono, laddove il prodotto è registrato. - rac19-12_mod05,36_significativa.pdf
Notifica non necessaria - rac19-12_notifica_non_necessaria.txt',NULL,N'19/04/2019
19/04/2019
30/04/2019
19/04/2019
16/05/2019
16/05/2019',N'19/04/2019',N'17/05/2019
29/04/2019
19/04/2019
29/04/2019
11/09/2019',N'No',N'nessun  impatto',N'No',N'La modifica non impatta sulle prestazioni del prodotto, la comunicazione non è necessaria',N'No',N'nessun impatto sul prodotto immesso in commercio',N'La modifica non impatta sulle prestazioni del prodotto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-05-29 00:00:00',N'Verificare l''assenza di NC almeno su 3 lotti.
Sui seguenti lotti non ci sono state NC al CQ: U0919BI, U1219BD, U0320-029.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-04-19 00:00:00','2019-09-11 00:00:00','2020-05-29 00:00:00'),
    (N'19-35',N'Preventiva','2019-11-11 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'La procedura di verifica dei dati dei controlli qualità con Ingenius non prevede la valutazione degli errori segnalati dallo strumento. Benchè QCT esegua normalmente la verifica questo può è essere tralasciato.',N'Si chiede di modificare il MOD10,15 "SCHEDA CONTROLLO QUALITA''" inserendo la verifica e registrazione degli errori da parte dello strumento in fase di verifica tecnica.',NULL,N'La richiesta prevede la registrazione di una verifica che viene già fatta in routine pertanto non impatta sulle attività di QCT, mentre ha impatto positivo per quanto riguarda la correttezza dell''analisi dei dati evitando errori di valutazione in caso di errori strumentali.
Nessun impatto sul processo.
Nessun impatto sul processo.
nessun impatto, si tratta di una modifica migliorativa sulla documentazione',N'11/11/2019
22/11/2019
22/11/2019
22/11/2019',N'Modifica del MOD10,15 rev02
messa in uso del mod10,15
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'MOD10,15_03 IN USO AL 4/12/19 - rap19-35_messa_in_uso_mod1015_03_rap19-35_mod1802_del_271119_1.msg
MOD10,15_03, MOD18,02 DEL 27/11/19 - rap19-35_messa_in_uso_mod1015_03_rap19-35_mod1802_del_271119.msg
 La modifica non è significativa, pertanto non è da notificare.  - rap19-35_mod05,36_non_significativa.pdf',NULL,N'11/11/2019
22/11/2019
22/11/2019',N'30/11/2019
20/12/2019',N'04/12/2019
04/12/2019
22/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto, si tratta di una modifica migliorativa sulla documentazione',N'CQ - Controllo Qualità prodotti',N'CQ14 - Fase postanalitica CQ: analisi e verifica tecnica dei risultati',N'VALUTARE',N'No',N'vedere MOD05,36',N'No',N'nessun impatto',N'Federica Farinazzo',N'No',NULL,'2020-06-01 00:00:00',N'verificare se sono presenti errori segnalati dallo strumento non individuati sul MOD10,15.
QCT esplicita sempre gli errori osservati durante la sessione di InGenius sul MOD10,15, quindi sono sempre esplicitati e non sono passati inosservati., si ritiene pertanto possibile chiudere l''azione correttiva con efficacia positiva.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-12-20 00:00:00','2019-12-04 00:00:00','2020-08-06 00:00:00'),
    (N'18-19',N'Preventiva','2018-06-18 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'L''aumento delle registrazioni in paesi extracee determina lo sviluppo di un processo adeguato per il controllo delle notifiche a seguito di modifiche significative del prodotto.',N'1) Individuare le modifiche significative e creare un modulo di registrazione;
2) individuare tutti i possibili input da cui può derivare una modifica di prodotto;
3) implementare il processo per la gestione delle notifiche dei cambiamenti significativi nei paesi extra UE e nelle aziende con cui è in corso un contratto OEM.
4) aggiornare la PR05,05 e valutare se necessario implementare le procedure, le istruzioni ed i moduli collegati;
5) a seguito di introduzione del nuovo processo valutarne l''efficacia ed aggiornare il risk assessment GM15 e Valutazione della sottomissione all''autorità competente / Organismo notificato, se necessario".',NULL,N'L''implementazione del processo per la gestione delle notifiche nei paesi extracee dei cambiamenti significativi migliorerà l''individuazione della significatività della modifica, migliorerà il fusso di informazioni per determinare se necessario o meno notificare nel paese d''interesse, ridurrà il rischio di una mancata notifica.
A livello di carico di ore lavoro l''impatto è significativo sia in fase iniziale, per la valutazione della notifica (per ciascun CC/RAC/RAP è necessario valutare se da notificare o meno attrverso la compilazione di un modulo aggiuntivo) sia in fase finale, per la notifica alle autorità/distributori e la successiva gestione delle richieste di documentazione che perverranno.
L''implementazione del processo per la gestione delle notifiche nei paesi extracee dei cambiamenti significativi migliorerà l''individuazione della significatività della modifica, migliorerà il fusso di informazioni per determinare se necessario o meno notificare nel paese d''interesse, ridurrà il rischio d',N'21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019',N'1) Come richiesto dal Remarks 7 dell''audit CEISO di 07/2018, individuare le modifiche significative avvalendosi del "MDSAP Companion document", della guida "NBOG''s Best Practice Guide" e della guida "Deciding When to Submit a 510(k) for a Change to an Existing Device" e del Notice of Change di DEKRA. 
4) implementare un processo per la gestione delle notifiche nei paesi extra EU dei cambiamenti significativi che preveda l''individuazione della modifica, la determinazione della significatività attraverso la "Matrice dei cambiamenti significativi" (chi lo determina), la verifica dei paesi in cui è necessario effettuare la notifica, l''effettuazione della notifica (chi e come effettua la notifica). Aggiornare la PR05,05 rev.03.  (SGQ)
Includere nella procedura e nei documenti correlati anche la gestione delle notifiche alle aziende OEM (es. R-BIO). (SGQ) 
Riportare in un''istruzione operativa la parte descrittiva dei change control.
2) creare un elenco/questionario dei cambiamenti significativi ed una matri',N'draft matrice cambiamenti significativi  - rap18-19_mod05,36_draft_00_matrice_dei_cambiamenti_significativi.xlsx
pr05,05_04 - rap18-19_messa_in_uso_pr0505_04_e_mod0536_ctt2_rap18-19_rap18-21.msg
PR14,01_02 in uso al 10/9/2019, con il riferimento alle nuove modalità di notifica - rap18-19_mess_in_uso_pr1401_02_e_pr1301_02rap18-21_rap18-19_rap17-34.msg
IO04,05_02 IN USO AL 06/08/2019 - rap18-19_messa_in_uso_io0405_02_rap18-21_rap18-19_archiviato_mod0405.msg
diagramma di flusso gestione notifiche cambiamenti - rap18-19_diagrammadiflusso_processogestione_notificacambiamenti.png
draft matrice cambiamenti - rap18-19_mod05,36_draft_00_matrice_dei_cambiamenti_significativi.xlsx
Matrice cambiamenti significativi e gestione cambiamenti oem in uso  - rap18-19_messa_in_uso_mod0536_e_mod0537_rev00_rap18-19.msg
Diagramma di flusso - rap18-19_diagramma_flusso_allegato.png
MESSI IN USO MOD04,15_04 e MOD04,18_04  - rac18-19_messa_in_uso_mod04xx_e_mod0536_00-ctt1_rap18-19_1.msg
LINKOMM versione 2.0 - rap18-19_linkomm_versione_2.0_in_uso.msg
MOD18,02 del 1/08/2019 - rap18-19_messa_in_uso_pr0505_04_e_mod0536_ctt2_rap18-19_rap18-21.msg
MESSI IN USO MOD04,15_04 e MOD04,18_04 - rac18-19_messa_in_uso_mod04xx_e_mod0536_00-ctt1_rap18-19.msg
MESSI IN USO MOD04,25_02 e MOD04,26_02  - rac18-19_messa_in_uso_mod04xx_e_mod0536_00-ctt1_rap18-19_2.msg
Messa in uso liste di riscontro - rac18-19_messa_in_uso_mod04xx_e_mod0536_00-ctt1_rap18-19_3.msg
PR04,01_03_PR04,04_02 - rap18-19_messa_in_uso_pr0401_03_pr0404_02_rac1819_rac19-28_1.msg
PR04,01_03_PR04,04_02 - rap18-19_messa_in_uso_pr0401_03_pr0404_02_rac1819_rac19-28.msg
Valutazione significatività notifica CC/RAC fino a 03/2019, NESSUNA modifica significativa - rap18-19_mod05,36compilati_nessunamodifica_significativaccda1a18_racda1a5_del_2019.pdf',NULL,N'21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019
21/01/2019',N'25/02/2019
31/05/2019
19/03/2019
29/04/2019
29/04/2019
29/04/2019
29/04/2019
06/07/2018
06/07/2018
01/10/2018
06/07/2018
24/04/2019
20/12/2019
29/04/2019
20/12/2019
29/04/2019',N'25/02/2019
30/07/2019
21/03/2019
25/07/2019
01/08/2019
30/03/2020
10/04/2019
17/05/2019
30/03/2020
17/05/2019
30/03/2020
17/05/2019',N'No',N'nessun impatto sui DMRI di prodotto, in quanto la PR05,05 non è nominata',N'No',N'non è necessario effettuare comunicazione al cliente',N'No',N'l amodifica è di sistema, non ha impatto sui prodotti immessi in commercio',N'Implementazione dei requisiti regolatori in accordo con il documento "MDSAP Companion Document, 2017-01-06 MDSAP AU G0002.1.004"',N'GM - Gestione delle modifiche',N'GM15 - Valutazione  notifica Autorità Competente / Organismo Notificato',N'la modifica ha impatto sul risk assessment GM15 ',N'No',N'La PR05,05 è inviata all''organismo notificato DEKRA, in vista dell''audit MDSAP di 09/2019',N'No',N'R-BioPharma è stato informato',N'Roberta  Paviolo',N'No',NULL,'2019-12-31 00:00:00',N'Verificare che tutti i CC/RAC-RAP estensioni d''uso e manutenzioni prodotto gestiti nel corso del 2019 siano stati valutati per significatività e sia stata inviata notifica nei paesi MDSAP.

Tutte le RAC/RAP/CC del 2019 hanno associato il modulo "Matrice dei cambiamenti significativi", MOD05,35, nei quali si valuta la significatività della modifica. Tutte le modifiche significative sono state notificate alle autorità competenti dei paesi MDSAP, laddove il prodotto era registrato, a modifica avvenuta. 
DEKRA, a 09/2019, assegna la NC "AR14/Mi-01" relativa all''implementazione dei cambiamenti sifìgnificativi senza l''approvazione dell''Organismo Notificato, cui si notificavano le modifiche ad attività terminate. Questa NC ha portato all''apertura di una richiesta di azione correttiva (RAC19-28) che prevede la comunicazione preliminare dei cambiamenti significativi all''Organismo Notificato, che li approva. Questo processo è stato applicato anche alle notifiche alle autorità competenti dei paesi MDSAP. La valutazione dell''efficacia è quindi da verificare nella RAC19-28.',N'Roberta  Paviolo',N'Negativo',N'La valutazione dell''efficacia è da verificare nella RAC19-28.',N'31/03/2020',NULL,'2019-12-20 00:00:00','2020-03-30 00:00:00','2020-03-30 00:00:00'),
    (N'20-11',N'Correttiva','2020-03-25 00:00:00',N'Federica Farinazzo',N'Validazioni',N'Non conformità',N'19-125',N'RTSD14-M MTHFR A1298C Q - PCR Alert kit in associazione a  7300 Real-Time PCR system',N'Manuale di istruzioni per l''uso
Uso Previsto',N'La causa della non conformità riscontrata è da riferirsi alla variabilità strumentale dipendente dalle modifiche apportate allo strumento 7300 Real-Time PCR system  durante l''intervento tecnico del fornitore  per la manutenzione ordinaria. In particolare la sostituzione della filter wheel "sporca", in associazione a nuovi lotti di calibrazione ABI  hanno causato la modifica del delta Ct tra FAM e VIC. Il fornitore non ha requisiti di massima fluorescenza pertanto dal loro punto di vista lo strumento ora ha una performance migliore avendo una filter wheel pulita e non opacizzata. La verifica puntuale in manutenzione della filter wheel è stata implementata dal fornitore solo recentemente e questo può spiegare perchè non sia stato visto questo mal funzionamento dello strumento nella manutenzione precedente.
Lo strumento in oggetto è in uso al CQ dal 2017 pertanto non è stata intercettata la deriva collegata ai lotti di calibrazione di ABI. Si esclude una deriva del prodotto, in quanto sono stati testati tutti i lotti disponibili come controcampioni con esiti sovrapponibili e perchè l''aumento di delta Ct è stato visto anche con gli altri prodotti di determinazione allelica. In base alle indagini eseguite si è visto però che tale modifica delle performance crea problemi di fuori specifica solo con il prodotto RTSD14, in quanto presenta di base un delta Ct più ampio rispetto agli altri prodotti. Questo spiega inoltre perché l''unico cliente ad aver avuto lo stesso problema evidenziato in CQ è l''unico che utilizza il prodotto in associazione con lo strumento 7300 RealTimePCRsystem.
Dal momento che il prodotto è in dismissione, e vi è un solo cliente coinvolto, è più efficace gestire questo cliente (vedi reclamo 19-65) e modificare l''uso previsto del prodotto eliminando l''utilizzo con lo strumento 7300 Real-Time PCR system  e limitandolo all''uso con lo strumento 7500 FAST dx Real-Time PCR Instrument. Valutare se possibile un''eventuale dismissione più rapida del prodotto.',N'Questa RAC (RAC20-11) NON è APPROVATA, in quanto la dismissione della linea di prodotti ALERT è stata anticipata al 30/06/2020 rispetto al 31/12/2020 pianificato inizialmente. Non si ritiene, pertanto, necessario apportare modifiche al prodotto RTSD14 oggetto dell''azione correttiva.
TUTTI GLI IMPATTI VALUTATI INIZIALMENTE per le aree coinvolte, e descritti di seguito, NON SONO APPROVATI, ad esclusione dell''impatto sul REGOLATORIO CHE SANCISCE LA MANCATA APPROVAZIONE DI QUESTA RAC e la gestione della dismissione della linea di prodotti ALERT nel CC20-23.
In particolare il prodotto RTSD14 e CTRD sarà sostituito con i kit "EER040032 Duplica RealTimeMix & Match MTHFR A1298C" di Euroclone/Clonit, prodotti validati sullo strumento 7500 FAST dx Real-Time PCR Instrument, e compatibili con lo strumento 7300 RealTimePCRsystem.',NULL,N'Il prodotto RTSD14 verrà dismesso entro fine anno, attualmente è da gestire un unico cliente che usa il prodotto in associazione allo strumento 7300 RealTimePCRsystem (vedi reclamo 19-65).
La modifica non ha impatto sul processo ma in termini di attività
La modifica deve essere recepita nelle modalità di controllo del prodotto
Attualmente vi è un solo cliente utilizzatore del prodotto RTSD14 in associazione a 7300 (vedi reclamo 19-65) resta da valutare  l''impatto della modifica in modo generale sui clienti nuovi e sul materiale di marketing.
Questa RAC (RAC20-11) NON è APPROVATA, in quanto la dismissione della linea di prodotti ALERT è stata anticipata al 30/06/2020 rispetto al 31/12/2020 pianificato inizialmente. Pertanto, non si ritiene necessario apportare modifiche al prodotto RTSD14 oggetto dell''azione correttiva. La dismissione della linea di prodotti ALERT è gestita nel CC20-23, in particolare l''RTSD14 e il CTRD14 saranno sostituiti con i kit  EER040032 Duplica RealTimeMix & Match MTHFR',N'21/04/2020',N'Gestione dell''unico cliente utilizzatore del prodotto RTSD14 in associazione a 7300 (vedi reclamo 19-65).
Sostituzione e installazione strumento 7300 RealTimePCRsystem con lo strumento 7500 fast dx per il cliente utilizzatore.
Modifica dei documenti per il controllo qualità.
-Valutazione degli impatti della modifica

vedere CC20-23
Aggiornamento IFU
Aggiornamento IFU
Valutazione degli impatti
Valutazione impatti su gare 
Valutare se possibile una dismissione in tempi più ristretti del prodotto RTSD14.',NULL,NULL,N'21/04/2020',N'21/04/2020',N'21/04/2020
21/04/2020
21/04/2020
21/04/2020
21/04/2020
21/04/2020
21/04/2020',N'No',N'nessun impatto',N'No',N'comunicazione al cliente attraverso avvertenza posta sul IFU',N'No',N'nessun impatto',N'-',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'la dismissione dei prodotti si conclude con l''archiviazione del FTP, non c''è impatto sul risk assessment DC12.',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-04-21 00:00:00',N'RAC non approvata, vedere CC20-23.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-04-21 00:00:00','2020-04-21 00:00:00','2020-04-21 00:00:00'),
    (N'20-28',N'Preventiva','2020-09-08 00:00:00',N'Renata  Catalano',N'Regolatorio',NULL,NULL,NULL,N'Documentazione SGQ',N'Processo di gestione delle modifiche del prodotto ELITe InGenius e prodotti correlati non pienamente integrato nel SGQ di EG SpA',N'Al fine di prevenire NC riguardanti mancate analisi di significatività di cambiamenti segnalati da PSS si chiede di:
- definire un flusso per cui per ciascun ECN/ECR inviato da PSS relativamente allo strumento ELITe InGenius o ai prodotti correlati venga valutata la necessità di recepimento all''interno del SGQ di EG SpA e la significatività della modifica (con relativa notifica preliminare alle Autorità Competenti);
- prevedere, nell''ambito della valutazione della significatività, la valutazione di un''eventuale nuova sottomissione 510k relativa al cambiamento;
- valutare, in vista dell''imminente commercializzazione negli USA del prodotto INT030-K, l''eventuale necessità di nuova sottomissione 510k relativamente alle modifiche avvenute dalla data di registrazione del prodotto all''FDA ad oggi',NULL,N'Possibile rallentamento nell''approvazione e implementazione delle modifiche dovendo valutare la necessità di recepimento all''interno del SGQ di EG SpA  e collaborare alla valutazione circa una nuova sottomissione 510k
Impatto positivo:
- più corretta gestione dei cambiamenti, relativa valutazione della significatività, notifica alle Autorità Competenti e nuova sottomissione 510k del prodotto ELITe InGenius e dei prodotti correlati. 
- Fascicolo Tecnico del prodotto ELITe InGenius e dei prodotti correlati maggiormente sotto controllo.
- commercializzazione del prodotto INT030-K in USA avendo valutato la necessità di nuova sottomissione 510k relativamente alle modifiche avvenute dalla data di registrazione del prodotto all''FDA ad oggi
Maggior impegno in termini di tempo rispetto ad oggi per valutare per ciascun ECN/ECR inviato da PSS relativamente allo strumento ELITe InGenius o ai prodotti correlati la necessità di recepimento all''interno del SGQ, per la revisione di RAC/RAP/CC risultanti, la gesti',N'22/10/2020
22/10/2020
22/10/2020
22/10/2020',N'Collaborare alla:
- definizione di un flusso per cui per ciascun ECN/ECR inviato da PSS relativamente allo strumento ELITe InGenius o ai prodotti correlati venga valutata la necessità di recepimento all''interno del SGQ di EG SpA 
- valutazione dell''eventuale necessità di nuova sottomissione 510k relativamente alle modifiche avvenute dalla data di registrazione del prodotto all''FDA ad oggi
Verificare quanto attualmente riportato nel Quality Agreement, nel template di ECN/ECR e nei documenti SGQ di EG SpA per eventualmente proporre una nuova classificazione ed un nuovo iter di apertura, condivisione e approvazione di ECN/ECR. 
Definire quali categorie di ECN/ECR vengono convogliate negli strumenti SGQ di EG SpA e come garantire le notifiche e le corrette valutazioni 510k.
Definire le responsabilità di ciascuno step del processo ed aggiornare la documentazione che lo richiede, allineandola e rendendola coerente (Quality Agreement, template ECN/ECR, modulo significatività,…)
Verificare quanto attual',NULL,NULL,N'22/10/2020
22/10/2020
22/10/2020
22/10/2020',N'21/12/2020
21/12/2020
21/12/2020',NULL,N'No',N'Essendo la RAP relativa al processo di gestione delle modifiche e non al processo produttivo non c''è alcun impatto sul DMRI dei prodotti InGenius',N'No',N'Non necessaria',N'No',N'Nessun impatto',N'TBD',N'GM - Gestione delle modifiche',N'GM5 - Verifica/individuazione ed approvazione  degli impatti',N'da valutare l''impatto sul RA GM5 al termine dellla vlautazione delle attività ed in mbase alle, eventuali, modifiche di processo ',N'No',N'DA fare MOD05,36',N'No',N'La RAP non coinvolge alcun prodotto del MOD05,37',N'Renata  Catalano',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-21 00:00:00',NULL,NULL),
    (N'21-2',N'Correttiva','2021-01-15 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-130',N'RTS170ING',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'La NC20-130 è stata causata da una concomitanza di fattori. In primo luogo, si è notato che l''STR13 ha un ritardo di 0.5 Ct sul Ch3 nel campione PC1:100 rispetto agli altri strumenti. Questo leggero ritardo si somma alle basse fluorescenze date dagli oligo Eurogentec utilizzati per la preparazione del lotto. Inoltre, questo lotto U1220-057 è stato preparato con gli stessi materiali di altri due lotti (U1220-053 e U1220-055) che hanno esiti conformi, anche se con scarse prestazioni.

Dal momento che la verifica del Ct dell''IC diluito è solo funzionale all''accertamento della corretta esecuzione del QC, mentre il test sull''IC e sull''efficienza di RT è quello effettuato con i 4 ng di total human RNA singolarmente, è possibile accettare in deroga il lotto e richiedere la modifica dei criteri di CQ per il campione IC diluito, diminuendo la stringenza ma verificando la preparazione della diluzione.
Tale valutazione non è stata fatta in sede di DT in quanto lo sviluppo e il rilascio del prodotto sono stati eseguiti in tempi molto brevi a causa della pandemia di covid19',N'SI chiede di modificare il cut-off del Ct per il Controllo Interno del campione PC diluito a 10 copie ad esempio utilizzando un limite di ct inferiore a 38. In base all''accettazione di questa modifica è possibile accettare in concessione il lotto U1220-057 oggetto della NC20-130 in funzione del nuovo criterio CQ stabilito',NULL,N'Il nuovo criterio non influisce sulla valutazione delle performance dell''IC e dell''enzima di trascrizione inversa.
Il nuovo criterio non influisce sulla valutazione delle performance dell''IC e dell''enzima di trascrizione inversa. Nessun impatto sul processo
Il nuovo criterio non influisce sulla valutazione delle performance dell''IC e dell''enzima di trascrizione inversa.
nessun impatto
Il nuovo criterio non influisce sulla valutazione delle performance dell''IC e dell''enzima di trascrizione inversa.',N'15/01/2021
15/01/2021
15/01/2021
15/01/2021',N'Modifica del MOD10,12-RTS170ING
formazione
Approvazione dell''accettazione in deroga del lotto U1220-057
Approvazione dell''accettazione in deroga del lotto U1220-057
Redazione del report e approvazione dell''accettazione in deroga del lotto U1220-057
messa in uso dei documenti
Approvazione dell''accettazione in deroga del lotto
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  ',N'messa in uso MOD10,13-RTS170ING REV03 E MOD10,12-RTS170ING REV04 - rac21-2_messa_in_uso_mod10-xx-rts170ing_cc20-77_nc21-8_rac21-2.msg
REPORT ACCETTAZIONE IN CONCESSIONE DEL LOTTO U1220-057 - rac21-2_rep2021-001_lottou1220-057_sars-cov-2.pdf
REPORT ACCETTAZIONE IN CONCESSIONE DEL LOTTO U1220-057 - rac21-2_rep2021-001_lottou1220-057_sars-cov-2_1.pdf
REPORT ACCETTAZIONE IN CONCESSIONE DEL LOTTO U1220-057 - rac21-2_rep2021-001_lottou1220-057_sars-cov-2_2.pdf
notifica preliminare effettuata - rac21-2_prior_notification_of_change_-rts170ing__rts170ing-96-.msg
notifica a modifica effettuata - rac21-2_implemented_prior_notification_of_change_-rts170ing__rts170ing-96-.msg
la modifica è significativa, pertanto è da notificare ai paesi presso cui il prodotto è registrato ed a DEKRA - rap21-2_mod05,36_significativa.pdf',NULL,N'15/01/2021
15/01/2021
15/01/2021
15/01/2021
15/01/2021',N'22/01/2021',N'11/02/2021
11/02/2021
11/02/2021
18/01/2021
15/03/2021',N'No',N'nessun impatto sul dmri del prodotto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Nessun impatto sui requisiti regolatori: Il nuovo criterio non influisce sulla valutazione delle performance dell''IC e dell''enzima di trascrizione inversa.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'in valutazione',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-09-30 00:00:00',N'Verifica assenza di NC con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2021-01-22 00:00:00','2021-03-15 00:00:00',NULL),
    (N'20-44',N'Preventiva','2020-11-24 00:00:00',N'Stefania Brun',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di produzione',N'Il prodotto SARS COV2 ha avuto un incremento notevole in termini di numeri di lotti da produrre. Al fine di allineare la documentazione di produzione e la tracciabilità dei lotti di Controllo Qualità si chiede la modifica dei moduli di produzione. 

In particolare non è più eseguita la preparazione del preCQ inteso come test di CQ eseguito su un piccolo lotto NON DI VENDITA, mirato alla valutazione delle materie prime.
Nel caso del prodotto in oggetto il lotto minimo di produzione è di 1600 tubi di PCR MIX, che in caso di non conformità di materiali dovrebbero essere eliminati; la procedura di preCQ permetteva di evitare la produzione di un lotto non conforme.  Questa prassi deve poter essere rintracciata a livello dei documenti con i relativi riferimenti ai lotti controllati. 

Inoltre al fine di migliorare l''efficienza di produzione del prodotto si valuta la possibilità di abbinare dei lotti di PCR MIX e di Rt Enzyme Mix anche se controllati in test di CQ distinti. Questa modifica può essere implementata solo per RTS170ING in quanto il prodotto RTS170ING-PM non ha ancora una variabilità sufficiente di lotti. ',N'Migliorare la tracciabilità dei lotti di controllo qualità riferiti ai vari semilavorati nei moduli di produzione RTS170ING e RTS170ING-PM.
Prevedere la possibilità di confezionare lotti di prodotto a partire da semilavorati controllati separatamente ( SOLO  per RTS170ING).',NULL,N'Impatto positivo: i documenti permetteranno di tracciare in modo efficace i lotti nei quali sono stati testati i vari semilavorati/reagenti.
Impatto positivo: i documenti permetteranno di tracciare in modo efficace i lotti nei quali sono stati testati i vari semilavorati/reagenti al fine delle indaigni di non conformità.
Impatto positivo: i documenti permetteranno di tracciare in modo efficace i lotti nei quali sono stati testati i vari semilavorati/reagenti al fine delle indaigni di non conformità.
A livello regolatorio non c''è nessun impatto tutti i componenti utilizzati sono sempre controllati. 
A livello regolatorio non c''è nessun impatto tutti i componenti utilizzati sono sempre controllati. 
Per il prodotto SARS (RTS170ING) valutare la possibilità di accoppiare nel confezionamento finale lotti di PCR MIX e di Rt Enzyme Mix controllati in associazione a lotti differenti di componenti. Dall''esito dei test dovrà emergere che tale accoppiamento non influenza le performance del prodotto.',N'05/02/2021
04/02/2021
04/02/2021
04/02/2021
04/02/2021
04/02/2021',N'verifica ed approvazione moduli
Modificare moduli di produzione del prodotto RTS170ING eliminando la registrazione del Pre-CQ ed includendo la registrazione dei lotti di CQ nei quali sono stati testati i vari semilavorati. Modificare le modalità di preparazione e le indicazioni riportate.
Aggiornamento dei moduli di CQ.
Solo per RTS170ING modifica delle modalità di confezionamento prevedendo l''abbinamento di semilavorati testati separatamente.
Simulazione
Formazione
Esecuzione test di riproducibilità al fine di verificare l apossibilità di eseguire confezionametni a partire da componenti controlati separatamente. Stesura report.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
messa in uso dei documenti
verifica esito test ed approvazione report',N'messa in uso moduli 170ing - rap20-44_messa_in_uso_mod_prod_promega_cc20-77_rap20-44_scp2020-015.msg
aggiornamento dei moduli di produzione per i prodotti sars - rap20-44_messa_in_uso_modli_prod_sars.msg
aggiornamento dei moduli di produzione per i prodotti sars - rap20-44_messa_in_uso_modli_prod_sars_1.msg
la modifica non è significativa, pertanto non è da notificare. - rap20-44_mod05,36__non_significativa.pdf
messa in uso moduli 170ing - rap20-44_messa_in_uso_mod_prod_promega_cc20-77_rap20-44_scp2020-015_1.msg
aggiornamento dei moduli di produzione per i prodotti sars - rap20-44_messa_in_uso_modli_prod_sars_2.msg
Report RTS170ING, RAP20-44 - rap20-44_rep2021-004_1.pdf
Report RTS170ING, RAP20-44 - rap20-44_rep2021-004.pdf',NULL,N'05/02/2021
04/02/2021
09/02/2021
04/02/2021
04/02/2021
04/02/2021',N'22/02/2021
01/02/2021',N'28/04/2021
28/04/2021
04/02/2021
17/02/2021
28/04/2021
04/02/2021',N'No',N'No in quanto il cambio di revisione dei documenti di produzione non impatta sul DMRI del prodotto',N'No',N'No in quanto è una modifica che non impatta sul CQ del lotto',N'No',N'No in quanto è una modifica che non impatta sul CQ del lotto',N'Nessun impatto: al CQ tutti i componenti sono controllati.',N'PR - Preparazione Lotto Unico',N'PR1 - pianificazione',N'nessun impatto sul RA PR1, le modalità differenziate per il pre-cq sono in essere solo per il kit SARS coV2 la cui elevata produzione non impatta sui rischi in termini di costi',N'No',N'vedere MOD05,36. ',N'No',N'Nessun virtual manufacturer',N'Ferdinando Fiorini',N'No',N'-','2021-10-30 00:00:00',N'Verificare il funzionamento del flusso produttivo del prodotto sars',NULL,NULL,NULL,NULL,NULL,'2021-02-22 00:00:00','2021-04-28 00:00:00',NULL),
    (N'20-41',N'Preventiva','2020-11-04 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'I componenti del prodotto sono acquistati da un fornitore qualificato ma è possibile che la variabilità evidenziata sia intrinseca al processo di produzione del fornitore e al processo di estrazione eseguito per il CQ.
Sono stati eseguiti dei test al fine di escludere eventuali cause interne: campione di riferimento degradato (1st standard WHO del CMV) ed errori nelle modalità di esecuzione dei test  (vedi "Report REP2020-178_RAP20-41_INT021EX_00").
Dallo studio della variabilità fatta con il monitoraggio dei risultati di CQ si conclude che il criteri di CQ dell''efficienza di estrazione deve essere portato a "quantità attesa - 0,4 Log".',N'Si chiede la modifica dei criteri di CQ al fine di prevedere la variabilità attesa quando vi è uno step di estrazione oltre a quello di amplificazione dei campioni.',NULL,N'La modifica è migliorativa per il CQ del prodotto in quanto evita di eseguire ripetizioni o di emettere esiti non conformi per lotti le cui prestazioni sono invece accettabili (entro 0,5 Log della quantità attesa).
Nessun impatto
Nessun impatto
Il CQ di un sistema di estrazione deve prevedere una maggiore variabilità rispetto al CQ di singoli sistemi di amplificazione, questo rende accettabile un limite di accettazione di CQ più ampio, come da richiesta.
Il CQ di un sistema di estrazione deve prevedere una maggiore variabilità rispetto al CQ di singoli sistemi di amplificazione, questo rende accettabile un limite di accettazione di CQ più ampio, come da richiesta.
Il CQ di un sistema di estrazione deve prevedere una maggiore variabilità rispetto al CQ di singoli sistemi di amplificazione, questo rende accettabile un limite di accettazione di CQ più ampio, come da richiesta.
Come si evidenzia nel Report REP2020-178_RAP20-41_INT021EX_00 i dati ottenuti dai test mettono in luce una variabilit',N'20/01/2021
19/01/2021
23/11/2020
23/11/2020
23/11/2020
23/11/2020',N'Modifica dei criteri inseriti nel MOD10,12-INT021EX e modifica del MOD10,13-INT021EX
Simulazione dei documenti
Formazione.
Revisione dei nuovi criteri di accettazione del controllo di qualità
Revisione dei nuovi criteri di accettazione del controllo di qualità
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
In caso di ricalcolo dei Fc dei prodotti Parvo B19, EBV e CMV, occorrerà aggiornare i relativi Fascicoli Tecnici di Prodotto per inserire eventuale reportistica prodotta
In caso di ricalcolo dei Fc dei prodotti Parvo B19, EBV e CMV, occorrerà aggiornare e rilasciare le IFU dei prodotti
Revisione ed approvazione dei nuovi criteri di CQ',N'FEP2020-178 firmato il 10/12/2020 - rep2020-178_rap20-41_int021ex_00_firmato_il_10-12-2020.docx
MOD10,13-INT021EX MOD10,12-INT021EX in uso - rap20-41_messa_in_uso_mod10xx-int021ex.msg
La modifica dei criteri di CQ sono una modifica significativa tuttavia, essendo documentazione confidenziale, non si ritiene necessario effettuare la notifica in quanto in Macedonia e India, autorità competenti presso cui il prodotto è stato registrato, non sono stati forniti i moduli di CQ. 										 										 - rac20-41_mod05,36_01_significativa.xlsx
MOD10,13-INT021EX MOD10,12-INT021EX in uso - rap20-41_messa_in_uso_mod10xx-int021ex_1.msg',NULL,N'04/11/2020
20/01/2021
20/01/2021
23/11/2020
23/11/2020
23/11/2020',N'23/12/2020',N'21/12/2020
21/12/2020
12/12/2020',N'No',N'La modifica dei criteri di CQ effettuata su un modulo già esistente non richiede l''aggiornamento del DMRI di prodotto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'Nessun impatto sui requisiti regolatori',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'da valutare',N'Si',N'vedere MOD05,36. La modifica dei criteri di CQ sono una modifica significativa tuttavia, essendo documentazione confidenziale, non si ritiene necessario effettuare la notifica in quanto in Macedonia e India, autorità competenti presso cui il prodotto è stato registrato, non sono stati forniti i moduli di CQ. ',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-06-30 00:00:00',N'Valutare l''esito del primo lotto testato con i nuovi criteri (lotto McNagel 2011-001)',NULL,NULL,NULL,NULL,NULL,'2020-12-23 00:00:00','2020-12-21 00:00:00',NULL),
    (N'20-1',N'Correttiva','2020-01-23 00:00:00',N'Roberta  Paviolo',N'Acquisti',N'Non conformità',N'19-56',N'NC18-26 (952-PINV 16A - pINV 16A), NC19-34 (952-pBetaglobina, 952-pCMV-MIEA, 952-pEnterovirus), NC19-45 (952-PBETAGLOBINA - pBETAGLOBINA ), NC19-56 (952-PRECA - pRECA)',N'Componenti principali
Documentazione SGQ',N'LIFE Technologies Italia non soddisfa sempre i requisiti richiesti da EGSpA  è pertanto indispensabile potersi approvvigionare da un secondo fonritore: "Gene Script", che fornisce il materiale con le stesse caratteristiche rispetto agli attuali codici utilizzati e presenta tempistiche di consegna più celeri.',N'Effettuare un controllo qualità su uno standard prodotto con la prima madre plasmidica pHSV2 in pGEM (100 µg in 100 µL di TE) fornita da GeneScript.
Aggiornare tutte le specifiche di prodotto aggiungendo il secondo fornitore.',NULL,N'Impatto positivo in quanto non solo la materia prima avrà due fornitori distinti da cui possibile approvvigionarsi, ma il nuovo fornitore "Gene Script" ha tempi più celeri di spedizione rispetto a LIFE Technologies Italia .
Impatto positivo in quanto è presente un doppio fornitore di materia prima. 
Impatto positivo in quanto è presente un doppio fornitore di materia prima. 
Impatto positivo, l''introduzione di un secondo fornitore permette di ridurre il rischio di una mancata fornitura di una materia prima. 
L''introduzione di un secondo fornitore non ha impatto sulla documentazione di gestione dei rischi: il cambio di fornitore di plasmidi non modifica gli effetti del reagenti sulle persone e sull''ambiente né le performance di prodotto. Inoltre, il rischio è già considerato nella documentazione attuale ed è mitigato attraverso le seguenti azioni: riportare le informazioni relative al fornitore e alla materia prima fornita nella Specifica, effettuare il controllo al ricevimento della materia pri',N'06/02/2020
06/02/2020
31/01/2020
06/02/2020
06/02/2020
06/02/2020',N'Acquisto dei plasmidi dal nuovo fornitore
Effettuare un CQ su uno standard di HSV2 prodotto con la madre plasmidica pHSV2 in pGEM (100 µg in 100 µL di TE) fornita da GeneScript.
produrre un lotto di Standard (STD) con la madre plasmidica pHSV2 in pGEM (100 µg in 100 µL di TE) fornita da GeneScript, da inviare al controllo qualità
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Il primo gruppo di specfiche da modificare sarà il seguente:
SPEC952-pFIIWT, 
SPEC952-pMTHFRMU, 
SPEC952-pMTHFR-1298AWT, 
SPEC952-FV-4070AWT, 
SPEC952-pHHV6. 
Tutte le altre specifiche verranno aggiornate come da pianificazione allegata.
effettuare formazione.
messa in uso delle specifiche',N'LOTTO TESTATO Q0120AB PROTOTIPO STD032PLD CON PLASMIDE GENESCRIPT - rac20-1_produzione_e_cq_madre_plasmidica_phsv2_in_pgem_per_test.docx
Aggiornamento SPE952-xxx cre - rac20-1_messa_in_uso_spec952-xx_cre_cc19-37_rac20-1.msg
08/2020: piano aggiornamento specifiche in corso di stesura - rac20-1_aggiornamentospecifiche_in_corso_pianificazione.txt
LOTTO TESTATO Q0120AB PROTOTIPO STD032PLD CON PLASMIDE GENESCRIPT - rac20-1_produzione_e_cq_madre_plasmidica_phsv2_in_pgem_per_test_1.docx
la modifica non è significativa, pertanto non è da notificare. - rac20-1_mod05,36__non_significativa.pdf',NULL,N'06/02/2020
10/02/2020
31/01/2020
06/02/2020
06/02/2020
06/02/2020',N'10/02/2020
21/12/2020
23/12/2020',N'10/02/2020
10/02/2020
06/02/2020',N'No',N'nessun impatto sui DMRI',N'No',N'non necessaria',N'No',N'nessun impatto ',N'il secondo fornitore deve fornire plasmidi con caratteristiche equivalenti all''attuale fornitore.',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto ',N'No',N'vedere MOD05,36. Il fornitore qualificato in uso non viene sostituito, ma affiancato da un secondo fornitore per ridurre il rischio di mancate forniture, si rtiene pertanto una modifica non significativa.',N'No',N'NON è necessario inviare specifica aggiornata per il targhet  CMV ',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-23 00:00:00',NULL,NULL),
    (N'18-22',N'Preventiva','2018-06-25 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Una mancata registrazione delle risultanze delle riunioni di preparazione alla visita ispettiva potrebbe rendere difficoltosa la verifica dell''esecuzione dell''audit nel rispetto degli input individuati durante il briefing tra auditor e QARA.',N'Mensilmente pianificare gli audit dei 2 mesi successivi e i loro briefing. Accorpare i briefing degli audit del mese.
Aggiornare il piano di audit al fine di renderlo più schematico per poter estrarre facilmente le informazioni desiderate.
Rivedere l''intero processo di "Gestione audit interni ed esterni" per valutare la possibilità di snellire il processo.',NULL,N'La pianificazione mensile degli audit dei 2 mesi successivi e i loro briefing accorpati permette un miglioramento nella pianificazione deglle attività degli auditor.
Il piano di audit è strutturato sul modulo di report (MOD02,02) affinchè non debbano essere gestiti 2 documenti separati (piano e report) che conterrebbero le stesse informazioni iniziali. Il piano viene prodotto al fine di stabilire i punti da verificare per ciascun requisito normativo. L''attuale organizzazione del piano è poco schematica agevolando la successiva stesura del report in una forma molto descrittiva. La schematizzazione dei punti da analizzare durante la visita ispettiva renderà, non solo più facile poter estrarre le informazioni desiderate, ma permetterà di avere dei layout di piano specifici per ogni processo con la conseguente diminuzione dei tempi di preparazione dell''audit.
Il nuovo layout di piano guiderà anche la stesura schematica del report , che dovrebbe permettere un miglioramento dell''estrazione dei dati durant',N'17/07/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018',N'Struttura di un file con la pianificazione degli audit, delle riunioni di briefing e de-briefing, facendo attenzione ad accorpare i briefing degli audit del mese.
Aggiornare il layout del MOD02,02 "RAPPORTO VERIFICA ISPETTIVA" (rev.01) per renderlo un modello di piano schematico e, con la successiva raccolta delle evidenze durante l''audit, un modello di report di facile compilazione:
- nei documenti SGQ di riferimento aggiungere la matrice delle competenze e l''analisi delle NC/R/RAC
- strutturare il paragrafo "Evidenze rilevate"  per ciascun requisito o gruppi di requisiti come segue: requisito, domanda da porre, evidenza rilevata (con particolare riferimento al MOD18,02), descrcizione della NC/osservazione, azione, esito.
Lean process:
- il lead auditor dell''audit precedente deve partecipare alla successiva visita ipsettiva per dare continuità alle osservazioni emerse in precedenza
- l''esito del audit deve dar seguito all''apertura da parte dei reposnabili di processo dei Change control/Azioni preven',N'MOD02,02 rev02 - rap18-22_messa_in_uso_pr0201_03_mod0202_02_rap18-22_rac1839_mod1802_del_50319.msg
PR02,02 rev03 - rap18-22_messa_in_uso_pr0201_03_mod0202_02_rap18-22_rac1839_mod1802_del_50319_1.msg
formazione auditor interni 05/03/19 - rap18-22_messa_in_uso_pr0201_03_mod0202_02_rap18-22_rac1839_mod1802_del_50319_2.msg
formazione auditor interni del 5/03/19 - rap18-22_messa_in_uso_pr0201_03_mod0202_02_rap18-22_rac1839_mod1802_del_50319_3.msg',NULL,N'25/06/2018
17/07/2018
17/07/2018
17/07/2018
17/07/2018',N'25/06/2018',N'25/06/2018
05/03/2019
05/03/2019
05/03/2019
05/03/2019',N'No',N'nessun impaatto',N'No',N'non necessaria nessuna comunicazione',N'No',N'nessun impaatto',N'Aggiornamento della procedura di audit interni, nessun impatto sui requisiti regolatori',N'AU - Gestione audit interni ed esterni
AU - Gestione audit interni ed esterni
AU - Gestione audit interni ed esterni',N'AU2 - Pianificazione ed Esecuzione di audit a fornitori
AU3 - Pianificazione di audit interni
AU4 - Pianificazione ed esecuzione di audit non annunciati',N'AU2, AU3, AU4  come da risk assessment firmati a 12/2017. AU1, AU5, AU7 da verificare a seguito di Lean process.',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'Stefania Brun',N'No',NULL,'2020-09-30 00:00:00',N'Valutazione con gli auditor del nuovo layout di piano, analisi dei CC/RAP aperti dai Responsabili di processo a seguito di firma del report di visita ispettiva.
29/05/2020: nel programma 2020 si è cercato di semplificare ulteriormente il processo di audit, al fine di diminuire il carico di ore, ma non l''efficacia del processo. Le soluzioni adottate sono state diminuire i partecipanti all''audit e utilizzare il pc durante l''audit per la stesura del report. Si rimanda a settembre 2020 la valutazione dell''efficacia.
17/11/2020 il processo di audit è ulteriormente stato aggiornato includendo la possibilità di effettuare audit da remoto, a causa della pandemia di sars-cov2 attualmente in atto,  sia esterni che interni. 
Il flusso così strutturato si ritiene essere efficace (i calcoli sul carico di lavoro sono visibili nel file Excel "MOD02,01_00_ProgrammaAuditInterni_2020").',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-06-25 00:00:00','2019-03-05 00:00:00','2020-11-17 00:00:00'),
    (N'20-46',N'Correttiva','2020-12-10 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-105',N'tutti i prodotti',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'Nell''ambito dell''indagine della NC20-82 e NC20-105 sul prodotto "RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240" abbiamo identificato come causa primaria i primers sintetizzati da Eurogentec: rt S2R ORF8 F1 8012141 rt S2R ORF8 R1 8012146 e rtS2R RdRp F2 802155 rtS2R RdRp R2 8012160. E'' stata notificata a EGT la NC e attendiamo un loro riscontro. Il lotto è stato accettato in deroga.

Considerando le seguenti evidenze:
-	Il lotto è stato preparato impiegando gli stessi lotti di oligonucleotidi già usati per altri 11 lotti di produzione risultati conformi.
-	Il livello di fluorescenza associabile al lotto di questi oligonucleotidi è risultato sub-ottimale.
-	Dalla ripetizione del test di sensibilità 5 replicati su 6 sono risultati positivi anziché 6/6 ossia il 77% dei campioni analizzati.
Sulla base delle evidenze sopra riportate il lotto è stato rilasciato in concessione.
Sulla base di tale esperienza si ritiene opportuno modificare l''attuale criterio per la ripetizione del test di sensibilità in uso per il prodotto in oggetto in modo che in caso di risultato 2/3 positivi sia prevista una ripetizione del test con un requisito di positività maggiore al 75%-80%.
Quest''indicazione è diversa da quella riportata nell''istruzione IO10,14 "MODALITÀ DI INDAGINE TECNICA" e ha la finalità di correggere la stringenza del requisito del test di sensibilità in caso di ripetizione dello stesso che NON altera in alcun modo le performance e la sicurezza del prodotto stesso.',N'Si chiede la modifica dei criteri di accettazione utilizzati per valutare i dati di CQ durante la fase di indagine della NC, in particolare la modifica è finalizzata a definire una percentuale di positività anziché un numero definito di campioni. Questo permette di valutare la differente sensibilità richiesta da un prodotto di tipo multiplex o uno con target unico.',NULL,N'La definizione dei criteri su un numero percentuale di positivi in base al tipo di prodotto (multiplex, target singolo, quantitativo) permette di gestire in modo migliore le indagini di fuori specifica.
La definizione dei criteri su un numero percentuale di positivi in base al tipo di prodotto (multiplex, target singolo, quantitativo) permette di gestire in modo migliore le indagini di fuori specifica.
La definizione dei criteri su un numero percentuale di positivi in base al tipo di prodotto (multiplex, target singolo, quantitativo) permette di gestire in modo migliore le indagini di fuori specifica.
La definizione dei criteri su un numero percentuale di positivi in base al tipo di prodotto (multiplex, target singolo, quantitativo) permette di gestire in modo migliore le indagini di fuori specifica.
',NULL,N'Modifica della IO10,14 
formazione
valutazione dei criteri da applicare per le ripetizioni
messa in uso della IO
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  ',N'PR10,02_08 - rac20-46_messa_in_uso_pr1002_08.msg
PR10,02_08 - rac20-46_messa_in_uso_pr1002_08_1.msg',NULL,N'10/12/2020',N'15/02/2021
26/02/2021',NULL,N'No',N'-',N'No',N'-',N'No',N'-',N'La modifica dei criteri di accettazione utilizzati per valutare i dati di CQ durante la fase di indagine della NC è finalizzata a definire una percentuale di positività anziché un numero definito di campioni. Questo permette di valutare la differente sensibilità richiesta da un prodotto di tipo multiplex o uno con target unico. L''introduzione di una percentuale di positività rende questo criterio più stringente senza alterare la sicurezza e le performance del prodotto.',N'CQ - Controllo Qualità prodotti',N'CQ17 - Gestione OOS di CQ',N'-',N'No',N'vedere MOD05,36. la modifica di un criterio utilizzato per l''indagine di NC non impatta sul processo di CQ, pertanto la modifica non è significativa.',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-26 00:00:00',NULL,NULL),
    (N'20-42',N'Correttiva','2020-11-10 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',N'20-51',N'CTR201ING',N'Documentazione SGQ
Processo di produzione
Processo di CQ e criteri di CQ',N'Per quanto riguarda la NC relativa al plasmide CtxM15 (plasmide per il controllo positivo) il fornitore (EGI) aveva individuato un una sequenza con una mutazione, ma la comunicazione a EGSpA è avvenuta solo, successivamente, nell''ambito del DT, ciononostante il plasmide era acquistato con la corretta sequenza mutata, ma il documento di specifica non risultava aggiornato. 
Il plasmide utilizzato per il prodotto CTR201ING, lotto U0520-061 (oggetto  della NC), non contiene la corretta sequenza ed è stato acquistato secondo la specifica prototipo.
Per quanto concerne il plasmide CTXM9 (usato per il controllo della mix) la causa primaria della NC non è stata identificata ma si suppone un errore a livello della concentrazione dichiarata dal fornitore del plasmide fino ad ora utilizzato (EGI), infatti le preparazioni ripetute, la rilettura e la correttezza della sequenza del nuovo plasmide confermano che la procedura adottata e la concentrazione sono corrette. ',N'Si chiede la modifica della sequenza contenuta nella specifica per il plasmide pCTXM15. Per quanto concerne il plasmide pCTXM9 si intende utilizzare la concentrazione dichiarata del plasmde (10 copie in rxn) e pertanto di chiede di modificare i limiti per il CQ della mix.',NULL,N'I criteri attuali sono stabiliti per il plasmide fornito da EGinc e non più in uso, pertanto la modifica permette di verificare i lotti utilizzando il nuovo plasmide
La modifica permette di acquistare e utilizzare il plasmde per il CTR con la sequenza corretta.
L''errore riscontrato con le sequenze del plasmide CTX-M-15 in fase di design transfer è dovuto ad una mancata comunicazione da parte di EGI della sequenza corretta dell''inserto. Tale problematica è parzialmente rientrata a seguito della corretta comunicazione della sequenza plasmidica. 
Il fatto che la stessa mix conforme al Controllo qualità dia Ct diversi con i PC forniti da due fornitori/processi diversi significa che è differente il calcolo della concentrazione dei plasmidi, ma non ci sono impatti sul prodotto. A far seguito a questa RAC il processo produttivo prevedrà il nostro fornitore e il nostro processo produttivo, è pertanto necessario ritarare gli intervalli di Ct del CQ usando la stessa mix conforme di riferimento. Questa nuova',N'29/12/2020
29/12/2020
29/12/2020
29/12/2020
29/12/2020',N'Definizione dei nuovi criteri per CTXM9
Modifica della Verifica tecnica MOD10,12-RTS201ING
formazione
Test del nuovo lotto prodotto con il plasmide CTXM15 
preparazione del nuovo lotto  di produzione con il plasmide corretto.
modifica della specifica del plasmide CTXM15 e approvazione dei nuovi criteri di CQ
messa in uso dei documenti
acquisto del nuovo plasmide
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  
messa in uso documenti',N'spec952-pctx-m-1-15_01 - rac20-42_messa_in_uso_spec952-pctx-m-1-15.msg
Nuovi criteri CQ CTXM9 - rac20-42_nuovi_criteri_cq_ctxm9.pdf
mod1012-rts201ing-ctr201ing_03 - rac20-42_messa_in_uso_mod1012-rts201ing-ctr201ing_03.msg
Nuovi criteri CTXM15 e PC - rac20-42_ctxm15_e_pc_nuovi_criteri.pdf
la modifica è significativa, pertanto è da notificare ai paesi presso cui il prodotto è registrato ed a DEKRA - rac20-42_mod05,36_01_significativa.pdf
notifica a modifica effettuata - rac20-42_notification_of_change_-ctr201ing.msg
mod1012-rts201ing-ctr201ing_03 - rac20-42_messa_in_uso_mod1012-rts201ing-ctr201ing_03_1.msg
spec952-pctx-m-1-15_01 - rac20-42_messa_in_uso_spec952-pctx-m-1-15_1.msg
messa in uso ap - rac20-42_messa_in_uso_ap_prodotti_egspa.msg',NULL,N'29/12/2020
29/12/2020
29/12/2020
29/12/2020
29/12/2020
29/12/2020',N'29/01/2021
26/02/2021
26/02/2021',N'09/02/2021
08/02/2021
17/02/2021
09/02/2021
01/04/2021
19/02/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Il prodotto deve mantenere le stesse caratteristiche di performance e sicurezza anche con i plasmidi acquistati da fornitori differenti.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto sul RA R&D1.1: se la modifica di una materia prima critica porta all''apertura di una manutenzione di prodotto è necessario seguire le varie fasi di progettazione associate alle manutenzioni.',N'No',N'da valutare',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-10-29 00:00:00',N'Esito attività: per il plasmide pCTX M9 è stato necessario ricalcolare il criterio di CQ a causa di una amplificabilità differente rilevata con il nuovo plasmide, questo tipo di problematica è stata rilevata anche con il plasmide CTXM15 (CTR201ING); infatti l''utilizzo di una nuova sequenza del plasmide (senza mutazione) non è stato risolutivo della non conformità rilevata (benchè vi sia stato un miglioramento nei Ct).
Nello specifico il lotto testato ha dato esito conforme ma al limite dei criteri (Ct 31,1 con un criterio di 31,4), pertanto la correzione deve essere ampliata.
Con RDM si è deciso di uniformare le modifiche da apportare ai due plasmidi ricalcolando il criterio di CQ anche per il CTR201ING, questo significa dover modificare il cutoff dell''AP che ad oggi risulterebbe essere troppo vicino alle prestazioni rilevate in CQ (Ct 32).
Attività a seguire:
-Preparazione del nuovo lotto in backorder -> MM FATTO 962-CTR201ING, LOTTO U0121-108
-Test di ripetibilità e definizione dei nuovi criteri di CQ -> QCT FATTO, VEDER ALLEGATO
-Modifica AP -> PVM FATTO MAIL DI MESSA IN USO AP DEL 18/02/2021 ALLEGATA
- Verifica degli AP -> RS FATTO MAIL DI MESSA IN USO AP DEL 18/02/2021 ALLEGATA
-Modifica Moduli di CQ e formazione -> QCT FATTO MAIL DI MESSA IN USO MOD1012-RTS201ING-CTR201ING rev 03 DEL 9/02/2021
-CQ del nuovo lotto -> QCT  FATTO 962-CTR201ING, LOTTO U0121-108 CONFORME
-Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  -> QAS
-Installazione del AP presso il cliente -> ASSISTENZA APPLICATIVA



VALUTAZIUONE EFFICACIA: verificare assenza reclami (a seguito di installazione del AP) e nc sul prodotto ESBL
',NULL,NULL,NULL,NULL,NULL,'2021-02-26 00:00:00','2021-04-01 00:00:00',NULL),
    (N'19-24',N'Preventiva','2019-08-05 00:00:00',N'Stefania Brun',N'Regolatorio',NULL,NULL,NULL,N'Documentazione SGQ',N'La creazione delle anagrafiche su Navision è di competenza del personale dell''ufficio ordini che non possiede le info relative all''anagrafica dell''utilizzatore associato/i ad un cliente di fatturazione e di spedizione. Questo rende l''istruzione di rintracciabilità lotti IO14,01 poco efficace.',N'Inserimento nell''anagrafica su NAV di ciascun cliente utilizzatore italiano (cliente di spedizione) degli indirizzi e-mail necessari per inviare Avvisi di Sicurezza e, in generale, comunicazioni urgenti e/o rilevanti. A ciascun cliente sarà associato un indirizzo e-mail "operativo" (Responsabile del Laboratorio o indirizzo generico del Lab) e un indirizzo PEC (il più prossimo all''utilizzatore tra quelli disponibili). Definizione della gestione delle anagrafiche aggiornate.',NULL,N'Impatto positivo in quanto la disponibilità di anagrafiche degli utilizzatori sempre aggiornate è utile per le attività di supporto. Gli Specialist saranno coinvolti nell''attività di monitoraggio periodico dello stato di aggiornamento delle informazioni 
La gestione di indirizzi di posta nominativi va valutata nell''ambito del regolamento GDPR
La gestione di indirizzi di posta nominativi va valutata nell''ambito del regolamento GDPR
Positivo in quanto permette di ottemperare in modo più efficace e tempestivo agli obblighi di vigilanza in caso di recall o FSN/FSCA
Positivo in quanto permette di ottemperare in modo più efficace e tempestivo agli obblighi di vigilanza in caso di recall o FSN/FSCA
Impatto in termini cairco ore/lavoro.
Impatto in termini cairco ore/lavoro.
Impatto in termini di carico ore/lavoro: è necessario definire il monitoraggio del processo di gestione/aggiornamento delle anagrafiche 
Positivo in quanto l''integrazione dell''anagrafica con i recapiti degli utilizzatori pot',N'24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
11/11/2019',N'ciascun FPS riceve la parte di elenco clienti estratto da NAV di propria competenza. Da ciascun cliente utilizzatore dei prodotti (cliente di spedizione) ottiene l''indirizzo e-mail più idoneo all''uso in questione (indirizzo responsabile Lab o indirizzo comune del Lab). Nel reperimento del dato si basa sui requisiti forniti dal Quality.
L''ufficio Gare supporterà il personale dell''Operation (Secretary) nel completamento delle anagrafiche per quanto riguarda il reperimento dell''indirizzo di posta certificata del Laboratorio sul sito dell''ente ospedaliero  
Secretary per ciascun cliente di spedizione ricerca l''indirizzo PEC a lui più prossimo  in cllaborazione con il personale dell''ufficio Gare
Valutare in quale documento SGQ indicare attività e responsabilità di aggiornamento delle anagrafiche degli utilizzatori
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Valutare se inserire l''integrazione  delle',N'Piano azioni - 2019_08_05_rap_19-24_procedura_invio_fsn___+_gestione_indirizzi_e-mail.docx
DRAFT_PROCDURA GESTIONE INDIRIZZI MAIL ED INTEGRAZIONE PR14,03 - rap19-24__2019_08_05_procedura_invio_fsn___+_gestione_indirizzi_e-mail_1.docx
La modifica non è significativa, pertanto non è necessario effettuare alunca notifica. - rap19-24_mod05,36_non_significativa.pdf',NULL,N'24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019',N'27/03/2020
27/03/2020
27/03/2019
27/03/2020
24/10/2019
27/03/2020
27/03/2019
27/03/2019
27/03/2019
27/03/2020',NULL,N'No',N'No in quanto riguarda le anagrafiche degli utilizzatori dei prodotti',N'No',N'No in quanto l''azione non prevede modifiche da comunicare al cliente',N'No',N'No in quanto l''azione non è relativa al prodotto/i',N'Tale azione permette di ottemperare in maniera più efficace e tempestiva ai requisiti di vigilanza  in caso di recall o gestione di FSCA/FSN',N'SW - Elenco Software
ASA - Assistenza applicativa prodotti',N'SW1 - NAV (sistema gestionale)
ASA3 - Gestione reclamo con impatto sulla vigilanza (FSCA)',N'Da valutare integrazione di questa attività all''interno dei risk assessment SW1 e ASA3 ',N'No',N'Non significativa, vedere MOD05,36',N'No',N'No in quanto non si applica ai Virtual Manufacturer',N'Sergio  Fazari',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-03-27 00:00:00',NULL,NULL),
    (N'18-3',N'Correttiva','2018-01-10 00:00:00',N'Stefania Brun',N'Assistenza strumenti interni',N'Non conformità',N'18-8',N'minor non conformity AR12/Mi-02',NULL,N'The calibration certificate N° 13525 was reviewed and approved by personnel on charge (Technical Service Manager: Mr. M. D''Agostino) who checked that calibration results were in compliance with EGSPA requirements.
That document verification was not performed according to the working instruction in place (IO11,08 "Verifica di un certificato di taratura esterna" Rev.00). As a matter of fact that working instruction reports the whole list of information to be verified within a calibration certificate and that list clearly includes the traceability of reference tools used.
Calibration certificates released the same day for other Eppendorf Biophotometers uses by EGSPA (serial number 00408 and 02906) showed the same objective evidence of NC.
Within the NC investigation activities the calibration certificates for any kind of instruments used by EGSPA were reviewed and all of them resulted in compliance with the prescriptions of the working instruction IO11,08 "Verifica di un certificato di taratura esterna".
Based on above evidences NC sounds to be due to a mere oversight. 
It is highly probable that root cause is related to an excessive workload. At that time the person in charge (TSM: Mr M D''Agostino) was responsible of managing both internal and external (in use at customer site) instruments and site infrastructure.
That organizational issue has been already solved since EGSPA became aware of that and several additional resources were assigned to those functions. Current organization comprises a specific team for managing external instruments assistance and a dedicated person (Technical Service Specialist: Mr. D Salemi) to manage internal instruments and site infrastructure.
',N'According to the root cause identified the corrective measures have been already put in place (instrument management reorganization). 
Nevertheless some corrective and preventive actions have to be put in place as it was seen during NC investigation:
-	Updating Instrument management procedure (PR11,01 "Gestione delle apparecchiature" Ed.03 Rev. 00) and Working Instruction (IO11,08 "Verifica di un certificato di taratura esterna" Rev.00) in order to align them to the new organizational structure,
-	Re-training of the person in charge for internal instrument management (TSS: Mr D. Salemi).',NULL,N'Positive impact since procedure and working instruction in place will be aligned to the new organizational structure.
Positive impact since procedure and working instruction in place will be aligned to the new organizational structure.',N'10/01/2018
10/01/2018',N'Updating Instrument management procedure (PR11,01 "Gestione delle apparecchiature" Ed.03 Rev. 00) and Working Instruction (IO11,08 "Verifica di un certificato di taratura esterna" Rev.00) in order to align them to the new organizational structure,
Re-training of the person in charge for internal instrument management (TSS: Mr D. Salemi).',N'MESSA IN USO PR11,01rev01, IO11,08rev01,MOD18,02 del27/07/2018 - rac18-3_messa_in_uso_io1108_e_pr1101_rev01_cc56-15_rap4-16_rac18-3.msg
re-training 27/7/2018 - rac18-3_messa_in_uso_io1108_e_pr1101_rev01_cc56-15_rap4-16_rac18-3.msg',NULL,N'10/01/2018
10/01/2018',N'28/02/2018
28/02/2018',N'27/07/2018
27/07/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'T - Taratura Esterna Strumenti',NULL,N'T6',N'No',NULL,N'No',NULL,N'Stefania Brun',N'No',NULL,'2018-08-30 00:00:00',N'Effectiveness verification will be performed during internal audits to internal instrument management process. Report n°7-18, 15/05/2018.
Specifically it will be verified whether new calibration certificates are released in compliance with information/data requested by IO11,08. 
TS5, certificate n°102/2018, verified by TSS, Mr D. Salemi, dated 21/06/2018.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-02-28 00:00:00','2018-07-27 00:00:00','2018-08-06 00:00:00'),
    (N'17-33',N'Preventiva','2017-12-29 00:00:00',N'Guglielmo Stefanuto',N'Ricerca e sviluppo',NULL,NULL,NULL,N'Materia prima',N'Il passaggio da pCR2 al vettore pGEM non permette l''uso del sito di restrizione dell''enzima Nco I.',N'Per la linearizzazione del nuovo costrutto pASPERGILLUS si rende necessario l''utilizzo di un nuovo enzima di restrizione che è stato identificato in Xmn I (o il suo isoschizomero Pdm I). A tal fine sarà preparata da DS con il supporto di R&D, e approvata da MM e QC, la seguente documentazione:
- specifica reagenti consumabili "Xmn I",
- modulo di produzione "Linearizzazione plasmidi", MOD955-pXXX-L-XmnI.
Inoltre sarà aggiornato il MOD09,23 "Elenco Prodotti Moduli Diluizioni".',NULL,N'Identificazione di un nuovo enzima di restrizione per la linearizzazione del nuovo costrutto
formazione. approvazione documenti. MOD18,02 del 29/12/2017
Creazione modulo di produzione "Linearizzazione plasmidi", MOD955-pXXX-L-XmnI ed aggiornamento del MOD09,23 "Elenco Prodotti Moduli Diluizioni".
Messa in uso moduli, aggiornamento DMRI e FTP.',N'29/12/2017
29/12/2017
29/12/2017
29/12/2017',N'Identificazione di un nuovo enzima di restrizione, Xmn I, per la linearizzazione del nuovo costrutto pASPERGILLUS e creazione della specifica.
Creazione modulo di produzione "Linearizzazione plasmidi", MOD955-pXXX-L-XmnI ed aggiornamento del MOD09,23 "Elenco Prodotti Moduli Diluizioni".
Messa in uso moduli, aggiornamento DMRI e FTP.',N'DMRI2017-003_RTS110PLD_STD110PLD_01 - dmri2017-003_rts110pld_std110pld_01.pdf',NULL,N'29/12/2017
29/12/2017
29/12/2017',N'28/02/2018
29/12/2017',N'29/12/2017
29/12/2017
12/09/2019',N'Si',N'Aggiornamento con la specifica SPEC950-215 e con MOD955-pXXX-L-XmnI.',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'GM - Gestione delle modifiche',N'GM5 - Verifica/individuazione ed approvazione  degli impatti',N'GM5 (valutazione impatti CC)',N'No',N'non necessaria',N'No',N'non necessaria',NULL,N'No',NULL,'2019-12-30 00:00:00',N'A 02/01/209 non sono presenti NC associate a questa azione correttiva. La RAC rimane aperta in quanto il DMRI2017-003 rev00 non è aggiornato.
09/2019 DMRI2017-003 aggiornato con la rev01.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-02-28 00:00:00','2019-09-12 00:00:00','2019-09-12 00:00:00'),
    (N'17-21',N'Preventiva','2017-09-11 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',NULL,NULL,NULL,NULL,N'Disallineamento documetni interni e dichiarazione del fornitore.',N'Dato che il prodotto è venduto per applicazioni IVD si chiede di utilizzare la scadenza assegnata dal fornitore anche al nostro semilavorato 957-MOD957-EPH-TAE, al fine di non incappare in malfunzionamenti non garantiti e non testati internamente, modificando la specifica di SPC950-212.',NULL,N'Modifica della SPEC950-212 indicando l''attribuzione della scadenza del fornitore.
Verificare in fase di ordine del materiale la disponibilità di lotti sufficientemente lunghi, per il nostro utilizzo. In base allo storico Sigma rislascia il lotto a dicembre con scadenza 24 mesi
Modifica del modulo 957-MOD957-EPH-TAE per le modalità di attribuzione scadenze
Verifica impatti 
Messa in uso sepcfica e documenti. ',N'11/09/2017
11/09/2017
11/09/2017
11/09/2017
11/09/2017',N'Modifica e formazione SPEC950-212.
MOdifica SPEC950-212, rve01, in uso al 7/2/18, formazione (MOD18,02) del 7/2/18.
Valituazione delle criticità per l''approvvigionamento. 
Modifica del modulo 957-MOD957-EPH-TAE formazione.
mod957-MOD957-EPH-TAE  in uso il 25/10/17, MOD18,02 del 12/10/17
messa in uso doc. in uso a 10/2017 e 02/2018, come da mail allegate.',N'mod957-eph-tae - rac17-21_messa_in_uso_mod957-eph-tae__1.pdf',NULL,N'11/09/2017
11/09/2017
11/09/2017
11/09/2017',N'28/02/2018
30/11/2017
28/02/2018',N'07/02/2018
21/02/2018
25/10/2017
07/02/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'No',NULL,'2018-06-30 00:00:00',N'Il lotto C0518AE è risultato conforme al CQ, non ci sono NC imputabili al prodotto EPH03',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-02-28 00:00:00','2018-02-21 00:00:00','2018-07-02 00:00:00'),
    (N'19-3',N'Correttiva','2019-02-15 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',N'18-132',N'R18-133 e R18-134 Prodotti RTS FTD',N'Manuale di istruzioni per l''uso',N'Come da e-mail di F.Farinazzo del 18.12.2018 "Abbiamo eseguito un test per 12 reazioni (sessione completa) e  il reagente preparato è stato sufficiente ma non vi era il minimo residuo di reagente nel tubo di mix. Si segnala che in CQ viene utilizzato il calcolo + 2rx per 9 campioni, questo eccesso può non essere sufficiente per reazioni da 9 a 12 quando presente anche un minimo errore di dispensazione. Si tenga conto che per i prodotti mono reagente che prevedono la dispensazione di 20 µL di mix è previsto un eccesso di 40 µL (nel caso di una sessione completa sono 3µL a reazione mentre in questi casi l''eccesso reazione è di 2,5µL). 
Benché non abbiamo riprodotto il problema non possiamo escludere che un errore anche minimo nella preparazione possa dare il risultato visto dai clienti (nel caso sotto si è preso il caso peggiore con un 5% in meno). 
',N'Si chiede la modifica delle IFU introducendo una tabella di preparazione delle mix a 3 categorie. Il volume di scarto medio è del 25% escludendo i casi peggiori in cui si fanno 1 o 2 reazioni, questo permette di non modificare il volume fornito nel kit dei reagenti, senza alterare i costi e le dispensazioni',NULL,N'La modifica proposta non impatta sul prodotto in quanto la suddivisione degli scarti rimane in un ambito di accettabilità del 25% medio. E'' necessario modificare le IFU con le nuove indicazioni
La modifica proposta non impatta sul prodotto in quanto la suddivisione degli scarti rimane in un ambito di accettabilità del 25% medio. E'' necessario modificare le IFU con le nuove indicazioni
nessun impatto sul processo, ma attività da svolgere
nessun impatto sul processo, ma attività da svolgere
La modifica corregge il problema riscontrato dalle tre segnalazioni dei clienti, dovrebbe permettere di evitare segnalazioni ed evitare campioni non validi e ripetizioni
La modifica corregge il problema riscontrato dalle tre segnalazioni dei clienti, dovrebbe permettere di evitare segnalazioni ed evitare campioni non validi e ripetizioni
La modifica corregge il problema riscontrato dalle tre segnalazioni dei clienti, dovrebbe permettere di evitare segnalazioni e soprattutto evitare campioni non validi e ripet',N'21/02/2019
21/02/2019
21/02/2019
21/02/2019
21/02/2019
21/02/2019',N'modifica IFU
Creazione TAB
Verifica sperimentale eseguita da distributore UK (vedi mail)
Fornire frase per modifica IFU
messa in uso IFU
comunicazione agli specialist della modifica
Aggiornamento FTP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'IFU IN USO  - rac19-3_ifu_rts507ing_rts523ing_rts533ing_rts548ing_rts553ing_-_rac19-3_r18-132_r18-133_r18-134_cc18-12.msg
proposta UK - modifica_volumi_aggiuntivi_reazioni.msg
MESSA IN USO IFU - rac19-3_ifu_rts507ing_rts523ing_rts533ing_rts548ing_rts553ing_-_rac19-3_r18-132_r18-133_r18-134_cc18-12_1.msg
Specialist destinatari della mail di messa in us odelle IFU - rac19-3_ifu_rts507ing_rts523ing_rts533ing_rts548ing_rts553ing_-_rac19-3_r18-132_r18-133_r18-134_cc18-12_2.msg
FTPRTS553_CTR553 AGGIORNATO - rac19-3_master_product_technical_file_index_ftprts553_ctr553.docx
La modifica è significativa, i prodotti registrati sono evidenti nel MOD05,29 del 29/03/2018 - rac19-3_mod05,36_significativa.pdf
mail di notifica - _egspa_sgq___notification_of_change_-rts5xxing-_rac19-3.msg
AREGENTINA, nessun impatto regolatorio. - re___egspa_sgq___notification_of_change_-rts5xxing-_rac19-3_1.msg
SUD AFRICA, nessun prodotto registrato. - re___egspa_sgq___notification_of_change_-rts5xxing-_rac19-3_2.msg
SINGAPORE,  nessun impatto regolatorio. - re___egspa_sgq___notification_of_change_-rts5xxing-_rac19-3_3.msg
COLOMBIA,  nessun impatto regolatorio. - re___egspa_sgq___notification_of_change_-rts5xxing-_rac19-3.msg
NoC dekra - rac19-3_notice_of_change_egspa_rts553ing_rts533ing.pdf
TAB RTS507ING - rac19-3_elite_ingenius_-tab_p04_-_rev_ab_-__product_mv_elite_mgb_panel_notice.pdf
TAB RTS523ING - rac19-3_elite_ingenius_-tab_p05_-_rev_ab_-__product_mv2_elite_mgb_panel_notice.pdf
TAB RTS548ING - rac19-3_elite_ingenius_-tab_p06_-_rev_ab_-__product_rv_elite_mgb_panel_notice.pdf
TAB RTS553ING - rac19-3_elite_ingenius_-tab_p21_-_rev_ab_-__product_rb_elite_mgb_panel_notice.pdf
TAB RTS533ING - rac19-3_elite_ingenius_-tab_p09_-_rev_ab_-__product_sti_elite_mgb_panel_notice.pdf',NULL,N'21/02/2019
22/02/2019
15/02/2019
21/02/2019
21/02/2019
21/02/2019',N'28/02/2019',N'15/03/2019
22/05/2019
11/03/2019
15/03/2019
15/03/2019
30/07/2019
02/08/2019',N'No',N'Nessun impatto sul DMRI del prodotto',N'Si',N'la comunicazione al cliente è svolta attraverso l''avvertenza posta prima del IFU',N'No',N'nessun impatto',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'notificato, vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2019-12-20 00:00:00',N'Verifica della presenza di ulteriori reclami con la stessa causa primaria della RAC in oggetto.

Il prodotto è stato dismesso a 12/2019 , vedere CC20-7 ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-02-28 00:00:00','2019-08-02 00:00:00','2020-03-17 00:00:00'),
    (N'19-40',N'Correttiva','2019-12-04 00:00:00',N'Matteo Milani',N'Assistenza strumenti esterni (Italia)',N'Reclamo',N'19-114',N'ELITe Galaxy - INT020',N'Componenti principali',N'Bug del software (SW Version 4.3.0.4686)',N'Correzione bug software con patch prevista per la settimana 51 da parte di Hamilton',NULL,N'Impatto positivo in quanto la patch del SW permette di ripristinare le funzionalità del SW
Impatto positivo in quanto la patch del SW permette di ripristinare le funzionalità del SW
Impatto positivo in quanto la patch del SW permette di ripristinare le funzionalità del SW
Impatto positivo in quanto la patch del SW permette di ripristinare le funzionalità del SW
Verifica impatto nuova patch SW su documenti Galaxy quali IFU
Verifica impatto nuova patch SW su documenti Galaxy quali IFU
Impatto in termini di attività.
Impatto in termini di attività.
Impatto in termini di attività.',N'16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019',N'Eseguire test di verifica sulla patch SW rilasciata
Verifica installazione versione (SW Version 4.3.0.4686) presso clienti con Galaxy
Installare nuova patch del SW presso clienti con SW Version 4.3.0.4686
Verifica impatto nuova patch SW su documenti Galaxy quali IFU
Rilascio nuova patch SW 
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornamento FTP e valutazione impatto sll''analisi dei rischi
Rilascio nuova patch SW ',N'modifica bloccata in attesa di aletri bug. solo il cliente del reclamo R19-114 ha avuto questo tipo di problematica. internamente non si è riusciti a ricreare il bug. - rac19-40_bug_sw_elite_galaxi_r19-114_modifca_bloccata_in_attesa_di_altre_correzioni.msg',NULL,N'16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/12/2019
16/01/2019',N'20/01/2020
21/01/2020
24/01/2020
28/02/2020
24/01/2020',N'16/12/2019',N'No',N'no',N'Si',N'si presso i clienti con SW Version 4.3.0.4686',N'Si',N'si presso i clienti con SW Version 4.3.0.4686',N'La correzione del bug consente di avere un sw conforme.',N'SW - Elenco Software',N'SW18 - ELITe GALAXY Software (EG SpA) (gestione strumenti EA, sw applicativo EGSpA per l''estrazione acido nucleico e PCR setup)',N'-',N'No',N'vedere MOD05,36.',N'No',N'nessun virtual manufacturer',N'Matteo Milani',N'No',NULL,NULL,N'Verifica assenza di reclami per la problematica in oggetto da parte dei clienti che hanno installata la nuova patch del SW',N'Stefania Brun',NULL,NULL,NULL,NULL,'2020-02-28 00:00:00',NULL,NULL),
    (N'19-38',N'Correttiva','2019-11-25 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',N'19-120',N'955-TWEEN80',N'Documentazione SGQ
Processo di produzione',N'Arrotondamento errato . Verificando il calcolo per la preparazione del 955-Tween-80-10 è stato evidenziato un errore di arrotondamento: il modulo riporta 0,957µL mentre il dato corretto è 0,958µL. L''errore ha avuto un''influenza minima il volume da aggiungere sarebbe risultato 8,617mL anziché 8,613mL. Questo tipo di errore è già stato evidenziato nelle preparazioni degli oligo (NC19-72) pertanto risulta utile aggiornare i moduli che possono dare origine ad errori analoghi così come fatto nella RAC19-26.',N'Si chiede di aggiornare i moduli sopracitati affinchè siano chiarite le modalità di esecuzione dei calcoli per ridurre gli errori di calcolo. Al fine di adeguare la documentazione ai requisiti GMP si chiede di differenziare la tabella dei calcoli da quella del trasferimento di volume,  prevedendo l''inserimento di spazi dedicati alla registrazione del volume pianificato da trasferire e a quello trasferito. Queste modifiche allineano i documenti alle norme GMP.
',NULL,N'Nessun impatto nel processo, solo in termini di attività
Il cambiamento permette di ridurre gli errori tecnici causati da errati arrotondamenti
Il cambiamento permette di ridurre gli errori tecnici causati da errati arrotondamenti
Nessun impatto nel processo, solo in termini di attività
Nessun impatto nel processo, solo in termini di attività
Impatto positivo in quanto la registrazione dettagliata di ciascuna attività mira all''adeguamento ai requisiti GMP.',N'25/11/2019
25/11/2019
25/11/2019
25/11/2019
25/11/2019
25/11/2019',N'Modifica dei moduli
 MOD955-GLIC10
MOD955-GLIC50
MOD955-CHAPS-10
MOD955-TWEEN80-10
formazione
Simulazione dei documenti
approvazione
valutare l''impatto sul RA P1.4 calcolo delle quantità
messa in uso dei moduli
aggiornamento del VMP, se varia il risk assesment P1.4
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'Mesa in uso moduli glicerolo e tween, MOD18,02 del 08/05/2020 - rac19-38_messa_in_uso_moduli_glicerolo_e_tween.msg
Messa in uso moduli glicerolo e tween, MOD18,02 del 08/05/2020 - rac19-38_messa_in_uso_moduli_glicerolo_e_tween_1.msg
La modifica non è significativa, pertanto non necessita di notifica. - rac19-38_non_significativa.pdf
Dall''analisi annuale 2020 risulta che il risk assessment è da aggiornare inserendo la PROBABILITA'' MEDIA e LIVELLO 3: confermata dai dati nei 4 anni. Alla luce dei dati registrati nei 4 anni (dal 2017 al 2020) è emerso una probabilità media che si verifichino errori sul calcolo delle quantità in fase di produzione. La matrice di gestione del rischio è da aggiornare a seguito di valutazione dell''efficacia dei controlli inseriti con le RAC19-38 (causa primari arrotondamenti errati; azione correttiva intrapresa: specificate nei moduli le modalità di esecuzione dei calcoli) e RAC19-26 (causa primaria: procedura non applicata per il calcolo del volume iniziale, azione correttiva intrapresa: aggiornamento dei moduli di produzione). L''aggiornamento sarà trattato nel report annuale 2020 - rac19-38_ranondamodificare.txt',NULL,N'25/11/2019
25/11/2019
25/11/2019
25/11/2019
25/11/2019
25/11/2019',N'15/01/2020
19/11/2019
31/01/2020
28/02/2020
15/01/2020',N'21/05/2020
21/05/2020
04/03/2021
21/05/2020
25/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'L''aggiornamento della documentazione alle attivià svolte e la loro registrazione mira all''adeguamento ai requisiti GMP. ',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.4 - Calcolo delle quantità',N'25/11/20198valutare l''impatto sul RA P1.4

21/05/2020
Introdurre le misure aggiuntive introdotte con questa RAC nel RA P1.4 e indicare la probabilità media di accadimento dell''evento come da dati riscontrati nei 3 anni 2017/2018/2019. ',N'No',N'vedre MOD05,36',N'No',N'valutare',NULL,N'No',NULL,'2020-12-31 00:00:00',N'Assenza di NC con causa primaria errori di calcolo per arrotondamenti errati. 
A 03/2021 non si sono registrate NC con causa primaria errori di calcolo per arrotondamenti errati. Il RA 1.4 verrà aggiornato a seguito di riesame annuale 2020.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-02-28 00:00:00',NULL,'2021-03-04 00:00:00'),
    (N'21-16',N'Correttiva','2021-05-20 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'21-38',N'Assay Protocol WNV ELITe',N'Assay Protocol',N'L''Assay Protocol WNV ELITe_Open_200_100_00 contiene ancora una dicitura obsoleta in cui compare il "sonication cycle 1" che viene visto come non compatibile con gli AP che hanno il "sonication cycle 0". Attualmente il prodotto non è soggetto a sonicazione per cui il set di assay open di WNV può essere modificato.',N'Modifica del set di assay open di WNV per eliminare la dicitura obsoleta "sonication cycle 1".',NULL,N'Nessun impatto a cambiamento avvenuto. Impatto in termini di attività
Impatto in termini di attività
Impatto positivo per i clienti poichè possono utilizzare l''AP generico per WNV su un estratto che viene amplificato anche in altri track per altri target
Impatto positivo per i clienti poichè possono utilizzare l''AP generico per WNV su un estratto che viene amplificato anche in altri track per altri target
Impatto positivo per i clienti poichè possono utilizzare l''AP generico per WNV su un estratto che viene amplificato anche in altri track per altri target
Nessun impatto a cambiamento avvenuto. Impatto in termini di attività',N'24/05/2021
24/05/2021
24/05/2021
24/05/2021
24/05/2021',N'Modifica del set di AP di WNV per eliminare la dicitura obsoleta "sonication cycle 1" 
Messa in uso degli Assay Protocol aggiornati a seguito delle modifiche apportate.
Aggiornamento nuova revisione AP presso i clienti
Aggiornamento TAB P28 - Rev AA e rilascio
verifica AP',N'WNV revisione (rev 01) AP OPEN  - rac21-16_ap_wnv_elite_mgb_kit.msg
WNV revisione (rev 01) AP OPEN  - rac21-16_ap_wnv_elite_mgb_kit_1.msg',NULL,N'24/05/2021
24/05/2021
24/05/2021
24/05/2021',N'28/05/2021',N'27/05/2021
27/05/2021',N'No',N'nessun impatto sul DMRI',N'No',N'da fare attraverso TAB',N'No',N'nessun impatto',N'nessun impatto regolatorio',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'verificare l''aggiornamento del RA AP1',N'No',N'vedere MOD05,36. la notifica alle autorità competenti non è necessaria per l''aggiornamento degli AP. ',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-28 00:00:00',NULL,NULL),
    (N'21-11',N'Correttiva','2021-03-19 00:00:00',N'Stefania Brun',N'Magazzino',N'Non conformità',N'21-16',N'INT021EX',N'Documentazione SGQ',N'L''errore è causato dal mancato confinamento in quarantena del lotto 2101/001, questo è imputabile alla mancata indicazione sulla specifica SPEC902-744502.205976 rev.00 utilizzata per il controllo al ricevimento del prodotto INT021EX del controllo funzionale e della registrazione dell''esito per completare l''accettazione. 
',N'1.Integrare all''interno della specifica del prodotto INT021EX il controllo funzionale da parte del CQ per l''accettazione del lotto e il rilascio per la vendita.
2.Verificare quali altri materiali/prodotti prevedono in fase di controllo al ricevimento il controllo funzionale in modo da integrare le specifiche utilizzate per il controllo al ricevimento
3.Creare per i consumabili InGenius le specifiche per il controllo al ricevimento in modo da includere anche il rilascio del lotto da parte di QA',NULL,N'Nessun impatto se non in termini di attività
Positivo in quanto assicura la corretta gestione di stato del prodotto in fase di controllo al ricevimento e accettazione
Positivo in quanto assicura la corretta gestione di stato del prodotto in fase di controllo al ricevimento e accettazione
Positivo in quanto assicura la corretta gestione di stato del prodotto in fase di controllo al ricevimento e accettazione
Positivo in quanto assicura la corretta gestione di stato del prodotto in fase di controllo al ricevimento e accettazione
Positivo in quanto assicura la corretta gestione di stato del prodotto in fase di controllo al ricevimento e accettazione
Nessuno se non in termini di attività
Positivo in quanto garantisce la tracciabilità per i prodotti per i quali è previsto il controllo funzionale
Positivo in quanto garantisce la tracciabilità per i prodotti per i quali è previsto il controllo funzionale',N'29/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
29/03/2021
29/03/2021
29/03/2021',N'Modifica della specifica per il prodotto INT021EX e per quelli che prevedono controllo funzionale.
Creazione delle specifiche dei consumabili InGenius per inserire sia controllo del CoA e autorizzazione al rilascio del lotto da parte di QA
Messa in uso delle specifiche revisionate e di quelle di nuova creazione per i consumabili InGenius
La modifica non è significativa, per tanto no è da notificare.
nessuna attività. il flusso di rilascio del lotto sarà indicato in specifica.',NULL,NULL,N'29/03/2021
25/03/2021
25/03/2021
25/03/2021
29/03/2021',N'19/04/2021
30/04/2021
28/05/2021
25/03/2021
29/03/2021',N'25/03/2021
29/03/2021',N'No',N'No in quanto il DMRI non contempla i moduli per il controllo in ingresso da parte del Virtual Manufacturer',N'No',N'No in quanto si tratta di documentazione interna al QMS',N'No',N'No in quanto si tratta di documentazione interna al QMS',N'La modifica delle specifiche migliora la tracciabilità e il controllo di stato del materiale/prodotti pertanto risponde ai requisiti regolatori',N'R - Ricevimento / stoccaggio Materiali e Strumenti',N'R5 - controllo di accettazione',N'-',N'No',N'No in quanto si tratta di documentazione interna al QMS che non ha impatto a livello di registrazione dei prodotti',N'No',N'No in quanto per i prodotti coinvolti è EGSpA il Virtual Manufacturer',N'Stefania Brun',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-28 00:00:00',NULL,NULL),
    (N'19-15',N'Preventiva','2019-06-12 00:00:00',N'Alessandra Gallizio',N'Produzione',NULL,NULL,NULL,N'Etichette',N'Poiché il primo lotto di vendita nella nuova versione (3° lotto pilota) dovrà essere utilizzato dai clienti per le attività di ricalibrazione annuale per il calcolo del fattore di conversione, è necessario apporre un identificativo esterno temporaneo per distinguere le due versioni di kit che potrebbero essere entrambe a disposizione dei clienti.',N'Si richiede di apporre sul lato corto della nunc del prodotto RTSG07PLD210 LP3 una etichetta verde riportante la scritta: "AVVERTENZA  Il kit presenta una miglioria tecnica per cui è necessario procedere secondo le indicazioni fornite dal fabbricante. Per informazioni rivolgersi a ingenius.support@elitechgroup.com"
L''etichetta verrà apposta a confezionamento ultimato e verrà compilato un apposito modulo di rilavorazione (MOD09,08) ',NULL,N'La rilavorazione del terzo lotto pilota non ha impatti sulle prestazioni del prodotto in quanto si seguono le indicazioni riportate per il confezionamento (MOD962-MOD962-RTSG07PLD210-Taq), che garantiscono idonee condizioni di permanenza a temperatura ambiente per apposizione delle''etichetta aggiuntiva.
La rilavorazione del terzo lotto pilota non ha impatti sulle prestazioni del prodotto in quanto si seguono le indicazioni riportate per il confezionamento (MOD962-MOD962-RTSG07PLD210-Taq), che garantiscono idonee condizioni di permanenza a temperatura ambiente per apposizione delle''etichetta aggiuntiva.
La rilavorazione del terzo lotto pilota garantisce la la corretta identificazione del lotto che deve essere sottoposto a ricalibrazione annuale per il calcolo del fattore di conversione.
L''impatto è positivo in quanto il cliente, informato dal personale del MKT, può distinguere tra il kit in uso ed il kit con il quale esegue le attività di ricalibrazione annuale per il calcolo del fattore di conversion',N'20/06/2019
20/06/2019
20/06/2019
20/06/2019
20/06/2019
20/06/2019',N'rilavorazione terzo lotto pilota: stampa etichetta verde e apposizione etichette su tutte le confezioni
rilasco del lotto rilavorato e verifica DHR
approvare il contenuto dell''etichetta verde riportante la scritta: "AVVERTENZA  Il kit presenta una miglioria tecnica per cui è necessario procedere secondo le indicazioni fornite dal fabbricante. Per informazioni rivolgersi a ingenius.support@elitechgroup.com" e veicolare informazione ai clienti.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
approvare il contenuto dell''etichetta verde riportante la scritta: "AVVERTENZA  Il kit presenta una miglioria tecnica per cui è necessario procedere secondo le indicazioni fornite dal fabbricante. Per informazioni rivolgersi a ingenius.support@elitechgroup.com" e veicolare informazione ai clienti.',N'RILAVORAZIONE ESEGUITA SUL LOTTO U0719AZ E U0719AB - rap19-15_rilavorazione_rtsg07pld210_stdg07pld210.pdf
Rilascio lotti per la vendita - rap19-15_rilascio_lotti_per_la_vendita_progetto_rtsg07pld210.msg
 La modifica non è significativa, pertanto non è necessario effettuare alcuna notifica. - rap19-15_mod05,36_non_significativa.pdf
L''etichetta verde riporta la seguente frase, e non quella riportata inzialemnte nel testo dalla RAP: "WARNING Product technical update, please contact your local support for further information AVVERTENZA Il prodotto presenta un aggiornamento tecnico, contattare il supporto locale per maggiori dettagli" - rap19-15_etichetta_verde.pdf',NULL,N'20/06/2019
20/06/2019
20/06/2019
20/06/2019
20/06/2019',N'28/06/2019
28/06/2019
28/06/2019
28/06/2019',N'25/11/2019
27/11/2019
20/06/2019
25/11/2019',N'No',N'nessun mpatto sulprodotto finito',N'Si',N'effettuata tramite etichetta stessa',N'No',N'nessun impatto',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun ra associato',N'No',N'veder MOD05,36. Non significativa si tatta di un''etichetta informativa, non sul prodotto.',N'No',N'nessun virtual manufacturer',N'Alessandra Gallizio',N'No',NULL,'2020-09-30 00:00:00',N'11/2019: dal momento che coesisteranno 2 versioni del prodotto BCR-ABL P210 ELITe MGB Kit, codice RTSG07PLD210 , la nuova contraddistinta da un etichetta di warning di colore verde apposta sul lato della confezione e la precedente (senza etichetta verde) verificare che non vi siano errori in fase di spedizione durante tutto il periodo di coesistenza.

Ad 08/2020 non sono presenti reclami di spedizione: l''ultimo lotto venduto nel formato "vecchio" è il lotto U1119AZ (scadenza 31/01/2021) in 10/03/2020. La coesistenza delle 2 versioni di kit è terminata a 03/2020, si ritiene pertanto possibile chiudere questa azione preventiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-06-28 00:00:00','2019-11-27 00:00:00','2020-08-27 00:00:00'),
    (N'19-9',N'Correttiva','2019-04-12 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-5',N'Contaminazioni causate da operatori',N'Documentazione SGQ',N'Probabile contaminazione da parte dell''operatore, raffreddato al momento dell''allestimento del test; manca in PR/IO l''indicazione di utilizzo di una mascherina in situazioni simili.',N'Si chiede di indicare nella IO24,01 "GESTIONE ABBIGLIAMENTO ALL''INTERNO DEI LABORATORI" la necessità di utilizzare una mascherina per la protezione del prodotto (non DPI) quando l''operatore presenta stati di raffeddamento o inflenzali, al fine di evitare contaminazioni dei reagenti.',NULL,N'Impatto positivo in quanto si migliorano le precauzioni collegate alla pulizia durante l''utilizzo/preparazione di reagenti.
Nei laboratori R&D non è necessario l''utilizzo di mascherine a protezione del prodotto.
Impatto positivo in quanto si migliorano le precauzioni collegate alla pulizia durante l''utilizzo/preparazione di reagenti.
Nei laboratori R&D non è necessario l''utilizzo di mascherine a protezione del prodotto.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'12/04/2019
12/04/2019
12/04/2019
12/04/2019
15/04/2019
15/04/2019
15/04/2019
15/04/2019',N'Modifica IO24,01 rev01 
Richiedere fornitura a ufficio acquisti
Approvazione della IO24,01 in cui si specifica che nei laboratori R&D non è necessario l''uso di mascherine a protezione dei prodotti.
Approvazione della IO24,01 e formazione a PT e PTC
Approvazione della IO24,01 in cui si specifica che nei laboratori R&D non è necessario l''uso di mascherine a protezione dei prodotti.
Messa in uso IO24,01 rev01
Aggiornamento PU1 "Controllo/pulizia periodica area di lavoro" inserendo come misura di controllo l''uso di mascherine in caso di operatore raffreddato e VMP.
Acquisto delle nuove mascherine  e monitoraggio scorta a magazzino
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019.msg
IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019_1.msg
IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019_2.msg
IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019_3.msg
IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019_4.msg
IO24,01_01 in uso al 29/10/19 - rac19-9_messa_in_uso_io2401_rev01_rac19-9_mod1802_del_18102019_5.msg
la modifica NON è significativa, pertanto non è necessario effettuare notifica presso le autorità competenti. - rac19-9_mod05,36_non_significativa.pdf
Nel risk assessment PU1 è stato inserito come misura di controllo l''uso dell mascherine al personale CQ e produzione in caso di rafferddore. Il RA non è ancora stato approvato in quanto da aggiornare anche per l''utilizzo dei panni monouso come da CC19-28. Si ritiene quindi possibile chiudere questa azione correttiva. - rac19-9_aggiornamento_ra_pu1_con_cc19-28.txt',NULL,N'12/04/2019
12/04/2019
12/04/2019
12/04/2019
15/04/2019
15/04/2019
15/04/2019
15/04/2019',N'28/06/2019',N'29/10/2019
29/10/2019
29/10/2019
29/10/2019
29/10/2019
13/03/2020
29/10/2019
22/05/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'PU - Pulizia Aree di lavoro e Strumenti',N'PU1 - Controllo/pulizia periodica area di lavoro',N'PU1 Controllo/pulizia periodica area di lavoro',N'No',N'non necessaria, vedere MOD05,36',N'No',N'non necessaria',N'Federica Farinazzo',N'No',NULL,'2020-03-13 00:00:00',N'verificare l''assenza di NC la cui causa primaria è dovuta a contaminzione a causa di personale raffreddato.
Nei 6 mesi successivi alla chiusura delle attività non si sono verificate altre NC con uguale causa primaria. Nel risk assessment PU1 è stato inserito come misura di controllo l''uso dell mascherine al personale CQ e produzione in caso di rafferddore. Il RA non è ancora stato approvato in quanto da aggiornare anche per l''utilizzo dei panni monouso come da CC19-28. Si ritiene quindi possibile chiudere questa azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-06-28 00:00:00','2020-03-13 00:00:00','2020-03-13 00:00:00'),
    (N'18-30',N'Correttiva','2018-10-03 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'18-18',N'IVS-0011 e IVS-0032',N'Documentazione SGQ',N'In base ai test eseguiti è possibile individuare come causa di NC la sospensione del campione di RNA. Ciò nonostante è evidenziato che il nuovo lotto ordinato e risospeso avente stesso lotto di partenza del fornitore, presenta quantità inferiori rispetto ai 2 lotti precedenti. Inoltre si è evidenziata una certa variabilità per quel che riguarda i dati ottenuti in base alla posizione del campione su blocco. Questi due fatti sono da tenere in considerazione per quel che riguarda le modalità di CQ e per l''utilizzo del campione stesso di riferimento. Infatti l''utilizzo del nuovo campione di riferimento il cui numero di copie è inferiore rispetto ai 2 lotti precedenti potrebbe causare l''esito non conforme per lotti di MIX che sarebbero risultati conformi appunto con i lotti precedenti. ',N'Modifica delle modalità di caratterizzazione e preparazione del campione per CQ, prevedendo la caratterizzazione del nuovo campione in base alla variazione definita rispetto al lotto in uso. L''azione correttiva va applicata anche al campione IVS-0032.
Stabilire delle date di utilizzabilità dei materiali plastici aperti nel laboratorio BSL2 CQ, data una bassa frequenza di utilizzo, al fine di prevenire eventuali contaminazioni da inibitori.',NULL,N'La modifica dovrebbe permettere al CQ una migliore standardizzazione del campione utilizzato come riferimento consentendo una valutazione il più possibile univoca dei lotti in caso di modifica del lotto di campione di riferimento. Resta da verificare e validare il metodo di standardizzazione al fine di individuare eventuali problematiche nel metodo.
La modifica dovrebbe permettere una valutazione il più possibile univoca dei lotti in caso di modifica del lotto di campione di riferimento.
Nessun impatto sul processo, sono previste azioni a carico del dipartimento',N'03/10/2018
08/10/2018
08/10/2018',N'Aggiornamento IO10,06
VS0011
Test di caratterizzazione
Test di validazione del fattore calcolato
Valutazione dei risultati
Modifica dei moduli/simulazione/formazione
IVS0032
Verifica disponibilità nuovo lotto P190
Test di caratterizzazione
Test di validazione del fattore calcolato
Valutazione dei risultati
Modifica dei moduli/simulazione/formazione
Valutazione dei dati di verifica della nuova modalità di CQ
messa in uso dei documenti',N'MOD,10,12-IVS-0032-ClonalControl-RNA rev01 - rac18-30_messa_in_uso_mod1012-ivs-0032-clonal-control-rna_01_rac18-30_mod1802_090719.msg
MOD10,12-IVS-0011-Clonal-Control-RNA rev01_MOD10,12-IVS-00XX-Clonal-Control-RNA_04 - rac18-30_messa_in_uso_mod_cq_rtsg07pld210_rac18-30_scp2019-005_scp2018-026.msg
rAPPORTO MODALITA'' DI CONTROLLO - rac18-30_rep2018-211_rac18-30_modalita_-controllo_ivs-0011_ivs-0032_00_1.pdf
PIANO D''AZIONE TERMINATO - rac18-30_ivs0011-32_action_plan_terminato.pdf
RAPPORTO MODALITA'' DI CONTROLLO - rac18-30_rep2018-211_rac18-30_modalita_-controllo_ivs-0011_ivs-0032_00_2.pdf
moduli in uso - rac18-30_messa_in_uso_mod_cq_rtsg07pld210_rac18-30_scp2019-005_scp2018-026_1.msg
MOD1012-IVS-0032-Clonal-Control-RNA_01  - rac18-30_messa_in_uso_mod1012-ivs-0032-clonal-control-rna_01_rac18-30_mod1802_090719.msg',NULL,N'03/10/2018
08/10/2018
08/10/2018',N'28/12/2018',N'07/08/2019
26/07/2019
07/08/2019',N'No',N'nessun impato sui DMI',N'No',N'non necessaria comunicaione',N'No',N'nessun impatto',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ1 - Recupero, preparazione e stoccaggio campioni da Fornitore Esterno',N'CQ1',N'No',N'nessun notifica necessaria',N'No',N'nessun notifica necessaria',NULL,N'No',NULL,'2019-08-07 00:00:00',N'Nuove modalità di controllo applicate a IVS001, lotto D0519AA, scadenza 30/04/2022 e IVS0032 lotto D0719AL, scadenza 31/07/2021. Essesndo i due materiali acquistati solo a scadenza del lotto, si ritengono sufficienti i dati ottenuti con i lotti sopracitati.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-12-28 00:00:00','2019-08-07 00:00:00','2019-08-07 00:00:00'),
    (N'20-48',N'Correttiva','2020-12-21 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-128',N'CTR180ING',N'Documentazione SGQ',N'La causa è collegata ad un errore umano non identificato dal controllo finale di CQ. La mancata rilevazione è causata da un errore puntuale, ma anche dalla procedura errata indicata durante il DT. In particolare non era stato indicato di eseguire la registrazione dell''esito di caratterizzazione sul MOD10,16 nel MOD10,18 durante la formazione; questo avrebbe aggiunto un ulteriore step di controllo permettendo probabilmente di identificare l''errore di registrazione della Verifica Tecnica. 
L''errore è limitato alla valutazione dell''idoneità del campione per uso CQ e la contaminazione del plasmide è minima e non impatta sull''esito del lotto di CTR180ING.',N'Si chiede di modificare la Verifica Tecnica inserendo le formattazioni condizionali come aiuto visivo nella valutazione dei risultati e di prevedere una nota per la valutazione del CTR180ING. In particolare nel caso di mancata idoneità dei plasmidi per uso CQ risulta necessario valutare l''impatto sul lotto prodotto CTR180ING.',NULL,N'Risulta necessario qualificare mediante simulazione dell''utilizzo le formattazioni della verifica tecnica. In routine la modifica è positiva in quanto aiuta QCT a verificare i risultati. 
Considerare queste modifiche nelle future Verifiche tecniche di prodotto.
Miglioramento del processo di verifica dei risultati di CQ.
Miglioramento del processo di verifica dei risultati di CQ.',N'19/01/2021
19/01/2021
19/01/2021',N'modifica della verifica tecnica MOD10,12-RTS180ING e MOD10,12-CTR180ING
Dato l''utilizzo frequente modificare anche MOD10,12-RTS170ING MOD10,12-CTR170ING MOD10,12-STD170ING
Simulazione per verifca della formattazione
Formazione
Modifica del DHRI di CTR180ING
messa in uso documenti
(RAC da inserire in RA CQ14 e CQ15)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  ',N'messa in uso MOD10,12-CTR170ING_03 e MOD10,12STD170ING_02 - rac20-48_messa_in_uso_mod1012-xx170ing.msg
MOD10,12-CTR180ING MOD10,12-RTS180ING REV01 IN USO - rac20-48_messa_in_uso_mod1012-ctr180ing_e_rts180ing.msg
RAC20-48_DHRI_01CTR180ING_29-01-2021 - rac20-48_dhri_01ctr180ing_29-01-2021.txt
messa in uso MOD10,12-CTR170ING_03 e MOD10,12STD170ING_02 - rac20-48_messa_in_uso_mod1012-xx170ing_1.msg',NULL,N'21/12/2020
19/01/2021
19/01/2021
19/01/2021',N'31/12/2020
29/01/2021
29/01/2021',N'12/03/2021
03/02/2021
03/02/2021
03/02/2021',N'No',N'nessun impatto',N'No',N'non necessaria, il lotto risulta comunque essere conforme',N'No',N'nessun impatto',N'L''errore è limitato alla valutazione dell''idoneità del campione per uso CQ e la contaminazione del plasmide è minima e non impatta sull''esito del lotto di CTR180ING.',N'CQ - Controllo Qualità prodotti',N'CQ14 - Fase postanalitica CQ: analisi e verifica tecnica dei risultati',N'da valutare se c''è impatto sul RA CQ14',N'No',N'vedere MOD05,36. La RAC non modifica il processo di CQ o i criteri di cq, ma facilita la verifica dei risultati di CQ.',N'No',N'nessun virtula manufacturer',N'Federica Farinazzo',N'No',NULL,NULL,N'valutazione efficacia: assenza NC con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2021-01-29 00:00:00','2021-03-12 00:00:00',NULL),
    (N'20-35',N'Correttiva','2020-10-14 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Reclamo',N'20-65',N'RTS120ING ',N'Assay Protocol',N'L''errore fornito dallo strumento è imputabile alla mancata corrispondenza tra il reference delle due mix di MDR-MTB riportato nel datamatrix delle etichette dei tubi e quello nel database InGenius. Infatti in quest''ultimo come confermato da RS il reference  è RTS120ING ed è associato alla mix 1 che viene utilizzata maggiormente dai clienti.  Invece quello del datamatrix è specifico per la MIX1 e per la MIX2 come è corretto che sia infatti come per gli altri codici a barre questo prende le informazioni relative a ref ,lotto e scadenza da Navision. Il reference su Nav dei 2 componenti è rispettivamente RTS120INGTB1 e RTS120INGTB2 come da PLP prodotto.
Occorre quindi procedere alla modifica del reference sul db di Ingenius in modo che corrisponda ai reference delle 2 Mix.
',N'Correzione dei reference delle mix di RTS120ING attraverso il tool del Software InGenius,  "ReagentSetsUpdater",  che gli specialist possono utilizzare per sostituire i reference corretti delle 2 mix in modo da allinearli a quelli forniti dal datamatrix delle etichette. Questa modifica evita la modifica degli assay protocol di MDR-MTB già installati presso i clienti. ',NULL,N'Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Positivo in quanto la modifica permette l''acquisizione corretta da parte dello strumento delle informazioni contenute nel datamatrix delle etichette del prodotto MDR-MTB e quindi previene errori di lettura da parte dello strumento InGenius
Positivo in quanto la modifica permette l''acquisizione corretta da parte dello strumento delle informazioni contenute nel datamatrix delle etichette del prodotto MDR-MTB e quindi previene errori di lettura da parte dello strumento InGenius
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività',N'14/10/2020
14/10/2020
14/10/2020
14/10/2020
14/10/2020
14/10/2020
14/10/2020',N'Modificare i reference delle mix in modo da allinearli a quelli del datamatrix delle etichette attraverso l''aggiornamento sul tool "ReagentSetsUpdater", del software InGenius

Eseguire dei test di verifica del tool "ReagentSetsUpdater" aggiornato
Verificare la presenza di altri prodotti con 2 mix
Comunicare ai distributori la modifica apportata attraverso la TAB
Pianificare l''installazione dei file presso tutti i clienti del prodotto MDR-MTB su InGenius
Reagenti Barcode_01
ReagentSetUpdater_01

Messa in uso dei file :
Reagenti Barcode_01
ReagentSetUpdater_01
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica',N'Messa in uso dei file : Reagenti Barcode_01 ReagentSetUpdater_01 - rac20-35_messa_in_uso_ap_file_-_reclamo_n._20_-65_rts120ing.msg
La modifica non è significativa in quanto non riguarda il labelling del prodotto - rac20-35_mod05,36__non_significativa.pdf',NULL,N'14/10/2020
14/10/2020
27/10/2020
27/10/2020
27/10/2020
14/10/2020
14/10/2020',N'16/10/2020
16/10/2020
16/10/2020
29/04/2021
16/10/2020',N'16/10/2020
16/10/2020
16/10/2020
16/10/2020',N'No',N'No',N'No',N'Si attraverso la TAB012',N'No',N'Si in quanto occorre installare presso tutti i clienti del prodotto MDR-MTB su InGenius la nuova versione dei file:
Reagenti Barcode_01
ReagentSetUpdater_01',N'La modifica non impatta sui requisiti regolatori in quanto permette l''acquisizione corretta delle info presenti nel datamatrix delle etichette quindi ripristina una funzionalità ',N'GM - Gestione delle modifiche',N'GM5 - Verifica/individuazione ed approvazione  degli impatti',N'Da valutare incidenza sulla componente probabilità',N'No',N'No in quanto non riguarda il labelling del prodotto',N'No',N'No in quanto non riguarda un prodotto oggetto di contratto con virtual manufacturer',N'Daniela  Atragene',N'No',NULL,NULL,N'Verificare assenza di reclami dovuti ad una mancata acquisizione delle info contenute nel datamatrix delle etichette interne del prodotto MDR-MTB',NULL,NULL,NULL,NULL,NULL,'2021-04-29 00:00:00',NULL,NULL),
    (N'19-23',N'Correttiva','2019-08-01 00:00:00',N'Clelia  Ramello',N'Ricerca e sviluppo',N'Reclamo',N'19-64',N'Software InGenius 1.3',N'Manuale di istruzioni per l''uso
Progetto',N'L''origine dei bug del SW è imputabile a cause diverse. In ogni caso il rilascio del nuovo pacchetto software fa parte della manutenzione del prodotto, parte integrante del ciclo di vita del software come previsto dalla PR05,03 "SVILUPPO E GESTIONE DEL SOFTWARE". Per manutenzione del prodotto si intendono tutte le attività volte a migliorare, estendere e correggere il software nel tempo, dopo la delivery del software al cliente.',N'E'' stata condivisa con il fornitore del software la lista dei bug/change request per valutazione su impatto e effort del relativo fixing. La lista è riportata nel file excel allegato nella sezione PM a questa RAC, inclusa la valutazione di priorità per EG (High, medium and low). Dove presente, viene riportato il link al relativo item Linkomm.
In accordo con la valutazione del fornitore, il nuovo pacchetto software includerà bug/change riportati nella presentazione .ppt allegata nella sezione PM.
',NULL,N'Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attività aggiuntive legate alla verifica e validazione del nuovo pacchetto software
Possibile impatto sulle allocazione delle risorse del team per attivit',N'13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019',N'Stesura TAB, training
Stesura TSB, training
Pianificazione aggiornamento sw presso i clienti
Gestione del progetto in accordo con PR05,03
Aggiornamento IFU, fascicolo tecnico, valutazione notifiche verso autorità
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Attività di verifica e validazione del nuovo pacchetto software
Pianificazione aggiornamento sw presso i clienti',N'REP2020-159 firmato il 4/11/2020, REP2020-158 firmato il 4/11/2020, REP2020-156 firmato il 3/11/2020, REP2020-134 firmato il 15/09/2020, REP2020-042 firmato il 4/11/2020 - rac19-23_reportpermessainsw.txt
tsb063_revb - rac19-23_tsb063_revb.msg
tsb063_reva - rac19-23_tsb_036_-_rev_a_-_sw_version_1.3.0.16.txt
validazione SCRIPT per limitare la formazione di GOCCE di eluato sulla PARETE degli elution TUBE dopo ESTRAZIONE - rep2020-134_report_of_performance_evaluation_of_new_extraction_script_rac19-23_rev00_mp_gs_cr.pdf
risk assessment pss - 20200702_risk_analysis_ingenius_change_elute_collention_eng_doc_pss.pdf
messa In Uso sw InGenius V1.3.0.16 - rap20-22_messa_in_uso_sw_ingenius_v1.3.0.16_2.msg
invio TSB new SW ver. 1.3.0.16 - rap20-22_invio_tsb_new_sw_ver._1.3.0.16.msg
TSB 036 - Rev A - SW Version 1.3.0.16 - rap20-22_tsb_036_-_rev_a_-_sw_version_1.3.0.16_2.txt
tsb063_revb - rac19-23_tsb063_revb_1.msg
Lista bug/change - ingeniusbugeg-pss_evaluation.xlsx
Bug/change selezionati per il nuovo pacchetto sw - ing_1.0_sw_version_1.3.0.13.pptx
si valuta di non far uscire la versione 1.3.0.14, ma attendere la versione 1.3.0.15 richiesta per la modifica del modello 9 richiesta nella RAC20-22 - rac19-23_mod0103_00_release_meeting_ingenius_sw_v_1.3.0.14_rac_19-23_signed.pdf
Release note sw Elite Ingenius versione 1.3.0.15 - releasenoteeliteingeniussw1.3.0.15_9-11-2020.pdf
messa in uso sw Elite Ingenius versione 1.3.0.15 - rac19-23_messa_in_uso_sw_ingenius_v_1.3.0.15.msg
messa In Uso sw InGenius V1.3.0.16 - rac19-23_messa_in_uso_sw_ingenius_v1.3.0.16.msg
elenco documentazione prodotta per la versione 16 - rac19-23_documentiprodottiperlavers16.docx
La notifica è significativa, da notificare nei paesi presso cui il prodotto è registrato. Per USA  verranno analizzati i documenti prodotti al fine di valutarne una sottomissione 510(k) - rac19-23_significativa.pdf
La modifica è significativa. - rac19-23_significativa_1.pdf
Notifica preliminare. - rac19-23_prior_notification_of_change_-sw_int030.msg',NULL,N'12/12/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019
13/11/2019',N'30/04/2020
30/04/2020
29/05/2020
29/05/2020
29/05/2020',N'02/03/2021
02/03/2021
13/01/2021
11/11/2020',N'No',N'nessun impatto',N'Si',N'con la TAB e TSB e con l''avvertenza allegata al IFU',N'Si',N'installazione della nuova versione sw',N'I bug riscontrati dai reclami non sono classificabili come incidenti secondo la definizione riportata sulla PR14,03',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'verificare se ci sono impatti sul risk assessment',N'Si',N'vedere MOD05,36. Per la notifica in USA, sulla base della documentazione prodotta da PSS e da EGSpA, sarà necessario valutare se sottomettere un nuvo 510(k).',N'No',N'No in quanto l''applicazione su Ingenius non si applica ai Virtual Manufacturer',N'Clelia  Ramello',N'No',NULL,'2021-07-31 00:00:00',N'Verificare l''assenza di reclami imputabili ai bug indicati in questa azione correttiva (attenzione: la formazione del SW V1.3.0.16 al personale di campo e la pianificazione dell''installazione presso i clienti è svolta a 01/2021)',NULL,NULL,NULL,NULL,NULL,'2020-05-29 00:00:00',NULL,NULL),
    (N'21-15',N'Preventiva','2021-05-14 00:00:00',N'Maria Lucia Manconi',N'Program Managment',NULL,N'21-12',NULL,N'Nuova revisione di software associati ad IVD',N'L''uso dei prodotti viral load (HIV e HCV) per validazione e demo, sulla stessa piattaforma ELITe InGenius, non permette l''uso dei prodotti che richiedono un''estrazione a partire da 1000 uL. I prodotti HIV e HCV richiedono infatti l''utilizzo dello script di estrazione da 600 uL.
Attualmente l''ELITe InGenius con versione software 1.3.0.16 consente solamente l''estrazione a partire da 1000 uL o 200 uL, mentre l''ELITe InGenius con versione software 1.3.0.16 e la patch 1.3.0.7940 (Mantis Ticket 475 e 477) in combinazione con lo script di estrazione da 600 uL (MV.exctraction.scr), consente solo l''estrazione da 600 uL o 200 uL. L''impossibilità di utilizzare, sullo stesso strumento, i prodotti in associazione allo script di estrazione da 600 uL e da 1000 uL  causa delle situazioni di insoddisfazione del cliente.
Inoltre PSS chiede di mettere in produzione sistemi ELITe InGenius senza PCR cool block, con Windows 10 come sistema operativo e che utilizzano i nuovi dyes da 500nm per la calibrazione delle ottiche. Questi tre cambiamenti saranno inclusi in un''unica patch rilasciata anteriormente alla nuova versione software e tracciata nel CC21-32

Riferimento a: 
CC21-07 (600 uL extraction), 
CC21-26 (Patch Mantis 475 and 477), 
RAC21-12, 
CC21-15 (Windows 10), 
ECR/ECN 159 (Elimination of PCR Cool Block), 
ECR/ECN 161 (Calibrator dye change)
CC21-32 (Patch No PCR Cool Blocl, W10, new dyes)',N'Per far fronte alle necessità sopra descritte si richiede al fornitore PSS di implementare una nuova versione software che includa tutte le funzionalità atte a prevenire problemi sul campo. ',NULL,N'L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.  
L''impatto è positivo, la nuova versione software ingloberà varie funzionalità del sistema necessarie alla sua manutenzione e aggiornamento.',NULL,N'Seguire la RAP garantendo che tutte le attività vengano svolte (in accordo con PR05,03)
Interfacciarsi con il fornitore per la documentazione relativa alla nuova versione software 
Stesura PLAN e REPORT della verifica interna della nuova versione software 
Revisione PLAN e REPORT di installazione della nuova versione software 
Valutazione aggiornamento del documento di bug list su Mantis 
Stesura del Summary Report 
Valutazione dell''aggiornamento dei requisiti software 
Valutazione 510(k)
Attività di verificare e validazione della nuove versione software
Integrazione e Revisione PLAN e REPORT della verifica interna della nuova versione software
Revisione PLAN e REPORT di installazione della nuova versione software
Revisione del Summary Report 
Revisione del documento di bug list su Mantis (se viene fatta come attività SIS)
Revisione valutazione 510(k)
In collaborazione con VAL, aggiornamento IFU delle SP1000 e User Manual
In collaborazione con VAL, valutare eventuali impatti s',NULL,NULL,NULL,N'29/10/2021',NULL,N'No',N'nessun impatto',N'Si',N'si attraverso rilascio nuova versione IFU',N'No',N'nessun impatto',N'-',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'-',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufaturer',N'Maria Lucia Manconi',N'Si',N'da valutare',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-10-29 00:00:00',NULL,NULL),
    (N'17-20',N'Correttiva','2017-09-08 00:00:00',N'Stefania Brun',N'Regolatorio',N'Non conformità',NULL,N'minor NC TDR09/Mi-01 da Report Dekra 2213879-TDR09-R1',NULL,N'Si è ritenuto che quanto scritto in analisi dei rischi fosse sufficiente per la valutazione rischi e benefici complessiva',N'Revisionare le conclusioni della gestione del rischio complessivo riportate sul MOD22,04 di STI ELITe MGB Panel (RTS533ING) ',NULL,N'Revisionare le conclusioni della gestione del rischio complessivo riportate sul MOD22,04 di STI ELITe MGB Panel (RTS533ING) 
Revisionare e approvare la valutazione dei rischi e benefici su MOD22,04  di STI ELITe MGB Panel (RTS533ING) 
Revisionare e approvare la valutazione dei rischi e benefici su MOD22,04  di STI ELITe MGB Panel (RTS533ING) ',N'12/09/2017
12/09/2017
12/09/2017',N'Riportare sui moduli MOD22,04 relativi alla valutazione del rischio residuo complessivo  le reference di pubblicazioni scientifiche a dimostrazione dell''analisi dei rischi/benefici effettuata. 
Il contenuto del MOD22,04 del prodotto STI ELITe MGB Panel (RTS533ING) è stato revisionato (rev.02) esplicitando che sulla base delle metodiche di indagine diagnostica esistenti i test basati sull''amplificazione degli acidi nucleici è quella più sensibile, specifica e veloce ',N'MOD22,04 foirmato 8/9/2017 - rac17-20_mod22,04evaluationofoverallresidualriskacceptability_rts533ing.pdf
mod22,04 - rac17-20_mod22,04evaluationofoverallresidualriskacceptability_rts533ing.pdf',NULL,N'12/09/2017
08/09/2017',N'08/09/2017
29/12/2017',N'08/09/2017
08/09/2017',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,NULL,N'No',NULL,N'No',NULL,N'Sergio  Fazari',N'Si',NULL,'2018-06-30 00:00:00',N'Verificare che le valutazioni del rischio residuo complessivo dei nuovi prodotti in sviluppo abbiano il razionale relativo ad una valutazione dei rischio residuo complessivo commisurata ad una valutazione rischi e benefici basata sulle reference di pubblicazioni scientifiche.
Tale evidenza è stata riportata nelle analisi dei rischi dei prodotti RTK015PLD, RTS553ING, RTS201ING:
- ESBL ELITe MGB Kit (RTS201ING), MOD22,04 rev01 2/2/2018
- Respiratory Bacterial ELITe MGB® Panel (RTS553ING), MOD22,04 rev01 22/12/2017
- CMV ELITe MGB Kit (RTK015PLD),  MOD22,04 rev02 15/11/2017.',N'Stefania Brun',N'Positivo',NULL,NULL,NULL,'2017-12-29 00:00:00','2017-09-08 00:00:00','2018-08-01 00:00:00'),
    (N'17-19',N'Correttiva','2017-09-08 00:00:00',N'Stefania Brun',N'Regolatorio',N'Non conformità',NULL,N'minor NC TDR09/Mi-02 da Report Dekra 2213879-TDR09-R1 ',N'Documentazione SGQ',N'Insufficiente considerazione nell''analisi dei rischi del prodotto STI ELITe MGB Panel di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente',N'Integrare l''analisi dei rischi inserendo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente',NULL,N'Integrare l''analisi dei rischi inserendo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente
Integrare l''analisi dei rischi inserendo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente
Integrare l''analisi dei rischi inserendo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente
Approvare la revisione dell''analisi dei rischi del prodotto STI ELITe MGB Panel (RTS533ING)per quanto riguarda il rischio dovuto a coinfezioni',N'12/09/2017
12/09/2017
12/09/2017
12/09/2017',N'Valutare all''interno dell''analisi dei rischi del prodotto STI ELITe MGB Panel  il rischio di falsi positivi causa presenza di coinfezioni . Riportare sull''IFU le evidenze dei test condotti o la mancata esecuzione degli stessi. Valutare nell''analisi dei rischi dei nuovi prodotti in sviluppo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente
Modificato MOD22,03del prodotto STI ELITe MGB Panel (RTS533ING)   revisione 01, data 08/09/17
Aggiornata l''IFU del prodotto STI ELITe MGB Panel (RTS533ING).
Valutare nell''analisi dei rischi dei nuovi prodotti in sviluppo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente',N' rev.00 del 18/05/2017 nel paragrafo "Limiti della procedura"  "Potenziali interferenze causate da particolari condizioni del paziente, come infezioni del tratto urinario o contaminazione del campione con sperma, potrebbero causare risultati non corretti" - sch_mrts533ing_00.pdf
MOD22,03_RTS553ING, riporta la frase relativa ai marker interferenti ed alla cross-rattività - 4_mod22,03_risk_management_sheet_rts553ing.pdf
MOD22,03 - rac17-19_mod22,03schedagestionerischiorts533ing.pdf
 rev.00 del 18/05/2017 nel paragrafo "Limiti della procedura"  "Potenziali interferenze causate da particolari condizioni del paziente, come infezioni del tratto urinario o contaminazione del campione con sperma, potrebbero causare risultati non corretti" - sch_mrts533ing_00_1.pdf',NULL,N'12/09/2017
08/09/2017
08/09/2017
12/09/2017',N'29/12/2017
08/09/2017
07/12/2017
29/12/2017',N'08/09/2017
08/09/2017
07/12/2017
03/10/2017',N'No',N'-',N'No',N'-',N'No',N'-',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'Alessandra Gallizio',N'Si',N'MOD22,03 ','2018-10-31 00:00:00',N'Valutare nell''analisi dei rischi dei nuovi prodotti in sviluppo il rischio di potenziali interferenze dovute alla presenza di coinfezioni o particolari stati patologici del paziente: MOD22,03 di RTS553ING firmato il 3/10/2017. ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-12-29 00:00:00','2017-12-07 00:00:00','2018-10-31 00:00:00'),
    (N'17-13',N'Correttiva','2017-05-26 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Reclamo',NULL,N'Simone Avanzi',N'Documentazione SGQ',N'1) L''indagine sulla funzionalità del lotto in uso presso il cliente non ha rilevato risultati falsi positivi che evidentemente si presentano con una bassa frequenza .
2) La verifica del disegno del prodotto ha evidenziato che il primer reverse è sovrapposto alla sonda per 6 basi: questo potrebbe favorire delle amplificazioni aspecifiche tardive che generano risultati falsi positivi.
3) Si richiede una modifica del disegno della sonda HSV2 per risolvere il problema delle amplificazioni aspecifiche. 
4) La nuova sonda dovrà anche permettere di neutralizzare la nuova mutazione di HSV2 identificata nella verifica in silico.',N'Studiare un nuovo disegno della sonda HSV2 con una sovrapposizione al primer reverse di massimo 4 basi.
Verificare con test sperimentali con il prodotto con la nuova sonda HSV2:
1) l''equivalenza delle prestazioni rispetto a quelle del prodotto attualmente in uso;
2) l''assenza di amplificazioni aspecifiche (falsi positivi),
3) l''equivalenza delle prestazioni di rilevazione/quantificazione con diluizioni di plasmidi contenenti le sequenze di HSV2 normale e di HSV2 mutato.
In caso di esito positivo dei test, aggiornare il prodotto attualmente in uso, RTS032PLD, con la sonda modificata.',NULL,N'Produzione di lotti pilota con il nuovo design della sonda
Produzione di lotti pilota con il nuovo design della sonda
Effettuazione verifiche in silico
Effettuazione verifiche in silico
Effettuazione verifiche in silico
Acquisto oligonucleotidi per lo studio di fattibilità e per la produzione dei lotti pilota.
Acquisto plasmidi con la sequenza di HSV2 normale e con la mutazione critica.
Aggiornamento anagrafiche.
Produzione lotti pilota con la nuova sonda identificata.
Controllo qualità dei lotti pilota.
Aggiornamento moduli produzione e formazione.
Controllo qualità dei lotti pilota.
Aggiornamento moduli produzione e formazione.
Valutazione dell''impatto sull''analisi dei rischi di prodotto ed eventuale aggiornamento.
Messa in uso dei documenti.
Aggiornamento fascicolo tecnico.
Invio comunicazione ai clienti della disponibilità del prodotto con la nuova sonda.
Valutazione dell''impatto sull''analisi dei rischi di prodotto ed eventuale aggiornamento.
Messa in uso dei documenti.
Ag',N'30/05/2017
30/05/2017
30/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017',N'Effettuazione verifiche in silico.
REP2017-021 "VIS HSV2 ELITE MGB KIT" del 30/05/2017.
REP2017-020 "VIS HSV2 Q PCR Alert ampliMIX" del 30/05/2017.
esecuzione di test sperimentali con diluizioni scalari  di un plasmide che contiene la mutazione riscontrata
Acquisto oligonucleotidi per lo studio di fattibilità e per la produzione dei lotti pilota. Utilizzata la sonda Elitech per il primo lotto pilota, la sonda eurogentec per il secondo.

Acquisto plasmidi con la sequenza di HSV2 normale e con la mutazione critica.

Aggiornamento anagrafiche.
Produzione lotti pilota con la nuova sonda identificata.
09/2017 produzione lotto pilota 1 con la nuova sonda identificata, 10/2017 produzione lotto pilota 2.

Controllo qualità dei lotti pilota.
21/09/2017 esito conforme controllo qualità secondo lotto pilota.
17/10/2017 esito conforme  controllo qualità secondo lotto pilota.
Aggiornamento moduli produzione e formazione.
Valutazione dell''impatto sull''analisi dei rischi di prodotto ed eventuale ag',N'MOD962-rts032pld-taq in uso - rac17-13_messa_in_uso_modrts032taq__1.pdf
MOD962-RTS032PLD REV03 - rac_17-13.pdf
SPEC951-RTS032PLD REV02 - rac17-13_messa_in_uso_spec951-rts032pld_rev02_.pdf
chiusura progetto e autorizzazione lotto U1017BA alla vendita - rac17-13_chiusura_progetto_rts032pld_e_autorizzazione_lotto_u1017ba_alla_vendita.pdf
mail di chiusura progetto - rac17-13_chiusura_progetto_rts032pld_e_autorizzazione_lotto_u1017ba_alla_vendita.pdf
nota informativa del 12/9/17 - rac17-13_2017-09-12_nota_informativa_hsv2_elitech.pdf',NULL,N'30/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017
26/05/2017',N'07/06/2017
30/08/2017
30/11/2017
30/11/2017
30/11/2017
30/11/2017
15/12/2017
30/09/2017
31/08/2017
29/12/2017
29/12/2017',N'30/05/2017
04/08/2017
30/10/2017
30/10/2017
04/10/2017
10/11/2017
13/11/2017
27/09/2017
14/11/2017
10/11/2017',N'No',N'-',N'Si',N'Invio comunicazione per la disponibilità del prodotto con la nuova sonda.',N'No',N'-',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'Nessuna modifica da apportare al VMP e risk assessment',N'No',N'-',N'No',N'-',N'Alessandra Gallizio',N'No',N'Se il progetto di "Manutenzione prodotto" va a buon fine l''attuale stima dei rischi resterà valida e non sarà necessario un aggiornamento dell''analisi del rischio del prodotto.','2019-01-31 00:00:00',N'Il primo lotto prodotto con la nuova sonda è U1017BA, il secondo lotto è U0218AE, il terzo lotto è U0818AQ, si aspettano almeno altri 3 mesi per effettuare l''analisi degli eventuali reclami. A gennaio 2019 non si sono registrati reclami a carico del prodotto.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-12-29 00:00:00','2017-11-14 00:00:00','2019-01-31 00:00:00'),
    (N'17-24',N'Correttiva','2017-10-12 00:00:00',N'Stefania Brun',N'Senior Advisor R&D',N'Reclamo',N'17-5',N'RTSG07PLD210',N'Manuale di istruzioni per l''uso',N'Allestimento della sessione da parte dell''utilizzatore in condizioni di temperatura non controllata 
',N'Modifica dell''IFU del prodotto per inserire che l''allestimento della sessione avvenga in condizioni di temperatura controllata. Inoltre prevedere l''aggiornamento della bibliografia  inserendo i riferimenti alla pubblicazione relativa all''aggiornamento del modello interpretativo dei calcoli',NULL,N'Aggiornare l''IFU del prodotto RTSG07PLD210
Approvazione e rilascio nuova revisione dell''IFU. Aggiornamento fascicolo tecnico (analisi rischi)
Aggiornare l''IFU del prodotto RTSG07PLD210',N'13/10/2017
13/10/2017
13/10/2017',N'Aggiornare l''IFU del prodotto RTSG07PLD210
Approvazione e rilascio nuova revisione dell''IFU. Aggiornamento fascicolo tecnico (analisi rischi)
Aggiornare l''IFU del prodotto RTSG07PLD210',N'rev07 - rac17-24_messa_in_uso_ifu_rtsg07pld210_rev07__1.pdf
aggiornato anche IFU RTSG07PLD190 REV03 - rac17-24_messa_in_uso_ifu_rtsg07pld190_rev03_.pdf',NULL,N'13/10/2017
13/10/2017
13/10/2017',N'30/11/2017
30/01/2018
30/11/2017',N'29/01/2018
23/10/2017',N'No',N'DMRI associato al progetto SCP2019-005 "m.p. One Step Taq MMIX Backup P210 ELITe MGB Kit".',N'No',N'in valutazione',N'No',N'nessun impatto',N'nessun impatto',N'IFU - Gestione Manuali
R&D - Svilupppo e commercializzazione prodotti',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)
R&D1.1 - Fase di Pianificazione: Pianificazione',N'IFU10 nessun impatto; R&D1.1 pianificazione progetto SCP2019-005 "m.p. One Step Taq MMIX Backup P210 ELITe MGB Kit".',N'No',N'in valutazione',N'No',N'non necessaria',N'Stefania Brun',N'Si',N'aggiornamento analisi del rischio','2019-03-26 00:00:00',N'Assenza di reclami.
A 07/2018 persistono reclami dovuti all''allestimento della reazione riconducibili all''assenza del cold blok (vedere allegato A).',N'Roberta  Paviolo',N'Negativo',N'Verificare che nella pianificazione del 2019 sia presente una manutenzione di prodotto che risolva le problematiche eviedenziate. 
Al 18/03/2019 è stato richiesto il codice progetto SCP2019-005 "m.p. One Step Taq MMIX Backup P210 ELITe MGB Kit".
Il progetto si chiude in data 27/11/2019, mail "rilascio lotti per la vendita".',N'12/2019 a chiusura progetto',NULL,'2018-01-30 00:00:00','2018-01-29 00:00:00','2020-01-03 00:00:00'),
    (N'18-2',N'Correttiva','2018-01-10 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Non conformità',N'18-7',N' minor NC AR12/Mi-01',N'Documentazione SGQ',N'1. Until now EGSPA considered appropriate the granularity used to describe verification and validation characteristics into the "high level" development and commercialization procedure (PR 04,01) currently in place. This is because a very detailed description of verification and validation characteristics and activities are available into the project specific Verification plans and reports and Validation plans and reports. Nevertheless, considering the wide scope of the above mentioned procedure which should include instruments and software, in addition to reagent, a more detailed description of Verification and Validation characteristics is desirable (see # 2 below).
2. Design and development of EGSPA instruments and software items has been always outsourced through qualified suppliers in accordance with related procedure in place for selection, qualification and evaluation of the suppliers" (PR06,01).  Design and development steps, including verification and validation, were thoroughly addressed by specific documents shared with the supplier (e.g Quality Agreements, Project plans etc…). That is the reason why the current development and commercialization procedure doesn''t properly address the instrument and software development steps. 
Nevertheless, EGSPA as legal manufacturer of instruments and software is aware of the need to include those items within the development procedures in place.
',N'1a. Revising the current design and development procedure (PR04,01 "Sviluppo e commercializzazione di prodotto" Ed.3 Rev.01) in order to describe in more details characteristics of verification and validation activities.
1b. Training personnel concerned by the change of the PR04,01.

2a. Revising the current design and development procedure (PR04,01 "Sviluppo e commercializzazione di prodotto " Ed.3 Rev.01) in order to describe the management of design and development steps for instruments and software, including verification and validation, even if those activities are outsourced.
2b. Revising the current SW management procedure in place (PR05,03 "Gestione del software" Ed.03, Rev.00) in order to align it with the new revision of the PR04,01 mentioned above.

2c. Revising the current Product validation procedure in place (PR04,04 "Gestione della validazione di prodotto" Ed.03, Rev.00) in order to align it with the new revision of the PR04,01 mentioned above.

2d. Assessing whether a specific (new) procedure to describe instruments development steps control is needed.

2e. Training personnel concerned by the change of the PR04,01, PR05,03, PR04,04 (and new instrument procedure if created).
',NULL,N'A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity
A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity
A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity
A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity
A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity
A positive impact since it will be an improvement of QMS procedures involved in terms of content relating issues objects of the non conformity',N'10/01/2018
10/01/2018
10/01/2018
10/01/2018
10/01/2018
10/01/2018',N'1a. Revising the current design and development procedure (PR04,01 "Sviluppo e commercializzazione di prodotto" Ed.3 Rev.01)
2a. Revising the current design and development procedure (PR04,01 "Sviluppo e commercializzazione di prodotto " Ed.3 Rev.01) in order to describe the management of design and development steps for instruments and software, including verification and validation, even if those activities are outsourced.

2b. Revising the current SW management procedure in place (PR05,03 "Gestione del software" Ed.03, Rev.00) in order to align it with the new revision of the PR04,01 mentioned above.
2c. Revising the current Product validation procedure in place (PR04,04 "Gestione della validazione di prodotto" Ed.03, Rev.00) in order to align it with the new revision of the PR04,01 mentioned above.
2d. Assessing whether a specific (new) procedure to describe instruments development steps control is needed.
2e. Training personnel concerned by the change of the PR04,01, PR05,03, PR04,04 (and new ',N'PR04,01rev02 e documenti allegati - rac18-2_messa_in_uso_pr0401_rev02_e_documenti_allegati_rac18-2.msg
PR04,01rev02 e documenti allegati - rac18-2_messa_in_uso_pr0401_rev02_e_documenti_allegati_rac18-2_1.msg
messa in uso e formazione PR05,03 rev01 - rac18-2_messa_in_uso_pr0503_rev01rac18-2.msg
PR04,01, rev01. Formazione del 8/6/18 (MOD18,02) - rac18-2_messa_in_uso_pr0404_rev01.msg
Aggiornata PR05,03 - rac18-2_messa_in_uso_pr0503_rev01rac18-2_1.msg
MOD18,02 del 6/6/2018 PR04,01rev02 e documenti allegati - rac18-2_messa_in_uso_pr0401_rev02_e_documenti_allegati_rac18-2_2.msg',NULL,N'12/01/2018
10/01/2018
10/01/2018
10/01/2018
10/01/2018
10/01/2018',N'30/03/2018
30/03/2018
30/03/2018
30/03/2018
30/03/2018
30/03/2018',N'14/06/2018
14/06/2018
19/06/2018
08/06/2018
19/06/2018
19/06/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'-',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.13 - Fase di Preparazione del Rilascio sul Mercato: Reportistica',N'R&D1.13',N'No',N'-',N'No',N'-',N'Stefania Brun',N'No',NULL,'2018-05-30 00:00:00',N'Effectiveness verification will be performed during internal audits to R&D, Program Management and Product Validation processes.
Specifically it will be verified:
-	That new instruments and/or new software are developed in compliance with  the new procedure(s) revision of , (no new instrument has been developed after released of new procedure revision; a new software version of Ingenius 1.3.0.10 has been released. Review of technical file update has verified according new revision of PR05,03 
-	knowledge and awareness of personnel involved.
R&D processes verify during internal audit, report n°8-18,17/07/2018 (auditor Catalano)
Program Management and Product Validation processes, report n°11-18, 2/08/2018 (auditor Catalano).
',N'Renata  Catalano',N'Positivo',NULL,NULL,NULL,'2018-03-30 00:00:00','2018-06-19 00:00:00','2018-08-02 00:00:00'),
    (N'17-25',N'Correttiva','2017-10-18 00:00:00',N'Marco Enrietto',N'Validazioni',N'Reclamo',N'17-21',N'assay protocol RTS020PLD',N'Manuale di istruzioni per l''uso',N'La verifica eseguita sull''assay prima del rilascio non ha intercettato l''errore. Questo parametro non è tra quelli verificati ai fini del rilascio.',N'Correzione dell''assay con il valore corretto inerente al ULoQ per le copie / mL
Codifica del modulo usato per registrare la verifica degli assay protocol, riportando tra i parametri oggetto di verifica anche quello inerente al ULoQ e collegati.',NULL,N'Correzione dell''assay con il valore corretto inerente al ULoQ per le copie / mL
Codifica del modulo usato per registrare la verifica degli assay protocol, riportando tra i parametri oggetto di verifica anche quello inerente al ULoQ e collegati.
Comunicazione ai clienti che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto
Comunicazione ai  clienti ed ai distributori /ICO che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto.
Comunicazione ai  clienti ed ai distributori /ICO che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto.',N'18/10/2017
18/10/2017
18/10/2017
18/10/2017
18/10/2017',N'Correzione dell''assay con il valore corretto inerente al ULoQ per le copie / mL
Codifica del modulo usato per registrare la verifica degli assay protocol, riportando tra i parametri oggetto di verifica anche quello inerente al ULoQ e collegati.
Comunicazione ai clienti che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto
Comunicazione ai distributori /ICO che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto.
Comunicazione ai  clienti che hanno questo assay protocol installato su Ingenius e pianificazione sostituzione assay corretto.',N'ASSAY EBV - rac17-25_messa_in_usoassay_ebv_.pdf
IFU EBV rev14 - rac17-25_messa_in_uso_ifu_rts020pld_ebv.pdf
FTP-020PLD AGGIORNATO - rac17-25_sezione_1_product_technical_file_index_rts020pld.docx
TAB17_revA del 13/11/2018 - rac17-25_elite_ingenius_-tab_p17_-_rev_aa_-__product_ebv_elite_mgb_notice_00.pdf',NULL,N'18/10/2017
18/10/2017
18/10/2017
18/10/2017
18/10/2017',N'29/12/2017
29/12/2017
30/03/2018
30/03/2018',N'27/12/2017
27/12/2017
13/11/2018
13/11/2018',N'No',N'non necessario',N'Si',N'Comunicazione ai clienti che hanno questo assay protocol installato su Ingenius (TAB17_revA del 13/11/2018)',N'Si',N'Comunicazione ai clienti che hanno questo assay protocol installato su Ingenius (TAB17_revA del 13/11/2018)',N'-',N'IFU - Gestione Manuali',N'IFU5 - approvazione e messa in uso manuali e assay protocol',N'nessun impatto',N'No',N'non necessaria',N'No',N'non necessaria',N'Marco Enrietto',N'No',NULL,'2019-09-04 00:00:00',N'Dal 27/12/2017 (data di messa in uso dell''assay protocol) a 07/2018 non ci sono stati reclami. TAB17_revA del 13/11/2018 rilasciata a clienti e distributori. A 09/2019 non sono presenti reclami con stessa causa primaria identificata.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-03-30 00:00:00','2018-11-13 00:00:00','2019-09-04 00:00:00'),
    (N'18-11',N'Correttiva','2018-03-16 00:00:00',N'Stefania Brun',N'Validazioni',N'Reclamo',N'18-24',N'RTS175PLD',N'Assay Protocol',N'La causa primaria è imputabile alla definizione dei requisiti di prodotto in quanto non è stato valutato l''inserimento del requisito in fase di pianificazione del progetto.
Il nuovo Assay Protocol (AP rev 04) è caratterizzato effettivamente da un limite superiore di Quantificazione di 10 volte inferiore rispetto a quello del precedente AP rev03 (da 25.000.000 a 2.500.000 virus/ml). Tale dato trova riscontro nelle prestazioni del prodotto descritte nell''IFU del kit (rev12). Questo valore è stato generato da test eseguiti con materiale di riferimento (International Unit) processando campioni simulati ad una concentrazione inferiore a quella teorica riportata nell''assay 03.  Non era possibile preparare campioni più concentrati data la concentrazione di partenza delle unità internazionali.

',N'Revisione del processo di definizione dei requisiti di prodotto per includere richieste ed applicazioni specifiche per i prodotti che lo richiedono
Correzione: Esecuzione di test per estendere il range dinamico di misurazione al fine di verificare il valore massimo effettivamente misurabile su un campione reale, sostituendo il valore teorico presente nell''assay protocol 03
',NULL,N'Revisione del processo di definizione dei requisiti di prodotto per includere richieste ed applicazioni specifiche per i prodotti che lo richiedono. Eseguire i test per la risoluzione del reclamo.
Revisione del processo di definizione dei requisiti di prodotto per includere richieste ed applicazioni specifiche per i prodotti che lo richiedono. Eseguire i test per la risoluzione del reclamo.
Revisione del processo di definizione dei requisiti di prodotto per includere richieste ed applicazioni specifiche per i prodotti che lo richiedono. Eseguire i test per la risoluzione del reclamo.
Revisione del processo di definizione dei requisiti di prodotto per includere richieste ed applicazioni specifiche per i prodotti che lo richiedono. Eseguire i test per la risoluzione del reclamo.
Aggiornamento dell''IFU
Aggiornamento dell''IFU
Aggiornamento dell''IFU
Comunicazione ai clienti del cambiamento relativo al nuovo upper limit of quantification
Necessità di comunicare il cambiamento alle autorità compete',N'16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018',N'Preparazione pannello di diluizione plasmide di BKV in matrice plasma e urine
Esecuzione test e analisi dati
Aggiornamento e verifica dell''assay protocol
Includere le richieste di applicazioni specifiche all''interno dei documenti di pianificazione estensioni d''uso
Aggiornamento dell''IFU
Aggiornamento fascicolo tecnico per analisi dei rischi
Includere nella procedura di validazione prodotto la valutazione dei requisiti legati a richieste di applicazioni specifiche all''interno dei documenti di pianificazione estensioni d''uso
Stesura TAB e invio ai clienti
Verificare necessità di notificare il cambiamento alle autorità competenti',N'ASSAY PROTOCOL BKV - rac18-11_in_uso_ifu_rts175pld_rev13_e_assay.pdf
IFU RTS175PLD rev13 - rac18-11_in_uso_ifu_rts175pld_rev13_e_assay_1.pdf
tab - rac18-11_elite_ingenius_-tab_p02_-_rev_ab_-__product_bkv_elite_mgb_notice.pdf
Si ritiene non necessaria l''aggiornamento dell''analisi dei rischi a fronte dei risultati ottenuti nei test (mail allegata) - rac18-11_aggiornamento_rac.msg',NULL,N'16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018
16/03/2018',N'30/03/2018
16/03/2018
13/04/2018
13/04/2018
30/04/2018',N'30/03/2018
30/03/2018
09/05/2018
30/03/2018
09/05/2018
25/03/2019
13/01/2021
31/08/2018
30/04/2018',N'No',N'nessun impatto',N'Si',N'attraverso la TAB del 31/07/2018',N'No',N'nessun impatto',N'-',N'IFU - Gestione Manuali',N'IFU5 - approvazione e messa in uso manuali e assay protocol',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun virtual maufacturer',NULL,N'Si',N'-',NULL,N'03/2019 possibilità con SGQT EVO di gestire le pianificazioni degli aggiornamenti degli assay attraverso l''apertura di un unico ticket che include tutti i clienti interessati, assegnati a ciascun FPS. 
A 01/2021 non sono stati prodotti ulteriori lotti, e non sono emerse NC relative al lotto U0620-015, si ritiene pertanto possibile chiudere l''azione correttiva senza attendere i 3 lotti di produzione che, verosimilmente alle vendite del prodotto, non verrebbero prodotti se non nell''arco di 1,5 anni (per questo prodotto la pianificazione dei lotti di produzione prevista è di massimo 2 lotti all''anno).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-04-30 00:00:00','2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'17-17',N'Correttiva','2017-08-31 00:00:00',N'Stefania Brun',N'Assistenza strumenti esterni',N'Reclamo',N'17-9',N'Reclamo 17-4; NC17-49',NULL,N'Il fornitore PSS ha riscontrato un difetto in 2 dei canali utilizzati per lo stampo dei piercing tip, lo stampo ha 4 fori di cui 2 sono incriminati.Il difetto comporta la presenza di un foro sul collo superiore dei tip che causa i problemi sopra descritti
(Cavità 1 & 2, si veda foto in allegato prodotto finale).
Il fornitore ha riscontrato un''incidenza del 60 % di pezzi difettosi , il prodotto finale conforme copre solo il 40% del totale (dopo controllo di qualità).
Il 60% è incriminato e viene subito scartato (cavità 1 & 2).
Cavità 3 & 4 (il 40% prodotto buono) viene ancora controllato a mano.
Le due Patch (EU & US) oltre che disabilitare il controllo della pressione durante la prese dei Piercing Tip, 
disabilitano anche il controllo "presenza" PCR Cap durante la fase finale del processo PCR.
(La presa del CAP e trasferimento nel PCR Well).',N'Rilascio da parte del fornitore PSS di una patch nelle 2 versioni EU v.1.2.1-P1.2 e US v.1.2.1.1-P1.2 per eliminare i messaggi di errore sopra descritti ed evitare l''interruzione della routine. Installazione da parte di EGSpA della patch sugli strumenti che hanno evidenziato il problema. Risoluzione da parte di PSS del difetto presente sullo stampo ed avvio della produzione di lotti di piercing tip con il nuovo stampo. ',NULL,N'Eseguire i test di verifica sulle 2 patch rilasciate da PSS
Eseguire i test di verifica sulle 2 patch rilasciate da PSS
Eseguire i test di verifica sulle 2 patch rilasciate da PSS
Eseguire i test di verifica sulle 2 patch rilasciate da PSS
Aggiornamento dell''analisi dei rischi e del FTP. Il fornitore PSS si occuperà di eseguire i test di verifica e invierà relativi report. Richiedere al fornitore PSS evidenza dell''indagine eseguita e della disponibilità dei nuovi lotti di piercing tip 
Aggiornamento dell''analisi dei rischi e del FTP. Il fornitore PSS si occuperà di eseguire i test di verifica e invierà relativi report. Richiedere al fornitore PSS evidenza dell''indagine eseguita e della disponibilità dei nuovi lotti di piercing tip ',N'31/08/2017
31/08/2017
31/08/2017
31/08/2017
31/08/2017
31/08/2017',N' I test eseguiti sulla path nelle 2 versioni US ed EU non hanno evidenziato incompatibilità con il sistema
Installazione patch sugli strumenti che hanno riscontrato tale problematica. Le configurazioni strumentali sono state aggiornate sul SW Sgat
Eseguire i test di verifica sulle 2 patch rilasciate da PSS
Aggiornamento del fascicolo tecnico dell''ELITe InGenius con i report del fornitore PSS relativi ai test di verifica e all''analisi dei rischi
Il fornitore PSS si occuperà di eseguire i test di verifica e invierà relativi report. Richiedere al fornitore PSS evidenza dell''indagine eseguita e della disponibilità dei nuovi lotti di piercing tip ',N'VERIFICATON PLAN PATCH 1.2.1.1, P1.2 - rac17-17_p280062pp054-00_verification_plan_for_sw1.2.1.1-p1.2.pdf
VERIFICATION REPORT PATCH 1.2.1.1-P1.2 - rac17-17_p280062vr616-00_verification_report_for_sw1.2.1.1-p1.2.pdf
verification report geneLEAD PP75 Inspection report - rac17-17_p280062vr572-00_genelead_pp75_inspection_report.pdf
change notice PSS - rac17-17_ei-n-0077en_fitting_force_pp75_signed.pdf
TSB018_NEW PIERCING TIPS - rac17-17_tsb_018_-_rev_a_-_new_piercing_tips.pdf
FTP aggiornato inserendo la ECN086 - rac17-17_elite_ingenius_engineering_change_notice_list_ftp_ecn086.pdf
Avanzamento indagine piercing tips e capping issue - piercing_tips_e_capping_issue_stato_azione_correttiva.docx
Piano di verifica patch sw 1.2.1.1 p.1.2 - p280062pp054-00_verification_plan_for_sw1.2.1.1-p1.2.pdf
Report di verifica SW 1.2.1.1 p.1.2  - p280062vr616-00_verification_report_for_sw1.2.1.1-p1.2.pdf
change notice PSS "Modify the mold of PP75 piercing tip" - rac17-17_ecn_086_-_modified_mold_of_pp75_piercing_tip.pdf
allegtao al change notice del 19/03/2018 - rac17-17_improvement_plan_for_weld_line.pdf
verification report PP75 - rac17-17_p280062vr696-00_pp75_verification_report.pdf',NULL,N'31/08/2017
31/08/2017
31/08/2017
31/08/2017
31/08/2017',N'30/09/2017
30/09/2017
30/09/2017
30/09/2017
30/04/2018',N'10/08/2017
26/07/2018
10/08/2017
15/05/2018
04/04/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'Marcello Pedrazzini',N'Si',NULL,'2019-03-19 00:00:00',N'Nell''arco di circa un anno non si sono verificati reclami con i nuovi lotti di piercing tips (prodotto ref.INT032CS) forniti da PSS dopo l''implementazione della misura correttiva in produzione relativa allo stampo ad 04/2018.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-04-30 00:00:00','2018-07-26 00:00:00','2019-03-19 00:00:00'),
    (N'20-6',N'Correttiva','2020-03-02 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-124',N' RTS078PLD 955-pADN-F41-L-PLD',N'Documentazione SGQ',N'La causa della NC è da ricondursi ad un problema di bassa efficienza del 955-pADN-F41-L-PLD usato come riferimento. La minore efficienza riscontrata nel lotto di riferimento è probabilemente imputabile alla variabilità insita dei lotti e al fatto che il materiale è prossimo alla scadenza.
E'' stato eseguito un test utilizzando come standard l''ADN-B-L-PLD (corrispondente a STD078PLD) e in tale test il lotto di 955-pADN-F41-L-PLD in CQ è quantificato correttamente, mentre il lotto di riferimento risulta essere NON CONFORME rispetto alla quantificazione che è al di sotto del limite di accettazione. In base alla conformità di quantificazione con lo Standard ADN-B-L-PLD, è possibile accettare il lotto del 955-pADN-F41-L-PLD in quanto idoneo per l''uso finale come campione per CQ.
E'' da valutare la possibilità di modificare le modalità CQ eseguendo la quantificazione con il plasmide pAdenoB, che viene preparato e  controllato con maggiore frequenza, inoltre  durante il controllo qualità della mix il campione viene quantificato utilizzando lo standard.',N'Si chiede la modifica delle modalità di controllo dei plasmidi di riferimento per il prodotto RTS078PLD ).

La preparazione dei plasmidi è fatta a ridosso della loro scadenza perchè, essendo poco utilizzati, non è necessario produrre preparazioni intermedie; il controllo avviene, quindi, ogni 2 anni circa. La procedura in uso per la verifica dell''idoneità dei campioni è eseguita anche su due punti di diluizione intermedia non utilizzati per il CQ della mix, e la quantificazione è eseguita utilizzando come curva standard la preparazione in uso del plasmide stesso.

I plasmidi sono utilizzati esclusivamente per il controllo della conformità della mix alle concentrazioni 50000 e 10 copie per reazione e la quantità è calcolata con lo standard del prodotto (pAdenoB).  La modifica consiste nel testare l''idoneità dei plasmidi alle concentrazioni di utilizzo, 50000 e 10 copie per reazione, utilizzando come curva standard pAdenoB per la quantificazione, mentre i criteri di idoneità corrisponderebbero ai criteri di conformità della mix per i rispettivi plasmidi.',NULL,N'La quantificazione eseguita esclusivamente rispetto allo standard potrebbe ridurre la capacità di controllo del plasmide stesso introducendo un passaggio di diluizione da parte di QCT, questo aspetto sarà da monitorare nell''ambito delle OOS di CQ.
Nessun impatto sul processo.
Nessun impatto sul processo.',N'09/03/2020
09/03/2020
09/03/2020',N'Modifica dei moduli:
-MOD10,18 
-MOD10,13-RTS078PLD
-MOD10,12-RTS078PLD
Messa in uso dei nuovi documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'MOD10,12-RTS078PLD_04, MOD10,13-RTS078PLD_02 - approvazione_rac20-6_aggiornamento_mod_cq_adeno_rts0078pld.msg
MOD10,12-RTS078PLD_04, MOD10,13-RTS078PLD_02 - approvazione_rac20-6_aggiornamento_mod_cq_adeno_rts0078pld_1.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-6_mod05,36__non_significativa.pdf',NULL,N'09/03/2020
09/03/2020
02/03/2020',N'30/04/2020',N'28/04/2020
28/04/2020
02/03/2020',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'Il prodotto non subisce alcuna modifica dal punto di vista ddelle prestazionie e sciurezza.',N'CQ - Controllo Qualità prodotti',N'CQ10 - Fase analitica CQ: Preparazione campioni / allestimento delle reazioni',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-12-21 00:00:00',N'Assenza NC con uguale causa primaria entro 6 mesi (almeno un lotto prodotto).
Ad 08/2020 è stato prodotto RTS078PLD-TAQ, lotto U0620-015, si attende di verificare l''assenza di NC fino a 12/2020.
A 01/2021 non sono stati prodotti ulteriori lotti, e non sono emerse NC relative al lotto U0620-015, si ritiene pertanto possibile chiudere l''azione correttiva senza attendere i 3 lotti di produzione che, verosimilmente alle vendite del prodotto, non verrebbero prodotti se non nell''arco di 1,5 anni (per questo prodotto la pianificazione dei lotti di produzione prevista è di massimo 2 lotti all''anno).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-04-30 00:00:00','2020-04-28 00:00:00','2021-01-13 00:00:00'),
    (N'18-12',N'Preventiva','2018-03-21 00:00:00',N'Daniele Salemi',N'Assistenza strumenti interni',NULL,NULL,NULL,NULL,N'In fase di valutazione dei requisiti richiesti al fornitore non è stato specificato come EGSpA intendesse assegnare l''esito delle verifiche "as found": come da norma ISO (analisi della media dei valori ottenuti), oppure come da istruzione operativa interna (analisi di ogni singolo valore).',N'La specifica di assistenza tecnica non è sufficientemente dettagliata (SPEC-MP-DPE, rev.01) nella descrizione dei requisiti di taratura richiesti al fornitore, è necessario un maggior grado di dettaglio su come analizzare i valori ottenuti e quindi su come assegnare la conformità o meno alle verifiche "as found". Aggiornamento specifica assistenza tecnica  (SPEC-MP-DPE, rev.01) specificando di svolgere l''analisi media dei valori in fase di verifica "as found" dello strumento attenendosi perciò alla normativa ISO.',NULL,N'L''analisi dei certificati risulta più snella dovendo rapportare, rispetto ai limiti di accettazione stabiliti da EGSpA, solo un valore (la media) invece che 10 (singole pesate), riducendo i tempi di verifica del certificato.
L''analisi dei certificati risulta più snella dovendo rapportare, rispetto ai limiti di accettazione stabiliti da EGSpA, solo un valore (la media) invece che 10 (singole pesate), riducendo i tempi di verifica del certificato.
Si ritiene che la valutazione dei risultati della verifica as found sul valore medio, invece che sulla singola pesata, non abbia un impatto sulle misure effettuate per preparare il prodotto e quindi sulla qualità dello stesso, in quanto si conforma a quanto stabilito dalla specifica normativa ISO 8655-2:2002
Si ritiene che la valutazione dei risultati della verifica as found sul valore medio, invece che sulla singola pesata, non abbia un impatto sulle misure effettuate per preparare il prodotto e quindi sulla qualità dello stesso, in quanto si conforma a qua',N'21/03/2018
21/03/2018
11/04/2018
11/04/2018
11/04/2018
11/04/2018
21/03/2018',N'Aggiornamento SPEC-MP-DPE, rev.01.
Accettazione dei Certificati
Approvazione SPEC-MP-DPE, rev.02.
Approvazione SPEC-MP-DPE, rev.02
Approvazione SPEC-MP-DPE, rev.02
Approvazione SPEC-MP-DPE, rev.02
messa in uso SPEC-MP-DPE, rev.02',N'SPEC-MP-DPE rev02 - rap18-12_messa_in_uso_spec-mp-dpe_rev02_rap18-12.msg',NULL,N'21/03/2018
14/06/2018
11/04/2018
11/04/2018
11/04/2018
11/04/2018
21/03/2018',N'30/04/2018
30/05/2018
30/05/2018
30/05/2018
30/05/2018
30/05/2018',N'14/06/2018
21/03/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'T - Taratura Esterna Strumenti',NULL,N'TE2 "Pianificazione TE": il cambiamento non modifica il risk assessment.',N'No',NULL,N'No',NULL,N'Daniele Salemi',N'No',NULL,'2018-12-30 00:00:00',N'Nei 6 mesi intercorsi dalla chiusura delle attività non sono emerse probelmatiche all''uso della SPEC-MP-DPE in rev02.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-05-30 00:00:00','2018-06-14 00:00:00','2019-01-08 00:00:00'),
    (N'18-13',N'Correttiva','2018-03-21 00:00:00',N'Stefania Brun',N'Assistenza strumenti esterni',N'Reclamo',NULL,N'Reclami puntali ',N'Progetto',N'Notificato a PSS in data 28/02/2018. L''indagine è stata già avviata da parte di PSS che ha identificato come causa primaria la combinazione tra il pipettatore e i puntali che in maniera random causano la comparsa di errori di dispensazione dovuti alla perdita di aria che influisce sulla pressione di aggancio del puntale.
PSS ha identificato una correzione dell''anomalia e l''ha introdotta nella versione 1.3 del SW:
•	SW v1.3 fornirà un avviso che consentirà all''utilizzatore di approvare i risultati in caso di comparsa random di messaggi di errore in quanto l''attaule versione 1.2 non permette di approvare i risultati in caso di messaggio di errore. 
Come azione correttiva sta valutando la produzione di puntali che sostituiranno gli attuali Axygen in modo da migliorare la compatibilità tra puntale e pipettatore ',N'PSS in collaborazione con EGSpA ha identificato delle soluzioni a breve e medio termine. Le prime relative all''applicazione di HIVAC-G sul single nozzle, e l''aggiunta di o-ring per rinforzare l''aggancio tra il nozzle e il puntale. L''azione a lungo termine è relativa alla realizzazione di puntali ad hoc da parte di PSS in modo da garantire piena compatibilità con lo strumento InGenius ',NULL,N'Positivi in quanto le azioni indentificate andranno a risolvere  gli errori strumentali di errata dispensazione/aspirazione e quelli relativi al rilevamento di falsi clot
Risoluzione definitiva della causa primaria della maggior parte di reclami su InGenius',N'21/03/2018
21/03/2018',N'Il dettaglio delle azioni a carico di EGSpA è riportato nell''allegato "Axygen Tips Action Plan_01032018"
Verifica dell''assenza di reclami ',N'Piano azioni PSS e EGSpA - axygen_tips_action_plan_01032018.xlsx
PROVA 1 CON O-RING, SOLUZIONE SUGLI STRUMENTI INSTALLATI - rac18-13_prova1_b0018e_02.05.2018_test17_singlepressuretest_analysisformat192.pdf
PROVA 1 CON O-RING, SOLUZIONE SUGLI STRUMENTI INSTALLATI - rac18-13_prova2_b0018e_07.05.2018_test_21_singlepressuretest_analysisformat192.pdf
Piano azioni PSS e EGSpA aggiornato al 27/06/2018 - rac18-13_stato_avanzamento_attivita_al_27-06-2018_axygen_tips_action_plan_18.06.2018_june_visit_revision.xlsx
progetto puntali PSS - rac18-13_progetto_puntali_pss_r_action_list_for_aygen_tips_issue.msg
Possibile progetto nuovo single nozzle - rac18-13_possibilita_sviluppo_nuovo_single_nozzle_action_list_for_aygen_tips_issue.msg
L''ipotesi di una produzione dei puntali da parte di PSS è stata scartata a seguito di analisi costi benefici (ipotesi onerosa e di efficacia dubbia per la risoluzione del probelma). - rac18-13_stato_avanzamento_attivita_al_11-12-2018_axygen_tips_action_plan_revision.xlsx
Mail di blocco delle attività legate all''ipotesi di produzione dei puntali da parte di PSS è stata scartata - rac18-13_mailgorreta_stopdevelopmentofsingletips.pdf
Prototipo definitivo di Single Nozzle end RIPROGETTATI, installati e testati  su 3 strumenti R&D (PQ vedere allegati cartacei).  - rac18-13_stato_attivita__luglio_2019.msg
Ordine O-RING selezionati - rac18-13_19-po-01083_ordineoringnuovoprogetto.pdf
TSB050_revA - rac18-13_elite_ingenius_tsb_050_-_rev_a_-_single_nozzle_end_retrofit.msg
La sostituzione del del single nozzle, riprogettato da PSS, sulle macchine installate dai clienti è stato bloccato a seguito del reclamo 20-46 (retrofit sospeso) - rac18-23_stato_a_07-2020_1.msg
elenco strumenti con nuovo single nozzle ad 08/2020 - rac18-13_elenco_ingenius_con_nuovo_single_nozzle_end_al_08-2020.xlsx
il single Nozzle è stato sostitutito nel 75% degli strumenti installati in Italia - rac18-13_singlenozzlesostitutito75strumentiinstallatiinitalia.txt
L''implementazione in campo è in corso da 28.10.2019. Si sta monitorando la situazione, a gennaio 2020 non ci sono ancora dati sufficienti per calcolare il Tempo Medio dall''ultima manutenzione preventiva al guasto (Main Time Before Failure). - rac18-13_statoa01-2020.msg',NULL,N'21/03/2018
22/05/2018',N'30/05/2019',N'29/03/2021
29/03/2021',N'No',N'nessun impatto',N'No',N'valutare',N'Si',N'Per lo strumento oggetto del reclamo s/n B0056 è prevista la sostituzione del single nozzle',N'nessun impato',N'ST - Elenco famiglie Strumenti',N'ST37 - Elite InGenius (STRxx)',N'ST37',N'No',N'da valutare ',N'No',N'nessuna notifica',N'Marcello Pedrazzini',N'Si',NULL,'2021-03-30 00:00:00',N'Verificare che la soluzione a medio termine, ossia l''inserimento di o-ring, risolva la problematica negli strumenti già installati. verificare l''assenza di reclami a seguito di nuova progettazione del single nozzle.
01/2020 (mail Giesen del 07/01/2020) l''implementazione in campo è in corso da 28.10.2019. Si sta monitorando la situazione, a gennaio 2020 non ci sono ancora dati sufficienti per calcolare il Tempo Medio dall''ultima manutenzione preventiva al guasto (Main Time Before Failure). 

7/2020 sostituzione single nozzle bloccato per problemi riscontrati sul nuovo progetto.

03/2021 i problemi riscontrati ad 08/2020 sono stati risolti, attualmente il nuovo single nozzle è installatto, in Italia, circa sul 75% degli strumenti . i distributori sono informati che devono sostituirlo.
Si ritiene possibile chiudere con efficacia positiva questa azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-05-30 00:00:00','2021-03-29 00:00:00','2021-03-29 00:00:00'),
    (N'20-12',N'Correttiva','2020-04-10 00:00:00',N'Lucia  Liofante',N'Ricerca e sviluppo',N'Reclamo',N'20-29',N'GeneFinder COVID-19 Plus RealAmp Kit',N'Progetto
Assay Protocol',N'La causa è da ricondurre ad una caratterizzazione parziale sulla piattaforma Ingenius del prodotto sviluppato dal fabbricante Osang. Pertanto la generazione di segnali aspecifici nei canali CH4 (gene E) e CH5 (Controllo Interno) dovuti a fenomeni di background elevato, con variabilità strumento-dipendente ha comportato risultati invalidi o nel caso del reclamo della Francia (id#) un risultato falso positivo .
',N'1) Completamento della caratterizzazione del prodotto di Osang sulla piattaforma Ingenius con la modifica dei parametri di analisi dell''AP in uso con emissione della revisione 02 recante le seguenti modifiche:
- CH4, gene E: innalzamento Ct Threshold da 50 a 150
- CH5, Controllo Interno: innalzamento Ct Threshold da 50 a 150
- cross-talk matrix: riduzione pari al 2% del segnale del Dye5 su CH4
2) Sviluppo interno a EGSPA di un nuovo prodotto per la rilevazione di COVID-19 (SCP2020-003) validato sulla piattaforma InGenius',NULL,N'Positivo in quanto lo sviluppo interno di un prodotto COVID-19 sulla piattaforma InGenius permette di caratterizzare il prodotto in modo più efficace e garantisce prestazioni ottimali in associazione alla piattaforma InGenius. Impatto in termini di attività legate allo sviluppo del progetto
Positivo in quanto lo sviluppo interno di un prodotto COVID-19 sulla piattaforma InGenius permette di caratterizzare il prodotto in modo più efficace e garantisce prestazioni ottimali in associazione alla piattaforma InGenius. Impatto in termini di attività legate allo sviluppo del progetto
Positivo in quanto lo sviluppo interno di un prodotto COVID-19 sulla piattaforma InGenius permette di caratterizzare il prodotto in modo più efficace e garantisce prestazioni ottimali in associazione alla piattaforma InGenius.
Positivo in quanto lo sviluppo interno di un prodotto COVID-19 sulla piattaforma InGenius permette di caratterizzare il prodotto in modo più efficace e garantisce prestazioni ottimali in associazione a',N'10/04/2020
10/04/2020
10/04/2020
10/04/2020
14/04/2020',N'L''aumento della stringenza dei parametri di analisi su CH4 e CH5 è in grado di  mitigare le problematiche riscontrate; a questo proposito sono state analizzate con macro in via preliminare 40 sessioni del Careggi di Firenze e sessioni selezionate dei clienti di Melegnano, Beclere (Fr) e Bulgaria. 
Non si ritiene al momento giustificabile un intervento a livello degli altri canali, data la bassa incidenza e degli eventi riscontrati.
1) Completamento della caratterizzazione del prodotto di Osang sulla piattaforma Ingenius con la modifica dei parametri di analisi dell''AP in uso con emissione della revisione 02 recante le seguenti modifiche:
- CH4, gene E: innalzamento Ct Threshold da 50 a 150
- CH5, Controllo Interno: innalzamento Ct Threshold da 50 a 150
- cross-talk matrix: riduzione pari al 2% del segnale del Dye5 su CH4
2) Sostituzione del prodotto OSANG con un nuovo prodotto EGSPA per la rilevazione di SARS-CoV-2 (SCP2020-003) validato sulla piattaforma InGenius
Partecipazione alle attività di sv',N'aggiornamento AP - rac20-12_messa_in_uso_ap_open_prodotti_di_terze_parti_rac20-12.pdf
Market release SARS-CoV2 RTS170ING e CTR170ING - rac20-12__egspa_qualita_sgq__market_release_-_sars-cov-2_elite_mgb_kit.pdf
Market release SARS-CoV2 RTS170ING e CTR170ING - rac20-12__egspa_qualita_sgq__market_release_-_sars-cov-2_elite_mgb_kit_1.pdf
nota tecnica indirizzata alla Francia - rac20-12_new_genefinder_ap_note_2020-05-15.pdf
APin uso il 10/04/2020 - rac20-12_messa_in_uso_ap_open_prodotti_di_terze_parti_rac20-12_1.pdf
la modifica degli AP non è considerata significativa - rac20-12_mod05,36_non_significativa_1.pdf',NULL,N'10/04/2020
10/04/2020
10/04/2020
10/04/2020
14/04/2020',N'15/04/2020
30/05/2020
15/04/2020
30/04/2020
30/04/2020
15/04/2020',N'10/04/2020
11/06/2020
11/06/2020
10/04/2020
15/05/2020
13/01/2021
10/04/2020',N'No',N'No in quanto il prodotto è fabbricato da terzi. Per il nuovo progetto  COVID-19 sviluppato internamente ad EGSpA nella documentazione prevista dal DHF verrà rilasciato anche il DMRI',N'Si',N'Si nota tecnica per gli utilizzatori che hanno esposto il reclamo che esponga le modifiche apportate all''AP',N'Si',N'Si, in quanto la nuova revisione dell''AP va a sostituire quella in uso',N'lo sviluppo di un prodotto COVID-19 validato su InGenius garantisce la piena rispondenza ai requisiti regolatori per quanto riguarda la caratterizzazione del saggio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'R&D1.9 da valutare nell''ambito del riesame periodico',N'No',N'vedere MOD05,35',N'No',N'No in quanto non riguarda un prodotto sotto contratto con Virtual Manufacturer',NULL,N'No',NULL,'2020-11-30 00:00:00',N'Verificare l''assenza di reclami imputabili alle prestazioni.
Gennaio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-05-30 00:00:00','2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'19-8',N'Correttiva','2019-04-12 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'18-155',N'RTS032PLD "HSV2 ELITe MGB Kit" ',N'Documentazione SGQ',N'La contaminazione è a livello del lotto 18070084 del fornitore (Elitech INC.) del materiale 951-014 T8-AP593 Dye Calibrator, tuttavia per il fornitore la contaminazione non sussiste in quanto il campione per i loro requisiti risulta negativo (Ct>40), mentre per i ns requisiti risulta basso positivo Ct=undet.
L''azione svolta mitiga il rischio di non rilevare contaminazioni a basso titolo.',N'Al fine di migliorare la rilevazione della contaminazione a basso titolo dei lotti di prodotto EliteMGB, si chiede l''inclusione nel test di contaminazione per il target dei campioni di CI già amplificati per il test di sensibilità. Questo permetterebbe di analizzare 18 campioni negativi anzichè 9, l''eventuale verifica mediante ripetizione passerebbe da 18 a 36 campioni.',NULL,N'La modifica permette di migliorare l''analisi del lotto ma in particolare il processo di indagini CQ collegate a contaminazioni per il target di bassa entità. La modifica riguarda i prodotti Elite MGB Realtime per i quali non è prevista la registrazione di eventuali contaminazioni nei campioni per la sensibilità del controllo interno. Per i prodotti testatai con Ingenius è già prevista l''anlisi di Ct per i target.
La modifica permette di migliorare l''analisi del lotto ma in particolare il processo di indagini CQ collegate a contaminazioni per il target di bassa entità. La modifica riguarda i prodotti Elite MGB Realtime per i quali non è prevista la registrazione di eventuali contaminazioni nei campioni per la sensibilità del controllo interno. Per i prodotti testatai con Ingenius è già prevista l''anlisi di Ct per i target.
La modifica permette di migliorare l''analisi del lotto ma in particolare il processo di indagini CQ collegate a contaminazioni per il target di bassa entità.
La modifica per',N'12/04/2019
12/04/2019
12/04/2019
12/04/2019
12/04/2019',N'Modifica dei moduli di verifica tecnica per i prodotti EliteMGB integrando l''analisi dei campioni per il controllo interno con la verifica dei Ct FAM.
Aggiornamento del RA CQ18
Messa in uso documenti di CQ
Verifica dell''aggiornamento del VMP a seguito di esito del risk assessment CQ18.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'Elenco MOD10,12 da modificare - elenco_doc_da_modificare.xlsx
MOD CQ in uso al 17/10/19 - rac19-8_messa_in_uso_mod_cq_rac19-8_mod1802_del_27092019.msg
i risk assessment del cq verranno rivalutati come da RAP21-3 - rac19-8_in_valutazione_come_da_rap21-3.txt
 MOD CQ in uso al 17/10/19  - rac19-8_messa_in_uso_mod_cq_rac19-8_mod1802_del_27092019_1.msg
i risk assessment del cq verranno rivalutati come da RAP21-3 - rac19-8_in_valutazione_come_da_rap21-3_1.txt
La notifica è significativa, la notifica deve essere svolta esclusivamente presso le autorità competenti che lorichiedono, laddove il prodotto è registrato. - rac19-8_mod05,36_significativa.pdf
Nessuna notifica è stata effettuata in quanto i documenti di cq sono confidenzali e vengono rilasciati solo a seguito di accordi scritti con i distributori. - rac19-8_notifica.txt',NULL,N'12/04/2019
12/04/2019
12/04/2019
12/04/2019',N'30/06/2019',N'17/10/2019
17/10/2019
27/01/2021
06/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Al CQ il prodotto è risultato NC e gestito secondo la PR13,01 gestione delle NC. non ci sono impatti regolatori',N'CQ - Controllo Qualità prodotti',N'CQ18 - Gestione OOS di Produzione',N'CQ18 Gestione OOS di Produzione',N'Si',N'vedere MOD05,36, modifica significativa',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2020-12-21 00:00:00',N'almeno 3 lotti prodotti con materia prima di elitech inc che non presentino la stessa nc rilevata da egspa secondo i requisiti stabiliti.
Lotti di RTS032PLD: U0120-002 e U0720-038 (lotto conforme al CQ, ma con una non conformità (NC20-69) la cui causa primaria è relativa ad un problema di inibizione, differente dal problema di contaminazione di oligo rilevata nella NC18-175). 
Ad 08/2020 si attende la produzione di un terzo lotto . 
A 11/2020 si attende la produzione di un terzo lotto e l''aggiornamento del risk assessment CQ18 con l''aumento della rilevabilità, come da "Riesame annuale e validation master plan, anno 2019" (vedere RAP21-2).',NULL,NULL,NULL,NULL,NULL,'2019-06-30 00:00:00','2021-01-27 00:00:00',NULL),
    (N'20-18',N'Correttiva','2020-06-23 00:00:00',N'Lucia  Liofante',N'Ricerca e sviluppo',N'Reclamo',N'20-37',N'909-EER048050 - DuplicaRT HPA-1A/B Genoty Kit',N'Assay Protocol',N'VALIDAZIONE di PRODOTTO: imputabile ad una caratterizzazione parziale delle prestazioni del prodotto sul sistema Ingenius  ',N'Analisi del database del cliente per ottimizzazione dei parametri di analisi dell''AP
Valutazione nuovi parametri individuati e rilascio nuova revisione dell''AP',NULL,N'Nessun impatto, se non legato alle azioni
Nessun impatto, se non legato alle azioni
Nessun impatto, se non legato alle azioni
Nessun impatto, se non legato alle azioni
Positivo, in quanto risolve la problematica rilevata dal cliente. Non ci sono altri clienti che utilizzano il rpodotto.
Nessun impatto, se non legato alle azioni',N'02/07/2020
02/07/2020
30/06/2020
30/06/2020
02/07/2020
02/07/2020',N'Studio dati cliente e definizione nuovi parametri di analisi
Validazione nuovi parametri di analisi
Studio dati cliente e definizione nuovi parametri di analisi
Validazione nuovi parametri di analisi
Installazione nuova versione AP presso i clienti utilizzatori di 909-EER048050 - DuplicaRT HPA-1A/B Genoty Kit (vedre elenco clienti allegati)
Messa in uso nuova revisione AP',N'con la messa in uso del nuovo ap si definiscono i nuovi parametri di analisi - rac20-18_messa_in_uso_ap_open_prodotti_di_terze_parti.msg
nota al cliente inviata collegata al reclamo 20-37 - nota_esito_indagine_20-37_1.pdf
con la messa in uso del nuovo ap, ha previsto una validazione dei nuovi parametri di analisi - rac20-18_messa_in_uso_ap_open_prodotti_di_terze_parti_1.msg
nota al cliente inviata collegata al reclamo 20-37 - nota_esito_indagine_20-37_2.pdf
nota al cliente inviata collegata al reclamo 20-37 - nota_esito_indagine_20-37_3.pdf
nota al cliente inviata collegata al reclamo 20-37 - nota_esito_indagine_20-37_4.pdf
come da reclamo20-37 Inviata NOTA al cliente e installata la versione 02 dell''AP "EC COAG_HPA_1_WB_Op_200_200" in data 30/06/2020 - rac20-18_installazioneap.txt
la modifica non è significativa, pertanto non è da notificare. - rac20-18_mod05,36__non_significativa_1.pdf
messa in uso nuovo ap - rac20-18_messa_in_uso_ap_open_prodotti_di_terze_parti_2.msg',NULL,N'02/07/2020
02/07/2020
30/06/2020
02/07/2020
02/07/2020
02/07/2020',N'30/06/2020',N'26/06/2020
26/06/2020
30/06/2020
30/06/2020
30/06/2020
23/06/2020',N'No',N'non applicabile, perchè trattasi di un prodotto del fornitore',N'No',N'da valutare',N'No',N'la modifica non ha impatto sul prodotto in commercio',N'da valutare',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'da valutare l''impatto di R&D1.9 a fine anno',N'No',N'da valutare',N'No',N'non applicabile',NULL,N'No',NULL,'2020-12-22 00:00:00',N'valutazione efficacia: assenza di reclami con uguale causa primaria.
A 01/2021 non sono stati registrati altre R con uguale causa primaria, si ritiene pertanto possibile chiudere la RAC',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-06-30 00:00:00','2020-06-30 00:00:00','2021-01-13 00:00:00'),
    (N'19-32',N'Correttiva','2019-10-21 00:00:00',N'Silvia Costa',N'Validazioni',N'Non conformità',NULL,N'CMV ELITe MGB Kit (RTK015PLD); EBV ELITe MGB Kit (RTS020PLD); BKV ELITe MGB Kit (RTS175PLD)',N'Manuale di istruzioni per l''uso',N'Errore nel riportare i dati e mancato controllo in seguito alla stesure delle IFU.',N'CORREZIONE: aggiornamento ed allineamento di IFU e Report di verifica e validazione. 
AZIONE CORRETTIVA: valutare se è possibile migliorare il processo di revisione delle IFU',NULL,N'I disallinemaneti dei dati individuati non hanno impatto sulle performance, permatanto è sufficiente l''avvertenza allegata lla IFU.
Valutare che i cambiamenti sulle performance non abbiano un impatto sulle gare in essere
verificare se necessario l''aggiornamento del materiale promozionale e delle presentazioni.
Migliorare l''accuretezza del controllo delle IFU riduce il rischio di rilasciare un manuale di istruzioni per l''uso con informazioni errate, mancanti o non chiare.
Migliorare l''accuretezza del controllo delle IFU riduce il rischio di rilasciare un manuale di istruzioni per l''uso con informazioni errate, mancanti o non chiare.
nessun impatto.
nessun impatto se non in termini di attività.',N'21/10/2019
21/10/2019
21/10/2019
21/10/2019
21/10/2019
21/10/2019',N'Informare gli FPS sulle modifiche attraverso la messa inuso delle IFU.
Aggiornamento del materiale promozionale e delle presentazioni.
Aggiornare la "PR05,04_02_Gestione Manuali d''Istruzione per l''Uso e documenti correlati" con le funzioni corrette e verificare se e come migliorare l''accuratezza del controllo delle IFU.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Allineare report di verifica e validazione e IFU',N'Report aggiunti al FTP di EBV per la RAC19-32: - PVR2014-035-00 Rev.02 del 07.11.2019 - PVR2014-035-01 Rev.02 del 07.11.2019 - rac19-32_aggiornamento_ftp.txt
 messa in uso IFU CMV_it_en rev16 e AP  - rac19-32_ifu_rtk015pld_nc19-112;_rac19-32_-_ap_rtk015pld_rac19-29_1.msg
 messa in uso IFU CMV_it_en rev16 e AP  - rac19-32_ifu_rtk015pld_nc19-112;_rac19-32_-_ap_rtk015pld_rac19-29.msg
 messa in uso IFU EBV_RTS020pld rev17, CTR020PLD rev14 e AP  - rac19-32_ifu_rts020pld_ctr020pld_rac19-32;_cc20-25.msg
la modifica non è significativa, per tanto non è da notificare presso le autorità comptenti. - rac19-32_mod05,36_non_significativa.pdf',NULL,N'21/10/2019
21/10/2019
21/10/2019
21/10/2019
21/10/2019',N'30/06/2020
21/03/2020
31/01/2020',N'21/10/2019',N'No',N'nessun impatto',N'Si',N'la comunicazione al cliente può essere fatta con l''avvertenza presente nelle IFU',N'No',N'nessun impatto',N'il prodotto non subisce alterazioni delle performance in quanto i dati presenti nei report rimangono invariati.',N'IFU - Gestione Manuali
IFU - Gestione Manuali',N'IFU1 - stesura manuale
IFU2 - riesame manuale',N'verificare come migliorare il processo di gestione delle IFU',N'No',N'vedere MOD05,36. La modifica non è significativa in quanto i report, sottoposti all''organismo notificato, contenevano i dati corretti, inoltre le modifiche non hanno impatto nè sulle prestazioni, nè sulla sicurezza ed efficacia del prodotto.',N'No',N' La modifica non è significativa in quanto i report contenevano i dati corretti, inoltre le modifiche non hanno impatto nè sulle prestazioni, nè sulla sicurezza ed efficacia del prodotto. Inoltre i valori da aggiornare per il targhet CMV non fanno parte dell''uso previsto del prodotto -Biopharma.',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-06-30 00:00:00',NULL,NULL),
    (N'20-29',N'Correttiva','2020-09-09 00:00:00',N'Maria Galluzzo',N'Fornitore',N'Reclamo',N'20-20',N'RTSD00ING',N'Nuova revisione di software associati ad IVD',N'Il cliente ha ottenuto alcuni risultati invalidi per il Fattore II con errore 30103 "Ct Calculation Error" che richiedono la verifica e l''approvazione da parte dell''operatore ma di fatto il sistema non permette di effettuare l''approvazione; il cliente è costretto a ripetere il campione con una nuova seduta nonostante sia presente il picco della Temperatura di Melting corretto per il Fattore II ed il Ct del Fattore V rientri nei limiti di accettazione impostati.
Considerando che l''interpretazione del risultato del Fattore II (ed anche del fattore MTHFR) è basata solo sulla sua curva di Melting e non sul Ct determinato e alla luce della problematica emersa presso il cliente, si segnala la necessità di una richiesta di implementazione a PSS del modello interpretativo 11 (utilizzato dal Software InGenius per il prodotto RTSD00ING) che consenta l''approvazione dell''errore 30103. ',N'Richiesta a PSS di implementazione del modello interpretativo 11 per l''approvazione dell'' "Error Code 30103" quando è presente il picco della Temperatura di Melting corretto per il Fattore II e per il fattore MTHFR ed il Ct del Fattore V rientra nei limiti di accettazione impostati.',NULL,N'Positivo poichè il cambiamento permetterà al cliente di non  dover ripetere il campione data la presenza del picco della Temperatura di Melting corretto per il Fattore II ed dell Ct del Fattore V che rientra nei limiti di accettazione impostati nell''AP. 
I tempi per l''attuazione della modifica non sono ancora pianificati. L''errore che appare al cliente è bloccante di eventuali falsi risultati e costringe il cliente a ripetere il campione, è pertanto possibile attendere il tempo necessario per la prossima revisione sapendo che non ci sono impatti sul prodotto e possibili falsi risultati.
Positivo poichè il cambiamento permetterà al cliente di non  dover ripetere il campione data la presenza del picco della Temperatura di Melting corretto per il Fattore II ed dell Ct del Fattore V che rientra nei limiti di accettazione impostati nell''AP. 
I tempi per l''attuazione della modifica non sono ancora pianificati. L''errore che appare al cliente è bloccante di eventuali falsi risultati e costringe il cliente ',N'18/09/2020
18/09/2020
18/09/2020
18/09/2020
18/09/2020
18/09/2020
18/09/2020',N'Comunicazione al cliente tramite TAB a cambiamento effettuato.
Eventuale aggiornamento del Fascicolo del prodotto INT030 con la documentazione relativa all''implementazione del modello interpretativo 11
Richiedere a PSS l''implementazione del modello interpretativo 11 per l''approvazione dell'' "Error Code 30103", fixing alla prossima release del sw non ancora pianificata a 09/2020 in quanto in corso di rilascio la versione 1.3.015 prevista per  10/2020.
Apertura Mantis a PSS e valutazione dei potenziali impatti prevedebili della modifica richiesta e di un test di verifica',N'TAB P12 - Rev AF - Product Coagulation ELITe MGB - rac20-29_messa_in_uso_tab__p12_-_rev_af_-_product_coagulation_elite_mgb.msg
messa inu so AP CE-IVD rev.05 - rac20-29_messa_in_uso_nuova_rev._ap_rtsd00ing_1.msg
messa in uso  MOD10,12-RTSD00ING - rac20-29_messa_in_uso_mod1012-rtsd00ing-ctrd00ing.msg
I cambiamenti di questi Assay Protocol si sono resi necessari in seguito al rilascio della nuova versione del software ELITe InGenius 1.3.0.16 e in risposta alla RAC20-29 collegata al Reclamo Clienti 20-20 - rac20-29_messa_in_uso_nuova_rev._ap_rtsd00ing.msg',NULL,N'18/09/2020
18/09/2020
18/09/2020
18/09/2020',N'30/06/2021
30/06/2021
30/06/2021
30/09/2020',NULL,N'No',N'No in quanto il modello interpretativo non è parte della documentazione di DMRI',N'No',N'Si per comunicare ai clienti che si sono imbattuti nell''errore la risoluzione apportata attraverso la modifica del modello interpretativo',N'No',N'No',N'La modifica del modello interpretativo non impatta sui requisiti regolatori in quanto non altera il profilo di rischio del prodotto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'valutazione a fine anno sul RA R&D1.9',N'No',N'da definire a seguito della valutazione della significatività',N'No',N'No in quanto non riguarda prodotti in OEM',N'Maria Galluzzo',N'No',NULL,NULL,N'Valutare a modifica avvenuta l''assenza di reclami imputabili a tale problematica',NULL,NULL,NULL,NULL,NULL,'2021-06-30 00:00:00',NULL,NULL),
    (N'17-10',N'Preventiva','2017-04-20 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',NULL,N'950-168 lotto 1859169-171',N'Materia prima',N'Il lotto di materia prima Tfi Mastermix che sarà utilizzata per i prossimi lotti di prodotti Elite, risulta meno performante con il prodotto CMV, è necessario verificare per tempo le prestazioni con gli altri prodotti in pianificazione',N'valutazione delle prestazioni con preCQ e test Ingenius',NULL,N'Possibili prodotti non conformi o al limite delle prestazioni
Possibili prodotti non conformi o al limite delle prestazioni
Test funzionale delle prestazioni dei preCQ
Test funzionale delle prestazioni dei preCQ
Gestione di deviazioni o eventuali NC',N'26/04/2017
26/04/2017
26/04/2017
26/04/2017
26/04/2017',N'elenco dei prodotti da produrre utilizzando Tfi Mastermix
preparazione dei preCQ con il lotto oggetto dell''azione con sufficiente anticipo, al fine di poter gestire eventuali deviazioni
Test CQ 
in base allle azioni precedenti',N'PIANIFICAZIONE - stato_plan_tfi_a_giugno2017.xlsx
Elenco del risultato dei test - rac17-10_plan_tfi_26_04_2017.xlsx',NULL,N'26/04/2017
26/04/2017
26/04/2017',N'30/05/2017
30/06/2017
30/09/2017',N'30/05/2017
07/12/2017
13/05/2019',N'No',N'Aggiornati, laddove necessario, insieme ai progetti di conversione TAQ',N'No',N'non necessaria',N'No',N'nessuno',N'nessun impatto sui requisiti regolatori',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.3 - Identificazione materiali, semilavorati e consumabili',N'nessun impatto sul risk assessment',N'Si',N'Notice of Change CMV 30/03/2016',N'No',N'messun virtual manufacturer interessato',N'Federica Farinazzo',N'No',N'non necessario','2019-04-30 00:00:00',N'TUTTI I PRODOTTI CON LA TFI SONO STATI CONVERTITI A TAQ, L''ULTIMO PRODOTTO CONVERTITO A TAQ è C. DIFFICILE, CHIUSO IN DATA 13/05/2019 (SCP2018-022).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-09-30 00:00:00','2019-05-13 00:00:00','2019-05-13 00:00:00'),
    (N'17-9',N'Correttiva','2017-04-19 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',NULL,N'RTK015PLD',NULL,N'La causa è da riferirsi alle nuove modalità di analisi con soglia a 0,2FAM e alla tendenza del prodotto TAq ad avere un Ct del punto 1e5 tendenzialemnte più elevato rispetto ai lotti Tfi.',N'- Valutazione da parte di R&D della possibilità di aumentare l''efficienza del prodotto, senza apportare modifiche importanti alla formulazione
- Modifica dei criteri di CQ da Ct 24 a Ct 24,5 (in seguito a verifica da parte di R&D di possibili aumetni dell''efficienza)
- Monitoraggio dei lotti
- Monitoraggio di reclami e segnalazioni.',NULL,N'Valutazione da parte di R&D della possibilità di aumentare l''efficienza del prodotto, senza apportare modifiche importanti alla formulazione
Modifica dei criteri di CQ da Ct 24 a Ct 24,5 Monitoraggio dei lotti
Monitoraggio segnalazioni e reclami presso i clienti
Monitoraggio segnalazioni e reclami presso i clienti',N'27/04/2017
27/04/2017
27/04/2017
27/04/2017',N'Valutazione da parte di R&D della possibilità di aumentare l''efficienza del prodotto, senza apportare modifiche importanti alla formulazione
Modifica dei criteri di CQ da Ct 24 a Ct 24,5 (in seguito a verifica da parte di R&D di possibili aumetni dell''efficienza) nei moduli di CMV.
Formazione al personale, eseguita in data 5/5/2017 (MOD18,02).
eseguito CQ sul lotto U0517BC 
Monitoraggio segnalazioni e reclami presso i clienti. Il monitoraggio è stato prolungato fino a dicembre al fine di ottenere un maggior numero di dati: non sono presenti reclami su CMV.
Messa in uso documenti',N'analisi Ct Elite - ct-std-5_after-02.xlsx
messa in uso - rac17-9_messa_in_uso_mod1012_.pdf',NULL,N'27/04/2017
27/04/2017
27/04/2017
27/04/2017',N'05/05/2017
05/06/2017
30/09/2017
30/05/2017',N'05/05/2017
22/05/2017
07/12/2017
22/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'CQ - Controllo Qualità prodotti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'Federica Farinazzo',N'No',N'non necessario','2017-12-07 00:00:00',N'Nel piano di azioni definitivo era già presente l''attività di monitoraggio delle segnalazioni e dei reclami dai clienti: non sono presenti  reclami su CMV.',N'Stefania Brun',N'Positivo',NULL,NULL,NULL,'2017-09-30 00:00:00','2017-12-07 00:00:00','2017-12-07 00:00:00'),
    (N'18-18',N'Preventiva','2018-06-13 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Errori nell''attribuzione della scadenza deriverebbero per la maggior parte dei casi da scadenze diverse per i diversi prodotti.',N'Uniformare le scadenze di CTR a 24 mesi.',NULL,N'Uniformando l''assegnazione delle scadenza a 24 mesi per CTR come per gli STD si riduce la possibilità di errore legata alla diversa tipologia di prodotto. In base ai dati a disposizione non vi è alcun impatto sulla stabilità dei prodotti.
Uniformando l''assegnazione delle scadenza a 24 mesi per CTR come per gli STD si riduce la possibilità di errore legata alla diversa tipologia di prodotto. In base ai dati a disposizione non vi è alcun impatto sulla stabilità dei prodotti.
Uniformando l''assegnazione delle scadenza a 24 mesi per CTR come per gli STD si riduce la possibilità di errore legata alla diversa tipologia di prodotto. In base ai dati a disposizione non vi è alcun impatto sulla stabilità dei prodotti.
L''assegnazione della scadenza a 24 mesi per tutti i prodotti CTr non ha impatto sul processo CQ. Gli impatti sono collegati alle attività da svolgere di DS per la modifica dei circa 50 documenti .
Una più efficace comunicazione della scadenza da assegnare, soprattutto laddove ci possano e',N'14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018',N'Aggiornare la PR04,03 nelle regole stabilite per l''assegnazione della scadenza.
Indicare chiaramente nei report di stabilità al paragrafo "Conclusion" le conclusioni del test e la "shelf-life" assegnata al prodotto.
Formare gli RT per la stesura del Report conclusivo della fase di sviluppo e del piano di Design Transfer che devono mettere in luce la scadenza da assegnare al prodotto in base ai risultati di stabilità dei lotti di sviluppo. Formare gli RT affinché queste informazioni siano  trasferite al Document Specialist durante il Design Transfer. La scadenza deve essere sempre presente sui moduli in revisione prototipo.
Pianificazione ed aggiornamento di tutti i moduli MOD962-CTRXXX
inserita per errore da eliminare
Comunicazione dell''uniformità delle scadenze presso i paesi in cui i prodotti sono registrati
Approvazione dei MOD962-CTRxxx  con la scadenza massima aggiornata
Messa in uso dei MOD962-CTRxxx con la scadenza massima aggiornata',N'pr04,03_02 e doc collegati - rap18-18__messa_in_uso_pr0403_02_doc_collegati_prdyyyxxx_mrdyyyxxx_rap19-13_cc18-18.msg
Non è approvata l''indicazione nei report di stabilità al paragrafo "Conclusion" le conclusioni del test e la "shelf-life" assegnata al prodotto. - rap18-18_attivita_r&d_non_approvata.txt
doc in modifica - rap18-18_scadmax24mm.xlsx
messa in uso ctr multipli, formazione del 30/7/18 - rap18-18_messa_in_uso_mod962-ctr-xxx_2.msg
messa in uso ctr alert, formazione del 30/7/18  - rap18-18_messa_in_uso_mod962_ctr_alert_rap18-18.msg
messa in uso ctr eleite, formazone del 30/07/18 - rap18-18_messa_in_uso_moduli_ctr_elite_scadenza_24_mesi_rap18-18_cc12-17.msg
messa in uso MOD962-CTRxx multipli, formazione del 30/07/2018 - rap18-18_messa_in_uso_mod962-ctr-xxx_1.msg
 messa in uso moduli CTR alert  - rap18-18_messa_in_uso_mod962_ctr_alert_rap18-18_1.msg
 messa in uso moduli CTR elite - rap18-18_messa_in_uso_moduli_ctr_elite_scadenza_24_mesi_rap18-18_cc12-17_1.msg
messa in uso moduli ctr-multipli - rap18-18_messa_in_uso_mod962-ctr-xxx.msg
messa in uso moduli CTR alert - rap18-18_messa_in_uso_mod962_ctr_alert_rap18-18.msg
messa in uso CTR elite - rap18-18_messa_in_uso_moduli_ctr_elite_scadenza_24_mesi_rap18-18_cc12-17.msg',NULL,N'14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018
14/06/2018',N'30/09/2018
14/06/2018',N'25/10/2019
25/10/2019
25/10/2019
23/10/2018
23/10/2018
23/10/2018
23/10/2018',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.6 - Fase di Sviluppo: Pianificazione',N'R&D1.6',N'No',N'non necessaria',N'No',N'non necessaria',NULL,N'Si',NULL,'2019-12-30 00:00:00',N'Non si sono verificate NC relative all''attribuzione della scadenza. tutte le estensioni d''uso sono gestite attraverso Change Control nei quali si dettaglia l''indicazione dei mesi di scadenza da riportare sui mduli di produzione.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-09-30 00:00:00','2019-10-25 00:00:00','2020-01-03 00:00:00'),
    (N'19-25',N'Correttiva','2019-09-05 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',N'19-99',N'Preparazione reagenti: dispensazione oligo',N'Processo di produzione',N'In base alla verifica dei moduli di utilizzo degli oligo per i controlli interni MOD09,33_Elenco stoccaggio aliquote oligonucleotidi, è stato evidenziato un errore di volume utilizzato nella preparazione del primers 25 µM rtPLD IC2 R. Il volume utilizzato risulta essere 265µL anzichè 429µL, la verifica dei volumi delle aliquote ha confermato quanto registrato nel documento. La verifica sperimentale, aggiungendo il volume mancante ha dato esito conforme e assolutamente in linea con le prestazione del preCQ.',N'Si chiede di rilavorare il lotto aggiungendo il volume di oligo 25 µM rtPLD IC2 R in difetto. Si chiede inoltre di rivedere la procedura di dispensazione degli oligo durante la fase di produzione al fine di standardizzarla maggiormente, aggiungendo uno step di controllo dei volumi, pianificazione e registrazione del numero di disensazioni per reagente. ',NULL,N'E'' ncessario valutare l''impatto in base alle modalità di rilavorazione che saranno stabilite nell''ambito della RAC. Per quanto riguarda la modifica della procedura richiesta ha un impatto positivo sul proesso di produzione in quanto in grado di monitorare e standardizzare meglio i processi.
E'' ncessario valutare l''impatto in base alle modalità di rilavorazione che saranno stabilite nell''ambito della RAC. Per quanto riguarda la modifica della procedura richiesta ha un impatto positivo sul proesso di produzione in quanto in grado di monitorare e standardizzare meglio i processi.
E'' ncessario valutare l''impatto in base alle modalità di rilavorazione che saranno stabilite nell''ambito della RAC. Per quanto riguarda la modifica della procedura richiesta ha un impatto positivo sul proesso di produzione in quanto in grado di monitorare e standardizzare meglio i processi.
Il miglioramento della procedura avrebbe un impatto positivo in quanto dovrebbe ridurre le non conformità collegate a dispensazioni errate',N'05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019',N'Approvazione della procedura di rilavorazione
aggiornamento del risk assessment P1.6
Effettuare la rilavorazione del lotto secondo la procedura di rilavorazione approvata ed a seguito di formazione
Preparare la procedura di rilavorazione 
Esecuzione del CQ del lotto rilavorato
(I documenti cartacei sono archiviati nell''ufficio CQ archivio RAC2019)
Modifica dei moduli di produzione (da definire in base tipo di modifica da attuare)
Informare, formare il personale di prodzuione sulla procedura di rilavorazione approvata
Come richiesto dalla normativa vigente la "procedura di rilavorazione"  deve tener conto:
- degli eventuali effetti avversi causati dalla rilavorazione (per esempio deve valutare se lo scongelamento del prodotto per la rilavorazione influisce sull''aggiornamento della scadenza, ed in tal caso esplicitare la necessità di ri-etichettatura dei tubi), 
- della necessità di effettuare una ri-verifica sul lotto rilavorato al fine di dimostrare la conformità ai requisiti iniziali e dimo',N'PROCEDURA RI-LAVORAZIONE - rac19-25_nc19-99_plan2019-028_firmato_pianorilavorazoinelottou0819au.pdf
plan2019-028 - pla2019-028_firmato.pdf
scheda rilavorazione-firmata - scheda_rilavorazione_lotto_firmata.pdf
scheda formazioen - mod18,02_scheda_formazione_rilavorazione.pdf
rep2019-085 - rep2019-085-report_rilavorazione-u0819au_rac19-25-nc19-99_firmato.pdf
Scheda rilavorazione compilata - scheda_rilavorazione_lotto_u0819au_compilata.pdf
aggiornamento moduli come da CC20-79 - cc20-79_messa_in_uso_mod962-rtsxxxing.msg
aggiornamento moduli come da CC20-79 - cc20-79_mod962-rts400ing_02.msg
aggiornamento moduli come da CC20-79 - cc20-79_moduli_prod_3.msg
Report rilavorazione firmato il 12/09/19 - rac19-25-nc19-99_rep2019-085-report_rilavorazione-u0819au_firmato.pdf
Report rilavorazione, firmato il 12/09/19 - rac19-25-nc19-99_rep2019-085-report_rilavorazione-u0819au_firmato_1.pdf
 Report rilavorazione, firmato il 12/09/19  - rac19-25-nc19-99_rep2019-085-report_rilavorazione-u0819au_firmato_2.pdf
La modifica non è significativa, pertanto non è da notificare alle autorità competenti  - rac19-25_mod05,36_non_significativa.pdf
aggiornamento moduli come da CC20-79 - rac19-25_aggiornamento_moduli_come_da_cc20-79.txt
RA non da modificare - rac19-25_ranondamodificare.txt
RA non da modificare - rac19-25_ranondamodificare_1.txt
MOD18,02 del 9/9/19 - rac19-25_mod18,02_2019-09-09_pt-pp_rilavorazionertst01pld-u0819au.pdf',NULL,N'05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019
05/09/2019',N'09/09/2019
13/09/2019
13/09/2019
13/09/2019
13/09/2019
13/09/2019
13/09/2019
30/09/2019',N'09/09/2019
04/03/2021
09/09/2019
28/04/2021
09/09/2019
12/09/2019
11/09/2019
12/09/2019
12/09/2019
28/04/2021
04/03/2021
26/09/2019
26/09/2019',N'No',N'da valutare in base ai dcumenti prodotti',N'No',N'non necessaria perchè il lotto non è stato immesso in commercio',N'No',N'non necessaria perchè il lotto non è stato immesso in commercio',N'Nel rispetto del requisitio normativo (paragrafo 8.3.4 norma ISO13485:2016) la rilavorazione del semilavorato, anche se non prevista dalla procedura di produzione in uso (PR09,01, rev 03), verrà svolta secondo una procedura scritta "ad hoc" per il prodotto RTST01PLD ed utilizzata solo in questo caso al fine di garantire la continuità di fornitura. 
E'' stato valutato che la rivolazione del semilavorato non introduce potenziali effetti avversi, in quanto sono mantenuti i requisiti di accettazione del prodotto a seguito di rilavorazione. E'' stato valutato che, rispettando le tempistiche di scongelamento e lavorazione descritte nel MOD962-RTST01PLD, ed applicando le regole di diminuzione della scadenza, le caratteristiche di performance e sicurezza del prodotto dopo la rilavorazione non subiscono alterazioni.',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.6 - Etichettatura semilavorati e componenti',N'da valutare ',N'No',N'vedere MOD05,36',N'No',N'vedere MOD05,36',NULL,N'No',NULL,'2021-10-29 00:00:00',N'La revisione della procedura di dispensazione degli oligo, durante la fase di produzione, al fine di standardizzarla maggiormente, aggiungendo uno step di controllo dei volumi, pianificazione e registrazione del numero di dispensazioni per reagente è stata apportata nel CC20-79 con l''introduzione di un modello generico da cui partire per la stesura delle schede di produzione dei prodotti Elite Ingenius che permetta di uniformare gli step di produzione e i tempi di esposizione a temperatura ambiente, con relativi cicli di dispensazione e confezionamento, per i prodotti con analogo processo.  Questi modelli sono stati messi in uso a 04/2021, è in corso la stabilità.
O7/05/2021 effettuati i controlli di stabilità del lotto di RTST01PLD rilavorato (da U0819AU a U0919AO), a 12 e 18 mesi: i test hanno dato esiti conformi. Da una analisi dei Ct, risulta che il lotto è stabile e non perde in termini di amplificabilità e sensibilità del controllo interno.

verificare l''assenza di NC con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2019-09-30 00:00:00','2021-04-28 00:00:00',NULL),
    (N'18-32',N'Correttiva','2018-10-22 00:00:00',N'Stefania Brun',N'Risorse Umane',N'Non conformità',NULL,N'Definizione requisiti e competenze nuove funzioni ',N'Documentazione SGQ',N'Secondo il MOD18,01 la Job Description di HRP prevede lo svolgimento di  attività riconducibili a 2 mansioni generali: Payroll/Amministrazione personale e  Gestione Risorse Umane e Formazione. Le attività legate alla Gestione Risorse Umane e Formazione, a causa della priorità assegnata alla mansione di Payroll/Amministrazione personale, sono svolte da HRP in modo saltuario e in maniera incompleta. La documentazione oggetto della presente NC (MOD18,01, ORG EG SpA, MOD18,06, MOD01,01), il cui aggiornamento è richiesto a seguito di ogni cambiamento dalla procedura PR18,01, è parte della documentazione di Gestione Risorse Umane e Formazione e, come riportato sopra, non è aggiornata da HRP secondo i tempi richiesti dalla procedura. E'' in corso un''analisi del processo in essere relativo alla Gestione Risorse Umane e Formazione valutando attività, tempi di svolgimento richiesti e responsabilità/risorse. L''esito dell''analisi dovrà portare ad una pianificazione dei tempi/modi per recuperare le attività arretrate e ad una revisione del processo in modo che sia rispondente ai requisiti normativi in modo sostenibile con le risorse allocate (termine previsto Q1 2019: a 03/2019 è stata introdotta la nuova funzione di HR Specialist)',N'CORREZIONE: aggiornare mansionario MOD18,01 e organigramma in modo da riflettere la struttura organizzativa attuale di EGSpA
AZIONE CORRETTIVA: effettuare un''analisi del processo relativo alla Gestione Risorse Umane e Formazione valutando attività, tempi di svolgimento richiesti e responsabilità/risorse a seguito di integrazione nel processo della nuova funzzione di HR Specialist (da 03/2019). L''esito dell''analisi dovrà portare ad una pianificazione per recuperare le attività arretrate e d all''aggiornamento della PR18,01',NULL,N'Positivi in quanto la ridefinizione del processo consentirà di gestire in maniera efficace ed efficiente la documentazione relativa alle risorse umane per quanto riguarda le competenze e le necessità di training
Positivi in quanto la ridefinizione del processo consentirà di gestire in maniera efficace ed efficiente la documentazione relativa alle risorse umane per quanto riguarda le competenze e le necessità di training
Positivi in quanto la ridefinizione del processo consentirà di gestire in maniera efficace ed efficiente la documentazione relativa alle risorse umane per quanto riguarda le competenze e le necessità di training
Positivi in quanto la ridefinizione del processo consentirà di gestire in maniera efficace ed efficiente la documentazione relativa alle risorse umane per quanto riguarda le competenze e le necessità di training
Positivi in quanto la ridefinizione del processo consentirà di gestire in maniera efficace ed efficiente la documentazione relativa alle risorse umane per quan',N'30/08/219
30/08/219
30/08/219
30/08/219
30/08/219
30/08/219
30/08/219
30/08/219
30/08/219',N'Aggiornare la PR18,01 a seguito di analisi al processo Gestione Risorse Umane e Formazione (valutazione attività, tempi di svolgimento richiesti e responsabilità/risorse)  con l''integrazione della nuova funzione di Hr Specialist.
in particolare:
- specificare il flusso temporale di aggiornamento dei documenti che va dal meno specifico, mansionario, alle più specifiche matrici delle competenze, dalle quali emergono le responsabilità proprie della funzione su cui basare l''aggiornamento di procedure ed istruzioni operative (effettuare formazione a riguardo).
-Specificare le modalità di gestione e la frequenza di aggiornamento del piano annuale corsi MOD18,03
-  Specificare che la valutazione  dell''efficacia dei corsi esterni è fatta da HRS in collaborazione con il responsabile di processo.
- Introdurre in PR18,01 il file excell "Database formazione interna.xlsx": modalità d''uso e di aggiornamento.
- inserire il responsabile dalle formazione alla rete vendite

Pianificazione delle attività arretr',N'PR18,01 rev 02 - rac18-32_messa_in_uso_pr1801_02_rac18-32.msg
L''aggiornamennto delle schede dipendente è stato pianificato - rac18-32_attivita_di_aggiornamento_schede_dipendente_pianificate.txt
MOD1816A e MOD816B_00 - rac18-32_messa_in_uso_mod1816a_e_mod816b_00.msg
Piano corsi, file excell, contrassegnando con colori differenti i corsi svolti (verdi) e i corsi in fase di attuazione (giallo) - mod_1803_01_piano_annuale_della_formazione1_anno_2019-attivazioni-.xlsx
PR18,01_02 - rac18-32_messa_in_uso_pr1801_02_rac18-32_1.msg
MOD1816A e MOD816B_00 - rac18-32_messa_in_uso_mod1816a_e_mod816b_00_1.msg
La modifica non è significative in quanto si tratta di un processo interno. - rac18-32_la_modifica_non_e_significativa_in_quabnto_trattasi_di_un_precesso_interno..txt
nella PR18,01 rev02 in uso è stata eliminata la parte relativa ai distributori e le parti relative alle veq (considereate come valutazione efficacia) - rac18-32_messa_in_uso_pr1801_02_rac18-32_2.msg',NULL,N'30/08/219
30/08/219
30/08/219
30/08/219
30/08/219
30/08/2019
30/08/219
30/08/2019',N'30/09/2019
30/08/2019',N'22/05/2020
27/09/2019
22/05/2020
27/09/2019
22/05/2020
22/05/2020
22/05/2020
14/12/2018',N'No',N'nessun impatto sui DMRI di prodotto',N'No',N'non necessaria alcuna comunicazione interna',N'No',N'nessun immpatto sul prodotto immesso in commercio',N'nessun impatto sui requisiti reglatori in quanto trattasi di riorganizzazione interna',N'PE - Gestione del Personale',N'PE3 - Formazione/addestramento continuo',N'PE2 Formazione/addestramento neoassunto - cambio mansione',N'No',N'notifica non significativa, vedere MOD05,36',N'No',N'nessuna notifica necessaria',NULL,N'No',NULL,'2020-11-27 00:00:00',N'AUDIT interno, pianificato per 06/2020.

Come da Audit interno n°7-20 del 16/07/2020 L''audit al processo di Gestione delle Risorse umane ha avuto un esito complessivo "Positivo" in quanto non sono state rilevate Non Conformità ma solo Osservazioni ed un''Azione di miglioramento.
Di seguito, in sintesi, la descrizione delle evidenze rilevate:
7-20-OSS1: L''organigramma in uso non riflette il riporto funzionale e gerarchico delle funzioni Produzione, CQ, Acquisti verso il COO della BU MDX
7-20-OSS2: Il mansionario in uso è disallineato rispetto all''organigramma in quanto non comprende le funzioni di nuova introduzione
7-20-OSS3: Non c''è evidenza della formazione a seguito di cambio mansione temporaneo per tutti gli spostamenti temporanei di mansione avvenuti durante il 2020
7-20-OSS4: Non c''è evidenza di un consuntivo dei corsi effettuati fin''ora nel 2020 rispetto a quelli pianificati e approvati nel piano 
7-20-OSS5: Non c''è evidenza di una pianificazione circa le attività necessarie alla messa in uso del nuovo SW per la formazione correlate alla PR05,03 relativa alla gestione dei SW
Azione miglioramento: Con il passaggio al SW Zucchetti delle schede dipendenti con la formazione eseguita è necessario stabilire come importare la formazione pregressa in modo da averla all''interno dello stesso contenitore.

In generale il processo risulta sotto controllo nonostante gli aspetti documentali ancora da aggiornare per via dei recenti e molteplici cambiamenti organizzativi, sicuramente l''integrazione della nuova risorsa EA-HRS insieme all''identificazione del SW che permetterà l''integrazione di tutti gli aspetti legati alla registrazione della formazione darà un impulso al miglioramento nella gestione di questo processo.

Le osservazioni verranno seguite come da processo di visite ispettive interne, l''azione correttiva può essere chiusa con efficacia positiva.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-09-30 00:00:00','2020-05-22 00:00:00','2020-08-06 00:00:00'),
    (N'20-20',N'Correttiva','2020-07-03 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-55',N'na',N'Documentazione SGQ',N'La causa della NC è da ricondurre all''utilizzo di un template contenente il ciclo termico e impostazioni di analisi errati.
QCT ha utilizzato il template in quanto era identificato con il codice del prodotto in analisi.
Il salvataggio del template è stato eseguito da RT durante lo sviluppo del prodotto e non eliminato al termine dell''utilizzo.
Dal confronto dei risultati ottenuti con i due cicli termici è stata evidenziata una differenza minima:
circa  0,5 Ct per il controllo interno, compreso tra 0,07 e 0,28 per lo standard.
L''utilizzo promiscuo degli strumenti con R&D deve essere maggiormente regolamentato per quanto riguarda il salvataggio dei templati. ',N'L''utilizzo dei template con gli strumenti Applied deve essere regolamentato in procedura: i templati salvati sugli strumenti devono essere verificati e approvati da QC e trattati come documentazione di CQ anche per quanto riguarda la gestione delle revisioni in uso.
Deve essere valutata da parte dell''ufficio IT la possibilità di bloccare i salvataggi nella cartella dei template o nel caso in cui ciò non sia possibile, il salvataggio di tamplate da parte di R&D deve essere identificato chiaramente nel nome stesso del template.',NULL,N'La messa in uso di template verificati e approvati permette di evitare errori nella modalità di CQ e velocizzare l''allestimento in quanto i template presenti non dovranno essere adattati. Attualmente è prassi salvare il nome del detector con il codice dello strumento (es FAM-RT15) e questo permette di tracciare lo strumento usato dal report della sessione a fini di monitoraggio; con l''attuazione della modifica questo non sarà possibile per cui sarà necessario salvare questa informazione nel nome del file. AL fine di evitare possibili dimenticanze  è necessario modificare il modulo MOD10,15 in modo tale da permettere una verifica di questa informazione .
La messa in uso di template verificati e approvati permette di evitare errori nella modalità di CQ e velocizzare l''allestimento in quanto i template presenti non dovranno essere adattati. Attualmente è prassi salvare il nome del detector con il codice dello strumento (es FAM-RT15) e questo permette di tracciare lo strumento usato dal report della sess',N'03/07/2020
03/07/2020
03/07/2020
13/07/2020
13/07/2020
13/07/2020
13/07/2020',N'Modifica della IO10,06 
Valutare l''introduzione di una nuova modifica dedicata alla procedura di stesura e gestione dei template
modifica del MOD10,18 con inserimento dell einformazioni sui template da usare
Preparazione dei template
Salvataggio sugli strumenti di laboratorio

Valutare la necessità di aggiornare il RA CQ11 e CQ12
formazione al personale R&D sulla gestione dei template prodotti in fase di sviluppo
Valutare la possibilità di prevedere un blocco nel salvataggio dei file template sui PC degli strumenti Applied
messa in uso dei template uso CQ
verificare / aggiornare la procedura per la gestione  (messa in uso) dei template ad uso cq',N'IO10,06_05, IO10,17_00, MOD10,28_00, MOD10,29_00, FORMAZIONE DEL 20/07/2020 - rac20-20_messa_in_uso_mod_e_io_rac20-200mod1802_del_2072020.msg
MOD10,18 rev15 - rac20-20_messa_in_uso_mod1018_15_rac20-20.msg
messa in uso template sdt_spg_00 e compilazione MOD10,280,2 - rac20-20_messa_in_uso_template_7500_sdt_spg_00.sdt_rac20-20.msg
messa in uso template sdt_RT-Qty_00.sdt, sdt_RT-Qual_00.sdt, sdt_RTS038PLD_00.sdt, sdt_RTS078PLD_00.sdt - rac20-20_messa_in_uso_template_7500_rac20-20.msg
messa in uso sdt_RTS098PLD_00.sdt - rac20-20messa_in_suo_template7500_rac20-20.msg
messa in uso template vari (23) - rac20-20_messa_in_uso_template_7500_uso_cq_rac20-20.msg
messa in uso sdt_ivs_cq_00 - rac20-20_messa_in_uso_sdt_ivs_cq_00.msg
messa in uso sdt_RTK602ING_00.sdt - rac20-20_messa_in_uso_templatehbv.msg
messa in uso template sdt_spg_00 e compilazione MOD10,280,2 - rac20-20_messa_in_uso_template_7500_sdt_spg_00.sdt_rac20-20_1.msg
messa in uso IO10,06_05, IO10,17_00, MOD10,28 e MOD10,29_00 - rac20-20_messa_in_uso_mod_e_io_rac20-200mod1802_del_2072020.msg
messa in uso MOD10,18_15 - rac20-20_messa_in_uso_mod1018_15_rac20-20.msg
messa in uso template sdt_RT-Qty_00.sdt, sdt_RT-Qual_00.sdt, sdt_RTS038PLD_00.sdt, sdt_RTS078PLD_00.sdt - rac20-20_messa_in_uso_template_7500_rac20-20.msg
messa in uso template RTS098PLD - rac20-20messa_in_suo_template7500_rac20-20.msg
messa in uso template vari  - rac20-20_messa_in_uso_template_7500_uso_cq_rac20-20.msg
messa in uso template sdt_IVS_CQ_00 - rac20-20_messa_in_uso_sdt_ivs_cq_00.msg
sdt_RTK602ING_00.sdt - rac20-20_messa_in_uso_templatehbv.msg
sdt_INT021EX_00.sdt - rac20-20_messa_in_uso_sdt_int021ex_00.sdt.msg
messa in uso PR05,08_02 - rac20-20_messa_in_uso_pr0508_02_dump_file.msg
Tutti i risk assessment di CQ verrano presi in analisi come da RAP21-3 - rac20-20_in_valutazione_come_da_rap21-3.txt
Non è possibile prevedere un blocco nel salvataggio dei file template sui PC degli strumenti  7500fastdx. è stato possibile mettere in sola lettura i template, in modo che per errore non possano essere sovrascritti. - rac20-20_templatesolalettura.msg
IO10,06_05, IO10,17_00, MOD10,28_00, MOD10,29_00, FORMAZIONE DEL 20/07/2020 - rac20-20_messa_in_uso_mod_e_io_rac20-200mod1802_del_2072020_2.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-20_mod05,36__non_significativa.pdf',NULL,N'03/07/2020
03/07/2020
03/07/2020
13/07/2020
13/07/2020
13/07/2020
13/07/2020',N'10/07/2020
04/09/2020
24/07/2020
30/07/2020
30/09/2020',N'30/07/2020
27/01/2021
27/01/2021
27/01/2021
21/07/2020
27/01/2021
30/07/2020',N'No',N'nessun impatto',N'No',N'non necessaria, perchè i lotti oggetto di NC non sono stati impattati e l''azione correttiva riguarda una modifica interna di processo',N'No',N'nessun impatto, perchè i lotti oggetto di NC non sono stati impattati e l''azione correttiva riguarda una modifica interna di processo',N'Le modifiche introdotte inseriscono dei punti di controllo aggiuntivi al processo (gestione dei template per uso cq e gestione delle metodiche di utilizzo della strumentazione sia in fase R&D che produzione) che lo rendono più sicuro',N'CQ - Controllo Qualità prodotti
CQ - Controllo Qualità prodotti',N'CQ11 - Fase analitica CQ: Amplificazione
CQ12 - Fase analitica CQ: Rilevazione (Real-Time PCR)',N'valutare l''impatto sui RA CQ11 e CQ12:  l''attività di valutazione dei risk assessment interessati è portata avanti con la RAP21-3',N'No',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-07-31 00:00:00',N'Valutazione efficacia: verificare assenza NC con uguale causa primaria nell''arco di 6 mesi',NULL,NULL,NULL,NULL,NULL,'2020-09-30 00:00:00','2021-01-27 00:00:00',NULL),
    (N'20-15',N'Preventiva','2020-05-22 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Difficoltà nell''eseguire indagini a seguito di VEQ negative dovute a molteplici fattori quali la mancata investitura iniziale del cliente a collaborare ai fini dell''indagine in caso di esito negativo relativo all''esecuzione di una VEQ nell''ambito del piano di sorveglianza post-market dei prodotti, al reperimento anche a distanza di tempo del materiale necessario (disponibilità del medesimo pannello presso l''ente) per ripetere la VEQ, al fine di poter svolgere delle indagini funzionali (impiegando gli stessi campioni del pannello di VEQ utilizzati dal cliente) in caso di esito negativo di VEQ.',N'Si chiede di ri-analizzare le minute delle riunioni di VEQ per definire la fattibilità delle azioni proposte.
Di seguito si riportano le proposte per migliorare il processo:
1) incrementare le VEQ eseguite con InGenius e valutare se ridurre la quantità dei pannelli/challenge da seguire
2) migliorare gestione /indagine delle VEQ con score negativi e valutare una ridefinizione delle responsabilità
3) valutare se è corretto aprire una NC dal momento che uno score negativo non è indicativo delle prestazioni di prodotto ma di un andamento rispetto ad un gruppo di lavoro
4) aggiornare istruzione rispetto alle tempistiche di indagine (valutare come fare per avere il materiale di VEQ per eventuali re-test in caso di score negativi)
5) verificare e valutare se la gestione del processo di VEQ attuale organizzata su più funzioni è efficace
6) valutare se la messa a disposizione dei dati sull''andamento dei vari prodotti/pannelli è idonea per il personale EGSpA e per la finalità di indagine sugli score negativi
7) valutare la riassegnazione della gestione del processo di VEQ sotto un''unica funzione che si occupi di tutte le fasi del processo dalla pianificazione delle VEQ, alla raccolta dei dati sugli esiti delle VEQ, al coordinamento dell''indagine in caso di score negativi; candidato il reparto validazioni
8) valutare se l'' attuale analisi statistica effettuata sui risultati di VEQ è sufficiente
',NULL,N'Impatto positivo, l''analisi dell''esito di VEQ negative all''interno di una non conformità permette, oltre a poter svolgere delle indagini funzionali che confermino  o meno l''esito ottenuto dal cliente, di effettuare delle analisi statistiche attraverso la raccolta costante di questi dati che, se lasciati esclusivamente nel report di VEQ non sarebbero analizzati nel complesso ai fini della sorveglianza post-market dei prodotti.
Le riunioni svolte sulla gestione del processo di VEQ forniscono un punto di partenza per poter effettuare un''analisi critica sulla gestione di questo processo. Sulla base di queste analisi sarà possibile attuare modifiche volte a migliorare il processo al fine di renderlo più efficace ed efficiente. 
Il fatto di non poter effettuare delle appropriate indagine su VEQ negative lascia presupporre che non siano attuate tutte le possibili attività a riguardo. Ciò nonostante, se ad un accurato riesame delle  minute delle riunioni, emergerà che il processo è efficiente ed efficace, ma',NULL,N'valutare il report fornito da SARD sull''analisi del processo di VEQ. Dall''esito ottenuto accordarsi sulle attività da svolgere con gli altri reparti coinvolti ed intervenuti nelle riunioni di veq 
come da analisi annuale della mappatura dei processi risulta necessario segmentare il processo ASA5 "sorvbeglianza post-market", al fine di effettuare una valutazione del rischio specifica sulla gestione del processo di veq. riesaminare ed approvare il risk assesment
Ri-esaminare le minute delle riunioni e fornire per ciascun punto un''analisi dettagliata su pro e contro dell''attività in essere (come da IO14,05) e dell''attività proposta. Specificare inoltre se il personale coinvolto nell''attività è adeguatamente formato attraverso evidenze scritte (MOD18,02 o mail).

Di seguito si riportano le proposte per migliorare il processo in uscita dalle riunioni:
1) incrementare le VEQ eseguite con InGenius e valutare se ridurre la quantità dei pannelli/challenge da seguire
2) migliorare gestione /indagine delle',NULL,NULL,NULL,N'30/07/2020
30/09/2020
30/07/2020
30/09/2020',NULL,N'No',N'nessun impatto in quanto trattasi della gestione del processo di VEQ nell''ambito della sorveglianza dei prodotti',N'No',N'non necessaria',N'No',N'nessun impatto in quanto trattasi della gestione del processo di VEQ nell''ambito della sorveglianza dei prodotti',N'Le VEQ forniscono dati sulle prestazioni dei prodotti nelle condizioni d''uso presso gli utilizzatori finali, è perciò necessario che il processo sia efficace ed efficiente.',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'come da analisi annuale della mappatura dei processi risulta necessario segmentare il processo ASA5 "sorvbeglianza post-market", al fine di effettuare una valutazione del rischio specifica sulla gestione del processo di veq',N'No',N'vedere MOD05,36',N'No',N'nessun vuirtual manufacturer coinvolto',N'Cristina Olivo',N'Si',NULL,NULL,N'A 09/2020 n on è previsto un aggiornamento del processo di gestione delle VEQ nè una valutazione della sua efficacia efficienza. la RAP può considerarsi per ora bloccata.',NULL,NULL,NULL,NULL,NULL,'2020-09-30 00:00:00',NULL,NULL),
    (N'20-14',N'Correttiva','2020-04-28 00:00:00',N'Fabio Panariti',N'Manutenzione e taratura strumenti interni',N'Non conformità',N'20-34',N'Datalogger Themis/T&D',N'Documentazione SGQ',N'Sistema di monitoraggio temperatura obsoleto.',N'Sostituzione di tutto il sistema di monitoraggio della temperatura: datalogger e sonde di temperatura (ad esclusione delle sonde per la misurazione della temperatura -70°C che rimangono invariate), concentratori e software di trasmissione/archiviazione dati; aggiunta di ripetitori di segnale.
Messa IN USO strumenti, validazione processo gestione temperature, aggiornamento documentazione.',NULL,N'Impatto positivo, in quanto la gestione del sistema sarà più semplice e senza anomalie di trasmissione.
Impatto positivo: semplificazione della gestione e presa in carico degli allarmi in quanto non arriveranno più e-mail di allarme imputabili alle mancate trasmissioni ma solo quelle relative a fuori range
Impatto positivo: semplificazione della gestione e presa in carico degli allarmi in quanto non arriveranno più e-mail di allarme imputabili alle mancate trasmissioni ma solo quelle relative a fuori range
Impatto positivo: semplificazione della gestione e presa in carico degli allarmi in quanto non arriveranno più e-mail di allarme imputabili alle mancate trasmissioni ma solo quelle relative a fuori range
Impatto positivo: semplificazione della gestione e presa in carico degli allarmi in quanto non arriveranno più e-mail di allarme imputabili alle mancate trasmissioni ma solo quelle relative a fuori range
Impatto positivo: semplificazione della gestione e presa in carico degli allarmi in quan',N'23/06/2020
23/06/2020
19/05/2020
19/05/2020
29/04/2020
19/05/2020
19/05/2020
29/04/2020
29/04/2020
19/05/2020',N'b
approvazione documenti  collegati al sistema di monitoraggio della temperatura (es. IO11,02)
approvazione documenti  collegati al sistema di monitoraggio della temperatura (es. IO11,02)
1) stesura piano di validazione del processo di monitoraggio delle temperature ed aggiornamento del risk assessment del sw (SW4) e dei datalogger (ST3)
2) codifica datalogger (nuova numerazione e  associazione strumento monitorato) ed apposizione dell''etichetta con il codice assegnato a ciascun datalogger, 
3) taratura nuove sonde su CG-FR-UC-IS-TA,
4) installazione nuove sonde nei pressi di quella obsoleta (questo permetterà di far coesistere i 2 sistemi durante l''installazione garantendo continuità nel monitoraggio delle temperature),
5) prima programmazione  dei nuovi datalogger da parte del fornitore (associazione datalogger/concentratore/strumento monitorato), 
6) collegamento del datalogger, associato allo strumento, con la sonda precedentemente posizionata, 
7) formazione del personale EGSpA da parte del',N'IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020_5.msg
SPEC-LO rev01 - cc20-41_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020_2.msg
IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020_1.msg
La documentazione relativa all''instalolazione dei datalogger (es: MOD11,13A e B) non è ancora stata completata per motivi organizzativi e di carico di lavoro: i nuovi datalogger installati sono stati 84 e, a causa dell''emergenza covid, il forniotore non è stato fisicamenete presente in azienda, ma ha contribuito all''installazione solo da remoto. La formazione al personale interno è avvenuta in data 04/08/2020. In aggiunta all''installazione del nuovo sistema datallogger ed alla consueta gestione programmata del sito e degli strumenti), si è aggiunta la gestione degli strumenti del nuovo laboratorio interno (nuove instllazioni e spostamenti dal lab. controlli positivi), in totale, rispetto  ai 21 strumenti che erano stati installati  nel 2019, nel 2020 ne risultano installati 152. questo spiega il ritardo nella stesura di tutta la documentazione richiseta.  Entro settembre verrà completata tutta la documentazione non ancora prodotta o in corso di redazione. i DRAFT sono  all''indirizzo G:DOCSGQBOZZEMANUTENZIONI e TARATURAISTRUZIONI. - rac20-14_stato_attivita_a_09-2020.txt
REPORT VALIDAZIONE DEL 23/07/2020 - mod09,12_00_rapportovalidazioneprocessocontrollotemperature_sw48_t&d_e_aleggato_1.pdf
RA nuovo sw T&D. il RA dei datalogger non subisce modifiche - mod09,27_01_sw48_t&d.pdf
IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020.msg
IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020_2.msg
Aggiornamento documento "identificazione di stato (conformi, nc, quarantena" dei refrigeratori della produzione, a seguito delle nuove installazioni - rac20-14_messa_in_uso_all-2_pr0801_06.msg
IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020_3.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-14_mod05,36__non_significativa.pdf
IO11,02_02 del 10/07/2020, MOD18,02 del 04/08/2020 - rac20-14_mesa_in_uso_io1102_02_rac20-14_mod1802_del_04082020_4.msg
Aggiornamento documento "identificazione di stato (conformi, nc, quarantena" dei refrigeratori della produzione, a seguito delle nuove installazioni - rac20-14_messa_in_uso_all-2_pr0801_06_1.msg
messa in uso specifiche assistenza - rac20-14__messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020.msg
VMP SW aggiornato - sw_mod09,30_01_processvalidationmasterplan.xlsx',NULL,N'23/06/2020
19/05/2020
19/05/2020
29/04/2020
19/05/2020
19/05/2020
19/05/2020
19/05/2020',N'30/06/2020
30/06/2020
30/06/2020
30/06/2020
30/06/2020
30/06/2020
30/09/2020
30/06/2020',N'15/09/2020
15/09/2020
14/12/2020
15/09/2020
15/09/2020
15/09/2020
16/04/2021
15/09/2020',N'Si',N'la modifica riguarda il sistema di monitoraggio delle temperature, non è una modifica di prodotto, tuttavia il sw di monitoraggio delle temperature (phc3) è citato nel DMRI, pertanto tutti i dmri sono da aggiornare',N'No',N'non applicabile, la modifica riguarda il sistema di monitoraggio delle temperature, non è una modifica di prodotto',N'No',N'non applicabile, la modifica riguarda il sistema di monitoraggio delle temperature, non è una modifica di prodotto',N'Il nuovo sistema di monitoraggio delle temperature semplifica la gestione degli allarmi eliminando i falsi allarmi non risolvibili con il precedente sistema. Non ci sono impatti aggiuntivi a livello regolatorio rispetto al sistema in uso.',N'ST - Elenco famiglie Strumenti
SW - Elenco Software',N'ST3 - Datalogger (LO)
SW4 - Phc3 (controllo temperature)',N'i risk assessment ST3 E SW4 devono essere verificati in base al nuovo sistema di monitoraggio delle temperature. Il nuovo sistema deve essere ri-validato, il VMP deve essere aggiornato. Il RA ST3 non è da aggiornare. è stato creato il RA SW48',N'No',N'vedere MOD05,36',N'No',N'non applicabile, la modifica riguarda il sistema di monitoraggio delle temperature, non è una modifica di prodotto',N'Daniele Salemi',N'No',NULL,'2020-12-31 00:00:00',N'Assenza di falsi allarmi per esempio dovuti a mancate trasmissioni nell''arco di 6 mesi dalla data di messa in uso del nuovo sistema di monitoraggio.
Al 11/01/2021 è ancora da aggiornare il VMP, documento, comunque, non influente sull''utilizzo del sistema datalogger.
I datalogger sono in uso da 07/2020, l''ultimo documento, il VMP, è stato aggiornato ad 04/2021. 
Da 07/2020 a 04/2021 non c''è stata alcuna NC relativa ai nuovi datalogger installati (codici da LO200 a LO299). Si ritiene pertanto possibile chiudere l''azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-09-30 00:00:00','2021-04-16 00:00:00','2021-04-16 00:00:00'),
    (N'19-20',N'Correttiva','2019-06-25 00:00:00',N'Stefania Brun',N'Sistema qualità',N'Non conformità',NULL,N'Requisiti Anvisa e FDA per approvazione etichette lotto specifiche prima dell''uso in produzione ',N'Documentazione SGQ',N'Mancata consapevolezza dei requisiti dei regolamenti Brasile e US per la necessità di approvazione delle etichette prodotte per lotto specifico prima del loro utilizzo in quanto requisito di sistema qualità non gestito a livello di regulatory assessment di prodotto',N'Aggiornare IO09,08 al fine di indicare l''approvazione delle etichette stampate prima dell''uso in Produzione; aggiornare anche la PR09,01 per integrare questo controllo e definire le responsabilità del 2° operatore in riferimento a questo aspetto

Aggiornare la IO09,07 "CONTROLLO AL RILASCIO" per i prodotti forniti in outsourcing quali InGenius che devono contenere tra la documentazione anche l''evidenza della verifica dell''etichetta prima dell''uso.
',NULL,N'Positivo in quanto garantisce la compliance con i requisiti inerenti l''approvazione delle etichette stampate prima dell''uso in produzione da parte dei regolamenti del Brasile (5.2.2.3 RDC 16/2013) e quello USA 21 CFR 820:120(b) 
Positivo in quanto garantisce la compliance con i requisiti inerenti l''approvazione delle etichette stampate prima dell''uso in produzione da parte dei regolamenti del Brasile (5.2.2.3 RDC 16/2013) e quello USA 21 CFR 820:120(b) 
Garantisce un controllo più accurato delle etichette stampate prima dell''uso, In questo modo in caso di errore nella compilazione delle etichette non è necessario effettuare la rilavorazione del lotto ma procedere alla stampa delle etichette corrette e distruzione di quelle errate
Garantisce un controllo più accurato delle etichette stampate prima dell''uso, In questo modo in caso di errore nella compilazione delle etichette non è necessario effettuare la rilavorazione del lotto ma procedere alla stampa delle etichette corrette e distruzione di quelle',N'08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
09/07/2019',N'Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica 
valutare se necessario l''aggiornamento dei DMRI /(inserimento MOD09,37)
Aggiornare IO09,08 al fine di indicare l''approvazione delle etichette stampate prima dell''uso in Produzione
Aggiornare la IO09,07 "CONTROLLO AL RILASCIO" per i prodotti forniti in outsourcing quali InGenius che devono contenere tra la documentazione anche l''evidenza della verifica dell''etichetta prima dell''uso.
messa in uso della documentazione
Aggiornamento, in accordo con MM, del VMP e del risk assessment P1.6
Aggiornare la PR09,01 per integrare questo controllo e definire le responsabilità del 2° operatore in riferimento a questo aspetto
formare il perosonale di produzione
Pianificazione ed aggiornamentodei moduli di produzione inserendo la verifica delle etichette da parte del secondo operatore.',N'IO09,08_02 - rac1-20_messa_in_uso_io0908_02_rac19-20.msg
Implementazione processo da parte di PSS per la waste box, in allegato un esempio dove mettono l''esemplare di etichetta utilizzato con la loro verifica - rac20_19_esempioapprovazioneetichettapss_f2102_lotno.n4ra0103800_20200907.pdf
 PRE09,01_03, MOD09,37_00, MOD09,08_02  - rac19-20_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20_1.msg
La modifica non è significativa, pertanto non è necessario effettuare alcuna notifica. - rac19-20_mod05,36_non_significativa.pdf
MOD18,02 DEL 3/09/2019 - rac19-20_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20_2.msg
IO09,08_02 - rac1-20_messa_in_uso_io0908_02_rac19-20_1.msg
PRE09,01_03, MOD09,37_00, MOD09,08_02 - rac19-20_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20.msg
mod963-xxx - rac19-20_messa_in_uso_mod963-xxx_cc19-38_rac19-20_mod1802_del_291019.msg
moduli_r-bio - rac19-20_messa_in_uso_moduli_r-bio_rac19-20,_mod18,02_del_11-11-2019.msg
mod962-rtsd00ing_03_rts200ing_02 - rac19-20_messa_in_uso_mod962-rtsd00ing_03_rts200ing_02_rac19-20_mod18-02_del_041219.msg
mod962-rts201ing_02 - rac19-20_messa_in_uso_mod962-rts201ing_02_cc19-46_rac19-20_mod1802_del_91219.msg
mod956-rtsg07pld-xx_mod962-rts000 - rac19-20_messa_in_uso_mod956-rtsg07pld-xx_mod962-rts000_rac19-20.msg
mod962-rtsg07pld-xx_mod956-rts003 - rac19-20_messa_in_uso_mod962-rtsg07pld-xx_mod956-rts003_rac19-20.msg
MOD962-CTR200ING, MOD962-CTR201ING - rac19-20_messa_in_uso_mod_cq_prod_cc19-37_rac19-20_mod1802_del_91219.msg
mod962-rts120ing_02 - rac19-20_messa_in_uso_mod962-rts120ing_02_cc19-51_rac19-20_mod1802_08012020.msg
modli_prod_cre_esbl - rac19-20_messa_in_uso_modli_cq_e_prod_cre_esbl_cc1937_rac19-20_mod1802_del_17-18022020.msg
messa_in_uso_modxx-extd - rac19-20_messa_in_uso_modxx-extd.docx
mod962-ic500 - rac19-20_messa_in_uso_mod962-ic500_cc18-7_rac19-20mod1802_14042020.msg
mod_ctr - rac19-20_mesa_in_uso_mod_ctr.msg
moduli_estrazione - rac19-20_messa_in_uso_moduli_estrazione_rac19-20.msg
moduli RTS e CTR OEM-FT - rac19-20_messa_in_uso_mod_oem-ft_rac19-20_cc12-17.msg
moduli produzione spgg07210 - rac19-20_messa_in_uso_modspg07-210_produzione_cc20-38__rac19-20.msg
RA P1.6 firmato in data 12/05/2020 - mod09,27_01_modulo_di_risk_assessment_p1,6_etichettaturasemilavoraticomponenti_cc18-51_rap19-11_rac19-20__1.pdf
moduli da modificare entro 09/2020 - rac19-20_pianmificazioneelencomodulidamodificare.xlsx
elenco DMRI da aggiornare - rac19-20_elenco_dmri_a_aggiornare_mod05,27_00_elenco_documentazionediprogettoevalidazione.pdf
DMRI aggiornati: P190, EBV, CHLAMYDIA, JCV, BKV, COAGULATION, HSV2, HSV1, HHV6, PARVOVIRUS, VZV, ASPERGILLUS, TOXO, MDR/MTB, ADENO, HHV8, HHV7 - rac19-20_dmri_aggiornati_1.xlsx
tutti i DMRI presenti nell''elenco sono stati aggiornati. - rac19-20_dmrielencoaggiornati.txt',NULL,N'09/07/2019
09/07/2019
08/07/2019
08/07/2019
09/07/2019
09/07/2019
08/07/2019
09/07/2019
09/07/2019',N'02/08/2019
02/08/2019
02/08/2019
30/10/2019
31/07/2019
30/09/2020',N'11/09/2019
14/05/2021
06/08/2019
08/06/2020
12/05/2020
04/09/2019
03/09/2019',N'Si',N'valutare se necessario l''aggiornamento dei DMRI. vedere elenco allegato ',N'No',N'non necessaria',N'No',N'nessun impatto',N'Rac aperta a seguito di valutazione dei requisiti Anvisa e FDA ',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.6 - Etichettatura semilavorati e componenti',N'valutazione impatto sul sottoprocesso ed aggiornamento del risk assessment P1.6 Etichettatura materiali e compoenti',N'No',N'NON significativa, vedere MOD05,36',N'No',N'NON significatia, vedere MOD05,36',NULL,N'Si',NULL,'2021-10-29 00:00:00',N'valutazione dell''efficacia: verificare le NC relative ad errori di scadenza, lotto e ref.
18/05/2021: nonostante i moduli di produzione non siano ancora tutti stati aggiornati con la tracciabilità del MOD09,37 di approvazione delle etichette, tale modulo è utilizzato da 09/2019, è pertanto possibile analizzare i dati. Da settembre 2019 sono state 5 i casi di errore nell''assegnazione  scadenza non rilevati durante l''approvazione dell''etichetta prima dell''uso (MOD09,37) (NC21-3, NC21-1, NC20-49, NC20-42, NC20-37). ad esclusione della NC20-49 relativa ad un BANG, tutti gli altri casi riguardano prodotti sars fabbricati in regime di urgenza e non uniformati ai prodotti routinari. in nessun caso un prodotto con data di scadenza errata è stato rilasciato al cliente, gli errori sono sempre stati individuati. DA VALUTARE L''ESITO DEL CONTROLLO INTRODOTTO VERIFICANDO IL NUMERO DI EVENTI IN UN PERIODO ROUTINARIO (DA MAGGIO AD OTTOBRE 2021)',NULL,NULL,NULL,NULL,NULL,'2020-09-30 00:00:00',NULL,NULL),
    (N'21-7',N'Preventiva','2021-02-05 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'La valutazione delle prestazioni dei lotti utilizzando specifiche più stringenti permette di individuare in anticipo eventuali derive di produzione/prodotto.',N'La modifica prevede lo studio di criteri di monitoraggio in base ai dati di CQ disponibili e l''inserimento di questi nella verifica tecnica per la valutazione della conformità. In alcuni casi lo studio dell''andamento dei prodotti può richiedere la modifica dei criteri stessi di CQ studiati nell''ambito del DT e pertanto riferiti ai soli primi 3 lotti di produzione. Ciascun caso sarà trattato singolarmente per la valutazione degli impatti.',NULL,N'La valutazione dei lotti utilizzando criteri più stringenti che non influiscono sulla conformità permette di mettere in luce eventuali problematiche di produzione o derive.
La valutazione dei criteri di CQ durante i DT possono basarsi sulla necessità di inserire in futuro i criteri di monitoraggio (non appena vi sia una numerosità dei dati sufficiente).
Nessun impatto sul processo
Impatto positivo, l''introduzione dei criteri di monitoraggio permette di individuare eventuali derive, prima di arrivare ad avere un lotto non conforme.',N'05/02/2021
05/02/2021
05/02/2021
15/03/2021',N'Analisi dei dati di CQ e definizione dei criteri di monitoraggio
Modifica delle Verifiche tecniche o Inserimento di Verifiche di Monitoraggio nel caso di documenti generici
Modifica della procedura e dell''istruzione di CQ
Formazione
(Vedi piano dettagliato allegato)
Eventuale valutazione degli impatti sul prodotto nel caso l''analisi dei CQ porti a una rivalutazione delle specifiche di Controllo Qualità.
Messa in uso dei documenti.
Modifica dei DMRI

Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'ELENCO DOC - plandoc_rap21-7.xlsx
mod1012rts400ing-ctr400ing_02 - rap21-7_messa_in_uso_mod1012rts400ing-ctr400ing_02.msg
mod1012-rtsd00ing-ctrd00ing_08 - rap21-7_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_08.msg
MOD10,12-RTS401ING-48-CTR401ING_02 - rap21-7_messa_in_uso_mod10,12-rts401ing-48-ctr401ing.msg
PR10,02_08 - rap21-7_messa_in_uso_pr1002_08.msg
IO10,06_07 - rap21-7_messa_in_uso_io1006_07.msg
mod1012rts400ing-ctr400ing_02 - rap21-7_messa_in_uso_mod1012rts400ing-ctr400ing_02_2.msg
mod1012-rtsd00ing-ctrd00ing_08 - rap21-7_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_08_1.msg
MOD10,12-RTS401ING-48-CTR401ING_02 - rap21-7_messa_in_uso_mod10,12-rts401ing-48-ctr401ing_1.msg
PR10,02_08 - rap21-7_messa_in_uso_pr1002_08_1.msg
IO10,06_07 - rap21-7_messa_in_uso_io1006_07_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-21_mod05,36__non_significativa.pdf',NULL,N'05/02/2021
05/02/2021
05/02/2021
05/02/2021',N'30/09/2021',N'05/02/2021',N'Si',N'da modificare se si introducono  nuovi moduli',N'No',N'nessun impatto, si tratta di un aggiornamento del processo interno',N'No',N'nessun impatto, si tratta di un aggiornamento del processo interno',N'Valutare le prestazioni dei lotti utilizzando specifiche più stringenti (criteri di monitoraggio) permette di individuare in anticipo eventuali derive di produzione/prodotto.',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'valutare',N'No',N'vedere mod05,36. L''introduzione di criteri di monitoraggio non è una modifica significativa da notificare in quanto non modifichi i criteri di cq e non cambi il processo di cq (a livello di modalità di esecuzione del cq). ',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-09-30 00:00:00',NULL,NULL),
    (N'18-27',N'Correttiva','2018-09-05 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'18-112',N'CTR200ING',N'Documentazione SGQ',N'La causa è stata un errore di pianificazione in quanto il lotto di CTR200ING è stato inviato contemporanemente al lotto pilota della MIX. Dato che quasi sempre durante i DT si eseguono i CQ sia sui lotti pilota di mix  e CTR  insieme, vi è stato un automatismo. Sarà necessario fare un database di pianificazione dedicato ai lotti pilota. ',N'Eseguire una pianificazione dedicata dei CQ per i lotti pilota,  database pianificazione ',NULL,N'L''impatto è relativo alle attività quotidiane di pianificazione, l''utilizzo di due file non permette di avere una visione di insieme del lavoro da svolgere e inoltre è necessario tenere aggiornati 2 file anzichè 1 solo. Vi è anche un impatto positivo in quanto è possibile strutturare diversamente il database per i lotti pilota, in particolare in relazione alla definizione delle scadenze che ad oggi segue le modalità dei lotti di produzione. Data la NC riscontrata è comunque necessario fare questa distinzione.
L''impatto è relativo alle attività quotidiane di pianificazione, l''utilizzo di due file non permette di avere una visione di insieme del lavoro da svolgere e inoltre è necessario tenere aggiornati 2 file anzichè 1 solo. Vi è anche un impatto positivo in quanto è possibile strutturare diversamente il database per i lotti pilota, in particolare in relazione alla definizione delle scadenze che ad oggi segue le modalità dei lotti di produzione. Data la NC riscontrata è comunque necessario f',N'05/09/2018
05/09/2018
05/09/2018',N'1-Creazione di un file di pianificazione specifico per i lotti pilota.
2-Inserimento  delle nuove modalità di pianificazione nella PR10,02.
3-Formazione a QCT e PP
Non vi sono azioni a carico. Sarà eseguita la formazione a PP che dovrà utilizzarlo per l''inserimento dei lotti
messa in uso documentazione',N'esempio file pianificazione lotti pilota a 02/2019 - rac18-27_programmazione_dt-lp_prod_cq_pianificata_a_02-2019.xlsm
PR10,02 rev06 - rac18-27_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2.msg
MOD18,02 del 30/01/2019 - rac18-27_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_1.msg
MOD18,02 del 30/01/2019 - rac18-27_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_2.msg
PR09,01 aggiornata specificando le attività legate alla pianificazione lotti pilota, MOD18,02 del 3/09/2019 - rac18-27_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20.msg
PR10,02 rev06 IN USO AL 14/02/2019 - rac18-27_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_3.msg',NULL,N'05/09/2018
05/09/2018
05/09/2018',N'30/10/2018
30/10/2018
30/10/2018',N'14/02/2019
10/09/2019
14/02/2019',N'No',N'nessun impatto',N'No',N'non necessaria alcuna comunicazione in quanto modifica di sistema',N'No',N'nessun impatto',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ4 - Pianificazione CQ',N'CQ4 valutare se da aggiornare',N'No',N'non necessaria alcuna comunicazione in quanto modifica di sistema',N'No',N'non necessaria ',NULL,N'No',NULL,'2020-01-07 00:00:00',N'La creazione di un file di pianificazione specifico per i lotti pilota ha eliminato la presenza di non conformità',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-10-30 00:00:00','2019-09-10 00:00:00','2020-01-07 00:00:00'),
    (N'19-27',N'Correttiva','2019-09-23 00:00:00',N'Stefania Brun',N'Global Service',N'Non conformità',N'19-96',N'disallineamento PR19,02 del service rispetto all''attuale struttura organizzativa del service',N'Documentazione SGQ',N'Per quanto riguarda il mancato aggiornamento delle responsabilità a livello di PR dovuta ad un''organizzazione del service che ha subito modifiche organizzative ricorrenti nel tempo. Mancata consapevolezza ISO13485 8.4 Analysis of data, quelli regolatori FDA 21 CFR 820.200(c) (who receives a service report that represents an event that must be reported to FDA as a medical device report automatically considers the report a complaint); RDC ANVISA 16/2013: 8.2.2 per quanto riguarda l''aspetto del riesame dei service report per i loro impatto sulla vigilanza',N'Aggiornare procedura PR19,02 per allineare la struttura del service all''attuale organizzazione, valutando eventuale fusione delle attuali due procedure relative al supporto applicativo (PR19,01) e quello tecnico (PR19,02).
Integrare lo scopo della procedura del service e del supporto con i requisiti regolatori quali quelli della ISO13485:2016 8.4 Analysis of data, quelli regolatori FDA 21 CFR 820.200(c) (who receives a service report that represents an event that must be reported to FDA as a medical device report automatically considers the report a complaint); RDC ANVISA 16/2013: 8.2.2 per quanto riguarda l''aspetto del riesame dei service report per i loro impatto sulla vigilanza.
Per l''estero, ossia per distributori e ICO specificare l''integrazione all''interno del programma di training di una sessione specifica su vigilanza/reclami.',NULL,N'Positivo in quanto una procedura aggiornata per quanto riguarda le responsabilità e i requisiti regolatori applicabili rende il personale coinvolto consapevole in caso di eventuale scostamento dell''operatività rispetto a quanto prescritto
Positivo in quanto una procedura aggiornata per quanto riguarda le responsabilità e i requisiti regolatori applicabili rende il personale coinvolto consapevole in caso di eventuale scostamento dell''operatività rispetto a quanto prescritto
Positivo in quanto una procedura aggiornata per quanto riguarda le responsabilità e i requisiti regolatori applicabili rende il personale coinvolto consapevole in caso di eventuale scostamento dell''operatività rispetto a quanto prescritto
Positivo in quanto una procedura aggiornata per quanto riguarda le responsabilità e i requisiti regolatori applicabili rende il personale coinvolto consapevole in caso di eventuale scostamento dell''operatività rispetto a quanto prescritto
Positivo in quanto una procedura aggiornata per quan',N'23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019',N'Collaborare alla revisione della procedura PR19,02
Collaborare alla revisione della procedura PR19,02
Collaborare alla revisione della procedura PR19,01 "Assistenza al cliente" al fine di integrare i contenuti di questa all''interno della PR19,02
Collaborare alla revisione della procedura PR19,01 "Assistenza al cliente" al fine di integrare i contenuti di questa all''interno della PR19,02
Collaborare alla revisione della procedura della PR19,02 e integrare i riferimenti alle procedure correlate quali Reclami, Analisi dati e Vigilanza.
Effettuare formazione alle funzioni coinvolte 
Revisionare e approvare PR19,01
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'pr19,01_rev01 - rac19-27_messa_in_uso_pr19,01_rev01_rac19-27,_12-18-oss1,_6-18-oss1,_12-19-oss4,19-18-oss1.pdf
pr19,01_rev01 - rac19-27_messa_in_uso_pr19,01_rev01_rac19-27,_12-18-oss1,_6-18-oss1,_12-19-oss4,19-18-oss1_1.pdf
pr19,01_rev01 - rac19-27_messa_in_uso_pr19,01_rev01_rac19-27,_12-18-oss1,_6-18-oss1,_12-19-oss4,19-18-oss1_2.pdf
pr19,01_rev01 - rac19-27_messa_in_uso_pr19,01_rev01_rac19-27,_12-18-oss1,_6-18-oss1,_12-19-oss4,19-18-oss1_3.pdf
pr19,01_01 - rac19-27_messa_in_uso_pr19,01_rev01_rac19-27,_12-18-oss1,_6-18-oss1,_12-19-oss4,19-18-oss1_4.pdf
mail formazione - rac19-27_qualita_formazione_pr1901_assistenza_al_cliente_application_and_technical_support_rev.01.msg
la modifica non è significativa, pertanto non deve essere notificata. - rap19-27_mod05,36_non_significativa.pdf',NULL,N'23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019
23/09/2019',N'30/10/2019
30/10/2019
30/10/2019
30/10/2019
30/10/2019
30/10/2019
30/10/2019
30/10/2019',N'28/04/2020
28/04/2020
28/04/2020
28/04/2020
29/04/2020
28/04/2020
23/10/2019',N'No',N'No in quanto tale procedura non è correlata al processo produttivo e di rilascio dei prodotti, per tale ragione non è contemplata nei DMRI',N'No',N'No in quanto l''aggiornamento della Procedura non ha impatto sugli utilizzatori',N'No',N'No in quanto l''aggiornamento della Procedura non ha impatto sui prodotti immessi in commercio',N'L''aggiornamento della procedura di assistenza e service permetterà la rispondenza ai requisiti regolatori applicabili',N'ASA - Assistenza applicativa prodotti
AST - Assistenza tecnica strumenti',N'ASA2 - Richiesta di assistenza
AST8 - Richiesta di assistenza',N'No in quanto l''aggiornamento della procedura non introduce nuovi rischi',N'No',N'No in quanto non rientra nelle modifiche significative che devono essere notificate',N'No',N'No in quanto non rientra nelle modifiche significative che devono essere notificate',N'Stefania Brun',N'No',NULL,'2020-10-31 00:00:00',N'valutazione efficacia: verificare l''assenza di nc a seguito di messa in uso della nuova versione della PR19,01 rev01.
Non sono emerse nc legate alle attività descritte nella PR19,01, il processo è efficace.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-10-30 00:00:00','2020-04-29 00:00:00','2020-11-17 00:00:00'),
    (N'20-37',N'Correttiva','2020-10-22 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-89',N'RTS170ING',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'In base al test di ripetizione risultato conforme e all''analisi dei logfile dello strumento che ha confermato il buon funzionamento dello stesso, si può concludere che la causa della NC è collegata a un problema tecnico durante la preparazione delle diluizioni dei campioni. Benchè dai logfile non emergano anomalie particolari il service ha consigliato di usare un volume morto maggiore ossia un eccesso minimo di 20µL. ',N'Si chiede la modifica delle modalità di preparazione delle diluizioni dei campioni aumentando i volumi in eccesso al fine di garantire il buon funzionamento della dispensazione dello strumento.',NULL,N'La modifica dovrebbe garantire un eccesso suifficiente allo strumento in particolare per il secondo replicato riducendo dispensazioni di volumi minori non rilevabili come errore strumentale.
Prevedere il giusto volume in eccesso per i moduli di CQ dei nuovi  progetti. Tale modifica non ha impatto sui criteri di CQ, ma mira a garantire il buon funzionamento della dispensazione dello strumento.
nessun impatto
nessun impatto',NULL,N'Modifica del modulo MOD10,13-RTS170ING aumentando i volumi in eccesso delle diluizioni. Formazione
nessuna azione solo approvazione della modifica
messa in uso dei documenti
valutare la significatività della modifica e l''eventuale notifica presso le autorità comptenti',N'MOD10,13-ctr170ing rev02 - rac20-37_messa_in_uso_mod1013-ctr170ing.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - rac20-36_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_2.msg
MOD10,13-ctr170ing rev02 - rac20-37_messa_in_uso_mod1013-ctr170ing_1.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - rac20-36_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_3.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-37_mod05,36__non_significativa.pdf',NULL,N'22/10/2020
04/11/2020',N'30/10/2020',N'06/11/2020
06/11/2020
06/11/2020
04/11/2020',N'No',N'nessun impatto le modifiche non influiscono sul titolo dei moduli e non introducono nuovi documenti ',N'No',N'nessun impatto, le modifiche riguardano procedure interne',N'No',N'nessun impatto, le modifiche riguardano procedure interne',N'la modifica garantisce il buon funzionamento della dispensazione dello strumento, non modifica i criteri di cq.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nel corso del riesame annuale si valuterà l''eventuale impatto sul profilo di rischio del RA R&D1.9',N'Si',N'vedere il MOD05,36. La modifica sarebbe da notificare, ma essendo i moduli di CQ confidenziali e non essendo stati richiesti da alcuna autorità comptente/distributori, non si ritiene necessario effettuare la notifica.',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-30 00:00:00','2020-11-06 00:00:00',NULL),
    (N'20-23',N'Correttiva','2020-07-24 00:00:00',N'Stefania Brun',N'Produzione',N'Non conformità',N'20-65',N'mancata gestione del cambiamento relativo all''introduzione di un nuovo laboratorio con il change control',N'Documentazione SGQ',N'Sotto valutazione degli impatti di un cambiamento strutturale relativo all''introduzione di un nuovo laboratorio sugli aspetti relativi al controllo ambientale e al sistema qualità',N'Valutare gli impatti relativi all''introduzione del nuovo laboratorio sulla documentazione del sistema qualità 
Verificare nella PR05,05 "CHANGE CONTROL" che siano inclusi anche i cambiamenti strutturali ',NULL,N'Impatto positivo, la documentazione SGQ sarà aggiornata e avrà recepito le modifiche introdotte dalla creazione del nuovo laboratorio. Maggiore garanzia sull''assenza di contaminazione del prodotto.
Impatto positivo, la documentazione SGQ sarà aggiornata e avrà recepito le modifiche introdotte dalla creazione del nuovo laboratorio. Maggiore garanzia sull''assenza di contaminazione del prodotto e sul controllo ambientale. Garanzia che il processo di gestione della modifica comprenda anche questo tipo di cambiamenti
Impatto positivo, la documentazione SGQ sarà aggiornata e avrà recepito le modifiche introdotte dalla creazione del nuovo laboratorio. Maggiore garanzia sull''assenza di contaminazione del prodotto e sul controllo ambientale. Garanzia che il processo di gestione della modifica comprenda anche questo tipo di cambiamenti
Impatto positivo, la documentazione SGQ sarà aggiornata e avrà recepito le modifiche introdotte dalla creazione del nuovo laboratorio. Maggiore garanzia sull''assenza di con',N'05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020',N'Valutare la necessità di aggiornamento dei documenti di produzione rispetto all''introduzione del nuovo laboratorio, collaborare con Document Specialist alla stesura ed effettuare la relativa formazione del personale.
PR09,04, IO24,01, MOD09,18
Stesura, in collaborazione con QAS del risk assessment per il nuovo ambiente ed aggiornamento del risk assessment relativo al laboratorio controlli positivi.
Valutare la necessità di aggiornamento dei documenti di controllo ambientale e pulizia rispetto all''introduzione del nuovo laboratorio, collaborare alla stesura.
Verificare nella PR05,05 "CHANGE CONTROL" che siano inclusi anche i cambiamenti strutturali ed eventualmente aggiornarla e fare la formazione al personale
Mettere in uso tutta la documentazione aggiornata
Verifica dei risk assessment per il laboratorio controlli positivi e il laboratorio controlli interni e aggiornamento del vmp
Valutare la necessità di aggiornamento dei documenti di controllo ambientale in seguito all''introduzione del nuovo',N'MOD09,18_02 - rac20-23_messa_in_uso_mod0918_0.msg
MOD09,18_02 - rac20-23_messa_in_uso_mod0918_0.msg
planimetria 09/2020 - all-2_pr09,04_planimetria_laboratori_09-2020.pdf
PR05,05 REV06 - rac20-23_messa_in_uso_pr_e_mod_per_ivdr_e_reg_giapppone_1.msg
verifica della documentazione inerente la strumentazione re-installata presso il laboratorio controlli interni durante audit interno (report n°9-20). le attività in uscita sono gestite in associazione all''audit. - 9-20_mod02,02_02_strumenti_interni__rapportovisitaispettiva.pdf
la modifica non è significativa, pertanto non è da notificare. - rac20-23_mod05,36__non_significativa.pdf
MOD09,18_02 - rac20-23_messa_in_uso_mod0918_0_1.msg
SPEC-MA REV01 - rac20-23__messa_in_uso_spec-ma_rev01_rac20-23.msg
PRO5,05 REV06 - rac20-23_messa_in_uso_pr_e_mod_per_ivdr_e_reg_giapppone.msg',NULL,N'05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020',N'05/10/2020
05/10/2020
05/10/2020
05/10/2020
30/10/2020
30/10/2020
05/10/2020',N'11/11/2020
11/11/2020
11/11/2020',N'No',N'No in quanto l''introduzione del nuovo laboratorio non ha impatti sul DMRI, a meno che non vengano redatte nuove Procedure/Istruzioni Operative',N'No',N'No in quanto l''introduzione del nuovo laboratorio non ha impatti sul cliente ',N'No',N'No in quanto l''introduzione del nuovo laboratorio non ha impatti sui prodotti immessi in commercio',N'L''introduzione di un nuovo laboratorio amplia gli spazi di produzione dei CP e CI è quindi migliorativo',N'GM - Gestione delle modifiche
P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',N'GM1 - Individuazione di una modifica e dei possibili impatti/azioni
P3.7 - Controllo/pulizia area lavoro',N'Da valutare necessità di aggiornamento del RA P3.7 per introduzione nuovo laboratorio e GM1 per la mancanza di registrazione della modifica strutturale all''interno del SGQ',N'No',N'vedere MOD05,36. ',N'No',N'L''introduzione di un nuovo laboratorio amplia gli spazi di produzione dei CP e CI è quindi migliorativo, verificare se necessario comunicare questa miglioria al virtula manufacturer ',N'Ferdinando Fiorini',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-10-30 00:00:00',NULL,NULL),
    (N'20-22',N'Preventiva','2020-07-20 00:00:00',N'Silvia Costa',N'Validazioni',NULL,NULL,NULL,N'Progetto',N'Si sono riscontrate 3 problematiche imputabili al fatto che il modello 9 non era ancora stato validato. Questo è il primo prodotto che valida il modello 9.
- Mantis 442: "For all the copies/reaction below 3.0000 (three) and not equal to 0.0000 (zero), the value shall be set to 3.0000 (three)"
- Mantis 443: "the sentence "quantity 
                    ',N'Richiedere una patch del software compatibile con software version 1.3.0.14 che includa i seguenti cambiamenti:
- Mantis 442: "For all the copies/reaction below 3.0000 (three) and not equal to 0.0000 (zero), the value shall be set to 3.0000 (three)"
- Mantis 443: "the sentence "quantity 
                    ',NULL,N'Gestione delle attività in accordo anche con la RAC19-23.
Gestione delle attività in accordo anche con la RAC19-23.
Gestione delle attività in accordo anche con la RAC19-23.
Gestione delle attività in accordo anche con la RAC19-23.
Gestione delle attività in accordo anche con la RAC19-23.
Impatto positivo sulle prossime validazioni
Impatto positivo sulle prossime validazioni
Impatto positivo sulle prossime validazioni
Impatto positivo sulle prossime validazioni
Impatto positivo sulle prossime validazioni
Possibile impatto sull''allocazione delle risorse del team per attività aggiuntive legate al rilascio della nuova patch software.
Possibile impatto sull''allocazione delle risorse del team per attività aggiuntive legate al rilascio della nuova patch software.
Possibile impatto sull''allocazione delle risorse del team per attività aggiuntive legate al rilascio della nuova patch software.
Possibile impatto sull''allocazione delle risorse del team per attività aggiuntive legate al ',N'08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020',N'Supporto a Validazioni per le comunicazioni con il fornitore (PSS) e l''ottenimento della patch con relativa documentazione.
Aggiornamento documento REP2015-042 per la nuova versione 1.3.0.15
Stesura piano validazione per l''installazione software version 1.3.0.15
Revisione report della validazione dell''installazione software version 1.3.0.15
Stesura piano di validazione 
Esecuzione dei test di validazione
Stesura del report di validazione 
Valutare aggiornamento IFU del prodotto BCR-ABL P210 ELITe MGB Kit
Formazione a specialist, MKT e gestione reclami
Revisione del piano validazione per l''installazione della patch sulla software version 1.3.0.15
Esecuzione del test e stesura del report della validazione per l''installazione della patch sulla software version 1.3.0.15
Stesura della TSB per l''installazione della patch
Revisione della TAB del prodotto BCR-ABL P210 ELITe MGB Kit per includere la necessità della patch.
Valutazione notifiche verso autorità competenti
Verificare la signi',N'come da minute di riunione la versione sw 1.3.0.14 legata alla rac19-13 non verrà rilasciata, si attende la versione 1.3.0.15 legata a questa azione correttiva. le attività integrative a supporto della rac20-22 sono elencate e scadenziate nel report di minute. - rac20-22_mod0103_00_release_meeting_ingenius_sw_v_1.3.0.14_rac_19-23_signed.pdf
REP2020-159 firmato il 4/11/2020, REP2020-158 firmato il 4/11/2020, REP2020-156 firmato il 3/11/2020, REP2020-042 firmato il 4/11/2020 - rap20-22_reportpermessainsw.txt
messa in uso sw InGenius V1.3.0.16 - rap20-22_messa_in_uso_sw_ingenius_v1.3.0.16.msg
REP2020-042 firmato il 4/11/2020 - rap20-22_reportpermessainsw_1.txt
PLAN2020-023_Installation Process Validation Plan InGenius SW V1.3.0.15 INT030 RAC19-23 RAP20-22 rev00_ firmato il 9/10/2020 - rap20-22_pianoinstallazionesw.txt
REP2020-159 firmato il 4/11/2020, REP2020-158 firmato il 4/11/2020, REP2020-156 firmato il 3/11/2020, REP2020-042 firmato il 4/11/2020 - rap20-22_reportpermessainsw_2.txt
REP2020-159 firmato il 4/11/2020, REP2020-158 firmato il 4/11/2020, REP2020-156 firmato il 3/11/2020, REP2020-134 firmato il 15/09/2020, REP2020-042 firmato il 4/11/2020 - rap20-22_reportpermessainsw_3.txt
la versione 15 del sw sarà fornita ai clienti esclusivamente in demo come deciso nella riunione del 6/11/2020. verrà installata sugli strumenti presso il cliente la versione 16 - mod0103_00_verbaleriunione_rilascio_versione_1.3.0.15.doc
RTSG07PLD210_IFU rev10, STDG07PLD210_IFU rev04, CTRG07PLD210 IFU rev00 - rap20-22_market_release_-_bcr-abl_p210_-_elite_positive_control-_ctrg07pld210-_scp2019-016.msg
formazione  - rap20-22_webinar_links_sw_1.3.0.16_download_sessions.msg
RAP20-22_TSB 036 - Rev A - SW Version 1.3.0.16 - rap20-22_tsb_036_-_rev_a_-_sw_version_1.3.0.16_1.txt
Il modello 9 non è mai stato associato a prodotti immessi in commercio. per tanto la modifica non è significativa, nè da notificare. - rap20-22_nonsignificativa.pdf
REP2020-159 firmato il 4/11/2020, REP2020-158 firmato il 4/11/2020, REP2020-156 firmato il 3/11/2020, REP2020-042 firmato il 4/11/2020 - rap20-22_reportpermessainsw_4.txt
messa in uso sw InGenius V 1.3.0.15 - rap20-22_messa_in_uso_sw_ingenius_v_1.3.0.15.msg
messa in uso sw InGenius V 1.3.0.16 - rap20-22_messa_in_uso_sw_ingenius_v1.3.0.16_1.msg',NULL,N'08/09/2020
10/09/2020
10/09/2020
10/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020
08/09/2020',N'30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020',N'13/01/2021
11/11/2020
11/11/2020
13/01/2021
11/11/2020
13/01/2021
13/01/2021
06/04/2021
15/01/2021
13/01/2021
13/01/2021
13/01/2021
11/09/2020
17/12/2020
06/04/2021
11/11/2020',N'No',N'-',N'No',N'-',N'No',N'nessun impatto in quanto il modello 9 non è associato a nessun prodotto attualmente in commercio',N'nessun impatto in quanto il modello 9 non è associato a nessun prodotto attualmente in commercio',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'DA VALUTARE ',N'No',N'vedere MOD05,36 Il modello 9 non è mai stato associato a prodotti immessi in commercio. per tanto la modifica non è significativa, nè da notificare.',N'No',N'nessun virtual manufacturer',N'Silvia Costa',N'Si',N'L''impatto delle problematiche rilevate sul modello interpretativo 9 e dell''introduzione della patch sulla gestione dei rischi dei prodotti RTSG07PLD210, STDG07PLD210 e CTRG07PLD210 verrà valutato durante la stesura della relativa documentazione (stesura in corso).
L''impatto delle problematiche rilevate sul modello interpretativo 9 e dell''introduzione della patch sulla gestione dei rischi del prodotto INT030 verrà valutato da PSS nel corso dell''aggiornamento di tale documentazione per il rilascio della versione SW  1.3.0.14 e della patch.','2021-07-31 00:00:00',N'Verificare l''assenza di reclami imputabili al modello 9 (attenzione: la formazione del personale di campo e l''installazione della versione 16 presso i clienti avverrà nel mese di 01/2021)
',NULL,NULL,NULL,NULL,NULL,'2020-10-30 00:00:00',NULL,NULL),
    (N'20-21',N'Preventiva','2020-07-06 00:00:00',N'Lucia  Liofante',N'Ricerca e sviluppo',N'Reclamo',N'20-32',N'962-RTS400ING (STI PLUS)',N'Manuale di istruzioni per l''uso',N'L''indagine eseguita sul database e su 4 campioni del cliente inviati in azienda ha confermato la presenza di fenomeni di segnale aspecifico nel canale del target NG (Neisseria Gonorrhoeae).
Si rileva che i fenomeni di aspecifico sono collegati all''analisi di swab oro-faringei, matrice non validata, e che sono caratterizzati da una Tm inferiore a quella del segnale specifico per NG. 
Come azione di mitigazione si rilascia PER IL CLIENTE una nuova revisione dell''Assay Protocol che sfrutta la Tm come strumento per discriminare segnali specifici da aspecifici (eventuali intervalli di accettazione sono comunque ancora da definire, NON stabiliti a suo tempo in fase di progetto).
Ultreriori test eseguiti su un plasmide recante una sequenza omologa a 2 strain di Neisseria Lactamica hanno dimostrato che RTS400ING può reagire con questi ceppi.
L''analisi di sequenziamento eseguita sui campioni del cliente ha rilevato la presenza di specie batteriche con sequenza non perfettamente omologa a Neisseria Lactamica; resta da determinare se le specie batteriche sequenziate appartengano realmente alla specie Neisseria Lactamica o si tratti di nuovi strain di Neisseria Gonorrhoeae.',N'Nell''ottica di rimarcare i rischi collegati all''utilizzo di matrici non validate in associazione al sistema RTS400ING+InGenius e al fine di supportare l''utilizzatore finale nel discernere tra campioni bassi positivi per N. gonorrhoeae e segnali aspecifici, si definiscono i seguenti interventi:
1) revisione IFU SCH mRTS400ING: 
- inserimento disclaimer che sottolinei la mancanza di dati su tamponi rettali e uretrali 
- inserimento disclaimer relativo a problematiche di cross-reattività riscontrate su tamponi oro-faringei
- aggiornare le sezione "Marcatori potenzialmente interferenti" riportando la cross-reattività con N. Lactamica, ma non a livello delle matrici validate
2) esecuzione test di cross-reattività e interferenza da parte di R&D onde approfondire le prestazioni di RTS400ING in tal senso
3) revisione dell''AP di RTS400ING con inserimento del ciclo di dissocazione e consequente calcolo della Tm',NULL,N'Impatti positivi: possibile riferirsi all''IFU nel presentare al cliente i rischi collegati all''utilizzo OFF-LABEL del prodotto; si ritiene inoltre che l''introduzione del calcolo della Tm possa fornire al cliente ulteriori certezze sull''affidabilità della diagnosi, evitando l''intervento del Supporto Applicativo per l''interpretazione dei risultati.
Impatto positivo: netta definizione sull''IFU dell''uso previsto per il prodotto e dei rischi collegati a utilizzi OFF-LABEL 
Impatto positivo: netta definizione sull''IFU dell''uso previsto per il prodotto e dei rischi collegati a utilizzi OFF-LABEL 
Impatto positivo: netta definizione sull''IFU dell''uso previsto per il prodotto e dei rischi collegati a utilizzi OFF-LABEL 
L''aggiornamento del IFU mira a rimarcare i rischi collegati all''utilizzo di matrici non validate in associazione al sistema RTS400ING+InGenius e al fine di supportare l''utilizzatore finale nel discernere tra campioni bassi positivi per N. gonorrhoeae e segnali aspecifici, l''impatto della modif',N'30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
21/07/2020
21/07/2020',N'creazione TAB
Stesura avvertenza; revisione, approvazione e messa in uso nuova revisione IFU
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica 
messa in uso nuova revisione AP e aggiornamento FTP con i Report dei test condotti da R&D e l''eventuale gestione dei rischi aggiornata
Inserimento sull''IFU:
- dei disclaimer relativi all''utilizzo di tamponi rettali/uretrali/oro-faringei
- della segnalazione di cross-reattività con Neisseria Lactamica
Esecuzione test di cross-reattività e interferenza con N. Lactamica
Verifica AP
Stesura avvertenza e messa in uso nuova revisione IFU
revisione IFU
Aggiornamento AP con inserimento ciclo di dissociazione per calcolo Tm',N'ifu sti plus rev03 - rap20-21_ifu_sti_plus_1.msg
ifu sti plus rev03 - rap20-21_ifu_sti_plus.msg
la modifica è significativa, pertanto è da notificare ai paesi presso cui il prodotto è registrato ed a DEKRA - rap20-21_mod05,36_significative.pdf
notifica preliminare effettuata - rap20-21_qualita_egspa_sgq_prior_notification_of_change_-rts400ing_-_rap20-21.msg
notifica preliminare dekra - rap20-21_eg_spa_notice_of_change_rts400ing.msg
notifica modifica attuata dekra - rap20-11_notice_of_change_modifica_attuata.msg
notifica modifica attuata a distributori - rap20-21_prior_notification_modifica_attuata.msg
Report: REP2018-136 Rev.01 e REP2018-137 Rev.01  - rap20-21_aggiornamento_ftp_sti_plus.msg
messa in uso ap con aggiunta tm - rap20-21_messa_in_uso_ap_sti_plus_rev02.msg
Pianificazione test di cross-reattività e interferenza con N. Lactamica - rap20-21_pianificazione_test_entro_30-09-2020_1.txt
rep2018-136 rev01 - rep2018-136_interfering_organisms_scp2017-030_01_final.docx
rep2018-137 rev01 - rep2018-137_absence_of_inhibition_scp2017-030_01_final.docx
AP sti plus con l''aggiunta della Tm, rev02 - rap20-21_messa_in_uso_ap_sti_plus_rev02.msg
TAB012 DEL 14/10/2020 - rap20-21_rev_ab_-reagent_database_update_for_automatic_scan.pdf
ifu sti plus rev03 - rap20-21_ifu_sti_plus_3.msg
AP sti plus con l''aggiunta della Tm, rev02 - rap20-21_messa_in_uso_ap_sti_plus_rev02_1.msg',NULL,N'30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
21/07/2020
21/07/2020',N'30/10/2020
30/10/2020
30/10/2020',N'14/10/2020
11/12/2020
11/12/2020
15/01/2021
11/12/2020
11/12/2020
11/11/2020
11/12/2020
11/11/2020',N'No',N'nessun impatto',N'Si',N'effettuata con l''avvertenza legata all''IFU',N'No',N'nessun impatto',N'L''impatto sul prodotto è positivo in quanto si rimarcano i rischi collegati all''utilizzo di matrici non validate, integrando l''IFU.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.2 - Fase di Pianificazione: Definizione requisiti',N'da valutare',N'Si',N'vedere MOD05,36. La modifica è significativa in quanto mira a rimarcare i rischi collegati all''utilizzo di matrici non validate, introducendo avvertenze con impatto sull''efficacia del prodotto.',N'No',N'nessun virtual manufacturer',NULL,N'No',N'In base ai dati attualmente a disposizione non occorre aggiornare la documentazione di gestione dei rischi. Il pericolo impattato è il pericolo A.2.1 a "Errore nell''interpretazione dell''uso previsto (ruolo del dispositivo medico nella diagnosi, monitoraggio, trattamento della malattia, natura dei campioni)". L''evento rilevato non modifica la valutazione del rischio relativo. Inoltre le azioni messe in atto descritte nella RAP rafforzano le misure di mitigazione del rischio già previste nella documentazione di gestione del rischio relativamente al pericolo A.2.1 a. A seguito della disponibilità degli esiti dei test di cross-reattività eseguiti da R&D occorrerà valutare nuovamente la necessità di aggiornamento della documentazione di gestione dei rischi. Il rilievo di un''eventuale cross-reattività con il patogeno Neisseria lactamica richiederà infatti di valutare l''impatto sui pericoli già identificati e sui relativi rischi stimati e valutati, in particolare sui pericoli A.2.10 b, A.2.11 b, B.2 g e B.2 h.','2021-05-17 00:00:00',N'Valutare assenza reclami con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2020-10-30 00:00:00','2021-01-15 00:00:00',NULL),
    (N'20-5',N'Correttiva','2020-03-02 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Non conformità',N'19-105',N'Strumento InGenius',N'Documentazione SGQ',N'La causa primaria della mancata dispensazione nel track 12 da parte dello strumento Elite Ingenius è riconducibile ad un errore tecnico di dispensazione del tubo di preCQ, il lotto di preCQ presentava volumi inferiori a 280 µL per tubo (tra 265 e 275 µL).
Nel corso dell''indagine è stata evidenziata una differenza nell''accuratezza di dispensazione degli strumenti Elite Ingenius di CQ di +2 µL, rispetto a quanto dichiarato da PSS sull''IFU dello strumento +1 µL (un errore del 5%).
Il service ha eseguito dei test sostituendo il single nozzle con l''ultima versione del componente, ma non è stato evidenziato un miglioramento per quanto riguarda l''accuratezza: la dispensazione è risultata ancora di 22 µL anziché 20 µL. (wo 6943/2019 e 6992/2019)',N'Per il calcolo dell''accuratezza PSS ha effettuato i test in condizioni ambientali di temperatura e pressione controllate ed utilizzando una bilancia di precisione per pesare i volumi dispensati, ottenendo un''accuratezza del +/-5%, stesso dato riportato sul documento "System Requirements" del nuovo strumento BeGenius. Per la verifica dell''accuratezza EGSpA ha effettuato i test misurando i volumi dispensati con le micropipette.
Per mitigare la problematica legata alla dispensazione da parte dello strumento InGenius degli ultimi tubi, si chiede di aggiornare l''attuale procedura di produzione in relazione al controllo volumetrico inserendo anche i prodotti validati su Ingenius  e di aumentare il volume fornito per tubo dei prodotti monoreagenti contenenti 280uL al fine di ridurre al minimo la possibilità di avere volumi inferiori negli ultimi track non identificabili dallo strumento e la modifica dei moduli di produzione per i prodotti a 280µL. Accertare con PSS i motivi della discordanza dei dati ottenuti.',NULL,N'La modifica richiesta recepisce quanto già evidenziato a livello sperimentale. L''accuratezza maggiore deve essere tenuta in considerazione nella stesura dei documenti e nella preparazione dei reagenti durante le sessioni.
La modifica richiesta recepisce quanto già evidenziato a livello sperimentale. L''accuratezza maggiore deve essere tenuta in considerazione nella stesura dei documenti e nella preparazione dei reagenti durante le sessioni.
La modifica richiesta recepisce quanto già evidenziato a livello sperimentale. L''accuratezza maggiore deve essere tenuta in considerazione nella stesura dei documenti e nella preparazione dei reagenti durante le sessioni.
La modifica richiesta recepisce quanto già evidenziato a livello sperimentale. L''accuratezza maggiore deve essere tenuta in considerazione nella stesura dei documenti e nella preparazione dei reagenti durante le sessioni.
Accertare con PSS i motivi della discordanza dei dati ottenuti.
A livello sperimentale, attualmente il test di "Tolleranza',N'20/03/2020
20/03/2020
20/03/2020
20/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020',N'Supporto a R&D per la valutazione dei nuovi criteri da indicare nell''IFU 
Monitoraggio delle problematiche a livello di CQ circa la dispensazione insufficiente
Modifica dei moduli di produzione per i prodotti RTSXXXING da 280 µL a 290 µL per tubo
Modifica dei moduli di produzione per i prodotti RTSXXXING da 280 µL a 290 µL per tubo
Accertare con PSS i motivi della discordanza dei dati ottenuti.
Nessuna modifica delle IFU dei prodotti.
Modifica e messa in uso dell''IFU 
messa in uso documenti
Valutare la significatività della modifica
per dispensare il nuovo volume con il dispensatore automatico DA9, creare lo script ING290
Approvazione nuovi documenti
Revisione anagrafiche prodotti
per dispensare il nuovo volume con il dispensatore automatico DA9, creare lo script ING290
Valutare se la modifica richiede una nuova analisi dei costi e revisione dei costi.',N'04/2021: dal monitoraggiodei lotti non sono emerse problematiche a livello di CQ circa la dispensazione insufficiente - rac20-5_nessunproblemadispensazione.txt
Come da report allegato, non ci sono perciò evidenze che richiedano alcuna azione correttiva in merito agli strumenti o ai risultati con essi ottenuti - rac20-5_gsc-rdm_considerazioni_in_merito_alla_rac20-5.docx
la modifica non è significativa, pertanto non è da notificare. - rac20-5_nonsignificativa.pdf
creazione script ing290 rev 00 - ing290_00.med__1.pdf
elenco documenti - rac20-5_elenco-doc-da-modificare.xlsx
MOD962-RTS401ING-48 MOD962-RTS150ING MOD962-RTS140ING MOD962-RTS200ING MOD962-RTS202ING-48 MOD962-RTS120ING - rac20-5_messa_in_uso_mod962-rtsxxxing.msg
MOD962-RTS120ING MOD962-RTS121ING  MOD962-RTS140ING MOD962-RTS150ING MOD962-RTS200ING MOD962-RTS201ING MOD962-RTS202ING-48 MOD962-RTS300ING MOD962-RTS401ING-48 MOD962-RTSD00ING - cc21-12_messa_in_uso_mod962-rtsxxing_2.msg
MOD962-RTS400ING_02 - cc21-12_mod962-rts400ing_02_4.msg
MOD955-40X-XXX MOD955-20X-XXX ALTRI IC - cc20-79_moduli_prod_2.msg
elenco - rac20-5_elenco-doc-da-modificare_1.xlsx
elenco documenti / codici da modificare - rac20-5_elenco-doc-da-modificare_2.xlsx
elenco volumi da modificare - rac20-05_modifica_volumi_ing.xlsx
per dispensare il nuovo volume con il dispensatore automatico DA9, è stato creato lo script ING290 - ing290_00.med_.pdf
MOD962-RTS120ING MOD962-RTS121ING MOD962-RTS140ING MOD962-RTS150ING MOD962-RTS200ING MOD962-RTS201ING MOD962-RTS202ING-48 MOD962-RTS300ING MOD962-RTS401ING-48 MOD962-RTSD00ING - cc21-12_messa_in_uso_mod962-rtsxxing_3.msg
mod962-400ing_02 - cc21-12_mod962-rts400ing_02_5.msg
MOD955-40X-XXX MOD955-20X-XXX ALTRI IC - rac20-5_moduli_prod.msg',NULL,N'02/03/2020
20/03/2020
20/03/2020
20/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020
27/03/2020',N'30/04/2020
31/07/2020
30/04/2020
30/04/2020
30/10/2020
27/03/2020
30/10/2020
30/10/2020',N'28/04/2021
19/04/2021
27/03/2020
27/03/2020
19/04/2021
27/03/2020
30/04/2020
19/04/2021
19/04/2021
30/04/2020',N'No',N'nessun impatto',N'No',N'non necessaria, l''implementazione del volume non ha impatto sulle IFU  (l''aumento dei volumi per tubo assicura una sufficiente quantità di reagente da dispensare riducendo al minimo gli effetti di dispensazioni inaccurate)',N'No',N'nessun impatto',N'L''aumento dei volumi per tubo assicura una sufficiente quantità di reagente da dispensare riducendo al minimo gli effetti di dispensazioni inaccurate (come identificate durante l''utilizzo in condizioni reali dello strumento).',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.14 - Misurazione e dispensazione semilavorati',N'l''impatto sul RA P1.14 sarà da valutare durante l''analisi annuale che sommerà tutti gli errori di dispensazione registrati',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2021-10-29 00:00:00',N'verificare assenza di NC analoghe a seguito di utilizzo del volume da 290ul',NULL,NULL,NULL,NULL,NULL,'2020-10-30 00:00:00','2021-04-28 00:00:00',NULL),
    (N'17-22',N'Correttiva','2017-09-18 00:00:00',N'Roberta  Paviolo',N'Assistenza strumenti esterni',N'Non conformità',NULL,N'17-106',NULL,N'I MOD11,19 in versione elettronica non sono stati validati prima dell''uso.',N'I moduli MOD11,19 "Elitech ELITe InGenius preventive maintenance" devono essere firmati dal l''esecutore dell''intervento, a seguito di verifica del risultato ottenuto, e dal cliente, quindi scansionati ed allegati a SGAT. Per utilizzare questi documenti in formato elettronico è necessario validarli per la parte compilabile e di esecuzione di calcoli.',NULL,N'Validazione del MOD11,19.
Validazione del MOD11,19.',N'29/09/2017
29/09/2017',N'Validazione del MOD11,19.
è stato creato un nuovo modulo di PQ "MOD11,26 Elitech Elite Ingenius PQ Report" che verrà usato in caso di installazione, manutenzione preventiva ed intervento di servizio per eseguire/registrare la PQ (come richiesto dall''OSS5 in uscita dall''audit del 17/7/17, nella quale si richiedeva di specificare quando è necessario eseguire le PQ a seguito di riparazione). Tale modulo andrà a sostituire i precedenti.
MOD11,26 rev00 in uso al 31/01/2018 come da mail allegata, formazione (MOD18,02) del 30/01/2018. il modulo è stato validato: MOD05,23 Piano di validazione firmato il 2/11/17, MOD05,24 firmato il 22/01/18.
archiviazione dei MOD11,19 e MOD11,16 alla revisione delle procedure di Installazione e PM prima di renderli obsoleti',N'mod11,26rev00 - rac17-22_messa_in_uso_mod11,26rev00.pdf
archiviareMOD11,16eMOD11,19dopo revisione procedure installazione e manutenzione - rac17-22_integrazione_attivita_di_archiviazione_documenti_a_seguito_di_revisione_procedure.pdf
MOD11,16 rev02 - rac17-22_messa_in_uso_mod11,16rev02.pdf',NULL,N'29/09/2017
31/01/2018',N'30/11/2017',N'31/01/2018
06/03/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'AST - Assistenza tecnica strumenti',NULL,N'Assistenza tecnica. 31/01/2018 con l''introduzione del nuovo modulo MOD11,26 aggiornare il VMP AST e valutare se la modifica ha impatto sui RA AT3, AST5, AST6. non ha impatto sui risk assessment, il VMP verrà aggiornato insieme ad altre modifiche di processo.',N'No',NULL,N'No',NULL,N'Marcello Pedrazzini',N'No',N'non necessario','2018-10-30 00:00:00',N'Il modulo MOD11,19 validato ed in versione bloccata non ha mai  causato NC.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-11-30 00:00:00','2018-03-06 00:00:00','2018-10-30 00:00:00'),
    (N'20-36',N'Correttiva','2020-10-20 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',NULL,N'RTS170ING',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'Il test di ripetizione con 6 replicati ha dato esito conforme, pertanto il dato evidenziato nella prima sessione di CQ è causata da un problema collegato al CQ. Il residuo di diluizione del punto PC1:100 non superato è stato amplificato nell''ambito della ripetizione con esito conforme. La variabilità riscontrata nel tubino di diluzione può essere ricondotto a una preparazione non corretta la cui causa può risiedere nei volumi eccessivamente bassi del PC utilizzato (2µL). Si chiede pertanto la modifica delle modalità di preparazione del campione.',N'Si chiede la modifica delle modalità di CQ per quanto concerne la preparazione del punto PC1:100 + IC. In particolare la modifica si suddivide in due step: il primo riguarda la modifica dei volumi del PC da utilizzare, infatti 2 µL è un volume mai utilizzato in CQ ed eccessivamente piccolo da non garantire la riproducibilità a causa di errori minimi. Il secondo riguarda la valutazione della stabilità della diluizione al fine di preparare un lotto più grande e ridurre tempi e variabilità in CQ.',NULL,N'La modifica dovrebbe garantire una maggior riproducibilità dei dati ottenuti per il campione diluito. La preparazione di uno stock di diluizione dovrebbe ridurre i tempi di allestimento del cq e garantire la riproducibilità tra i test
Nessun impatto sul processo
Nessun impatto sul processo
Nessun impatto sul processo',N'20/10/2020
27/10/2020
27/10/2020
27/10/2020',N'Modifica urgente: Modifica del MOD10,13-RTS170ING aumentando i volumi del PC nella diluzione del campione PC1:100
Modifica secondaria: In base alla stabilità della diluizione stock del campione PC1:100 preparare un modulo di preparazione della diluzione e modificare il MOD10,13 per l''utlizzo della stessa
Test di stabilità dello stock del campione PC1:100
messa in uso dei documenti
eventuale modifica del DMRI (modifica secondaria)
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica',N'MOD10,13-ctr170ing rev02 - rac20-36_messa_in_uso_mod1013-ctr170ing.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - rac20-36_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020.msg
MOD955-CTR-RNA_4ng-Ul_02, MOD18,02 del 19/11/2020 - rac20-36_messa_in_uso_mod955-ctr-rna_4ng-ul_02__mod1802_del_19112020.msg
Il secondo step, riguardante la valutazione della stabilità della diluizione al fine di preparare un lotto più grande e ridurre tempi e variabilità in CQ,  si ritiene non necessario in quanto invece di usare il PC si utilizza lo STD. - rac20-36_secondostepnonnecessario.txt
MOD10,13-ctr170ing rev02 - rac20-36_messa_in_uso_mod1013-ctr170ing_1.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - rac20-36_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_1.msg
MOD955-CTR-RNA_4ng-Ul_02, MOD18,02 del 19/11/2020 - rac20-36_messa_in_uso_mod955-ctr-rna_4ng-ul_02__mod1802_del_19112020.msg
la modifica secondaria non si ritiene necessaria, i DMRI NON sono da aggiornare - rac20-36_secondostepnonnecessario_1.txt
la modifica è significativa, pertanto è da notificare, ma solo nei paesi cui sono stati inviati i moduli di cq (essendo questi documenti strettamente confidenziali) - rac20-36_mod05,36__significativa.pdf
i moduli di CQ non sono stati inviati a nessuna autorità competente, si ritiene pertanto non necessario effettuare la notifica del cambiamento in quanto trattasi di documenti strettamente confidenziali. - rac20-36_notificanonnecessaria.txt',NULL,N'20/10/2020
27/10/2020
27/10/2020
27/10/2020',N'20/11/2020
30/11/2020',N'20/11/2020
20/11/2020
20/11/2020
04/11/2020',N'Si',N'da aggiornare',N'No',N'nessun impatto',N'No',N'nessun impatto',N'la modifica del cq ne  garantisce la riproducibilità, a differenza degli attuali volumi eccessivamente piccoli che non garantivano la riproducibilità a causa di errori minimi.',N'CQ - Controllo Qualità prodotti',N'CQ10 - Fase analitica CQ: Preparazione campioni / allestimento delle reazioni',N'impatto su RA CQ10 da valutare a fine anno in base agli eventi',N'Si',N'si, solo nei paesi a cui sono stati inviati i moduli di cq (in quanto contengono informazioni confidenziali)',N'No',N'nessun virtula manufacturer',NULL,N'No',NULL,'2021-04-30 00:00:00',N'verificare assenza NC con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2020-11-30 00:00:00','2020-11-20 00:00:00',NULL),
    (N'20-26',N'Correttiva','2020-08-27 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-74',N'RTSD00ING "Coagulation ELITe MGB® Kit"',N'Documentazione SGQ',N'Il lotto in oggetto è risultato non conforme a causa di una TM rilevata per il canale 4 ( campione MTHFR Tm wt) in 1 dei 4 campioni NTC. 
Il test di ripetizione ha confermato la presenza saltuaria di tale Tm (1 campione su 8). 
La Tm rilevata sembra collegabile alla presenza di una minima fluorescenza aspecifica.
Si chiede l''accettazione in deroga del lotto e la modifica dei criteri di CQ per i campioni NTC.',N'Si chiede la modifica dei criteri di CQ prevedendo l''utilizzo dei criteri di validità del controllo negativo dell''AP per l''utilizzatore, ovvero il solo esame dei Ct per la valutazione dei campioni NTC. Gli attuali criteri di CQ prevedono la valutazione anche della presenza della Tm, dato che in assenza di ct determinati non influisce sull''utilizzo finale del prodotto, e come nel caso in oggetto, può essere collegabile alla presenza di una minima fluorescenza aspecifica.',NULL,N'I criteri devono essere modificati valutando eventuali contaminazioni dei reagenti che vadano ad inficiare le prestazioni di prodotto, ma in grado di accettare bassi livelli di contaminazione o segnali aspecifici non influenti sulla validazione del controllo negativo.
Questa modifica dei criteri di CQ non influisce sul prodotto ma mira a considerare eventuali contaminazioni dei reagenti/aspecifici, che altrimenti andrebbero ad inficiare il risultato del CQ a causa di variabilità non influenti sulle prestazioni del prodotto.
nessun impatto in termini di processo ,ma attività da pianificare',N'07/09/2020
07/09/2020
07/09/2020',N'modifica del mod10,12-RTSd00ing e formazione
Valutazione dei nuovi criteri di CQ per i campioni NTC.
messa in uso dei documenti',N'MOD10,12-RTSD00ING-CTRD00ING_06, MOD18,02 del 2/11/2020 - rac20-26_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_06_2.msg
la modifica è significativa, pertanto è da notificare, ma solo nei paesi cui sono stati inviati i moduli di cq (essendo questi documenti strettamente confidenziali) - rac20-26_mod05,36__significativa.pdf
i moduli di CQ non sono stati inviati a nessuna autoritÃ  comptente, si ritiene pertanto non necessario effettuare la notifica del cambiamento in quanto trattasi di documenti strattamente confidenziali. - rac20-36_notificanonnecessaria_1.txt
MOD10,12-RTSD00ING-CTRD00ING_06, MOD18,02 del 2/11/2020 - rac20-26_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_06.msg
MOD10,12-RTSD00ING-CTRD00ING_06, MOD18,02 del 2/11/2020 - rac20-26_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_06_1.msg',NULL,N'07/09/2020
07/09/2020
07/09/2020',N'30/11/2020
30/11/2020
30/11/2020',N'03/11/2020
03/11/2020
03/11/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Questa modifica dei criteri di CQ non influisce sulla qualità del prodotto ma mira a considerare eventuali contaminazioni dei reagenti, che altrimenti andrebbero ad inficiare le performance di prodotto, e ad accettare i bassi livelli di contaminazione o segnali aspecifici non influenti sulla validazione del controllo. ',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'A fine anno valutare quante modifiche dei ciretri di CQ sono emerse e che impatto hanno sul RA R&D1.9',N'No',N'vedere MOD05,36',N'No',N'Nessun virtual manufacturer',NULL,N'No',NULL,'2020-05-30 00:00:00',N'Valutare assenza NC con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,'2020-11-30 00:00:00','2020-11-03 00:00:00',NULL),
    (N'20-13',N'Preventiva','2020-04-14 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Reclamo',N'19-117',N'RTS400ING - STI PLUS ELITe MGB Kit',N'Manuale di istruzioni per l''uso',N'I test eseguiti da EG Spa sullo strumento e sui reagenti rientrati dal Cliente hanno escluso problemi di contaminazione causati dallo strumento InGenius o legati ai reagenti (Mix, CPE).La problematica di falsi positivi lamentata dal cliente sembra quindi connessa a fenomeni di contaminazione avvenuti in fase pre-analitica, in particolare durante la preparazione dei campioni Cervical Swab. Anche se non è stato possibile identificare la causa, le cause potrebbero essere riconducibili alla manipolazione dei Cervical Swab e in particolare ai seguenti due passaggi: a) contaminazione del laboratorio imputabile allo smaltimento dei tamponi cervico-vaginali usati e altamente positivi b) cross-contaminazione da un campione all''altro dovuta ad uso non appropriato della micropipetta nel processo di trasferimento del liquido contenuto all''interno del tubo del tampone al tubo dell''InGenius.',N'Seppur il reclamo sia da ritenersi NON FONDATO poiché NON imputabile direttamente ai prodotti EGSPA, si ritiene comunque necessaria un''azione a livello degli utilizzatori per rinforzare le precauzioni da adottare nella fase pre-analitica per evitare il verificarsi di fenomeni contaminazione.Tra le possibili azioni si propongono le seguenti:1) dettagliare maggiormente, rispetto a quanto fatto ora all''interno dell''IFU del prodotto, le misure da adottare per ridurre il rischio di contaminazione della fase pre-analitica 2) aggiungere uno o più punti nel Troubleshooting dell''IFU che forniscano all''utilizzatore dei criteri per valutare se una seduta analitica possa essere ritenuta affetta da contaminazione 3) Valutare l''estensione della modifica alle IFU anche ad altri kit che per tipo di campioni analizzati (es. swabs) potrebbero essere più a rischio di contaminazioni di altri  4) porre in risalto le criticità legate alla fase pre-analitica durante il training agli utilizzatori finali e al personale applicativo dei distributori/ICO e produrre una Nota Tecnica da diffondere a tutto il Supporto Tecnico che tratti l''argomento.',NULL,N'A regime nessun impatto in quanto non è prevista alcuna modifica ai prodotti o ai metodi.
Impatto positivo, l''aggiornamento del IFU con le misure da adottare per ridurre il rischio di contaminazione in fase pre-analitica e l''integrazione dei troubleshooting riducono il rischio che l''utilizzatore non individui una seduta analitica  affetta da contaminazione. Tale aggiornamento deve essere esteso a tutti i prodotti che utilizzano gli swabs.
Impatto positivo, l''aggiornamento del IFU con le misure da adottare per ridurre il rischio di contaminazione in fase pre-analitica e l''integrazione dei troubleshooting riducono il rischio che l''utilizzatore non individui una seduta analitica  affetta da contaminazione. Tale aggiornamento deve essere esteso a tutti i prodotti che utilizzano gli swabs.
Impatto positivo, l''aggiornamento del IFU con le misure da adottare per ridurre il rischio di contaminazione in fase pre-analitica e l''integrazione dei troubleshooting riducono il rischio che l''utilizzatore non individui ',N'08/05/2020
08/05/2020
08/05/2020
08/05/2020
08/05/2020
08/05/2020
08/05/2020
12/05/2020
12/05/2020
12/05/2020
12/05/2020',N'revisione ed approvazione delle IFU 
Creazione, entro il prossimo training, di un modulo specifico all''interno delle sessioni di training per tutto il personale tecnico di campo (EG e terze parti) relativo al rischio di contaminazione (misure di prevenzione e di criteri di riconoscimento) 
Stesura di una nota informativa per tutto il personale tecnico di campo (EG e terze parti) che divulghi le informazioni relative al rischio di contaminazione (misure di prevenzione e di criteri di riconoscimento) 
valutazione sull''aggiornamento di tutte le IFU di prodotto che necessitano la manipolazione dei campioni d''origine attraverso swabs (vedere elenco allegato)
Aggiornamento delle IFU con rafforzamento delle misure di prevenzione della contaminazione nella sezione "Campioni e Controlli"/"Tamponi cervico-vaginali" e aggiunta dei criteri di allerta per il rischio di contaminazione nel troubleshooting IFU RTS400ING - STI PLUS ELITe MGB Kit  e revisione della nota tecnica.
revisione, approvazione e messa in IFU',N'modulo specifico all''interno delle sessioni di training - rap20-13_ingenius_residual_risks_overview_-_operator__service_2020.pptx
SCH mRTS400ING_02 - rap20-13_ifu_rts400ing_rap20-13.msg
SCH mRTS400ING_02 - rap20-13_ifu_rts400ing_rap20-13_1.msg
aggiornamento IFU dei prodotti che utilizzano swabs alla prima revisione  - rap20-13_aggiornareifuprodotticheutilizzanoswabsallaprimarevdelamanuale.txt
Notice of change DEKRA del 06/05/2020 - noc_eg_spa_sti_plus_rap20-13.pdf
Notice of change DEKRA del 06/05/2020 - rap20-13_mail_noc.pdf
Notifica alle autorità competenti dei paesi in cui il prodotto è registrato - rap20-13_prior_notification_of_change_-rts400ing-_rap20-13.msg
La modifica è significativa pertanto da notificare. - rap20-13_mod05,36_significativa.pdf
notifica ai distributori per cambiamento avvenuto. - rap20-13_egspa_sgq_prior_notification_of_change_-rts400ing-_rap20-13.msg
notifica a DEKRA per cambiamento avvenuto - rap20-13_comunicazione_a_dekra_cambiamento_avvenuto.msg
nota informativa per tutto il personale tecnico di campoche divulghi le informazioni relative al rischio di contaminazione (misure di prevenzione e di criteri di riconoscimento)  - rap20-13_2020_09_01_technicaladvisory_applicationteam.pdf
elenco prodotti che usano swabs - rap_20-13_elenco_prodotti_che_utilizzano_swabs.msg
aggiornamento IFU dei prodotti che utilizzano swabs alla prima revisione - rap20-13_aggiornareifuprodotticheutilizzanoswabsallaprimarevdelamanuale_1.txt
SCH mRTS400ING_02 - rap20-13_ifu_rts400ing_rap20-13_2.msg',NULL,N'08/05/2020
08/05/2020
08/05/2020
08/05/2020
08/05/2020
12/05/2020
12/05/2020
12/05/2020',N'29/05/2020
29/05/2020
29/05/2020
29/05/2020
29/05/2020
30/11/2020
31/12/2020
29/05/2020',N'05/06/2020
08/09/2020
08/09/2020
02/09/2020
09/06/2020
05/06/2020
24/07/2020',N'No',N'No in quanto la revisione dell''IFU non comporta l''aggiornamento del DMRI',N'Si',N'Si con nuova revisione dell''IFU e nota tecnica (TAB)',N'No',N'No in quanto non comporta modifiche nell''utilizzo del prodotto ',N'L''aggiornamento delle IFU, dei prodotti che necessitano della manipolazione dei campioni d''origine attraverso swabs, con le misure da adottare per ridurre il rischio di contaminazione in fase pre-analitica e l''integrazione dei troubleshooting riducono il rischio che l''utilizzatore non individui una seduta analitica  affetta da contaminazione.',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'Da valutare nel corso del riesame periodico ',N'Si',N'vedere MOD05,36',N'No',N'No in quanto il prodotto non è oggetto di un contratto con un Virtual Manufacturer',NULL,N'No',N'Pericolo b.2.r "Contaminazione da carryover o da campioni" già mitigato nell''analisi dei rischi corrente attraverso la chiara identificazione dell''uso, dell''utilizzatore e dell''area di lavoro previsti, attraverso la stesura di dettagliate procedure di utilizzo del prodotto, la formazione degli utilizzatori e l''utilizzo del prodotto in associazione al sistema integrato e automatizzato ELITe InGenius. Tutte le indicazioni per l''utilizzatore sono riportate sul manuale di istruzioni per l''uso, comprese quelle relative all''impiego dei campioni clinici,  e si rimanda alle sezioni di "Avvertenze e Precauzioni, "Limiti della Procedura" e "Problemi e Soluzioni" per ulteriori precisazioni.','2020-11-30 00:00:00',N'Assenza di reclami imputabili a fenomeni di cross-contaminazione riconducibili alla fase pre-analitica.
01-2021 da giugno/2020 (data di messa in uso della nuova IFU) non ci sono stati altri reclami imputabili a fenomeni di cross-contaminazione riconducibili alla fase pre-analitica. l''azione preventiva rimane aperta per procedere all''aggiornamento delle IFU dei prodotti che usano swabs.',NULL,NULL,NULL,NULL,NULL,'2020-11-30 00:00:00',NULL,NULL),
    (N'17-32',N'Correttiva','2017-12-21 00:00:00',N'Stefania Brun',N'Regolatorio',N'Non conformità',N'17-153',N'Dekra Report 2221510-TDR10-R0',N'Documentazione SGQ',N'The IVDD Annex I ER 1 and EN ISO 14971:2012 Annex ZC state that  the manufacturer must undertake the overall risk-benefit analysis (weighing all risks combined against the benefit) in all cases, regardless of the application of criteria established in the management plan. Differently the EG SpA procedure PR22,01 "GESTIONE DEI RISCHI" states that a risk-benefit analysis needs to be performed only when an AFAP level is detected. In the case of Respiratory Bacterial all risks resulted acceptable, no AFAP level was detected, for this reason and according to PR22,01 the statement required by Dekra was not included.',N'The following actions will be performed:
- update of the MOD22,04 rev.00 of the products Respiratory Bacterial ELITe MGB Kit (RTS553ING) and Respiratory Bacterial -ELITe Positive Control (CTR533ING) to include the statement required by Dekra
- update of the EG SpA procedure PR22,01 "Gestione dei rischi" rev.01 to clarify that, according to IVDD Annex I ER 1 and EN ISO 14971:2012 Annex ZC an overall risk-benefir analysis must take place in any case and not only when an AFAP level is detected',NULL,N'The update of the procedure PR22,01will require to perform an overall risk-benefit analysis weighing all risks combined against the benefit in all cases
The update of the procedure PR22,01will require to perform an overall risk-benefit analysis weighing all risks combined against the benefit in all cases
The update of the procedure PR22,01will allow to be compliant with IVDD, Annex I, ER 1 and EN ISO 14971:2012 Annex ZC',N'21/12/2017
21/12/2017
21/12/2017',N'Update MOD22,04 of the products Respiratory Bacterial ELITe MGB Kit (RTS553ING) and Respiratory Bacterial -ELITe Positive Control (CTR533ING)  to include the statement required by Dekra.
MOD22,04
RTS553ING AND CTR553ING MOD22,04 Rev.01 of 22/12/2017
Update procedure PR22,01, put it in place and perform a training on it
Approve  procedure PR22,01',N' PR22,01_02, MOD18,02 del 16/9/19  - rac17-32_messa_in_uso_pr2201_02_rap18-21_rap17-32_mod1802_del_160919_1.msg
 PR22,01_02, MOD18,02 del 16/9/19  - rac17-32_messa_in_uso_pr2201_02_rap18-21_rap17-32_mod1802_del_160919.msg',NULL,N'21/12/2017
22/12/2017
21/12/2017',N'31/12/2017
31/01/2018
31/01/2018',N'22/12/2017
16/09/2019
16/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'non necessaria',N'aggirnamento della procedura anche dettagliando i requisiti MDSAP',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'R&D1.10',N'No',N'non necessaria',N'No',N'non necessaria',N'Renata  Catalano',N'Si',N'update MOD22,05 of the products Respiratory Bacterial ELITe MGB Kit (RTS553ING) and Respiratory Bacterial -ELITe Positive Control (CTR533ING)','2019-12-30 00:00:00',N'To verify the compliance of a MOD22,04 produced according to the new revision of PR22,01.

A form MOD22,04 "Evaluation of overall residual risk acceptability" Rev.04, 28/02/2020, produced according to the new revision of PR22,01.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2019-09-16 00:00:00','2020-06-08 00:00:00'),
    (N'17-31',N'Correttiva','2017-12-21 00:00:00',N'Stefania Brun',N'Regolatorio',N'Non conformità',N'17-152',N'Dekra report N° 2221510-TDR10-R0',NULL,N'The organization is aware of the 98/79/EC prescription which requires CE mark to be accompanied by the Notified Body''s identification number on the labeling and instruction for use of the device. Nevertheless the IFU was submitted without the identification number because that prescription was mistakenly thought to be applied only to the final IFU release (after Notified Body certification) by the personnel in charge of reviewing and approving the IFU. The same case was found in a previous product submission to DEKRA.The item of interest is NOT properly addressed by the QMS since it is not mentioned in any procedure. As a consequence of that the process is not well determined
The procedure PR05,04 rev.01 currently in place doesn''t mention to include in the draft version of the IFU the notified body number 0344 near the CE mark.


',N'The IFU of the products Respiratory Bacterial ELITe MGB Panel (RTS553ING) and Respiratory Bacterial-ELITe Positive Control (CTR553ING) will be updated to include the notified body number 0344 near the CE mark.
The PR05,04 "GESTIONE DEI MANUALI DI ISTRUZIONE PER L''USO E DOCUMENTI CORRELATI" will be updated by clearly describing the requirement to accompany the CE mark with the NB Identification number in any stage of the document life when it is related to products belong Annex II.
The same change described above will be implemented into the working instruction IO04,06"ISTRUZIONE OPERATIVA CREAZIONE ETICHETTE PRODOTTI CE-IVD E RUO" for product labeling management. This is a preventive action since NO NC was detected with CE mark on the labels.
  
',NULL,N'Specifying better the process by the update of QMS documents PR05,04 and IO04,06  it will be possible to reduce the possibility of error about this stage
Specifying better the process by the update of QMS documents PR05,04 and IO04,06  it will be possible to reduce the possibility of error about this stage
Specifying better the process by the update of QMS documents PR05,04 and IO04,06  it will be possible to reduce the possibility of error about this stage
Specifying better the process by the update of QMS documents PR05,04 and IO04,06  it will be possible to reduce the possibility of error about this stage
Specifying better the process by the update of QMS documents PR05,04 and IO04,06  it will be possible to comply with prescriptions of 98/79/EC Directive for products belong Annex II',N'21/12/2017
21/12/2017
21/12/2017
21/12/2017
21/12/2017',N'Update the IFU of the products Respiratory Bacterial ELITe MGB Panel (RTS553ING) and Respiratory Bacterial-ELITe Positive Control (CTR553ING) to include the notified body number 0344 near the CE mark
Update the PR05,04 "GESTIONE DEI MANUALI DI ISTRUZIONE PER L''USO E DOCUMENTI CORRELATI" by clearly describing the requirement to accompany the CE mark with the NB Identification number in any stage of the document life when it is related to an Annex IV conformity procedure.
Update the working instruction IO04,06"ISTRUZIONE OPERATIVA CREAZIONE ETICHETTE PRODOTTI CE-IVD E RUO" by clearly describing the requirement to accompany on the label the CE mark with the NB Identification number for products belong Annex II also during the submission of product to the Notified Body 
Put in place and training about new revision of the procedure PR05,04 and of the working instruction IO04,06
Approuve the new revision of the procedure PR05,04 and of the working instruction IO04,06',N'ifu_553ing - rac17-31_messa_in_uso_ifu_553ing.pdf
PR05,04 rev02 - rac17-31_messa_in_uso_pr05,04_rev02.pdf
MOD18,02 PR05,04 rev02 del 7/12/2017 - rac17-31_messa_in_uso_pr05,04_rev02_1.pdf
IO04,06 rev02, training MOD18,02 11/10/18 - rac18-23_messa_in_uso_io0406_rac17-31_rac18-23.msg',NULL,N'21/12/2017
21/12/2017
21/12/2017
21/12/2017
21/12/2017',N'22/12/2017
22/12/2017
31/01/2018
31/01/2018',N'09/02/2018
07/12/2017
11/10/2018
11/10/2018
11/10/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'IFU - Gestione Manuali',NULL,N'IFU1 Stesura manuale istruzioni per l''uso',N'No',NULL,N'No',NULL,N'Stefania Brun',N'No',NULL,'2019-01-31 00:00:00',N'Verification of IFU and labels sent for a new product submission to Dekra (STI PLUS): IFU and Labels RTS400ING are print with correctly simbols; the declaration of conformity is release 20/12/2018 (MOD05,05-RTS400ING).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2018-10-11 00:00:00','2019-01-31 00:00:00'),
    (N'17-30',N'Preventiva','2017-12-18 00:00:00',N'Renata  Catalano',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'According to procedure PR05,02 "Gestione della documentazione dei prodotti IVD" Technical File (TF) of the products commercialized after 10/2013 are organized into stages as the Design History File (DHF). This simplifies the TF creation starting from the DHF but documents are not presented in a readily searchable manner.',N'The structure of the TF of the products commercialized after 10/2013 needs to be redefined in such a way to present documentation in a clear, organized, reacily searchable and unambiguous manner. EG SpA procedure PR05,02 needs to be updated to include all changes introduced in the TF structure.',NULL,N'With the new structure of the TF it would be easier to search documents and to update the TF',N'21/12/2017',N'Update the procedure PR05,02 put in place and perform a training on it. per motivi organizzativi l''azione preventiva è stata posticipata  a ottobre/2018',N'PR05,02 rev03 MOD18,02 del 29/07/2019 - rap17-30_messa_in_uso_pr0502_03_rap18-21_rac17-30_mod1802_del_29072019.msg',N'QARA
QMA',N'18/12/2017',N'31/01/2018',N'31/07/2019',N'No',N'nessun impatto sui DMRI',N'No',N'non necessaria alcuna comunicazione ',N'No',N'nessun impatto sul prodotto immesso in commercio',N'Procedura ggiornata anche secondo i regolamenti MDSAP (vedere RAP18-21)',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC13 - Fascicolo tecnico di Prodotto',N'no impact in DC13',N'No',N'La modifica non è significativa in quanto migliorativa del processo interno di gestione degli FTP ed in linea con i regolamenti dei paesi MDSAP. Non necessarria alcune notifica',N'No',N'Non necessarria alcune notifica',N'Renata  Catalano',N'No',NULL,'2019-09-30 00:00:00',N'Come da Report DEKRA del 19/09/2019, numero Report identification: 2235223-AR14-R0 DRAFT, non ci sono state NC imputabili al fascicolo tecnico.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2019-07-31 00:00:00','2020-01-03 00:00:00'),
    (N'17-29',N'Correttiva','2017-11-22 00:00:00',N'Stefania Brun',N'Validazioni',N'Reclamo',N'17-2',N'RTS020PLD fattore di conversione Qiasymphony',N'Manuale di istruzioni per l''uso',N'Verifica dei lotti utilizzati dal cliente. Verifica dei report di VEQ di altri laboratori e comparazione con i dati ottenuti da EGSpA con il sistema InGenius.
Dalle analisi dei dati disponibili si evidenzia che il problema è imputabile alla conversione del risultato del Qiasymphony da copie/ml a UI probabilmente dovuto ad un non corretto fattore di conversione per la matrice sangue intero, riportato sull''IFU del prodotto RTS020PLD.',N'Esecuzione di test  sulla piattaforma Qiasymphony con campioni di sangue intero.',NULL,N'Esecuzione di test  sulla piattaforma Qiasymphony con campioni di sangue intero.
Esecuzione di test  sulla piattaforma Qiasymphony con campioni di sangue intero.
Modifica IFU prodotto RTS020PLD',N'06/12/2017
06/12/2017
06/12/2017',N'Identificare centro che abbia disponibilità dello strumento Qiasymphony:"Laboratorio Micorbiologia", ospedale Molinette Torino
Esecuzione di test  sulla piattaforma Qiasymphony con campioni di sangue intero. REP2018-001, FIRMATO IL 12/01/2018
Modifica IFU prodotto RTS020PLD ed aggiornamento del FTP',N'report firmato - rac17-29_rep2018-001.pdf
IFU REV14 - rac17-29_messa_in_uso_ifu_rts020pld_ebv.pdf
FTP_020PLD (RTS020PLD, CTR020PLD, STD020PLD) aggiornato - rac17-29_sezione_1_product_technical_file_index_rts020pld.docx',NULL,N'06/12/2017
06/12/2017
06/12/2017',N'31/01/2018
31/01/2018
31/01/2018',N'30/01/2018
30/01/2018
30/01/2018',N'No',N'nessun impatto',N'Si',N'Comunicazione al cliente che ha esposto il reclamo relativamente alla modifica del fattore di conversione',N'No',N'nessun impatto',N'-',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL1 - Fase di pianificazione: pianificazione',N'Nessun impatto',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'Marco Enrietto',N'No',NULL,'2019-01-30 00:00:00',N'Da 01/2018 a 01/2019 non ci sono stati reclami relativi all''uso del prodotto EBV ELITe MGB kit in associazione a sistema di estrazione Qiasymphony.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2018-01-30 00:00:00','2019-01-30 00:00:00'),
    (N'17-28',N'Correttiva','2017-11-22 00:00:00',N'Stefania Brun',N'Sistema qualità',N'Reclamo',N'17-26',N'Assay protocol "Assay_Rubella_TM__OPEN_200_100.asy"',NULL,N'L''assay "Assay_Rubella_TM__OPEN_200_100.asy" installato sullo strumento in uso presso il cliente è risultato errato in quanto i fluorofori sono invertiti. La causa di tale non conformità è da attribuire alla mancata verifica funzionale da parte del personale EGSpA prima dell''installazione sullo strumento ',N'-Notifica al Ministero della Salute del Rapporto iniziale di incidente e di quello finale contenente la presente azione correttiva. Verifica della necessità o meno di notificare l''evento anche presso Autorità Competenti di paesi extra EEA in base ai regolamenti nazionali (es. Canada)
- Revisione delle procedure collegate al ciclo di vita degli assay protocol al fine di rafforzarne le modalità di controllo prima della loro installazione.
- Formazione a tutto il personale "di campo" dedicato all''assistenza ai clienti',NULL,N'revisione di procedure esistenti ed eventuale creazione di nuove
revisione di procedure esistenti ed eventuale creazione di nuove
revisione di procedure esistenti ed eventuale creazione di nuove
adozione di modalità più accurate e onerose nella gestione degli assay protocol utilizzati dai clienti
Notifica al Ministero della Salute del Rapporto iniziale di incidente e di quello finale contenente la presente azione correttiva. Verifica della necessità o meno di notificare l''evento anche presso Autorità Competenti di paesi extra EEA in base ai regolamenti nazionali (es. Canada)
adozione di modalità più accurate e onerose nella verifica degli assay protocol prima della loro messa in uso
adozione di modalità più accurate e onerose nella verifica degli assay protocol prima della loro messa in uso',N'22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017',N'creazione Procedura specifica di gestione degli assay protocol. Oggi tale processo è descritto all''interno di una documento condiviso con la gestione delle IFU. In tale procedura dovranno essere descritti chiaramente: le responsabilità di creazione e di verifica degli assay protocol, le loro modalità d''installazione presso i clienti, le eventuali responsabilità dei clienti al momento del loro utilizzo (specie per gli assay protocol dedicati a prodotti di terze parti) e le modalità di gestione degli aggiornamenti. FORMAZIONE DEL 29/03/2018 (MOD18,02).

Realizzazione di un''istruzione operativa che dettagli i passaggi tecnici di creazione degli assay protocol e la loro verifica

Formazione del personale "di campo" (CSC, FPS, GATL, FAS) sulla nuova procedura e istruzione relativa al ciclo di vita degli assay protocol. MOD18,02 del 17/04/2018.
Aggiornare PR19,02 "Assistenza tecnica strumenti" per indentificare modalità di monitoraggio degli assay protocol installati presso strumenti in uso dai client',N'PR05,04rev02 - rap17-28_messa_in_uso_pr05,04_rev02.pdf
PR05,08rev00,MOD05,07Arev01, MOD05,34rev00, MOD04,30rev00 - rac17-28_messa_in_uso_pr05,08_rev00emodulicollegati.pdf
IO04,08 e nuovi moduli - rap17-28_messa_in_uso_io04,08-mod04,28-mod04,29.pdf
30/11/17eseguitaFormazione - rac_17-28_mod18,02_formazioneio04,08_mod04,28_mod04,29.pdf
PARAGRAFO 3.7 MONITORAGGIO ASSAY PRESSO CLIENTE - rac17-28_messa_in_uso_pr19,02_rev00.pdf
all4 - rac_17-28_allegato_4_rapportoincidentedapartedelscamillo.pdf
all5 - rac_17-28_allegato_5_rapporto_iniziale.pdf
notificaCanada - rac_17-28_notificacanada_4-12-17.pdf
all6 - rac17-28_rapporto_finale_all.6_elitechgroup_11.07.2018_1.pdf
IO04,08 e moduli nuovi - rap17-28_messa_in_uso_io04,08-mod04,28-mod04,29_1.pdf
30/11/17eseguitaFormazione - rac_17-28_mod18,02_formazioneio04,08_mod04,28_mod04,29_1.pdf
messa in uso IO04,08 e mouduli collegati - rap17-28_messa_in_uso_io04,08-mod04,28-mod04,29_2.pdf
30/11/17eseguitaFormazione - rac_17-28_mod18,02_formazioneio04,08_mod04,28_mod04,29_2.pdf',NULL,N'22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017
22/11/2017',N'31/01/2018
02/12/2017
31/01/2018
07/11/2017
05/12/2017
01/12/2017
01/12/2017',N'06/04/2018
01/12/2017
17/04/2018
11/12/2017
11/07/2018
01/12/2017
01/12/2017',N'No',NULL,N'Si',N'Non sono necessarie altre azioni sul campo oltre a quelle riportate sul reclamo N°17-26 in quanto l''assay protocol "Assay_Rubella_TM__OPEN_200_100.asy" era installato unicamente presso lo strumento ELITe InGenius presso laboratorio virologia dell''Osp.San Camillo Forlanini di Roma',N'No',NULL,NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,N'R&D 1.8 Al momento dell''apertura della presente azione correttiva questo risck assessment era in corso di revisione, le misure previste da questa AC sono state incluse ',N'No',NULL,N'No',NULL,N'Sergio  Fazari',N'No',NULL,'2018-06-29 00:00:00',N'Non sono stati registrati reclami o altre segnalazioni riferibili ad errori di creazione degli Assay Protocol successivi alla messa in atto delle presenti misure correttive (nuova IO04,08_00 "Istruzione operativa per la creazione di Assay Protocol InGenius", e procedura PR05,08 "Gestione Assay Protocol"). Questa evidenza fa ritenere efficaci le misure adottate. Nonostante ciò è raccomandabile mettere in atto un monitoraggio continuo e strutturato (per es. attraverso un''azione preventiva) dei reclami riferibili agli Assay Protocol vista l''alta frequenza di questo tipo di reclami evidenziata nell''ultimo anno se pur originati prima della messa in uso della istruzione e procedura sopra citate.',N'Sergio  Fazari',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2018-07-11 00:00:00','2018-07-11 00:00:00'),
    (N'17-26',N'Preventiva','2017-11-09 00:00:00',N'Stefania Brun',N'Produzione',NULL,NULL,NULL,NULL,N'Tale mancanza potrebbe indurre la produzione ad attribuire una scadenza non supportata dalle stabilità reali sui prodotti/semilavorati',N'Introdurre l''indicazione della scadenza massima da attribuire al prodotto finito',NULL,N'Modificare i moduli MOD955-pXXX_ESBL, CRE-Diluizione Plasmide, 962-CTR200ING, 962-CTR201ING, 962-CTRD00ING
Modificare i moduli MOD955-pXXX_ESBL, CRE-Diluizione Plasmide, 962-CTR200ING, 962-CTR201ING, 962-CTRD00ING
Messa in uso nuove revisioni moduli
Messa in uso nuove revisioni moduli
Indicare sulla base delle stabilità reali la scadenza massima da attribuire al prodotto finito/semilavorato
Indicare sulla base delle stabilità reali la scadenza massima da attribuire al prodotto finito/semilavorato',N'01/12/2017
01/12/2017
31/05/2018
31/05/2018
01/12/2017
01/12/2017',N'Modificare i moduli MOD955-pXXX_ESBL, CRE-Diluizione Plasmide, 962-CTR200ING, 962-CTR201ING, 962-CTRD00ING
Formazione del personale di produzione sulla nuova revisione dei moduli.
MOD18,02 del 21-22/05/2018
Messa in uso nuove revisioni moduli
Indicare sulla base delle stabilità accelerate la scadenza massima da attribuire al prodotto finito/semilavorato. ',N'MOD962-CTR200INGrev01, MOD962-CTR201INGrev01, MOD955-pxxx-esbl-cre-dilplasREV02 - rap17-26_messa_in_uso_moduli_.pdf
Stabilità accelerate report - rap17-26_allegato_1_elencostabilita.docx
mail messa in uso - rap17-26_messa_in_uso_moduli__1.pdf',NULL,N'01/12/2017
01/12/2017
01/12/2017
01/12/2017',N'31/01/2018
31/12/2017
31/01/2018
31/01/2018',N'31/05/2018
31/05/2018
31/05/2018
31/05/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'Si',NULL,'2018-11-30 00:00:00',N'A distanza di 6 mesi dalla chiusura delle azioni non sono state riscontrate NC per assegnazione errata della scadenza.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-01-31 00:00:00','2018-05-31 00:00:00','2018-11-30 00:00:00'),
    (N'18-29',N'Correttiva','2018-09-28 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',NULL,N'Reclamo n°18-58 RTSD00ING',NULL,N'The root cause investigation has put in evidence the following items: i) the production lot of the kit was fully in compliance with the expected specifications ii) the equipment ELITe InGenius installed into the AZ Sint-Lucas laboratory was fully in compliance with the functional specifications iii) NO complaints were received from other customers reporting similar "malfunction" observed in the Reclamo #18-58  iv) stress tests did not succeed to reproduce the "malfunction" observed  v) low concentration of the target DNA did negatively affect the melting curve accuracy. 
Even if the root cause was not clearly identified, the following conclusions allows to implement some corrective actions: i) the event described in the complaint was mainly due to local, and not determined, technical issues  ii) low DNA target concentration in the amplification reaction is a condition which may negatively affect final results accuracy iii) verification by the end-user of the melting curve profile reduce the risk of misdiagnosis.',N'The corrective action objective is to increase the assay reactions yield in order to get a more detectable melting curve profile even with low DNA concentration or inhibited samples. This action will be implemented by increasing the number of reaction cycles from 35 to 40."Feasibility and impacts studies related to that change was described in the "Report of Feasibility Study".
In addition, a further remark about the verification by the end-user of the melting curve profile before finale results acceptance will be introduced in the product IFU.',NULL,N'Possible impacts on product performances (false positive reults) due to a longer amplification reaction.
No relavant impacts. No registrations update required after correttive action implementation.
No relavant impacts. No registrations update required after correttive action implementation.
The change will be reduce the probability of discrimination errors. 
No impacat, only action.
No impacat, only action.
No impact, only action.
No impact, only action.
(Costumer and Service Support) The change will be reduce the probability of discrimination errors. 
(Costumer and Service Support) The change will be reduce the probability of discrimination errors. 
No direct impacts. See R&D impact.',N'28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018',N'Feasibility Study; 
Evaluation Impact of Risk analisys;  
"Report of Feasibility Study"; 
Release of new assay protocol for users and the QC.
Report Form Field Safety Corrective Action (update)
Final Report FSCA form.
Plan and upgrade the assay protocol version  in the customer ELITe InGenius
New assay protocol, assay protocol CQ and IFU release.
Techinical file update.
New assay protocol and TAB verification.
Update assay protocol.
Plan and upgrade the assay protocol version  in the customer ELITe InGenius
Send the TAB to Distributors.
Update of new QC Assay protocol.',N'MOD04,30_int11 rev00 - rac18-29_messa_in_uso_mod0430_int11_rac18-29.msg
investigation on coagulation ELITe MGB kit thermal profile - rac18-29_report_firmato29-09-2018.pdf
ASSAY PROTOCOL RTSD00ING CQ rev02 - rac18-29_messa_in_uso_ap_coagulazione_per_cq_1.msg
Initial Report - 20180726_report_form_fsca_elitech_signed.pdf
Update - 20180808_report_form_fsca__update_elitech_signed.pdf
update 2 - 20181010_report_form_fsca_udate_elitech_signed.pdf
assay protocol in use - rac18-29__assay_protocol_coagulation_rev.03_rac18-29.msg
assay protocol CQ in use - rac18-29_messa_in_uso_ap_coagulazione_per_cq_2.msg
TAB COAGULATION TAB P12 revAC - rac18-29_tab_p12-_rev_ac_elite_ingenius_-tab_p12-_rev_ac_-_product_coagulation_elite_mgb_notice_00.pdf
CUSTOMER LIST - rac18-29_rtsd00ing_elenco_clienti.xlsx
Mail CSC di termine aggiornamento AP presso clienti  - rac18-29_terminatoaggiornamentoappressoclienti.pdf
SEND THE TAB TO DISTRIBUTORS - rac18-29_couminciazione_ai_distributori_elite_ingenius_tab_ac.msg
ASSAY PROTOCOL CQ RTSD00ING rev02 - rac18-29_messa_in_uso_ap_coagulazione_per_cq.msg
MOD10,12-RTSD00ING-CTRD00ING_04_MOD18,02 del 4/12/18 - rac18-29_messa_in_uso_mod1012-rtsd00ing-ctrd00ing_04_rac18-29.msg
UPDATE ASSAY PROTOCOL - rac18-29__messa_in_uso_assay_protocol_coagulation_rev.03_rac18-29.msg
final report - 2019_05_07_fsca_final_report_elitech_signed.pdf',NULL,N'28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018',N'31/10/2018
16/10/2018
31/01/2019
30/11/2018
31/01/2019
16/11/2018
16/11/2018
31/01/2019
31/01/2019
30/11/2018',N'14/11/2018
08/05/2019
08/05/2019
20/11/2018
08/05/2019
20/11/2018
05/11/2018
21/02/2019
11/12/2018
05/12/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,N' R&D1.3',N'No',NULL,N'No',NULL,NULL,N'No',NULL,'2019-05-08 00:00:00',N'After 4 products lots distributions: U0618AQ, U0818AG, U1218AP, U0319AM are conforms. No complaint from 06/2018 to 05/2019. Veq: report number 30686MolGen61 (10/2018) and 30686MolGen62 (03/2019) are conforms.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-01-31 00:00:00','2019-05-08 00:00:00','2019-05-09 00:00:00'),
    (N'19-37',N'Preventiva','2019-11-19 00:00:00',N'Federica Farinazzo',N'Produzione',NULL,NULL,NULL,N'Documentazione SGQ',N'L''azione preventiva mira a prevenire errori di dispensazione durante la preparazione degli oligo ed evitare tempi eccessivamente lunghi di lo scongelamento.',N'Si chiede la modifica dei moduli di preparazione degli oligo per quanto riguarda:
- le indicazioni dei tempi di scongelamento (e manipolazione al buio per le sonde)
- indicazioni più specifiche sui tempi di permanenza a Temperatura ambiente
- la pianificazione dei volumi da dispensare e successiva registrazione degli stessi',NULL,N'Impatto positivo: la stima dei tempi di scongelamento e le indicazioni sui tempi di permanenza a temperatura ambiente permettono di avere dei documenti più specifici e adeguati alla realtà operativa. La tracciabilità dei volumi da dispensare previene errori e, in caso di non conformità, permette di verificare l''effettiva dispensazione dei reagenti.
Impatto positivo: la stima dei tempi di scongelamento e le indicazioni sui tempi di permanenza a temperatura ambiente permettono di avere dei documenti più specifici e adeguati alla realtà operativa. La tracciabilità dei volumi da dispensare previene errori e, in caso di non conformità, permette di verificare l''effettiva dispensazione dei reagenti.
Impatto positivo: la stima dei tempi di scongelamento e le indicazioni sui tempi di permanenza a temperatura ambiente permettono di avere dei documenti più specifici e adeguati alla realtà operativa. La tracciabilità dei volumi da dispensare previene errori e, in caso di non conformità, permette di verifi',N'19/11/2019
19/11/2019
03/12/2019
03/12/2019
03/12/2019
02/12/2019',N'- Definizione dei tempi indicativi per lo scongelamento degli oligo e del diluente
- Modifica dei moduli di preparazione degli oligo
- Formazione
- Approvazione e simulazione dei documenti
Non c''è nessun impatto sul sotto-processo P1.8 Scongelamento e mescolamento materiali e semilavorati
Presa visione delle modifiche
- messa in uso dei moduli
Non è necessario aggiornare il VMP, in quanto il sotto-processo P1.8 non è impatto.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'Messa in uso moduli oligo, MOD18,02 del 25/11/2019 - rap19-37__messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119.msg
Messa in uso moduli oligo, MOD18,02 del 25/11/2019 - rap19-37__messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119_1.msg
messa in uso moduli oligo - rap19-37__messa_in_uso_moduli_oligo_rac19-26_rap19-37_mod1802_del_251119_2.msg
La modifica non è significativa, pertanto non è necessario effettuare alcuna modifica. - rap19-37_mod05,36_non_significativa.pdf',NULL,N'19/11/2019
19/11/2019
03/12/2019
04/12/2019
02/12/2019
02/12/2019
03/12/2019',N'06/12/2019
22/11/2019
31/01/2020
20/12/2019
31/01/2020',N'09/12/2019
09/12/2019
03/12/2019
04/12/2019
09/12/2019
04/12/2019
02/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'L''aggiornamento della documentazione alle attivià svolte e la loro registrazione mira all''adeguamento ai requisiti GMP.',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.8 - Scongelamento e mescolamento materiali e semilavorati',N'le modifiche non hanno impatto sul RA P1.8 in qunato riguardano la verifica delle tempistiche di scongelamento per tenere sotto controllo i tempi ed evitare che diventino inutilmente eccessivi. Non riguardano, invece la miscelazione, dopo lo scongelamento che resta invariata.',N'No',N'vedere MOD05,36',N'No',N'verificare',NULL,N'No',NULL,'2020-08-27 00:00:00',N'Assenza NC imputabili ad errori di dispensazione o scongelamento oligo.
Ad 08/2020 non si sono verificati errori di dispensazione o scongelamento oligo.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-01-31 00:00:00','2019-12-09 00:00:00','2020-08-27 00:00:00'),
    (N'20-49',N'Correttiva','2020-12-23 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'20-124',N'RTS015PLD',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N' La causa della NC è collegata alla definizione del valore di riferimento per la PQ che non tiene conto della variabilità dei dati ottenibili in CQ. Infatti il test di CQ ha riportato un Ct precoce per le 6000copie ma analizzandoi i dati delle ripetizioni la deviazione ottenuta di 0,5 Ct è accettabile considerando che all''interno della stessa sessione abbiamo una deviazione media di 0,3Ct.',N'In seguito all''analisi della problematica e dei dati di PQ ottenuti sul campo si è deciso di produrre un prodotto dedicato alla PQ per lo strumento che possa essere controllato dal CQ in modo esteso e come da PQ al fine di definire con maggiore accuratezza il limite da utilizzare presso i clienti',NULL,N'Maggior sicurezza del dato fornito a service per la PQ. 

Prodotto RUO dedicato : viene incontro alle richieste di alcuni paesi come il canada .
Avere un prodotto dedicato e con un criterio definito con maggiore accuratezza viene incontro alle esugenze di installazione
Avere un prodotto dedicato e con un criterio definito con maggiore accuratezza viene incontro alle esugenze di installazione
Pianificazione della produzione di  un prodotto dedicato in base ai quantitativi stimati da service e assitenza struemnti
Maggior sicurezza del dato fornito a service per la PQ. 

Maggior sicurezza del dato fornito a service per la PQ. 
',NULL,N'tutte le attività sarano pianificate nell''ambito del DT (vedi apertura SCP)
Tutte le attività saranno gestite nell''ambito del progetto
apertura progetto 
Tutte le altre attività saranno pianificate da PM nell''ambito del progetto',NULL,NULL,N'06/04/2021',N'31/01/2021',NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-01-31 00:00:00',NULL,NULL),
    (N'18-5',N'Correttiva','2018-01-31 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-145',N'STD015 pCMV-MIEA-L-TRI
RTS015M',N'Manuale di istruzioni per l''uso',N'Dall''analisi delle carte di controllo relative ai CQ eseguiti sul prodotto (2008-2018) si è evidenziata una variabilità legata al  requisito di Ct per gli strumenti Alla luce di tutte le analisi sulle carte di controllo dei prodotti analizzati si evince che sia la mix che lo STD015 risultano estremamente  stabili nel tempo e non si assiste ad alcuna deriva del prodotto (si veda allegato delle CdC).
Dato quindi che il prodotto, nel tempo, non ha subito una deriva, l'' unico elemento che sembra coinvolto nel non superamento dei criteri di accettazione sembrerebbe essere lo strumento impiegato per l'' amplificazione (RT3, di cui, tra l'' altro, non si ha a disposizione uno storico, essendo uno strumento da poco acquisito al CQ).
Per questo motivo è stata effettuata un''analisi approfondita sulla variabilità degli strumenti in QC (test n°1) che ha confermato i Ct più tardivi con l''RT3 rispetto agli altri due strumenti in uso. Questo ci permette di valutare la modifica dei criteri di CQ, basati sugli strumenti in uso e sulle prestazioni richieste per il prodotto. Per questo sarà aperta una RAC per la richiesta di cambiamento dei criteri da 22,10 < Ct FAM < 23,10 a Ct FAM < 24, ossia la media +3dev.st dei dati ottenuti con i 3 strumenti.
Alla luce di tutte le analisi sulle carte di controllo dei prodotti analizzati si evince che sia la mix che lo STD015 risultano estremamente  stabili nel tempo e non si assiste ad alcuna deriva del prodotto (si veda allegato delle CdC).
Dato quindi che il prodotto, nel tempo, non ha subito una deriva, l'' unico elemento che sembra coinvolto nel non superamento dei criteri di accettazione sembrerebbe essere lo strumento impiegato per l'' amplificazione (RT3, di cui, tra l'' altro, non si ha a disposizione uno storico, essendo uno strumento da poco acquisito al CQ).
Per questo motivo è stata effettuata un''analisi approfondita sulla variabilità degli strumenti in QC (test n°1) che ha confermato i Ct più tardivi con l''RT3 rispetto agli altri due strumenti in uso. Questo ci permette di valutare la modifica dei criteri di CQ, basati sugli strumenti in uso e sulle prestazioni richieste per il prodotto. 
',N'Cambiamento dei criteri di CQ per il test di VALIDAZIONE della SESSIONE, attualmente compreso tra 22,10  e 23,10, a Ct minore di 24. I nuovi criteri proposti derivano dall''analisi dei dati ottenuti con i 3 strumenti in uso in CQ. In particolare il dato è stato ottenuto dalla somma della media  e 3 deviazioni standard dei dati.
cambiamento dei criteri da 22,10 < Ct FAM < 23,10 a Ct FAM < 24, ossia la media +3dev.st dei dati ottenuti con i 3 strumenti.
La modifica del valore di Ct non ha impatto sull''uso previsto in quanto è al di sotto del Ct 25 indicato sull''IFU',NULL,N'Modifica dei documenti di CQ per CMV Alert ed eventali altri prodotti correlati (EXTG01, RTS015, 955-pCMV-MIEA-L).
Valutazione degli impatti sul prodotto: la modifica del valore di Ct non ha impatto sull''uso previsto in quanto è al di sotto del Ct 25 indicato sull''IFU.
Approvazione dei nuovi criteri.
Comunicazione a Dekra, messa in uso dei documenti. ',N'31/01/2018
31/01/2018
31/01/2018',N'Modifica dei documenti di CQ per CMV Alert ed eventali altri prodotti correlati (EXTG01, RTS015, 955-pCMV-MIEA-L).
Approvazione dei nuovi criteri.
Comunicazione a Dekra, messa in uso dei documenti. la comunicazione a DEKRA non risulta essere necessaria. Moduli in uso al 15/2/2018, formazione (MOD18,02) del 8/2/2018.',NULL,NULL,N'31/01/2018
31/01/2018
31/01/2018',N'28/02/2018
15/02/2018
31/03/2018',N'15/02/2018
08/02/2018
15/02/2018',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'-',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto',N'No',N'non necessaira',N'No',N'non necessaira',N'Federica Farinazzo',N'No',NULL,'2020-04-30 00:00:00',N'Assenza NC CQ.
Sono stati prodotti i seguenti lotti di vendita di RTK015 U0218AN e U0918AY U0619AW e STD015 U0618AJ e U0819AL tutti conformi. A 01/2020 Si attende un terzo lotto di STD015. Ad 04/2020 si valuta che l''azione correttiva può essere chiusa con solo 2 lotti, invece di 3, in quanto il prodotto verrà dismesso (non più prodotto, non più venduto) al 1/07/2020, come da CC20-23.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-03-31 00:00:00','2018-02-15 00:00:00','2020-04-24 00:00:00'),
    (N'19-6',N'Correttiva','2019-03-06 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-26',N'RTS120ING',N'Documentazione SGQ',N'Criterio di CQ non è in linea con la capability di processo: la media dei dati di monitoraggio più 3 Dev Std è 33,5, il criterio del CQ è 33, pertanto la NC potrebbe essere causata da criteri troppo stringenti. 
Analisi eseguite:
Test di riproducibilità del campione 6000 copie IC per reazione su due diversi strumenti (STR8 e STR9)
Analisi dei dati di CQ.',N'Si chiede la modifica dei criteri di CQ relativi al controllo interno, al fine di considerare la distribuzione normale dei dati verificata dal monitoraggio dei lotti in CQ. ',NULL,N'La modifica permette di garantire la corretta valutazione della conformità dei lotti, considerando la distribuzione normale dei dati verificata nel monitoraggio dei lotti.
La modifica permette di garantire la corretta valutazione della conformità dei lotti, considerando la distribuzione normale dei dati verificata nel monitoraggio dei lotti.
La modifica del Criterio di Accettazione del test di CQ con 6000 copie di target IC2 (sensibilità per il controllo interno) portandolo da Ct = 33 a Ct = 34 ha un impatto sull''AP per l''analisi dei campioni clinici a livello del cut-off del controllo interno per la validazione dei campioni negativi. Un lotto che passa il CQ con un Ct IC2 superiore a 33 potrebbe generare più risultati "Non Validi" di quanto riscontrato con i lotti utilizzati in verifica e validazione.
Nessun impatto se no in termini di attività.
Nessun impatto se no nin termini di attivià.
Nessun impatto se no nin termini di attivià.',N'06/03/2019
06/03/2019
06/03/2019
06/03/2019
06/03/2019
06/03/2019',N'Monitoraggio dei dati di CQ
Modifica dei criteri di CQ relativi al CI
Eseguire un''analisi comparativa dei valori di Ct cel controllo interno nei CQ dei lotti usati in validazione e dei risultati ottenuti per i campioni clinici negativi in validazione. Dovranno essere simulati i possibili risultati che si sarebbero ottenuti con un un lotto conforme a Ct 33 e uno conforme a Ct 34 e si dovrà valutare il numero di Non Validi ottenuti nelle due situazioni.
Essendo la causa della NC probabilmente anche collegata ad una sonda IC2 poco performante (Bothell ha segnalato che il fluoroforo AP680 può degradarsi facilmente), si suggerisce la sostituzione della sonda IC2 attuale con una marcata con AP690 (colorante più stabile sviluppato da Bothell per sostituire l''AP680).
controllo AP ed aggiornamento TAB
Messa in uso nuovi documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'Monitoraggio CI - rts120ing_monitoraggio-ci.pdf
MESSA IN USO MOD10,12-RTS120ING-CTR120ING_01. MOD18,02 DEL 08/03/2019 - rac19-6_messa_in_uso_mod1012-rts120ing-ctr120ing_rac19-1_rac19-6.msg
Messa in uso MOD10-+12-RTS120ING-CTR120ING_01 - rac19-6_messa_in_uso_mod1012-rts120ing-ctr120ing_rac19-1_rac19-6_1.msg
La modifica è significativa, la notifica deve essere effettuata esclusivamente alle autorità competenti che lo richiedono, laddove il prodotto è registrato. - rac19-6_mod05,36_significativa.pdf
Nei paesi in cui RTS120ING è registrato non è stato fornito alcun documento CQ. La notifica è a inviare. - rac19-6_estratto_mod05,29_paesi_in_cui_il_prodotto_e_registrato.pdf',NULL,N'06/03/2019
06/03/2019
06/03/2019
06/03/2019
06/03/2019',N'31/03/2019',N'11/03/2019
11/03/2019
05/07/2019',N'No',N'nessun impatto',N'No',N'-',N'No',N'-',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'La modifica è significativa, vedere MOD05,36',N'No',N'La modifica è significativa, vedere MOD05,36',NULL,N'No',NULL,NULL,N'Verifica della normale distribuzione dei dati attraverso il monitoraggio di almeno 3 lotti in CQ. ',NULL,NULL,NULL,NULL,NULL,'2019-03-31 00:00:00',NULL,NULL),
    (N'21-1',N'Preventiva','2021-01-14 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'Il numero elevato di lotti prodotti richiede una nuova analisi dei criteri per il monitoraggio stabiliti in fase iniziale di produzione del prodotto. Tali criteri devono essere rivisti sia in funzione dei differenti lotti di materiali utilizzati nel tempo sia in funzione del nuovo fornitore di enzimi Promega.',N'Rivedere i limiti di monitoraggio definiti per RTS170ING sulla base dei lotti prodotti e considerando anche alcuni lotti di produzione con gli enzimi Promega.',NULL,N'L''analisi dei dati permette di stabilire dei nuovi criteri maggiormente in linea con la variabilità di produzione  e pertanto garantire la valutazione di una eventuale deriva di produzione.
L''analisi dei dati permette di stabilire dei nuovi criteri maggiormente in linea con la variabilità di produzione  e pertanto garantire la valutazione di una eventuale deriva di produzione.
L''analisi dei dati permette di stabilire dei nuovi criteri maggiormente in linea con la variabilità di produzione  e pertanto garantire la valutazione di una eventuale deriva di produzione.',N'14/01/2021',N'analisi dei dati di CQ includendo alcuni lotti prodotti con enzimi Promega in modo da valutare eventuali variazioni legate a questi materiali
Modifica del modulo MO10,12-RTS170ING
Formazione
messa in uso dei documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  ',N'mod1012-rts170ing_05 - rap21-1_messa_in_uso_mod1012-rts170ing.msg
mod1012-rts170ing_05 - rap21-1_messa_in_uso_mod1012-rts170ing_1.msg
la modifica non è significativa, pertanto non è da notificare. - rap21-1_mod05,36_01_non_significativa.xlsx',NULL,N'14/01/2021',N'31/03/2021',N'19/04/2021
19/04/2021
19/04/2021',N'No',N'nessun impatto sul DMRI di prodotto',N'No',N'non necessaria, la rap agisce sui criteri di monitoraggio',N'No',N'nessun impatto ',N'la rap agisce sui criteri di monitoraggio, non di cq',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-10-31 00:00:00',N'Valutazione efficacia: verificare che i limiti di monitoraggio impostati siano congrui',NULL,NULL,NULL,NULL,NULL,'2021-03-31 00:00:00','2021-04-19 00:00:00',NULL),
    (N'21-6',N'Correttiva','2021-02-03 00:00:00',N'Maria Galluzzo',N'Assistenza applicativa',N'Reclamo',N'20-72',N'AP MDR_MTB',N'Assay Protocol',N'E'' stato confermato dalle software house Dedalus, Lutech e quella francese che la difficoltà di interfacciamento dei risultati del prodotto RTS120ING (MDR-MTB) da InGenius al LIS è dovuta alla sintassi della stringa di output prodotta da InGenius che non è interfacciabile per via del simbolo ":"  che risulta non eliminabile dopo la dicitura RESISTANCE POSITIVE (per eliminarla è necessario richiedere a PSS di modificare il modello 12). Quindi anche togliendo il simbolo  ":", inserito erroneamente da EGSpA dopo la dicitura "Typing as follows", la stringa risulterebbe comunque  non interfacciabile. In allegato al reclamo 20-72 le risposte specifiche di ciascuna software house con le spiegazione tecniche fornite.

Riassumendo:
 
- STRINGA RICHIESTA dalle software house:
MTB: DNA detected Typing as follows RIF: RESISTANCE POSITIVE rpoB2, rpoB3, rpoB4, rpoB1INH: RESISTANCE POSITIVE inhA
 
- STRINGA PRODUCIBILE come output di InGenius:
MTB: DNA detected Typing as follows RIF: RESISTANCE POSITIVE: rpoB2, rpoB3, rpoB4, rpoB1INH: RESISTANCE POSITIVE: inhA
 
- STRINGA PRODOTTA con AP modificato Rev.03 :
MTB: DNA detected Typing as follows: RIF: RESISTANCE POSITIVE: rpoB2, rpoB3, rpoB4, rpoB1INH: RESISTANCE POSITIVE: inhA',N'Correzione PROVVISORIA:
1)  Per i clienti che hanno InGenius interfacciato al LIS tramite Eliteware e grazie alla nuova versione validata di Eliteware (software house Lutech), esiste la possibilità di acquisire i risultati dei Ct e delle Tm del test MDR-MTB ottenuti su InGenius, con un file csv esportabile da dare all''Eliteware che li interpreta con le regole del modello interpretativo 12 e quindi li può trasmettere al LIS ospedaliero (l''Eliteware in questo caso farebbe l''interpretazione del risultato al posto di InGenius); questa soluzione necessita di una validazione per il controllo delle regole impostate per non andare incontro a risultati errati. 
2)  Per i clienti che hanno InGenius direttamente interfacciato al LIS è necessaria l''installazione forzata di ELITeWare (per attuare la soluzione descritta al punto precedente) anche laddove il cliente non ne abbia fatto richiesta, soluzione che richiede una discussione commerciale ed una valutazione dei costi/benefici in merito.
Per la correzione provvisoria del reclamo è necessario fare riferimento alla RAC21-6

Correzione DEFINITIVA:
Modifica del modello interpretativo 12 per MDR-MTB eseguibile solo da PSS ; in merito alla richiesta di modifica esiste già il ticket Mantis n° 452 aperto (RAC20-17) che può essere accorpato al Ticket Mantis 438 "Aligment for Interpretation Model sentences" e che confluisce in questa RAC. Attualmente la problematica non è ancora stata discussa con PSS per la definizione delle tempistiche e delle priorità. 
Per la correzione definitiva del reclamo è necessario fare riferimento alla RAC21-5 (che sostituisce la RAC20-4 chiusa per efficacia negativa su cui inizialmente è stata direzionata la correzione del reclamo) poichè la causa primaria è da ricondurre alla mancata definizione e condivisione con PSS delle specifiche da utilizzare per gli output, non solo del modello interpretativo 12, ma di tutti i modelli interpretativi del SW InGenius. L''azione correttiva, che ha lo scopo di DEFINIRE e CONDIVIDERE con PSS le specifiche per TUTTI i MODELLI INTERPRETATIVI in modo che siano applicate da PSS anche negli upgrade dei SW e dei modelli interpretativi, è declinata nelle seguenti attività: 
-	Integrare la "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" con un allegato contenente una parte inerente ai criteri che definiscono la "sintassi" della stringa del risultato (regole della sintassi degli output dei vari modelli) ed una parte in cui sono riportate le frasi definite in base ai criteri e che sono da inserire negli AP.
-	Condividere con PSS un documento con le stringhe definite in modo standardizzato
-	Aggiornare il MOD05,35 con le stringhe definite in base ai criteri sopra citati poichè impattano sull''interfaccia con ELITeWare.
-	Sviluppare e validare un sistema interno di verifica dell''interfacciamento degli assay protocol prima del loro rilascio tramite prova di connessione ad ELITeWare finalizzato al miglioramento della sicurezza e della qualità del processo di interfacciamento.
-             Integrazione all''interno della "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" la parte inerente i criteri che definiscono la "sintassi" della stringa del risultato per ottenere una standardizzazione del processo ed evitare di generare errori nella stringa
-             Progetto di prova di connessione al LIS tramite ELITeWare prima del rilascio degli AP per avere un sistema interno di validazione degli assay protocol.',NULL,N'Impatto positivo per i clienti poichè possono interfacciare il test MDR-MTB tramite l''ELITeWare, il MiddleWare di InGenius validato. 
Impatto positivo per i clienti poichè possono interfacciare il test MDR-MTB tramite l''ELITeWare, il MiddleWare di InGenius validato. 
Impatto positivo per i clienti poichè possono interfacciare il test MDR-MTB tramite l''ELITeWare, il MiddleWare di InGenius validato. 
Positivo in quanto risolve il reclamo del cliente e ripristina la conformità ai requisiti richiesti dai clienti e offerti da EGSpA
La modifica del modello 12 permetterà di risolvere le problematiche per i clienti che hanno lo strumento Ingenius interfacciato al LIS tramite ELITeware, potrebbero permanere errori nei LIS non associati ad ELITeWare.
Positivo in quanto permette di evitare la trasmissione manuale al LIS dei risultati e soddisfa i requisiti richiesti dal cliente 
Impatto in termini di attività
Impatto in termini di attività',N'11/03/2021
11/03/2021
10/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021',N'Verifica della soluzione proposta del file csv e controllo delle regole impostate su ELITeWare in collaborazione con la software house Lutech.
Installazione della soluzione presso i clienti interessati.
Verifica della soluzione proposta del file csv e controllo delle regole impostate su ELITeWare in collaborazione con la software house Lutech.
Valutare i tempi di fattibilità relativi alla messa in pratica della correzione che richiederebbe un''attività di verifica prima del rilascio e la modifica del Modello interpretativo 12 da parte del fornitore PSS
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  
Richiedere al fornitore PSS la modifica del modello interpretativo 12 ( ticket Mantis n° 452 e Ticket Mantis 438 "Aligment for Interpretation Model sentences", e valutazione delle tempistiche)
Aggiornamento del AP a seguito di messa in uso del nuovo modello 12 fornito da PSS
verifica del modello inter',NULL,NULL,N'11/03/2021
11/03/2021
10/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021',N'31/03/2022',NULL,N'No',N'nessun impatto',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'La modifica del modello 12 permetterà di risolvere le problematiche per i clienti che hanno lo strumento Ingenius interfacciato al LIS tramite ELITeware, potrebbero permanere errori nei LIS non associati ad ELITeWare.',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',N'Attualmente non viene effettuato un controllo dei dati trasmessi al lis, è in corso l''introduzione di tale controllo come da RAC21-5',N'No',N'per le modifiche degli ap non è necessario effettuare la notifica alle autorità',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2022-03-31 00:00:00',NULL,NULL),
    (N'18-14',N'Correttiva','2018-05-14 00:00:00',N'Stefania Brun',N'Assistenza applicativa',N'Reclamo',NULL,N'Reclamo n° 128-17',N'Oggetto',N'La causa primaria degli errori è generata dall''attuale impostazione del software dell''ELITe InGenius, il quale necessita di 4 punti del controllo positivo prima di applicare i criteri di accettazione definiti nell''assay protocol (normalmente 3 deviazioni standard). Questa impostazione causa 2 tipi di errori:
1.Una discrepanza dal valore atteso relativo alla concentrazione conosciuta del controllo positivo non monitorata nei primi 4 punti.
2.Il calcolo di una deviazione standard troppo stringente che determina il fallimento delle sedute successive ai primi 4 punti ',N'Studio della variabilità dei valori di concentrazione dei controlli positivi per la determinazione di valori standard da inserire come default nelle carte di controllo dei saggi quantitativi.',NULL,N'Positivo in quanto permetterà di supportare i cliente nell''utilizzo delle carte di controllo e permetterà di risolvere in via definitiva i problemi di sedute fallite causa attuale impostazione del software dell''ELITe InGenius
Positivo in quanto permetterà di supportare i cliente nell''utilizzo delle carte di controllo e permetterà di risolvere in via definitiva i problemi di sedute fallite causa attuale impostazione del software dell''ELITe InGenius
Positivo in quanto permetterà di monitorare le prestazioni di tutti i saggi quantitativi con criteri simili a quelli in uso per il controllo qualità dei prodotti
Da valutare se applicare le carte di controllo nel corso degli studi di validazione
Positivo in quanto l''utilizzo delle carte di controllo permette di rispondere ai requisiti richiesti dal cliente 
Positivo in quanto permette di evitare reclami di performance non fondati dovuti al fallimento delle sedute successive ai 4 punti causa impostazione attuale del database',N'14/05/2018
14/05/2018
14/05/2018
14/05/2018',N'Organizzazione dell''impostazione a posteriori dei punti definiti a seguito della presente azione correttiva sugli strumenti installati
Formazione ai FPS per l''impostazione delle carte di controllo e l''utilizzo del "Installation backup database"
Studio della variabilità dei valori di quantità dei controlli positivi e/o campioni per la determinazione dei valori di deviazione standard da inserire come default iniziale nelle carte di controllo dei saggi quantitativi.
Messa in uso della nuova revisione del software "Installation backup database"',N'mail di formazione agli FPS - rac18-14_messa_in_uso_dell_installation_database_-_nuova_revisione_1.msg
ReportControl Chart Template - rac18-14_controlcharttemplate.pdf
in uso installation database - rac18-14_messa_in_uso_dell_installation_database_-_nuova_revisione.msg',NULL,N'14/05/2018
14/05/2018
15/05/2018
15/05/2018',N'31/05/2018',N'05/02/2018
14/06/2018
05/02/2018',N'No',N'nessun impatto',N'No',N'effettuata al cliente che ha reclamato',N'No',N'nessun impatto',N'monitorare le prestazioni di tutti i saggi quantitativi con criteri simili a quelli in uso per il controllo qualità dei prodotti',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun virtual manufacturer',NULL,N'Si',N'Monitorare l''impostazione  l''installazione delle carte di controllo sugli strumenti installati è difficoltoso (vedere anche RAC20-16). l''assenza di reclami con uguale causa primaria permette la chiusura dell''azione correttiva.','2020-08-06 00:00:00',N'Monitorare l''impostazione  l''installazione delle carte di controllo sugli strumenti installati è difficoltoso (vedere anche RAC20-16). l''assenza di reclami con uguale causa primaria permette la chiusura dell''azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-05-31 00:00:00',NULL,'2020-08-06 00:00:00'),
    (N'18-9',N'Correttiva','2018-03-08 00:00:00',N'Guglielmo Stefanuto',N'Ricerca e sviluppo',N'Non conformità',N'17-32',N'Linearizzazione plasmidi con nuovo enzima XmnI',N'Materia prima',N'La causa di questa NC è imputabile all''enzima ScaI.
Quest''ultimo, ormai, è l''enzima che più di tutti serve a  linearizzare i nostri plasmidi (quasi tutti i plasmidi in pGEM); l''efficacia dello stesso già in passato era risultata inferiore rispetto a quella di altri enzimi come NcoI o BglII, infatti la digestione con Sca I viene eseguita sempre a +37°C ma per due ore anziché una sola. Gli enzimi di restrizione devono essere trattati a freddo (utilizziamo allo scopo un portaprovette refrigerato) in quanto termosensibili.
Eseguiti test: utilizzo punto 10 pEBV EBNA1, linearizzato con lo stesso lotto di enzima ScaI utilizzato in questa NC, con un lotto diverso di ScaI, test eseguiti associando lotto di buffer di ScaI oggetto della NC con il lotto nuovo di enzima ScaI e viceversa.
Eseguita nuova digestione raddoppiando volume dell''enzima ScaI (Lotto in uso) e una nuova linearizzazione utilizzando enzima diverso: XmnI.
22/11/2017 I risultati dei test sono stati passati all''R&D per la valutazione dell''azione correttiva',N'Dall''indagine svolta da Produzione risulta che la digestione del plasmide pEBV EBNA1 con ScaI è meno efficiente di quella eseguita con XmnI. Si propone di utilizzare XmnI per eseguire la linearizzazione di questo plasmide e anche degli altri per cui è possibile (assenza di siti nell''inserto).
Il sito di XmnI dista poche basi da quello di ScaI (circa 120 bp) e non sono prevedibili impatti sulla qualità del semilavorato. Le attuali procedure di CQ sui DNA standard o Positive Control sono sufficienti a attestare la conformità dei prodotti ottenuti con il nuovo enzima XmnI.
La Produzione segnalerà al CQ i primi lotti dei DNA standard o Positive Control preparati con il nuovo enzima XmnI per un monitoraggio del processo di sostituzione. 
Le azioni già intraprese (aliquotamento e uso in ghiaccio) riguardo le manipolazioni di ScaI e del suo buffer sono da applicare anche a XmnI.
La documentazione relativa a XmnI ("Specifica reagenti") e il processo (modulo "Scheda  linearizzazione") è già stata preparata e messa in uso per la produzione del plasmide pASPERGILLUS.
La formazione del personale di Produzione per l''uso della "Scheda  linearizzazione" con XmnI è già stata effettuata per la produzione del plasmide pASPERGILLUS.',NULL,N'Per lo sviluppo dei prodotti non c''è alcun impatto, è presente una nuova scheda linearizzazione per l''enzima di restrizione XmnI utilizzabile con i nuovi plasmidi. 
Aggiornamento modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23, con l''indicazione del nuovo enzima XmnI dove previsto.
Formazione del personale di Produzione.
Monitoraggio del processo di sostituzione dell''enzima ScaI con il nuovo enzima XmnI nei nuovi lotti di DNA standard o Positive Control.
Aggiornamento modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23, con l''indicazione del nuovo enzima XmnI dove previsto.
Formazione del personale di Produzione.
Monitoraggio del processo di sostituzione dell''enzima ScaI con il nuovo enzima XmnI nei nuovi lotti di DNA standard o Positive Control.
Aggiornamento modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23, con l''indicazione del nuovo enzima XmnI dove previsto.
Formazione del personale di Produzione.
Monitoraggio del processo di sostituzione dell''enzima ScaI con il nuovo enzima',N'08/03/2018
08/03/2018',N'Verificare quali plasmidi attualmente linearizzati con ScaI possono essere linearizzati con il nuovo enzima XmnI.
Segnalare a Document Specialist la modifica del modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23, MOD09,23 e MOD955-pXXX-L-ScaI portando a 3 ore il tempo richiesto per la linearizzazione.
Aggiornamento modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23, con l''indicazione del nuovo enzima XmnI dove previsto e MOD955-pXXX-L-ScaI portando a 3 ore il tempo richiesto per la linearizzazione.
Formazione del personale di Produzione. MOD18,02 21/22-5-2018.
Monitoraggio del processo di sostituzione dell''enzima ScaI con il nuovo enzima XmnI nei nuovi lotti di DNA standard o Positive Control.
Messa in uso della nuova revisione del modulo "Elenco Prodotti / Moduli Diluizioni", MOD09,23.',N'IN USO MOD09,23 - rac18-9_messa_in_uso_mod_diluizione_plasmidica_rac17-16_e_altri_rac17-27_cc12-17_cc18-17_cc18-25_rap17-26_nc17-125.msg
inserito il nuovo enzima Xmnl - mod09,23_04_elenco_prodotti-moduli-diluizioni_1.xlsx',NULL,N'18/04/2018
18/04/2018
18/04/2018
08/03/2018',N'31/05/2018',N'07/05/2018
07/05/2018
25/05/2018
25/05/2018',N'Si',N'aggiornare i DMRI di prodotto',N'No',N'non necessaria',N'No',N'nessun impatto',N'-',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'R&D1.9',N'No',N'-',N'No',N'-',N'Anna Capizzi',N'No',NULL,'2018-07-30 00:00:00',N'Monitoraggio del processo di sostituzione dell''enzima ScaI con il nuovo enzima XmnI nei nuovi lotti di DNA standard o Positive Control. 16.07.2019 Si osservano ancora delle bande di non linearizzato sulle linearizzazioni dei plasmidi che non danno origine a NC in quanto corrispondenti ad una quantità esigua di DNA non linearizzato (vedere tabella dei lotti allegata "RAC18-9_tabellalinearizzazioneplasmidi_LOTTI")',N'Stefania Brun',N'Negativo',N'L''utilizzo di Xmn1 non ha risolto i problemi di linearizzazione pertanto proseguono le attività di indagine per l''identificazione delle criticità del processo: tra queste la linearizzazione eseguita utilizzando il termoblocco. Aggiornare il requisito a pag.4 del modulo MOD955-pXXX-L-XMN1in modo da includere anche i casi di plasmide non linearizzato al 100%',N'non sono emerse NC relative alla linearizzazione',N'Positivo','2018-05-31 00:00:00','2018-05-25 00:00:00','2021-03-04 00:00:00'),
    (N'18-8',N'Correttiva','2018-03-07 00:00:00',N'Katia Arena',N'Magazzino',N'Reclamo',N'18-10',N'18-1;18-2;18-4;18-5',N'Documentazione SGQ',N'Dopo una verifca interna con i magazzinieri si è visto che l''attuale picking list è di difficile utilizzo e compilazione.',N'I miglioramenti proposti sono i seguenti: Accorpamento per il  codice e per lo stesso lotto; la lista dei prodotti avviene per temperratura di stoccaggio prevedendo che prima venogono prelevati i TA, poi +4 C, poi -20 C ed infine i -70 C; infine accorpamento di prelievo per stessa ubicazione. Inoltre si è valutato di eliminare le informazioni non necessarie al prelievo dei prodotti ( descrizione e scadenza). Infine per evidenziare meglio i prodotti si è pensato di separarli con interlinea.  ',NULL,N'Impatto positivo in quanto permetterà ai magazzinieri di avere una lista di prelievo molto più semplice da gestire con un flusso di lavoro più funzionale
Impatto positivo in quanto permetterà ai magazzinieri di avere una lista di prelievo molto più semplice da gestire con un flusso di lavoro più funzionale
Impatto positivo in quanto permetterà ai magazzinieri di avere una lista di prelievo molto più semplice da gestire con un flusso di lavoro più funzionale
Impatto positivo in quanto permetterà ai magazzinieri di avere una lista di prelievo molto più semplice da gestire con un flusso di lavoro più funzionale
Riduzione dei reclami di spedizione dovuti a mancanza di merce citata nel DDT.',N'07/03/2018
07/03/2018
07/03/2018
07/03/2018
07/03/2018',N'I miglioramenti proposti sono i seguenti: Accorpamento per il  codice e per lo stesso lotto; la lista dei prodotti avviene per temperratura di stoccaggio prevedendo che prima venogono prelevati i TA, poi +4 C, poi -20 C ed infine i -70 C; infine accorpamento di prelievo per stessa ubicazione. Inoltre si è valutato di eliminare le informazioni non necessarie al prelievo dei prodotti ( descrizione e scadenza). Infine per evidenziare meglio i prodotti si è pensato di separarli con interlinea.  
Introduzione nuovo modulo di Navision ( Modulo Warehouse) 
02/2020: il modulo di Modulo Warehouse non è approvato per il nuov NAV2018. Il nuovo NAV2018 gestisce in automatico  il barcode. si valuta pertanto opportuno utilizzare il QRcode, già presente sulle etichette, per la gestione del magazzino. PW fornirà in allegato il dettagli delle attività e le tempistiche.
Introduzione barcode nella picking list
Aggiornamento procedura PR15,01 ed eventuali istruzioni associate
Verifica e messa n uso della PR15,01',NULL,NULL,N'07/03/2018
07/03/2018
07/03/2018
07/03/2018
07/03/2018',N'16/05/2018
16/05/2018
31/05/2018
31/05/2018
31/05/2018',N'07/03/2018
24/09/2018
24/09/2018',N'No',N'nessun impatto, modifica di processo',N'No',N'nessun impatto, modifica di processo',N'No',N'nessun impatto, modifica di processo',N'nessun impatto, la modifica migliora il processo di gestione delle spedizioni attrvarso l''uso di un barcode',N'SW - Elenco Software',N'SW1 - NAV (sistema gestionale)',N'da valutare',N'No',N'nessun impatto, modifica di processo',N'No',N'nessun impatto, modifica di processo',NULL,N'No',NULL,NULL,N'a 03/2021: il progetto di barcode era terminato, ma le nuove esigenze dettate dal  nuovo magazzino EGSpA e dalla dismissione del magazzino esterno ILMED hanno richiesto una ri-analisi del progetto
',NULL,NULL,NULL,NULL,NULL,'2018-05-31 00:00:00',NULL,NULL),
    (N'17-12',N'Correttiva','2017-05-19 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-48',N'SPG07210',N'Documentazione SGQ',N'In base all''indagine è evidente un problema legato ai criteri e modalità di CQ . Infatti il lotto testato parallelamente al lotto in uso presenta prestazioni paragonabili. inoltre il test è stato eseguito con il prodotto Elite one-step e ha permesso di ridurre al minimo la variabilità sperimentale e quindi la verifica del lotto. Le modalità attuali prevedono l''utilizzo del prodotto BRK200 e del prodotto Alert pertanto l''esito finale è influenzato dal doppio dei fattori sperimentali rispetto all''utilizzo del prodotto One-step.',N'Il lotto può essere accettato e PW può far richiesta del prodotto al fornitore
Modifica delle modalità di CQ utilizzando il prodotto one-step',NULL,N'Modifica dei moduli di CQ del prodotto.
Verifica del lotto in uso (già eseguita in fase di indaigne) e del nuovo lotto che sarà fornito dal fornitore con le nuove modalità di CQ
Modifica dei moduli di CQ del prodotto.
Verifica del lotto in uso (già eseguita in fase di indaigne) e del nuovo lotto che sarà fornito dal fornitore con le nuove modalità di CQ
Modifica dei moduli di CQ del prodotto.
Verifica del lotto in uso (già eseguita in fase di indaigne) e del nuovo lotto che sarà fornito dal fornitore con le nuove modalità di CQ
Definire i nuovi criteri di controllo qualità, in accordo con il QC.
Definire i nuovi criteri di controllo qualità, in accordo con il QC.
In accordo con R&D e QC sviluppare una specifica per il controllo dei campioni inviati per controllo funzionale.
In accordo con R&D e QC sviluppare una specifica per il controllo dei campioni inviati per controllo funzionale.
messa in uso documenti
messa in uso documenti
30/11/2018: è in corso il riordino dei material',N'19/05/2017
19/05/2017
19/05/2017
19/05/2017
19/05/2017
19/05/2017
19/05/2017
19/05/2017
19/05/2017',N'Modifica dei moduli di CQ del prodotto.
Messa in uso moduli al 24/5/2017 e formazione al personale del 24/05/2017 (MOD18,02 per produzione e cq)
Verifica del lotto in uso (già eseguita in fase di indaigne) e del nuovo lotto che sarà fornito dal fornitore con le nuove modalità di CQ
30/11/2018: è in corso il riordino dei materiali per la produzione di un nuovo lotto. Aggiornare i moduli di produzione.
Definire i nuovi criteri di controllo qualità, in accordo con il QC.
Il CQ non è passato, NC17-78 pertanto sono stati modificati i criteri avvicinando il requisito alle specifiche del prodotto
30/11/2018: è in corso il riordino dei materiali per la produzione di un nuovo lotto. Essendoci ancora 35 kit (lotto U0617AR) a magazzino si è valutato opportuno effettuare dei test di stabilità per poter aumentare la scadenza, essendo 5 anni la scadenza assegnata alla materia prima dal fornitore (2022/04), vedere SPEC950-181allegata.
In accordo con R&D e QC sviluppare una specifica per il controllo dei',N'Dati_Criteri - confrontotest.xlsx
nuovi criteri in seguito a NC17-78 - mod1012-spg07-210_04.xlsx
messa in uso MOD10,12-SPG07-210 rev03 - rac17-12_messa_in_uso_mod_spg07_.pdf
Indagine NC17-78 - mod10,11b_nc17-78_spg07-210_u0617ar.pdf
messa in uso - rac17-12_messa_in_uso_mod_spg07_.pdf
Mail conferma superamento test stabilità accelerata per estensione scadenza lotto U0617AR - rac17-12_test_stabilita_accelerata_superato_mail_campanale_pag2.msg
Report STB2019-008 del 19/02/19 - rac17-12_stb2019-008.pdf
lotto U0617AR rilavorato: nuovo lotto U0919AK con scadenza 2019/11 - rac17-12_rilavoraionespg_u0617ar.pdf
MOD CQ e MOD PROD in uso al 6/03/2019, MOD18,02 del 05/03-19 - rac17-12_messa_in_uso_moduli_prod_e_cq_spg07-210_rac17-12.msg
rilavorazione lotto  - rac17-12_rilavoraionespg_u0617ar.pdf
MOD CQ E PROD in uso al 6/03/19 - rac17-12_messa_in_uso_moduli_prod_e_cq_spg07-210_rac17-12_1.msg
SEPC950-181_01, in uso al 18/03/19 - rac17-12_messa_in_uso_specxxx_nc19-24_rac18-33_rac17-12.msg',NULL,N'19/05/2017
19/05/2017
19/05/2017
30/11/2018
19/05/2017
30/11/2018
19/05/2017
30/11/2018
30/11/2018',N'30/05/2017
31/07/2017
22/05/2017
31/07/2017
15/06/2017',N'24/05/2017
11/09/2017
06/03/2019
11/09/2017
19/02/2019
30/03/2018
16/01/2019
31/05/2017
18/03/2019
16/01/2019',N'No',N'nessun impatto',N'No',N'non necessaira',N'No',N'nessun impatto',N'-',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'Non necessario aggiornare i RA per il controllo funzionale dei prodotti che arrivano in prova come "minicampioni"',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',N'non necessario','2019-12-31 00:00:00',N'Utilizzo nuovi moduli di produzione e cq al primo riordino della materia prima (scadenza lotto rilavorato 11/2019).
Il nuovo lotto è stato prodotto a 06/2019, U0619BH, con i nuovi moduli e autorizzato per il rilascio al cliente a 18/11/2019.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-07-31 00:00:00','2019-03-18 00:00:00','2020-01-07 00:00:00'),
    (N'21-9',N'Correttiva','2021-02-17 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',NULL,N'CTRCPE',N'Documentazione SGQ
Processo di produzione
Processo di CQ e criteri di CQ',N'La causa della contaminazione è collegata a una contaminazione della madre plasmidica emersa solo in questa preparazione delle diluizioni. La manipolazione è avvenuta a fine 2019 quando il laboratorio per i target e per i controlli interni era ancora promiscuo. La contaminazione non è stata individuata prima in quanto il numero di replicati in test è troppo esiguo per poter determinare una frequenza così bassa di contaminanti.',N'Si chiede di prevedere un sistema di controllo delle contaminazioni più efficace in grado di rilevare eventuali contaminazioni per tutti i target. Nello specifico, utilizzando vettori diversi per i plasmidi pBetaglobina e pIC2 (esempio pUC18) rispetto a quello utilizzato per i target (pGem-T easy) è possibile sviluppare un sistema di amplificazione che vada a rilevare la presenza del vettore pGem-T easy nelle preparazioni di CTRCPE; l''eventuale positività può essere indicativa di contaminazione per i target nella preparazione.
Inoltre al fine di ridurre le fonti di contaminazione è necessario separare qualsiasi fase di preparazione dei controlli interni rispetto ai target, manipolazioni dei plasmidi/mS2, delle linearizzazioni inclusi i reagenti utilizzati (enzimi buffer e acqua e tubi) e  lo stoccaggio di diluenti. ',NULL,N'Nessun impatto sul processo solo in termini di attività
La modifica permette di ridurre gli eventi causa di contaminazione ambientale durante la produzione e di migliorare il processo di verifica della contaminazione da plasmidi target
La modifica permette di ridurre gli eventi causa di contaminazione ambientale durante la produzione e di migliorare il processo di verifica della contaminazione da plasmidi target
La modifica permette di identificare più facilmente gli eventi di contaminazione durante la produzione e di migliorare il processo di verifica della contaminazione da plasmidi target. I 2 vettori sono omologhi per l''80%, è necessario dimostrare l''equivalenza di performance tra il prodotto in uso ed il prodotto con il nuovo vettore (test di confronto e dimostrazione della conformità ai requisiti di CQ). Parallelamente, per prevenire contaminazioni nel CTRCPE, è necessario sviluppare un sistema di amplificazione che vada a rilevare la presenza del vettore pGem-T easy nelle preparazioni di CTR',NULL,N'Approvvigionamento dei plasmidi con i nuovi vettori 
Approvvigionamento dei materiali per il prodotto di amplificazione uso CQ
Approvazione delle nuove specifiche
Stesura dei moduli di produzione del prodotto di amplificazione uso CQ
Modifica del MOD09,23
Modifica della IO09,04 preparazione standard plasmidici
Modifica MOD10,12-CTRCPE e MOD10,13-CTRCPE inserimento controllo delle diluizioni ad alto titolo di pIC2 e pBETAGLOBINA
Moduli di CQ del prodotto specifico pGem RTSpGem
Modifica del MOD10,18
Formazione
Test sui lotti pilota
Preparazione dei lotti pilota del prodotto di amplificazione specifico per pGem
Preparazione delle diluizioni dei controlli interni con il nuovo vettore 
Ordine dei plasmidi pIC2 e pBetaglobina con vettore pUC18
Modifica delle specifiche 952
Studio dei primers e sonde "RTSpGem"
Test di sviluppo del prodotto
Draft di produzione/CQ
Stesura Specifiche "RTSpGem"
Formazione
messa in uso dei documenti
modifica DMRI per CTRCPE
La modifica è significativa, identif',NULL,NULL,N'16/03/2021',N'31/07/2021',NULL,N'Si',N'da modificare',N'Si',N'effettuata attraverso una nota tecnica TN 21/01-ITA del 15/02/2021 a tutti i clienti utilizzatori del prodotto',N'Si',N'L''anomalia riscontrata ha impatto sull''utilizzo dei 2 lotti del prodotto  CPE Internal Control, U0221-001 e U0221-018, esclusivamente in associazione al prodotto EBV ELITe MGB kit (ref RTS020PLD) mentre NON interferisce in alcun modo con tutti gli altri kit della linea ELITe MGB.
I lotti di CPE Internal Control interessati da questa nota, se usati in associazione al prodotto EBV ELITe MGB kit possono infatti generare risultati falsi positivi per EBV-DNA. Sulla base delle evidenze sperimentali raccolte al nostro interno questo può avvenire con una frequenza variabile e comunque non superiore al 10% dei test. In tutti i casi il segnale, seppur superiore alla soglia di rilevazione, risulta essere molto debole, associabile a Ct tardivi (Ct> 38) e quindi a quantità di EBV-DNA molto basse (carica virale: 2-3 copie/reazione corrispondenti a ',N'è presente un impatto sui test eseguiti in precedenza con il prodotto  CPE Internal Control, lotti U0221-001 e U0221-018. Gli eventuali falsi positivi ottenuti avrebbero una rilevanza clinica molto limitata a causa del Ct tardivo e della conseguente bassa carica virale, ciononostante è opportuno che il cliente esegua le seguenti azioni immediate:
- interrompere l''utilizzo dei due lotti di CPE-Internal Control oggetto di questa nota in associazione a qualsiasi lotto del prodotto EBV ELITe MGB kit
- riconsiderare gli eventuali esiti positivi ottenuti in precedenza con la combinazione di prodotti riportata sopra, con particolare riguardo verso quelli caratterizzati da Ct tardivi (Ct >38) e/o da cariche virali particolarmente basse (EBV-DNA < 40 copie/ml) e, se possibile, ritestarli a partire dall''estrazione utilizzando un lotto di CPE-Internal Control diverso da quelli oggetto di questa nota.
EGSpA sostituirà le confezioni di CPE-Internal Control utilizzate in associazione al prodotto EBV ELITe MGB kit.',N'ALTRO - Altro',N'ALTRO - ALTRO',N'verificare se presente un RA relativo alla contaminazione',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'eleonora deva',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-07-31 00:00:00',NULL,NULL),
    (N'17-15',N'Correttiva','2017-07-03 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-81',N'RTS523ING CTR523ING U0517AF-CB',N'Documentazione SGQ',N'Essendo il prodotto OEM e da noi unicamente dispensato, in DT lo studio è stato eseguito su un unico lotto di produzione, il dato statistico si conferma pertanto troppo preliminare. 
',N'In base a queste considerazioni e al fatto che il prodotto è in OEM e quindi soggetto a criteri di CQ e modifiche del processo da parte di FT, deve essere valutata l''ipotesi di modifica dei criteri al fine di valutare le specifiche di prodotto in base alle indicazioni dell''Assay Protocol. ',NULL,N'Modifica dei criteri di CQ dei prodotti FT
Aggiornamento dei moduli
Valutazione dei nuovi requisiti di CQ
Valutazione accettazione in deroga in base ai nuovi requisiti stabiliti',N'03/07/2017
03/07/2017
03/07/2017',N'aggiornamento MOD10,13 e MOD10,12 FT + IC500.
Moduli aggiornati, formazione MOD18,02 del 9/8/17
messa in uso documenti.
mesa in uso al 10/8/2017, vedi mail allegata.',N'report di indagine sul lotto U0517AF e U0517CB oggetto della NC17-81 e della RAC in oggetto.RAC - mod10,11b_nc17-81_rts523ing-ctr523ing_u0517af-cb.pdf',NULL,N'03/07/2017
03/07/2017',N'31/08/2017
31/08/2017',N'09/08/2017
10/08/2017',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto',N'No',N'non necessaria',N'No',N'non necessaria',N'Federica Farinazzo',N'No',N'no','2019-10-31 00:00:00',N'I primi 3 lotti di RTS523ING / CTR523ING sono rispettivamente U0818BE-U0219AU-U0619BF e U0818BC-U1118BB-U0219BQ-U0619Bg, il cui esito CQ è conforme. ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-08-31 00:00:00','2017-08-10 00:00:00','2019-09-04 00:00:00'),
    (N'20-34',N'Correttiva','2020-10-09 00:00:00',N'Stefania Brun',N'Global Service',N'Reclamo',N'20-64',N'INT030 ELITe InGenius',N'Componenti principali',N'La causa è da imputare alla mancata aspirazione del campione dovuta ad un coagulo che ostruisce il puntale del single nozzle che il software non ha rilevato perchè il controllo della pressione non è esercitato lungo tutta la fase di aspirazione del campione ma solo in determinati punti pertanto se il coagulo blocca l''aspirazione in questi punti lo strumento non segnala l''errore con la conseguenza di non trasferire il campione nel tubo secondario.',N'Il fornitore PSS fornirà la versione 1.3.0.16  ad integrazione delle funzionalità del software che prevedono il controllo pressorio lungo tutta la fase di aspirazione del campione da tubo primario e il controllo della presenza del campione una volta trasferito nel tubo secondario.',NULL,N'Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello strumento con la conseguente possibilità di fornire un risultato anomalo
Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello strumento con la conseguente possibilità di fornire un risultato anomalo
Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello strumento con la conseguente possibilità di fornire un risultato anomalo
Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello strumento con la conseguente possibilità di fornire un risultato anomalo
Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello strumento con la conseguente possibilità di fornire un risultato anomalo
Il cambiamento è positivo in quanto riduce il rischio della mancata rilevazione di coaguli da parte dello st',N'13/10/2020
13/10/2020
13/10/2020
13/10/2020
14/10/2020
14/10/2020
14/10/2020
22/10/2020
23/10/2020
27/10/2020
28/10/2020',N'Valutare con PSS  l''integrazione dell''analisi dei rischi dello strumento 
Aggiornare il Fascicolo Tecnico FTPINT030 con la nuova documentazione prodotta da PSS
release note 

Aggiornare FTP
Supportare SIS nelle attività di verifica della versione 1.3.0.16 che verrà rilasciata da PSS per correggere quanto sopra descritto; stesura PLAN e REPORT di installazione del pacchetto 
Pianificare l''aggiornamento di tutti gli strumenti installati con i 2 script
Stesura ed invio TSB
Fornire nuovo pacchetto sw versione 1.3.0.16 
Collaborare con il Global service alla verifica degli script, stesura PLAN e REPORT validazione nuova versione sw 1.3.0.16 
nessuna attività a carico
Pianificare l''aggiornamento di tutti gli strumenti installati 
Bridge test team Bothell 
510K evaluation 
Summary Report 
Draft Release Note ',N'Implementazione modifica script SampleDisp.scr per aspirazione campione da tubo primario - r_bug_fixing_sw_ingenius_prossima_versione_cosa_chiedere_a_pss_minuta.msg
plan2020-029 - plan2020-029_installation_process_validation_plan_ingenius_sw_v1.3.0.16_int030_rac20-34_rev00_signed.pdf
plan2020_031 - plan2020_031_plan_of_verification_and_validation_of_sw_v1.3.0.16_int030_rac_20-34_00_signed.pdf
rep2020-175 - rep2020-175_verification_report_elite_ingenius_sw_v1.3.0.16_int030_rac_20-34_rev00_signed.pdf
rep2020-179 - rep2020-179_summary_vv_report_elite_ingenius_release_1.3.0.16_int030_rac20-34_rev00_signed.pdf
rep2020-162 - rep2020-162_installation_process_validation_report_ingenius_sw_v1.3.0.16_int030_rac20-34_rev00_signed.pdf
TSB 063 Rev. A  "SW Version 1.3.0.16" - rac19-23_invio_tsb_new_sw_ver._1.3.0.16.msg
plan2020-029	30/11/2020 plan2020_031	03/12/2020 rep2020-175	16/12/2020 rep2020-179	18/12/2020 rep2020-162	18/12/2020 - rac19-23_messa_in_uso_sw_ingenius_v1.3.0.16.msg
release note del 16/12/2020 - 2020_12_16_release_note_sw_1.3.0.16_ingenius.pdf',NULL,N'13/10/2020
13/10/2020
14/10/2020
14/10/2020
22/10/2020
23/10/2020
27/10/2020
28/10/2020',N'30/12/2020
30/12/2020
30/11/2020
15/03/2021
15/12/2020
13/11/2020
30/11/2020
31/08/2021
31/08/2021',N'16/12/2020
18/12/2020
13/01/2021
18/12/2020
18/12/2020
30/04/2021
18/12/2020',N'No',N'Nessun impatto sul DMRI',N'No',N'Si da fare',N'No',N'Si da pianificare aggiornamento di tutti gli strumenti installati',N'Il cambiamento non impatta sui requisiti regolatori se non in maniera migliorativa in quanto ripristina il profilo di rischio dello strumento',N'SVM1 - Virtual Manufacturer: ricerca e sviluppo strumento ',N'SVM1.3 - Ricerca e sviluppo progettazione software , presso Virtual Manufacturer',N'Da valutare ',N'No',N'vedere Mod05,36',N'No',N'No in quanto lo strumento non è oggetto di accordi con altri virtual manufacturer',N'Marcello Pedrazzini',N'Si',N'Da valutare integrazione con PSS',NULL,N'Dopo l''implementazione dell''azione verificare assenza di reclami imputabili alla mancata rilevazione di coaguli da parte dello strumento',NULL,NULL,NULL,NULL,NULL,'2021-08-31 00:00:00',NULL,NULL),
    (N'19-29',N'Correttiva','2019-10-03 00:00:00',N'Maria Galluzzo',N'Validazioni',N'Reclamo',N'19-82',N' Assay Protocol : CMV ELITe_WB_200_100_03 e CMV ELITe_PL_200_100_02',N'Assay Protocol',N'L''aggiornamento, che ha richiesto la disinstallazione degli Assay Protocol presenti e l''installazione delle ultime revisioni degli Assay Protocol, ha evidenziato che gli AP associati alla matrice sangue e plasma (CMV ELITe_WB_200_100_03 e CMV ELITe_PL_200_100_02) hanno il campo delle unità di misura "Default Result Reporting Unit" dell''Interpretation Model impostato in IU/mL e gli AP associati alle matrici CSF, U, BS e AF in gEq/mL. La modifica delle unità di misura è stata apportata a seguito di una RAC precedente, RAC19-7. Poichè il sistema in uso presso il cliente prevede l''interfacciamento al LIS tramite hub ELITeWare, quest''ultimo ha fornito i seguenti errori  "UDM: VERIFICA UNITA'' DI MISURA" dovuti proprio al disallineamento delle unità di misura rispetto a quelle impostate sugli AP. Questo disallineamento ha comportato un''errata conversione del risultato, di seguito descritto:
-	un valore numerico espresso in IU/mL che non era corretto poiché è stato convertito due volte, una volta su InGenius e una volta su ELITeWare generando il dato finale errato di 3.051 IU/ml invece del dato corretto di 4.358 IU/ml 
-	un valore numerico espresso in gEq/mL che è corretto poiché i gEq/mL sono considerabili eguagliabili alle copies/mL ma che ha la dicitura di riconoscimento sbagliata proprio perché espresso in gEq/mL e non in copies/mL.
(Si veda il report di indagine allegato al reclamo 19-82 per ulteriori dettagli)
CAUSA PRIMARIA: rilascio degli Assay Protocol in modalità Closed di WB e PL per CMV con una nuova impostazione dell''unità di "as default" in IU/ml (e non in gEq/mL) è stata apportata senza una valutazione accurata degli impatti. La modica degli AP è stata apportata nell''ambito del progetto di estensione d''uso per il reagente di estrazione InGenius SP1000, in occasione del quale gli AP sono stati anche aggiornati in linea con le prescrizioni della IO04,08 allora in uso che indicava genericamente di convertire gli AP nelle unità di misura disponibili per il target.La valutazione dell''impatto a livello di sistemi interfacciati al LIS tramite l''ELITeware è stata eseguita da parte del Global Support e R&D durante una riunione ma non è stata tracciata e condivisa con il reparto validazioni che ha poi apportato le modifiche.',N'CORREZIONE: effettuare le modifiche alle unità di misura degli attuali AP rilasciati, come da IO04,08_01 "Istruzione operativa per la creazione di Assay Protocol InGenius" appena revisionata (09.09.2019), in cui, per la creazione di Assay Protocol Model 2 (3.0.2) sono definite le modalità di impostazione nel Default Result Reporting Unit.
AZIONE CORRETTIVA: valutazione relativa alla revisione del processo di creazione e modifica degli Assay Protocol (IO04,08)',NULL,N'Positivo in quanto dopo la revisione del processo di Gestione delle modifiche degli Assay Protocol tutti gli impatti di modifiche relative ad AP rispetto anche a sistemi esterni quali ELITeWare saranno presi in considerazione prima di implementare qualsiasi tipo di modifica
Positivo in quanto dopo la revisione del processo di Gestione delle modifiche degli Assay Protocol tutti gli impatti di modifiche relative ad AP rispetto anche a sistemi esterni quali ELITeWare saranno presi in considerazione prima di implementare qualsiasi tipo di modifica
Positivo in quanto dopo la revisione del processo di Gestione delle modifiche degli Assay Protocol tutti gli impatti di modifiche relative ad AP rispetto anche a sistemi esterni quali ELITeWare saranno presi in considerazione prima di implementare qualsiasi tipo di modifica
Positivo in quanto con la revisione del processo di creazione e modifica degli Assay Protocol (IO04,08) è stata introdotta la modifica relativa all''impostazione nel Default Result Reporting Un',N'03/10/2019
03/10/2019
03/10/2019
03/10/2019
03/10/2019
18/10/2019
03/10/2019
03/10/2019',N'Effettuare le modifiche alle unità di misura degli attuali AP rilasciati, come da IO04,08_01 "Istruzione operativa per la creazione di Assay Protocol InGenius" appena revisionata (09.09.2019), in cui, per la creazione di Assay Protocol Model 2 (3.0.2) sono definite le modalità di impostazione nel Default Result Reporting Unit. Si veda nel report allegato, paragrafo "MISURE D''INTERVENTO (CORREZIONI) PROPOSTE PER TUTTI I PRODOTTI COINVOLTI", l''elenco degli Assay protocol dei prodotti in cui è necessario apportare le modifiche richieste come per gli AP CMV ELITe_WB_200_100_03 e CMV ELITe_PL_200_100_02.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
verifica TAB
Nel caso di aggiornamenti degli Assay Protocol è necessario verificare presso i centri interfacciati che il fattore di conversione rimanga lo stesso; dovesse cambiare sarà necessario aggiornarlo anche su EliteWare.
Nel caso di aggiornamento della ',N'Report di Indagine - report_indagine_r19-82_1.docx
AP CMV - rac19-29_ifu_rtk015pld_nc19-112;_rac19-32_-_ap_rtk015pld_rac19-29.msg
AP PARVO - rac19-29_ifu_rts070pld_rac19-29.msg
AP EBV - rac19-29_mail_invio_tab_p17_-_rev_ab_-_product_ebv_elite_mgb_notice_1.msg
La modifica non è significativa, pertanto non è da notificare. - rac19-29_mod05,36__non_significativa.pdf
 TAB P14 - Rev AB PVB19 ELITe MGB KIT  - rac19-29_messa_in_uso_tab_parvo_1.msg
 TAB P16 - Rev AC CMV ELITe MGB KT  - rap19-29_elite_ingenius_-tab_p16_-_rev_ac_-__product_cmv_elite_mgb_notice_1.pdf
TAB P17 - Rev AB EBV ELITe MGB notice  - rac19-29_mail_invio_tab_p17_-_rev_ab_-_product_ebv_elite_mgb_notice.msg
TAB P16 - Rev AC CMV ELITe MGB KT - rap19-29_elite_ingenius_-tab_p16_-_rev_ac_-__product_cmv_elite_mgb_notice.pdf
TAB P14 - Rev AB PVB19 ELITe MGB KIT - rac19-29_messa_in_uso_tab_parvo.msg
TAB P17 - Rev AB  EBV ELITe MGB notice - rac19-29_tab_p17_-_rev_ab_-_product_ebv_elite_mgb_notice.pdf.pdf
AP CMV - rac19-29_ifu_rtk015pld_nc19-112;_rac19-32_-_ap_rtk015pld_rac19-29_1.msg
APPARVO - rac19-29_ifu_rts070pld_rac19-29.msg
AP EBV - rac19-29_messa_in_suo_nuova_revisione_ap_ebv_rac_19-29.msg',NULL,N'03/10/2019
03/10/2019
03/10/2019
03/10/2019
18/10/2019
03/10/2019
03/10/2019',N'31/10/2019
23/10/2019',N'13/05/2020
13/05/2020
13/01/2021
13/05/2020
13/05/2020
13/05/2020
03/10/2019',N'No',N'No in quanto il DMRI non contiene le revisioni degli AP nè delle procedure',N'Si',N'Si in quanto gli AP modificati devono essere installati presso i clienti',N'No',N'No, il prodotto in commercio non è impattato',N'I prodotti non subiscono modifiche alle prestazioni e sicurezza.',N'IFU - Gestione Manuali
R&D - Svilupppo e commercializzazione prodotti',N'IFU2 - riesame manuale
R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'nessun impatto, anche se (dall''analisi degli eventi che ricadono in questi sottoprocessi) la probaboilità del verificarsi di questo evento diventasse "alta", la matrice di gestione del rischio non subirebbe alterazioni',N'No',N'vedere MOD05,36, le modifiche di AP e della  PR05,08  non sono modifiche che necessitano di essere notificate',N'No',N'vedere MOD05,36, le modifiche di AP e della PR05,08 non sono modifiche che necessitano di essere notificate al Virtual manufacturer',NULL,N'No',NULL,'2020-11-30 00:00:00',N'VALUTAZIONE EFFICACIA: verificare che con i nuovi ap non si verifichino altri reclami con uguale causa primaria.
Gennaio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva. (l''unico reclamo presente è il 20-61 su RTS070PLD la cui causa primaria non è stata individuata).
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2019-10-31 00:00:00','2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'19-11',N'Preventiva','2019-04-16 00:00:00',N'Roberta  Paviolo',N'Regolatorio',NULL,NULL,NULL,N'Documentazione SGQ',N'L''approvazione di tutte le etichette di tutti i REF di prodotto è sufficiente a garantire la correttezza delle informazioni regolatorie e la conformità dell''etichetta. Tuttavia, si ritiene opportuno introdurre l''approvazione dei template all''interno del processo di creazione delle etichette affinchè si possa semplificare l''approvazione delle etichette specifiche di prodotto. 
La messa in uso del template prevederà l''approvazione della parte regolatoria, di marketing e di produzione, la messa in uso di una nuova etichetta specifica per prodotto prevederà solo più l''approvazione delle caratteristiche intrinseche del prodottostesso (es: componenti, temperatura di stoccaggio ecc).',N'Aggiornamento ISTRUZIONE OPERATIVA CREAZIONE ETICHETTE PRODOTTI CE-IVD E RUO, codice IO04,06 rev02 per introdurre la gestione dei template come draft, in uso ed obsoleti ed i responsabili dell''approvazione.',NULL,N'Impatto positivo in quanto si standardizza la gestione dei template defininendo in istruzione le responsabilità di creazione, modifica ed approvazione. Inoltre, si ridurranno i tempi  di creazione ed approvazione delle etichette per i nuovi prodotti che utilizzano template in uso, in quanto si andranno solo più a verificare le caratteristiche intrindeche del prodotto, avendo già approvato la parte regolatoria.
Impatto positivo in quanto si standardizza la gestione dei template defininendo in istruzione le responsabilità di creazione, modifica ed approvazione. Inoltre, si ridurranno i tempi  di creazione ed approvazione delle etichette per i nuovi prodotti che utilizzano template in uso, in quanto si andranno solo più a verificare le caratteristiche intrindeche del prodotto, avendo già approvato la parte regolatoria.
Impatto positivo in quanto si standardizza la gestione dei template defininendo in istruzione le responsabilità di creazione, modifica ed approvazione. Inoltre, si ridurranno i tempi  d',N'07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019',N'Aggiornamento ISTRUZIONE OPERATIVA CREAZIONE ETICHETTE PRODOTTI CE-IVD E RUO, codice IO04,06 rev02:
- introduzione gestione dei template come draft, in uso ed obsoleti 
- definizione responsabili delle creazione, verifica ed approvazione dei template,
- inserimento dei riferimenti ai nuovi moduli di approvazione dei template e delle etichette.
Creazione di un nuovo modulo di approvazione dei template ed aggiornamento del modulo di approvazione etichette MOD04,13 CHECK LIST ETICHETTE rev02.
Formazione al personale coinvolto sulla revisione dell''istruzione s sui nuovi moduli di approvazione template ed etichette.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
All''inizio dei test per la validazione del getionale NAV 2018 (vedere CC18-51), in particolare all''inizio della validazione del processo di stampa ed etichettatura dei prodotti, approvazione dei template per la parte regolatoria (simboli richiesti ',N'Introduzione nell''istruzione di creazione delle etichette il processo di gestione dei template - rap19-11_messa_in_uso_istruzione_e_moduli_approvazione_template_etichette.msg
MOD04,13_03, MOD04,31_00 - rap19-11_messa_in_uso_istruzione_e_moduli_approvazione_template_etichette_1.msg
MOD18,02 31/07/2019 - rap19-11_formazione_gestion_etichette_e_documenti_a_supporto_del_31-07-19.txt
In uso creazione etichette, allegato tempalte etichette, modulo di approvazione template ed etichette  - rap19-11_messa_in_uso_istruzione_e_moduli_approvazione_template_etichette_2.msg
VMP, RA et1, ET2, ET3 aggiornati e firmati il 12/05/2020 - mod09,30_01_et_cc18-51_rap19-11_cc19-19.pdf
La modifica NON è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - rac19-11_mod05,36_non_significativa.pdf
TEMPLATE APPROVATI IL 07/01/2020 - rap19-11_template_approvati_il_07-01-2020.txt
a 12/2020 (con la futura dismissione dei bang e dei loro accessori), rimangono da implementare in NAV le seguenti etichette: •             910-EXTB01 esterna, 910-EXTB02 interne ed esterne, 910-INT02100100 - rap19-11_implementazione_in_nav_etichette_rimaste_word.msg
TEMPLATE APPROVATI IL 07/01/2020  - rap19-11_template_approvati_il_07-01-2020_1.txt
RAP19-11_template approvati il 07-01-2020 - rac19-11_mod05,36_non_significativa.pdf',NULL,N'07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019
07/05/2019',N'31/05/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019
28/06/2019
31/05/2019
31/05/2019
31/10/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019',N'05/08/2019
05/08/2019
31/07/2019
17/05/2019
07/01/2020
05/08/2019
12/05/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020',N'No',N'nessun impatto sui DMRI, la IO04,06 non è citata',N'No',N'non necessaria alcuna comunicazione, miglioramento processo interno',N'No',N'nnessun impatto sul prodotto immesso in commercio, miglioramento processo interno',N'Istruzione conforme ai requisiti regolator',N'ET - Gestione Etichette
ET - Gestione Etichette
ET - Gestione Etichette',N'ET1 - Individuazione dei simboli/dati da inserire sull''etichetta
ET2 - Stesura dell''etichetta
ET3 - Approvazione dei contenuti dell''etichetta',N'Aggiornate ET1, ET20, ET3 in data 12/05/2020',N'No',N'non siginificativa, vedre MOD05,36',N'No',N'non siginificativa, vedre MOD05,36',N'Roberta  Paviolo',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2019-10-31 00:00:00',NULL,NULL),
    (N'20-27',N'Preventiva','2020-09-07 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ
Processo di produzione',N'Il prodotto SARS COV2 è stato sviluppato inizialmente solo come kit qualitativo e solo successivamente come quantitativo. L''immissione in commercio del kit quantitativo è prevista per metà ottobre. Visto che l''attuale produzione di RTS170ING avviene con ritmi elevati (3-4 lotti a settimana) è necessario che questi siano testati al cq sia come kit qualitativi (prodotto in commercio) che quantitativi (prodotto in commercio a metà ottobre). Attendere l''immissione in commercio del prodotto STD170ING per poter controllare il lotti già prodotti anche per le prestazioni quantitative risulterebbe complicato a livello organizzativo oltre che essere rischioso per il cliente che dovrebbe essere informato dei lotti utilizzabili solo con il CTR170ING e di quelli utilizzabili anche con lo STD170ING.',N'Testare al CQ i lotti di produzione utilizzando, oltre ai lotti di produzione di CTR170ING, anche i lotti pilota di STD170ING. Tale richiesta, in deroga alla procedura che prevede la completa separazione di prodotti non ancora rilasciati da quelli in commercio, è fatta per prevenire usi improprio del prodotto RTS170ING al momento dell''immissione in commercio del prodotto STD170ING. Pertanto la richiesta è di tipo temporaneo: i lotti testati con l''utilizzo di lotti pilota di STD170ING saranno allegati alla presente RAP.',NULL,N'L''impatto è positivo in quanto i lotti possono essere controllati, oltre che qualitativamente, anche quantitativamente, ossia già con le modalità di utilizzo previste  per i clienti. I tempi di esecuzione del CQ ed il rilascio dell''esito in modalità quantitativa risultano superiori  rispetto al tempo previsto per il solo uso qualitativo, questo tempo maggiore risulterebbe già da subito anzichè da ottobre; d''altro canto sarebbe comunque necessario testare i lotti quantitativamente al di fuori del controllo qualità  prima dell''immissione in commercio del prodotto.
I tempi di esecuzione del CQ e il rilascio dell''esito risultano superiori già da ora rispetto al tempo previsto per il solo uso qualitativo.
L''esecuzione delle prestazioni quantitative del prodotto verrà eseguito da parte del CQ permettendo di ridurre i test di valutazione quantitativa dei lotti eseguiti fino ad ora da R&D in fase di progettazione.
L''impatto è positivo in quanto i lotti prodotti da oggi all''immissione in commercio del ',N'08/09/2020
08/09/2020
10/09/2020
10/09/2020',N'Esecuzione dei controlli qualità utilizzando le nuove modalità qualitative e quantitative
Preparazione dell''elenco dei lotti testati in deroga alla Procedura
nessuna attività
nessuan attività
Allegare tutti i lotti di RTS170ING testati con lotti pilota di STD170ING e gli esiti del CQ.
Nell''eventualità di lotti che non passano in il cq per le prestazioni quantitative prevedere la loro commercializzazione solo per uso qualitativo.',N'la modifica non è significativa, in quanto il prodotto quantitativo (STD170ING) non è ancora immesso in commercio, pertanto non è da notificare. - rap20-27_mod05,36_nonsignificativa.pdf
dichiarazione di conformità rev33 firmata il 15/10/2020 - mod05,05c_real_time_elite_mgb_33.pdf
Elenco Lotti produzione ritestati con metodo quantitativo - elenco_lotti_produzione.xlsx',NULL,N'07/09/2020
08/09/2020
10/09/2020
10/09/2020',N'31/10/2020
31/10/2020',N'21/10/2020
08/09/2020
10/09/2020
15/10/2020',N'Si',N'aggiornare DMRI2020-005',N'No',N'non necessaria perchè il prodotto STD170ING non è ancora stato commercializzato',N'No',N'nessun impatto: i prodotto RTS170ING immesso in commercio è attualmente utilizzabile solo con il CTR170ING. Quando verrà immesso in commercio il prodotto STD170ING il porodotto RTS170ING potrà essere utilizzato anche per le le prestazioni quantitative (aggiornamento IFU) e i lotti prodotti da 09/2020 alla data di immissione in commercio saranno già stati verificati al CQ per le prestazioni quantitative.',N'L''impatto è positivo in quanto si prevengono possibilità di futuri utilizzi impropri del prodotto RTS170ING.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'a fine anno valutare quanti eventi relativi a pianificazioni differenti dalle richieste di mercato sono emersi',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2021-04-30 00:00:00',N'vetrificare assenza di reclami imputabili al prodotto STD170ING
Al 28/10/2021 non sono stati registrati reclami imputabili al prodotto STD170ING',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2020-10-31 00:00:00','2020-10-21 00:00:00','2021-04-28 00:00:00'),
    (N'17-18',N'Correttiva','2017-09-04 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',NULL,N'RTS078PLD; NC17-40; NC17-70; NC17-86',NULL,N'I criteri di CQ non sono stati in grado di far emergere l''errore tecnico e il materiale di partenza del lotto fornito è sufficiente solo per un test CQ. ',N'Modifica del MOD962-RTS078PLD 
Modifica del MOD10,12-RTS078PLD al fine di evidenziare il rischio collegato così come descritto nel Risk Assesment CQ10.
Dato che lo stesso tipo di problema si è verificato anche nelle NC17-70 NC17-86, per quel che riguarda il test di specificità per il controllo interno, e in base ai risultati di analisi del RA CQ10 verrà pianificata la modifica delle VT che presentano lo stesso tipo di rischio.
',NULL,N'Modifica del MOD962-RTS078PLD 
Modifica del MOD10,12-RTS078PLD 
Modifica del MOD10,12-XXX 
Valutazione delle modifiche dei criteri CQ
Valutazione RA e messa in uso dei documenti',N'04/09/2017
04/07/2017
11/09/2017',N'Modifica del MOD962-RTS078PLD 
Modifica del MOD10,12-RTS078PLD
Modifica del MOD10,12-XXX.
moduli CQ in uso al 26-2/2018, come da mail allegata, formazione MOD18,02 del19/02/2018
Modifica RA CQ10
Valutazione delle modifiche dei criteri CQ
Valutazione RA  messa in uso dei documenti',N'messa in uso moduli CQ - rac17-18_messa_in_uso_mod10,12-xxx.pdf
MOD962-RTS078PLD REV02 - rac17-18_messa_in_uso_mod962-rts088pldrev02_.pdf
MOD962-RTS078PLD REV02 - rac17-18_messa_in_uso_mod962-rts098pldrev02__1.pdf
MESSA IN USO MODULI CQ - rac17-18_messa_in_uso_mod10,12-xxx_1.pdf
action plan - rac17-18_ra-cq10_action_plan.xlsx',NULL,N'04/09/2017
04/09/2017
04/09/2017',N'31/12/2017
31/12/2017
31/12/2017',N'19/02/2018
26/02/2018
26/02/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'CQ - Controllo Qualità prodotti',NULL,N'CQ10 e CQ8',N'No',NULL,N'No',NULL,N'Federica Farinazzo',N'No',N'nessun impatto','2019-01-08 00:00:00',N'Dal lotto U0817AK risultato NC, come da NC17-103, sono stati prodott 2 lotti U0118AA-U0518BD-U0918AA-U1218AV risultati conformi.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-12-31 00:00:00','2018-02-26 00:00:00','2019-01-08 00:00:00'),
    (N'17-11',N'Correttiva','2017-04-28 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-33',N'RTSACC02',N'Documentazione SGQ',N'Piastre AB non conformi ( vedi NC16-17 fornitore AB)
Procedura di tracciabilità non applicata (Vedi NC17-33) ',N'Azioni con il fornitore AB per soluzione della problematica riscontrata
Sistemare la tracciabilità interna del prodotto',NULL,N'registrazione su NAV e su specifica del lotto e scadenza quando indicato dal fornitore
Confezionamenti dei lotti di RTSACCxx utilizzando un singolo lotto di componente per rendere possibile la tracciabilità
Notifica al fonitore per indagini causa e sostituzione del lotto
Trattativa con AB per la fornitura di formati adatti al nostro uso 
-
aggiornamento moduli e formazione
aggiornamento moduli e formazione',N'28/04/2017
28/04/2017
09/06/2017
28/04/2017
28/04/2017
28/04/2017',N'vedi allegato
messa in uso documenti.
in uso IO09,17 al 8/8/17, come da mail allegata.
Trattativa con AB per l''esecuzione del controllo funzionale
aggiornamento moduli e formazione. formazione del 22/05/2017 (MOD18,02)',N'action plan - rac17-11_action_plan.pdf
messa in uso MOD910_RTSACC1 e 2 - 2017-05-22_in_uso_mod910-rtsaccx_rac17-11.pdf
IO09,17 scadenze - rac17-11_io0917_scadenze_.pdf
messa in uso spec953-004, spec953-198 - rac17-11_messa_in_uso953-004_953-198_.pdf
Ad OTTOBRE/2018 è stato formalizzato il controllo delle piastre affettuato dal personale del CQ attraverso dei moduli di registrazione (MOD10,25 e MOD10,26). La formazione (MOD18,02) è stata svolta in data 15/11/2018 - rac17-11_messa_in_uso_moduli_controllo_ricevimento_piastre_rac17-11.msg
SPEC953-198_03 - rac17-11_spec953-198_03_rac17-11_mod1802_15112018.msg
trattativa colo fornitore - rac17-11_trattativa_col_fornitore_per_controllo_funzionale.pdf',NULL,N'28/04/2017
28/04/2017
24/04/2017
28/04/2017',N'30/06/2017
22/05/2017
31/12/2017
22/05/2017',N'30/06/2017
10/08/2017
06/12/2018',N'No',N'non necessario',N'No',N'-',N'No',N'-',N'-',N'F - Gestione dei Fornitori',N'F6 - Gestione delle NC dei fornitori',N'F6',N'No',N'-',N'No',N'-',NULL,N'No',NULL,'2020-06-29 00:00:00',N'ThermoFischer non ha individuato la causa primaria di distacco dei foglietti, ma installeranno un tool nell''impianto produttivo per la simulazione sullo strumento dell''adesione del foglietto. Per tutto il 2019 effettueremo il controllo meccanico in ingresso di ogni lotto di piastre. 
10/01/2020: nel 2019 ci sono stati 2 lotti non conformi relativi al numero d''ordine 18-PO-01600: I2878-Q125 e I2778-Q716.  La trattative con AB per per la fornitura di formati adatti al nostro uso non ha avuto seguito. Il controllo delle piastre continuerà per ulteriori 6 mesi al fine di confermare che non sono presenti lotti non conformi.
29/06/2020 l''azione correttiva si ritiene possa essere chiusa: da ottobre 2018 a giugno 2020 sono stati individuati solo 2 lotti non confomri, si ritiene pertanto possibile dismettere il controllo effettuato meccanico in ingresso di ogni lotto di piastre (archiviazione MOD10,25 e MOD10,26).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2017-12-31 00:00:00',NULL,'2020-06-29 00:00:00'),
    (N'18-15',N'Preventiva','2018-05-28 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'I criteri per il monitoraggio in uscita dalla RAC17-15 erano stati stabiliti in seguito all''analisi dei dati ottenuti con l''unico pilota e come pianificato in design transfer. La modifica del controllo qualità in seguito alla RAC17-15 prevedeva l''apertura di un''azione preventiva in caso di dati al di fuori dei criteri',N'Saranno preparate delle carte di controllo e in seguito all''analisi di almeno 3 lotti di produzione per ciascun prodotto saranno modificati i criteri di monitoraggio e di conseguenza i documenti di CQ',NULL,N'I criteri di monitoraggio sono stabiliti al fine di verificare eventuali derive del prodotto. Ad oggi i criteri non sono rappresentativi di questo come indicato sopra. I criteri di monitoraggi osono più stringenti di quelli di CQ e di quelli previsti dall''AP pertanto non vi sono impatti sul prodotto.
I criteri di monitoraggio sono stabiliti al fine di verificare eventuali derive del prodotto. Ad oggi i criteri non sono rappresentativi di questo come indicato sopra. I criteri di monitoraggi osono più stringenti di quelli di CQ e di quelli previsti dall''AP pertanto non vi sono impatti sul prodotto.
miglioramento del monitoraggio delle prestazioni dei prodotti FTD in associazione ad ELITe InGenius
Il moniotraggio dei lotti permette di mantenere sotto controllo i prodotti con dei dati statistici significativi.
Il moniotraggio dei lotti permette di mantenere sotto controllo i prodotti con dei dati statistici significativi.',N'28/05/2018
28/05/2018
28/05/2018
28/05/2018
28/05/2018',N'creazione delle carte di controllo 
analisi dei dati
definizione criteri
modifica MOD10,12
formazione
Revisione in collaborazione con QC dei dati elaborati con il primo lotto di produzione rapportati ai nuovi dati raccolti, al fine di stabilire i nuovi criteri per il  monitoraggio.
Nessuna attività a carico.
Nessuna attività a carico.',N'Plan - rap18-15_monitoraggio_fasttrack_plan.xlsx
RTS507ING - rts507ing_monitoraggio.docx
RTS548ING - rts548ing_monitoraggio.docx
RTS533ING - rts533ing_monitoraggio.docx
RTS523ING - rts523ing_finale.docx
MESSA IN USO MOD10,12-XXX FTD, KOD18,02 21/03/2019 - rap18-15_messa_in_uso_mod1012-ic500_05_rap18-15_mod1802_210319.msg
RTS553ING - rts553ing_finale_09-04.pdf
Riepilogo modifiche - rap18-15_riepilogo_modifiche-criteri_rap18-15.pdf
MESSA IN USO MOD10,12-RTS553ING-CTR553ING - rap18-15_messa_in_uso_mod1012-rts553ing-ctr553ing_rev01_rap18-15.msg
In uso AP RTS507ING, cut-off da 33 a 34 - rap18-15_assay_protocol_mv_rev.02_rap18-15.msg
riepilogo modifiche - rap18-15_riepilogo_modifiche-criteri_rap18-15_1.pdf',NULL,N'28/05/2018
28/05/2018
28/05/2018
28/05/2018',N'31/12/2018
31/12/2018
31/12/2018
28/05/2018',N'07/05/2019
09/04/2019
28/05/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'Lucia  Liofante',N'No',NULL,'2019-12-31 00:00:00',N'Verificare che i requisiti di monitoraggio nei moduli MOD10,12-xxx siano sufficienti per individuare lotti disomogenei.

Nel corso di circa 1 anno ci sono stati dei lotti che non hanno superato i requisiti di monitoraggio, in generale non sono lotti lineari, ma sono disomogenei. Si ritiene che i requisiti di monitoraggio nei moduli MOD10,12-xxx siano sufficienti per individuare lotti disomogenei, è pertanto possibile chiudere l''azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,'2018-12-31 00:00:00','2019-05-07 00:00:00','2020-08-06 00:00:00'),
    (N'20-8',N'Correttiva','2020-03-12 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',N'20-15',N'RTS120ING MDR/MTB ELITe MGB® Kit',N'Assay Protocol',N'A livello di progettazione, c''è stata un''errata valutazione della probabilità del verificarsi di alcuni fenomeni inibitori che rendono non efficace il modello interpretativo, come dimostrato dall''esito dell''indagine: il controllo interno della prima seduta non è rilevato, il target MTB è rilevato, tutte le Tm relative al target rpoB sono assenti. Il campione è probabilmente inibito e l''ipotesi è confermata dal fatto che la ripetizione del campione diluito 1:10 fornisca un risultato ottimale.
In assenza dei valori di Tm, il modello interpretativo 12 (dedicato a MDR-MTB) fornisce un risultato di "Resistenza positiva" come conseguenza di una possibile mancata ibridazione (ad esempio in caso di delezione). La delezione totale del gene rpoB non è però possibile e l''assenza di tutte e quattro le Tm genera un''interpretazione "Resistenza positiva" potenzialmente falsa in caso di inibizione della reazione.',N'Come azione a lungo termine si chiede di interfacciarsi con PSS per la modifica del modello interpretativo o una mitigazione dell''errata attribuzione di RESISTANCE POSITIVE, resa possibile in caso di inibizioni del campione come quella rilevata. In particolare l''attribuzione RESISTANCE POSITIVE per un campione deve avvenire solo quando è presente almeno una Tm calcolata per il target rpoB, l''idoneità del campione deve essere verificata per la presenza di Ct del CI in tutte e due le mix quando non si amplifica nessun target.
Come mitigazione si chiede di integrare l''IFU con un''avvertenza con impatto sulla sicurezza del prodotto che spieghi che in caso di mancanza di Tm rilevata per tutti e 4 i target rpoB il test deve essere considerato non valido e deve essere ripetuto diluendo il campione eluato.',NULL,N'La modifica del modello interpretativo permetterebbe di non avere più risultati errati "RESISTANCE POSITIVE" in caso di assenza di Tm per tutti e 4 i target rpoB. Questa modifica deve essere valutata con PSS e se verrà approvata ed attuata avrà impatto sugli Assay Protocol di RTS120ING che andranno aggiornati, verificati, validati e messi in uso.
Aggiungere alle IFU un''avvertenza che spieghi che in caso di mancanza di Tm rilevata per tutti e 4 i target rpoB il test deve essere considerato non valido e deve essere ripetuto diluendo il campione eluato.
Essendo il reclamo un caso isolato (con campione artificiale ed è un "falso positivo", quindi con minor impatto sul paziente) si ritengono non necessarie modifiche al processo di sviluppo.
La modifica del modello interpretativo permetterebbe di non avere più risultati errati come "RESISTANCE POSITIVE". Questa modifica deve essere valutata con PSS, se verrà approvata ed attuata avrà impatto sugli Assay Protocol di RTS120ING che andranno aggiornati, veri',N'27/03/2020
14/04/2020
14/04/2020
14/04/2020
14/04/2020
14/04/2020',N'Valutazione con PSS della possibilità di modificare il modello interpretativo.
Modifica delle IFU aggiungendo un''avvertenza.
Valutazione con PSS della possibilità di modificare il modello interpretativo 
nessana azione.
valutazione dell''impatto sull''analisi dei rischi di prodotto.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
messa in uso IFU',N'la richiesta di cambiamento è stata inoltrata a PSS. PSS preventiva 1,5 mesi di lavoro per aggiornare il modello 12. Tale aggiornamento non avverrà a breve, ma solo a seguito di altri bug/correzioni/miglioramenti raccolti nel corso dell''anno. La mitigazione dell''azione correttiva avviene momentaneamente con  l''aggiornamento dell''IFU. la causa primaria verrà eliminata solo con la correzione del modello 12. - rac20-8_mantis_ticket_441.msg
l''aggiornamento del IFU è pianificato per fine settembre/2020 - rac20-8_pianificazioneaggiornamentoifuentro09-2020_mail9-9-2020rs.txt
IFU MDR/MTB ELITe MGB Kit rev04 - rac20-08_ifu_mdrmtb_elite_mgb_kit.msg
aggiornamento del modello 12 accettato da PSS (circa 1,5 mesi per l''esecuzione dell''aggiortnamento) - rac20-8_mantis_ticket_441_1.msg
la modifica è significativa, ma verrà notificata ai paesi presso cui il prodotto èe registrato nel momento in cui si chiederà a PSS di prenderla in carico. - rac20-8_mod05,36_significativa.pdf
notifica a modifica IFU effettuata - rac20-8_notification_of_change_-rts120ing.msg
IFU MDR/MTB ELITe MGB Kit rev04 - rac20-08_ifu_mdrmtb_elite_mgb_kit_1.msg',NULL,N'27/03/2020
14/04/2020
14/04/2020
14/04/2020
14/04/2020
16/04/2020',N'31/12/2020',N'05/03/2021
05/03/2021
05/03/2021',N'No',N'nessun impatto',N'Si',N'attraverso la TAB',N'No',N'messun impatto sul prodotto in commercio',N'L''aggiunta all''IFU di un''avvertenza che spieghi che in caso di mancanza di Tm rilevata per tutti e 4 i target rpoB il test deve essere considerato non valido e deve essere ripetuto diluendo il campione eluato, introduce un''avvertenza con impatto sulla sicurezza del prodotto.',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'se si valuta necessario aggiornare il modello interepreativo con PSS, valutare se c''è impatto sulla gestione del rischio del sw Ingenius (SW20)',N'No',N'vedre MOD05,36. la modifica è significativa, ma verrà notificata ai paesi presso cui il prodotto è registrato nel momento in cui si chiederà a PSS di prenderla in carico.',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2020-12-31 00:00:00',NULL,NULL),
    (N'20-4',N'Correttiva','2020-02-14 00:00:00',N'Marcello Pedrazzini',N'Global Service',N'Reclamo',N'20-17',N'962-RTS120ING',N'Assay Protocol',N'Mancata verifica della stringa invio a LIS prima del rilascio dell''AP',N'Aggiornamento AP includendo i ":", ed installazione presso tutti i clienti interessati.
Identificare un metodo per il controllo dei dati rilasciati al LIS',NULL,N'Impatto positivo, l''introduzione di modalità per il controllo degli AP nella stringa "invio a LIS", garantirebbe la messa in uso di AP corretti anche per quanto riguarda l''interfacciamento con il LIS.
Impatto positivo, l''introduzione di modalità per il controllo degli AP nella stringa "invio a LIS", garantirebbe la messa in uso di AP corretti anche per quanto riguarda l''interfacciamento con il LIS.
Impatto positivo, l''introduzione di modalità per il controllo degli AP nella stringai"invio a LIS", garantirebbe la messa in uso di AP corretti anche per quanto riguarda l''interfacciamento con il LIS.
Impatto positivo, l''introduzione di modalità per il controllo degli AP nella stringai"invio a LIS", garantirebbe la messa in uso di AP corretti anche per quanto riguarda l''interfacciamento con il LIS.
Valutare quali modalità introdurre per il controllo degli AP nella stringa "invio al LIS". In termini di impatto sul carico di lavoro, sarebbe necessario del tempo per effettuare questo tipo di controllo.
',N'26/03/2020
26/03/2020
10/04/2020
10/04/2020
26/03/2020
26/03/2020
26/03/2020
26/03/2020',N'Identificazione clienti interessati ed installazione del nuovo AP di RTS120ING.
stesura e rilascio TAB
messa in uso AP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Valutazione delle modalità di controllo dei dati rilasciati al LIS
Correzione AP RTS120ING
Valutazione delle modalità di controllo dei dati rilasciati al LIS',N'mod0430_int12_versione_sw_1.3_01 - rac20-4_messa_in_uso_mod0430_int12_versione_sw_1.3_rac20-4.msg
TAB P24 Rev. AD, MAIL INVIATA IL 7/7/2020 - rac20-17_mail7-7-2020_elite_ingenius-tab_p24-rev_ad-_product_mdr_mtb_elite_mgb_notice.pdf
la modifica non è significativa, pertanto non è da notificare. - rac20-4_mod05,36__non_significativa.pdf
messa in uso AP RTS120ING - rac20-4_ifu_rts120ing_ap_rts120ing.msg
attivita_spostata_sulla_rac21-6 - rac20-4_attivita_spostata_sulla_rac21-6_1.txt
attivita_spostata_sulla_rac21-6 - rac20-4_attivita_spostata_sulla_rac21-6.txt',NULL,N'26/03/2020
26/03/2020
10/04/2020
26/03/2020
26/03/2020
26/03/2020',N'06/03/2020
31/12/2020',N'07/07/2020
09/03/2020
14/02/2020
08/04/2021
26/03/2020
08/04/2021',N'No',N'nessun impatto',N'No',N'invio TAB',N'No',N'nessun impatto',N'é necessario il rilascio di AP correttamente letti dal LIS.',N'IFU - Gestione Manuali',N'IFU2 - riesame manuale',N'è in corso l''aggiornamento del processo gestione IFU e AP suddividendolo in 2 processi differenti. I risk assessment interessati saranno AP1 "Stesura e verifica AP" e AP3 "Gestione Modifiche AP".',N'No',N'vedere MOD05,03',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,N'Valutazione efficacia in 2 fasi: per la correzione apportata all''AP verificare l''assenza di reclami con uguale causa primaria. Per la modifica di processo sul controllo dei dati rilasciati al LIS verificare l''implementazione del processo.

27/01/2021: valutazione efficacia negativa: la correzione apportata all''AP non è stata sufficiente ad eliminare la causa primaria, come emerso dal reclamo R20-72 e della NC20-84. L''eliminazione della causa primaria prevede una correzione sul modello 12 riportata nella RAC21-6 e la valutazione delle modalità di controllo dei dati rilasciati al LIS, anch''essa riportata nella RAC21-6. Si ritiene pertanto possibile chiudere questa azione correttiva.',N'Roberta  Paviolo',N'Negativo',N'vedere RAC21-6',NULL,NULL,'2020-12-31 00:00:00',NULL,'2021-04-08 00:00:00'),
    (N'21-12',N'Correttiva','2021-04-14 00:00:00',N'Roberta  Paviolo',N'Ricerca e sviluppo',N'Non conformità',N'21-35',N'RTS120ING_MDR-MTB',N'Nuova revisione di software associati ad IVD',N'Baco introdotto da PSS a causa della risoluzione di un precedente baco sul saggio. Mancanza di analisi retrospettiva.',N'Richiedere a PSS la correzione affinché il sistema possa stampare il report del controllo positivo e di visualizzare i risultati della temperatura di Melting dalla schermata  di controllo durante l''utilizzo del protocollo di analisi MDR-MTB. ',NULL,N'La nuova sw patch che verrà prodotta da PSS, sia per risolvere la problematica legata al "diluition factor" dei prodotti viral load HIV e HCV (CC21-26) sia per permettere la stampa del report del controllo positivo e la visualizzazione dei risultati della temperatura di Melting dalla schermata di controllo durante l''utilizzo del protocollo di analisi MDR-MTB, contiene il fixing del problema. Per motivi di tracciabilità delle modifiche del sw si è deciso di installare la sw patch insieme allo script ad hoc per i prodotti viral load che modifica il volume da 1000 a 600 il volume di campione processato con le cartucce SP 1000.  Quindi la patch non potrà essere installata presso i clienti che utilizzano il prodotti MDR/MTB in quanto potrebbe compromettere il possibile uso dei prodotti EGSpA in associazione alle cartucce SP 1000. 
La nuova sw patch dovrà essere inclusa da PSS nella nuova versione del sw per risolvere la problematica legata al prodotto MDR/MTB.
La nuova sw patch che verrà prodotta da PSS, ',N'21/04/2021
21/04/2021
21/04/2021
21/04/2021
21/04/2021
21/04/2021',N'nessuna attività a carico: la nuova sw patch NON potrà essere installa presso i clienti che utilizzano il prodotto MDR/MTB.
la nuova sw patch NON potrà essere installa presso i clienti che utilizzano il prodotto MDR/MTB, verrà comunque verificata secondo il piano di azioni del CC21-26
gestire con PSS l''integrazione della nuova sw patch nella nuova versione del sw Ingenius (inserire il riferimento alla RAC che gestirà la modifica)
la nuova sw patch NON potrà essere installa presso i clienti che utilizzano il prodotto MDR/MTB, verrà comunque verificata secondo il piano di azioni del CC21-26
la nuova sw patch NON potrà essere installa presso i clienti che utilizzano il prodotto MDR/MTB, verrà comunque verificata secondo il piano di azioni del CC21-26
nessuna attività a carico: la nuova sw patch NON potrà essere installa presso i clienti che utilizzano il prodotto MDR/MTB.',NULL,NULL,N'21/04/2021
21/04/2021
21/04/2021
21/04/2021
21/04/2021
21/04/2021',N'31/12/2021',NULL,N'No',N'nessun impatto',N'No',N'non necessaria perchè la nuova sw patch non potrà essere installata',N'No',N'nessun impatto',N'La nuova sw patch fissa la modifica, ma non è risolutiva perché viene installata in associazione ad uno script necessario per il funzionamento dei prodotti viral load (gestiti attraverso il CC21-26) in contrapposizione con l''utilizzo dei prodotti in associazione al SP1000.',N'SVM1 - Virtual Manufacturer: ricerca e sviluppo strumento ',N'SVM1.3 - Ricerca e sviluppo progettazione software , presso Virtual Manufacturer',N'da valutare',N'No',N'na, perchè la modifica non verrà installata presso i clienti che riscontrano la problematica. la significatività verrà valutata con la RAC che gestirà la nuova versione del sw ingenius che dovrà includere anche questa patch, funzionante per i prodotti utilizzati in associazione a SP1000 e in particolare poer il prodotto RTS120ING.',N'No',N'nessun virtual manufacturer',N'Maria Lucia Manconi',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-12-31 00:00:00',NULL,NULL),
    (N'17-23',N'Preventiva','2017-09-18 00:00:00',N'Roberta  Paviolo',N'Assistenza strumenti esterni',NULL,NULL,NULL,N'Progetto',N'Verificare se è necessario mantenere sotto controllo la temperatura dello strumento InGenius nel caso subisca deviazioni nel corso del tempo.',N'In corso uno studio atto a verificare la deviazione della temperatura sugli strumenti InGenius installati in Italia.',NULL,N'Stesura di un piano di progetto, raccolta dati sugli strumenti installati in Italia (verifica della temperatura dello strumento Ingenius),
analisi dei dati ottenuti, stesura del report con le conclusioni. In base ai risultati ottenuti eventuale aggiornamento della documentazione e formazione del personale interessato.',N'29/09/2017',N'Stesura di un piano di progetto, raccolta dati sugli strumenti installati in Italia (verifica della temperatura dello strumento Ingenius),
analisi dei dati ottenuti, stesura del report con le conclusioni. In base ai risultati ottenuti eventuale aggiornamento della documentazione e formazione del personale interessato.',N'Mail_Individuazione periodo di ricalibrazione della temperatura del PCR block Ingenius - rap17-23_progetto-mail_individuazione_del_periodo_di_ricalibrazione_della_temperatura_del_pcrblok_ingenius.pdf
progetto ptt - rap17-23_progetto_ingenius-010-p_18016_a_elite_ingenius_pcr_block_temperature_evaluation.pptx
I dati del primo anno (2019) sono stati raccolti su 29 strumenti in Italia e 6 strumenti installati nei Laboratori di ricerca EGSpA. La valutazione dell''intervallo di intervento (annuale/biennale etc) è in fase di studio sulla base dei dati raccolti. Solo uno strumento è risultato da ricalibrare, la causa primaria non è ancora stata determinata (caso isolato o caso causato da fattori esterni che non riguardano la stabilita del blocco PCR). È in valutazione la possibilità di estendere le prove per un ulteriore anno, al fine di per avere i dati su un intervallo biennale e non solo annuale. - rac18-13_statoa01-2020_1.msg
A 07/2020, a seguito di discussione con R&D, si decide di continuare i check della temperatura per tutto il 2020, in quanto i dati raccolti non sono sufficienti. - rap17-23_stato_a_07-2020.msg
03/2021 la raccolta dati su 4 anni ha dimostrato che: a partire dal 4°anno ci sono strumenti che iniziano a essere non calibrati: in media ogni anno la temperatura si scosta di 0,08°C, portando il blocco PCR fuori calibrazione dopo 4 anni. La scelta di impostare una frequenza di manutenzione programmata sul blocco PCR include una serie di attività e di decisioni da prendere:  - valutare di ampliare il limite di accettazione del risultato: RDM conferma che da +-0,3 si può passare a +-0,4; - valutare come gestire i termometri ed il tool di calibrazione che fornisce PSS e che sono particolarmente costosi;  - valutare l''impatto della calibrazione annuale del tool presso PSS;  - PSS deve modificare lo script di calibrazione al fine di ridurre i tempi di eseciuzione della ri-calibrazione del blocco pcr;  - valutare come gestire i tempi che gli FSE impiegano per effettuare la manutenzione programmata in quanto, effettuando la calibrazione del blocco pcr, aumenteranno;  - in Italia il controllo sugli strumenti con + di 4 anni è già partito.   Per tutte queste attività la RAP rimarrà aperta sicuramente ancora un anno. - rap17-23_statoa03-2021.txt',N'GATL',N'29/09/2017',N'31/12/2021',NULL,N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto',N'ST - Elenco famiglie Strumenti',N'ST37 - Elite InGenius (STRxx)',N'STR37',N'No',N'non necessaria',N'No',N'non necessaria',N'Marcello Pedrazzini',N'Si',N'da valutare in base ai dati che si otterranno','2019-12-31 00:00:00',N'la chiusura del porgetto è prevista per fine 2019.',NULL,NULL,NULL,NULL,NULL,'2021-12-31 00:00:00',NULL,NULL),
    (N'21-14',N'Correttiva','2021-05-07 00:00:00',N'Maria Galluzzo',N'Validazioni',N'Reclamo',N'21-32',N'HHV7 ELITe MGB ',N'Assay Protocol',N'Nell''attuale versione (01) degli AP, in uso da fine 2018, è stato inserito un Ct cut-off = 35 per il target negli AP Plasma e Whole Blood di HHV7 perchè studi clinici hanno dimostrato che una larga fetta della popolazione ha HHV7.  
Il cut off è stato inserito erroneamente anche nell''AP per la matrice generica, unico AP utilizzabile con matrici che non hanno una validazione sulla piattaforma InGenius.',N'Eliminazione del cut off per il target HHV7 nell''AP generico HHV7 ELITe_OPEN_200_100_01 e verifica che nella "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" sia presente la modalità di gestione dell''impostazione dei cut-off nel caso di creazione di un AP generico.',NULL,N'Nessun impatto a cambiamento avvenuto. Impatto in termini di attività
Nessun impatto a cambiamento avvenuto. Impatto in termini di attività
Impatto in termini di attività
Impatto positivo per i clienti poichè possono utilizzare l''AP generico rilevando la presenza anche di minime quantità di target su matrice generica
Impatto positivo per i clienti poichè possono utilizzare l''AP generico rilevando la presenza anche di minime quantità di target su matrice generica
La modifica ha impatto nell''uso OPEN-off label del prodotto, non ha nessun impatto sull''utilizzo CE-IVD.',N'11/05/2021
11/05/2021
10/05/2021
13/05/2021
13/05/2021
10/05/2021',N'Modifica dell''AP generico HHV7 ELITe_OPEN_200_100_01 con eliminazione del valore di cut off sul target HHV7.
Verifica che nella "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" sia presente la modalità di gestione dell''impostazione dei cut-off nel caso di creazione di un AP generico.
Messa in uso dell''Assay Protocol aggiornato a seguito delle modifiche apportate.
Aggiornamento nuova revisione AP presso i clienti
Rilascio TAB P27 - Rev AB 
verifica dell''AP aggiornato',N'HHV7 revisione (rev 01) degli AP OPEN e degli AP OPEN QL  - rac_21-14_messa_in_uso_ap_open_prodotti_egspa.msg
HHV7 revisione (rev 01) degli AP OPEN e degli AP OPEN QL - rac_21-14_messa_in_uso_ap_open_prodotti_egspa_1.msg
la modalità di gestione dell''impostazione dei cut-off nel caso di creazione di un AP generico è presente nella "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius"  - rac21-14_verifica_effettuata.txt',NULL,N'10/05/2021
10/05/2021
10/05/2021
13/05/2021
13/05/2021
10/05/2021',NULL,N'10/05/2021
10/05/2021
10/05/2021
10/05/2021',N'No',N'nessun impatto sul DMRI',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'Nessun impatto sull''utilizzo CE-IVD del prodotto.',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',N'da valutare a fine anno',N'No',N'la modifica degli AP non è da notificare',N'No',N'nessun virtual manufacurer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'21-13',N'Correttiva','2021-05-07 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'21-33',N'RTK403ING HSV1&2',N'Assay Protocol',N'L''AP generico "HSV ELITe_Open_200_100_00" è stato creato sul modello dell''AP generico "HSV ELITe_Open_200_50_00  che rispecchia le impostazioni dell''AP validato del prodotto RTK403ING per lesioni muco cutanee, "HSV ELITe_MCS_200_50_00", con protocollo 200 in 50, controllo interno IC2 di Bothell e cut-off specifico per protocollo IVD. 
Nella creazione dell''AP generico 200_100 non si è tenuto in considerazione che la diluizione 200_100  può portare ad un fallimento del cut-off per l''IC che era stato calcolato sulla diluizione 200-50.  L''errore è imputabile ad una caratterizzazione parziale trattandosi di un AP generico per uso off-label.',N'Modifica del valore di IC cut-off dell''AP HSV ELITe_Open_200_100 basandosi su analisi dati del cliente. Prevedere nella PR05,08 un paragrafo per i prodotti validati su ingenius e poi customizzati da EGSpA per il cliente su matrici non validate, nel quale si può prevedere un test di compatibilità aggiuntivo.',NULL,N'La modifica ha impatto nell''uso OPEN-off label del prodotto, non ha nessun impatto sull''utilizzo CE-IVD. Il cutoff è reso necessario dal fatto che questo protocollo Open eluisce nel doppio del volume di quello IVD e quindi l''Internal Control necessità di criteri più permissivi che sono stati studiati sulla base dei risultati ottenuti dal cliente.
L''introduzione in procedura di una nuova categoria di AP (AP EGSpA customizzati) gestiti attraverso specifiche informazioni scritte sulle richieste del cliente, quali volumi e cut-off, prevede un aumento dei tempi di rilascio.
La modifica ha impatto nell''uso OPEN-off label del prodotto, non ha nessun impatto sull''utilizzo CE-IVD. Il cutoff è reso necessario dal fatto che questo protocollo Open eluisce nel doppio del volume di quello IVD e quindi l''Internal Control necessità di criteri più permissivi che sono stati studiati sulla base dei risultati ottenuti dal cliente.
L''introduzione in procedura di una nuova categoria di AP (AP EGSpA customizzati) gestiti ',N'10/05/2021
10/05/2021
10/05/2021
10/05/2021
10/05/2021
10/05/2021
10/05/2021
10/05/2021
10/05/2021',N'Modifica del valore di IC cut-off dell''AP HSV ELITe_Open_200_100 basandosi su analisi dati del cliente. Riportare i risultati ottenuti dall''analisi allegandoli alla presente RAC nello specifico campo sottostante.
verifica ed approvazione PR05,08
 fornire a RAS un elenco di tutti gli AP EGSpA customizzati 
Messa in uso dell''Assay Protocol aggiornato a seguito delle modifiche apportate.
Prevedere nella PR05,08 un paragrafo per i prodotti validati su ingenius e poi customizzati da EGSpA per il cliente su matrici non validate, nel quale si può prevedere un test di compatibilità aggiuntivo; gli AP OPEN devono essere divisi in 2 categorie: 
-	OPEN, ossia AP che sono la copia esatta dell''AP validato, 
-	OPEN CUSTOMIZZATO, ossia AP creati sulla base delle richieste del cliente. In questo caso il supporto deve fornire informazioni dettagliate su quello che vuole il cliente, quali parametri modificar, in particolare volumi e cut-off. A questa nuova categoria faranno parte sia i prodotti di cui siamo legal m',N'HSV 1&2 revisione (rev 01) degli AP OPEN - rac21-13_messa_in_uso_ap_open_prodotti_egspa.msg
HSV 1&2 revisione (rev 01) degli AP OPEN - rac21-13_messa_in_uso_ap_open_prodotti_egspa_1.msg
HSV 1&2 revisione (rev 01) degli AP OPEN - rac21-13_messa_in_uso_ap_open_prodotti_egspa_2.msg',NULL,N'10/05/2021
10/05/2021
13/05/2021
13/05/2021',NULL,N'10/05/2021
10/05/2021
10/05/2021',N'No',N'l''aggiornamento dell''AP non ha impatto sul DMRI',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'Nessun impatto sull''utilizzo CE_IVD del prodotto. ',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',NULL,N'No',N'l''aggiornamento degli AP non è notificato alle autorità competenti',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'21-8',N'Preventiva','2021-02-11 00:00:00',N'Gabriella Campanale',N'Ricerca e sviluppo',NULL,NULL,NULL,N'Manuale di istruzioni per l''uso',N'Si suppone che l''installazione della nuova versione del software (versione 16) sullo strumento ELITe InGenius abbia causato l''insorgere di questa problematica.',N'Sul manuale di istruzioni per l''uso del prodotto RTS170ING verrà modificata la tabella relativa ai volumi di preparazione delle miscele di reazione adeguandola ai nuovi volumi.',NULL,N'Nessun impatto sul prodotto e sul cliente.
Il prodotto non subisce alcuna modifica. La modifica dell''IFU previene eventuali reclami.
Nessun impatto sul prodotto che non subisce alcuna modifica.
Nessun impatto.',N'11/02/2021
11/02/2021
11/02/2021
11/02/2021',N'comunicazione della modifica attraverso avvertenza allegata all''IFU
Messa in uso del manuale
Aggiornamento del manuale di istruzioni per l''uso
revisione manuale di istruzioni per l''uso',NULL,NULL,N'11/02/2021
11/02/2021
11/02/2021',NULL,NULL,N'No',N'nessun impatto',N'Si',N'attraverso avvertenza allegata all''IFU',N'No',N'nessun impatto',N'Il prodotto non subisce alcuna modifica.',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'vedere MOD05,36. La modifica all''IFU non introduce controindicazioni o avvertenze con impatto sulla sicurezza ed efficacia del prodotto.',N'No',N'nessun virtual manufacturer',N'Gabriella Campanale',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'21-5',N'Correttiva','2021-02-01 00:00:00',N'Maria Galluzzo',N'Validazioni',N'Reclamo',N'21-1',N'AP CMV ELITe_WB_Rev.05',N'Documentazione SGQ
Assay Protocol',N'La causa primaria è imputabile ad una modifica introdotta da PSS nella versione 3.0.2 nell''output delle frasi del modello interpretativo 2 utilizzato per creare la revisione 05 dell''Assay Protocol di CMV_WB, indipendentemente dalla versione del SW e dell''APT; ad oggi non abbiamo ancora mai fornito a PSS specifiche precise in merito all'' output delle frasi dei modelli interpretativi.

Per risolvere nell''immediato la problematica del cliente,  la software house Abbott ha subito aggiornato la regola dell'' AP CMV_WB Rev.05  inserendo una transcodifica sul driver in modo da eliminare le ultime 5 cifre (IU/ml) anziché le ultime 6 (IU/ml.). Questa modifica è stata fatta solamente per l''AP CMV_WB poichè da un''analisi preliminare del DB risulta che tutti gli altri AP quantitativi presenti hanno ancora il punto finale dopo l''unità di misura.',N'L''azione ha dunque lo scopo di DEFINIRE e CONDIVIDERE con PSS le specifiche per TUTTI i MODELLI INTERPRETATIVI in modo che siano standardizzate per l''interfacciamento ed applicate da PSS negli upgrade dei SW e dei modelli interpretativi senza che vengano introdotte modifiche non richieste. 
In questa azione correttiva rientra anche la soluzione definitiva al Reclamo 20-72.

Per ottenere una standardizzazione delle frasi è dunque necessario:
-	Integrare la "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" con un allegato contenente una parte inerente ai criteri che definiscono la "sintassi" della stringa del risultato (regole della sintassi degli output dei vari modelli) ed una parte in cui sono riportate le frasi definite in base ai criteri e che sono da inserire negli AP.
-	Condividere con PSS un documento con le stringhe definite in modo standardizzato (in tutte le lingue della GUI)
-	Aggiornare il MOD05,35 con le stringhe definite in base ai criteri sopra citati poichè impattano sull''interfaccia con ELITeWare.
-      Aggiornare i MOD04,30 con la versione di software APT (SW35) utilizzati per la creazione/aggiornamento dell''AP.
-	Sviluppare e validare un sistema interno di verifica dell''interfacciamento degli assay protocol prima del loro rilascio, tramite prova di connessione ad ELITeWare finalizzato al miglioramento della sicurezza e della qualità del processo di interfacciamento.

Nello specifico, per l''AP di CMV WB rev.05 è necessario decidere se modificarlo inserendo nuovamente il punto dopo l''unità di misura o se mantenere l''attuale output in virtù di una strategia comune per tutti gli AP previa verifica di quali AP presentano il punto e quali no. ',NULL,N'Impatto positivo: l''introduzione di specifiche per la definizione della stringa degli AP garantirebbe la messa in uso di AP corretti per quanto riguarda l''interfacciamento con il LIS. Non ci sono impatti diretti o indiretti sulle performance del prodotto.
Impatto positivo: l''introduzione di specifiche per la definizione della stringa degli AP garantirebbe la messa in uso di AP corretti per quanto riguarda l''interfacciamento con il LIS. Non ci sono impatti diretti o indiretti sulle performance del prodotto.
Impatto positivo: l''introduzione di specifiche per la definizione della stringa degli AP garantirebbe la messa in uso di AP corretti per quanto riguarda l''interfacciamento con il LIS. Non ci sono impatti diretti o indiretti sulle performance del prodotto.
Impatto positivo: l''introduzione di specifiche per la definizione della stringa degli AP garantirebbe la messa in uso di AP corretti per quanto riguarda l''interfacciamento con il LIS. Non ci sono impatti diretti o indiretti sulle performance del prod',N'09/03/2021
09/03/2021
09/03/2021
09/03/2021
09/03/2021
09/03/2021
09/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021',N'In collaborazione con R&D integrare la "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius" con un allegato contenente una parte inerente ai criteri che definiscono la "sintassi" della stringa del risultato (regole della sintassi degli output dei vari modelli) ed una parte in cui sono riportate le frasi definite in base ai criteri e che sono da inserire negli AP.
In collaborazione con Assistenza applicativa aggiornare il MOD05,35 con le frasi definite in base ai criteri specificati nell''integrazione della "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGenius".
Eliminare
In collaborazione con R&D implementazione nel MOD04,30 "MODULO DI CONTROLLO ASSAY PROTOCOL" di tutti i modelli di un campo per registrare la versione del Software dell''Assay Parameter Tool utilizzate nella creazione degli assay protocol
Revisionare aggiornamento RA AP1
In collaborazione con Validazioni integrare la "IO0408_01_Istruzione operativa per la creazione di Assay Protocol InGeniu',NULL,NULL,N'09/03/2021
09/03/2021
09/03/2021
09/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021
11/03/2021',NULL,NULL,N'No',N'nessun impatto sul DMRI di prodotto',N'No',N'non necessaria',N'No',N'nessun impatto',N'A livello regolatorio la rac elimina la causa primaria di errori derivanti dall''interfacciamento con il LIS attraverso la definizione dei requisiti dei modelli interpretativi da fornire a PSS e permette di standardizzare i modelli interpretativi per l''interfacciamento ai LIS e l''applicazione degli stessi negli aggiornamenti del SW dello strumento ELITe InGenius. 
Un processo di controllo sui dati rilasciati al LIS attraverso ElitWare riduce gli errori di interfacciamento, ma non agisce sugli interfacciamenti ai diversi LIS non supportati da EliteWare.',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'aggiornare il risk assessment con l''introduzione del sistema interno di verifica dell''interfacciamento degli assay protocol prima del loro rilascio tramite prova di connessione ad ELITeWare ',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Alessia Cotza',N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'21-3',N'Preventiva','2021-01-27 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Nei primi risk assessment svolti nel 2015 la probabilità era stata valutata su base teorica e non  su base statistica (media aritmetica del numero di eventi su più anni: NC o R) . 
I risk assessment erano stati sviluppati individuando il caso peggiore: alla luce dei dati raccolti nel corso degli anni è emerso che la maggior parte degli eventi (NC/R) non sono da associarsi al caso peggiore ma a casi con severità inferiore.  
Nel 2019 era emerso l''aggiornamento di 120 RA, confermati nel 2020, per allineare la probabilità alla statistica. Nessuno dei RA impattati anche sulla matrice di gestione dei rischi ha dato seguito a processi non sotto controllo, ma i RA non sono stati aggiornati per la gravosità delle attività in termini di ore/lavoro.',N'Gestire i RA a livello complessivo all''interno di un unico file Excel. laddove ritenuto necessario suddividere i RA in 3 gradi di severità, associare correttamente le NC in base grado di severità in modo da avere dei profili di rischio più accurati.',NULL,N'Impatto positivo: la gestione dei RA a livello complessivo all''interno di un unico file Excel permette di avere sempre una visione complessiva del processo di CQ. La suddivisione di ciascun RA in 3 gradi di severità permette di associare correttamente le NC in base grado di severità: si moltiplicano i profili di rischio, ma sono più accurati.
Questo metodo di gestione dei profili di rischio associati ai sotto-processi per grado di severità, se funzionale, potrà essere applicato ad altri processi la cui segmentazione della severità è necessaria per le NC non associabili al caso peggiore, per esempio nel caso della dispensazione.
La gestione dei RA per ciascun processo a livello complessivo all''interno di un unico file Excel ha un impatto positivo: creati i nuovi modelli di RA  ogni anno si effettuerà un''analisi complessiva del rischio associato a ciascun sotto-processo su un unico documento di RA, con una migliore visione complessiva del processo. 
Invece di aggiornare 191 RA sarà sufficiente aggi',NULL,N'Creare il modello pilota di RA complessivo.
Associare le NC/R ai corretti gradi di severità, quindi associare le RAC/RAP atte a mitigare o eliminare il rischio di errore.',NULL,N'QC',NULL,NULL,NULL,N'No',N'nessun impatto',N'No',N'non necessaria, si tratta di un processo interno',N'No',N'na',N'i requisiti regolatori sono rispettati. il processo è reso lean.',N'ALTRO - Altro',N'ALTRO - ALTRO',N'creazione di un unico modello di RA complessivo per tutti i sotto-processi di ciascun processo',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Roberta  Paviolo',N'Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'20-47',N'Preventiva','2020-12-18 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',NULL,NULL,NULL,N'Assay Protocol',N'L''errore compare a seguito del mancato Ct della fluorescenza per il CI in associazione a un campione positivo. Tale errore emerge a seguito delle modifiche apportate al sw InGenius V 1.3.0.15 in associazione all''AP di RTS170ING in fase di CQ, dove la quantità di CI è molto inferiore a quella di un campione reale. Infatti in base ai dati di Validazione ed R&D il controllo è sempre presente anche con quantità molto elevate di target e pertanto tale problema non dovrebbe comparire presso i clienti. Benché la probabilità sia molto bassa esiste la possibilità che tale errore compaia anche presso gli utilizzatori, richiedendo l''approvazione manuale del campione.',N'Si chiede di modificare l''amp Criteria dell''AP di RTS170ING portandolo a 2.',NULL,N'La modifica non impatta dal punto di vista delle performance né sul campo, né sui criteri di CQ, ma risolve la segnalazione di un errore che, presso il cliente, ha una bassa probabilità di verificarsi. Si ritiene pertanto non necessario effettuare l''aggiornamento degli AP installati presso i clienti con tempistiche stringenti, ma eventualmente risolvere in modo puntuale l''emergere di questa problematica.
La modifica è positiva in quanto elimina la necessità di approvare manualmente i campioni e di segnalare l''errore sui documenti
La modifica è positiva in quanto riduce la possibilità benchè minima di segnalazioni da parte dei clienti a causa dell''errore 30108.
La comparsa di questo tipo di errore deve poter essere valutato nei nuovi progetti
La modifica è positiva in quanto riduce la possibilità benchè minima di segnalazioni da parte dei clienti a causa dell''errore 30108.
La modifica è positiva in quanto riduce la possibilità benchè minima di segnalazioni da parte dei clienti a causa de',N'19/01/2021
18/12/2020
19/01/2021
19/01/2021
19/01/2021
19/01/2021',N'Installare l''AP presso il cliente in associazione ad altre modifiche (esempio, l''estensione della calibrazione da 30 a 60 giorni (CC20-69).
installazione sugli strumenti CQ della nuova versione degli AP
Aggiornamento degli AP per i clienti e per il CQ
Messa in uso nuovi AP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica  
verifica degli AP per i clienti e per il CQ',N'Insieme all''estensione d''uso del prodotto RTS170ING su saliva, è stato aggiornato anche il valore del AMP (da 1 a 2) - rac20-47_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137.msg
Insieme all''estensione d''uso del prodotto RTS170ING su saliva, è stato aggiornato anche il valore del AMP (da 1 a 2) - rac20-47_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137_2.msg
Insieme all''estensione d''uso del prodotto RTS170ING su saliva, è stato aggiornato anche il valore del AMP (da 1 a 2) - rac20-47_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137_1.msg
Le modifiche agli AP non vengono notificate - rac20-47_mod05,36_01_non_significativa.pdf',NULL,N'19/01/2021
18/12/2020
19/01/2021
19/01/2021
19/01/2021
19/01/2021',NULL,N'19/01/2021
18/02/2021
18/02/2021
18/02/2021
18/12/2020
18/02/2021',N'No',N'nessun impatto',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'-',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto su RA  R&D1.9',N'No',N'vedere MOD05,36, la modifica degli AP non necessita notifica.',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2021-08-31 00:00:00',N'valutare l''assenza di reclami relativi  ad un errore 30108 Ct error',NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-18 00:00:00',NULL),
    (N'20-40',N'Preventiva','2020-10-30 00:00:00',N'Stefania Brun',N'Produzione',N'Reclamo',N'20-61',N'STD070PLD',N'Documentazione SGQ',N'Il cliente ha inserito i tubini direttamente sullo strumento InGenius per cui non è possibile che abbia scambiato o preparato in modo errato i punti della curva; ci ha inoltre fornito le immagini relative alle etichette e ai volumi del secondo set di provette: le etichette sono corrette nonché il livello del volume di soluzione all''interno del tubino di PVB19 Q - PCR Standard 103 (tappo verde) che esclude che la soluzione si possa essere concentrata in seguito ad evaporazione. 
Un''analisi del processo produttivo ha messo in evidenza che la dispensazione non viene completamente eseguita con il dispensatore automatico Hamilton Nimbus poiché l''avanzo di liquido nel fondo dei tubi di aspirazione viene dispensato con un dispensatore manuale che non sempre viene registrato sul modulo di produzione del prodotto; in questo caso, dei 232 tubini registrati, 8 sono stati dispensati manualmente ma non è stato registrato il dispensatore utilizzato (DPE o MP). La dispensazione risulta non omogenea e non è possibile tracciare gli 8 tubini dispensati manualmente dall''operatore che potrebbero essere stati inavvertitamente preparati con l''avanzo di liquido del fondo dei tubi di aspirazione del semilavorato PVB19 Q - PCR Standard 104.
E'' necessario definire una misura preventiva che indichi le modalità di recupero dei residui della dispensazione automatica e che abbassi il rischio che possano essere scambiati i fondi dei tubi di aspirazione di semilavorati diversi dello stesso prodotto. 
La causa primaria di questa RAC è la fase di etichettatura dei tubi aggiuntivi e non quella di dispensazione.
Per ulteriori dettagli si riporta al "Report Indagine_R20-61_STD070PLD_ING_Chile_MicroBAC" allegato.',N'Tracciare la modalità di recupero del volume residuo al termine della dispensazione in modalità automatica per STD e CTR come già previsto per la dispensazione delle MIX da SPEC-USO-DA9 rev.02 e specificare nella IO09,02 di dispensazione, che si possono dispensare con una micropipetta i volumi residui solo se si sono già preparati ed etichettati precedentemente i contenitori finali, mentre ogni ulteriore residuo deve essere eliminato.',NULL,N'Positivo in quanto garantisce la tracciabilità delle operazione svolte
Positivo in quanto garantisce la tracciabilità delle operazione svolte
Positivo in quanto garantisce la tracciabilità delle operazione svolte
Positivo in quanto garantisce la tracciabilità delle operazione svolte
Positivo in quanto garantisce la tracciabilità delle operazione svolte
nessun impatto se non in termini di attività.',N'01/12/2020
01/12/2020
01/12/2020
30/10/2020
30/10/2020',N'messa in uso documenti
aggiornare il vmp con l''aggiornamento del RA P3.15
Uniformare la specifica d''uso dei Nimbus con le stesse indicazioni contenute nella Specifica d''Uso degli STARlet. 
Formazione del personale produzione
Specificare nella IO09,02 di dispensazione, che si possono dispensare con una micropipetta i volumi residui solo se si sono già preparati ed etichettati precedentemente i contenitori finali. Ogni ulteriore residuo deve essere eliminato. Formazione al personale controlli positivi.',N'spec-uso-da7_da8_rev01 - rac20-40_messa_in_uso_spec-uso-da7__da8_rev01.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-40_mod05,36_01_non_significativa.pdf
spec-uso-da7__da8_rev01 - rac20-40_messa_in_uso_spec-uso-da7__da8_rev01_1.msg
non si ritiene necessario aggiornare la IO09,02, è sufficiente inserire l''indicazione nella specifica d''uso del dispensatore automatico - rac20-40_non_si_ritiene_necessario_aggiornare_la_io09,02_sufficiente_la_specifica_d_uso.txt',NULL,N'30/10/2020
27/11/2020
27/11/2020',NULL,N'01/02/2021
01/02/2021
01/02/2021
01/02/2021',N'No',N'no in quanto la nuova revisione della specifica non impatta sul DMRI di CTR e STD',N'No',N'non applicabile',N'No',N'non applicabile',N'non applicabile',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.3 - Identificazione materiali, semilavorati e consumabili',N'il sottoprocesso P1.3 non è da aggiornare ',N'No',N'vedereMOD05,36. il controllo aggiuntivo non modifica il processo produttivo, ma permette la tracciabilità dei tubi dispensati  con l''avanzo di volume del dispensatore automatico utilizzando le micropipette.',N'No',N'non applicabile ',NULL,N'Si',NULL,'2021-08-31 00:00:00',N'Valutare assenza reclami con uguale causa primaria',NULL,NULL,NULL,NULL,NULL,NULL,'2021-02-01 00:00:00',NULL),
    (N'20-33',N'Correttiva','2020-10-08 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'20-71',N'RTS140ING Bordetella Elite MGB kit',N'Assay Protocol',N'Cambiando il valore dell''AMP criteria da 4 a 1 sui target BP (CH5) e BH (CH6) all''interno degli Assay Protocol, i campioni «BP POS 4» (Track 2) e «BP POS 5» (Track 3) mostrano una baseline normale ed il Ct viene determinato; tale modifica non impatta sul Ct degli altri campioni della sessione che presentavano una baseline normale per il target BP. 
La rianalisi eseguita sui campioni di Validazione del progetto Bordetella conferma che la modifica dell''Assay Protocol con l''AMP criteria dei canali 5 e 6 (BP e BH rispettivamente) da 4 a 1 non ha impatti.',N'Modifica del valore dell''AMP criteria da 4 a 1 sui target BP (CH5) e BH (CH6) all''interno degli Assay Protocol (AP) e rianalisi dei dati di Validazione per valutare assenza di impatti. ',NULL,N'Positivo perchè la modifica del parametro AMP è inerente ai target di tipizzazione ed un''eventuale debole e falsa positività sui canali 5 e 6 se non è associata alla positività del canale 1, non viene considerata.
Positivo perchè la modifica del parametro AMP è inerente ai target di tipizzazione ed un''eventuale debole e falsa positività sui canali 5 e 6 se non è associata alla positività del canale 1, non viene considerata.
Positivo poichè non altera il profilo di rischio del prodotto
Positivo poiché previene il reiterarsi di situazioni analoghe a quelle oggetto del reclamo. Negativo in termini di tempo e risorse per la necessità di una doppia installazione: l''attuale sw installato sugli strumenti InGenius necessita del AMP criteria 1 per risolvere il problema oggetto del reclamo, è pertanto necessario aggiornare gli AP con questa modifica; una seconda installazione in contemporanea con la nuova release del software InGenius (1.3.0.15), permetterà il calcolo del Ct delle curve con doppia ',N'23/10/2020
23/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020',N'Rianalisi dati di validazione e modifica degli AP (inclusi quelli cq).
Verifica degli AP per altri prodotti con AMP = 1 e pianificazione dell''aggiornamento entro la release del sw 1.3.0.15 per permettere all''assistenza applicativa un''installazione presso i clienti sia del nuovo sw che degli AP modificati.
Valutazione della significatività della modifica
- installazione del AP con AMP=1 nell''immediato (pianificare gli interventi)
- installazione dell''AP con AMP=2 insieme al nuovo SW 1.3.0.15
- Pianificazione dell''aggiornamento dell''AP modificato presso i clienti
- Stesura della TAB per la modifica degli AP
Messa in uso della versione dell''AP con l''AMP=1, funzionante con l''attuale versione del sw
Messa in uso della nuova versione dell''AP con l''AMP diverso da 1, funzionante con l''attuale versione del sw
Verifica AP aggiornati
verifica AP CQ',N'rianalisi dati di validazione - rac20-33_2020-14-10_r20-71_rac20-33.pptx
la modifica non è significativa, pertanto non è da notificare. - rac20-33_mod05,36__non_significativa.pdf
Messa in uso TAB "ELITe InGenius -TAB P42 - Rev AA -  Bordetella  ELITe MGB notice" - rac20-33_messa_in_uso_tab.msg
l''aggiornamento AP Bordetella con AMP=2 è obbligatorio contestualmente all''upgrade del software. - rac20-33_pianificazioneinstallazioniamp2clienti.msg
messa inuso AP funzionanti con versione sw InGenius 1.3.0.15 per i prodotti hev, sti plus e bordetella - rac20-33_messa_in_uso_ap_prodotti_egspa_per_sw_1.3.0.15_1.msg
aggiornati AP CQ di STI PLUS - rac20-33_qualita_ap_cq_sti_plus_rts400ing.msg
messa inuso AP funzionanti con versione sw InGenius 1.3.0.15 per i prodotti hev, sti plus e bordetella - rac20-33_messa_in_uso_ap_prodotti_egspa_per_sw_1.3.0.15.msg
UGUALE PROBLEMATICA HEV (130ING) E STI PLUS (400ING) - rac20-33_prodotti_con_uguale_problematica.msg',NULL,N'23/10/2020
23/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020',NULL,N'14/10/2020
27/10/2020
04/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020',N'No',N'nessun impatto ',N'No',N'viene prodotta una TAB',N'No',N'nessun impatto',N'La modifica non altera il profilo di rischio del prodotto',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'valutare l''impatto nell''analisi annuale',N'No',N'veder MOD05,36',N'No',N'nessun virtula manufacturer',NULL,N'No',NULL,'2021-07-31 00:00:00',N'Valutazione efficacia: assenza reclami imputabili al parametro AMP (attenzione: la formazione sulla versione 1.3.0.16 del sw Ingenius al personale di campo e l''obbligatorietà di installare gli AP Bordetella con AMP=2 contestualmente all''upgrade del software è di 01/2021).
',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'20-32',N'Correttiva','2020-09-18 00:00:00',N'Lucia  Liofante',N'Controllo qualità',N'Non conformità',N'20-66',N'964-RTK602ING',N'Progetto',N'Dall''indagine emerge che risulterebbe necessario caratterizzare ogni lotto ricevuto di campione di riferimento HBV Zeptometrix, al fine di stabilire la quantità di IU/mL di partenza e aggiornare di conseguenza le modalità di diluizione del campione a livello del MOD10,13-RTK602ING.
Da qui la scelta di utilizzare come campione di riferimento il materiale "PEI Reference Preparation HBV DNA", maggiormente affidabile in termini di quantificazione (IU/mL) e riproducibilità.',N'SCP2019-020/-021/-022. Sostituzione, a livello di tutti e tre i progetti, del materiale Zeptometrix, individuato inizialmente come campione di riferimento per l''esecuzione dei CQ su strumento InGenius, con materiale del fornitore PEI (Paul-Ehrlich-Institut), ovvero:
- PEI Reference Preparation HIV-1 RNA
- PEI Reference Preparation HCV RNA
- PEI Reference Preparation HBV DNA',NULL,N'Nessun impatto, escluse le azioni a carico
Impatto positivo: con il materiale PEI viene meno la necessità di eseguire la caratterizzazione prima dell''utilizzo come campione di riferimento in CQ e la conseguente necessità di aggiornare di volta in volta i moduli MOD10,13 (misure indispensabili invece per l''utilizzo del materiale Zeptometrix), alleggerendo quindi le procedure di CQ dei prodotti RTK600ING/RTK601ING/RTK602ING. Impatto negativo: il materiale di Zeptometrix era costituito da virus inattivato, mentre il materiale PEI è costituito da virus attivo, andando ad aumentare il rischio nella manipolazione del campione.
Impatto positivo: con il materiale PEI viene meno la necessità di eseguire la caratterizzazione prima dell''utilizzo come campione di riferimento in CQ e la conseguente necessità di aggiornare di volta in volta i moduli MOD10,13 (misure indispensabili invece per l''utilizzo del materiale Zeptometrix), alleggerendo quindi le procedure di CQ dei prodotti RTK600ING/RTK601ING/RTK602ING. Im',N'12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020',N'Assegnazione codici NAV per i tre nuovi materiali PEI (950-XXX)
Stesura di un modulo comune per ricostituzione e dispensazione del materiale liofilizzato PEI HIV/HCV/HBV (in revisione LP)
Adattamento dei moduli MOD10,13-RTK600ING/MOD10,13-RTK601ING/MOD10,13-RTK602ING in revisione LP per utilizzo campione PEI. I relativi MOD10,12, e quindi i criteri di accettazione, non sono sottoposti a modifica
Verificare che il personale sia adeguatamente addestrato alla manipolazione dei campioni biologici (in particolare i neoassunti). Avviare l''approvvigionamento di DPI specifici per il rischio biologico, in numero sufficiente per la frequenza di lavorazione. Valutazione dell''adeguatezza della strumentazione in uso ed eventuale integrazione.
Creazione specifiche reagenti (950-xxx) per PEI Reference Preparation HIV-1 RNA, PEI Reference Preparation HCV RNA, PEI Reference Preparation HBV DNA
Aggiornamento DMRI e DHRI relativi ai tre progetti con riferimenti a nuovo materiale PEI e a nuova documentazione collegata ',N'mod957-qc-pei_xxx_rev00 - rac20-32_messa_in_uso_mod957-qc-pei_xxx_rev00.msg
il primo modulo MOD10,13-RTK602ING che utilizza il PEI è la rev. LP4 del 10-09-2020. - rac20-32_mod10,13-rtk602ing_pei_lp4_10-09-2020.txt
il primo modulo MOD10,13-RTK602ING che utilizza il PEI è la rev. LP4 del 10-09-2020. - rac20-32_mod10,13-rtk602ing_pei_lp4_10-09-2020_1.txt
SPEC950-228 HBV REFERENCE, SPEC950-229 HCV REFERENCE, SPEC950-23 HIV-1 REFERENCE - rac20-32_messa_in_uso_950-xxx_pei_reference_1.docx
DMRI2020-003 DMRI2020-004 DMRI2020-005 DMRI2020-006 - rac20-32_dmriedhriaggiornati.txt
Aliquots can be stored for single use (no additional freeze-thawing) at-70°C or below for at least 6 month. Nevertheless trending (e. g. Ct values if using real-time NAT) has to be performed to monitor that no adverse effects happened - rac20-32_stability_of_pei_reference_preparation_hbvhcvhiv.msg
04/2021: Il segnale rosso luminoso esterno al laboratorio BSL2 CQ verrà installato con il prossimo progetto di ampliamento dei laboratori. - rac20-32_noinstallazionesegnale_luminoso.txt
nessun nuovo strumento installato - rac20-32_nessun_nuovo_strumento_installato.txt',NULL,N'12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020
12/10/2020',NULL,N'17/11/2020
20/11/2020
28/04/2021
28/04/2021
27/01/2021
28/04/2021
14/09/2020
28/04/2021
28/04/2021
28/04/2021',N'Si',N'aggiornare con le specifiche',N'No',N'NA, il prodotto non è ancora immesso in commercio',N'No',N'NA, il prodotto non è ancora immesso in commercio',N'Utilizzo di un campione di riferimento maggiormente affidabile in termini di quantificazione (IU/mL) e riproducibilità.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'valutare a fine anno gli eventi su R&D1.1',N'No',N'NA, il prodotto non è ancora immesso in commercio',N'No',N'nessun virtual manufacturer',N'Federica Farinazzo',N'No',NULL,'2021-05-30 00:00:00',N'verificare assenza NC con le materie prime PEI',NULL,NULL,NULL,NULL,NULL,NULL,'2021-04-28 00:00:00',NULL),
    (N'20-31',N'Correttiva','2020-09-16 00:00:00',N'Lucia  Liofante',N'Controllo qualità',N'Non conformità',N'20-51',N'CTR201ING - ESBL - ELITe Positive Control',N'Documentazione SGQ
Stabilità',N'é in corso l''indagine sulla causa primaria da parte di CQ.  Il lotto è stato segregato in area NC da verrà eliminato.',N'Vista l''attuale impossibilità di produrre un lotto di 962-CTR201ING che rientri nei criteri di CQ in uso, si richiede l''estensione della scadenza del lotto già esistente, U0120-010, da 24 a 30 mesi (fino a marzo 2021)',NULL,N'Impatto positivo: viene meno la necessità di produrre un nuovo lotto di 962-CTR201ING e vengono riqualificati dei kit obsoleti (n°9 kit)
Impatto positivo: viene meno la necessità di eseguire il CQ su un nuovo lotto
Impatti solo in termini di azioni',N'16/09/2020
16/09/2020
16/09/2020',N'Rilavorazione del lotto U0120-010 con estensione della scadenza da 24 a 30 mesi (fino a marzo 2021)
Modifica scadenza controcampione del lotto rilavorato
Valutazione della fattibilità dell''estensione della scadenza sulla base dei dati di stabilità accelerata di 962-CTR201ING e comunicazione a Manufacturing',N'Report stabilità a 30 mesi STB2019-128 , STB2019-181, STB2019-200  - rac20-31_ctr201ing_estensione_scadenza_30_,esi.msg',NULL,N'16/09/2020
16/09/2020
16/09/2020',NULL,N'16/09/2020
16/09/2020
16/09/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto sul prodotto immesso in commercio',N'La scadenza dei CTR rimane invariata (massimo 24 mesi), solo in questo caso, per evitare il back order ed essendo in possesso dei dati di stabilità a 30 mesi, si è deciso di ri-lavorare 9 kit di CTR120ING e di estendere la scadenza a 30 mesi.',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto',N'No',N'vedere MOD05,36. La scadenza dei CTR rimane invariata (massimo 24 mesi), solo in questo caso, per evitare il back order ed essendo in possesso dei dati di stabilità a 30 mesi, si è deciso di ri-lavorare 9 kit di CTR120ING e di estendere la scadenza a 30 mesi. si valuta non necessario effettuare alcuna notifica.',N'No',N'nessun vritual manufacturer',NULL,N'No',NULL,'2021-03-31 00:00:00',N'Verificare che nessuno dei 9 kit rilavorati a 30 mesi del lotto U0120-010 (scadenza 31/03/2021) abbia dei reclami a carico',NULL,NULL,NULL,NULL,NULL,NULL,'2020-09-16 00:00:00',NULL),
    (N'20-19',N'Correttiva','2020-06-23 00:00:00',N'Lucia  Liofante',N'Ricerca e sviluppo',N'Reclamo',N'20-43',N'909-IFMR-45 - GeneFinder? COVID-19 Plus RealAmp Kit',N'Assay Protocol',N'Specifici lotti di prodotto (2004-R45-18 e 2004-R45-25) sono caratterizzati dalla presenza di un segnale di fondo che incrocia la soglia di analisi nel canale del gene N su piattaforma InGenius ',N'Analisi sessioni cliente e identificazione parametri di analisi che correggano la rilevazione dei segnali aspecifici e conseguente refertazione di campioni falsi positivi
Ottimizzazione dei cut-off del Controllo Positivo in conseguenza della modifica dei parametri di analisi  per correzione aspecifico e in relazione alla problematica sul gene E rilevata nel reclamo 20-40
Richiesta reso al fornitore Osang Healthcare sui lotti coinvolti ancora presenti a magazzino
Richiesta al fornitore Osang Healthcare dell''esito dell''indagine interna per identificazione causa primaria delle prestazioni fuori specifica ',NULL,N'Nessun impatto, se non in termini di azioni
Nessun impatto, se non in termini di azioni
Nessun impatto, se non in termini di azioni
Nessun impatto, se non in termini di azioni
Positivi, in quanto la modifica dell''AP consente di contenere le problematiche di segnali aspecifici emerse sul campo e il reso dei lotti a magazzino preverrebbe il manifestarsi di nuovi eventi presso i clienti
D''altro canto, l''aumento della stringenza sul canale del gene N potrebbe esasperare i problemi di rilevazione dei campioni bassi positivi già emersi con il reclamo 20-33
Nessun impatto perchè l''incidente riguarda le prestazioni dei lotti del prodotto IFMR-45 - GeneFinder? COVID-19 Plus RealAmp Kit
Nessun impatto perchè l''incidente riguarda le prestazioni dei lotti del prodotto IFMR-45 - GeneFinder? COVID-19 Plus RealAmp Kit
Nessun impatto, se non in termini di azioni
Nessun impatto, se non in termini di azioni
Positivi, perchè il reso risolverebbe l''incremento del valore di magazzino legato al confinamento ',N'06/07/2020
06/07/2020
30/06/2020
30/06/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020',N'Analisi sessioni cliente e identificazione parametri di analisi che correggano la rilevazione dei segnali aspecifici e conseguente refertazione di campioni falsi positivi
Ottimizzazione dei cut-off del Controllo Positivo in conseguenza della modifica dei parametri di analisi  per correzione aspecifico e in relazione alla problematica sul gene E rilevata nel reclamo 20-40
nessuna 
Attività di review delle prestazioni sul campo del prodotto 909-IFMR-45 - GeneFinder COVID-19 Plus RealAmp Kit
Intallazione nuova versione AP presso i clienti utilizzatori
Supporto al fornitore Osang Healthcare nella gestione della notifica dell''incidente
Invio NOTA TECNICA redatta in collaborazione con il fornitore Osang Healthcare a tutti i clienti che hanno ricevuto i lotti coinvolti
Richiesta al fornitore Osang Healthcare dell''esito dell''indagine interna per identificazione causa primaria delle prestazioni fuori specifica 
Messa in uso nuova versione AP
A seguito di trattativa, gestione del reso con il fornitor',N'NOTA TECNICA DEL 1-6-2020 - 2020-06-01_nota_tecnica_genefinder_covid_01_it.pdf
NOTA TECNICA DEL 1-6-2020 - 2020-06-01_nota_tecnica_genefinder_covid_01_it_1.pdf
review delle prestazioni sul campo del prodotto - rac20-19_genefinder_covid-19_2020-06-16_review_prestazioni_sul_campo.pptx
CLIENTI ITALIA - clienti_italia_-_covid.xlsx
PAESI EU E EXTRA EU - eu_countries_sales_and_notifications.xlsx
Presso tutti i clienti Italia è stata installata la nuova versione dell''AP, ad esclusione del Negrar, il cui aggiornamento è pianificato entro settembre - rac20-19_stato_installazione_rev03ap_presso_clienti_italia_09-2020_manca_solo_negrar.txt
eseguita installazione nuova versione AP al negra - rac20-19_termineinstallazione_rev03ap_eseguitaalnegrar.msg
FSN OSANG - fsn_18jun2020_signed.pdf
NOTA TECNICA_EN_FIRMATA IL 1/06/2020 - 2020-06-01_technical_notice_genefinder_covid_01_en.pdf
Identificazione causa primaria da parte del fornitore Osang - root_cause_and_improvement_by_osang_2020.07.02_2.pdf
messa in uso ap  - rac20-19_nuova_versione_assays_osang_ifmr45.msg
Trattativa kit osang per ritiro kit spagna, francia e su africa - rac20-19_mail_per_la_gestione_del_rientro_kit_a_osang.msg
dichiarazione di reso kit dal sud africa - rac20-19_dichiarazione_di_reso_da_sud_africa.pdf',NULL,N'06/07/2020
06/07/2020
30/06/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020
06/07/2020',NULL,N'01/06/2020
01/06/2020
01/06/2020
16/06/2020
15/10/2020
18/06/2020
01/06/2020
02/07/2020
19/05/2020
04/03/2021',N'No',N'nessun impatto prodotto del fornitore OSANG',N'Si',N'invio nota tecnica',N'Si',N'invio nota tecnica con correzione AP ',N'Impatto regolatorio a carico del fornitore OSANG',N'ALTRO - Altro',N'ALTRO - ALTRO',N'NA',N'No',N'A carico del fornitore OSANG',N'No',N'Nessun virtula manufacture',NULL,N'No',NULL,'2020-12-18 00:00:00',N'Assenza reclami con uguale causa primaria.
17/11/2020 i reclami che convogliano in questa azione correttiva sono R20-38, R20-39, R20-40, R20-43 e R20-49, non ci sono altri reclami con uguale causa primaria.
4/03/2021 non ci sono reclami con uguale causa primaria. La RAC può essere chiusa.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2021-03-04 00:00:00','2021-03-04 00:00:00'),
    (N'20-17',N'Correttiva','2020-06-22 00:00:00',N'Alessandro Bondi',N'Validazioni',N'Reclamo',N'20-17',N'MDR/MTB (RTS120ING)',N'Assay Protocol',N'Allineandoci con la software house Lutech e quella francese del GLIMS si è identificato il seguente tracciato per conformare la stringa : 
MTB:DNA detected Typing as followsRIF:RESISTANCE POSITIVE rpoB2, rpoB3, rpoB4, rpoB1INH:RESISTANCE POSITIVE inhA',N'Necessaria una modifica all''assay protocol in modo che le stesse frasi dei risultati riportino i separatori "  " e " : " nelle posizioni specifiche individuate. Necessario poi generare un risultato di prova per verificarne la conformatà rispetto all''atteso.',NULL,N'Impatto positivo, corretto interfacciamento ai LIS.
La modifica non ha impatto sul prodotto in quanto si tratta di una modifica per l''interfacciamento con il lis.
Nessun impatto se non in termini di attività.
La modifica non ha impatto sul prodotto in quanto si tratta di una modifica per l''interfacciamento con il lis. La verifica dell''assay protocol per quanto riguarda i dati rilasciati al LIS è in corso di valutazione nella RAC20-4',N'22/06/2020
01/07/2020
01/07/2020
01/07/2020',N'Aggiornamento TAB e caricamento assay protocol sugli strumenti presso i clienti.
Verifica dell''assay protocol nuova versione generato da Validazioni
Messa in uso del nuovo protocollo generato e revisionato secondo procedura
Creazione nuovo assay protocol come da esito indagine',N'TAB P24 Rev. AD, MAIL INVIATA IL 7/7/2020 - rac20-17_mail7-7-2020_elite_ingenius-tab_p24-rev_ad-_product_mdr_mtb_elite_mgb_notice_1.pdf
ap in uso al 2/7/2020 - rac20-17_messa_in_suo_nuova_revisione_ap_coagulation_rac_20-16__rac_20-17.msg
ap in uso al 2/7/2020 - rac20-17_messa_in_suo_nuova_revisione_ap_coagulation_rac_20-16__rac_20-17_1.msg
la modifica non è significativa, pertanto non è da notificare. - rac20-17_mod05,36__non_significativa.pdf
ap in uso al 2/7/2020 - rac20-17_messa_in_suo_nuova_revisione_ap_coagulation_rac_20-16__rac_20-17_2.msg',NULL,N'01/07/2020
01/07/2020
01/07/2020
01/07/2020',NULL,N'07/07/2020
02/07/2020
02/07/2020
02/07/2020',N'No',N'nessun impatto',N'Si',N'si effettua con la TAB',N'No',N'nessun impatto sul prodotto immesso in commercio',N'è in corso di valutazione la verifica dei dati rilasciati al lis nella RCA20-4',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'La valutazione sull''impatto sul risk assessment AP1 verrà valutata a fine anno',N'No',N'vedre MOD05,36, le modifiche relative agli AP non sono notificate.',N'No',N'non necessaria',NULL,N'No',NULL,'2021-01-30 00:00:00',N'verificare assenza reclami con uguale causa primaria:
27/01/2020 efficacia negativa: all''installazione del AP presso il cliente (ELITECHFRANCE (Signes)) la correzione è risultata fallimentare, finché non vengono eliminati i ":" dopo "Typing as follows" dal fornitore PSS non sarà possibile mettere in uso un AP funzionale. Richiesta a PSS la modifica del modello interpretativo 12 (Ticket Mantis 452), riportato nella rac21-6. si ritiene pertanto possibile chiudere quest''azione correttiva.',N'Roberta  Paviolo',N'Negativo',N'Richiesta a PSS della modifica del modello interpretativo 12 (Ticket Mantis 452)',NULL,NULL,NULL,'2020-07-07 00:00:00','2021-04-08 00:00:00'),
    (N'20-16',N'Correttiva','2020-06-16 00:00:00',N'Alessandro Bondi',N'Assistenza applicativa',N'Non conformità',N'20-47',N'AP prodotti EGSpA',N'Assay Protocol',N'Manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso il cliente',N'Per agevolare gli FPS nella pianificazione delle attività di aggiornamento degli AP presso il cliente potrebbe essere utile un''automazione che evidenzi gli AP obsoleti presso i clienti, si ritiene pertanto necessario valutare i costi per un aggiornamento della dashboard. ',NULL,N'Impatto positivo: una gestione controllata dell''installazione degli AP presso i clienti (in ordine di obbligatorietà dell''aggiornamento se derivante da RAC o da modifiche di prodotto incompatibili con la versione dell''AP precedente) garantisce che il cliente abbia sempre installata l''ultima versione e che non permangano versioni obsolete che potrebbero generare errori. Il monitoraggio automatico attraverso la dashboard delle versioni di AP installate presso il cliente rende necessario valutare i costi per un aggiornamento della dashboard e mettere in atto il cambiamento. 
Impatto positivo: una gestione controllata dell''installazione degli AP presso i clienti (in ordine di obbligatorietà dell''aggiornamento se derivante da RAC o da modifiche di prodotto incompatibili con la versione dell''AP precedente) garantisce che il cliente abbia sempre installata l''ultima versione e che non permangano versioni obsolete che potrebbero generare errori. Il monitoraggio automatico attraverso la dashboard delle versioni di ',N'16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
05/08/2020
05/08/2020
05/08/2020
16/06/2020
02/09/2020
02/09/2020',N'Richiesto al fornitore HandsLab una quotazione per l''aggiornamento della funzionalità sulla Dashboard
formazione agli FPS
Creazione risk assessment di processo (AP4) e risk assessment sulla dashboard, sulla base di quest''ultimo stabilire se validare il sw e/o il processo. Esecuzione di test sulla dashboard.
Valutazione sul monitoraggio dell''installazione degli AP di terze parti (rilasciati ai clienti Italia a seguito di gara, rilasciati ai clienti Italia con lettera di manleva, rilasciati ai distributori esteri con lettera di manleva) e conseguente aggiornamento della PR05,08
Valutare eventuali impatti sulla struttura attuale della Dashboard e se il web repository può essere collegato alla lista delle versioni degli AP
Aggiornamento della procedura PR19,01 inserendo le modalità di gestione degli assay protocol presso il cliente, della PR05,08 e valutazione della stesura di un''istruzione operativa che dettagli tutte le attività svolte dagli specialist
Aggiornamento del VMP e del risk assessment',N'Quotazione e richiesta progetto - ss20_assay_notification_elitechgroup.pdf
PR19,01_rev02 - rac20-16_messa_in_uso_pr1901_rev02_1.msg
la modifica non è significativa, pertanto non è da notificare. - rap20-16_mod05,36_01_non_significativa.xlsx
PR05,08_01 - rac20-26_messa_in_uso_pr0508_02_dump_file.msg
PR19,01_rev02 - rac20-16_messa_in_uso_pr1901_rev02.msg',NULL,N'16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
05/08/2020
16/06/2020
05/08/2020
16/06/2020',NULL,N'26/03/2021
27/01/2021
26/03/2021
26/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria, aggiornamento di processo',N'No',N'nessun impatto, aggiornamento di processo',N'Una gestione controllata dell''installazione degli AP presso i clienti garantisce che il cliente abbia sempre installata l''ultima versione e che non permangano versioni obsolete che potrebbero generare errori.',N'AP - Gestione Assay Protocol',N'AP4 - Gestione installazione assay Assay Protocol presso il cliente',N'AP4 da aggiornare a seguito di modifiche',N'No',N'vedere MOD05,36',N'No',N'non necessaria',N'Alessandro Bondi',N'No',NULL,NULL,N'Verificare assenza di reclami dopo l''implementazione della modifica da attribuire a versioni obsolete degli AP presso il cliente',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'20-10',N'Correttiva','2020-03-16 00:00:00',N'Stefania Brun',N'Ricerca e sviluppo',N'Non conformità',N'20-9',N'RTSD00ING ',N'Materia prima
Progetto',N'Livelli di fluorescenza ridotti osservati in associazione a determinati lotti di PCR cassettes (J190401 e J190513)  tipizzazione eterozigote errata per campioni omozigoti mutati la cui frequenza è molto bassa e inoltre risultati inconclusive per i campioni wt la cui frequenza è alta. Non sono state evidenziate performance negative su altri prodotti in associazione ai lotti indicati di PCR cassettes ma esclusivamente con il prodotto RTD00ING e specificatamente con il target del FII la cui fluorescenza finale è più bassa rispetto a quella degli altri target',N'Come azione a breve termine si propone di controllare tutti i lotti di PCR cassette tramite CQ in associazione al prodotto RTSD00ING. 
Come azione a lungo termine si richiede la manutenzione del prodotto RTSD00ING al fine di migliorare le prestazioni e renderle meno sensibili a performance non ottimali del materiale di consumo quale PCR cassette.
Monitorare le performance presso gli utilizzatori e provvedere alla sostituzione dei lotti di PCR cassettes nei casi di risultati inconclusive',NULL,N'Impatto in termini di attività 
Positivo in quanto permette di prevenire reclami relativi a risultati inconclusivi
Positivo in quanto permette di prevenire reclami relativi a risultati inconclusivi
Impatti in termini di attività 
Impatto positivo in quanto permette di prevenire incidenti quale tipizzazione eterozigote errata per campioni omozigoti mutati
Impatto positivo in quanto permette di prevenire incidenti quale tipizzazione eterozigote errata per campioni omozigoti mutati
Una manutenzione del prodotto RTSD00ING permetterebbe di migliorare le prestazioni e renderle meno sensibili a performance non ottimali del materiale di consumo quale, per esempio PCR cassette.',N'10/07/2020
10/07/2020
10/07/2020
16/03/2020
10/07/2020
10/07/2020
10/07/2020',N'Comunicare al CQ l''arrivo di nuovi lotti di PCR cassette e prevedere rientro di 1 kit presso EGSpA da Ilmed
Segnalare attraverso una TAB la problematica rilevata
Monitorare gli utilizzatori del prodotto RTD00ING e organizzare sostituzione lotti di PCR cassettes se necessario
Eseguire il CQ dei lotti di PCR cassettes utilizzando i requisiti del prodotto RTSD00ING. Seguire e utilizzare i moduli MOD10,13-RTSD00ING-CTRD00ING e MOD10,12-RTSD00ING-CTRD00ING per quanto riguarda la sessione 2. Le PCR cassette risultano idonee in caso di verifica tecnica superata.
Valutare l''attuale analisi dei rischi del prodotto RTSD00ING per verificare che il rischio evidenziato sia incluso e successivamente a seguito di manutenzione del prodotto
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Manutenzione del prodotto RTSD00ING al fine di migliorare le prestazioni e renderle meno sensibili a performance non ottimali del mat',N'A 07-2020 i lotti monitorati sono i seguenti J200114 J200226 J200512 - rac20-10_stato_monitoraggio_lotti_pcr_cassette_a_07-2020.txt
A 07-2020 i lotti monitorati sono i seguenti J200114 J200226 J200512 - rac20-10_stato_monitoraggio_lotti_pcr_cassette_a_07-2020.txt
il lotto TJ201207 è conforme - rac20-10_esito_cq_pcr_cassette_tj201207_01-21.msg
i lotti TJ210101A e TJ210101B-2 sono conformi - rac20-10_esito_cq_pcr_cassette_tj210101a_e_tj210101b-2__04-21.msg
il lotto tj210222-1 è conforme - rac20-10_esito_cq_pcr_cassette_tj210222-1.msg
il lotto TJ210301B è conforme - rac20-10_esito_cq_pcr_cassette_tj210301b.msg
RAC20-10_si ritiene non necessario monitorare i lotti i pcr cassette presso gli utilizzatori - rac20-10_si_ritiene_non_necessario_monitorare_i_lotti_i_pcr_cassette_presso_i_clienti.txt
la significatività della modifica è associata al codice progetto SCP2020-007 - rac20-10_la_significativita_della_modifica_e_da_vedere_in_associazione_al_codice_progetto_scp2020-007.txt
RICHIESTA CODICE PROGETTO SCP2020-007 - rac20-5__richiesta_codice_scp_coagulation.msg',NULL,N'10/07/2020
10/07/2020
10/07/2020
16/03/2020
10/07/2020
10/07/2020
10/07/2020',NULL,N'05/08/2020
31/08/2020',N'No',N'No ',N'No',N'Da valutare in base alle modifiche apportate al prodotto',N'No',N'Si ',N'é stato aperto un progetto di manutenzione, SCP2020-007, che andrà a migliorare il prodotto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'aperta una manutenzione di prodotto, SCP2020-007.',N'Si',N'MOD05,36 da vedere in riferimento al codice progetto SCP2020-007',N'No',N'No in quanto non è un prodotto OEM con virtual manufacturer',N'walter carbone',N'Si',N'Valutare l''attuale analisi dei rischi del prodotto RTSD00ING per verificare che il rischio evidenziato sia incluso e successivamente a seguito di manutenzione del prodotto',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'20-3',N'Correttiva','2020-02-12 00:00:00',N'Katia Arena',N'Acquisti',N'Non conformità',N'19-127',N'Prodotti OEM Fast Track',N'Documentazione SGQ',N'Le tempistiche di fornitura non sono state rispettate rispetto ai precedenti ordini, FTD ha comunque rispettato i tempi indicati nel contratto di fornitura. Il ritardo di fornitura può essere associato alla riorganizzazione interna per l''acquisizione da parte di Siemens Healthineers Company.',N'Per mitigare il ritardo nella fornitura delle bulk rispetto all''atteso, FTD suggerisce di compensare con forniture più corpose e firmare la documentazione di rilascio del lotto fornita da FTD.',NULL,N'Visionare e firmare i CQ inviati da FTD
Le tempistiche di fornitura da parte di FTD erano più rapide rispetto a quanto indicato nel contratto. il rispetto delle tempistiche come da contratto ha un impatto negativo in quanto, per avere disponibilità dei prodotti, è necessario aumentare i quantitativi ordinati per ciascuna fornitura.
La definizione delle tempistiche di fornitura non ha impatto sul SGQ, le quantità richieste a FTD devono essere proporzionate alle richieste del cliente. La firma della documentazione di FTD per il rilascio del lotto da parte di CQ è una buona prassi per la tracciabilità del controllo effettuato.',N'09/03/2020
12/02/2020
12/02/2020',N'Visionare e firmare i CQ inviati da FTD
Implementazione dei quantitativi ordinati e firma della documentazione FTD di rilascio del lotto da parte di CQ come suggerito da FTD.
nesssuna attività a carico',N'Ad 08/2020 l''implementazione dei quantitativi ordinati è sufficiente a soddisfare le richieste dei clienti.  La firma della documentazione FTD di rilascio del lotto da parte di CQ come suggerito da FTD è ancora in essere almeno fino a valutazione dell''efficacia di questa rac. - rac20-3_monitoraggio_tempistiche_di_fornitura.txt',NULL,N'09/03/2020
12/02/2020
12/02/2020',NULL,N'06/08/2020
12/02/2020',N'No',N'na',N'No',N'na',N'No',N'na',N'Le tempistiche di fornitura non hanno impatti sui requisiti regolatori, è importante effettuare ordini adeguati alle richieste dei clienti affinché vi sia sempre disponibilità del prodotto.',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'na, l''azione correttiva non conduce a nessuna modifica documentale o di processo, ma mira a monitorare il fornitore',N'No',N'na',NULL,N'No',NULL,'2021-02-26 00:00:00',N'Monitoraggio delle tempistiche di fornitura nel corso di un anno.
ad 08/2020 non ci sono NC relative a ritardi nelle tempistiche di fornitura rispetto a quanto indicato nel contratto.
a 01/2021 ad 08/2020 non ci sono NC relative a ritardi nelle tempistiche di fornitura si ritiene pertanto possibilie chiudere la rac.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2020-08-06 00:00:00','2021-01-13 00:00:00'),
    (N'19-39',N'Preventiva','2019-12-04 00:00:00',N'Alessandra Gallizio',N'Produzione',NULL,NULL,NULL,N'Etichette',N'Poiché è probabile la coesistenza delle due versioni di kit presso il cliente nei primi mesi dopo la chiusura del progetto di manutenzione prodotto, è necessario distinguere temporaneamente le due versioni del kit, per guidare il cliente all''uso delle IFU corrette. E'' necessario apporre un identificativo esterno temporaneo per distinguere la nuova versione del kit.',N'Si richiede di apporre, sul lato corto della nunc del prodotto RTSG07PLD190, lotto U1119AO, (ed anche del prodotto correlato STDG07PLD190, lotto U1119AU) una etichetta verde riportante la dicitura: "WARNING Product technical update, please contact your local support for further information - AVVERTENZA Il prodotto presenta un aggiornamento tecnico, contattare il supporto locale per maggiori dettagli"
L''etchetta verrà apposta a confezionamento ultimato e verrà compilato un apposito modulo di rilavorazione (MOD09,08)',NULL,N'la rilavorazione del lotto U1119AO (e U1119AU) non ha impatti sulle prestazioni del prodotto in quanto si eseguono le indicazioni riportate per il  confezionamento (MOD962-RTSG07PLD190), che garantiscono idonee condizioni di permanenza a temperatura ambiente per apposizione della etichetta aggiuntiva.
la rilavorazione del lotto U1119AO (e U1119AU) garantisce la corretta identificazione del lotto 
la rilavorazione del lotto U1119AO (e U1119AU) attraverso l''apposizione di una etichetta che lo differenzia dal lotto in uso mitiga il rischio presso il cliente di utilizzare le IFU relative alla vecchia versione di kit
l''impatto è positivo in quanto il cliente, informato dal personale MKT, può distinguere agevolmente tra la nuova e la vecchia versione di kit fintanto che entrambe potranno coesistere',N'06/12/2019
16/12/2019
16/12/2019
16/12/2019',N'rilavorazione del terzo lotto pilota (RTS e STD): stampa etichetta verde  e apposizione su tutte le confezioni
rilascio del lotto rilavorato e verifica DHR
verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
approvare il contenuto dell''etichetta verde e veicolare l''informazione ai clienti tramite apposita lettera informativa',N'I prodotti RTSG07PLD109, lotto U1119AO (30kit), e STDG07PLD190, lotto U1119AU (20 kit), sono stati rilavorati con l''aggiunta dell''etichetta verde, come da MOD09,08 firmato al 20/01/2020  - rap19-39_rilavorazioneu1119ao,_u1119au.txt
lotti rilavorati - rap19-39_rilavorazioneu1119ao,_u1119au.txt
la modifica non è significiativa, per tanto non è da notificare. - rac19-39_mod05,36_non_significativa.pdf
dicitura etichetta verde approvata - rap19-39_rilavorazioneu1119ao,_u1119au_1.txt',NULL,N'06/12/2019
16/12/2019
16/12/2019
16/12/2019',NULL,N'21/01/2020
21/01/2020
16/12/2019
21/01/2020',N'No',N'nessun impatto',N'Si',N'attraverso l''etichetta l''apposizione dell''etichetta',N'No',N'nessun impatto',N'Coesisteranno 2 prodotti con uguale packaging, ma IFU differenti, l''etichetta posta sulla scatola mitiga il rischio di errore',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun ra associato',N'No',N'vedere MOD05,36. Non significativa si tatta di un''etichetta informativa, non sul prodotto.',N'No',N'nessun virtual manufacturer',N'Alessandra Gallizio',N'No',NULL,'2020-07-30 00:00:00',N'VALUTAZIONE EFFICACIA. 01/2020: dal momento che coesisteranno 2 versioni del prodotto BCR-ABL, la nuova contraddistinta da un etichetta di warning di colore verde apposta sul lato della confezione e la precedente (senza etichetta verde) verificare che non vi siano errori in fase di spedizione durante tutto il periodo di coesistenza.

Ad 08/2020 non sono presenti reclami di spedizione: l''ultimo lotto venduto nel formato "vecchio" è il lotto U1119AN (scadenza 31/01/2021) in data 14/04/2020. La coesistenza delle 2 versioni di kit è terminata a 04/2020, si ritiene pertanto possibile chiudere questa azione preventiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2020-01-21 00:00:00','2020-08-27 00:00:00'),
    (N'19-34',N'Correttiva','2019-11-06 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'19-45',N'RTS548ING - Respiratory Viral MGB Panel',N'Assay Protocol',N'I campioni a basso titolo non vengono rilevati a causa dei limiti dell''algoritmo di calcolo del Software InGenius, nonostante la presenza delle curve di amplificazione valide e inoltre il ciclo termico a 40 cicli non ne permette può limitare l''amplificazione efficace di campioni a basso titolo.',N'Al fine di migliorare e/o irrobustire le prestazioni del prodotto RTS548ING  in associazione al sistema ELITe InGenius, si richiede di valutare la modifica del ciclo termico e degli Assay parameter  (Amp criteria) in modo da ottenere una maggiore efficienza di reazione in termini di Ct. Queste modifiche sono già state apportate per i prodotti OEM/FTD (RTS507ING e RTS523ING) con esiti positivi (vedi RAP19-22 e RAC19-2).',NULL,N'Una modifica del ciclo termico a livello di temperatura di annealing/extension e di numero di cicli avrebbe un forte impatto sulle prestazioni del prodotto e potrebbe richiedere una rivalidazione.
La modifca del parametro AMP criteria permetterà di calcolare il Ct anche nel caso di amplificazioni deboli come quella del campione in oggetto evitando il reiterarsi della problematica presso i clienti.
Questo tipo di modifica necessita della ri-analisi con la macro Excel dei dati di validazione per verificare eventuali cambiamenti dei risultati già ottenuti.
In base alle modifiche apportate al ciclo termico, il cambiamento potrebbe richiedere una rivalidazione.  
Impatto positivo se le modifiche apportate non pregiudicano le prestazioni e la sicurezza del prodotto. 
Nessun impatto se non in termini di attività.
La modifica apportata eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La modifica apportata eviterà il reiterarsi della problematic',NULL,N'Valutazione degli effetti di una modifica del ciclo termico su campioni a basso titolo e degli Assay Parameter sui dati pregressi.
Modifica / verifica AP del cliente e AP del CQ.
Verifica  dell''AP modificato
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Rilascio Assay Protocol modificati.
Installazione presso i clienti della nuova versione dell'' AP che verrà rilasciato in seguito alle modifiche
Stesura TAB
Prove di settaggio del ciclo termico su campioni a basso titolo. Installazione dell''AP modificato sullo strumento InGenius di CQ',NULL,NULL,NULL,NULL,NULL,N'No',N'nessun impatto',N'Si',N'con la TAB',N'No',N'valutare',N'valutare',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun prodotto',NULL,N'No',NULL,NULL,N'a 10/2020 è stato immesso in commercio il prodotto EGSpA RTS160ING "Respiratory Viral PLUS elite MGB kit", si ritiene pertanto possibile non procedure con le attività indicate in questa RAC e chiuderla.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,NULL,'2021-02-24 00:00:00'),
    (N'19-33',N'Correttiva','2019-11-05 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',N'Non conformità',N'19-118',N'Assay Parameter Tool',N'Documentazione SGQ',N'La PR05,03 rev01 non tratta esplicitamente i software commerciali, ossia i sw acquistati da un fornitore qualificato ed utilizzati secondo l''uso previsto dettato dal fornitore, ma parla genericamente di sw non ivd. Questa tipologia di sw non sempre sono accompagnati da un risk assessment e dalla documentazione di iq/oq/pq, come richiesto dalla procedura, soprattutto nei casi di sw considerati commerciali.',N'Individuare tutti i sw utilizzati in EGSpA (nominati nel VMP) che non hanno un risk assessemnt collegato.
Verificare che i sw commerciali siano comunque stati insallati e verificati prima dell''uso con evidenze scritte (iq/oq e pq se necessaria o documentazione simile)
Produrre i risk assessment mancanti (utilizzatori).
Aggiornare la PR05,03 rev01con una classificazione più stringente dei sw presenti in azienda, in particolare i sw considerati commerciali.
Effettuare formazione affinchè tutti gli utilizzatori di sw affinchè siano a conoscenza della documentazione necessaria per la messa in uso di un sw, anche se considerato commerciale.',NULL,N'Corretta gestione dei sw associati a strumentazione Elitech.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolatori.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolatori.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolatori.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolatori.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolatori.
Corretta gestione dei sw utilizzati in EGSpA (influenti sulla qualità del prdotto), sulla base della loro classificazione. Rispetto dei requisiti regolator',NULL,N'Redigere i risk assessment mancanti dei sw associati a strumentazione elitech, individuati da QAS.
Individuare i sw utilizzati da EGSpA, nominati nel VMP, influenti sulla qualità del prodotto che non hanno un risk assessment collegato.
Verificare i documenti a supporto dell''installazione e verifica dei sw commerciali.
Valutare se necessario produrre della documentazione di IQ/OQ/PQ ed il personale che deve produrla.
Verificare che tutti i sw utilizzati in azienda, influenti sulla quallità del prodotto, siano stati codificati e gestiti secondo la procedura in essere PR05,03 (per esempio: il SIMULATORE degli assay protocol).
Analizzare il processo di gestione degli assay protocol separatamente dal processo di gestione delle IFU, quindi produrre dei risk assessment solo per la gestione degli AP, come evidenziato nell''osservazione OSS1-13-19 (audit validazioni).
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifi',N' Dei 35 SW in uso (come da elenco MOD05,08), 16 non hanno un risk assessment collegato. Di questi 7 riguardano sw commerciali associati ad uno strumento e hanno la documentazione di IQ/OIQ/PQ associata allo strumento. Tra i rimanenti sw è presente l''Assay Parametr Tool e altri sw pubblici (esempio: Annib, FASTA ecc).  - rac19-33_listasweapplicazioniinfluentiqualita_senza_ra.xlsx
La modifica non è significativa, pertanto non è da notificare presso le autorità comptenti. - rac19-33_mod05,36_non_significativa.pdf',NULL,NULL,NULL,N'05/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Rispetto dei requisiti regolatori.',N'IST - IQ-OQ-PQ strumenti (e software incluso)
ISW - IQ-OQ-PQ software customizzati',N'IST5 - Rapporto IQ / OQ / PQ
ISW5 - Rapporto IQ / OQ / PQ',N'valutare l''impatto sui processi di iq/oq/pq di sw associati a strumentazione e dei sw customizzati',N'No',N'vedere MOD05,36',N'No',N'non necessaria',NULL,N'Si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'19-31',N'Correttiva','2019-10-08 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'19-76',N'Assay Protocol STI PLUS  ELITe MGB® Kit',N'Assay Protocol',N'Dai risultati delle analisi condotte (si veda Report Indagine_R19-76 allegato) si può escludere un mal funzionamento o una diminuzione delle performance del prodotto «STI PLUS ELITe MGB® Kit», codice RTS400ING. La discordanza dei dati è attribuibile ai limiti dell''algoritmo di calcolo del  software del sistema che non è sufficientemente sensibile per rilevare un plot di amplificazione visibile su campioni a basso titolo. Pertanto, il reclamo risulta fondato dal momento che è associato al software dello strumento ELITe InGenius®. La cliente è stata contattata telefonicamente da CCA per confermare la positività del campione oggetto del reclamo e per suggerire di monitorare il paziente nel tempo vista la bassa positività.',N'La modifica dell''AMP criteria delll''AssayProtocol permette di rilevare correttamente le curve dei campioni a basso titolo (come descritto nell''oggetto del reclamo 19-76) e  campioni non rilevati (19-71). Si propone quindi la modifica l''AMP criteria portandolo dal valore di 4 a 1 e verrà eseguita una ri-analisi dei dati di validazione.
',NULL,N'A fronte di un esito positivo della ri-analisi dei dati di validazione, la modifica apportata eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
A fronte di un esito positivo della ri-analisi dei dati di validazione, la modifica apportata eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
A fronte di un esito positivo della ri-analisi dei dati di validazione, la modifica apportata eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
A fronte di un esito positivo della ri-analisi dei dati di validazione, le modifiche apportate migliorano le prestazioni e la sicurezza del prodotto. Impatto positivo. 
La modifca del parametro AMP criteria del canale NG permetterà di calcolare più frequentemente il Ct nel caso di amplificazioni deboli comeil campione in oggetto del reclamo. D''altra parte questa modifica potrebbe rendere più frequenti i cas',N'24/10/2019
24/10/2019
24/10/2019
24/10/2019
18/10/2019
18/10/2019
24/10/2019
24/10/2019',N'Verifica dei clienti in possesso della versione attuale dell''AP ed installazione della nuova versione.
Creazione TAB.
verifica dei clienti cui aggiornare l''Assay protocol
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Ri-analisi con la macro Excel dei dati di validazione, modifica Assay Protocol,  verifica della TAB collegata.
-
Rilascio Assay Protocol modificati.
Ri-analisi con la macro Excel dei dati di validazione, verifica AP',NULL,NULL,N'24/10/2019
24/10/2019
24/10/2019
18/10/2019
24/10/2019
24/10/2019
24/10/2019',NULL,N'07/09/2020
07/09/2020
07/09/2020
07/09/2020
07/09/2020
07/09/2020',N'No',N'nessun impatto in quanto il DMRI non contiene le revisioni degli AP nè delle procedure',N'Si',N'comunicazione attraverso la TAB',N'No',N'il manuale di istruzioni riporta già l''indicazione di verificare i plot prima di approvare i risultati  ',N'Il prodotto non subisce modifiche alle prestazioni e sicurezza. ',N'SW - Elenco Software
R&D - Svilupppo e commercializzazione prodotti',N'SW20 - ELITe InGenius Software
R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto, anche se (dall''analisi degli eventi che ricadono in questo sottoprocesso) la probabilità di verificarsi di questo evento diventasse alta, la matrice di gestione del rischio non subirebbe alterazioni',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-09-07 00:00:00',N'Dall''analisi della sorveglianza post-market non emergono modifiche al profilo di rischio del prodotto confermando la decisione di non apportare modifiche all''AP, in accordo anche con l''assenza di altri reclami con uguale causa primaria.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2020-09-07 00:00:00','2020-09-07 00:00:00'),
    (N'19-30',N'Correttiva','2019-10-03 00:00:00',N'Maria Galluzzo',N'Ricerca e sviluppo',N'Reclamo',N'19-86',N'Assay Protocol utilizzato con lo strumento ELITe InGenius con il prodotto 909-V31-100/F FRT - HPV14Screen16,18,45 Typ RealT',N'Assay Protocol',N'Il prodotto in questione viene fornito in distribuzione. E'' utilizzato in associazione alla piattaforma InGenius in modalità Open come concordato con l''utilizzatore all''inizio della fornitura (Vedi in allegato lettera di manleva sottoscritta). La fondatezza dell''evento segnalato è stata confermata dall''indagine interna di cui l''evidenza è in allegato.
L''evento accaduto è stato prontamente riconosciuto dal cliente che ha messo in pratica le informazioni fornite nella fase  iniziale di utilizzo del prodotto; per questo motivo non si ritiene necessario classificare l''evento come incidente. La causa, in base all''indagine condotta, risulta essere legata ad alcuni parametri dell''Assay Protocol che in quanto rilasciato in modalità Open, non è validato.  
',N'Per mitigare la problematica riscontrata nel reclamo R19-86 è necessario modificare l''assay protocol. I cambiamenti apportati dovrebbero portare ad evitare il più possibile che curve con baseline negativa per il target HPV nel canale 4 vengano interpretate come undetermined e ad evitare che alcuni segnali del target g45 nel canale 6 vengano mal interpretati. Le modifiche da apportare riguardano:
• Amp criteria del canale 4 da settare =1;
• Sottrarre 0.1 di fluorescenza del dye 5 sul canale 6 applicando la cross talk assay specifica;
• Diminuire la Ct threshold del canale 6 a 100;
• Aggiornare tutti i target con skip cycle =1.

Effettuare un''analisi retrospettiva del database del cliente con i criteri dell''Assay Protocol modificati. 

Come azione a lungo termine è previsto un aggiornamento tecnologico con un prodotto che attualmente è in fase di validazione  su InGenius.',NULL,N'La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.
La mitigazione eviterà il reiterarsi della problematica presso il cliente, quindi l''impatto è valutabile come positivo.',N'16/10/2019
16/10/2019
16/10/2019
16/10/2019
16/10/2019
16/10/2019
16/10/2019',N'Installazioni  presso il cliente dell'' Assay Protocol con i criteri modificati come descritto sopra in "Descrizione dell''azione".
verifica di tutti i clienti cui è stato rilasciato AP
Valutare il tipo di comunicazione  da fornire al cliente sull''azione intrapresa.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Analisi retrospettiva del database del cliente con i criteri dell''Assay Protocol modificati come descritto sopra
Rilascio dell''Assay Protocol con le modifiche apportate',N'Visita presso il cliente e comunicazione comunicazione sull''azione intrapresa. - rac19-30_installazione_ap_presso_cliente_1.msg
Installazione AP presso il cliente - rac19-30_installazione_ap_presso_cliente.msg
Analisi retrospettiva - rac19-30_analisi_retrospettiva_db_cliente_con_ap_modificato.pdf
Messa in uso AP  - rca19-30_messa_in_uso_ap_open_prodotti_di_terze_parti.msg
La modifica non è significativa pertanto non è da notificare. - rac9-30_mod05,36_non_significativa.pdf',NULL,N'16/10/2019
16/10/2019
16/10/2019
16/10/2019
16/10/2019',NULL,N'18/10/2019
18/10/2019
16/10/2019
05/11/2019
15/10/2019',N'No',N'nessun impatto',N'Si',N'effettuare comunicazione all''unico cliente che utilizza il prodotto oggetto del reclamo',N'No',N'nessun impatto',N'Il prodotto non subisce modifiche di prestazioni e sicurezza.',N'IFU - Gestione Manuali',N'IFU2 - riesame manuale',N'nessun impatto, anche se (dall''analisi degli eventi che ricadono in questo sottoprocesso) la probabilità di verificarsi di questo evento diventasse alta, la matrice di gestione del rischio non subirebbe alterazioni',N'No',N'vedere MOD05,36, la modifica degli AP non è considerata una modifica significativa',N'No',N'nessun virtual manufacturer ',NULL,N'No',NULL,'2020-05-29 00:00:00',N'Assenza reclami a distanza di 6 mesi dalla chiusura delle azioni.
a distanza di 6  mesi dalla chiusura azioni, non sono stati rilevati altri reclami con uguale causa primaria.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-11-05 00:00:00','2020-05-29 00:00:00'),
    (N'19-21',N'Correttiva','2019-07-11 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'19-67',N'BANG08',N'Progetto',N'La NC è riconducibile a 2 cause di natura diversa. Per la mix ABL il fuori specifica è stato causato da un errore tecnico di CQ , mentre per la mix MBR si ipotizza un errore a livello produttivo non chiaramente identificato. Il prodotto presenta un''alta frequenza di non conformità: dal 2015 ad oggi 3 lotti su 6 hanno avuto test di sensibilità NC delineando un prodotto con prestazioni al limite della sensibilità.',N'In base all''analisi delle Non conformità e delle OOS di CQ relative al prodotto (vedi allegato sezione CQ) è stata evidenziata una riduzione di performance del prodotto in relazione alla sensibilità (dati interni) e alla presenza di segnali aspecifici di difficile interpretazione (reclami). Questo richiederebbe una manutenzione di prodotto al fine di garantire le performance del prodotto rispetto alle specifiche dichiarate. La decisione di intraprendere un progetto di manutenzione del prodotto deve essere valutata nell''ambito di una swot analysis o analisi costi benefici e rispetto ad altre soluzioni alternative  (es. dismissione del prodotto momento e     selezione di un prodotto alternativo in distribuzione o di aggiornare le specifiche del prodotto in base a una nuova valutazione relativa alla sensibilità)',NULL,N'-
-
-
-
-
-
-
La richiesta è strettamente collegata alla qualità del prodotto e sarà necessario valutare gli impatti definitivi in base all''azione che seguirà dalla presente RAC.
La richiesta è strettamente collegata alla qualità del prodotto e sarà necessario valutare gli impatti definitivi in base all''azione che seguirà dalla presente RAC.
La richiesta è strettamente collegata alla qualità del prodotto e sarà necessario valutare gli impatti definitivi in base all''azione che seguirà dalla presente RAC.',N'19/07/2019',N'Valutazione degli impatti in base alle azioni suggerite:
-Modifica delle specifiche di sensibilità. Test di riproducibilità aggiornamento IFU e RA
-Aggiornamento del prodotto. Tutte le attività previste per gli aggiornamenti del prodotto
-Dismissioni. Colaborazione nella scelta del prodotto da rivendere
Valutazione degli impatti in base alle azioni suggerite:
-Modifica delle specifiche di sensibilità. Test di validazione aggiornamento IFU e RA
-Aggiornamento del prodotto. Tutte le attività previste per gli aggiornamenti del prodotto
-Dismissioni. nessuna attività
Valutazione degli impatti in base alle azioni suggerite:
-Modifica delle specifiche di sensibilità. Verifica dell''impatto su materiali di MKT
-Aggiornamento del prodotto. Verifica dell''impatto su materiali di MKT
-Dismissioni. Selezione prodotto alternativo di rivendita. Si riportano sotto alcune aziende i codici presenti.
 nested
https://www.abanalitica.com/it/catalogo/prodotto/genequality-bcl-2/ 
https://www.invivoscribe.com/',NULL,NULL,NULL,NULL,NULL,N'No',N'-',N'No',N'-',N'No',N'-',N'-',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto sul risk assessment, eventuale pianificazione di un progetto di manutenzione',N'No',N'-',N'No',N'-',NULL,N'Si',N'-',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'19-19',N'Preventiva','2019-06-25 00:00:00',N'Katia Arena',N'Acquisti',NULL,NULL,NULL,N'Confezione',N'Il fornitore VWR ha segnalato, telefonicamente, delle possibili problematiche a livello produttivo.',N'La scatola NUNC da 10 posti è il contenitore utilizzato per il confezionamento di prodotti di Real time, Standard e Controlli Positivi. Per evitare l''impossibilità di fornire tali prodotti ai clienti, in accordo con MM e PP, si è valutato di procedere nel seguente modo:
- Definire la lista di prodotti pianificati da giugno a settembre, in modo da stimare il consumo di NUNC da 10 posti;
- confezionare solo delle quantità esigue che coprano le richieste del cliente, ed i rimanenti tubi confezionarli in un secondo momento utilizzando il modulo di rilavorazione ed attribuendo un nuovo lotto;
- verificare se esistono altri distributori cui ordinare il materiale;
- verificare  se Elitech Inc ha disponibilità di NUNC da 10 posti ed eventualmente ordinarle;
- a fine settembre, se il fornitore non ha ancora fornito il materiale ordinato, e se le scorte a magazzino EGSpA sono esaurite si valuterà l''ipotesi di confezionare i prodotti in 2 scatole NUNC da 5 posti. ',NULL,N'La mancanza di un materiale di confezionamento può causare ritardi nella consegna del prodotto finito presso il ciente. 
La mancanza di un materiale di confezionamento può causare ritardi nella consegna del prodotto finito presso il ciente. 
La mancanza di un materiale di confezionamento può causare ritardi nella consegna del prodotto finito presso il ciente. La produzione di lotti di confezionamento separati attraverso rilavorazioni, aumenta i tempi di produzione, ma non ha influenza sulle performance del prodotto, in quanto si rispettano i tempi di confezionamento definiti nei moduli di produzione. 
La mancanza di un materiale di confezionamento può causare ritardi nella consegna del prodotto finito presso il ciente. La produzione di lotti di confezionamento separati attraverso rilavorazioni, aumenta i tempi di produzione, ma non ha influenza sulle performance del prodotto, in quanto si rispettano i tempi di confezionamento definiti nei moduli di produzione. 
Valutare l''impatto econonomico nella',N'25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019',N'1) verificare se esistono altri distributori cui ordinare il materiale ed eventualmente implemtare in tal senso la SPEC954-141
2) verificare  se Elitech Inc ha disponibilità di NUNC da 10 posti ed eventualmente ordinarle
3) gestire il materiale  attraverso un ordine aperto nel quale si pianificano i quantitativi fissi di materiale da acquistare
7) verificare se sono presenti altri materiali per i quali non è disponibile un secondo fornitore e che, allo stato attuale, non sono estiti attraverso ordini aperti con quantitativi d''acquisto pianificati durante l''anno. 
4) Definire la lista di prodotti pianificati da giugno a settembre, in modo da stimare il consumo di NUNC da 10 posti;
5) confezionare solo delle quantità esigue che coprano le richieste del cliente, ed i rimanenti tubi confezionarli in un secondo momento utilizzando il modulo di rilavorazione ed attribuendo un nuovo lotto;
6) a fine settembre, se il fornitore non ha ancora fornito il materiale ordinato, e se le scorte a magazzino EGSpA so',N'CONSEGNA NUNC da 10, approvvigionamento sotto controllo, nessun interevento aggiuntivo da svolgere. - rap19-19_chiusura_rac,_corretto_approvvigionamento_nunc_da_10_posti.msg
al 16/07/19 il fornitore ha conseganto le scatole NUNC da 10, non sarà necessarioapportare alcuna modifica al confezionamento, pertanto non è necessaria alcuna notifica (cambiamento NON avvenuto). - rap19-19_mod05,36_non_significativa.pdf',NULL,N'25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019
25/06/2019',NULL,N'16/07/2019
16/07/2019
16/07/2019
16/07/2019
16/07/2019
16/07/2019
16/07/2019',N'No',N'se si valuta necessario sostituire un materiale di confezionamento di un prodotto è necessario aggiornare il DMRI.
16/07/2019 mail di ARENA (PW), consegna di scatole NUNC da 10 da parte de fornitore, approvvigionamento sotto controllo. DMRI non da aggiornare',N'No',N'se si valuta necessario sostituire un materiale di confezionamento di un prodotto è necessario informare il cliente sul nuovo pakaging provvisorio. 
16/07/2019 mail di ARENA (PW), consegna di scatole NUNC da 10 da parte de fornitore, approvvigionamento sotto controllo. ',N'No',N'nessun impatto',N'Attuare le attività volte a mantenere la continuità di fornitura.',N'A - Gestione degli Acquisti',N'A1 - Pianificazione approvvigionamento materiali',N'A1 "Pianficazione approvvigionamento materiali", non necessario nessun aggioramento',N'No',N'vedere MOD05,36, la notifica non è necessaria in quanto le scatole NUNC da 10 sono state consegnata mail di ARENA (PW) del 16/07/2019',N'No',N'necessario informare R-BIO se si valuta necessario sostituire un materiale di confezionamento di un prodotto è necessario informare il cliente sul nuovo pakaging provvisorio. 
16/07/2019 mail di ARENA (PW), consegna di scatole NUNC da 10 da parte de fornitore',N'Katia Arena',N'No',NULL,'2019-07-16 00:00:00',N'come da mail di Arena (PW) del 16/07/2019 le scatole NUNC da 10 sono state consegnate e l''approvvigionamento è tornato sotto controllo. Nessuna delle attività indicate nella richiesta di azionie corretttiva è da svolgere, si reputa pertanto possibile chiudere la RAC senza alcuna valutazione dell''efficacia.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-07-16 00:00:00','2019-07-16 00:00:00'),
    (N'19-17',N'Correttiva','2019-06-18 00:00:00',N'Federica Farinazzo',N'Sistema qualità',N'Reclamo',N'19-36',N'SCH mINT032SP200 
SCH mINT033SP1000 ',N'Manuale di istruzioni per l''uso',N'La presenza di volume residuo nel sonicator tube al termine della corsa, è causata da un uso improprio da parte del cliente poiché ha verosimilmente inserito un volume maggiore di campione di quello previsto.
Infatti lo strumento in caso di presenza di un volume superiore ai 200uL di campione,  aspira il volume presente fino a un massimo di 300 µL, lasciando l''eccesso nel sonicator tube. (Vedi report di indagine allegato).
Verificando le IFU della cartuccia di estrazione è emerso che l''indicazione relativa al volume da inserire nei sonicator tube è ambigua/fuorviante , potrebbe infatti indurre il cliente a trasferire volumi di campione superiori al previsto, poiché  200µL è indicato come "Volume minimo" anziché "Volume esatto".',N'Si chiede di correggere l''indicazione di  "Volume minimo" , al fine di evitare ambiguità ai clienti durante l''utilizzo dei sonicator tube: il trasferimento di un volume di estrazione maggiore ai 200µL cambia le concentrazioni degli acidi nucleici che si ottengono nell''eluato falsando le analisi quantitative in primis e qualitative (bassissimi positivi rilevati con un Ct inesatto).',NULL,N'Gli impatti sono relativi all''utilizzatore finale: l''utilizzo di un volume superiore nel sonicator tube (300 anziché 200 o 1100 anzichè 1000) può causare errori di quantificazione per prodotti quantitativi, la modifica permette di chiarire le modalità di utilizzo riducendo la possibilità di errori
Gli impatti sono relativi all''utilizzatore finale: il trasferimaneto di un volume superiore a quanto indicato nel sonicator tube (per esempio 300 anziché 200 µL o 1100 anzichè 1000 µL) può causare problemi nella procedura di estrazione con conseguenti errori di quantificazione del target per prodotti quantitativi o di rilevazione dei bassi titoli di target nei prodotti qualitativi.
Il chiarimento nel manuale riduce la possibilità di errori.
La stesura ed il riesame delle IFU sono state analizzate nel risk assessment IFU1 che prevde un livello alto di severità in caso di informazioni errate nel manuale di istrzuzioni per l''uso, dal 2016 ad ogni non ci sono stati reclami / nc la cui causa primaria è ',N'08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019',N'verifica delle IFU
Correzione del paragrafo "Preparazione dei campioni" nella "Procedura" delle IFU SCH mINT032SP200 e SCH mINT033SP1000.
nessuna attività da svolgere
Messa in uso nuova revisione IFU
aggiornamento FTP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'INT032SP200_06, INT033SP1000_01 - rac19-17_ifu_int032sp200_int033sp1000_rac19-17.msg
INT032SP200_06, INT033SP1000_01 - rac19-17_ifu_int032sp200_int033sp1000_rac19-17_1.msg
MESSA IN USO IFU INT032SP200_06, INT033SP1000_01 - rac19-17_ifu_int032sp200_int033sp1000_rac19-17_2.msg
IFU aggiornato all''indirizzo di rete. on essendo la IFU fisicamente presente nel FTP si reputa sufficiente tale azione. - rac19-17_l_ifuaggiornataeall_indirizzodirete.txt
La modifica non è significativa, pertanto non è da notificare - rac19-17_mod05,36_non_significativa.pdf',NULL,N'08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019
08/07/2019',NULL,N'18/07/2019
18/07/2019
08/07/2019
18/07/2019
07/11/2019
31/07/2019',N'No',N'Nessun impatto sul DMRI',N'No',N'la comunicazione al cliente è svolta con l''avvetezza rilasciata con le IFU',N'No',N'nessun impatto sul prodotto immesso in commercio',N'Nessun impatto sui requisiti regolatori',N'IFU - Gestione Manuali',N'IFU1 - stesura manuale',N'IFU1 nessun impatto',N'No',N'modifica non significativa veddere MOD05,36',N'No',N'modifica non significativa veddere MOD05,36',NULL,N'Si',NULL,'2020-05-30 00:00:00',N'Assenza reclami a distanza di 6 mesi dalla chiusura delle azioni.
a distanza di 6  mesi dalla chiusura azioni, non sono stati rilevati altri reclami con uguale causa primaria.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-11-07 00:00:00','2020-05-29 00:00:00'),
    (N'19-16',N'Correttiva','2019-06-14 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Non conformità',N'19-57',N'RTS400ING',N'Progetto',N'Ct risulta non determinato a causa di una errata interpretazione della curva di fluorescenza da parte del software nel del calcolo del Ct.',N'Richiesta la modifica dell''AP al fine di non generare risultati falsi negativi con MG ad alto titolo e possibilmente permettere il calcolo del Ct da parte del software anche in caso di curve di amplificazione con baseline negativa. 
L''azione correttiva volta ad eliminare la causa primaria della NC richiederebbe la modifica del software stesso ed è in discussione con PSS.',NULL,N'Il problema evidenziato ha un impatto sui risultati prodotti dal software ELITe InGenius. Gli Assay Protocol relativi al prodotto saranno modificati per quanto riguarda i parametri di calcolo del Ct del target MG. I risultati dei test di verifica e validazione dovranno essere rianalizzati con le nuove impostazioni per approvare la modifica dell''Assay Protocol.
La modifica degli Assay Protocol permette solo una mitigazione del problema in quanto si passa dal rischio di "falso negativo" ad un rischio di "errore" con Ct non calcolato o di una determinazione di Ct più tardivo rispetto al reale.
La soluzione definitiva del problema è la modifica dell''algoritmo di calcolo del Ct da parte di PSS che avrebbe impatti ancora maggiori richiedendo la validazione della nuova versione del software ELITe InGenius.
La modifica ha impatto positivo sul prodotto in quanto evita la diagnosi errata di falsi negativi, attraverso la segnalazione di un errore. Saranno analizzati i dati pregressi di Ricerca e Validazione al fin',N'14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019',N'Test con la macro Excel di PSS sui dati del CQ anomalo per identificare i nuovi parametri di analisi dei risultati del target MG.
Verifica dell''impatto della modifica rianalizzando con la macro Excel di PSS e con i nuovi parametri di analisi del target MG i dati di verifica e di validazione del prodotto.
Stesura del nuovo AP per gli utilizzatori e per il CQ.
Fornire i dati provenienti da database di validazione degli utilizzatori
Verifica degli AP
Aggiornamento delle TAB
Test estesi sui lotti di vendita e vVerifica dell''efficacia della modifica sui lotti rilasciati
Messa in uso nuovi AP

nessuna attività
Gestione con gli utilizzatori dell''FSN.
Pianificare installazione nuovi AP.
messa in uso AP
Aggiornamento FTP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Stesura FSN.
Notifica alle Autorità competenti.
Comunicazione agli utilizzatori.',N'report investigation and mitigation - rac19-16_reportonivestigationandmitigationforstiplus.pdf
AP rev01 - rac19-6_messa_in_uso__ap_sti_plus_rev.01_nc19-57;_rac19-16.msg
AP rev01 - rac19-6_messa_in_uso__ap_sti_plus_rev.01_nc19-57;_rac19-16_1.msg
TAB P32 revAB - rac19-16_elite_ingenius_-tab_p32_-_rev_ab_-__product_sti_plus_elite_mgb_notice.pdf
AP rev 01 U0519T e U0519BS - 2019-06-19_test_newap_u0519bt-u0519bs.pdf
AP rev 01 U0519BS e U0519BT sessione 1 - 2019-06-18_test_new_ap_u0519bs-u0519bt.pdf
FSN 19/01-ITA - 2019_06_19_avviso-di-sicurezza_19-01_sti_plus_it.docx
Elenco clienti - rac19-16_2019_06_20_sti_plus_customers.xlsx
Nuovi AP installati presso clienti - rac19-16_30-9-19_conclusione_ggiornamento_assay_protocol_presso_clienti_interessati.txt
AP rev01 - rac19-16_messa_in_uso__ap_sti_plus_rev.01_nc19-57;_rac19-16.msg
FTPAGGIORNATO - rac19-16_ftp_sezione1_rts400ing.pdf
La modifca dell''assay protocol non è significativa. Tuttavia la notifica presso i paesi cui il prodotto è registrato è fatta per mancato incidente. - rac19-16_mod05,36_non_significativa.pdf
FSN19-1_IT - 2019_06_19_fsn_19-01_ita_elitech_sti-plus.pdf
FSN19-1_FR - 2019_06_19_field_safety_notice_19-01_sti_plus_fr.pdf
FSN19-1_GER - 2019_06_19_field_safety_notice_19-01_sti_plus_ger.pdf
FSN19-1_EN - 2019_06_19_field_safety_notice_19-01_sti_plus_en.docx
NOTIFICA DEKRA - rac19-16_2019_06_19_dekra_vigilance_rep_notice_elitech_-_sti_plus.pdf
FSCA REPORT - rac19-16_2019_06_19_report_form_fsca_elitech_sti-plus_signed.pdf
REPORT INCIDENT - rac19-16_2019_06_19_report_form_incident_elitech_sti_plus_signed.pdf
Invio TAB p32 a distributori/ico - rac19-16_elite_ingenius_-tab_p32_-_rev_ab_-__product_sti_plus_elite_mgb_notice.pdf',NULL,N'14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019
14/06/2019',NULL,N'10/07/2019
09/07/2019
26/07/2019
14/06/2019
30/09/2019
10/07/2019
10/07/2019
26/06/2019
09/07/2019',N'No',N'La modifica non ha impatto sui DMRI di prodotto',N'Si',N'Inviato avviso sicurezza relativo ad azione correttiva in campo (FSN 19/01-ITA e FSN 19/01-ENG del 19/06/2019)',N'Si',N'Come da avviso sicurezza relativo ad azione correttiva in campo (FSN 19/01-ITA e FSN 19/01-ENG del 19/06/2019) paragrafo "Impatto sui test eseguiti in precedenza con il prodotto in oggetto e Azioni Immediate"',N'nessun impatto',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'L''azione correttiva volta ad eliminare la causa primaria della NC richiederebbe la modifica del software ELITeInGenius, in discussione con PSS. in caso di modifica del sw valutare l''aggiornamento del risk assessment.',N'Si',N'Effettuata, come da allegati',N'No',N'Non necessaria',NULL,N'No',NULL,'2020-09-30 00:00:00',N'Assenza reclami con uguale causa primaria su almeno 3 lotti.
Sui seguenti lotti non sono stati registrati reclami: U0919BI, U1219BD e U0320-029.
L''azione correttiva volta ad eliminare la causa primaria della NC ha richiesto la modifica del software ELITeInGenius, da parte di PSS (vedere anche RAC19-23); è previsto per 09/2020 il rilascio della versione 1.3.0.14, rimandata a fine ottobre 2020 con la messa in uso della versione 1.3.0.15.
La versione 1.3.0.15 è stata rilasciata il 9/11/2020, i cambiamenti apportati eliminano la causa primaria della nc, è pertanto possibile chiudere questa azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-09-30 00:00:00','2020-11-17 00:00:00'),
    (N'19-14',N'Correttiva','2019-06-10 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Reclamo',N'19-55',N'RTS130ING',N'Assay Protocol',N'In CQ, la reazione del Controllo Interno del lotto in oggetto presenta circa due Ct di ritardo rispetto alla razione del lotto utilizzato in sviluppo per la determinazione del Cut-off del controllo interno nei campioni negativi.
Il test di sensibilità del CI (4000 copie per reazione) nel CQ del lotto oggetto del reclamo mostra un valore medio di Ct = 31.60. Lo stesso test nel CQ dei lotti di sviluppo mostra un valore medio di Ct = 29.82.
Un ritardo di 2 Ct nella reazione di controllo interno, sommato alle altre variabilità collegate a questo test, può spiegare il superamento dell'' IC Cut-off dell''AP nei campioni negativi con conseguente risultato invalido.
Si veda nell''allegato del reclamo l''analisi dettagliata.  ',N'Si chiede la modifica del Ct limit per il controllo interno negli AP del prodotto, considerando la variabilità dei lotti e strumentale come evidenziato in indagini e tenendo conto del fatto che il prodotto è RUO.',NULL,N'La modifica ha impatti positivi perchè tiene conto delle prestazioni di lotti conformi ma meno performanti rispetto ai lotti di sviluppo.
La problematica in  oggetto è stata causata dalla diversa efficienza del Lotto di Sviluppo e del lotto corrente.
Il valore dell''IC Ct cut-off dovrà essere modificato (da 30 a  32) tenendo conto dei risultati ottenuti dal cliente con il lotto corrente del prodotto.
Impatto positivo in quanto il cliente con il nuovo assay protocol non replica l''errore descritto nel reclamo in oggetto (R19-55)
Impatto positivo in quanto il cliente con il nuovo assay protocol non replica l''errore descritto nel reclamo in oggetto (R19-55)
Nessun impatto.
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
nessun impatto se non in termini  di attività.',N'10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019',N'Nessuna azione
Modifica dell''AP per l''utilizzatore con valore dell''IC Ct cut-off  a  32.
Pianificare l''installazione della nuova versione dell''AP presso i clienti utilizzatori (vedere allegato)
verificare TAB ed inviarla approvata
nessun attività.
Messa in uso della nuova versione degli AP 
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornamento FTP
verifica assay protocol e aggiornamento TAB P29 revAA',N'ASSAY PROTOCOL rev01 - rac19-14_messa_in_uso_ap_open_prodotti_egspa_-_rac19-14_r19-55.msg
elenco clienti cui venduto il prodotto - rac19-14_vendite_rts130ing_hev.msg
Installare AP di HEV CE-IVD presso i clienti cui era installato l''AP per il prodtto RUO - rac19-14_installareap_hev_ce-ivd_presso_clienti_con_ap_ruo.txt
Stato AP al 17/03/2020: ancora presenti AP in versione 00 presso il cliente di Pordenone, tuttavia essendo il prodotto venduto, lotto U0918BO, scaduto a 31/12/2019, non si ritiene più necessario effettuare l''aggiornamento dell''AP. - rac19-14_statoassaysprotocols_installatipresoclienti.xlsx
 ASSAY PROTOCOL rev01  - rac19-14_messa_in_uso_ap_open_prodotti_egspa_-_rac19-14_r19-55_1.msg
La modifica non p significativa, pertanto non è da notificare. - rap19_14_mod05,36_non_significativa.pdf
assay protocol rev01 in uso al 11/06/19 - rac19-14_messa_in_uso_ap_open_prodotti_egspa_-_rac19-14_r19-55_2.msg
 tab in uso al9/7/19  - rac19-14_invio_-tab_p29_-_rev_ab_-_product_hev_elite_mgb_notice_of_change_1.msg
invio TAB 9/7/19 - rac19-14_invio_-tab_p29_-_rev_ab_-_product_hev_elite_mgb_notice_of_change.msg
immessi in commercio RTS130ING, STD130ING, CTR130ING il 25/10/2019 - rac19-14_25-10-19_immissioneincommerciohev_ce-ivd.txt',NULL,N'10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019
10/06/2019',NULL,N'10/06/2019
11/06/2019
17/03/2020
09/07/2019
10/06/2019
11/06/2019
26/06/2019
25/10/2019
09/07/2019',N'No',N'nessun impattp',N'No',N'installazione AP al primo intervento',N'No',N'nessun impatto',N'Il prodotto è RUO',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'non necessarioa, vedere MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-01-30 00:00:00',N'é stato immesso in commercio il nuovo prodotto CE-IVD il 25/10/2019, il prodotto RUO non è più fabbricato. 
Verificare l''installazione del AP CE-IVD presso i clienti cui era installato il RUO.
I lotti del codice articolo 962-RTS130ING forniti ai clienti, come da mail di SAD-OP del 4/06/2019, sono U0917AA scadenza al 31/12/2018 e U0918BO scadenza 31/12/2019. Entrambi i lotti sono scaduti. Come visibile dal file "RAC19-14_STATOAssaysProtocols_installatiPRESOclienti" al 17/03/2020 sono ancora presenti delle versioni obsolete di AP presso i clienti in cui il kit era stato venduto (in particolare "OSP. S.MARIA DEGLI ANGELI-PORDENONE"), tuttavia essendo il prodotto scaduto non si ritiene necessario effettuare l''aggiornamento del AP. La RAC può ritenersi chiusa.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2020-03-17 00:00:00','2020-03-17 00:00:00'),
    (N'19-13',N'Preventiva','2019-05-20 00:00:00',N'Roberta  Paviolo',N'Ricerca e sviluppo',NULL,NULL,NULL,N'Manuale di istruzioni per l''uso',N'I test di congelamento-scongelamento  e incubazione on-board su ELITe InGenius svolti sui prodotti in sviluppo Pneumocystis ELITe Standard (SCP2018-023) e HEV ELITe Standard (SCP2018-026) dimostrano che è possibile utilizzare l''aliquota del prodotto per un massimo di 4 sessioni (8 sessioni totali con il kit). Sulle IFU di prodotti simili (STDxxxPLD), già in commercio, è dichiarato che è possibile utilizzare l''aliquota per un massimo di 8 sessioni (16 sessioni totali con il kit) con le "Open Platform" e non è dichiarato nulla per l''uso in associazione all''ELITe InGenius. Si ritiene pertanto necessario aggiornare le indicazioni di utilizzo riportate sulle IFU dei prodotti simili (vedi Allegato 1) in modo da correggere la mancata indicazione sul numero di sessioni permesse in associazione allo strumento ELITe InGenius.
Inoltre è richiesto l''aumento di volume da 120 µL a 160 µL delle aliquote dei prodotti STDxxxPLD che prevedono un volume di utilizzo di 10 µL (vedi Allegato 1) per rendere più sicuro l'' uso in associazione allo strumento ELITe InGenius.
Un''ulteriore richiesta è uniformare l''indicazione del numero di sessioni permesse (4) in associazione allo strumento ELITe InGenius per alcuni prodotti CTRxxxPLD (vedi Allegato 1) e uniformare il numero di tubi (3) forniti per alcuni prodotti CTRxxxPLD (vedi Allegato 1) .
Per quanto riguarda la causa primaria del mancato aggiornamento delle IFU degli STDxxxPLD è attribuita a test non abbastanza approfonditi in fase di verifica e validazione durante le estensioni d''uso previsto su ELITe InGenius.',N'Riportare chiaramente sulle IFU di tutti i prodotti STDxxxPLD (vedi Allegato 1) l''indicazione di utilizzo di un massimo di 4 volte per tubo in modalità PCR ONLY (2 ore) in associazione allo strumento ELITe InGenius (numero di sessioni permesse).
Riportare chiaramente sulle IFU di alcuni i prodotti CTRxxxPLD (vedi Allegato 1) l''indicazione di utilizzo di un massimo di 4 volte per tubo in modalità EXTRACT+PCR (3 ore) in associazione allo strumento ELITe InGenius (numero di sessioni permesse). 
Modifica del volume di dispensazione per alcuni prodotti STDxxxPLD (vedi Allegato 1) portandolo da 120 µL a 160 µL .
Modifica del formato per alcuni prodotti CTRxxxPLD (vedi Allegato 1) portandolo da 1 tubo da 160 µL a 3 tubi da 160 µL, in modo da avviare un processo di standardizzazione del formato dei prodotti controlli positivi.',NULL,N'Impatto sullo sviluppo dei prodotti futuri: in fase di definizione dei requisiti, verificare il formato dei prodotti sulla base delle limitazioni imposte dagli strumenti. Eseguire sempre i test di simulazione d''uso on-board in fase di verifica e validazione.
Nessun impatto sul processo di CQ. Impatto temporaneo sulle attività di DS per adeguamneto moduli di produzione STD da portare a volume di 160 µL e CTR da confezionare con 3 tubi.
Le modifiche di formato dei prodotti STD da portare a volume di 160 µL e CTR da confezionare con 3 tubi ha impatto sul processo di produzione in termini di pianificazione dei quantitativi da produrre  e sui costi di materie prime e materiali di consumo.
Per la modifica di formato (portare da 120 µL a 160 µL la soluzione) dei 4 prodotti STD interessati (vedi Allegato 1) non è prevedibile un impatto significativo.
Per la modifica di formato (portare da 1 tubo a 3 tubi il confezionamento) dei 5 prodotti CTR interessati (vedi Allegato 1) è prevedibile un impatto signifi',N'20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019',N'Revisionare la procedura dei Controlli di Stabilità PR04,03 esplicitando meglio le modalità di esecuzione dei controlli on-board da eseguire in fase di verifica e validazione.
Adeguamento dei moduli di produzione STD da portare a volume di 160 µL e CTR da confezionare con 3 tubi e formazione al personale di produzione (vedere Allegato 1).
Allineamento con gli acquisti per la pianificazione delle quantità di materiali da ordinare.
Allinemaneto delle distinte base sul sistema gesitonale.
Messa in suo della documentazione aggiornata.
Aggiornamento del materiale di marketing (semplified IFU e presentazioni).
verifica ed eventuale aggiornamento del listino prezzi a seguito del ricalcolo dei costi di produzione
Aggiornamento del listino indicando:
- per gli STD (tutti) oltre alle sessioni per le open platform anche le sessioni per ELITe InGenius,
- per i 5 CTR  (vedi Allegato 1) che andranno modificati il nuovo numero di sessioni per le open platform e per ELITe InGenius,
- inoltre per gli RTS ',N'Messa in uso MODULI mod962STD037PLD_01, STD076_01, STD100PLD_01, STD176PLD_02 . - rap19-13_messa_in_uso_mod962-stdxxx_rap19-13_3.msg
Aggiornamento IFU STD020PLD_14, STD031PLD_16, STD032PLD_16, STD035PLD_12, STD038PLD_10, STD070PLD_12, STD175PLD_12 - rap19-13_ifu_std_elite_rap19-13_-i_tranche-.msg
aggiornamento IFU STD036PLD_12, STD078PLD_11 - rap19-13_ifu_std_elite_rap19-13_-ii_tranche-.msg
aggiornamento IFU STD015PLD_08 - rap19-13_ifu_std015pld_elite_rap19-13.msg
 aggiornamento IFU STD076PLD_02  - rap19-13_ifu_std076pld_rap19-13.msg
aggiornamento STD100PLD_01 e CTR100PLD_01 - rap19-13_ifu_std100pld_e_ctr100pld_rap19-13.msg
aggiornamento  IFU RTS076PLD e CTR076PLD - rap19-13_ifu_rts076pld_ctr076pld_26-02-2020.msg
aggiornamento IFU STD176PLD - rap19-13_ifu_std176pld_rap19-13_13-12-19.msg
aggiornamento IFU STD037PLD_04 - rap19-13_ifu_std037pld_rap19-13.msg
aggiornamento IFU CTR037PLD_04, CTR076PLD_02_CTR176PLD_11 - rap19-13_ifu_ctr_elite_rap19-13_-iii_tranche-.msg
CMV ELITe MGB Kit, HSV1 ELITe MGB Kit, HHV6 ELITe MGB Kit, HHV8 ELITe MGB Kit, Parvovirus B19 ELITe MGB Kit, Adenovirus ELITe MGB Kit, BKV ELITe MGB Kit		 		 HHV7 ELITe MGB KitT,  JCV ELITe MGB KiT - rap19-13_ifu_rts_aggiornate.msg
Le etichette corrispondenti ai prodotti la cui IFU è stata aggiornata sono state modificate con la nuova revisione della IFU - rap19-13_ifu_std_elite_rap19-13_-ii_tranche-.msg
Le etichette corrispondenti ai prodotti la cui IFU è stata aggiornata sono state modificate con la nuova revisione della IFU - rap19-13_ifu_std_elite_rap19-13_-i_tranche-.msg
Le etichette corrispondenti ai prodotti la cui IFU è stata aggiornata sono state modificate con la nuova revisione della IFU - rap19-13_ifu_ctr_elite_rap19-13_-iii_tranche-.msg
a 01-2020 tutte le etichette sono state rivalidata come da CC18-51 - rap19-13_a_01-2020_tutte_le_etichette_sono_state_rivalidata_come_da_cc18-51.txt
FTP_031PLD (RTS03PLD, CTR031PLD, STD031PLD) AGGIORNATO - rap19-13_sezione_1_product_technical_file_index_09.docx
 FTP_STD015PLD AGGIORNATO - rap19-13_sezione_1_product_technical_file_index_std015pld.txt
FTP020 (RTS020PLD, STD020PLD, CTR020PLD) AGGIORNATO - rap19-13_sezione_1_product_technical_file_index_std020pld.docx
FTP032PLD (RTS032, STD032PLD, CTR032) AGGIORNATO - rap19-13_sezione_1_product_technical_file_index_std032pld.docx
FTP035PLD (RTS035PLD, STD035PLD, CTR035PLD) - rap19-13_sezione_1_product_technical_file_index_std035pld.docx
 FTP078PLD (RTS078PLD, STD078PLD, CTR078PLD)  - rap19-13_sezione_1_product_technical_file_index_std078pld.docx
 FTP175PLD (RTS175PLD, STD175PLD, CTR175PLD)  - rap19-13_sezione_1_product_technical_file_index_std175pld.docx
Dal 31/10/2019 è sufficiente la messa in uso delle IFU, senza integazione del FTP. - rap19-13_indicazioni_dekra.txt
La modifica è significatova pertanto da notificare laddove il prodotto è registrato. - rap19-13_mod05,36_significativa_soloperstd.pdf
Notifica a DEKRA - rap19-13_elitechgroup_spa_notice_of_change_dekra.msg
effettuata notifica nei paesi in cui i prodotti sono registrati - rap19-13__notification_of_change_-stdxxxpld_ctrxxxpld-_rap19-13_23-12-19.msg
Messa in uso MODULI mod962STD037PLD_01, STD076_01, STD100PLD_01, STD176PLD_02 . MOD18,02 del8/07/2019 - rap19-13_messa_in_uso_mod962-stdxxx_rap19-13.msg
Non è stato approvata la modifica ai CTR, pertanto i moduli NON sono da modificare. - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati.msg
 La modifica del numero dei tubi dei CTR non è stata approvata, non variano le quantità da  ordinare - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_3.msg
distinte base aggiornate - rap19-13_messa_in_uso_mod962-stdxxx_rap19-13_1.msg
PR04,03_02 e modili collegati - rap19-13__messa_in_uso_pr0403_02_doc_collegati_prdyyyxxx_mrdyyyxxx_rap19-13_rap18-18.msg
La modifica del numero dei tubi dei CTR non è stata approvata, non variano le quantità da ordinare  - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_7.msg
 distinte base aggiornate  - rap19-13_messa_in_uso_mod962-stdxxx_rap19-13_2.msg
costi STD approvati - rap19-13_modifica_volumi_dispensazione_std_approvazione_costi.msg
costi CTR non approvati - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_8.msg
La modifica del numero dei tubi dei CTR non è stata approvata, i materiali di MKT non sono da aggiornare - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_5.msg
 La modifica del numero dei tubi dei CTR non è stata approvata, i conteggi in gara NON variano  - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_2.msg
messa in uso ifu I tranche - rap19-13_ifu_std_elite_rap19-13_-i_tranche-_1.msg
messa in uso ifu II tranche - rap19-13_ifu_std_elite_rap19-13_-ii_tranche-_1.msg
invio TAB HSV1 - rap19-13_tab_p01_-_rev_ab_-_product_hsv1_elite_mgb.msg
invio TAB HSV2 - rap19-13_tab_p07_rev_ab_-_product_hsv2_elite_mgb.msg
invio TAB HHV8 - rap19-13_tab_p18_-_rev_ab_-_product_hhv8_elite_mgb_notice.msg
invio TAB ADENO - rap19-13_tab_p10_rev_ac_-_product_adv_elite_mgb_notice.msg
invio TAB BKV - rap19-13_elite_ingenius_-tab_p02_-_rev_ac_-_product_bkv_elite_mgb_notice.msg
La modifica del numero dei tubi dei CTR non è stata approvata, i conteggi in gara NON variano - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_1.msg
 La modifica del numero dei tubi dei CTR non è stata approvata, i materiali di MKT non sono da aggiornare  - rap19-13_modifica_numero_tubi_ctr_costi_non_approvati_6.msg',NULL,N'20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019
20/05/2019',NULL,N'25/10/2019
31/07/2019
22/07/2019
22/07/2019
22/07/2019
13/05/2020
13/05/2020
28/05/2021
31/01/2020
31/10/2019
23/12/2019
31/07/2019
31/07/2019
31/07/2019
20/05/2019
20/05/2019
13/05/2020',N'No',N'nessun impatto sui DMRI',N'No',N'effettuare comunicazione al cliente attraverso avvertenza rilasciata con IFU',N'No',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'R&D1.10',N'Si',N'MOD05,36 modifica significativa',N'No',N'MOD05,36 modifica non significativa per R-Biopharma',N'Guglielmo Stefanuto',N'Si',N'vedere allegato allegato con elenco prodotti','2021-05-28 00:00:00',N'Verificare assenza reclami
05/2020: la messa in uso delle IFU di STD e CTR con le corrette indicazioni sul numero di sessioni da svolgere ha effettivamente prevenuto l''insorgenza di reclami. Permane un''attività residua di allineamento delle IFU dei prodotti RTS.
05/2021: con la messa in uso delle IFU degli RTS si ritiene di chiudere questa azione preventiva, anche se permane l''aggiornamento di alcune IFU in lingue differenti da IT e EN.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2021-05-28 00:00:00','2021-05-28 00:00:00'),
    (N'19-10',N'Correttiva','2019-04-15 00:00:00',N'Stefania Brun',N'Validazioni',N'Reclamo',NULL,N'Eliteware ordinamento campioni all''interno della lista di lavoro',N'Documentazione SGQ',N'Dall''analisi dei log files Eliteware invia allo strumento InGenius una lista di test con ordine diverso da quella impostata dall''operatore. La lista proveniente da Eliware e ricevuta da InGenius viene mostrata a GUI con lo stesso ordine.
Appare quindi che ''a monte'' Eliteware rielabori la lista compilata dall''utente, la riorganizzi e poi la invii allo strumento.
L''evento "query all" non è stato collaudato dagli specialisti Lutech durante la messa in servizio del software ELITe Ware, poiché al momento dell''installazione non era una funzione disponibile sullo strumento InGenius, lo è diventata a seguito dell''aggiornamento del software strumentale
(versione 1.3) effettuata, in autonomia, da ELITech nel gennaio 2019. La causa primaria sembra imputabile ad un''errata valutazione degli impatti del cambiamento dalla versione 1.2 alla versione 1.3 del SW Ingenius con l''attivazione della funzione "query all" in associazione ad Eliteware.
L''evento si verifica se sussistono contemporaneamente queste 4 condizioni:
- Query All impostata;
- Stato provetta "incompleta"(la provetta viene caricata su uno strumento per effettuare esami "parziali" e successivamente viene caricata su altro strumento per essere "completata");
- Necessità di mettere in ripetizione uno o più test della provetta, senza rimuovere il test dalla lista originale (creando una doppia lista);
- Si avviano gli esami per il "completamento" (su altro strumento) precedentemente alla ripetizione; ',N'1. Disabilitare la funzione di "query all" che può innescare la trasmissione dei campioni a InGenius in un ordine differente
rispetto da quello proposto su ELITe Ware.
2. Sensibilizzazione degli operatori sul corretto modo di operare, in particolare sui seguenti aspetti:
- Controllo dell''ordine di caricamento dei campioni dello strumento.
- Rimozione dalla seduta dei campioni da rieseguire.
3. Aggiornamento del manuale utente, ponendo maggiore accento sul corretto flusso di lavoro di fase di programmazione delle sedute.
4. Aggiornamento da parte del fornitore Lutech del sw EliteWare per evitare che la problematica si ripresenti
5. Validazione della nuova versione del sw EliteWare in associazione al SW 1.3 InGenius e al LIS
',NULL,N'Positivo in quanto la correzione del SW Eliteware da parte del fornitore Lutech permette anche in presenza della funzione query all attiva di prevenire l''ordinamento errato dei campioni all''interno della lista di lavoro
Positivo in quanto la correzione del bug del SW EliteWare permette di evitare l''ordinamento errato dei campioni all''interno della lista di lavoro inviata al LIS che potrebbe comportare un''errata diagnosi con conseguente notifica all''Autorità Competente
Positivo in quanto la correzione del bug del SW EliteWare permette di evitare l''ordinamento errato dei campioni all''interno della lista di lavoro inviata al LIS che potrebbe comportare un''errata diagnosi con conseguente notifica all''Autorità Competente
 Positivo in quanto la versione modificata dell''ELITEware preverrà il rischio di ordinamento casuale dei campioni
 Positivo in quanto la versione modificata dell''ELITEware preverrà il rischio di ordinamento casuale dei campioni
 Positivo in quanto la versione modificata dell''ELITEwar',N'15/04/2019
15/04/2019
15/04/2019
19/04/2019
19/04/2019
19/04/2019',N'Validazione della nuova versione del sw EliteWare con la correzione del bug in associazione al SW 1.3 InGenius e al LIS
Invio notifica ai clienti che hanno installato EliteWare collegato al LIS in associazione a più di 1 strumento
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Formazione utilizzatori  su nuova versione ELITeware v.1.0.3 da parte di Lutech
Revisione del Manuale utente ELITeware da parte del fornitore Lutech, ora in revisione 2 per Modifica paragrafi gestione ripetizioni e
programmazione InGenius',N'lutech_elitEwAREvALIDATIONsTATUS - rac19-10_054-eli-v&v99__validation_status.pdf
Validation report EGSpA - rac19-10_rep2019-029_elite_ingenius_lis__connectivity_validation_report_code_int030_scp2016-028_compliant_19-22_00_signed.pdf
E-mail nota tecnica  - rac19-10_nota_tecnica_eliteware_elenco_laboratori.msg
Nota tecnica n°19-1 ITA - rac19-10_2019_03_25_nota_tecnica_eliteware_originale-ita.pdf
Nota tecnica n°19-1 EN - rac19-10_2019_03_25_nota_tecnica_eliteware_originale-en.pdf
Attestato formazione cliente Bologna - attestato_corso_eliteware_bologna-20190417102339_1.pdf
Nuova revisione Manuale ELITEware - ew-umi_elite_ware_manuale_utente_rev2.pdf
La modifica NON è significativa, si ritiene pertanto non necessario effettuare aluna notifica presso le autorità comptenti. - rac19-10_mod05,36_non_significativa.pdf',NULL,N'15/04/2019
15/04/2019
15/04/2019
19/04/2019
19/04/2019',NULL,N'15/04/2019
26/03/2019
26/06/2019
19/04/2019
19/04/2019',N'No',N'nessun impatto',N'Si',N'Inviata nota alla lista di clienti fornita da CSC',N'Si',N'Si in quanto l''avviso inviato ai clienti è finalizzato a disabilitare la funzione "query all" che potrebbe comportare un ordinamento errato e/o ribadire la necessità di verifica della lista di lavoro prima della validazione e conseguente invio al LIS',N'Nessun impatto',N'GM - Gestione delle modifiche',N'GM5 - Verifica/individuazione ed approvazione  degli impatti',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Clelia  Ramello',N'No',NULL,'2019-12-27 00:00:00',N'Verifica assenza di reclami imputabile ad un ordinamento errato della lista di lavoro da parte del EliteWare. 
Nei 6 mesi successivi alla chiusura delle attività non ci sono stati reclami con la stessa causa primaria.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-06-26 00:00:00','2020-01-07 00:00:00'),
    (N'19-7',N'Correttiva','2019-03-25 00:00:00',N'Federica Farinazzo',N'Validazioni',N'Reclamo',N'19-29',N'RTS020PLD',N'Manuale di istruzioni per l''uso',N'Il software Ingenius non usa le caselle dell''interpretation model in cui sono riportati i valori espressi in IU/mL, riferiti a LLOD LLOQ ULOQ, ma effettua il calcolo partendo dalle copie/mL e moltiplicando per il Fc. La differenza di arrotondamento dei decimali è la causa della discrepanza nei valori ottenuti. Attualmente nel software non è però possibile inserire numeri con decimali per il calcolo, l''eventuale implementazione sarà valutata in una revisione successiva del SW, in quanto non è stato valutato adeguatamente nei requisiti del SW 1.3.',N'Modifica dei valori di LoD LLoQ e ULoQ in UI/M, relativi al sistema Ingenius, riportati nelle IFU di EBV. 
Verfica ed eventuale modifica, degli altri target  con esito in UI/mL: CMV, EBV, BKV, JCV e ParvoB19.
La modifica consentirebbe l''uniformità di dati tra quelli calcolati dal Software e quelli riportati sulle IFU. 
Sarà allegata alla RAC l''elenco dei cambiamenti dei target impattati.
Per evitare che si verifichino altre situazioni analoghe prevedere nella procedura di validazione la verifica della corrispondenza dei dati tra IFU e SW per le UI/ml.',NULL,N'L''istruzione operativa di creazione degli assay protocol (IO04,08) necessità di una modifica al fine di verificare la corrispondenza dei dati tra IFU e SW per le UI/ml. 
L''impatto è positivo in quanto corregge le segnalazioni dei clienti e previene future segnalazioni.
L''impatto è positivo in quanto corregge le segnalazioni dei clienti e previene future segnalazioni.
Non c''è impatto sul cliente ricevente il materiale di marketing in quanto l''uso dei prodotti avviene sempre secondo l''IFU, si ritiene comunque necessario aggiornare i documenti riportanti dati contrastanti
Nessun impatto.
Nessun impatto.
Impatto positivo, la correzione delle IFU e l''introduzione in procedura del controllo dei dati tra IFU e SW, ridurrà il numero di segnalazioni da parte del cliente. 
Impatto positivo, la correzione delle IFU e l''introduzione in procedura del controllo dei dati tra IFU e SW, ridurrà il numero di segnalazioni da parte del cliente. 
Impatto positivo, la correzione delle IFU e l''introduzione in ',N'07/05/2019
07/05/2019
07/05/2019
07/05/2019
25/03/2019
25/03/2019
07/05/2019
07/05/2019
07/05/2019',N'Verifica dei target per cui sono disponibili le IU (CMV, EBV, BKV, JCV e ParvoB19) e conseguente modifica delle IFU 
Aggiornamento del MOD05,35
Modifica dell''istruzione operativa di creazione degli assay protocol (IO04,08) al fine di verificare la corrispondenza dei dati tra IFU e SW per le UI/ml. 
Supporto nella verifica dei target impattati (CMV, EBV, BKV, JCV e ParvoB19) e successiva approvazione delle IFU corrette
Aggiornamento / verifica del MOD05,35
Aggiornamento materiale di marketing
nessuna attività
Messa in uso IFU
Agggiornamento FTP  (i target impattati sono CMV, EBV, BKV, JCV e ParvoB19).
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica (i target impattati sono CMV, EBV, BKV, JCV e ParvoB19).',N'Verifica IFU RTK015PLD - ra19-7_messa_in_uso_ifu_rtk015pld.pdf
verifica IFU RTS020PLD - ra19-7_messa_in_uso_ifu_rts020pld.pdf
verifica IFU RTS175PLD. Per gli analiti JCV e PARVOB19 non è necessario effettuare la modifica - ra19-7_messa_in_uso_ifu_rts175pld.pdf
mod05,35 AGGIORNATO - rac19-7_copia_mod05,35_00_lis_assay_parameter_2019-06-12.xlsx
IO04,08_01 - rac19-7_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919.msg
 mod05,35 AGGIORNATO  - rac19-7_copia_mod05,35_00_lis_assay_parameter_2019-06-12_1.xlsx
Tabella modifiche e comunicazioni con PSS - r_sw_ingenius_1.0_and_iu_calculation_1.msg
Modifiche UI/mL per Target  - tabella_modifiche.xlsx
IN USO IFU RTK015PLD rev14 - ra19-7_messa_in_uso_ifu_rtk015pld_1.pdf
IN USO IFU RTS020PLD rev16 - ra19-7_messa_in_uso_ifu_rts020pld_1.pdf
IN USO IFU RTS175PLD rev15 - ra19-7_messa_in_uso_ifu_rts175pld_1.pdf
La modifica non  è significativa, pertanto non p necessario effettuare notifiche presso le autorità competenti, o a fornitori OEM. - rac19-7_mod05,36_non_significativa.pdf
FTP RTK015PLD AGGIORNATO - rap19-7_sezione_1_product_technical_file_index_rtk015pld.txt
FTP_02 (RTS020PLD, CTR020PLD, STD020PLD) AGGIORNATO - rap19-7_sezione_1_product_technical_file_index_rts020pld.docx
FTP_175 (RTS175PLD, CTR175PLD, STD175PLD) AGGIORNATO  - ra19-7_messa_in_uso_ifu_rts175pld.pdf
FTP PARVO E JCV sono aggiornati attraverso l''aggiornamento del IFU - rac19-7_gliftpdiparvoejcvnondevonoessereaggiornatiinquantoesufficientel_aggiornamentodellaifu.txt',NULL,N'07/05/2019
07/05/2019
07/05/2019
25/03/2019
07/05/2019
07/05/2019
07/05/2019',NULL,N'13/09/2019
12/06/2019
17/12/2019
25/03/2019
12/06/2019
17/12/2019
26/06/2019',N'No',N'nessun impatto sul DMRI in quanto si tratta di un allineamento dei valori in UI/mL per il limite di rilevazione LoD, limiti di quantificazione ULoQ e LLoQ  rispetto a quelli sul SW Ingenius che quindi comporta una nuova revisione delle IFU',N'Si',N'17.07.2019 CCA ha contatto i clienti che hanno lamentato il problema (rif. R18-117 e R19-29) per comunicare disponibilità delle nuove versioni delle IFU che integrano le correzioni relative ai valori in UI/mL per il limite di rilevazione LoD, limiti di quantificazione ULoQ e LLoQ ',N'Si',N'Comunicato attraverso TAB',N'na',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL3 - Fase di Verifica, Validazione e Rilascio sul Mercato: attività',N'nessun impatto',N'No',N'na',N'No',N'na ',NULL,N'No',NULL,'2020-11-30 00:00:00',N'Assenza di reclami per mancata corrispondenza dei dati tra IFU e SW per le UI/ml.
Al 17/11/2020 non si registrano reclami imputabili a mancata corrispondenza tra IFU e sw per le UI/ml (tutti i prodotti individuati erano stati corretti, l''aggiornamento dell''istruzione operativa IO04,08 e la sua applicazione hanno eliminato il ripetersi di errori imputabili alle UI7ml).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-12-17 00:00:00','2020-11-17 00:00:00'),
    (N'19-5',N'Preventiva','2019-02-15 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'I set genetici dei campioni NIBSC sono stati confermati per i gli altri 2 set genetici, con i prodotti Alert RTSD01-V, RTSD01-II e RTSD08. (vedi allegato CARATTERIZZAZIONE CAMPIONI NIBSC: REAL TIME ALERT). Inoltre la caratterizzazione è confermata dai dati di monitoraggio dei lotti del prodotto di RTSD00ING (vedi RTSD00ING-Monitoraggio  )',N'Modifica della Verifica tecnica MOD10,12-RTD00ING, valutando le Tm di tutti e 3 i set genetici per tutti i campioni NIBSC.
 Al fine di monitorare i lotti di produzione si vuole inoltre inserire nella Vt i criteri di monitoraggio, come già fatto per i prodotti Fast Track, al fine di poter avviare azioni in caso di deriva dei criteri.  (vedi allegato MOD10,12-RTSD00ING-draft).',NULL,N'La modifica permette di ampliare il numero di campioni analizzabili durante il CQ migliroando la valutazione delle performance del lotto. I criteri di monitoraggio permettono di segnalare eventali anomali nel tempo.
La modifica permette di ampliare il numero di campioni analizzabili durante il CQ migliroando la valutazione delle performance del lotto. I criteri di monitoraggio permettono di segnalare eventali anomali nel tempo.
Il processo di CQ per il prodotto risulterebbe migliore anceh da un punto di vista di deriva dei lotti
Il processo di CQ per il prodotto risulterebbe migliore anceh da un punto di vista di deriva dei lotti',N'15/02/2019
15/02/2019
15/02/2019
15/02/2019',N'Modifica della Scheda Appunti: MOD10,13-RTSD00ING-CTRD00ING, per cambio disposizione campioni da analizzare
Modifica della Verifica Tecnica: MOD10,12-RTSD00ING-CTRD00ING.
Approvazione delle modifiche. 
seguire la Formazione delle modifiche
messa in uso nuoo documento
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'MESSA IN USO MODULI, mod18,02 DEL 7/5/19  - rap19-5_messa_in_uso_modulin_rap19-5_mod1802_del_7519_1.msg
MESSA IN USO MODULI, mod18,02 DEL 7/5/19  - rap19-5_messa_in_uso_modulin_rap19-5_mod1802_del_7519_2.msg
MESSA IN USO MODULI, mod18,02 DEL 7/5/19 - rap19-5_messa_in_uso_modulin_rap19-5_mod1802_del_7519.msg
La modifica NON è significativa, NON è necessario effettuare la notifica nei paesi in cui il prodotto è registrato. - rac19-5_mod05-36_9-4-19.pdf',NULL,N'15/02/2019
15/02/2019
15/02/2019
15/02/2019',NULL,N'22/05/2019
22/05/2019
22/05/2019
09/04/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'nessun impatto.',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'na',N'No',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-01-30 00:00:00',N'Verificare il monitoraggio di tutti i 3 set genetici per tutti i campioni NIBSC su almeno 3 lotti.
Di seguito 3 lotti di RTSD00ING U0619AF, U0919BJ, U0120-044 ed i lotti dei NIBSC utilizzati:
FII WT: P0718AA-wt
FII HET: P0718AC-het
FII MU: P0718AB-mu

FV WT: P0818AC-wt, P1019CX-wt
FV HET: P0818AD-het,  P1019BS-het
FV MU: P0818AB-mu, P1019BR-mu.
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-05-22 00:00:00','2020-03-17 00:00:00'),
    (N'19-4',N'Preventiva','2019-02-15 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'I campioni ad uso CQ possono essere trattati in modo distinto dal prodotto finito per quel che concerne l''esito quando non indicativi delle performance del prodotto.
La pianificazione di produzione non è in grado di sincronizzare sempre i lotti di CTR e MIX.',N'Distinzione della valutazione rispetto alle specifiche delle diluizioni per uso CQ rispetto al CTR e introduzione del controllo delle diluizioni per i prodotti in cui non erano previsti (vedi elenco allegato nelle azioni CQ)',NULL,N'La modifica permete di evitare false non conformità per i prodotti quando non sono superate le diluizioni uso CQ o per le diluizioni che ad oggi potrebbero essere messe in uso senza una verifica, nei casi di invio al CQ del CTr e diluizione senza il lotto di MIX 
La modifica è migliorativa in quanto permette di non avere false non conformità di prodotto e conseguenti ripetizioni e gestione du database SGQ 
La modifica è migliorativa in quanto permette di non avere false non conformità di prodotto e conseguenti ripetizioni e gestione du database SGQ',N'15/02/2019
15/02/2019
15/02/2019',N'modifica dei moduli CQ (vedi allegato)
formazione
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
',N'elenco moduli impattati - rap19-03_dil.xlsx
Messa in uso dei moduli CQ come da pianificazione. MOD18,02 del 20/06/2019  - rap19-4_messa_in_uso_modxx_rap19-4_rac19-15.msg
 Messa in uso dei moduli CQ come da pianificazione - rap19-4_messa_in_uso_modxx_rap19-4_rac19-15_1.msg
 La modifica NON è significativa, pertanto non è necessario effettuare notifiche presso le autorità competenti.  - rac19-4_mod05,36_non_significativa.pdf',NULL,N'15/02/2019
15/02/2019
15/02/2019',NULL,N'09/07/2019
09/07/2019
09/04/2019',N'No',N'nessun impatto sul DMRI',N'No',N'non necessaria alcuna comunicazione',N'No',N'nessun impatto sul prodotto immesso in commercio',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'modifica non significativa vedere MOD05,36',N'No',N'modifica non significativa vedere MOD05,36',NULL,N'No',NULL,'2020-01-31 00:00:00',N'Assenza NC su 3 lotti con diluizioni per uso CQ.
I lotti con diluizioni per uso CQ elencati di seguiti sono conformi:
CTR200ING: U1019BC (MOD10,12_RTS200ING-CTR200ING_05),
CTR201ING: U0120-10 (MOD10,12_RTS201ING-CTR201ING_01),
955-pMYCO-P-L-TRI5: P0220-114 (MOD10,12-955-pXXX-Qual_04)',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-07-09 00:00:00','2020-03-17 00:00:00'),
    (N'19-2',N'Correttiva','2019-02-13 00:00:00',N'Federica Farinazzo',N'Ricerca e sviluppo',N'Non conformità',N'19-7',N'IC500 e RTS523ING',N'Assay Protocol',N'In base alle indagini eseguite, la causa della NC ottenuta con il prodotto IC500 in associazione al prodotto RTS523ING è collegata alla minor efficienza di amplificazione del prodotto RTS523ING con il sistema ELITe InGenius  rispetto al sistema AB 7500 utilizzato da FT e con il quale il fornitore ha stabilito le specifiche. Quando i prodotti presentano prestazioni non ottimali rispetto alle specifiche di FT, con il sistema ELITe InGenius  gli stessi lotti causano risultati non validi o non determinati. La causa della minore efficienza tra i due sistemi è collegata anche al ciclo termico: l''utilizzo di un ciclo termico NON FAST aumenta la sensibilità dell''amplificazione.
Si segnala inoltre che, in base al montoraggio dei risultati dei CQ eseguiti sui lotti dei prodotti Fast Track, è necessario valutare anche le prestazioni di RTS533ING e RTS507ING, con possibile modifica degli AP (ciclo termico o cut-off).',N'Miglioramento delle prestazioni dei prodotti RTS523ING + IC500 in associazione al sistema ELITe InGenius. In particolare si può tentare una ottimizzazione del ciclo termico o, in alternativa, all''utilizzo dell''IC500 a una concentrazione più elevata. 
Aggiornamento dell''8/07/19: La richiesta di modifica viene ampliata anche alle criticità rilevate con il reclamo 18-76   PERFORMANCE - Falsi negativi per Parechovirus, chiedendo una valutazione delle modifiche dell''Assay Cross-talk Matrix per ottimizzare la fluorescenza anche del canale Parechovirus.',NULL,N'Una modifica del ciclo termico a livello della fase di annealing/extension o numero di cicli avrebbe un forte impatto sulle prestazioni dei prodotti e potrebbe richiedere una rivalidazione. Modifiche della fase di trascrizione inversa o denaturazione (già effettuate da FTD per i suoi kit corrispondenti agli OEM EG SpA) possono essere ritenute accettabili senza necessità di una rivalidazione.
La modifica può impattare i prodotti e richiedere una rivalidazione, in base ai risultati dei test con i vari cicli termici. Modifiche minime possono essere reputate accettabili.
Nessun impatto se non in termini di carico ore lavoro.
La modifica è positiva per la valutazione del prodotto perchè permette di accettare i lotti in linea con le specifiche del fornitore
La modifica è positiva per la valutazione del prodotto perchè permette di accettare i lotti in linea con le specifiche del fornitore
La modifica può impattare i prodotti e richiedere una rivalidazione, in base ai risultati dei test con i vari c',N'19/02/2019
19/02/2019
19/02/2019
12/03/2019
12/03/2019
19/02/2019
19/02/2019
19/02/2019',N'Modifica del ciclo termico nella fase di trascrizione inversa o denaturazione, test di valutazione del ciclo termico ottimale e successiva modifica e verifica AP cliente e AP CQ.
In caso di modifica della fase di annealing/extension, valutazione degli impatti sulla validazione del prodotto ed eventuale rivalidazione con modifica IFU. 
Aggiornamento del 17/04/2019:
Valutazione della modifica dei parametri della dell''Assay Cross-talk Matrix per ottimizzareare la fluorescenza del canale IC e successiva modifica e verifica AP cliente e AP CQ.
Aggiornamento dell''8/07/2019
In riferimento al reclamo 18-76   PERFORMANCE - Falsi negativi per RTS523ING: Valutazione della modifica dei parametri della dell''Assay Cross-talk Matrix per ottimizzare la fluorescenza del canale Parechovirus successiva modifica e verifica AP cliente e AP CQ 

Verifica AP.
Valutazione degli impatti sulla validazione del prodotto ed eventuale rivalidazione (con modifica IFU), 
TAB
aggiornamento del 17/04/2019
Valutazione della modific',N'AP rev03 approvato - rac19-2_assay_protocol_mv2_rev.03_rac19-2.msg
AP rev03 in uso al 21/02/19 - rac19-2_assay_protocol_mv2_rev.03_rac19-2_1.msg
AP rev04 in uso - rac19-2_ap_rts507ing_rap19-22_e_rts523ing_rac19-2.msg
Esito CQ_IC500_U0119AB - esito_cq_ic500_u0119ab_rac19-2.pdf
risultai con nuovo ap - rac19-2_rts523ing_nuovoap_risultati.pdf
cocnlusione attività - rac19-2_conclusione_attivita.msg
ri-validazione non necessaria - rac19-2_non_necessaria_ri-validazione.txt',NULL,N'19/02/2019
19/02/2019
19/02/2019
12/03/2019
19/02/2019
19/02/2019
19/02/2019',NULL,N'24/10/2019
24/10/2019
24/10/2019
31/12/2019
13/01/2021
09/12/2019
13/01/2021',N'No',N'non necessario',N'No',N'non necessaria',N'No',N'nessun impatto',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'valutare',N'No',N'non necessaria, vedere MOD05,36',N'No',N'non necessaria',NULL,N'No',NULL,'2019-04-17 00:00:00',N'Efficacia negativa, le attività proposte non sono state sufficienti ad eliminare la causa primaria di NC. Al 17/04/2019 si richiede l''itegrazione della RAC con l''aggiunta di altre attività',N'Federica Farinazzo',N'Negativo',N'Valutazione della modifica del settaggio di cross talk per aumentare la fluorescenza finale
Aggiornamento AP cliente + AP CQ
Valutazione degli impatti sulla validazione del prodotto ed eventuale rivalidazione (con modifica IFU).
installazione nuovo AP e Test dei lotti non conformi con il nuovo AP
messa in uso nuovo AP: e-mail del 24/10/2019
creazione ed invio TAB (ELITe InGenius -TAB P05 - Rev AC -  Product MV2 ELITe MGB Panel notice), inviata il 18/10/2019
Ri-verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica (non necessario, gli AP non necessitano di notifica, è sufficeinte la TAB).
Tutte le attività sopracitate sono state svolte a 9/12/2019.

Individuazione dei clienti che utilizzano il prodotto, pianificazione ed aggiornamento NUOVO AP presso gli stessi.

Gennaio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva',N'30/06/2020',N'Positivo',NULL,'2021-01-13 00:00:00','2021-01-13 00:00:00'),
    (N'18-40',N'Preventiva','2018-12-20 00:00:00',N'Renata  Catalano',N'Audit esterni',NULL,NULL,NULL,N'Documentazione SGQ',N'La documentazione di progettazione prevista dalle procedure PR04,01 e PR04,04 per quanto riguarda la definizione dei requisiti di progetto/prodotto e lo stato di aggiornamento della pianificazione di progetto è carente rispetto a quanto previsto dalla 13485:2016 e dai Regolamenti relativi all''MDSAP. Alcuni aspetti, evidenziati dalle Osservazioni e dai Remarks riportati nella "Descrizione origine", andrebbero trattati in maniera più approfondita in modo che la documentazione di progetto prodotta sia in grado di soddisfare in maniera completa ed esaustiva i requisiti normativi. ',N'Presentare ad ogni Riesame di progettazione una revisione aggiornata del DHFI e della Gantt di Progetto
Aggiornare i documenti di definizione dei requisiti in modo che trattino in maniera più dettagliata ed esaustiva i requisiti di sicurezza e usability e diano evidenza di aver considerato la gestione dei rischi come input di progettazione ',NULL,N'Impatto positivo: maggior dettaglio nella definizione dei requisiti di usability e sicurezza e nell''utilizzo dell''output della gestione dei rischi come input di progettazione garantisce una più solida definizione dei requisiti di progetto/prodotto
Maggior tempo richiesto per la stesura della documentazione di definizione dei requisiti e pianificazione
Impatto positivo: maggior dettaglio nella definizione dei requisiti di usability e sicurezza e nell''utilizzo dell''output della gestione dei rischi come input di progettazione garantisce una più solida definizione dei requisiti di progetto/prodotto. L''aggiornamento ad ogni Riesame di Gantt e DHFI consente un monitoraggio dello stato di avanzamento del progetto più efficace.
Maggior tempo richiesto per la stesura della documentazione di definizione dei requisiti e pianificazione
Impatto positivo: maggior dettaglio nella definizione dei requisiti di usability e sicurezza e nell''utilizzo dell''output della gestione dei rischi come input di progettazione gara',N'20/12/2018
20/12/2018
20/12/2018
20/12/2018',N'Revisione contenuti PRD (progetti nuovo sviluppo) e Piano Generale di Verifica e Validazione (progetti di manutenzione di prodotto) per trattare in maniera più approfondita i requisiti di sicurezza e usability e integrare l''output della documentazione di gestione dei rischi nella definizione dei requisiti di progettazione.
Creazione di una revisione aggiornata di DHFI e Gantt da consegnare in ciascun Riesame (progetti nuovo sviluppo e manutenzione di prodotto in cui RT è Project Leader)
Revisione contenuti MRD (progetti nuovo sviluppo, OEM) per trattare in maniera più approfondita i requisiti di usability.
Creazione di una revisione aggiornata di DHFI e Gantt da consegnare in ciascun Riesame
Revisione contenuti Piano Generale di Verifica e Validazione (progetti di estensione d''uso) per trattare in maniera più approfondita i requisiti di sicurezza e usability e integrare l''output della documentazione di gestione dei rischi nella definizione dei requisiti di progettazione.
Creazione di una revisione ',NULL,NULL,N'21/12/2018
21/12/2018
21/12/2018
21/12/2018',NULL,N'30/10/2019
30/10/2019
30/10/2019
30/10/2019',N'No',N'nessun impatto',N'No',N'nessun impatto, aggiornamento processo interno',N'No',N'nessun impatto, aggiornamento processo interno',N'nessun impatto, aggiornamento processo interno',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto',N'No',N'nessun impatto, aggiornamento processo interno',N'No',N'nessun virtual manufacturer coinvolto',NULL,N'No',NULL,'2019-12-20 00:00:00',N'AUDIT INTERNO:
eseguito audit interno in data 30/10/2019, firmato il 6/12/2019',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-10-30 00:00:00','2019-12-06 00:00:00'),
    (N'18-38',N'Correttiva','2018-12-19 00:00:00',N'Roberta  Paviolo',N'Sistema qualità',N'Non conformità',N'18-135',N'NC CEISO',N'Documentazione SGQ',N'Mancata esplicitazione nella PR06,01 della qualifica dei consulenti.',N'Inserire in PR06,01 le modalità per la qualifica dei consulenti, in particolare coloro che forniscono consulenza sui prodotti e sulla parte regolatoria. Gestire i distributori che tarttatano aspetti regolatori, quali la registrazione dei prodotti presso la CA locale, al pari dei fornitori, quindi sottoporli a qualifica e a valutazione periodica.',NULL,N'Impatto positivo in quanto tutto il personale con cui EGSpA interagisce per la parte regolatoria o per consulenze sui prodotti è qualificato.
Impatto positivo in quanto tutto il personale con cui EGSpA interagisce per la parte regolatoria o per consulenze sui prodotti è qualificato.
Impatto positivo in quanto tutto il personale con cui EGSpA interagisce per la parte regolatoria o per consulenze sui prodotti è qualificato.',N'19/12/2018
19/12/2018
19/12/2018',N'Aggiornamento PR06,01_rev01 inserendo le modalità per la qualifica dei consulenti, in particolare coloro che forniscono consulenza sui prodotti e sulla parte regolatoria, e per la gestione dei dstributori  considerandoli al pari dei fornitori.
Gestire i distributori che trattatano aspetti regolatori, quali la registrazione dei prodotti presso la CA locale, al pari dei fornitori, quindi sottoporli a qualifica e a valutazione periodica.
messa in uso della documentazione.',N'PR06,01 rev02 22/07/2019 (vedere RAP18-21) - rac18-38_messa_in_uso_pr0601_02_rap18-21.msg
PR06,01 rev02 22/07/2019 (vedere RAP18-21) - rac18-38_messa_in_uso_pr0601_02_rap18-21_1.msg
PR06,01 rev02 22/07/2019 (vedere RAP18-21) - rac18-38_messa_in_uso_pr0601_02_rap18-21_2.msg',NULL,N'19/12/2018
19/12/2018
19/12/2018',NULL,N'16/09/2019
03/01/2020
16/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Rispetto dei regolamenti extra UE dei paesi aderenti al MDSAP.',N'F - Gestione dei Fornitori',N'F1 - Identificazione del fornitore',N'I distrbutori entreranno nel processo di "Gestione fornitori" e saranno gestiti come fornitori, sui sottoprocessi non ci sono impatti.',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'Stefania Brun',N'No',NULL,'2020-12-30 00:00:00',N'Verificare almeno 3 distributori selezionati e qualificati.
17/11/2020 nel corso dell''audit interno al processo "Coordinamento vendite estero" del 28/10/2010, n° di report 11-20 sono emersi dei rilievi relativi ad un parziale disallineamento fra il processo in essere ed una carenza nella documentazione di selezione e qualifica dei Distributori. Si ritiene pertanto necessario mantenere questa azione correttiva aperta.',NULL,NULL,NULL,NULL,NULL,NULL,'2020-01-03 00:00:00',NULL),
    (N'18-37',N'Preventiva','2018-12-19 00:00:00',N'Michela Boi',N'Sistema qualità',NULL,NULL,NULL,NULL,N'La revisione di AP per InGenius potrebbe avere un impatto critico sulle performance del prodotto stesso. In questo caso, si ritiene opportuno implementare il modo di comunicazione agli utilizzatori finali della disponibilità di una nuova revisione dell''AP. A questo fine è necessario effettuare una valutazione dell''impatto della modifica dell''AP all''inizio del processo di revisione dell''AP.',N'Ogni qualvolta si debba eseguire una revisione di un AP per InGenius, è necessario effettuare una chiara valutazione dell''impatto che la modifica apportata possa avere sulle performance del prodotto. In alcuni casi, infatti, è necessario aggiornare e utilizzare la nuova revisione dell''AP per evitare di avere risultati falsi positivi o falsi negativi. Sulla base di criteri stabiliti, una modifica all''AP potrebbe avere un impatto ALTO o BASSO sulle performance del prodotto. Tale distinzione permette di valutare se la messa in uso della nuova revisione dell''AP deve essere accompagnata da azioni specifiche verso gli utilizzatori. In caso infatti, la modifica abbia un Impatto ALTO, la messa in uso della nuova revisione dell''AP deve avvenire a seguito della rilavorazione del lotto di prodotto, che prevede l''applicazione di una etichetta aggiuntiva con cui il cliente viene avvisato della disponibilità di una nuova revisione dell''AP, e della comunicazione tramite mailing list agli utilizzatori finali. Nel caso in cui, invece, la modifica abbia un impatto BASSO, la messa in uso segue l''attuale procedura.',NULL,N'Positivo in quanto permette di valutare in maniera più accurata gli impatti sulle prestazioni del prodotto prima del rilascio della modifica di un AP
Positivo in quanto permette di valutare in maniera più accurata gli impatti sulle prestazioni del prodotto prima del rilascio della modifica di un AP
Prevede uno step aggiuntivo al termine del processo di confezionamento nel caso in cui la valutazione dell''impatto della modifica sia ALTO
Positivo in quanto previene la possibilità di ricevere reclami dovuti all''uso da parte del cliente di assay protocol non aggiornati
Positivo in quanto previene la possibilità di ricevere reclami dovuti all''uso da parte del cliente di assay protocol non aggiornati
Positivo in quanto previene la possibilità di ricevere reclami dovuti all''uso da parte del cliente di assay protocol non aggiornati
Positivo in quanto previene la possibilità di ricevere reclami dovuti all''uso da parte del cliente di assay protocol non aggiornati
Positivo in quanto previene la possib',NULL,N'stabilire una check list contenente i criteri di valutazione degli impatti della modifica dell''AP
Prima della modifica di un AP è necessario effettuare una valutazione dell''impatto che tale modifica potrebbe avere sulle performance del prodotto all''interno del CC o della RAC/RAP
rilavorazione del lotto di prodotto per l''apposizione di un''etichetta che evidenzi la disponibilità di una nuova revisione dell''AP
aggiornare la PR05,08 "Gestione Assay Protocol", includendo la valutazione degli impatti delle modifiche apportate agli AP e distinguendo le diverse azioni da seguire nel caso in cui l''impatto sia ALTO o BASSO. Indicare che la modifica di AP deve essere sempre gestita attraverso un CC o una RAC/RAP in modo da applicare la check list di valutazione impatti
aggiornare il MOD05,07A per l''approvazione degli AP, includendo una parte che evidenzi l''avvenuta valutazione degli impatti,  il risultato di tale valutazione e le azioni che ne conseguono
formare il personale interessato
Stabilire il tipo d',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'IFU - Gestione Manuali',NULL,N'IFU6 "stesura etichette con informazioni per scaricare IFU elettronico" + CS6 "etichettatura prodotti" (RA P1.6)',N'No',NULL,N'No',NULL,NULL,N'No',NULL,'2019-12-20 00:00:00',N'Verificare assenza di reclami relativi all''uso di AP non aggiornati presso i clienti',N'Stefania Brun',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'18-36',N'Correttiva','2018-12-17 00:00:00',N'Guglielmo Stefanuto',N'Ricerca e sviluppo',N'Non conformità',N'18-76',N'Coagulation ELITe MGB Kit con primer Eurogentec',N'Progetto',N'La causa primaria non è stata identificata, probabilmente un''efficienza diversa di amplificazione porta ad una diversa quantità di amplicone a singolo filamento nel prodotto di reazione a fine ciclo e questo altera la cinetica di dissociazione (aspetto della curva di dissociazione) e la rilevazione della Tm dell''allele wildtype (picco wt).',N'Una modifica del primer ratio per il target MTHFR può rendere meno variabile quantità di amplicone a singolo filamento nel prodotto di reazione a fine ciclo, stabilizzare la cinetica di dissociazione e la rilevazione della Tm dell''allele wildtype e permettere l''uso di oligonucleotidi sia sintetizzati da EGI MDx sia da Eurogentec.
In particolare, si testeranno concentrazioni leggermente più elevate di primer reverse (ratio 1,1 : 1) in modo da evitare di alterare la stabilità del prodotto.
Se i risultati di questo studio di fattibilità saranno positivi e saranno approvati dalle funzioni coinvolte da questa RAP, si procederà alla modifica del prodotto attraverso un progetto di manutenzione di prodotto.',NULL,N'Nessun impatto se non in termini di ore lavorative
Nessun impatto se non in termini di ore lavorative
Valutare attraverso uno studio di fattibilità le nuove concentrazioni di primer in modo da permettere l''uso di oligonucleotidi sia sintetizzati da EGI MDx sia da Eurogentec.
In caso di esito positivo, validare il processo di produzione del saggio con la nuova formulazione e verifcare l''equivalenza delle sue prestazioni attraverso un progetto di Manutenzione Prodotto, a seguito di approvazione da parte della Direzione.
Valutare attraverso uno studio di fattibilità le nuove concentrazioni di primer in modo da permettere l''uso di oligonucleotidi sia sintetizzati da EGI MDx sia da Eurogentec.
In caso di esito positivo, validare il processo di produzione del saggio con la nuova formulazione e verifcare l''equivalenza delle sue prestazioni attraverso un progetto di Manutenzione Prodotto, a seguito di approvazione da parte della Direzione.
Aggiornamento della documentazione di prodotto come da studio di f',N'20/12/2018
20/12/2018
21/12/2018
21/12/2018',N'Testare concentrazioni più elevate di primer reverse (ratio 1,1 : 1) attraverso uno studio di fattibilità
A seguito di approvazione della direzione, eseguire un progetto di manutenzione di prodotto sulla base dei risultati ottenuti nello studio di fattibilità.
Aggiornamento modulistica produzione e formazione al personale di produzione
Isolare i lotti di oligonucleotidi Eurogentec spostandoli nel magazzino NC per prevenirne l''utilizzo fino alla chiusura progetto.
Le attività a carico saranno gestite nel progetto di manutenzione di prodotto.
A chiusura progetto ricevere l''informazione riguardo l''eliminazione della limitazione sulla specifica oligonucleotidi relativa al fornitore Eurogentec
Le attività a carico saranno gestite nel progetto di manutenzione di prodotto.
Archiviare la specifica 951-RTSD00ING rev01 con limitazione d''uso relativo all''acquisto esclusivo di EG MDx e rimettere in uso la specifica 951-RTSD00ING rev01 senza limitazione d''uso e comunicare a Produzione e Acquisti la poss',N'Report investigation eliteMGBkit formulation - rac18-36_investigationoncoagulationelitemgbformulaton.pdf
MP non richiesta - rac18-36_manutenzione_prodotto_non_richiesta_dalla_direzione.msg
MP non richiesta - rac18-36_manutenzione_prodotto_non_richiesta_dalla_direzione_1.msg
MP non richiesta - rac18-36_manutenzione_prodotto_non_richiesta_dalla_direzione_2.msg
MP non richiesta - rac18-36_manutenzione_prodotto_non_richiesta_dalla_direzione_3.msg',NULL,N'20/12/2018
20/12/2018
21/12/2018
21/12/2018
21/12/2018',NULL,N'09/01/2019
10/09/2019
10/09/2019
10/09/2019
10/09/2019',N'No',N'MP non richiesta, nessun impatto sui DMRI',N'No',N'MP non richiesta, non necesssaria alcuna comuniczione al cliente',N'No',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto',N'No',N'MP non richiesta, non necesssaria alcuna notifica',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2019-09-10 00:00:00',N'La manutenzione del prodotto Coagulation ELITe MGB Kit, codice RTSD00ING, per permettere l''uso degli oligonucleotidi Kaneka Eurogentec SA, prevista dalla RAC18-36, al momento non è stata richiesta dalla Direzione. soi reputa pertanto possibile chiudere l''azione correttiva.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-09-10 00:00:00','2019-09-10 00:00:00'),
    (N'18-35',N'Correttiva','2018-12-12 00:00:00',N'Stefania Brun',N'Produzione',N'Non conformità',N'18-147',N'RTS200ING LP2',N'Documentazione SGQ',N'Nel trasferimento delle informazioni relative ai lotti di MP da usare per LP2 di CRE Production Planner ha registrato sul file di programmazione condiviso "Programmazione Lotti Produzione" il lotto di sonda uguale a quello del LP1 o errato. Non è possibile rintracciare che tipo di errore è stato commesso se in fase di assegnazione del lotto di MP o da parte del tecnico di produzione in fase di impegno dei lotti per la produzione del LP2 in quanto il foglio contenente lo screenshot del file di programmazione relativo ai lotti MP da utilizzare non è stato trovato. Inoltre questa prassi di conservare il foglio con lo screenshot non è definita in procedura',N'CORREZIONE: pianificazione di un mini lotto di produzione utilizzando il lotto di sonda destinato per LP2 per esecuzione di test di stabilità reali in modo da confermare la stabilità interlotto.
AZIONE CORRETTIVA: 
1.coinvolgere Production Planner in fase di Design transfer plan in modo da rendere noti i requisiti richiesti dal piano. 
2. Integrare le metodiche di pianificazione dei lotti di materie prime, soprattutto per i lotti pilota di progetti di manutenzione.
3. Integrare nella procedura di produzione PR09,01 le modalità utilizzate per la pianificazione',NULL,N'Positivo in quanto garantirà un corretto trasferimento delle informazioni da R&D a produzione in fase di design transfer plan.
Positivo in quanto garantirà un corretto trasferimento delle informazioni da R&D a produzione in fase di design transfer plan.
Positivo in quanto è integrato il passaggio di informazioni relative alle necessità per la produzione dei lotti pilota ai tecnici di produzione. Positivo in quanto permetterà di tracciare l''assegnazione dei lotti significativi di materia prima e garantirà un controllo aggiuntivo prima della produzione del lotto pilota.
Positivo in quanto è integrato il passaggio di informazioni relative alle necessità per la produzione dei lotti pilota ai tecnici di produzione. Positivo in quanto permetterà di tracciare l''assegnazione dei lotti significativi di materia prima e garantirà un controllo aggiuntivo prima della produzione del lotto pilota.
Positivo in quanto è integrato il passaggio di informazioni relative alle necessità per la produzione dei lo',N'12/12/2018
12/12/2018
12/12/2018
12/12/2018
12/12/2018
12/12/2018
24/01/2019
24/01/2019
24/01/2019',N'Programmare test stabilità reale con mini lotto.
Coinvoligimento con Production Planner, i tecnici di produzione ed il personale dell''approvvigionamento durante la condivisione dei requisiti definiti nel design transfer plan (tabella riassuntiva inviata tramite mail).
Integrare le attività del Production Plannaer inserendo la registrazione sul MOD962-xxx delle materie prime significative, a seguito di verifica sul file "Programmazione lotti produzione". In caso di lotti pilota, dopo che il tecnico di produzione incaricato di produrre il lotto ha inserito i lotti in NAV e stampato la "Lista materiali", PP effettua un secondo controllo delle materie prime impegnate sul file "Programmazione lotti produzione".
Stampare e allegare la sezione del file di Programmazione lotti con i lotti di MP da utilizzare per la produzione del lotto
Integrare la procedura PR09,01 con le modalità di pianificazione e con la condivisione tra RT e PP/PT dei requisiti definiti nel design transfer plan.
Formare il personale',N'Pianificazione mini lotto stabilita'' reale U0119AQ - rac18-35_pianificazione_controlli_di_stabilita.pdf
condivisione dei requisiti definiti nel design transfer plan (tabella riassuntiva inviata tramite mail) - rac18-35_esempiomailpersonaleproduzionelotti_pilota_p210.msg
PR09,01_REV03 in uso al 04/009/2019, MDO18,02 del 03/09/2019 - rac18-35_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20.msg
PR09,01_REV03 in uso al 04/009/2019, MDO18,02 del 03/09/2019 - rac18-35_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20_1.msg
PR09,01_REV03 in uso al 04/009/2019, MDO18,02 del 03/09/2019 - rac18-35_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20_3.msg
MDO18,02 del 03/09/2019 - rac18-35_messa_in_uso_pr0901_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20_2.msg
 condivisione dei requisiti definiti nel design transfer plan (tabella riassuntiva inviata tramite mail)  - rac18-35_esempiomailpersonaleproduzionelotti_pilota_p210_1.msg
programmazione produzione lotti pilota - rac18-35_programmazione_dt-lp_prod_cq.xlsm
evidenzianione lotti pilota e legenda - rac18-35_programmazione_lotti_prod_materie_prime.xlsx',NULL,N'24/01/2019
24/01/2019
24/01/2019
24/01/2019
24/01/2019
24/01/2019
24/01/2019
24/01/2019',NULL,N'04/02/2019
19/04/2019
10/09/2019
10/09/2019
10/09/2019
10/09/2019
19/04/2019
10/09/2019',N'No',N'non necessario aggiornare i DMRI',N'No',N'non necessaria',N'No',N'nessun impatto,cambiamento di sistema',N'Nessun impatto',N'A - Gestione degli Acquisti
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'A1 - Pianificazione approvvigionamento materiali
P1.1 - Pianificazione',N'P1.1, A1',N'No',N'non necessaria, in quanto modifica di sistema',N'No',N'non necessaria',N'Anna Capizzi',N'No',NULL,'2020-03-30 00:00:00',N'Verificare assenza di NC imputabili a disallineamenti tra requisiti di progetto e produzione in fase di design transfer dopo l''implementazione delle modifiche. NC19-62 sullo stesso argomento.
17/11/2020: non sono presenti altre nc imputabili a disallineamenti tra requisiti di progetto e produzione in fase di design transfer. Il processo si ritiene efficace.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-09-10 00:00:00','2020-11-17 00:00:00'),
    (N'18-34',N'Preventiva','2018-12-07 00:00:00',N'Roberta  Paviolo',N'Assistenza applicativa',NULL,NULL,NULL,N'Documentazione SGQ
Manuale di istruzioni per l''uso',N'La standardizzazione dei template previene errori durante la creazione degli stessi al momento dell''installazione da parte del personale teccnico.',N'Creare e qualificare il modello di template, renderlo disponibile all''utilizzo da parte del personale tecnico, stabilire il flusso di attività all''interno di SGQ.
valutare, oltre allo strumento Roche gli altri strumenti utilizzati in associazione con prodotti EGSpA che necessitano di template standardizzato.',NULL,N'Impatto positivo in quanto si riduce il tempo di creazione del template sullo strumento, si riduce le possibilità di errore e si standardizza la procedura.
Impatto positivo in quanto si riduce il tempo di creazione del template sullo strumento, si riduce le possibilità di errore e si standardizza la procedura.
Impatto positivo in quanto si riduce il tempo di creazione del template sullo strumento, si riduce le possibilità di errore e si standardizza la procedura.
Impatto positivo in quanto si standardizza la procedura di creazione dei template necessari per l''utilizzo dei prodotti EGSpA validati sulle
diverse piattaforme.
Impatto positivo in quanto si standardizza la procedura di creazione dei template necessari per l''utilizzo dei prodotti EGSpA validati sulle
diverse piattaforme.
Impatto positivo in quanto si standardizza la procedura di creazione dei template necessari per l''utilizzo dei prodotti EGSpA validati sulle
diverse piattaforme.
Impatto positivo in quanto si standardizza la proced',N'05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019',N'Formazione, informazione degli speciliast sul processo di gestione dei template.
Formazione, informazione dei sistributori sul processo di gestione dei template.
Creare, verificare ed approvare template in associazione strumento / linea di prodotto: 7500FastDx / ELITe MGB, 7500FastDx / ELITe MGB OneStep (con inclusi i kit ELITe non OneStep da correre insieme in caso), 7500FastDx / Fast Track Diagnostice e Roche z480 / ELITe MGB.
Definire, in accordo con SGQ e R&D, il flusso di attività legate allo sviluppo ed alla messa in uso dei template, necessari per l''utilizzo dei prodotti EGSpA validati sulle diverse piattaforme. Verificare la PR05,08 rev00. Il DHFI contiene già la l''aggiornamento d ei template (punto "Files containing analysis parameters approved", MOD05,07A).  
Formazione al personale tecnico coinvolto nella creazione dei template.
Aggiornamento delle IFU per i prodotti interessati.
Aggiornare le IFU alla prima modifica significativa (es. estensione d''uso).
Definire, in accordo con VAL',N'MOD18,02 del 09/09/2019 - rap18-34_formazione_specialist_pr0508_gestione_assay_protocol_rev.01.msg
Messa in uso template open platform - rap18-34_template_open_platforms.msg
messa in uso template open patform - rap18-34_template_open_platforms_3.msg
 PR05,08_01  - rap18-34_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919.msg
MOD18,02 del 09/09/2019 - rap18-34_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919_1.msg
PR05,08_01 - rap18-34_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919_2.msg
 PR05,08_01 e MOD05,07A_02 - rap18-34_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919_3.msg
indirizzo di archiviazione dei template - rap18-34_template_open_platforms_1.msg
Per ciascun target sarà prevista nell''IFU  l''impostazione manuale dei parametri di analisi in alternativa all''utilizzo del template, la valutazione del rischio non risulta modificata. Infatti, seppur l''introduzione dell''utilizzo dei template consenta di ridurre la probabilità di errore, la stima della probabilità si basa sull''evento a più alto rischio (impostazione manuale) e rimane dunque invariata. - rap18-34_aggiornamento_analisi_rischi_non_necessaria.pdf
RDM e GASC non ritengono necessario l''aggiornamento delle IFU. - rap18-34_aggiornamento_ifu_non_necessaria_2.txt
RDM e GASC non ritengono necessario l''aggiornamento delle IFU. - rap18-34_aggiornamento_ifu_non_necessaria_1.txt
messa in uso template open patform - rap18-34_template_open_platforms_2.msg
RDM e GASC non ritengono necessario l''aggiornamento delle IFU. - rap18-34_aggiornamento_ifu_non_necessaria.txt
RDM e GASC non ritengono necessaio l''aggiornamento delle IFU - rap18-34_aggiornamento_ifu_non_necessaria_3.txt',NULL,N'05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
05/03/2019
07/10/2019',NULL,N'25/10/2019
25/10/2019
25/10/2019
13/09/2019
09/09/2019
25/10/2019
13/09/2019
25/10/2019
13/09/2019
25/10/2019
25/10/2019
09/10/2019
25/10/2019
25/10/2019',N'No',N'Nessun impatto sui DMRI in quanto non contegnono gli applicativi rilasciati ai clienti (comeassay protocol e template)',N'No',N'Inserita l''avvertenzarelativa all''estensione d''uso con la piattaforma Roche cobas z 480 analyzer.',N'No',N'Nessun impatto',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'Nessun risk assessment impattato',N'Si',N'effettuate le notifiche sull''estensione d''uso su Roche',N'No',N'-',NULL,N'No',NULL,'2020-01-03 00:00:00',N'Non ci sono stati reclami relativi all''installazione dei template da parte del personale teccnico.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-10-25 00:00:00','2020-01-03 00:00:00'),
    (N'18-33',N'Correttiva','2018-12-04 00:00:00',N'Katia Arena',N'Acquisti',N'Non conformità',N'18-122',N'Richiesta cambiamento criteri per attribuzione della scadenza',N'Documentazione SGQ',N'Nei certificati del CG Diluent, codice 950-214, rilasciati dal fornitore Thermo  precedenti a 09/2018, la DOM non era riportata, ma era riportata solo la data di scadenza. A questo prodotto veniva assegnata un''utilizzabilità di 1 anno dalla data di apertura, secondo la regola indicata nella SPEC950-214 rev00, il prodotto non è mai stato utilizzato oltre la data di scadenza.
Con l''introduzione, da parte del fornitore, della DOM nei certifictai di analisi e con la regola indicata nella SPEC950-214 rev01 di aggiungere 30 mesi alla DOM è emersa una scadenza troppo bassa per accettare la materia prima. A seguito di richiseta di informazioni al fornitore in merito ai criteri di attribuzione della scadenza dei prodotti Low Glycerol Platinum Taq e CG Diluent ( cod. B1982B001), è emerso che il fornitore assegna una scadenza di 6 anni dalla data di fabbricazione (DOM), rendendo perciò inapplicabile la regola interna EGSpA di asseganre 30 mesi.',N'Aggiornare le regole di attribuzione della scadenza al CG Diluent, codice 950-214 tenendo conto che il fornitore attribuisce una scadenza di 6 anni dalla DOM.',NULL,N'Impatto positivo in quanto, conoscendo le regole di scadenza che il fornitore Thermo applica (mail del 24/09/2018 allegata) è possibile attribuire una scadenza interna congrua alla durata di vita del prodotto.
Impatto positivo in quanto, conoscendo le regole di attribuzione della scadenza che il fornitore Thermo applica (mail del 24/09/2018 allegata) è possibile attribuire una datainterna di utilizzabilità congrua alla durata di vita del prodotto.
Impatto positivo in quanto, conoscendo le regole di attribuzione della scadenza che il fornitore Thermo applica (mail del 24/09/2018 allegata) è possibile attribuire una datainterna di utilizzabilità congrua alla durata di vita del prodotto.
Impatto positivo in quanto, conoscendo le regole di attribuzione della scadenza che il fornitore Thermo applica (mail del 24/09/2018 allegata) è possibile attribuire una datainterna di utilizzabilità congrua alla durata di vita del prodotto.
Nessun impatto se non in termini di ore lavoro.',N'12/12/2018
12/12/2018
12/12/2018
12/12/2018
12/12/2018',N'Aggiornamento SPEC950-214 rev01, con estensione scadenza da 30 mesi a 4 anni dal DOM.
Nessuna attività a carico.
Approvazione della SPEC950-214.
Messa in uso documenti',N'MAIL - 20181204145909436.pdf
SPEC950-214_02 - rac18-33_messa_in_uso_specxxx_nc19-24_rac18-33_rac17-12.msg
SPEC950-214_02 - rac18-33_messa_in_uso_specxxx_nc19-24_rac18-33_rac17-12_1.msg
SPEC950-214_02 - rac18-33_messa_in_uso_specxxx_nc19-24_rac18-33_rac17-12_2.msg
SPEC950-214_02 - rac18-33_messa_in_uso_specxxx_nc19-24_rac18-33_rac17-12_3.msg',NULL,N'12/12/2018
12/12/2018
12/12/2018
12/12/2018',NULL,N'18/03/2019
18/03/2019
18/03/2019
18/03/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'L''aggiornamento delle regole di attribuzione della scadenza non alterano le prestazioni del prodotto.',N'F - Gestione dei Fornitori',N'F6 - Gestione delle NC dei fornitori',N'F6',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2020-04-30 00:00:00',N'verificare la scadenza almeno di 1 lotto di CG diluent (si acquista una volta all''anno circa).
A 12/2019 non è ancora presente il nuovo lotto.
24/04/2020 Nuovo lotto 00855372, 00846842 (n° di oridne 19-PO-01642), SPEC950-214 rev02 del 05/03/2019, controllo in accettazione del 21/01/2020 fatto da Vernacchia: la modifica richiede che la data di scaenza sia calcolata a 4 anni dal di fabbricazione (DOM), la data di DOM è 10/2018, la data di scadenza è 10/2022, la specifica è compilata correttamente.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-03-18 00:00:00','2020-04-24 00:00:00'),
    (N'18-31',N'Preventiva','2018-10-10 00:00:00',N'Sergio  Fazari',N'Regolatorio',NULL,NULL,NULL,NULL,N'La conformità alle suddette normative è dichiarata durante lo sviluppo dei prodotti nei Regulatory Assessment e Plan. Sebbene tale dichiarazione presenti alcuni elementi di fondatezza quali:  i) packaging ridotto all''essenziale ii) packaging privo di sostanze/componenti pericolosi iii) packaging allineato a quello di prodotti concorrenti presenti sul mercato, la mancanza di un processo strutturato di verifica espone EGSPA al rischio di NON conformità alle suddette normative. ',N'L''azione preventiva ha l''obiettivo di creare un documento per l''assessment della conformità ai requisiti essenziali della normativa (per es. una checklist da associare al Packaging & Labeling Plan).
Tale verifica andrebbe inserita nelle attività di sviluppo dei prodotti e il documento conservato all''interno del File Tecnico.  ',NULL,N'++++',NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
    (N'18-28',N'Preventiva','2018-09-26 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Non esistono in commercio dei campioni clinici da utilizzare come riferimento per questo tipo di diagnosi.
Il reperimento dei campioni e la loro caratterizzazione è molto laborioso, vi sono inoltre alcune probabilità di non ottenimento di un campione idoneo.',N'Si chiede di valutare la correttezza del Delta CT per gli eterozigoti esclusivamente con il CTR. Nel caso del campione mu si utilizza il plasmide a causa della difficoltà di reperimento e che il campione CTR simula correttamente la presenza di un allele wt e uno mu.',NULL,N'L''impatto sul processo è positivo in quanto semplifica le modalità di CQ. 
Da un punto di vista di valutazione del lotto l''utilizzo di un campione reale non ha mai messo in luce una non conformità del lotto. Si tenga conto inoltre che il delta Ct previsto per il CTR è minore rispetto a quello di un campione clinico, permettendo un''analisi più stringente rispetto all''utlizzo del prodotto per la disciminazione su campioni
Nessun impatto sul processo e sul prodotto
Da un punto di vista di valutazione del lotto l''utilizzo di un campione reale non ha mai messo in luce una non conformità del lotto. Si tenga conto inoltre che il delta Ct previsto per il CTR è minore rispetto a quello di un campione clinico, permettendo un''analisi più stringente rispetto all''utlizzo del prodotto per la disciminazione su campioni
Nessun impatto sul processo e sul prodotto
Da un punto di vista di valutazione del lotto l''utilizzo di un campione reale non ha mai messo in luce una non conformità del lotto. Si tenga conto i',N'26/09/2018
26/09/2018
26/09/2018
26/09/2018',N'Modifica dei moduli di CQ per i prodotti RTSD13 e RTSD14, modifica della scheda di caratterizzazione. Variazione dle magazzino campioni CQ
Non vi sono azioni a carico.
Ricevere la formazione sui moduli.
Messa in uso documenti.',N'MESSA IN USOP MOD10,13-DeterminazioneAllelica rev01 , MOD18,02 del 30/10/18 - rap18-18_messa_in_uso_modxxxcorrezioni_+_mopd1013-det.allelica_rac17-27_rap18-28.msg
MOD10,19 rev02 - rap18-28_messa_in_uso_mod1019_rap18-28.msg
MESSA IN USOP MOD10,13-DeterminazioneAllelica rev01  - rap18-18_messa_in_uso_modxxxcorrezioni_+_mopd1013-det.allelica_rac17-27_rap18-28_1.msg
MESSA IN USO MOD10,19 rev02 - rap18-28_messa_in_uso_mod1019_rap18-28_1.msg',NULL,N'26/09/2018
26/09/2018
26/09/2018',NULL,N'24/01/2019
26/09/2018
24/01/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'La modifica non altera le prestazini del prodotto.',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'na',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2019-12-30 00:00:00',N' Verificare assenza NC sui prodotti alert RTSD13 e RTSD14. Non sono state riscontrate NC sui lotti prodotti da 01/2019:
- RTSD13-M U0819AA, U0919AS
- RTSD14-M U0219BF, U0719BL, U1019BR.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-01-24 00:00:00','2020-01-07 00:00:00'),
    (N'18-26',N'Correttiva','2018-08-03 00:00:00',N'Stefania Brun',N'Validazioni',N'Reclamo',N'18-79',N'Assay_MV2 ELITe_CSF_200_100_01, rilascia il risultato per i patogeni a RNA in modo errato',N'Assay Protocol',N'Il reclamo risulta essere fondato in quanto l''assay in uso, Assay_MV2 ELITe_CSF_200_100_01, rilascia il risultato per i patogeni a RNA (enterovirus e parechovirus) nel modo errato, ovvero: "DNA detected" e "DNA Not detected or below the LoD".
Pertanto si dovrà procedere con la correzione del metodo: MV2 ELITe_CSF_200_100_01 (assay CE-IVD), La modifica andrà eseguita per il Target 1 (enterovirus) e il Target 2 (parechovirus), indicando la dicitura corretta:
-              RNA detected
-              RNA Not detected or below the LoD',N'Correzione: modifica Assay_MV2 ELITe_CSF_200_100_01 in modo da allineare il risultato a quanto indicato dall''IFU nel paragrafo relativo a "Procedura" tabella di interpretazione dei risultati.
Verifica allineamento AP dei prodotti a RNA rispetto a quanto indicato dall''IFU.
Aggiunta di una nota nel MOD04,30 in cui si riporta l''intervento sulla frase del Modello 1 in modo da applicare la modifica nelle revisioni successive dell''Assay Protocol',NULL,N'Positivo in quanto permette di allineare l''AP all''IFU per quanto riguarda l''espressione del risultato
Positivo in quanto permette di allineare l''AP all''IFU per quanto riguarda l''espressione del risultato',NULL,N'modifica Assay_MV2 ELITe_CSF_200_100_01 in modo da allineare il risultato a quanto indicato dall''IFU nel paragrafo relativo a "Procedura" tabella di interpretazione dei risultati.
Verifica allineamento AP dei prodotti a RNA rispetto a quanto indicato dall''IFU.
Aggiunta di una nota nel MOD04,30 in cui si riporta l''intervento sulla frase del Modello 1 in modo da applicare la modifica nelle revisioni successive dell''Assay Protocol',N'Assat protocol MSV2 Elite_CSF_200_100_02, in uso al 20/8/2018 - rac18-26_qualita_sgq_messa_in_uso_assay_protocol_mv2_-_rac18-26.msg
TAB P05 revAB - rac18-26_elite_ingenius_-tab_p05_-_rev_ab_-__product_mv2_elite_mgb_panel_notice.pdf',NULL,NULL,NULL,N'20/08/2018
18/03/2020',N'No',N'nessun impatto',N'No',N'nessun impatto',N'Si',N'Sostituire la rev.01 dell'' Assay_MV2 ELITe_CSF_200_100_01 con quella corretta. Tale errore non inficia le performance e il rilascio dei risultati',N'L''errore non inficia le performance e il rilascio dei risultati',N'IFU - Gestione Manuali',N'IFU5 - approvazione e messa in uso manuali e assay protocol',N'ifu5 "APPROVAZIONE ASSAY E IFU"',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',NULL,N'No',NULL,'2018-12-31 00:00:00',N'valutazione efficacia: verificare che con l''aggiornamento dai clienti dell''AP non si verifichino più reclami con uguale causa primaria.
a 03/2020 dai clienti sono ancora presenti AP in revisioni obsolete rispetto alla TAB p05 revAB, nonostante non ci siano ulteriori reclami con uguale causa primaria.
Nel 2019 è stata aperta la RAC19-2 che sostituiva nuovamente gli AP aumentandone la revisione. Anche in questo caso sono ancora presenti presso i clienti revisioni obsolete rispetto alla TAB P005 revAC. 
Questa azione correttiva può chiudersi in quanto obsoleta rispetto alla RAC19-2: l''ultima revisione dell''AP contiene anche le modifiche relative a questa RAC.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2020-03-18 00:00:00','2020-03-18 00:00:00'),
    (N'18-25',N'Correttiva','2018-07-05 00:00:00',N'Stefania Brun',N'Ordini',N'Non conformità',N'18-53',N'verifica stato registrazione prodotti per evasione ordini paesi extra UE',N'Documentazione SGQ',N'La causa primaria risiede nella inadeguatezza della procedura degli ordini in uso PR03,01 che indica che il file MOD05,29 con lo stato di registrazione dei prodotti si trova in una directory diversa da quella in uso, ossia G:DOCSGQREGISTRAZIONIORDINI. Si fa presente che tale necessità è stata valutata ed indicata nel documento di requisiti della nuova versione di Navision2017 ElitechGroup NAV Core Model Requieremnts
Documents V01.01.docx e che in ogni caso la soluzione che verrà adottata è comunque un file con lo stato di registrazione dei prodotti nei vari paesi pertanto non sarà praticabile una soluzione completamente automatizzata, ma sarà presente un messaggio di avviso che confrontando sulla tabella allegata il codice prodotto con il paese di destinazione indica che il prodotto non è registrato. In attesa della nuova revisione di Navision occorre modificare la PR03,01 indicando anche come deve essere effettuato il controllo e fornirne l''evidenza con un check da riportare sull''ordine ',N'Aggiornamento della PR03,01 "GESTIONE ORDINI CLIENTE" per esplicitare meglio le modalità di verifica dello stato di registrazione dei prodotti presso i vari paesi prima dell''evasione dell''ordine e fornire evidenza del controllo effettuato prima dell''evasione dell''ordine',NULL,N'Il controllo dello stato di registrazione dei prodotti nei vari paesi rallenta il processo di evasione degli ordini esteri ma si rende necessario per evitare impatti regolatori negativi o interdizioni in fase di sdoganamento dei prodotti
Impatto positivo in quanto permette di evitare impatti regolatori negativi quali non conformità in sede di audit o interdizioni in fase di sdoganamento dei prodotti',N'05/07/2018
05/07/2018',N'Aggiornamento della PR03,01 "GESTIONE ORDINI CLIENTE" per esplicitare meglio le modalità di verifica dello stato di registrazione dei prodotti presso i vari paesi prima dell''evasione dell''ordine e fornire evidenza del controllo effettuato prima dell''evasione dell''ordine
nessuna attività a carico.',N'pr03,03_01_all-1_pr03,01_00, MOD18,02 10/06/19 - rac18-25_messainuso_pr03,03_01_all-1_pr03,01_00.pdf',NULL,N'05/07/2018
05/07/2018',NULL,N'10/06/2019
05/07/2018',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Rispetto dei requisiti regolatori dei paesi MDSAP',N'SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'SP2 - Identificazione prodotti e imballaggi',N'SP2 aggiornato',N'No',N'non necessaria',N'No',N'nessu virtual manufacturer',NULL,N'No',NULL,'2018-12-30 00:00:00',N'Verifica durante un audit interno / esterno dell''assenza di ordini evasi verso paesi extra UE per prodotti che non abbiano la registrazione completata presso la locale Autorità competente. Dall''audit interno 4-19 del 05/04/2019 è stata aperta la NC19-50 in quanto sono stati riscontrati degli ordini evasi per prodotti non registrati senza indicazione sulla bolla di trasporto che fossero per uso RUO',N'Stefania Brun',N'Negativo',N'Revisionare la PR03,01 per rinforzare i controlli ed integrare la rilavorazione con etichetta RUO: PR03,01 rev01 in uso al 10/06/2019',N'Il nuovo termine per la valutazione dell''efficacia è in occasione dell''audit MDSAP di Dekra dal 16 al 20/09/2019.
Come da Report DEKRA del 19/09/2019, numero Report identification: 2235223-AR14-R0 DRAFT, non ci sono state NC imputabili alla verifica dei prodotti registrati presso i paesi MDSAP prima dell''evasione dell''ordine. ',N'Positivo',NULL,'2019-06-10 00:00:00','2020-01-07 00:00:00'),
    (N'18-23',N'Correttiva','2018-06-27 00:00:00',N'Stefania Brun',N'Sistema qualità',N'Non conformità',NULL,N'simbolo CE su etichetta interna tubi prodotto RUO',NULL,N'La causa primaria è imputabile alla mancata esplicitazione del codice templato nella IO04,06 da utilizzare nel caso di prodotti che contengono RT enzyme e prevedono una fase 956 precedente al 962.L''errore non è stato rilevato in precedenza poichè è sfuggito durante il controllo del DHR. Inoltre, al momento del controllo al rilascio del lotto da parte di SGQ non era possibile evidenziarlo in quanto sul modulo 962-RTS130ING non sono presenti le etichette dei tubini interni, perchè riportate sui moduli di preparazione 956 dei singoli componenti. Modificata anagrafica etichetta con codice templato 25 anziché 80 essendo un prodotto che contiene RT enzime comune ai prodotti One step la cui lavorazione e confezionamento è eseguita in una fase precedente al 962, quella 956. Sul gestionale per i prodotti one step è prevista la compilazione di 2 anagrafiche: 1. Etichette-scheda etichette lean packaging,  2.Progettazione prodotto-articoli-etichette, l''impostazione attuale su Nav che prevede la compilazione di 2 anagrafiche non è chiara rispetto a quella dei prodotti 962 che ne ha solo una.
',N'Modifica della IO04,06  per indicare il codice templato nella IO04,06 da utilizzare nel caso di prodotti che contengono RT enzyme e prevedono una fase 956 precedente al 962.',NULL,N'Positivo in quanto permetterà di evitare NC di creazione layout etichette per i prodotti One step che hanno una doppia anagrafica su Nav da compilare rispetto ai codici 962',N'27/06/2018',N'Aggiornare IO04,06 "ISTRUZIONE OPERATIVA CREAZIONE ETICHETTE PRODOTTI CE-IVD E RUO" rev00.',N'IO04,06 rev02, MOD18,02 del 11/10/18 - rac18-23_messa_in_uso_io0406_rac17-31_rac18-23_1.msg',N'QM',N'27/06/2018',NULL,N'11/10/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,N'ET - Gestione Etichette',NULL,N'ET3 "approvazione etichetta"',N'No',NULL,N'No',NULL,NULL,N'No',NULL,'2019-06-28 00:00:00',N'La pianificazine del 2019 non prevede lo sviluppo di prodotti RUO. Si ritiene pertano necessario mantenere aperta questa RAC.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2018-10-11 00:00:00','2019-03-25 00:00:00'),
    (N'18-20',N'Correttiva','2018-06-20 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'18-73',N'ER140',N'Documentazione SGQ',N'Documenti errati.',N'Verifica del materiale disponibile in magazzino e stoccaggio come controcampione
Si richiede la modifica dei documenti di produzione del prodotto per l''invio del controcampione al CQ.
Verificare se esitono altri prodotti con situazioni analoghe e conseguente modifica.
Modifica della procedura di gestione dello stoccaggio dei controcampioni, in particolare la decisione delle quantità è fatta in fase di stesura dei documenti e questo può comportare errori simili a quello riscontrato e a non avere la stessa quantità per categorie di prodotto.
Per questo sarebbe auspicabile un documento elenco per tutti i prodotti in modo da visualizzare con facilità il materiale da inviare al CQ.',NULL,N'L''impatto è positivo sul processo di valutazione dei controcampioni da archiviare, inoltre è risolutivo per la NC riscontratat. Dal punto di vista delle attività sarà necessario rivedere tutti i documenti di produzione pertanto andranno pianificate le attività che richiederanno tempo. Lo spazio destinato allo stoccaggio è presente.
L''impatto è positivo sul processo di valutazione dei controcampioni da archiviare, inoltre è risolutivo per la NC riscontratat. Dal punto di vista delle attività sarà necessario rivedere tutti i documenti di produzione pertanto andranno pianificate le attività che richiederanno tempo. Lo spazio destinato allo stoccaggio è presente.
Saranno modificate le fonti di informazione del materiale da inviare al CQ e non più reperibili direttamente nel documento di produzione.
Valutazione eventuale modifica dei costi standard in conseguenza dell''invio dei controcampioni. ER140 non era previsto per eventuali altri prodotti sarà coinvolta per la valutazione',N'20/06/2018
20/06/2018
20/06/2018',N'vedere descrizione dell''azione 
vedere descrizione dell''azione ',N'MOD957-ER140_02 e MOD910-ER140_01, MOD18,02 del 13/09/2018 - rac18-20_messa_in_uso_mod_er140_rac18-20_cc12-17.msg
stato aggiornamento - rac18-20_statoaggiornamentocontrocampioni.msg
MOD962-RTS078PLD-Taq_01 - rac18-20_messa_in_uso_mod962-rts078pld-taq_01_rac18-20_mod1802_del_27819.msg',NULL,N'20/06/2018
20/06/2018',NULL,N'24/09/2019
24/09/2019',N'No',N'nessun impatto sui DMRI',N'No',N'non necessaria alcuna comunicazione',N'No',N'nessun impatto',N'nessun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC2 - Redazione documenti',N'DC2 "Redazione documenti", nessun impatto',N'No',N'la modifica non è significativa, pertanto non è da notificare',N'No',N'la modifica non è significativa, pertanto non è da notificare',NULL,N'No',NULL,'2019-12-31 00:00:00',N'Verificare a campione la presenza di controcampioni
24/09/2019: nel corso di un anno non ci sono state altre carenze di controcampioni. Si aggiornano comunque i moduli che, da analisi, risultano essere in difetto (p190 e p210 saranno aggiornati con i progetti in corso entro 12/2019).',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-09-24 00:00:00','2020-01-30 00:00:00'),
    (N'18-10',N'Correttiva','2018-03-08 00:00:00',N'Stefania Brun',N'Assistenza strumenti esterni',N'Reclamo',NULL,N'interferenze elettromagnetiche, etichetta "No mobile"',N'Etichette',N'L''indagine condotta in collaborazione con PSS evidenzia che la causa primaria potrebbe essere dovuta ad interferenze elettromagnetiche che causano un malfunzionamento della scheda installata sul single nozzle. Lo strumento in questione è stato sostituito.

',N' Azioni correttive:
- nelle nuova versione di SW 1.3 (attualmente in fase di testing) sono state riviste le soglie di pressione che causavano l''errata attivazione del messaggio di errore

Azione di mitigazione:
dato che l''errata attivazione potrebbe essere in parte  imputabile ad interferenze elettromagnetiche riconducibili alle frequenze dei cellulari, si propone di mettere sullo strumento un''etichetta "no mobile" per riflettere quanto già riportato sul manuale utente.',NULL,N'Eseguire test aggiuntivi presso centro esterno accreditato ripetere prove di compatibilità elettromagnetica (EMC)
Valutare la necessità di notifica del cambiamento (aggiunta etichetta NO MOBILE) alle Autorità competenti presso le quali è registrato lo 
Aggiornare fascicolo tecnico dello strumento e analisi dei rischi',N'08/03/2018
08/03/2018
08/03/2018',N'Esaminare i risultati ottenuti dalla ripetizione delle prove di compatibilità elettromagnetica (EMC) eseguite presso il centro accreditato
Valutare la necessità di notifica del cambiamento (aggiunta etichetta NO MOBILE) alle Autorità competenti presso le quali è registrato lo strumento
Aggiornare fascicolo tecnico dello strumento e analisi dei rischi',N'esiti prove EMC - allegato_rac18.docx
Certificato prove EMC  - tr-17-121.pdf
piano azioni RAC18-10 - rac18-10_pianoazioni_i_no_mobile_phone_adhesive_sticker_action_plan.msg
strumenti prodotti con etichchetta "no mobile phone" - rac18-10_etichettasullostrumento_no_mobile_phone_sticker.msg
TSB042revA_apposizione etichetta no mobile phone sugli strumenti installati - rac18-10_elite_ingenius_-tsb_042_-_rev_a_-_no_mobile_phone_sticker_implementation_with_signature.pdf
invio TSN042 revA - rac18-10_invio_tsb_042_-_rev_a_-_no_mobile_phone_sticker.msg
PSS ha valutato non necessario aggiornare l''analisi del rischio. - rac18-10_aggiornamento_rac.msg
IFU Elite_ingenius rev02, aggiunto nuovo simbolo CAUTION etichetta NO MOBILE PHONE - rac18-10_qualita_sgq_market_release_-_elite_ingenius_ifu_rev02.msg
Non è necessario notificare alle autorità competenti in quanto l''etichetta "no mobile phone" non è integrata nell''etichetta dello strumento. - rac18-10_invio_tsb_042_-_rev_a_-_no_mobile_phone_sticker_1.msg',NULL,N'08/03/2018
08/03/2018
08/03/2018',NULL,N'08/04/2019
08/04/2019
04/03/2021',N'No',N'nessun impatto',N'Si',N'Predisporre TSB per informare clienti sull''implementazione della misura di mitigazione relativa all''etichetta NO MOBILE. (8/4/19 TSB042revA_apposizione etichetta no mobile phone sugli strumenti installati )',N'Si',N'Predisporre piano per aggiornamento delle macchine già installate (aggiunta etichetta NO MOBILE)',N'nessun impatto',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'SW20 e ST20 STRUMENTOE  SW iNGENIUS',N'No',N'Non è necessario notificare alle autorità competenti in quanto l''etichetta "no mobile phone" non è integrata nell''etichetta dello strumento. ',N'No',N'non necessaria',N'Pim Giesen',N'No',NULL,'2019-12-30 00:00:00',N'Da 04/2018, a 04/2019 non ci sono stati altri reclami la cui causa primaria può essere imputabile a interferenze elettromagnetiche.
La RAC rimane aperta per l''aggiornamento del FTP e per l''apposizione delle etichette sugli strumenti installati; a 09/2019: 
- in Italia l''apposizione dell''etichetta viene eseguita in concomitanza con un intervento tecnico e/o Manutenzione Ordinaria.
- le etichette sono state consegnate a Distributori e ICO durante L''ECCMID. L''apposizione dell''etichetta, come da "TSB 042 - Rev A - No Mobile Phone Sticker", è svolta durante visita presso il cliente. 

A 03/2021 non ci sono state altri reclami con questa causa primaria. Le attività di etichettatura si ritengono terminate.
 
',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2021-03-04 00:00:00','2021-03-04 00:00:00'),
    (N'18-4',N'Correttiva','2018-01-29 00:00:00',N'Federica Farinazzo',N'Controllo qualità',N'Non conformità',N'17-125',N'MOD09,23',NULL,N'Documentazione non chiara',N'Modifica documenti:
-MOD09,23 mancano i plasmidi nell''elenco e va chiarito quando inviarli.
-indicare nel modulo 962-CTR200ING che per le nuove preparazioni devono essere inviati al CQ anche i plasmidi nuovi (da verificare)
-Ampliare ad ESBL.
-Dato che i plasmidi in uso scadono a dicembre, risulta necessario testare le diluizioni dei nuovi plasmidi prima della messa in uso, al di fuori del CQ della mix già eseguito (si allegano i risultati).
',NULL,N'MOD09,23 mancano i plasmidi nell''elenco e va chiarito quando inviarli.
-indicare nel modulo 962-CTR200ING che per le nuove preparazioni devono essere inviati al CQ anche i plasmidi nuovi (da verificare)
-Ampliare ad ESBL.
-Dato che i plasmidi in uso scadono a dicembre, risulta necessario testare le diluizioni dei nuovi plasmidi prima della messa in uso, al di fuori del CQ della mix già eseguito (si allegano i risultati).
Approvazione e Formazione dei documenti
Approvazione e Formazione dei documenti
-',N'29/01/2018
29/01/2018
29/01/2018',N'MOD09,23 mancano i plasmidi nell''elenco e va chiarito quando inviarli.
-indicare nel modulo 962-CTR200ING che per le nuove preparazioni devono essere inviati al CQ anche i plasmidi nuovi (da verificare)
-Ampliare ad ESBL.
-Dato che i plasmidi in uso scadono a dicembre, risulta necessario testare le diluizioni dei nuovi plasmidi prima della messa in uso, al di fuori del CQ della mix già eseguito (si allegano i risultati).
Approvazione e Formazione dei documenti
-',N'ctr200ing:punti di diluizione CQ con il 962 - mod09,23_04_elenco_prodotti-moduli-diluizioni.xlsx
CQ mix già eseguito - cq_plasmidicre_p1017bi-bh-bj-bm-bg_mod10,12-rts200ing-ctr200ing_03.pdf
messa in uso MOD09,23, formazione del 21/22-5-18 (MOD18,02) - rac18-4_sgq_messa_in_uso_mod_diluizione_plasmidica_rac17-16_e_altri_rac17-27_cc12-17_cc18-17_cc18-25_rap17-26_nc17-125.msg
messa in uso MOD09,23, formazione del 21/22-5-18 (MOD18,02)  - rac18-4_sgq_messa_in_uso_mod_diluizione_plasmidica_rac17-16_e_altri_rac17-27_cc12-17_cc18-17_cc18-25_rap17-26_nc17-125_1.msg',NULL,N'29/01/2018
29/01/2018',NULL,N'25/05/2018
25/05/2018
25/05/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'-',N'P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',NULL,N'P3.13',N'No',NULL,N'No',NULL,N'Federica Farinazzo',N'No',NULL,'2018-09-30 00:00:00',N'Nel MOD10,01 "richiesta controllo Qualità" ricevuto da QCT il 23/7/18, era chiaramente indicato che il personale di produzione rilascia al CQ 2 tubi per ciascun plasmide (tra cui VIM e IMP), come indicato nel MOD09,23, ultimo aggiornamento del 29/08/2018.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2018-05-25 00:00:00','2018-09-04 00:00:00'),
    (N'17-35',N'Correttiva','2017-12-29 00:00:00',N'Stefania Brun',N'Regolatorio',N'Non conformità',N'17-139',N'Non conformità audit interno CEISO n°14-17 ',N'Documentazione SGQ',N'Manca la descrizione in procedura o istruzione di controllo al rilascio delle modalità per autorizzare il rilascio dei lotti/n°seriali di prodotti/strumenti OEM. Inoltre per i reagenti e strumenti InGenius tale NC si collega alla mancata osservanza della clausola presente sul quality Agreement tra EGSpA e PSS. E'' stata notificata nel corso dell''audit a PSS di settembre 2017. Il fornitore ha intrapreso un''azione correttiva riportata in allegato dove spiega la causa che ha generato il mancato invio dei Certificati di conformità/collaudo degli strumenti (root cause: mancato passaggio di consegne all''interno di PSS per l''osservanza della clausola contenuta nel contratto e mancata richiesta da parte di EGSpA relativa al rilascio per ogni strumento spedito di un certificato di conformità collaudo',N'Aggiornare la IO09,07 Istruzione operativa controllo rilascio lotti al fine di definire le modalità da attuare per autorizzare il rilascio dei lotti di prodotti/strumenti per i quali EGSpA risulta fabbricante',NULL,N'Definire flusso di verifica delle documentazione di conformità (certificati di analisi, esito CQ, certificati di collaudo) rilasciata dal fornitore e approvazione formale per rilascio lotto/n°seriale 
Definire flusso di verifica delle documentazione di conformità (certificati di analisi, esito CQ, certificati di collaudo) rilasciata dal fornitore e approvazione formale per rilascio lotto/n°seriale 
Positivo in quanto permette di ottemperare alle prescrizioni della ISO13485:2016 anche per i prodotti/strumenti la cui produzione è eseguita da altri soggetti per conto di EGSpA',N'29/12/2017
29/12/2017
29/12/2017',N'Definire directory dove salvare i certificati/documentazione conformità del fornitore per i lotti/seriali da rilasciare e per quelli autorizzati per il rilascio
Aggiornare la IO09,07 Istruzione operativa controllo rilascio lotti al fine di definire le modalità da attuare per autorizzare il rilascio dei lotti di prodotti/strumenti per i quali EGSpA risulta fabbricante
Approvazione IO09,07',N'IO09,07rev03_paragrafo2.4 definizione directory; MOD18,02 del 8/6/18 - rac17-35_messa_in_uso_io0907_rev03_rac17-35_cc18-21.msg
IO09,07rev03 - rac17-35_messa_in_uso_io0907_rev03_rac17-35_cc18-21_1.msg',NULL,N'29/12/2017
29/12/2017
29/12/2017',NULL,N'08/06/2018
08/06/2018
08/06/2018',N'No',N'nessun impatto sul dmri',N'No',N'non necessaria',N'No',N'nessun impatto',N'I regolamenti richiedono il rilascio di prodotti e strumenti da parte del fabbricante. ',N'CS - Confezionamento (anche per CQ), Stoccaggio  e Rilascio al cliente del prodotto',N'CS12 - controllo per l''autorizzazione al rilascio al cliente',N'CS12, in aggiornamento',N'No',N'non necessaria, trattasi dell''aggiornamento di un  flusso interno',N'No',N'nessun virtual manufacturer',N'Stefania Brun',N'Si',N'non necessaria, trattasi dell''aggiornamento di un  flusso interno','2019-06-11 00:00:00',N'a distanza di un anno verificare il flusso del rilascio degli strumenti InGenius. Il flusso di lavoro è stato verificato in sede di audit interno in data 06/06/2019, report n°8-19, rilevando che"il processo corrente relativo al rilascio dello strumento e dei reagenti ELITe InGenius non corrisponde nei tempi e nelle modalità a quanto descritto nella Rev.03 dell''IO09,07".',N'Roberta  Paviolo',N'Negativo',N'Revisionare il processo con le parti coinvolte definendo secondo quali tempistiche occorre effettuare il controllo della documentazione relativa agli strumenti/reagenti e richiedendo a PSS di indicare i seriali da spedire e di avvisare circa l''avvenuto caricamento di nuova documentazione sull''FTP. Aggiornare l''IO09,07 di conseguenza.

17/11/2020: permane la problematica del caricamento della documentazione relativa agli strumenti/reagenti nei tempi consoni a poter eseguire il controllo al rilascio da parte di EGSpA. GSC si è interfacciato ripetutamente con PSS che si giustifica con la mancanza di spazio sufficiente sul server usato per la condivisione dei documenti, problematica non reale secondo GSC. Vista l''impossibilità nel trovare accordi con PSS e visto che una soluzione drastica, come imporre che senza i documenti di collaudo caricati lo strumento non può essere spedito oppure non viene pagato, non è realizzabile in tempi di pandemia nè si giudica opportuna a livello economico, si ritiene che questa azione correttiva possa essere chiusa con valutazione dell''efficacia negativa. Il processo così come strutturato non è efficace né efficiente. ',N'30/11/2020',N'Negativo',NULL,'2018-06-08 00:00:00','2020-11-18 00:00:00'),
    (N'17-34',N'Correttiva','2017-12-29 00:00:00',N'Stefania Brun',N'Sistema qualità',N'Non conformità',N'17-135',N'Non conformità da audit interno CEISO',N'Documentazione SGQ',N'Nonostante il modulo per le NC MOD13,01 contenga i campi relativi a "processo VMP" e impatto "Risk assessment" non è esplicitato il razionale che giustifichi la scelta dell''azione intrapresa in base al rischio della NC così come su RAC/RAP e Change control. Sulle RAC/RAP si riporta la decisione finale sulla azione scelta senza riportare il razionale basato su una valutazione rischi /benefici. Queste valutazione sono affrontate nel corso di riunioni specifiche ma no trovano formalizzazione sui moduli di NC, Change Control e RAC/RAP (sottoprocesso GM1)',N'Revisionare le procedure delle NC, azioni correttive/preventive, reclami in modo da indicare l''approccio basato sui rischi nella valutazione della NC/reclamo e nella scelta di una determinata azione correttiva',NULL,N'impatti positivi in quanto permetteranno di tracciare l''approccio basato sui rischi
Rispondenza completa alle prescrizioni della ISO13485:2016 e alla linea guida MDSAP G0002.1004 Companion document',N'29/12/2017
10/01/2018',N'Revisione delle seguenti PR: PR13,01; PR14,02; PR14,01
Approvazione delle nuove revisioni delle procedure PR13,01; PR14,02; PR14,01',N'PR14,01_02 e PR13,01_02 in uso al 10/09/19 - rac17-34_mess_in_uso_pr1401_02_e_pr1301_02rap18-21_rap18-19_rap17-34.msg
PR14,02 in uso al 09/09/2019 - rac17-34_messa_in_uso_pr1402_04_rap18-21_mod1802_120919.msg
 PR14,01_02 e PR13,01_02 in uso al 10/09/19  - rac17-34_mess_in_uso_pr1401_02_e_pr1301_02rap18-21_rap18-19_rap17-34_1.msg
 PR14,02 in uso al 09/09/2019  - rac17-34_messa_in_uso_pr1402_04_rap18-21_mod1802_120919_1.msg',NULL,N'29/12/2017
10/01/2018',NULL,N'16/09/2019
16/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'Aggiornamento delle procedure con l''approccio basato sul rischio per motivare la decisione di intraprendere una determinata azione correttiva. ',N'ALTRO - Altro
GM - Gestione delle modifiche',N'ALTRO - ALTRO
GM8 - Valutazione del rischio sul processo (CC - RAC/RAP) ',N'GM8 nessun impatto',N'No',N'non necessaria',N'No',N'non necessaria',N'Stefania Brun',N'No',NULL,NULL,N'Verificare l''assenza di NC da parte dell''ente di certificazione relative alla mancata applicazione dell''approccio basato sui rischi per NC, RAC/RAP, reclami. 
Come da Report DEKRA del 19/09/2019, numero Report identification: 2235223-AR14-R0 DRAFT, non ci sono state NC imputabili all''approccio basato sui rischi per NC, RAC/RAP, reclami. ',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2019-09-16 00:00:00','2020-01-03 00:00:00'),
    (N'17-27',N'Correttiva','2017-11-22 00:00:00',N'Federica Farinazzo',N'Controllo qualità',NULL,NULL,NULL,N'Documentazione SGQ',N'Parziale inefficacia della RAC26-16, evidenziabile solo da maggio 2017. ',N'Data l''inefficacia della modifica già intrapresa e la recente introduzione del preCQ per i CTR contenenti più plasmidi, (modalità più efficace per la valutazione dei punti da utilizzare 955-pXXX-L-TRI6 per le preparazioni dei CTR) ,  si chiede di valutare l''adozione di tale procedura anche per i CTRD200ING, CTRD01-II ,CTRD01-V, CTRD08, CTRD13 e CTRD14. In particolare se per i CTR Alert gli impatti sono considerati troppo elevati si chiede la modifica del ciclo termico utilizzato aumentando i cicli da 30 (IFU) a 35. Tale modifica verrebbe comunque implementata in fase di QC come azione a breve termine, al fine di non incorrere in false non conformità, in caso di accettazione della richiesta di modifica.',NULL,N'Diminuzione del numero di CQ ( per ingenius da 6 a 1 PCQ )
modifica dei documenti di CQ e Produzione (10 Moduli + IO)
Si allega piano delle attività
Diminuzione del numero di CQ ( per ingenius da 6 a 1 PCQ )
modifica dei documenti di CQ e Produzione (10 Moduli + IO)
Si allega piano delle attività
Riduzione delle attività di preprazione dei punti per CTR  ( per ingenius 18 punti di diluzione in meno + preCQ)
Valutazione degli impatti sul prodotto della modifica richiesta
Valutazione e messa in uso dei nuovi documenti',N'22/11/2017
22/11/2017
21/11/2017
21/11/2017
21/11/2017',N'Si allega piano delle attività
Valutazione degli impatti sul prodotto della modifica richiesta
Valutazione e messa in uso dei nuovi documenti',N'piano - rap17-27_ctrda_action_plan.xlsx
Moduli messi in uso come da piano allegato - rac17-27_messa_in_uso_moduli_.pdf
piano aggiornato con i MOD10,13-xxx - rac17-27_messa_in_uso_mod1013-xx_mod1022-xx_io1006_nc17-143_rac17-27.msg
moduli in uso (MOD10,13) come da piano aggiornato - rac17-27_messa_in_uso_mod1013-xx_mod1022-xx_io1006_nc17-143_rac17-27_1.msg
messa in uso MOD10,13-determinazioneAllelica rev01, MOD18,02 del 30/10/2018 - rap18-18_messa_in_uso_modxxxcorrezioni_+_mopd1013-det.allelica_rac17-27_rap18-28_2.msg
messa in uso moduli come da piano allegato - rac17-27_messa_in_uso_moduli__1.pdf',NULL,N'22/11/2017
21/11/2017
21/11/2017',NULL,N'16/07/2018
31/05/2018
31/05/2017',N'Si',N'DMRI2016-001, DMRI2016-002, DMRI2016-005, DMRI2017-005, DMRI2018-005, DMRI2018-006',N'No',N'non necessaria',N'Si',N'nessun impatto',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'non necessaria',N'No',N'non necessaria',N'No',N'non necessaria',N'Federica Farinazzo',N'No',NULL,'2019-12-31 00:00:00',N'A 02/01/209 non sono presenti NC associate a questa azione correttiva. La RAC rimane aperta in quanto i seguenti DMRI2016-001, DMRI2016-002, DMRI2016-005, DMRI2017-005, DMRI2018-005, DMRI2018-006 non sono aggiornati',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2018-07-16 00:00:00','2019-01-02 00:00:00'),
    (N'17-16',N'Correttiva','2017-07-27 00:00:00',N'Federica Farinazzo',N'Produzione',N'Non conformità',N'17-75',N'NC17-75 955-PP210-L-TRI5/4/3/2 
NC17-45 955-pAdenoE-L',NULL,N'Errore tecnico di dispensazione',N'Si richiede di rivedere le modalità di calcolo dei volumi di plasmidi in modo che risulti più fruibile sia per i calcoli ch eper le dispensazioni dei campioni',NULL,N'Modifica delle modalità di calcolo
modifica dei documenti',N'22/09/2017
22/09/2017',N'Modifica delle modalità di calcolo
modifica dei documenti',N'elenco moduli modificati - rac17-16_messa_in_uso_moduli_.pdf',NULL,N'22/09/2017
22/09/2017',NULL,N'31/05/2018
31/05/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,NULL,N'No',NULL,'2019-01-08 00:00:00',N'A gennaio 2019 non sono presenti NC imputabili ai nuovi moduli MOD955-Pxxx-ic-cq, mod955-Pxxx-ep-DILUIZIONEplASMIDE, MOD955-pXXX-CTR-multipli.',N'Roberta  Paviolo',N'Positivo',NULL,NULL,NULL,NULL,'2018-05-31 00:00:00','2019-01-08 00:00:00');

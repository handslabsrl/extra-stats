-- DROP TABLE ZZ_list_change
IF NOT EXISTS (
    select * from sysobjects where name='ZZ_list_change' and xtype='U'
) CREATE TABLE ZZ_list_change (
    [N] NVARCHAR(100),
    [Data_apertura] NVARCHAR(100),
    [Aperto_da] NVARCHAR(100),
    [Oggetto_del_cambiamento] NVARCHAR(100),
    [Richiesta_di_cambiamento] NVARCHAR(4000),
    [Valore_aggiunto] NVARCHAR(4000),
    [Conseguenze_se_il_cambiamento_non_fosse_implementato] NVARCHAR(1000),
    [Prodotto] NVARCHAR(1000),
    [Funzioni_riesame] NVARCHAR(100),
    [Area] NVARCHAR(500),
    [Descrizione_impatti] NVARCHAR(2000),
    [Data_approvazione] NVARCHAR(500),
    [Azione] NVARCHAR(2000),
    [Allegati] NVARCHAR(MAX),
    [Funzioni_coinvolte] NVARCHAR(500),
    [Data_approvazione_1] NVARCHAR(500),
    [Data_di_implementazione] NVARCHAR(500),
    [Data_completamento] NVARCHAR(500),
    [DMRI_Prodotto] NVARCHAR(100),
    [DMRI_Prodotto_testo] NVARCHAR(250),
    [Comunicazione_al_cliente] NVARCHAR(100),
    [Comunicazione_al_cliente_testo] NVARCHAR(250),
    [Impatto_sul_prodotto_immesso_in_commercio] NVARCHAR(100),
    [Impatto_sul_prodotto_immesso_in_commercio_testo] NVARCHAR(250),
    [Processo_VMP] NVARCHAR(1000),
    [Sottoprocesso] NVARCHAR(1000),
    [Risk_Assessment] NVARCHAR(2000),
    [Notifica_Autorit] NVARCHAR(100),
    [Descrizione_Notifica_Autorit] NVARCHAR(1000),
    [Notifica_Virtual_Manufacturer] NVARCHAR(100),
    [Descrizione_Notifica_Virtual_Manufacturer] NVARCHAR(500),
    [Modifica_analisi_del_rischio_prodotto] NVARCHAR(100),
    [Interventi_analisi_del_rischio] NVARCHAR(1000),
    [Project_leader] NVARCHAR(100),
    [Data_di_implementazione_CC] NVARCHAR(100),
    [Allegati_1] NVARCHAR(100),
    [Data_chiusura] NVARCHAR(100),
    [CC_non_approvato] NVARCHAR(100),
    [Motivo_della_mancata_approvazione] NVARCHAR(1000),
    [Data_non_approvazione] NVARCHAR(100)
);
INSERT INTO ZZ_list_change VALUES
    (N'21-36','2021-06-04 00:00:00',N'Stefania Brun',N'Documentazione SGQ',N'Modifica della frequenza di compilazione del report relativo alla sorveglianza post-market dei prodotti dell''Allegato II elenco B direttiva 98/79/EC attraverso il MOD19,21 "Sorveglianza post-produzione" in modo da renderlo semestrale anzichè trimestrale',N'Ottimizzazione delle risorse e dei tempi dal momento che si è constatato che i dati impiegati per la sorveglianza analizzati trimestralmente non sono significativi in quanto sono ridotti dato il periodo limitato',N'Avere pochi dati disponibili ai fini della sorveglianza post-market relativi a 3 mesi con maggiore dispendio di tempo e risorse',N'910-RTK015PLD - CMV ELITe MGB KIT 100 T
962-CTR015PLD - CMV - ELITe Positive Control
962-CTR097 - CHLAMYDOPHILA pn Pos.Contr.25T
962-CTR098 - CHLAMYDIA Tr Pos. Contr. 25 T
962-CTR098PLD - CHLAMYDIA tr.ELITe Pos.Con.12T
962-CTR533ING - STI-ELITe Positive Control
962-CTRT01PLD - TOXO g.re ELITe Pos.Contr. 12T
962-RTK015 - Q-CMV RTime Complete Kit 96 T
962-RTS097-M - CHLAMY pn Q-PCR AmpliMIX 96T
962-RTS098-M - CHLA tr. Q-PCR AmpliMIX 96T
962-RTS533ING - STI ELITe MGB Panel
962-STD015 - CMV Q-PCR STANDARD 8 Test
962-STD015PLD - CMV ELITe STANDARD 8 Test
962-CTR553ING - Respirat. Bacter. Pos. Control
962-RTS553ING - Respirat. Bacterial MGB Panel
962-CTR400ING - STI PLUS - ELITe Posit Control
962-RTS098PLD.- - CHLAMY tr. ELITe MGB KIT 100T
962-RTS400ING - STI PLUS ELITe MGB Kit
962-RTST01PLD.- - TOXOPLASMA ELITe MGB Kit
962-CTR015PLD-R - CMV - ELITe Positive Control R',N'QARA
RAS',N'Regolatorio
Sistema qualità',N'Positivo in quanto l''arco temporale di 6 mesi permette di avere un quadro più ampio di dati sul prodotto ai fini della sorveglianza
Positivo in quanto permette di ottimizzare tempo e risorse ',NULL,N'Valutare se la modifica è significativa e necessita di essere comunicata all''ON
Revisionare il MOD19,21 al fine di modificare la frequenza da 3 a 6 mesi ',NULL,N'QM
RAS',NULL,NULL,NULL,N'No',N'No in quanto non è contemplato dal DMRI',N'No',N'No in quanto non riguarda l''utilizzatore',N'No',N'No in quanto non ha impatti sul prodotto',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'Da valutare se la modifica della frequenza richiede aggiornamento del RA',N'No',N'No ',N'No',N'No in quanto non riguarda prodotti gestiti attraverso un contratto con un Virtual Manufacturer',N'No',N'non impatta sull''analisi dei rischi',N'Stefania Brun',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-35','2021-05-28 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Eliminare la data e firma di tutti i rocchetti di etichette in fase di controllo al ricevimento. Mantenere solo un controllo a campione: almeno un rocchetto di etichette per ogni ciclo di stampa rilasciato dal fornitore. Il controllo a campione svolto su un''etichetta per confronto con l''etichetta matrice.',N'Il controllo a campione è più efficace ed accurato. I tempi di controllo al ricevimento si riducono, rendendo il processo più efficiente ed efficace.',N'Elevati tempi di rilascio del prodotto conforme in fase di controllo al ricevimento. Rischio di un controllo più formale che sostanziale, con rischio di non intercettare eventuali imprecisioni nelle etichette',NULL,N'QARA
QM
RDM
PW-
TGSWC',N'Acquisti
Magazzino
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini di attività.
Eliminare la data e firma di tutti i rocchetti di etichette in fase di controllo al ricevimento non influisce sul controllo in ingresso delle etichette. Infatti, si mantiene un controllo a campione (almeno un rocchetto di etichette per ogni ciclo di stampa rilasciato dal fornitore) svolto su un''etichetta per confronto con l''etichetta matrice e si mantengono separati spazialmente i luoghi di stoccaggio delle etichette conformi (magazzino di produzione) rispetto ai luoghi di stoccaggio delle etichette da controllare o che risultassero NC in fase di controllo al ricevimento (magazzino, fisicamente distanziato dal mag. di produzione). Si ritiene che tale controllo sia più efficace e più efficiente in termini di tempistiche. 
L''eliminazione della data e firma di tutti i rocchetti di etichette riduce i tempi di controllo al ricevimento, rendendo disponibili le etichette in tempi minori.
La prevenzione del mescolamento delle etichette in fase di produzione è già strutturata ',N'31/05/2021
31/05/2021',N'- chiedere al fornitore una dichiarazione che ci confermi che ogni scatola contiene rocchetti derivanti da un unico ciclo di stampa, per cui tutte le etichette contenute sono identiche: sarebbe opportuno che ogni ciclo di stampa abbia un lotto o una data di stampa (se in una giornata c''è un unico ciclo di stampa)
- contattare il fornitore per verificare i controlli di processo sulla stampa delle etichette (es. verifica con la cianografia originale prima di avviare la stampa definitiva, verifica con telecamere, altro). A seguito della risposta valutare se necessario avere fornitori alternativi
- valutare l''opportunità di fornitori alternativi 
formazione al personale sull''esecuzione dei controlli al ricevimento sul materiale etichette
Verificare quali SPEC954-xxx archiviare a seguito della dismissione prodotti
Migliorare lo stoccaggio dei rocchetti in uso durante le fasi di produzione per la stampa delle etichette degli STD
nessuna attività a carico
aggiornamento di tutte le specifiche etichette',NULL,N'PW-
TGSWC
PP
QARA
RS
QAS
MM
PCT',N'31/05/2021
31/05/2021',NULL,NULL,N'No',N'nessun impatto',N'No',N'non necessaria, si tratta di una modifica interna ad un processo',N'No',N'nessun impatto',N'R - Ricevimento / stoccaggio Materiali e Strumenti',N'R3 - controllo al ricevimento',N'il processo di controllo al ricevimento non cambia, cambia un controllo specifico per i codici delle etichette. il RA R3 non è da modificare',N'No',N'vedere MOD05,37',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',N'Anna Capizzi',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-34','2021-05-25 00:00:00',N'Alessandra Gallizio',N'Processo di CQ e criteri di CQ',N'A seguito della definizione del Fattore di Conversione per il kit HCV ELITe MGB kit, il materiale di riferimento PEI è stato caratterizzato vs il WHO ed il titolo risultante è 65,000 IU/ml invece del valore dichiarato 80,000 IU/mL. 
In conseguenza del cambiamento, il controllo qualità dei tre lotti pilota del kit HCV verrà ripetuto utilizzando sia il fattore di conversione definitivo (2,36), sia la nuova quantificazione del materiale di riferimento PEI.
L''Assay Protocol con il fattore di conversione definitivo verrà caricato sugli strumenti del CQ.
Si richiede pertanto una modifica della procedura di Controllo Qualità di HCV in associazione a ELITe InGenius, mediante la revisione del modulo di preparazione del materiale di riferimento PEI (MOD957-QC-PEI_xxx) e la revisione del modulo Scheda Appunti (MOD10,13-RTK601ING).',N'la modifica permette di effettuare il Controllo Qualità su ELITE InGenius utilizzando correttamente il materiale di riferimento PEI',N'utilizzo non corretto del materiale di riferimento PEI',N'964-RTK601ING - HCV ELITe MGB Kit',N'QARA
QC
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'nessun impatto
nessun impatto

nessun impatto',NULL,N'revisione del modulo di preparazione del materiale di riferimento PEI (MOD957-QC-PEI_xxx) e la revisione del modulo Scheda Appunti (MOD10,13-RTK601ING)
revisione Assay protocol
messa in uso documenti',NULL,N'QCT/DS
RS
QAS
RAS',NULL,NULL,NULL,N'No',N'nessun impatto',N'No',N'non necessaria, prodotto non ancora immesso in commercio',N'No',N'non necessaria, prodotto non ancora immesso in commercio',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'non necessaria, prodotto non ancora immesso in commercio',N'No',N'nessun virtual manufacturer',N'No',N'no',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-33','2021-05-20 00:00:00',N'Gianmarco  LUNARDI',N'Documentazione SGQ',N'Si richiede la modifica delle seguenti specifiche:
- SPEC954-073_00_Nastro: cambio del codice del fornitore, il vecchio fornitore TECMARK srl F01968 è stato acquisito da FINLOGIC SpA F07830. ',N'Anagrafiche aggiornate.',N'Non si potrebbero emettere nuovi ordini.',N'954-073 - Nastro, f.to 110mmx270mt APR6',N'MM
PW-',N'Acquisti
Produzione
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo, si acquista dal fornitore corretto, senza emettere ordini/richiedere offerte a contatti errati.
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività',N'26/05/2021
26/05/2021
26/05/2021
26/05/2021',N'approvazione specifica
Aggiornare la SPEC954-073_00_Nastro: cambio del codice del fornitore, il vecchio fornitore TECMARK srl F01968 è stato acquisito da FINLOGIC SpA F07830. 
messa in uso documento.
la modifica non è significativa, in quanto materiale non critico. ',NULL,N'RS
PW-
QAS
QARA
QAS',N'26/05/2021
26/05/2021
26/05/2021',NULL,NULL,N'No',N'NESSUN IMPATTO',N'No',N'non necessaria, trattasi di documentazione interna',N'No',N'NESSUN IMPATTO',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'NESSUN IMPATTO su RA A2',N'No',N'vedere MOD05,36. la modifica non è significativa, in quanto materiale non critico. ',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Anna Capizzi',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-32','2021-05-18 00:00:00',N'Maria Lucia Manconi',N'Nuova revisione di software associati ad IVD',N'Il fornitore PSS chiede di poter implementare una software patch che permetta di mettere in produzione a partire da Luglio 2021 strumenti ELITe InGenius con le seguenti caratteristiche: 
- Assenza del PCR Cool Block, come richiesto dal documento di PSS "ECR/ECN: 159", necessaria per ridurre i costi di produzione senza generare alcun impatto sugli strumenti per EGSpA che non utilizzano il PCR Cool Block. 
- nuova PC Motherboard e nuovo Sistema operativo Windows 10, come già trattato nel CC21-15, i nuovi PC associati allo strumento conterranno il sistema operativo Windows 10 in quanto  Windows 7 è in obsolescenza e i vecchi PC non supportano il Windows 10.
- Calibrazione dei LED utilizzando i nuovi dyes da 500 nm, come richiesto dal documento di PSS "ECR/ECN: 161", che evitano di portare il canale 1 della nuova ottica in saturazione come avviene utilizzando i dyes attuali da 600 nm.
La nuova patch non verrà installata sugli strumenti attualmente in campo, ma solamente in quelli di nuova produzione a partire da Luglio 2021. La nuova versione software 1.3.0.17, gestita nella RAP21-15, conterrà anche queste modifiche e potrà essere installata sugli strumenti in campo. ',N'La nuova software patch permetterà di avere strumenti a un prezzo inferiore, con il sistema operativo più aggiornato ed evita possibili problemi di saturazione del canale 1 durante la calibrazione delle ottiche. ',N'Senza l''implementazione della patch, il fornitore non potrebbe mettere in produzione nuovi strumenti a partire da Luglio 2021 a causa dell''irreperibilità di PC con sistema operativo Windows 7 e si continuerebbero ad avere problemi con il canale 1 in saturazione durante la calibrazione. ',NULL,N'QARA
RDM
SIS',N'Program Managment
Regolatorio
Ricerca e sviluppo',N'Attività da svolgere. L''impatto è positivo, la nuova sw patch non influirà sulle funzionalità in essere del SW. La nuova patch sarà installata solamente sugli strumenti che usciranno dalla produzione a partire da Luglio 2021 e fino al rilascio della nuova versione software a settembre 2021. Nessuno di questi strumenti sarà spedito negli Stati Uniti, pertanto non sarà necessario effettuare alcun tipo di valutazione 510k sulla patch. Quando la patch sarà inclusa nella versione software 1.3.0.17, a settembre 2021, sarà fatta la valutazione di impatto 510k.
Attività da svolgere. L''impatto è positivo, la nuova sw patch non influirà sulle funzionalità in essere del SW. La nuova patch sarà installata solamente sugli strumenti che usciranno dalla produzione a partire da Luglio 2021 e fino al rilascio della nuova versione software a settembre 2021. Nessuno di questi strumenti sarà spedito negli Stati Uniti, pertanto non sarà necessario effettuare alcun tipo di valutazione 510k sulla patch. Quando la ',NULL,N'Seguire il CC garantendo che tutte le attività vengano svolte.
Interfacciarsi con il fornitore per la documentazione relativa alla software patch 
Revisione documentazione fornita dal fornitore
Stesura del Summary Report 
Valutazione dell''aggiornamento dei requisiti software 
Stesura Release Note a fine progetto
Revisione documentazione fornita dal fornitore
Revisione del Summary Report  ',NULL,N'SIS
SIS
SIS
SIS
SIS
QARA
RAS
RDM
RS
RDM
RS',NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-31','2021-05-18 00:00:00',N'Maria Lucia Manconi',N'Documentazione SGQ',N'In accordo con i test fatti da PSS e come riportato nei documenti "P34001PV016-00 ELITe BeGenius Pipetting Test Report" e "P34001PV032-00_ELITe BeGenius_pre-production Cooler Unit Environmental Test Report", si richiede l''aggiornamento del "Product Requirement Document" (PRD2019-007, Rev.01) e del "Software Requirement Document" (SRD2019- 001, Rev.01), relativi al progetto SCP2018-024 (REF INT040), per l''allineamento dei requisiti alla documentazione di PSS contenuta nel DHF. L''introduzione delle modifiche non impattano i requisiti funzionali. 
Si richiede inoltre l''aggiornamento dei TEST PROTOCOL (vedere attività PM) per ampliare le liste di prelievo materiali, allineare gli step dei test in base alle specifiche dello strumento ed allinearli alle revisioni 02 dei documenti di PRD e SRD.',N'L''aggiornamento dei documenti di PRD e SRD consente di disporre di documenti di definizione dei requisiti più completi e allineati con la documentazione del DHF,  a seguito della chiusura della fase di sviluppo. 
Le modifiche proposte nei documenti di Test Protocol li rendono più intuitivi per l''utente che li deve svolgere. ',N'Il mancato aggiornamento dei documenti determinerebbe: 
- documenti di definizione dei requisiti (PRD e SRD) meno informativi; 
- esecuzioni dei test protocol imprecise e inesatte con perdita di tempo per la ricerca del materiale mancante. ',NULL,N'GSC
QARA
RDM
SIS
IMM
PVM
IASM&MMI',N'Global Service
Marketing
Marketing Internazionale
Program Managment
Regolatorio
Ricerca e sviluppo
Validazioni',N'Impatto positivo. Le modifiche proposte sono il risultato del confronto avvenuto nel corso della fase di Sviluppo fra il Team di EG SpA (compresa la funzione Program Management) e PSS. L''implementazione del CC consente di avere dei requisiti e dei test protocol maggiormente allineati e adeguati rispetto ai risultati finora ottenuti e di tracciare le modifiche apportate.
Impatto positivo. Le modifiche proposte sono il risultato del confronto avvenuto nel corso della fase di Sviluppo fra il Team di EG SpA (compresa la funzione Program Management) e PSS. L''implementazione del CC consente di avere dei requisiti e dei test protocol maggiormente allineati e adeguati rispetto ai risultati finora ottenuti e di tracciare le modifiche apportate.
Impatto positivo. Le modifiche proposte sono il risultato del confronto avvenuto nel corso della fase di Sviluppo fra il Team di EG SpA (compresa la funzione Program Management) e PSS. L''implementazione del CC consente di avere dei requisiti e dei test protocol maggiormente',NULL,N'Seguire il CC garantendo che tutte le attività vengano svolte.
Aggiornare e approvare i documenti PRD2019-007 e SRD2019-001 
Condividere i documenti PRD2019-007 e SRD2019-001 con PSS per loro revisione/approvazione. 
Aggiornare e approvare i documenti di Test Protocol:
- TP2021-002, Rev.00: Test Protocol Consumable Scan Management ELITe BeGenius INT040 
- TP2021-004, Rev.00: Test Protocol Barcode Printer Management ELITe BeGenius INT040
- TP2021-005, Rev.00: Test Protocol Hepa Filter Management ELITe BeGenius INT040
- TP2021-006, Rev.00: Test Protocol UV Lamp Management ELITe BeGenius INT040
- TP2021-007, Rev.00: Test Protocol Login & Logout Management ELITe BeGenius INT040
- TP2021-009, Rev.00: Test Protocol Software Versioning Management ELITe BeGenius INT040
- TP2021-012, Rev.00: Test Protocol Home & Preferences Management ELITe BeGenius INT040
- TP2021-013, Rev.00: Test Protocol Error & Database Management ELITe BeGenius INT040
- TP2021-016, Rev.00: Test Protocol Startup Management ELITe B',NULL,N'SIS
SIS
SIS
SIS
MS
IASM&MMI
MS
IMM
PVS
PVM
RDM
RS
RDM
RT
GSC
GSS
QARA
QM
QAS
RAS
QARA
QM
QAS
RAS',NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-30','2021-05-18 00:00:00',N'barbara pirani',N'Progetto',N'Si chiede l''implementazione del software Results Manager al fine di:
-Permettere la lettura dei valori delle temperature di melting, da parte del Results Manager a partire da sessioni effettuate sugli strumenti ABI 7500FastDx, CFX96 e ARIA Dx. 
-Permettere la possibilità di scelta del carattere maiuscolo o minuscolo, nel campo Abbreviation.  
-Permettere la configurazione dei risultati quantitativi in notazione scientifica esponenziale o standard.
Le richieste sono tracciate su Mantis con i tickets 481 480 478',N'Queste funzionalità sono necessarie per permettere le installazioni dei Dump per i kit che si basano sulle temperature di melting (ad esempio SARS-CoV-2 Varianti) presso i clienti, per supportare e migliorare l''attività da parte dello Specialist nell''installazione dei Dump, per facilitare la visualizzazione dei risultati quantitativi da parte dei clienti.',N'Senza la possibilità della lettura, da parte del Results Manager, dei valori delle temperature di melting a partire da sessioni effettuate sugli strumenti ABI 7500FastDx, CFX96 e ARIA Dx, i clienti non possono usufruire dell''interpretazione dei risultati di sessioni che sfruttano la temperatura di melting per identificare i target (ad esempio per il kit SARS-CoV-2 Varianti).
Senza la possibilità di scelta del carattere maiuscolo e minuscolo nel campo Abbreviation, ci sarebbero problemi di interfacciamento con il LIS per quei Dump i cui target hanno un nome scritto con un misto di lettere maiuscole e minuscole (ad esempio il target RdRd nel Dump del kit SARS-CoV-2).
Senza la possibilità di scelta del formato dei risultati, quest''ultimi vengono visualizzati solo in formato scientifico.',NULL,N'FPS
PVS
QM
RT
ITM
SIS',N'Assistenza Applicativa
Assistenza strumenti esterni (Italia)
Program Managment
Ricerca e sviluppo
Sistema informatico
Sistema qualità
Validazioni',N'Le modifiche apportate al sw Result Manager NON hanno impatto sulle funzionalità in essere e non alterano le prestazioni del sw dichiarate dal fabbricante. Le modifiche hanno un impatto positivo in quanto permettono le installazioni dei Dump per i kit che si basano sulle temperature di melting (ad esempio SARS-CoV-2 Varianti) presso i clienti, migliorano l''attività da parte dello Specialist nell''installazione dei Dump, facilitano la visualizzazione dei risultati quantitativi da parte dei clienti.
Le modifiche apportate al sw Result Manager NON hanno impatto sulle funzionalità in essere e non alterano le prestazioni del sw dichiarate dal fabbricante. Le modifiche hanno un impatto positivo in quanto permettono le installazioni dei Dump per i kit che si basano sulle temperature di melting (ad esempio SARS-CoV-2 Varianti) presso i clienti, migliorano l''attività da parte dello Specialist nell''installazione dei Dump, facilitano la visualizzazione dei risultati quantitativi da parte dei clienti.
Impatto in t',NULL,N'Supervisionare attività fornitore , revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24)
Collezionare i seguenti documenti da parte del fornitore: Product Certificate_Vxxxx, RM-ResultsManagerVxxxx, RM-ResultsManagerVerificationSummaryReport,ETGrequiredchangesvxxxx
Revisionare piano e report di validazione (MOD05,23 MOD05,24), preparare eventuali dump di supporto durante i test
Comunicazione al cliente del cambiamento, training al cliente, supportare interfacciamento con Middleware e LIS.
Supportare la connettività della nuova versione verso middleware e LIS.
Redigere piano di validazione, validazione del sw e stesura del report di validazione secondo modello MOD05,23 MOD05,24 in collaborazione con R&D.
Messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente , revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24).
Revisionare e approvare piano e re',NULL,N'SIS
RDM
RT
SIS
FPS
FPSC
CSC
PVS
PVM
QARA
QM
QAS
ITM',NULL,N'30/07/2021
30/07/2021',NULL,N'No',N' na',N'Si',N'si',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW46 - Result Manager',N'nessun impatto',N'No',N'na, il sw è ruo',N'No',N'nessun virtual manufacturer',N'No',N'na',N'barbara pirani','2021-07-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-29','2021-05-17 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'In uscita dall''audit 3-21 ai processi di stoccaggio e spedizione si chiede di:
- aggiornare la PR15,02 "Gestione Magazzino" rev03 specificando: l''utilizzo di un''etichetta NC per lo stoccaggio di strumentazione cliente non decontaminata; le tempistiche di prelievo a magazzino. Verificare la necessità di richiesta di autorizzazione a SAD-OP;
- aggiornare la PR15,02 "Gestione Magazzino" rev03 introducendo i magazzini esterni e i requisiti di stoccaggio temporaneo richiesti. da EGSpA;
- aggiornare la IIO15,01 "Imballaggio e Spedizione EG SpA-Cliente" rev01  introducendo il controllo aggiuntivo relativo all''indicazione del destinatario e del mittente direttamente sull''imballo e revisionare l''istruzione nelle parti non verificate durante questo audit; 
- aggiornare la IO15,02 "Scarichi di Magazzino" rev01 trattando carichi e scarichi di magazzino;
- declassare il fornitore ILMED da fornitore outsourcing a fornitore di servizi riferiti al prodotto (livello 2.10);
- aggiornare la IO15,03 "Ordine, Imballaggio e Spedizione EGSpA-Magazzino Esterno" rev02  con le attività svolte da ILMED ed indicare accorgimenti sullo stoccaggio della merce negli scaffali bassi per prevenire temperatura fuori dai limiti di accettazione.
- Effettuare dei monitoraggi della temperatura nel periodo estivo in ILMED, attività da gestire durante un audit presso il magazzino.',N'Allineamento della documentazione alle attività svolte.
Introduzione del controllo aggiuntivo relativo all''indicazione del destinatario e del mittente direttamente sull''imballo.
Verifica della temperatura di stoccaggio nel magazzino esterno ILMED.
Ri-classificazione del magazzino esterno ILMED che da fornitore in outsourcing può essere declassato a fornitore di servizio.',N'Documentazione disallineata. nessuna evidenza sulla temperatura di stoccaggio nel magazzino esterno ILMED.',NULL,N'QARA
QM
PW-
TGSWC',N'Acquisti
Magazzino
Sistema qualità',N'Ri-classificazione del magazzino esterno ILMED da fornitore in outsourcing a fornitore di servizio. 
Impatto positivo, la documentazione si allinea con le attività svolte.
Ri-classificazione del magazzino esterno ILMED da fornitore in outsourcing a fornitore di servizio. 
Impatto positivo, la documentazione si allinea con le attività svolte.
Impatto positivo, la documentazione si allinea con le attività svolte. Il controllo aggiuntivo relativo all''indicazione del destinatario e del mittente direttamente sull''imballo, dovrebbe prevenire i possibili errori del corriere in fase di smistamento degli imballi prima del raggiungimento del cliente finale.
Impatto positivo, la documentazione si allinea con le attività svolte. Il controllo aggiuntivo relativo all''indicazione del destinatario e del mittente direttamente sull''imballo, dovrebbe prevenire i possibili errori del corriere in fase di smistamento degli imballi prima del raggiungimento del cliente finale.
Impatto positivo, si da evidenza delle eff',NULL,N'declassare il fornitore ILMED da fornitore outsourcing a fornitore di servizi riferiti al prodotto (livello 2.10), aggiornare la documentazione del fornitore
aggiornare la IO15,02 "Scarichi di Magazzino" rev01 trattando carichi e scarichi di magazzino, in collaborazione con TGSWC
 aggiornare la PR15,02 "Gestione Magazzino" rev03 specificando: l''utilizzo di un''etichetta NC per lo stoccaggio di strumentazione cliente non decontaminata; le tempistiche di prelievo a magazzino e la suddivisione tra materiale prelavato dal magazzino produzione e quello dal magazzino . Verificare la necessità di richiesta di autorizzazione a SAD-OP;
- aggiornare la PR15,02 "Gestione Magazzino" rev03 introducendo i magazzini esterni e i requisiti di stoccaggio temporaneo richiesti. da EGSpA;
- aggiornare la IO15,01 "Imballaggio e Spedizione EG SpA-Cliente" rev01  introducendo il controllo aggiuntivo relativo all''indicazione del destinatario e del mittente direttamente sull''imballo e revisionare l''istruzione nelle parti non ver',NULL,N'PW-
TGSWC
PW-
QM
QAS
TGSWC
QAS',NULL,N'31/08/2021
31/08/2021
31/08/2021
31/08/2021',NULL,N'No',N'nessun impatto sul DMRI',N'No',N'non necessaria alcuna comunicazione al cliente',N'No',N'nessun impatto sul prodotto immesso in commercio',N'SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'SP5 - Controllo al ricevimento dei prodotti presso magazzino esterno 
SP6 - Stoccaggio dei prodotti presso magazzino esterno 
SP7 - Registrazione del movimento di carico presso il magazzino esterno sul gestionale EGSpA',N'da aggiornare SP5, SP6, SP7, creazione di un RA per la spedizione dai magazzini temporanei al magazzino EGSpA',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Daniele Salemi','2021-08-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-28','2021-05-05 00:00:00',N'Federica Rinaldo',N'Documentazione SGQ',N'Si richiede di adottare 1 ML come unità minima  a cui riferire le quantità dei componenti, in sostituzione a kit/equivalenti, per le schede di preparazione dei semilavorati "buffer"',N'Uniformare il calcolo delle quantità da produrre in miscele composte da quantità diverse di buffer',N'La pianificazione della quantità da produrre è meno soggetta ad errori',N'955-5X-ELITE-BUFFER - 5x ELITe Buffer
955-TAQBUF - 12,5x Taq Buffer
955-5XONESTEPBUFFER - 5X OneStep Buffer
955-5X-PCR-BUFFER-CT - 5X PCR Buffer CT',N'MM
QC
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini di attività
Uniformare il calcolo delle quantità da produrre in miscele composte da quantità diverse di buffer
Uniformare il calcolo delle quantità da produrre in miscele composte da quantità diverse di buffer
impatto in termini di attività
Nessun impatto sulla preparazione del reagente in quanto cambia solo il volume finale.',N'31/05/2021
14/05/2021
14/05/2021
31/05/2021
31/05/2021',N'modifica dei modelli e, conseguentemente, dei moduli di produzione 
simulazione del documento ed approvazione
Modifica delle distinte base dei 4 buffer, cambiando unità di misura dal VOL di 1 kit a 1 mL e le quantità relative di ciascun componente.
verifica della significatività 
messa in uso documenti
nessuna attività',NULL,N'QCT/DS
QAS
PT
PT
RDM',N'31/05/2021
14/05/2021
14/05/2021
31/05/2021
31/05/2021',NULL,N'31/05/2021',N'No',N'nessun impatto',N'No',N'non necessaria, la modifica riguarda documentazione interna, senza impatto sul prodotto',N'No',N'nessun impatto, la modifica riguarda documentazione interna, senza impatto sul prodotto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.4 - Calcolo delle quantità',N'da valutare ',N'No',N'vedere MDO05,36',N'No',N'nessun virtual manufacturer',N'No',N'da verificare',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-27','2021-04-12 00:00:00',N'Renata  Catalano',N'Documentazione SGQ',N'A seguito dell''emissione della nuova norma ISO14971:2019 "Applicazione della gestione del rischio ai dispositivi medici", che sostituisce la ISO14971:2012, risulta necessario aggiornare la documentazione del Sistema di Gestione della Qualità (SGQ) di EG SpA ad essa relativa al fine di soddisfare i nuovi requisiti. Sebbene la ISO14971:2019 risulta essere la norma corrente, la ISO14971:2012 risulta essere la norma attualmente armonizzata. Fino a che l''edizione del 2019 non sarà armonizzata, la documentazione del SGQ conforme alla ISO14971:2012 è considerata valida.',N'L''adeguamento ai requisiti della ISO14971:2019 consente di avere la documentazione del SGQ conforme alla nuova edizione della norma.',N'La documentazione del SGQ in assenza di aggiornamento risulterebbe non conforme alla ISO14971:2019',NULL,N'QARA
QM
QAS
RAS',N'Regolatorio
Sistema qualità',N'Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''adeguamento della documentazione SGQ alla nuova ISO14971:2019 consente di avere la documentazione conforme ai requisiti della normativa corrente
Impatto positivo. L''ad',N'12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021',N'inserire la nuova edizione della norma ISO14971:2019 nel modulo MOD05,03_ELENCHI NORME e salvare la norma nell''apposita cartella sul server
Archiviare la norma ISO14971:2012 eliminandola dal modulo MOD05,03_ELENCHI NORME e archiviando il file relativo nell''apposita cartella, quando l''edizione 2019 sarà armonizzata
Mettere in uso la versione aggiornata della procedura PR22,01 "Gestione dei rischi"
Effettuare valutazione della significatività della modifica attraverso la compilazione del MOD05,36
Aggiornare le dichiarazioni di conformità
esecuzione di una gap analisi per valutare le differenze fra le due edizioni della norma e creazione di un modello aggiornato della documentazione di gestione del rischio per implementare i nuovi requisiti. Il nuovo modello prevederà anche l''ottimizzazione dei modelli documentali al fine di renderne più agevole la compilazione, l''aggiornamento e la fruibilità.
Aggiornamento, approvazione e archiviazione nel FTP della documentazione di gestione dei rischi di tu',NULL,N'RAS
RAS
QAS
RAS
QARA
RAS
RAS
QAS
QM',N'10/05/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021
12/04/2021',N'31/05/2022',NULL,N'No',N'Il cambiamento non richiede l''aggiornamento dei DMRI di prodotto.',N'No',N'La variazione non richiede alcuna comunicazione al Cliente',N'No',N'La variazione non ha alcun impatto sui prodotti immessi in commercio',N'R&D - Svilupppo e commercializzazione prodotti
R&D - Svilupppo e commercializzazione prodotti
DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'R&D1.3 - Fase di Selezione del Prototipo: Pianificazione
R&D1.10 - Fase di Verifica e Validazione: Reportistica
DC13 - Fascicolo tecnico di Prodotto',N'Da valutare',N'No',N'da valutare in base all''esito del MOD05,36',N'No',N'da valutare in base all''esito del MOD05,36',N'Si',N'l''adeguamento ai requisiti della norma ISO14971:2019 richiede l''aggiornamento della documentazione di gestione dei rischi di tutti i prodotti commercializzati da EG SpA',N'Renata  Catalano','2022-05-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-26','2021-04-08 00:00:00',N'Maria Lucia Manconi',N'Nuova revisione di software associati ad IVD',N'Si richiedere al fornitore PSS di implementare una software patch che risolva due bug tracciati come Mantis Ticket 475 e 477. 
La nuova software patch permetterà di impostare il corretto valore del "dilution factor" (Mantis Ticket 475) presente nell''assay protocol al posto del valore (1) attualmente riportato in automatico dal software, durante il setup della routine sullo strumento ELITe InGenius. Tutti gli assay protocol ad oggi rilasciati utilizzano come valore di dilution factor , per cui non ci sono impatti, mentre la nuova software patch è fondamentale per gli assay protocol dei prodotti viral load che saranno rilasciati in futuro in quanto utilizzano un dilution factor differente.
Inoltre fisserà anche la problematica riscontrata nella NC21-35, comunicata a PSS attraverso il  Mantis Ticket 477, e gestita attraverso la RAC21-12: permetterà al sistema di stampare il report del controllo positivo e di visualizzare i risultati della temperatura di Melting dalla schermata di controllo durante l''utilizzo del protocollo di analisi MDR-MTB. 

Per consentire l''utilizzo dei prodotti viral load HIV e HCV (RTK600ING E RTK601ING), la nuova software patch dovrà essere installata con uno script ad hoc (MV.exctraction.scr) per i due prodotti che modifica il porotocollo di estrazione della cassetta ELITe InGenius SP1000 partendo da un campione con volume di 600 uL. ',N'Risolve due bug del software di cui uno fondamentale per il rilascio dei due prodotti viral load.',N'Senza l''implementazione della patch l''utente continuerebbe a non poter stampare il report del controllo positivo e visualizzare i risultati della temperatura di Melting durante l''utilizzo del prodotto MDR-MTB. Inoltre durante il setup della routine, potrebbe utilizzare erroneamente il valore di "dilution factor" (1), assegnato automaticamente dal sw, invece di quello corretto contenuto nell''assay protocol durante l''utilizzo dei prodotti HIV e HCV. Senza questa modifica l''utente potrebbe rilasciare una diagnosi errata del paziente.',NULL,N'CSC
GSC
QARA
RDM
SIS',N'Assistenza strumenti esterni (Italia)
Global Service
Program Managment
Regolatorio
Ricerca e sviluppo',N'L''impatto è positivo, la nuova software patch non dovrebbe influire sulle funzionalità in essere del software: PSS ci fornirà la documentazione ad evidenza.  
La nuova sw patch non verrà installata sugli strumenti USA, pertanto non sarà necessario effettuare alcun tipo di valutazione sul 510k. Tale valutazione sarà svolta in una fase successiva quando la sw patch verrà inclusa nel sw principale.
L''impatto è positivo, la nuova software patch non dovrebbe influire sulle funzionalità in essere del software: PSS ci fornirà la documentazione ad evidenza.  
La nuova sw patch non verrà installata sugli strumenti USA, pertanto non sarà necessario effettuare alcun tipo di valutazione sul 510k. Tale valutazione sarà svolta in una fase successiva quando la sw patch verrà inclusa nel sw principale.
L''impatto è positivo, la nuova software patch non dovrebbe influire sulle funzionalità in essere del software: PSS ci fornirà la documentazione ad evidenza.  
La nuova sw patch non verrà installata sug',N'08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021',N'Seguire il CC garantendo che tutte le attività vengano svolte.
Interfacciarsi con il fornitore per la documentazione relativa alla software patch 
Stesura PLAN e REPORT della verifica interna della nuova sw patch (ATTENZIONE: verificare che sia riportato il riferimento sia a questo CC che alla RAC21-12 che gestisce la correzione legata al prodotto RTS120ING)
Revisione PLAN e REPORT di installazione della nuova sw patch 
Aggiornamento del documento di bug list su Mantis 
Stesura del Summary Report 
Valutazione dell''aggiornamento dei requisiti software 
Revisione PLAN e REPORT della verifica interna della nuova sw patch (ATTENZIONE: riportare il riferimento sia a questo CC che alla RAC21-12 che gestisce la correzione legata al prodotto RTS120ING).
Revisione PLAN e REPORT di installazione della nuova patch
Revisione del Summary Report 
Revisione del documento di bug list su Mantis 
Stesura PLAN e REPORT di installazione della nuova sw patch (ATTENZIONE: riportare il riferimento sia a quest',NULL,N'SIS
SIS
SIS
SIS
SIS
SIS
SIS
RT
RDM
RDM
RT
RDM
RT
GSS
GSS
GSS
CSC
FSE
QARA
RAS
RAS
GSC
GSS',N'08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021
08/04/2021',N'30/06/2021',NULL,N'No',N'nessun impatto',N'No',N'nessun impatto per i prodotti: HIV e HCV sono ancora da immettere in commercio',N'No',N'nessun impatto: i prodotti HIV e HCV non sono ancora immessi in commercio',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'da valutare',N'No',N'na, hiv e hcv non sono ancora immessi in commercio',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Maria Lucia Manconi','2021-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-25','2021-03-26 00:00:00',N'Alessia Cotza',N'Assay Protocol',N'Adeguamento dei Dump File attualmente in uso alla versione del Results Manager 21.2.19.364.
Modifiche che verranno introdotte:
- i dump file SARS-CoV-2 ELITe_7500_00 e SARS-CoV-2 ELITe_CFX96_00 verranno compattati in un unico dump SARS-CoV-2 ELITe_Closed_00.
- i dump file SARS-CoV-2 ELITe_AriaDx_00 e SARS-CoV-2 ELITe_Op_CFX_Qs5_00 verranno compattati in un unico dump SARS-CoV-2 ELITe_Open_00.
- per i dump file SARS-CoV-2 Plus_Op_CFX96_QS5,  RV ELITe MGB® Panel_CFX_00, RB ELITe MGB® Panel_CFX_00 verranno modificati gli External ID nella sezione target del Dump FIle e verrà cambiato il nome del Dump (SARS-CoV-2 Plus_Open_00, RV ELITe MGB® Panel_Open_00, RB ELITe MGB® Panel_Open_00).
- i dump file GF COVID19 Plus_7500_00 e GF COVID19 Plus_CFX_pr01 verranno compattati in un unico dump GF COVID19 Plus_00.
- i dump file GF COVID19 Plus_NCO_7500_00 e GF COVID19 Plus_NCO_CFX_pr01 verranno compattati in un unico dump GF COVID19 Plus_NCO_00.
L''aggiornamento dei Dump File non ha impatto sulle performance dei prodotti.


',N'Possibilità di utilizzare un solo Dump File per sessioni provenienti da diversi strumenti di amplificazione',N'I clienti da cui è stata installata la versione 21.2.19.364 del Results Manager non potrebbero utilizzare alcuni dei Dump File (non adeguati) attualmente in uso',N'962-RTS548ING - Respiratory Viral MGB Panel
962-RTS553ING - Respirat. Bacterial MGB Panel
909-IFMR-45 - GeneFinder™ COVID-19 Plus Real
962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240
962-RTS180ING - SARS-CoV-2 PLUS ELITe MGB Kit',N'QARA
RDM
PVM
IASM&MMI',N'Assistenza Applicativa
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo per allineamento dump file  a nuova versione software
Impatto positivo per allineamento dump file  a nuova versione software
Impatto positivo per allineamento dump file  a nuova versione software
Nessun impatto se non in termini di attività da svolgere
Nessun impatto se non in termini di attività da svolgere
L''aggiornamento dei Dump File non ha impatto sulle performance del prodotto',N'26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021
26/03/2021',N'- installazione presso i clienti delle nuove versioni dei Dump File
- comunicazione e messa in uso dei nuovi Dump File
-La modifica degli AP non è considerata significativa.

Modifica dei Dump File
Verifica dei Dump File',NULL,N'FPS
GPS
QAS
PVS
RT
RS',N'26/03/2021
26/03/2021
26/03/2021
26/03/2021',NULL,NULL,N'No',N'nessun impatto sul DMRI',N'No',N'nessuna comunicazione al cliente',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non è presente un RA per la gestione dei dump file',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-24','2021-03-25 00:00:00',N'Alessandra Gallizio',N'Manuale di istruzioni per l''uso',N'Aggiornamento del manuale del kit MDR/MTB ELITE MGB Kit, RTS120ING per eliminare uno degli scopi previsti  (rilevazione del DNA del Mycobacterium tuberculosis complex) che prevedeva solo l''utilizzo del componente "TB1 PCR Mix" senza l''utilizzo del componente "TB2 PCR Mix" deputato all''identificazione delle principali mutazioni associate alla resistenza alla Rifampicina e/o Isoniazide. ',N'L''eliminazione dello scopo previsto eliminerebbe gli sbilanciamenti tra i componenti "TB1 PCR Mix" e "TB2 PCR Mix". Per i clienti che desiderano effettuare unicamente la rilevazione del DNA Mycobacterium tuberculosis complex è disponibile il nuovo kit MTB EXTRA ELITe MGB Kit, RTS121ING.',N'Si verificherebbe uno sbilanciamento tra i componenti "TB1 PCR Mix" e "TB2 PCR Mix".',N'962-RTS120ING - MDR TB ELITe MGB Kit',N'QARA
IMM
IASM&MMI',N'Acquisti
Marketing
Regolatorio
Ricerca e sviluppo
Validazioni',N'La modifica non ha impatto sul prodotto o sulle sue performance. Ha però impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
La modifica non ha impatto sul prodotto o sulle sue performance. Ha però impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
La modifica non ha impatto sul prodotto o sulle sue performance. Ha però impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
Impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
Impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
Impatto sulla documentazione di prodotto in corso di stesura al fine di adeguarsi ai requisiti del Regolamento (EU) 2017/746
Impatto sulla documentazione di prodotto',N'25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021',N'messa in uso IFU aggiornato SCH mRTS120ING
verificare significatività della modifica e se necessario identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Aggiornamento della documentazione del prodotto MDR/MTB ELITe MGB Kit in corso di stesura per l''allineamento ai requisiti IVDR modificando lo scopo come previsto da questo CC  (documentazione di evidenza clinica, gestione del rischio, sorveglianza post-market, IFU, Riassunto della sicurezza e delle prestazioni)
Aggiornamento dell''IFU SCH mRTS120ING
Aggiornamento della documentazione del prodotto MDR/MTB ELITe MGB Kit in corso di stesura per l''allineamento ai requisiti IVDR modificando lo scopo come previsto da questo CC (documentazione di evidenza clinica)
Revisione dell''IFU SCH mRTS120ING
Aggiornamento della documentazione del prodotto MDR/MTB ELITe MGB Kit in corso di stesura per l''allineamento ai requisiti IVDR modificando lo scopo come previsto da questo CC (documentazione di evidenza clinica)
Revisione dell''IFU',N'RTS120ING_05, CTR120ING_02 - cc21-24_ifu_mdrmtb_elite_mgb_kit_rts120ing.msg
RTS120ING_05, CTR120ING_02 - cc21-24_ifu_mdrmtb_elite_mgb_kit_rts120ing_1.msg',N'QARA
RAS
QARA
QAS
RS
PVM
QARA
RAS
RDC
RS
PVS
PVM
MS',N'25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021
25/03/2021',NULL,N'16/04/2021
16/04/2021
16/04/2021
16/04/2021',N'No',N'nessun impatto sul DMRI di prodotto',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'na',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Si',N'la documentazione di gestione del rischio è da modificare nel MOD22,01 relativo alla descrizione del prodotto. L''aggiornamento verrà effettuato direttamente sulla documentazione aggiornata per l''IVDR',N'Alessandra Gallizio',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-23','2021-03-19 00:00:00',N'Marta Carena',N'Documentazione SGQ',N'Aggiornamento istruzioni operative 19,03 "GESTIONE DOCUMENTO DI MARKETING" e 19,04 "GESTIONE MAILING LIST PER COMUNICAZIONI"  e MOD05,06 di approvazione per implementare il sistema di codifica dei documenti (inserimento della data) e aggiornamento in base alle nuove funzioni aziendali.',N'La codifica dei documenti con la data nel codice rende il documento più intellegibile.',N'Istruzioni operative e modulo 05,06 obsoleti',NULL,N'QARA
QM
GASC
IMM
SA-M',N'Manutenzione e taratura strumenti interni
Marketing
Sistema qualità',N'Impatto positivo, la documentazione è aggiornata nel rispetto dell''operatività e delle nuove mansioni.
Impatto positivo, la documentazione è aggiornata nel rispetto dell''operatività e delle nuove mansioni.
-',N'23/03/2021
23/03/2021',N'Aggiornamento istruzioni operative 19,03 "GESTIONE DOCUMENTO DI MARKETING" e 19,04 "GESTIONE MAILING LIST PER COMUNICAZIONI"  e MOD05,06 
 Verifica, approvazione e messa in uso documenti
f',NULL,N'MA
QAS
TGSWC',N'23/03/2021
23/03/2021',NULL,NULL,N'No',N'na',N'No',N'na, aggiornamento documentazione interna',N'No',N'na, aggiornamento documentazione interna',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na, aggiornamento documentazione interna',N'No',N'vedere MOD05,36',N'No',N'na, aggiornamento documentazione interna',N'No',N'na, aggiornamento documentazione interna',N'Marta Carena',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-22','2021-03-18 00:00:00',N'Marta Carena',N'Documentazione SGQ',N'Integrazione nella Quick Guide del prodotto RTS171ING della variante nigeriana e modifica dei nomi dei controlli positivi  da  PC484 a PC Wild Type e PC501 a PC Mutato.',N'Quick Guide completa con tutte le varianti per rendere più fruibile la comprensione del prodotto al cliente',N'Quick Guide incompleta, possibile interpretazione non chiara',NULL,N'QC
QM
RDM
GASC
IMM',N'Controllo qualità
Marketing
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini di attività
Impatto positivo, nomi fruibili al cliente e lista completa di varianti
Nessun impatto se non in termini di attività
Impatto in termini di attività: l''aggiornamento dei nomi impatta sugli AP.',N'18/03/2021
18/03/2021
18/03/2021
18/03/2021',N'Modifica dei moduli
Aggiornamento della Quick Guide
Aggiornamento etichette, messa in uso dei moduli e della Quick Guide
aggiornamento AP',N'mod962-ctr171ing_01 - cc21_22_mod962-ctr171ing_01.msg
moduli_cq_rts171ing - cc21-22_moduli_cq_rts171ing.msg
MODULI_CQ_CTR171ING - cc21-22_messa_in_uso_mod10,xx-ctr171ing.msg
quickguide_sars-cov-2_variants - cc21-22_quickguide_sars-cov-2_variants_elite_mgb_kit.msg
quickguide_sars-cov-2_variants - cc21-22_quickguide_sars-cov-2_variants_elite_mgb_kit_1.msg
etichette aggiornate secondo PLP - cc21-22_etichetteaggiornatesecondoplp.txt
PLP2021-002 AGGIORNATA NEI NOMI E NEI CODICI CoV-2 Wild Type Positive Control,  CTR171ING-WT CoV-2 Mutant Positive Control, CTR171ING-MU - plp2021-002__packaginglabelingplan_scp2021-009_01_gs.docx
MODULI_CQ_CTR171ING - cc21-22_messa_in_uso_mod10,xx-ctr171ing_1.msg
MODULI_CQ_RTS171ING - cc21-22_moduli_cq_rts171ing.msg
ap_cq_sars-cov-2_varianti - cc21-22_ap_cq_sars-cov-2_varianti_rts171ing.msg',N'QCT/DS
MS
QM
QAS
RAS
RS',N'18/03/2021
18/03/2021
18/03/2021
18/03/2021',NULL,N'03/05/2021
22/04/2021',N'Si',N'da aggiornare se cambiano i nomi',N'No',N'attraverso Quick Guide; TAB: "ELITe InGenius -TAB P45 - Rev AA - Product SARS-CoV-2 Variants ELITe MGB"',N'No',N'nesun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'NA, il prodotto è RUO',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-21','2021-03-15 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Per ottenere la registrazione del prodotto "Respiratory Viral ELITe MGB Panel" (ref. RTS548ING) in Colombia e, poter vendere il prodotto in quel territorio, il distributore ci chiede che venga modificata l''IFU del prodotto aggiungendo delle referenze bibliografiche.',N'Con l''implementazione della modifica, si riuscirebbe a registrare (e quindi a vendere) questo prodotto in Colombia.',N'Non saremmo in grado di ottenere la registrazione del prodotto in Colombia.',N'962-RTS548ING - Respiratory Viral MGB Panel',N'QARA',N'Regolatorio',N'La modifica non ha impatto sul prodotto o sulle sue performance. L''IFU non necessita di aggiornamenti futuri per questo preciso paragrafo che si aggiunge con la modifica.
La modifica non ha impatto sul prodotto o sulle sue performance. L''IFU non necessita di aggiornamenti futuri per questo preciso paragrafo che si aggiunge con la modifica.',N'15/03/2021
15/03/2021',N'Aggiornare l''IFU del prodotto RTS548ING con il paragrafo per le referenze bibliografiche (fornite da FastTrack, original manufacturer del prodotto)
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica',N'RTS548ING REV03 - cc21-21_messa_in_uso_ifu_respiratory_viral_elite_mgb_panel_rts548ing.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-21_mod05,36__non_significativa_1.pdf',N'QARA
RAS
QARA
QAS',N'15/03/2021
15/03/2021',NULL,N'22/03/2021
22/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'nessun impattto su RA IFU10',N'No',N'vedere MOD05,36. La modifica non ha impatto nè sul prodotto, nè sulle sue prestazioni, pertanto non è da notificare.',N'No',N'nessun virtual manufacturer',N'No',N'nessun intervento',N'Michela Boi',NULL,NULL,'2021-03-22 00:00:00',N'No',NULL,NULL),
    (N'21-20','2021-03-12 00:00:00',N'Daniela  Atragene',N'Assay Protocol',N'Miglioramento dell''interpretazione del risultato per il Controllo Positivo del kit RTS171ING "SARS-CoV-2 Variants ELITe MGB Kit ", in quanto viene rilevato un doppio picco di TM, causando la non approvazione sullo stesso.',N'possibile approvazione del PC',N'Non possibile approvazione del PC',NULL,N'RDM
GASC
QAS
RAS',N'Assistenza applicativa
Ricerca e sviluppo
Sistema qualità',N'Nessun impatto solo in termini di attività.
Impatto positivo, le modifiche al prodotto RUO sono migliorative, non alterano le performance del prodotto.
Impatto positivo, le modifiche al prodotto RUO sono migliorative, non alterano le performance del prodotto.
Impatto positivo, le modifiche al prodotto RUO sono migliorative, non alterano le performance del prodotto.
Riduzione delle segnalazioni dovuta alla mancanza di approvazione del PC
Riduzione delle segnalazioni dovuta alla mancanza di approvazione del PC',N'12/03/2021
12/03/2021
12/03/2021
12/03/2021
15/03/2021
15/03/2021',N'Messa in uso Assay
Aggiornamento ASSAY
Revisione Assay
-
Aggiornamento degli assay presso la base installata
Rilascio TAB',N'messa in uso ap - cc21-20_messa_in_uso_ap_1.msg
messa in uso ap - cc21-20_messa_in_uso_ap.msg
TAB P45 - Rev AA  - cc21-22_messa_in_uso_tab.msg',N'RS
FPS
GPS
RS
RDO
RDM
RT
RS
RAS
GASC
GPS',N'12/03/2021
12/03/2021
15/03/2021
15/03/2021',NULL,N'15/03/2021
15/03/2021
07/05/2021
13/04/2021',N'No',N'nessun impatto',N'No',N'attraverso TAB',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun ra',N'No',N'na, il prodotto è ruo',N'No',N'nessun virtual manufacturer',N'No',N'na, il prodotto è ruo',NULL,NULL,NULL,'2021-05-07 00:00:00',N'No',NULL,NULL),
    (N'21-19','2021-03-12 00:00:00',N'Maria Lucia Manconi',N'Progetto',N'Implementazione da parte del fornitore Lutech di una nuova versione software ELITeWare che permetta l''interfacciamento tra ELITe BeGenius e il LIS ospedaliero.  ',N'Il software permetterà la comunicazione tra ELITe BeGenius e il LIS ospedaliero.',N'Impossibilità di comunicazione tra l''ELITeWare e ELITe BeGenius che renderebbe difficile la comunicazione tra ELITe BeGenius e LIS ospedaliero.',NULL,N'CSC
GSC
QARA
GASC
SIS',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Global Service
Program Managment
Sistema qualità',N'Impatto in termini di attività da svolgere e impatto positivo per la gestione del cliente nella comunicazione dati dei pazienti e dei test tra l''ELITe BeGenius e il sistema ospedaliero.
Impatto in termini di attività da svolgere e impatto positivo per la gestione del cliente nella comunicazione dati dei pazienti e dei test tra l''ELITe BeGenius e il sistema ospedaliero.
Impatto in termini di attività da svolgere e impatto positivo per la gestione del cliente nella comunicazione dati dei pazienti e dei test tra l''ELITe BeGenius e il sistema ospedaliero.
Impatto in termini di attività da svolgere.
Impatto in termini di attività da svolgere.
Impatto in termini di attività da svolgere.
Impatto in termini di attività da svolgere
Impatto in termini di attività da svolgere
Impatto in termini di attività da svolgere
Impatto in termini di attività da svolgere ',N'15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021',N'Training (ppt/demo) ai clienti e al personale EG sul funzionamento del software con il sistema ELITe BeGenius
Supporto durante i test della nuova versione software in sede 
Coinvolgimento nell''attività relativa alla stesura di PLAN e REPORT della validazione interna del nuovo software, sia in termini di contenuti che in termini di revisione documentale 
Supportare il fornitore per l''interfacciamento tra ELITe BeGenius e la nuova versione ELITeWare dal cliente (Italia) 
Rendere disponibile al rilascio della nuova versione software un sistema che comprenda la strumentazione ELITe BeGenius collegata al software
Stesura TSB
Seguire il CC garantendo che tutte le attività vengano svolte come da GANTT.
Interfacciarsi con il fornitore per:
concordare le tempistiche per lo svolgimento delle attività di sviluppo e per le giornate dei test di validazione in sede, 
per il rilascio della documentazione relativa alle modifiche apportate al software ed alla loro verifica (incluso il manuale utente) 
per ',N'GANTT - gantt_cc-_eliteware-elite_begenius.xlsx',N'FPS
FPS
CSC
GSS
GSS
SIS
SIS
SIS
SIS
QARA
QAS',N'15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021',NULL,NULL,N'No',N'na',N'No',N'na',N'No',N'na',N'SW - Elenco Software',N'SW45 - Ingenius ELITeWare',N'nessun impatto',N'No',N'na',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Maria Lucia Manconi',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-18','2021-03-11 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Modifica delle QuickGuide associate al prodotto "SARS-CoV-2 Variants ELITe MGB Kit" (ref. RTS171ING), prodotto RUO, per l''aggiunta di Avvertenze e Precauzioni.',N'Non essendoci un manuale d''istruzioni completo per questo prodotto, l''utilizzatore finale sarebbe a conoscenza delle avvertenze e precauzioni di cui deve tenere conto durante l''utilizzo del prodotto',N'L''utilizzatore finale potrebbe utilizzare il prodotto in modo errato',NULL,N'QARA
RDM
GASC
PVM',N'Regolatorio',N'Impatto positivo, anche in assenza di un manuale di istruzioni per l''uso completo, si fornisce al cliente un IFU ridotto contenente tutte le informazioni necessarie all''uso del prodotto incluse Avvertenze e Precauzioni.',N'11/03/2021',N'modifica delle QuickGuide con l''aggiunta di Avvertenze e Precauzioni',N'quickguide aggiornata - cc21-18_quickguide_sars-cov-2_variants_elite_mgb_kit.msg',N'RAS',N'11/03/2021',N'12/03/2021',N'11/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'nessun impatto sul RA IFU10',N'No',N'NA, il prodotto è RUO',N'No',N'nessun virtual manufaturer',N'No',N'nessun intervento',N'Michela Boi','2021-03-12 00:00:00',NULL,'2021-03-11 00:00:00',N'No',NULL,NULL),
    (N'21-17','2021-03-03 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Realizzazione di una nuova istruzione operativa che descriva le modalità di gestione degli ordini delle spare parts  da parte dei distributori e dei relativi listini.',N'Miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA',N'Ritardi nella fornitura e spedizione delle spare parts',NULL,N'OP
QARA
EAM
L&SAM',N'Global Service
Ordini
Sistema qualità
Vendite Estero',N'Impatto positivo, miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA.
Impatto positivo, miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA.
Impatto positivo, miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA.
Impatto positivo, miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA.
Impatto positivo, miglioramento del processo di ordine, spedizione e fornitura delle spare parts da parte dei distributori di EGSPA.',N'22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021',N'creazione istruzione operativa
TSB
messa in uso documenti',N'IO11,21REV00 - cc21-17_messa_in_uso_io1121_spare_parts_order_policy__price_list_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-17_mod05,36_01_non_significativa.pdf
IO11,21REV00 - cc21-17_messa_in_uso_io1121_spare_parts_order_policy__price_list.msg',N'GSC
QAS
GSC',N'22/03/2021
22/03/2021
22/03/2021',NULL,N'30/03/2021
30/03/2021',N'No',N'nessun impatto',N'No',N'nessun impatto, la comunicazione sarà effettuata ai distributori',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun risk assessment associato al processo di gestione delle spare parts',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-16','2021-03-02 00:00:00',N'Clelia  Ramello',N'Documentazione SGQ',N'Aggiornamento della documentazione EG SpA relativa al progetto SCP2018-024 (ref INT040). 
L''aggiornamento riguarda principalmente i documenti  "Product Requirement Document" (PRD2019-007, Rev.00) e "Software Requirement Document" (SRD2019-001, Rev.00), i cambiamenti da apportare sono riassunti di seguito e dettagliati nel file allegato (ALLEGATO 1).
•	aggiornamento del template dei documenti per riportare l''area a cui i requisiti di progettazione sono riconducibili e per riportare il collegamento di ciascun requisito con i rischi e le mitigazioni contenute nella documentazione di gestione dei rischi del prodotto;
•	aggiornamento/aggiunta/rimozione di specifici requisiti di progettazione;
•	variazione nei riferimenti ai rischi e alle mitigazioni riportate;
•	variazione del nome dello strumento, modificato da "ELITe InGenius 2.0" a "ELITe BeGenius".
Per quanto riguarda quest''ultimo cambiamento, la denominazione corretta sarà da riportare, oltre che sulla versione aggiornata del PRD e SRD, sulla documentazione di progetto/prodotto non ancora redatta o in corso di stesura. La seguente documentazione già approvata non verrà aggiornata per modificare il nome del prodotto in quanto non si tratta di documentazione di prodotto ma di progetto e la modifica del nome non ha impatto sulla sicurezza o sulle prestazioni del prodotto:
- Marketing Requirement Document (MRD2018-001 rev.00): il nome corretto verrà riportato sui documenti che derivano da esso  (SRD, PRD, PLP).
- Regulatory Assessment (RA2019-005 rev.00): il nome corretto verrà riportato nel Regulatory Plan redatto a partire dall''Assessment che è in corso di stesura.
- Project Plan (PLAN2018-039 rev.00)
- Project Plan (PLAN2018-039 rev.00)
',N'L''aggiornamento dei template di PRD e SRD consente di disporre di documenti di definizione dei requisiti più completi e allineati con la documentazione dei rischi di prodotto. 
La modifica proposta nel PRD e nell''SRD dei requisiti di progettazione e dei riferimenti ai rischi e alle mitigazioni è il risultato del confronto avvenuto nel corso della fase di Sviluppo fra EG SpA e PSS, la sua implementazione consente di avere dei requisiti/rischi/mitigazioni maggiormente allineati e adeguati rispetto ai risultati finora ottenuti.
Riportare "ELITe BeGenius" sui documenti di progetto consente di allineare la documentazione al nome definito per il prodotto.
',N'Il mancato aggiornamento dei documenti determinerebbe:
- documenti di definizione dei requisiti (PRD e SRD) meno informativi e non allineati con la documentazione dei rischi di prodotto;
- definizione di requisiti/rischi/mitigazioni meno allineati e adeguati rispetto ai risultati finora ottenuti;
- disallineamento tra il nome del prodotto riportato nella documentazione di progetto e quello effettivo.',NULL,N'GSC
QARA
RDM
MS
PVM
VP_R&D',N'Global Service
Marketing
Program Managment
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo. La modifica proposta nel PRD e nell''SRD dei requisiti di progettazione e dei riferimenti ai rischi e alle mitigazioni è il risultato del confronto avvenuto nel corso della fase di Sviluppo fra il Team di EG SpA (compresa la funzione Marketing) e PSS, la sua implementazione consente di avere dei requisiti/rischi/mitigazioni maggiormente allineati e adeguati rispetto ai risultati finora ottenuti.
Impatto positivo. La modifica proposta nel PRD e nell''SRD dei requisiti di progettazione e dei riferimenti ai rischi e alle mitigazioni è il risultato del confronto avvenuto nel corso della fase di Sviluppo fra il Team di EG SpA (compresa la funzione Program Management) e PSS, la sua implementazione consente di avere dei requisiti/rischi/mitigazioni maggiormente allineati e adeguati rispetto ai risultati finora ottenuti e di tracciare le modifiche apportate.
Impatto positivo. La modifica proposta nel PRD e nell''SRD dei requisiti di progettazione e dei riferimenti ai rischi e alle mitigazioni è ',N'08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021',N'Revisionare e approvare i contenuti aggiornati dei documenti PRD2019-007 e SRD2019-001 e assicurarsi che la documentazione di competenza relativa al SCP2018-024 riporti il nome ELITe BeGenius
Aggiornare e approvare i documenti PRD2019-007 e SRD2019-001 e assicurarsi che la documentazione di competenza relativa al SCP2018-024 riporti il nome ELITe BeGenius
Condividere i documenti PRD2019-007 e SRD2019-001 con PSS per loro revisione/approvazione. Comunicare la modifica del nome a PSS in modo che anche la documentazione prodotta da loro riporti il nome corretto
Revisionare e approvare i contenuti aggiornati dei documenti PRD2019-007 e SRD2019-001 e assicurarsi che la documentazione di competenza relativa al SCP2018-024 riporti il nome ELITe BeGenius
Revisionare e approvare i contenuti aggiornati dei documenti PRD2019-007 e SRD2019-001 e assicurarsi che la documentazione di competenza relativa al SCP2018-024 riporti il nome ELITe BeGenius
Revisionare e approvare i contenuti aggiornati dei documenti PRD2',NULL,N'MS
MA
SIS
PVS
PVM
QARA
QM
QAS
RAS
RDM
RT
RS
GSC
GSTL
SIS
GSTL',N'08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021',N'30/06/2021
30/06/2021
30/06/2021
30/06/2021
30/06/2021
30/06/2021',NULL,N'No',N'il DMRI, prodotto da PSS, non è ancora stato approvato',N'No',N'NA, il prodotto non è ancora immesso sul mercato',N'No',N'NA, il prodotto non è ancora immesso sul mercato',N'ALTRO - Altro',N'ALTRO - ALTRO',N'da valutare',N'No',N'NA, il prodotto non è ancora immesso sul mercato',N'No',N'NA, il prodotto non è ancora immesso sul mercato',N'No',N'la documentazione di gestione dei rischi è ancora in versione draft, la documentazione approvata terrà conto delle modifiche introdotte',N'Clelia  Ramello','2021-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-15','2021-03-01 00:00:00',N'barbara pirani',N'Nuova revisione di software associati ad IVD',N'Aggiornamento del sistema operativo Windows  da OS7 a OS10 in quanto, OS7 non è più supportato.
Impatto positivo, il passaggio alla versione OS10 permette di rendere funzionanti gli strumenti installati, la validazione permetterà di garantire le identiche prestazioni del prodotto e garantirà il mantenimento di tutte le funzionalità già in essere.',N'Possibilità di installare nuovi strumenti ELITe InGenius e mantenere operativi quelli attuali aggiornando il sistema operativo Windows (OS10)',N'Non potrebbero più essere installati strumenti ELITe InGenius aventi sistema operativo Windows (OS7).',NULL,N'GSC
QARA
RDM
FSE
GSTL
SIS
RS
VP_R&D',N'Assistenza strumenti esterni (Italia)
Global Service
Program Managment
Regolatorio
Ricerca e sviluppo',N'Possibile impatto sull'' allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto OS10
Possibile impatto sull'' allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto OS10
Possibile impatto sull'' allocazione delle risorse del team per attività aggiuntive legate al rilascio del nuovo pacchetto OS10
Impatto positivo, il passaggio alla versione OS10 permette di rendere funzionanti gli strumenti installati, la validazione permetterà di garantire le identiche prestazioni dei prodotti e garantirà il mantenimento di tutte le funzionalità già in essere.
Impatto positivo, il passaggio alla versione OS10 permette di rendere funzionanti gli strumenti installati, la validazione permetterà di garantire le identiche prestazioni dei prodotti e garantirà il mantenimento di tutte le funzionalità già in essere.
Impatto positivo, il passaggio alla versione OS10 permette di rendere funzionanti gli strumenti installati, la valid',N'22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021',N'Supervisionare le attività del fornitore che ci fornisce il PC contenente la versione OS Windows 10
Stesura Piano e Report di validazione
Verificare se l''aggiornamento da Windows OS7 a OS10 possa impattare su 510K
Attività di verifica del nuovo OS10
Revisione Piano e Report di validazione
Verificare se l''aggiornamento da Windows OS7 a OS10 possa impattare su 510K
Aggiornamento manuale INT_030-CEIVD e INT_030 US-IVD
Supporto nella stesura della documentazione relativa all''analisi della significatività della modifica secondo i regolamenti fda.
Revisione Piano e Report di validazione
Attività di verifica e validazione del nuovo OS10
Stesura TSB ed invio a ICO e distributori (la sostituzione non è obbligatoria) training
Installazione nuovo OS Windows 10 presso i clienti
Verificare se l''aggiornamento da Windows OS7 a OS10 possa impattare su 510K
Aggiornamento IFU
L''installazione del nuovo OS Windows 10 presso i clienti è prevista in caso di:
•	riparazioni che richiedono la sost',NULL,N'SIS
SIS
SIS
RS
RDM
RS
GSTL
GSTL
GSS
GSTL
GSS
CSC
FSE
QARA
FSE
QARA
QAS
RAS
SIS
RDM
RS
GSC
GSTL
RAS
RT
RS
RAS
GSTL
RAS
RS',N'22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021
22/03/2021',N'30/06/2021
30/06/2021
30/06/2021',NULL,N'No',N'-',N'No',N'-',N'No',N'-',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'nessun impatto sul RA di SW20',N'No',N'vedere MOD05,36 e verificare l''impatto su 510k',N'No',N'nessun virtual manufacturer',N'No',N'-',N'barbara pirani','2021-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-14','2021-03-01 00:00:00',N'Alessia Cotza',N'Assay Protocol',N'Adeguamento del pacchetto di Assay Protocols relativi al prodotto RTS175PLD alla versione 1.3.0.16.
Modifiche che verranno introdotte: 
- AP Plasma 200 ULoQ 130.000.000 IU/mL
- AP Plasma 1000 ULoQ 170.000.000 IU/mL
- AP Urine 200 ULoQ 160.000.000 IU/mL
 L''aggiornamento del AP non ha impatto sulle performance del prodotto',N'é possibile impostare come limite massimo di Upper Limit of Quantification (ULoQ) il valore esatto invece dell''attuale 100000000.',N'Upper Limit of Quantidication (ULoQ) riportato nell''Assay Protocol non sarebbe quello corretto.',N'910-RTS175PLD - BKV ELITe MGB KIT 100 T',N'QARA
GASC
PVM',N'Assistenza applicativa
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo per allineamento dei valori di ULoQ a quelli riportati su IFU. 
Nessun impatto se non in termini di attività da svolgere
Nessun impatto se non in termini di attività da svolgere
Nessun impatto se non in termini di attività da svolgere
Nessun impatto se non in termini di attività da svolgere
Nessun impatto se non in termini di attività da svolgere
 L''aggiornamento del AP non ha impatto sulle performance del prodotto',N'11/03/2021
11/03/2021
11/03/2021
09/03/2021
09/03/2021
09/03/2021',N'- installazione presso i clienti delle nuove versioni degli AP
- aggiornamento TAB
comunicazione e messa in uso dei nuovi assay protocol
La modifica degli AP non è considerata significativa.
Modifica degli Assay Protocol
comunicazione e messa in uso dei nuovi assay protocol
- installazione presso i clienti delle nuove versioni degli AP
- eventuale aggiornamento TAB
verifica degli Assay Protocol',N'messa in uso ap con il valore di ULoQ (IU/mL) - cc21-14_ap_bkv_elite_mgb_kit_1.msg
comunicazione modifica TAB - cc21-14_eliteboard_documental_repository.msg
AP in uso per il valore di ULoQ (IU/mL) - cc21-14_ap_bkv_elite_mgb_kit.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-14_mod05,36__non_significativa.pdf
AP in uso per il valore di ULoQ (IU/mL) - cc21-14_ap_bkv_elite_mgb_kit_2.msg',N'PVS
QAS
FPS
GPS
FPS
GPS
QAS
QAS
RS',N'11/03/2021
11/03/2021
08/03/2021
09/03/2021
11/03/2021',NULL,N'15/03/2021
15/03/2021
15/03/2021
15/03/2021',N'No',N'nessun impatto',N'Si',N'attraverso TAB',N'No',N'nessun impatto',N'AP - Gestione Assay Protocol',N'AP3 - Gestione delle modifiche degli Assay Protocol',N'nessun impatto sul RA AP3',N'No',N'La modifica degli AP non è considerata significativa.',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-13','2021-03-01 00:00:00',N'barbara pirani',N'Manuale di istruzioni per l''uso
Progetto',N'Il modulo di Pre-Analitica (sw associato allo strumento STARLETPAI di Hamilton, gestito nel progetto codice, SCP2021-007) aliquota da tubi primari verso i rack InGenius e le piastre Bioer. E'' inoltre in grado di preparare le piastre PCR per diversi termociclatori.
Il driver prevede lo scambio bidirezionale di file txt: lista dei barcode dei tubi letti sul modulo Pre-analitica e la programmazione delle aliquote che la Pre-analitica deve eseguire su piastre Bioer e su rack InGenius (BeGenius).',N'Il driver permette di organizzare il processo di aliquotazione del modulo di Pre-Analitica in modo automatico, limitando l''intervento manuale sulla strumentazione e agevolando così il flusso di lavoro del personale tecnico del laboratorio',N'Il processo di aliquotazione continuerebbe ad essere manuale e richiederebbe tempo, in quanto non automatizzato, da parte del personale tecnico del laboratorio.',NULL,N'GASC
GPS
SIS
VP_R&D',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Global Service
Program Managment
Regolatorio
Ricerca e sviluppo',N'Impatto a livello di attività
Impatto a livello di attività
Impatto a livello di attività
Impatto a livello di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto positivo, le nuove funzionalità velocizzeranno il lavoro da parte del personale tecnico 
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività',N'01/03/2021
01/03/2021
01/03/2021
01/03/2021
15/03/2021
15/03/2021
15/03/2021
15/03/2021',N'Gestione delle Attività vengano svolte, come da Gantt.
Interfacciarsi con il fornitore per concordare le tempistiche per lo svolgimento delle attività, e per il rilascio della documentazione relativa alle modifiche apportate al sw ed alla loro verifica (incluso il manuale utente).
Attività relativa all'' Assistenza Applicativa nella revisione di PLAN e REPORT 
Richiedere al fornitore la Regression analysis
Valutare se è necessario aggiornare il Risk Assessment
Rendere disponibili al rilascio del sw da parte del fornitore un sistema che comprenda la strumentazione Pre-Analitica collegata al middleware 
Stesura TSB
Valutare se è necessario aggiornare il Risk Assessment
Validare le nuove funzionalità sul sistema Pre-Anlitica-ELITeWare, utilizzando i MOD05,23 "piano validazione sw" e MOD05,24 "rapporto validazione sw"
Svolgere la formazione a tutti i reparti interessati
Training mediante ppt/demo delle nuove funzionalità ai clienti e interno a EG
Attività relativa alla stesura di PLAN ',N'Gantt  - gantt_cc21-13_03032021_1.xlsx',N'SIS
GSC
GSTL
GSTL
GSS
GPS
SIS
GPS
FPS
QARA
SIS
SIS
SIS
RS
SIS
SIS
GSC
GSTL
GASC
GPS
SIS
RS
GPS',N'01/03/2021
15/03/2021
15/03/2021
15/03/2021',NULL,N'15/03/2021',N'No',N'na',N'No',N'na, lo strumento customizzato non è in commercio',N'No',N'na, lo strumento customizzato non è in commercio',N'ALTRO - Altro',N'ALTRO - ALTRO',N'sarà da creare il risk assessment sullo strumento e sul sw',N'No',N'na, lo strumento customizzato non è in commercio',N'No',N'nessun virtual manufacturer',N'No',N'-',N'barbara pirani',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-12','2021-02-24 00:00:00',N'Paolo Saracco',N'Stabilità',N'Sulla base dei dati di stabilità reali, come comunicato da R&D in data 23/02/2020, estendere la scadenza a 24 mesi dei seguenti prodotti: 
962-RTS201ING	(ESBL ELITe MGB KIT),
962-RTS300ING (Meningitis Bacterial ELITe MGB KIT), 
962-RTS400ING (STI PLUS ELITe MGB KIT), 
962-RTSD00ING (Coagulation ELITe MGB KIT).',N'Portare a 24 mesi la durata dei prodotti',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS201ING - ESBL ELITe MGB Kit
962-RTSD00ING - Coagulation - ELITe MGB KIT
962-RTS300ING - Meningitis Bact ELITe MGB Kit
962-RTS400ING - STI PLUS ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Non c''è nessun impatto sulle prestazioni dei prodotti, come dimostrato dai report di stabilità:
962-RTS201ING (ESBL ELITe MGB KIT): 
•	STB2018-135
•	STB2019-011
•	STB2019-055
962-RTS300ING (Meningitis Bacterial ELITe MGB KIT):
•	STB2020-093
•	STB2020-095
•	STB2020-102
962-RTS400ING (STI PLUS ELITe MGB KIT):
•	STB2020-094
•	STB2020-096
•	STB2020-097
962-RTSD00ING (Coagulation ELITe MGB KIT):
•	STB2019-003
•	STB2019-027
•	STB2019-141
nessun impatto se non in termini di attività',N'25/02/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021',N'nessuna attività a carico
messa in uso documento modificato
L''estensione della scadenza è una modifica la significativa, identificare i paesi in cui il prodotto è registrato, effettuare la modifica preliminare. Notificare a DEKRA
Effettuare la notifica a modifica svolta. Notificare a DEKRA
aggiornamento ftp
nessuna attività a carico
aggiornamento dei moduli',N'MOD962-RTSD00ING_04, MOD962-RTS300ING_02, MOD962-RTS201ING_03,  - cc21-12_messa_in_uso_mod962-rtsxxing.msg
MOD962-RTS400ING_02 - cc21-12_mod962-rts400ing_02.msg
la modifica è significativa, pertanto è da notificare ai paesi presso cui il prodotto è registrato ed a DEKRA - rap21-2_mod05,36_significativa_1.pdf
notifica preliminare effettuata - cc21-12_prior_notification_of_change_-rts201ing_rts300ing_rts400ing_rtsd00ing.msg
notifica preliminare dekra effettuata - cc21-12_notice_of_change__rts400ing_dekra.msg
documenti inviati a DEKRA - cc21-12_documewntiinviatiadekra.msg
notifica a modifica effettuata - cc21-12_notifica_definitiva_rts201ing_rts300ing_rts400ing_rtsd00ing.msg
notifica a dekra a modifica effettuata - cc21-12_notice_of_change__rts400ing_dekra_definitiva.msg
MOD962-RTSD00ING_04, MOD962-RTS300ING_02, MOD962-RTS201ING_03, - cc21-12_messa_in_uso_mod962-rtsxxing_1.msg
MOD962-RTS400ING_02 - cc21-12_mod962-rts400ing_02_1.msg',N'MM
QAS
RAS
RAS
RS
QCT/DS
RAS',N'25/02/2021
08/03/2021
08/03/2021
08/03/2021
08/03/2021',NULL,N'08/03/2021
06/04/2021
19/04/2021
06/04/2021
06/04/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9, le attività di estensione della scadenza sono parte della procedura di sviluppo',N'Si',N'vedere MOD05,36. ',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-11','2021-02-19 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Modifica Specifica assistenza tecnica TB, modifica Specifica assistenza tecnica BT, modifica Specifica assistenza tecnica Refrigeratori',N'Nuovo modello di termo blocco installato in Produzione, nuovo Bagno Termostatato installato in Controlli Positivi, nuovo modello di Frigoriferi e congelatori installati in Magazzino.',N'Possibili errori nella taratura dello strumento al momento della manutenzione, errate procedure in caso di manutenzione straordinaria.',NULL,N'T&GSC
T&GSS',N'Manutenzione e taratura strumenti interni
Sistema qualità',N'Impatto positivo: l''utilizzo di documentazione aggiornata evita possibili errori nella taratura dello strumento al momento della manutenzione ed errate procedure in caso di manutenzione straordinaria.
impatto in termini di attività',N'19/02/2021
19/02/2021',N'Modifica Specifica assistenza tecnica TB, modifica Specifica assistenza tecnica BT, modifica Specifica assistenza tecnica Refrigeratori
messa in uso documenti',N'la modifica non è significativa, pertanto non è da notificare. - cc21-11_mod05,36__non_significativa.pdf',N'T&GSS
QAS',N'19/02/2021
19/02/2021',NULL,NULL,N'No',N'na',N'No',N'na',N'No',N'na',N'T - Taratura Esterna Strumenti',N'T1 - Gestione informazioni dello strumento',N'nessun impatto sul RA T1',N'No',N'la modifica non è significativa, in quanto tratta l''aggiornamento di documentazione interna relativa ai requisiti di assistenza tecnica strumenti interni',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-10','2021-02-15 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Modifica templates e nome dei seguenti documenti:
- MOD11,18 "ELITe InGenius Technical Application bullettin (TAB)" in "TAB Template"
- MOD11,22 "ELITe InGenius Technical Application bullettin (TAB) Index List" in " TAB Index"
- MOD11,17 "ELITe InGenius Technical Service bullettin (TSB)" in "TSB Template"
- MOD11,21 "ELITe InGenius Technical Service bullettin (TSB) Index List" in "TSB Index"  ',N'Unificare ed uniformare i template TSB/TAB per uso generale per tutti i progetti (non solo ELITe InGenius, ma anche ELITeWare, BeGenius, etc...) ',N'Impossibilità di rilasciare TSB/TAB per altri progetti',NULL,N'GSC
GSTL
GASC
GPS
GSS',N'Assistenza applicativa
Global Service
Sistema qualità',N'Impatto positivo: standardizzazione delle informazioni presenti su TAB e TSB  e utilizzo di un unico template per progetti differenti.
Impatto positivo: standardizzazione delle informazioni presenti su TAB e TSB  e utilizzo di un unico template per progetti differenti.
Impatto positivo: standardizzazione delle informazioni presenti su TAB e TSB  e utilizzo di un unico template per progetti differenti.
Impatto positivo: standardizzazione delle informazioni presenti su TAB e TSB  e utilizzo di un unico template per progetti differenti.',N'15/02/2021
15/02/2021
15/02/2021
15/02/2021',N'Unificare ed uniformare i template TSB/TAB per uso generale per tutti i progetti (non solo ELITe InGenius, ma anche ELITeWare, BeGenius, etc...) 
Unificare ed uniformare i template TSB/TAB per uso generale per tutti i progetti (non solo ELITe InGenius, ma anche ELITeWare, BeGenius, etc...) 
formazione al personale che compila, verifica ed approva tab e tsb
messa in uso della documentazione',N'messa_in_uso_modulo_tab-tsb - cc21-10_messa_in_uso_modulo_tab-tsb.msg
messa_in_uso_modulo_tab-tsb_ - cc21-10_messa_in_uso_modulo_tab-tsb_1.msg
messa_in_uso_modulo_tab-tsb - cc21-10_messa_in_uso_modulo_tab-tsb_2.msg
FORMAQZIONE ONLINE DEL 23/02 - cc21-10_invitoformazione_nuovi_template_tsbtab.msg',N'QAS
GSC
GASC
GSC',N'15/02/2021
15/02/2021
15/02/2021
15/02/2021',NULL,N'22/02/2021
22/02/2021
23/02/2021
22/02/2021',N'No',N'verificare l''impatto',N'No',N'non necessaria, modifica di documentazione interna',N'No',N'na, la modifica riguarda riguarda esclusivamente dei documenti',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Marcello Pedrazzini',NULL,NULL,'2021-02-23 00:00:00',N'No',NULL,NULL),
    (N'21-9','2021-02-09 00:00:00',N'barbara pirani',N'Progetto',N'Si chiede l''implementazione del software Results Manager al fine di: 
-	supportare l''import del file dello strumento ARIA DX in formato txt 
-	migliorare la gestione delle colonne usate dal software per identificare l''external ID relativo ai target da rilevare. In questo modo si potrà utilizzare lo stesso Dump File per analizzare sedute provenienti da diversi strumenti di amplificazione (semplicemente inserendo nelle colonne prescelte l''external ID indicato nel Dump)
-	bug fixing dell''errore di mapping dei test quando non è esplicitato l''external ID. Il software non è in grado di riconoscere automaticamente il dump file che deve essere utilizzato per l''analisi della sessione e associa casualmente in prima battuta un dump contente almeno un fluoroforo analizzato nella sessione
Le richieste sono tracciate su Mantis con i tickets 467 471 473',N'Queste funzionalità sono necessarie per permettere l''interfacciamento di strumenti ARIA DX che non supportano l''export di dati in formato Excel, per facilitare la creazione dei dump file da parte di EGSpA e per evitare all''utente di dover associare manualmente i test ID,',N'Senza il supporto del formato txt dell''export, i clienti non possono interfacciare direttamente l''ARIA DX al Results Manager. Senza la gestione migliorata delle colonne usate, ogni strumento dovrebbe avere il suo dump dedicato incrementando il lavoro di creazione e di mantenimento dei dump file da parte di EGpA, senza il fixing della problematica del mapping l''utente deve fare una associazione manuale del test.',NULL,N'FPS
PVS
QM
RT
ITM
SIS',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Program Managment
Ricerca e sviluppo
Sistema informatico
Sistema qualità
Validazioni',N'Una volta rilasciata la nuova versione software, l''Assistenza applicativa ha impatto in termini di attività: comunicazione al cliente del cambiamento, training al cliente, supportare interfacciamento con middleware e LIS 
Una volta rilasciata la nuova versione software, l''impatto sull''assistenza strumenti esterna è in termini di attività:  connettività della nuova versione verso middleware e LIS
Le modifiche apportate al sw Result Manager NON hanno impatto sulle funzionalità in essere e non alterano le prestazioni del sw dichiarate dal fabbricante. Le modifiche hanno un impatto positivo in quanto migliorano l''interfacciamento di strumenti, come ARIA DX, che non supportano l''export dei dati nel formato Excel. In termini di attività SIS deve supervisionare l''attività del fornitore assicurandosi che la documentazione prodotta sia conforme a quanto richiesto da EGSpA per l''effettuazione della validazione.
Le modifiche apportate al sw Result Manager NON hanno impatto sulle funzionalità in essere e no',N'16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021',N'Comunicazione al cliente del cambiamento
Training al cliente
Supportare interfacciamento con middleware e LIS 
Supportare la connettività della nuova versione verso middleware e LIS 
Supervisionare attività fornitore , revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24), stesura draft Release note, aggiornamento bug list 

Collezionare i seguenti documenti da parte del fornitore: Product Certificate_Vxxxx, RM-ResultsManagerVxxxx, RM-ResultsManagerVerificationSummaryReport,ETGrequiredchangesvxxxx
Revisionare piano e report di validazione (MOD05,23 MOD05,24), preparare eventuali dump di supporto durante i test
Revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24) 
Messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente , revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24) , approvazione Release note, approvazione bug list
''',N'Richiesta frase aggiuntiva evidenza regression analysis - results_manager_document.msg
product_certificate_21.2.19.364 - product_certificate_21.2.19.364.pdf
New Features and Improvements - rm-resultsmanager21.2.19.364-190221-1529.pdf
Verification Summary Report, ETG required changes v21.2.19.364 - rm-resultsmanagerverificationsummaryreport,etgrequiredchangesv21.2.19.364-190221-1528.pdf
MOD05,24 REPROT VALIDAZIONE 01/03/2021 MOD05,23 PIANO VALIDAZIONE 01/03/2021 - cc21-9_piano_report_validazione.txt
release_note_v21.9.19.364 - cc21-9_release_note_v21.9.19.364.pdf
MESSA IN USO  results_manager_v.21.2.19.364 - cc21-9_results_manager_v.21.2.19.364.msg
MOD05,24 REPROT VALIDAZIONE 01/03/2021 MOD05,23 PIANO VALIDAZIONE 01/03/2021 - cc21-9_piano_report_validazione_1.txt
MOD05,24 REPROT VALIDAZIONE 01/03/2021 MOD05,23 PIANO VALIDAZIONE 01/03/2021 - cc21-9_piano_report_validazione_2.txt
release_note_v21.9.19.364 - cc21-9_release_note_v21.9.19.364_1.pdf
MESSA IN USO _release_note_v21.9.19.364 - cc21-9_results_manager_v.21.2.19.364_1.msg
MOD05,24 REPROT VALIDAZIONE 01/03/2021 MOD05,23 PIANO VALIDAZIONE 01/03/2021 - cc21-9_piano_report_validazione_3.txt
il sw è RUO, pertanto non è necessario valutare la significatività delle modifiche. - cc21-9_significativiatamodificana.txt
product_certificate_21.2.19.364 - product_certificate_21.2.19.364_1.pdf
New Features and Improvements - rm-resultsmanager21.2.19.364-190221-1529_1.pdf
Verification Summary Report, ETG required changes v21.2.19.364 - rm-resultsmanagerverificationsummaryreport,etgrequiredchangesv21.2.19.364-190221-1528_1.pdf',N'FPS
GASC
CSC
SIS
RDM
RT
ITM
QARA
QM
QAS
PVS
PVM
QARA
QAS
SIS',N'16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021
16/02/2021',NULL,N'08/03/2021
03/03/2021
03/03/2021
03/03/2021
08/03/2021
03/03/2021
03/03/2021',N'No',N'il prodotto non ha il DMRI',N'Si',N'attraverso TAB',N'No',N'nessun impatto sul sw immesso in commercio',N'SW - Elenco Software',N'SW46 - Result Manager',N'RA SW46 da aggiornare',N'No',N'il sw è RUO, pertanto non è necessario valutare la significatività delle modifiche.',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-8','2021-02-08 00:00:00',N'Sabrina Bertini',N'Processo di produzione',N'Modifica del processo di produzione del 956-CTRCPE in seguito ad un aumento dei volumi di vendita di 962-CTRCPE',N'Riduzione del numero dei lotti di preparazione, riduzione del numero di CQ e conseguente contenimento dei costi ',N'Aumento della frequenza di preparazione dei lotti di 956-CTRCPE, aumento dei CQ e quindi aumento dei costi',N'956-CTRCPE - CPE',N'MM
PCT
PP
QARA
QM
RDM
QCT/DS
RDC
RS',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo',N'Riduzione del numero dei lotti di preparazione e conseguente contenimento dei costi. 
Riduzione del numero dei lotti di preparazione e conseguente contenimento dei costi. 
Riduzione del numero dei controlli qualità e conseguente contenimento dei costi 
valutare impatto positivo sul prodotto e sul processo (tempi di scongelamento e preparazione;  volume delle mini bulk)
Impatto positivo, la riduzione del numero dei lotti di prodotti diminuisce il carico di lavoro legato all''approvazione delle etichette e al rilascio dei prodotti finiti
Impatto positivo, la riduzione del numero dei lotti di prodotti diminuisce il carico di lavoro legato all''approvazione delle etichette e al rilascio dei prodotti finiti
Impatto positivo, la riduzione del numero dei lotti di prodotti diminuisce il carico di lavoro legato all''approvazione delle etichette e al rilascio dei prodotti finiti',N'17/02/2021
17/02/2021',N'Valutare l''impatto sulle apparecchiature: apparecchiature per migliorare la lavorazione a freddo per volumi grandi, apparecchiature di stoccaggio per volumi maggiori di prodotto.
necessaria la formazione, a carico di DS, sulle nuove metodiche di preparazione del CPE
aggiornare i moduli di produzione (diluente plasmidi (utilizzo di contenitori sterili anzichè di vetro), moduli di linearizzazione, modulo preparazione bulk (956-CTRCPE))
formazione al personale produzione
stabilire il nuovo volume di produzione e testare tutte le fasi di preparazione del prodotto; redigere report delle attività
stabilire se necessario fare un lotto pilota.
valutare la significatività della modifica e l''eventuale notifica alle autorità competenti.
messa in uso documenti
Aggiornamento Fascicolo Tecnico di Prodotto ',NULL,N'RDM
QCT/DS
MM
QARA
QAS
QAS
RAS',NULL,NULL,NULL,N'Si',N'da valutare l''aggiornamento a seguito della definizione del processo aggiornato',N'No',N'non necessaria, l''aggiornamento del processo produttivo non modifica in alcun modo il prodotto nè le sue performance',N'No',N'nessun impatto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.4 - Calcolo delle quantità
P1.8 - Scongelamento e mescolamento materiali e semilavorati
P1.9 - Misurazione e trasferimento materiali e semilavorati
P1.10 - Mescolamento materiali e semilavorati',N'nessun impatto sul processo',N'No',N'vedere mod05,36',N'No',N'nessun virtual manufacturer',N'Si',N'da valutare la necessità di aggiornamento della documentazione di gestione dei rischi a seguito della definizione del processo aggiornato',N'Sabrina Bertini',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-7','2021-02-08 00:00:00',N'Samuela Margio',N'Progetto',N'Il presente Change Control richiede l''approvazione a procedere con l''introduzione di due modifiche a carico dei progetti HIV1 ELITe MGB Kit (RTK600ING) e HCV ELITe MGB Kit (RTK601ING):
1. SAMPLE VOLUME IN PCR: il volume di campione da inserire nella reazione di PCR passa da 10ul a 20ul, raddoppiando rispetto a quanto indicato negli MR 85 dei documenti MRD2019-003 e MRD2019-005.
2. SAMPLE VOLUME IN ESTRAZIONE: il volume di estrazione passa da 1000ul (come indicato negli MR90 dei documenti MRD2019-003 e MRD2019-005) a 600ul, con eluizione in 50ul. Verrà mantenuto il prodotto di estrazione ELITe InGenius® SP 1000 (INT033SP1000).
Prima di questo CC era stata proposta una soluzione diversa (vedi CC20-84) che non ha raggiunto i risultati attesi. Per tale ragione il CC20-84 non è stato implementato per HIV e HCV.  
Inoltre al fine di allineare l''usabilità del sub-componente CPE (CPE601ING e CPE600ING) con il sub-componente MIX il numero di sessioni di lavoro per tubino di CPE passerà da 12 a 6. In ogni sessione sarà possibile allestire un minimo di dure reazioni, per un totale di 12 reazioni per tubino di CPE. ',N'Tale cambiamento è frutto delle attività documentate nei Feasibility Report (Feasibility HCV ELITe MGB Kit High sensitivity, REP2021-011 e Feasibility HIV1 ELITe MGB Kit High sensitivity, REP2021-012) e volte ad implementare la sensibilità analitica (Limit of Detection) dei prodotti attualmente non rispondente al requisito di mercato. Sulla base dei dati raccolti la nuova stima di LoD ammonta a 20IU/ml per HCV (target = 15IU/ml) e 30IU/ml per HIV (target = 70IU/ml). ',N'Qualora il cambiamento non fosse implementato si aprirebbero due scenari:
A) proseguire con il progetto con performance di prodotto non in linea con i requisiti di mercato (opzione attualmente non approvata dal Team di Prodotto - vedi mancata approvazione CC20-84) 
B) attendere lo sviluppo da parte di PSS di un nuovo prodotto di estrazione da 1000ul. Tale prodotto sarebbe ottimizzato per l''uso con alti volumi di campione aumentando l''attuale efficienza di estrazione e prevenendo le problematiche di gelificazione all''interno della cartuccia, Questa soluzione porterebbe ad un ritardo sullo sviluppo del prodotto di almeno 2 mesi e mezzo (ferma restando la consegna del nuovo kit di estrazione in Aprile). Inoltre sarebbe necessario marchiare il nuovo prodotto CE-IVD.',N'964-RTK601ING - HCV ELITe MGB Kit
964-RTK600ING - HIV1 ELITe MGB Kit',N'MM
PMS
QARA
QC
RDM
MS
PVM
VP_R&D',N'Acquisti
Controlling
Controllo qualità
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo',N'Nessun impatto negativo a cambiamento avvenuto. Le performances di prodotto miglioreranno, pertanto sarà possibile procedere con minor rischio alla fase di Verifica e Validazione.
Nessun impatto negativo a cambiamento avvenuto. Le performances di prodotto miglioreranno, pertanto sarà possibile procedere con minor rischio alla fase di Verifica e Validazione.
Nessun impatto negativo a cambiamento avvenuto. Le performances di prodotto miglioreranno, pertanto sarà possibile procedere con minor rischio alla fase di Verifica e Validazione.
Impatto positivo sulle performances di prodotto. Impatto negativo sulla tempistica di chiusura del progetto. I ritardi dovuti alle performances non ottimali del prodotto hanno portato ad un rallentamento delle attività di progetto. La chiusura stimata rischia di non arrivare in tempo per la sottomissione a DEKRA secondo l''attuale IVDD 81 Luglio 2021)  
Impatto positivo sulle performances di prodotto. Impatto negativo sulla tempistica di chiusura del progetto. I ritard',N'18/02/2021
18/02/2021
18/02/2021
18/02/2021
17/02/2021
17/02/2021',N'Aggiornamento della documentazione di Design Transfer:
-PML
-Design Transfer Plan
-MOD di Produzione e CQ (incluso nuovo modulo per STD e materiale di riferimento)
-Assay Protocols
Aggiornamento Piano di Verifica e Validazione 
Stesura Report di Feasibility
Avviare ordine per materiale per nuovi lotti pilota (5LP HIV e 4-5 LP HCV)
Pianificare Lotti Pilota
Aggiornare Gantt di progetto
Pianificare Riesame di Sviluppo 
Aggiornare MKT sui possibili esiti a seguito del cambiamento.
Avvio produzione Lotti Pilota come da pianificazione concordata con Project Leader.
Aggiornare Anagrafiche di prodotto e avviare nuova costificazione
Procedere con l''acquisto delle materie prime come da indicazione di R&D
Aggiornare la costificazione dei prodotti HIV1 ELITe MGB KIt e HCV ELITe MGB Kit
Aggiornamento MOD Prod e CQ come da indicazioni di R&D + Formazione Prod e CQ sui moduli stessi (incluso nuovo modulo di STD)
Verificare necessità di aggiornamento Documenti gestione di rischio, RA/RP e RE. ',N'IN USO mod957-qc-pei_xxx - cc21-7_mod957-qc-pei_xxx.msg
Aggiornamento PROGRAM-MKT-R&D_Cambiamento 600-50 e 20+20 - viral_load_update_08022021_cambiamento_sp1000.pptx',N'RDM
RS
PC
RS
PMS
PP
QCT/DS
PMS
PMS
PP
PW
PT
C
QCT/DS
VP_R&D
RAS',N'18/02/2021
18/02/2021
18/02/2021
18/02/2021
17/02/2021
18/02/2021',N'18/02/2021
18/02/2021
19/02/2021
08/02/2021',N'18/02/2021
18/02/2021
18/02/2021
18/02/2021
17/02/2021',N'No',N'da valutare',N'No',N'na, prodotto in sviluppo',N'No',N'na, prodotto in sviluppo',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.8 - Fase di Sviluppo: Reportistica',N'nessun impatto',N'No',N'na, prodotto in sviluppo',N'No',N'nessun virtual manufacturer',N'No',N'da valutare',NULL,'2021-02-19 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-6','2021-02-03 00:00:00',N'Gabriella Campanale',N'Assay Protocol',N'Adeguamento del pacchetto di Assay Protocols relativi al prodotto RTS170ING alla versione 1.3.0.16 del software e creazione dell''Assay Protocol quantitativo che non prevede l''utilizzo dell''Internal Control.
Modifiche che verranno introdotte: 
- ULoQ 250000000
- Amp. cr. 2 per canale IC (come da RAP20-47)
- creazione nuovo AP quantitativo con possibilità di non utilizzare l''IC esogeno.
 L''aggiornamento del AP non ha impatto sulle performance del prodotto',N'Nel caso degli Assay Protocols quantitativi, l''operatore non è costretto a utilizzare l''Internal Control esogeno; inoltre è possibile impostare come limite massimo di Upper Limit of Quantification (ULoQ) 250000000c/mL invece delle attuali 100000000.',N'Gli utilizzatori della versione quantitativa del prodotto sarebbero costretti ad usare un controllo interno endogeno in quanto l''impostazione attuale dell''AP obbliga ad utilizzarlo.',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240
962-RTS170ING-96 - SARS-CoV-2 ELITe MGB Kit - 96 ',N'QARA
QC
RDM
PVM',N'Assistenza applicativa
Controllo qualità
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Riduzione dell''errore 30108 IC: Ct calculation error - If significant amplification is observed in PCR plot, target detected
Riduzione di richichiesta da parte dei clienti di implementare l''AP con la possibilità di non utilizzare l''IC
Nessun impatto sul prodotto. Possibilità, da parte dell''operatore, di eseguire la sessione di amplificazione senza l''obbligo di aggiungere il Controllo Interno esogeno (CPE)
nessun impatto se non in termini di attività da svolgere
nessun impatto se non in termini di attività da svolgere',N'03/05/2021',N'- installazione nuova revisione degli AP sugli strumenti
- modifica dei moduli di cq
- installazione presso i clienti delle nuove versioni degli AP
- stesura TAB
- modifica pacchetto Assay Prtotocols
- creazione nuovo Assay Protocol Assay_SARS-CoV-2 ELITe_QT_Open_200_100
- aggiornamento del manuale
- verifica degli Assay Protocols
- analisi sessioni di validazione
- Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
- comunicazione e messa in uso dei nuovi assay protocol
- messa in uso manual aggiornato',N'TAB P40 - Rev AC  - elite_ingenius_-tab_p40_-_rev_ac_-_product_sars-cov-2_elite_mgb_notice.pdf
- modifica pacchetto Assay Prtotocols - creazione nuovo Assay Protocol Assay_SARS-CoV-2 ELITe_QT_Open_200_100 - aggiornamento del manuale - cc21-6_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137.msg
ap e ifu in uso - cc21-6_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137_1.msg
- modifica pacchetto Assay Prtotocols - creazione nuovo Assay Protocol Assay_SARS-CoV-2 ELITe_QT_Open_200_100 - aggiornamento del manuale - cc21-6_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-6_mod05,36__non_significativa.pdf
installato ap cq su strumenti interni - cc21-6_ifu_sars-cov-2_elite_mgb_kit_scp2020-0137.msg
MOD10,13-CTR170ING_03, MOD10,13-RTS170ING_04, MOD10,13-STD170ING_02 - cc21-6messa_in_uso_mod1013-xx170ing_cc21-6.msg',N'FPS
GPS
RS
PVS
QAS
QCT/DS',N'03/05/2021',N'07/05/2021',N'24/05/2021
24/02/2021
18/02/2021
24/02/2021',N'No',N'VERIFICARE IMPATTO SUL DMRI',N'Si',N'attraverso TAB',N'No',N'nessun impatto sul prodotto',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'nessun impatto sul RA AP1',N'No',N'vedere MOD05,36. La modifica degli AP non è considerata significativa in termini di notifica all''autorità competente.',N'No',N'nessun virtula manufacturer',N'No',N'da verificare',NULL,'2021-05-07 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-5','2021-01-28 00:00:00',N'eleonora deva',N'Documentazione SGQ',N'Si richiede la modifica del MOD10,01 per l''introduzione di uno spazio dedicato a informazioni utili a QCT, come l''invio di campioni uso CQ. Inoltre, si chiede di eliminare la colonna "ENTRO IL" poichè non più presente nel database di pianificazione.',N'Questa modifica permette un miglioramento della tracciabilità dei campioni uso CQ ed eventuali altre informazioni utili.',N'Se il cambiamento non fosse implementato la tracciabilità dei campioni uso CQ continuerebbe ad essere carente.',NULL,N'MM
QARA
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Miglioramento della tracciabilità dei campioni uso CQ inviati insieme ai CTR.
Miglioramento della tracciabilità dei campioni uso CQ inviati insieme ai CTR.
Nessun impatto sul processo, ma solo in termini di attività.',N'28/01/2021
28/01/2021
28/01/2021',N'Modifica del MOD10,01 e relativa formazione e simulazione
Simulazione del documento
Messa in uso della nuova revisione del MOD10,01',N'mod10,01 rev04 - cc21-5_messa_in_uso_mod1001_1.msg
messa in uso MOD10,01 rev04  - cc21-5_messa_in_uso_mod1001.msg
la modifica non è significativa, pertanto non è da notificare. - cc21-5_mod05,36__non_significativa.pdf',N'QC
QCT/DS
PP
QARA
QM
QAS',N'28/01/2021
28/01/2021
28/01/2021',NULL,N'18/02/2021
18/02/2021
18/02/2021',N'No',N'la modifica non prevede modifiche al DMRI',N'No',N'non necessaria, si tratta della modifica di un modulo ad uso interno',N'No',N'nessun impatto, si tratta della modifica di un modulo ad uso interno',N'PR - Preparazione Lotto Unico',N'PR1 - pianificazione',N'in corso di ceazione un  risk assessement che includa la gestione delle richieste di CQ e pre CQ da parte della produzione (vedere anche RAP20-44) ',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,'2021-02-18 00:00:00',N'No',NULL,NULL),
    (N'21-4','2021-01-26 00:00:00',N'Katia Arena',N'Materia prima',N'A causa della pandemia COVID 19, diversi fornitori di plastiche hanno difficoltà a soddisfare gli ordini per l''aumento delle quantità richieste dovute alla crescita improvvisa dei consumi. Si chiede pertanto la ricerca di secondi fornitori dei seguenti materiali di consumo utilizzati in R&D, CQ e produzione:
- tubi falcon da 15 ml e 50 ml, rispettivamente i codici interni SPEC953-068 e SPEC953-069
- contenitori da 125ml, 250ml, 500 ml, 1l, 2,l, 5l, rispettivamente codici interni SPEC953-064_Flacone ST PET 125 ml, SPEC953-189_02_Contenitori_250_mL, SPEC953-029_02_Contenitori_500mL, SPEC953-172_02_Contenitori_1L, SPEC953-238_00_Contenitori_2L, SPEC953-244_00_Contenitori_5L',N'Ricerca di un secondo fornitore per soddisfare l''alta richiesta di prodotti e materiali.',N'L''insufficienza di contenitori potrebbe portare ad un blocco della produzione.',N'953-029 - Flacone ST PET 500 ml (60pz)
953-064 - Flacone ST PET 125 ml (100pz)
953-068 - Centrifuge Tube 15 ml Printed
953-069 - Provette PP 50 ml fondo conico
953-172 - Flacone ST PET 1000 ml (30 pz)
953-238 - Flacone ST PET 2000 mL (14pz)
953-244 - Flacone ST PET 5000 mL (6pz)',N'MM
QARA
QC
QM
RDM',N'Acquisti
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo',N'Nel caso si selezionasse un secondo fornitore l''ufficio acquisti avrebbe la possibilità di ordinare da due distinti fornitori scegliendo per ogni acquisto l''opzione più vantaggiosa in termini di costi e disponibilità dei prodotti.
I contenitori oggetto del cambiamento sono utilizzati durante i processi produttivi di controllo qualità e di ricerca, non sono contenitori primari del prodotto. E'' necessario dimostrare l''equivalenza dei nuovi contenitori selezionati con quelli correntemente in uso attraverso confronto delle specifiche e, se necessario, test sperimentali. In generale, l''introduzione di secondi fornitori di materiale plastico (equivalente a quello in uso) ha un impatto positivo in quanto garantisce l''approvvigionamento anche in situazioni critiche, come la pandemia in corso.
I contenitori oggetto del cambiamento sono utilizzati durante i processi produttivi di controllo qualità e di ricerca, non sono contenitori primari del prodotto. E'' necessario dimostrare l''equivalenza dei nuovi contenit',N'02/02/2021
02/02/2021
02/02/2021
02/02/2021
02/02/2021
02/02/2021
17/02/2021',N'Gestione accordi con i fornitori alternativi. Gestione della selezione e qualifica del fornitore.
Individuazione fornitori alternativo/i
Valutare i nuovi contenitori ed aggiornare/creare nuove specifiche (ATTENZIONE: la creazione di nuove specifiche ha un elevato impatto sull''aggiornamento di tutti i DMRI, valutare accuratamente la possibilità di aggiornare le specifiche esistenti)
Valutare l''esecuzione di test per verificare l''equivalenza dei contenitori approvvigionati da fornitori alternativi e stesura di un report riassuntivo
messa in uso documentazione
Verificare la significatività della modifica e se necessario identificare i Paesi in cui il prodotto è registrato per effettuare la notifica
modifica allegato a ALL1_IO10,06_CONTENITORI_CQ
formazione
messa in uso
nessuna attività',N'spec953-068_02_e_spec953-069_02 - cc21-4_messa_in_uso_spec953-068_02_e_spec953-069_02.msg
 I contenitori per i quali si cerca un secondo fornitore non sono contenitori primari, ma materiali di consumo utilizzati nel processo produttivo, di CQ o R&D considerati NON critici. L''introduzione di un secondo fornitore non si ritiene, pertanto, una modifica significativa da notificare. - cc21-4_mod05,36__non_significativa_1.pdf',N'PW
RT
RS
RS
QAS
QARA
QAS
MM
QC
QCT/DS',N'02/02/2021
02/02/2021
02/02/2021
17/02/2021
17/02/2021',NULL,N'17/02/2021
17/02/2021',N'No',N'se NON vengono introdotte nuove specifiche i DMRI non sono da aggiornare',N'No',N'non necessaria, viene introdotto un secondo fornitore per materiali di consumo utilizzati nei processi di produzione ricerca e cq',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto',N'No',N'vedere MOD05,36. I contenitori per i quali si cerca un secondo fornitore non sono contenitori primari, ma materiali di consumo utilizzati nel processo produttivo, di CQ o R&D considerati NON critici. L''introduzione di un secondo fornitore non si ritiene, pertanto, una modifica significativa da notificare.',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-3','2021-01-18 00:00:00',N'Enzo Scelsi',N'Documentazione SGQ',N'Si richiede di aggiornare Bartender alla nuova versione in uso per la miglior gestione delle stampe da Navision',N'Avere la versione aggiornata del software',N'Nessuna',NULL,N'ITT
MM
QARA
QM
ITM',N'Produzione
Regolatorio
Sistema informatico',N'Nessun impatto in quanto dalla release note della versione 2021 (allegata), dalle nuove funzionalità introdotte e dalla risoluzione dei bug non si riscontrano impatti sul processo di stampa etichette.
Nessun impatto in quanto dalla release note della versione 2021 (allegata), dalle nuove funzionalità introdotte e dalla risoluzione dei bug non si riscontrano impatti sul processo di stampa etichette.
Nessun impatto in quanto dalla release note della versione 2021 di Bartender, dalle nuove funzionalità e dalla risoluzione dei bug non si riscontrano impatti sul processo di stampa etichette, NON è pertanto necessario rivalidare il processo, ma si ritiene sufficiente effettuare dei test minimi al fine di verificare che le etichette stampate dalla versione in produzione e dalla versione 2021 in test siano identiche.
Nessun impatto in quanto dalla release note della versione 2021 di Bartender, dalle nuove funzionalità e dalla risoluzione dei bug non si riscontrano impatti sul processo di stampa etichette, ',N'18/01/2021
18/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',N'Test nella versione di prova: effettuare una verifica minima tra etichette stampate con la versione in uso e con la versione in test (almeno 1 etichetta per ciascun formato)
Approvazione test di verifica
Mettere in uso la nuova versione di Bartender 2021 (aggiornare MOD05,08)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Test nella versione di prova e messa in uso sul server di produzione: effettuare una verifica minima tra etichette stampate con la versione in uso e con la versione in test (almeno 1 etichetta per ciascun formato)',N'Release note  bartender 2021 - cc20-3_bartender_2021_new_features.docx
La nuova versione di Bartender, v. 11.2/161872, installata in produzione in data 11/2/2021,  ha segnalato un errore dureante il normale funzionamento del processo di stampa etichette non emerso durante i test. Il Responsabile IT ha valutato opportuno annullare l''utilizzo della nuova versione, ri-installare la vecchia e contattatare il fornitore per avere informazioni su come procedere. - cc21-3_noaggiornamentobartender_1.txt
bartender v.11.2161872 - cc21-3_messa_in_uso_bartender_v._11.2161872.msg
La nuova versione di Bartender, v. 11.2/161872, installata in produzione in data 11/2/2021,  ha segnalato un errore dureante il normale funzionamento del processo di stampa etichette non emerso durante i test. Il Responsabile IT ha valutato opportuno annullare l''utilizzo della nuova versione, ri-installare la vecchia e contattatare il fornitore per avere informazioni su come procedere. - cc21-3_noaggiornamentobartender.txt',N'ITT
MM
QARA
QM
QAS
QARA
QAS',N'19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',N'29/01/2021
29/01/2021
29/01/2021
29/01/2021',NULL,N'No',N'nessun impatto sul DMRI',N'No',N'non necessaria, è una modifica relativa ad un sw interno',N'No',N'nessun impatto sul prodotto immesso in commercio',N'SW - Elenco Software',N'SW3 - Bartender Edition Automation (gestione stampa etichette)',N'nessun impatto sul RA SW3 in quanto le modifiche apportate con la nuova revisione non hanno alcun impatto sul processo di stampa delle etichette',N'No',N'vedere MOD05,03. la notifica non è necessaria in quanto il sw non è un IVD e la nuova versione non ha impatto sul processo di stampa delle etichette',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,'2021-01-29 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'21-2','2021-01-15 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso
Assay Protocol',N'Creazione di manuali ad hoc per la registrazione dei prodotti CMV ELITe MGB Kit, EBV ELITe MGB Kit e BKV ELITe MGB KIt a Singapore. Questi manuali riportano solo la validazione di alcune matrici solo su InGenius e solo l''applicazione qualitativa (BKV con applicazione quantitativa solo su plasma). Creazione degli AP qualitativi dei prodotti.',N'Possibilità di registrare questi prodotti a Singapore e quindi venderli.',N'Nessuna registrazione e vendita a Singapore',N'962-RTK015PLD.- - CMV ELITe MGB Kit 100T
962-RTS020PLD.- - EBV ELITe MGB KIT 100T
962-RTS175PLD.- - BKV ELITe MGB KIT 100T',N'QARA
RDM
PVM',N'Regolatorio
Ricerca e sviluppo
Validazioni',N'Nessun impatto.
Nessun impatto.
Nessun impatto.
Gestione di manuali aggiuntivi, creati apposta per Singapore.
Gestione di manuali aggiuntivi, creati apposta per Singapore.
Nessun impatto.
Nessun impatto.',N'19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',N'revisione delle nuove IFU
Aggiornamento dei report diagnostici (PVR2014-038-03 e REP2017-051) dell''estensione d''uso del prodotto BKV ELITe MGB Kit in associazione ad InGenius e plasma con la nuova statistica
Creazione degli AP qualitativi
creazione di IFU specifiche per Singapore.  Questi manuali riportano solo la validazione di alcune matrici solo su InGenius e solo l''applicazione qualitativa (BKV con applicazione quantitativa solo su plasma)  e saranno rilasciate al distributore di Singapore
Rilascio delle IFU e degli AP
Revisione delle nuove IFU
Revisione degli AP',NULL,N'PVS
PVM
PVS
PVM
RAS
RDM
RS
PVS
RDM
RS
RAS',N'19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',NULL,NULL,N'No',N'nessun impatto',N'No',N'nessuna comunicazione necessario se non al Distributore di Singapore',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'nessun impatto',N'No',N'nessuna notifica necessaria, la modifica viene fatta per poter registrare i prodotti a Singapore.',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Michela Boi',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'21-1','2021-01-12 00:00:00',N'Alessandra Gallizio',N'Dismissione prodotto',N'A seguito degli accordi intrapresi tra ELITech e R-Biopharm, ELITech non produrrà più per conto di R-Biopharm (Legal Manufacturer) i seguenti kit: RIDAGENE CMV (PG9005), RIDAGENE EBV (PG9045) , RIDAGENE HSV1 (PG9015), RIDAGENE HSV2 (PG9025) e RIDAGENE VZV (PG9035). Si richiede pertanto la dismissione dei codici dei materiali di confezionamento, dei codici materiali, dei moduli di produzione e di controllo qualità.
Sempre nell''ambito degli accordi intrapresi tra ELITech ed R-Biopharm, i prodotti di R-Biopharm PG1725, PG2405 e PG2205 non verranno convertiti in prodotti OEM per ELITech: si richiede la chiusura dei progetti SCP2017-012, SCP2017-013, SCP2019-010.',N'eliminazione di codici e documenti inutilizzati',N'possibile fonte di confusione dovuta alla presenza in uso di codici e documenti obsoleti',N'963-PG9005-CMV - RIDA GENE CMV KIT
963-PG9015-HSV1 - RIDA GENE HSV1 KIT
963-PG9025-HSV2 - RIDA GENE HSV2
963-PG9035-VZV - RIDA GENE VZV
963-PG9045-EBV - RIDA GENE EBV',N'MM
PW
QC
QM
RAS
VP_R&D',N'Acquisti
Controllo qualità
Magazzino
Ordini
Produzione
Regolatorio
Sistema qualità',N'nessuno
Non essendo i fabbricanti di questi prodotti l''impatto è minimo ed in termini di attività: archiviazione della documentazione di produzione (moduli prod. e cq ed etichette) e degli FTP. Non c''è impatto sui clienti in quanto gestiti da R-Bio, non c''è impatto sulle autorità competenti in quanto i prodotti sono registrati da R-Bio. non c''è impatto sulla gestione dei reclami in quanto sono gestiti da R-Bio (per semplicità nella tracciabilità indicare gli ultimi lotti di ciascun prodotto in questo cc)
Non essendo i fabbricanti di questi prodotti l''impatto è minimo ed in termini di attività: archiviazione della documentazione di produzione (moduli prod. e cq ed etichette) e degli FTP. Non c''è impatto sui clienti in quanto gestiti da R-Bio, non c''è impatto sulle autorità competenti in quanto i prodotti sono registrati da R-Bio. non c''è impatto sulla gestione dei reclami in quanto sono gestiti da R-Bio (per semplicità nella tracciabilità indicare gli ultimi lotti di ciascun prodotto in que',N'19/01/2021
19/01/2021',N'dismissione codici (vedi allegato) e archiviazione fascicoli
 archiviazione della documentazione di produzione (moduli prod. e cq ed etichette)
archiviazione degli FTP
indicare gli ultimi lotti di ciascun prodotto
nessuna attività a carico
dismissione della stampante delle etichette r-bio
fornire a QAS gli ultimi lotti prodotti',N'Elenco documentazione obsoleta - allegato_al_cc_1-21.docx
MODULI CQ ARCHIVIATI - cc21-1__mod_cq_da_archiviare.xlsx',N'QM
QAS
QAS
RAS
QAS
QC
ITT
MM
MM
PP',N'19/01/2021
19/01/2021
19/01/2021',N'26/02/2021
12/01/2021',N'19/04/2021
19/01/2021',N'No',N'nessun impatto',N'No',N'non essendo i fabbricanti non è nostra responsabilità dare comunicazione al cliente',N'No',N'nessun impatto sul prodotto immesso in commercio',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'la dismissione dei prodotti non ha impatto sul RA DC12',N'No',N'non essendo i fabbricanti non è nostra responsabilità dare comunicazione alle autorità competenti',N'No',N'è il virtual manufacturer stesso che ci ha comunicato la dismissione dei prodotti ',N'No',N'na',N'Roberta  Paviolo','2021-02-26 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-85','2020-12-23 00:00:00',N'Katia Arena',N'Fornitore
Materia prima',N'A causa della pandemia COVID 19, diversi fornitori di plastiche hanno difficoltà a soddisfare gli ordini per l''aumento delle quantità richieste dovute a una crescita improvvisa dei consumi di alcuni prodotti utilizzati dai nostri clienti. Questo potrebbe avere un impatto anche per le forniture di platiche utilizzate nella produzione dei nostri kit.  
In particolare, Il fornitore Sarstedt non riesce a garantirci le forniture di tubi da 0.5ml, codice 909-72.730005, e di tubi da 2 ml, codice 909-72.694005 necessari ai nostri clienti per trasferire i volumi richiesti per i test sullo strumento Elite InGenius.
Gli stessi tubi Sarsted (codice interno SPEC953-061 "Provetta 0,5 ml" e SPEC953-065 "Provetta 2,0 ml")  sono utilizzati anche come contenitori primari per i nostri prodotti,  portandoci a supporre che una difficoltà di approvvigionamento potrebbe avere un impatto negativo anche sulla produzione dei nostri kit se dovesse esserci un ulteriore aumento.',N'Ricerca di un secondo fornitore per soddisfare l''alta richiesta di prodotti e materiali.',N'Incapacità a soddisfare le richieste dei clienti e, nel caso della produzione dei nostri prodotti, potremmo incorrere in un blocco di produzione.',N'909-72.694005 - Micro tube 2ml, PP-10x100pz
909-72.730005 - 0,5 ml tube separ lid 1000pz
953-061 - Provetta 0,5 ml tappo staccato
953-065 - Provetta 2,0 ml tappo staccato',N'GSC
MM
PP
PW
QARA
QC
QM
RDM
SAD-OP
GASC
C',N'Acquisti
Assistenza applicativa
Controllo qualità
Global Service
Marketing
Produzione
Regolatorio
Ricerca e sviluppo',N'Nel caso si selezionasse un secondo fornitore l''ufficio acquisti avrebbe la possibilità di ordinare da due distinti fornitori scegliendo per ogni acquisto l''opzione più vantaggiosa in termini di costi e disponibilità dei prodotti.
La selezione di nuovi tubi utilizzati come contenitori primari nei prodotti EGSpA necessita di test per verificarne l''equivalenza con i tubi Sarsted attualmente utilizzati in produzione.
La selezione di nuovi tubi utilizzati come contenitori primari nei prodotti EGSpA necessita di test per verificarne l''equivalenza con i tubi Sarsted attualmente utilizzati in produzione.
Ricerca nuovi fornitori di tubi necessari ai nostri clienti per trasferire i volumi richiesti per i test sullo strumento Elite InGenius. Esecuzione di test sullo strumento InGenius per verificarne la compatibilità.
La selezione di nuovi tubi utilizzati come contenitori primari nei prodotti EGSpA necessita di test per verificarne l''equivalenza con i tubi Sarsted attualmente utilizzati.
La selezione di n',N'25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021',N'Gestione accordi con i fornitori alternativi. Gestione della selezione e qualifica del fornitore.
Esecuzione test per verificare l''equivalenza dei tubi approvvigionati da fornitori alternativi con i tubi Sarsted attualmente utilizzati in produzione e stesura di un report riassuntivo. Comunicazione immediata dell''equivalenza dei test a SGQ per procedere tempestivamente alla notifica preliminare.
aggiornare i moduli di produzione.
Esecuzione di test sullo strumento InGenius per verificare la compatibilità del nuovo tubo e stesura di un report riassuntivo
Valutare nuovi tubi per la produzione e aggiornare/creare nuove specifiche (ATTENZIONE: la creazione di nuove specifiche ha un elevato impatto sull''aggiornamento di tutti i DMRI, valutare accuratamente la possibilità di aggiornare le specifiche esistenti)
Esecuzione test per verificare l''equivalenza dei tubi approvvigionati da fornitori alternativi con i tubi Sarsted attualmente utilizzati in PRODUZIONE e stesura di un report riassuntivo
Valutazio',N'TEST TUBI STEROGLAS 2 ML CONFORME - cc20-85_test_tubi_steroglass_2_ml_conforme.pptx
risultati test con tubi forniti da APTACA e SIC - cc20-85_sarsted_tube_alternative_and_comparison_221220.pptx
la notifica è da effettuare, nei paesi in cui sono state inviate le specifiche dei materiali, solo nel caso siano introdotti nel processo produttivo dei nuovi contenitori primari (secondo fornitore de seguenti tubi, codice interno: SPEC953-061 "Provetta 0,5 ml" e SPEC953-065 "Provetta 2,0 ml". La notifica non è da effettuare per l''introduzione di nuovi materiali di consumo per uso cliente (tubi di rivendita, codice 909-72.730005, e 2 ml, codice 909-72.694005 ) - cc20-85__mod05,36_significativa.pdf',N'RDM
PW
QC
GSTL
GSS
QCT/DS
RS
RS
QARA
QM
QARA
QAS
RAS
RAS
MS
GASC
QARA
RAS
QAS
RDM',N'25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021
25/01/2021',NULL,NULL,N'No',N'I DMRI di prodotto sono da aggiornare unicamente in caso di creazione di nuove specifiche',N'Si',N'da effettuare',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto',N'Si',N'da effettuare nel caso i nuovi tubi siano introdotti nel processo produttivo',N'No',N'nessun virtual manufacturer',N'No',N'Il presupposto di questo change control è la dimostrazione dell''equivalenza fra i tubi attualmente in uso e i nuovi tubi che verranno selezionati. A seguito di tale dimostrazione non sarà necessario aggiornare la documentazione di gestione dei rischi in quanto l''equivalenza garantirà le medesime prestazioni e la medesima stabilità del reagente. I tecnici di Produzione saranno adeguatamente formati e le modalità di utilizzo da parte del cliente non varieranno.',N'Federica Farinazzo',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-84','2020-12-22 00:00:00',N'Samuela Margio',N'Progetto',N'Durante i test effettuati in fase di sviluppo sul prodotto HIV1 ELITe MGB Kit (RTK600ING, SCP2019-021) ed in fase di verifica e validazione sul prodotto HBV ELITe MGB Kit (RTK602ING, SCP2019-022) con il kit di estrazione ELITe InGenius SP 1000 (INT033SP1000) in associazione a campioni di 1 mL di plasma EDTA e al protocollo 1000-50, sono emersi problemi di efficienza e di ripetibilità dell''estrazione. Tali fenomeni si sono manifestati come gelificazione del campione in test all''interno della cartuccia di estrazione e aggregazione delle biglie magnetiche che hanno portato alla perdita delle biglie magnetiche in diversi passaggi (= pozzetti della cartuccia), alla contaminazione della barriera per gli aerosol e alla contaminazione delle pipette stesse dello strumento.
Dal punto di vista dei prodotti, le principali conseguenze di tali eventi sono state 1) un elevato numero di campioni invalidi con Ct del Controllo interno superiore all''atteso, 2) una dispersione dei dati relativi al controllo interno superiore all''atteso 3) una scarsa efficienza del sistema che ha portato ad una sottostima dei campioni testati e a una sensibilità insufficiente con la matrice Plasma EDTA 4) una scarsa ripetibilità dei risultati ottenuti nei test di Controllo Qualità. 
Dal punto di vista dello strumento, tale fenomeno ha causato l''alterazione della funzionalità delle pipette fino al loro blocco dovuto alla contaminazione con i lisati, con conseguente necessità di manutenzione straordinaria per la pulizia delle siringhe e rischio di contaminazione della strumentazione stessa.
A seguito di una prima indagine è stato possibile escludere l''interessamento di uno specifico lotto di estrazione, poiché anche lotti diversi hanno presentato lo stesso fenomeno.
PSS, produttore del kit di estrazione, ha dichiarato che il fenomeno può verificarsi con alcuni campioni di plasma e può essere evitato diluendo il campione in acqua, suggerimento ritenuto però non praticabile. A tale proposito, PSS sta già lavorando ad un nuovo prodotto per l''estrazione di 1 mL di campione, che sarà disponibile a metà del 2021.
Una possibile mitigazione è rappresentata dall''uso diretto di una quantità di campione di partenza inferiore a 1 mL. Il protocollo 800-50 che impiega campioni di 800 uL di plasma non è però stato risolutivo, poiché il problema si è presentato nuovamente presso il centro esterno "CoQuaLab" che aveva avviato i test di validazione per HBV con i campioni clinici certificati negativi.
Al fine di ridurre i rischi di malfunzionamento del sistema (ELITe InGenius + ELITe MGB Kit) si richiede il cambiamento di kit di estrazione e del relativo protocollo di estrazione passando al prodotto ELITe InGenius SP 200 (INT032SP200) con protocollo 200-50. Questo prodotto è infatti ampiamente validato ed utilizzato con campioni clinici.
Tale cambiamento deve essere applicato ai tre progetti della categoria "Viral Load" per cui è richiesta uniformità di protocolli di estrazione: HIV1 ELITe MGB Kit (RTK601ING, SCP2019-021), HBV ELITe MGB Kit (RTK602ING, SCP2019-022), HCV ELITe MGB Kit (RTK601ING, SCP2019-020), e al progetto CMV RNA ELITe MGB Kit (RTS115ING, SCP2019-019), per cui era previsto l''uso del kit di estrazione ELITe InGenius SP 1000 (INT033SP1000).
Inoltre al fine di allineare l''usabilità del sub-componente CPE HBV (CPE602ING) con il sub-componente MIX il numero di sessioni di lavoro per tubino di CPE passerà da 12 a 6. In ogni sessione sarà possibile allestire un minimo di due reazioni, per un totale di 12 reazioni per tubino di CPE',N'Il passaggio al kit di estrazione ELITe InGenius SP 200 (INT032SP200) e del protocollo 200-50 consentirebbe, in base alle evidenze ottenute, di proseguire subito i progetti senza mettere a rischio la funzionalità degli strumenti ELITe InGenius e la ripetibilità dei risultati ottenuti con saggi diagnostici.
Dai test preliminari è atteso un calo di sensibilità (da 3 a 5 volte) del saggio diagnostico ma garantisce una migliore robustezza dell''estrazione che permetterà di evitare completamente l''insorgenza di fenomeni di gelificazione del campione. 
L''uso del kit di estrazione ELITe InGenius SP 200 e del protocollo 200-50 consentirebbe inoltre di proseguire subito i progetti partendo con la fase di Verifica e Validazione senza attendere la nuova versione del kit di estrazione da 1 mL di campione in sviluppo presso PSS (ritardo di circa 6 mesi). 
Dal punto di vista dell''usabilità, con il kit di estrazione ELITe InGenius SP 200 è possibile caricare sullo strumenti ELITe InGenius direttamente il tubo primario, cosa non consentita con il kit di estrazione ELITe InGenius SP 1000 (INT033SP1000).
Inoltre, quando sarà effettuato il secondo round di verifica e validazione dei saggi diagnostici in associazione al nuovo strumento ELITe BeGenius, sarà possibile eseguire i test con la nuova versione del kit di estrazione di PSS e il volume di 1 mL di campione.
Per quanto il valore aggiunto sia stato documentato sopra, è necessario sottolineare che il cambio del prodotto di estrazione avrà un impatto negativo sul Limite di Rilevazione dei tre sistemi che difficilmente potrà rispettare quanto richiesto dai relativi Market Requirement Documents e incorporato nei Product Requirement Documents.
Inoltre, dal momento che questi progetti non hanno tempi di buffer, il cambio del prodotto di estrazione causerà un ritardo nel rilascio dei tre progetti della categoria "Viral Load", HIV1 ELITe MGB Kit (RTK601ING, SCP2019-021), HBV ELITe MGB Kit (RTK602ING, SCP2019-022), HCV ELITe MGB Kit (RTK601ING, SCP2019-020), dell''ordine di un mese.
Per il progetto CMV RNA ELITe MGB Kit (RTS115ING, SCP2019-019), essendo che la fase di sviluppo deve ancora iniziare, il cambio del prodotto di estrazione non avrà impatti.',N'Se il cambiamento non fosse implementato sarebbe necessario attendere la disponibilità del nuovo kit di estrazione da parte di PSS (possibile data di rilascio a metà 2021) e sospendere i tre progetti per circa 6 mesi.',N'964-RTK601ING - HCV ELITe MGB Kit
964-RTK600ING - HIV1 ELITe MGB Kit
964-RTK602ING - HBV ELITe MGB Kit',N'QARA
QC
RDM
MS
RAS
PVM
VP_R&D',N'Controllo qualità
Marketing
Program Managment
Regolatorio
Ricerca e sviluppo
Validazioni',N'Nessun impatto negativo a cambiamento avvenuto. Utilizzo di procedure e processi consolidati.
Nessun impatto negativo a cambiamento avvenuto. Utilizzo di procedure e processi consolidati.
Nessun impatto a cambiamento avvenuto.
Nessun impatto a cambiamento avvenuto.
Impatto positivo, in quanto il volume di campione da analizzare viene ridotto, pertanto una maggiore aliquota risulta essere disponibile per eventuali re-test.
Impatto positivo, in quanto il volume di campione da analizzare viene ridotto, pertanto una maggiore aliquota risulta essere disponibile per eventuali re-test.
Impatto positivo, in quanto il volume di campione da analizzare viene ridotto, pertanto una maggiore aliquota risulta essere disponibile per eventuali re-test.
Impatto positivo, in quanto il volume di campione da analizzare viene ridotto, pertanto una maggiore aliquota risulta essere disponibile per eventuali re-test.
Impatto positivo, in quanto il volume di campione da analizzare viene ridotto, pertanto una maggiore a',N'22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
25/01/2021',N'Aggiornare moduli di CQ per i tre progetti.
Ripetere tutti CQ su ELITe InGenius finora eseguiti per i prodotti "Viral Load".
Aggiornare GANTT.
Monitorare aggiornamento documentazione di progetto.
Aggiornamento documentazione di progetto:
- Report di sviluppo di HIV,
- Piano generale di V&V dei tre progetti"Viral Load",
- schede appunti di verifica dei tre progetti "Viral Load",
- documenti di CQ dei tre prodotti "Viral Load",
- DMRI (cambio kit di estrazione per QC) dei tre prodotti "Viral Load",
- Manuali di Istruzioni per l''Uso dei tre prodotti "Viral Load".

Compilare e verificare le nuove revisioni degli Assay Protocols per i prodotti "Viral Load".
Ripetere tutti i test per i controlli di stabilità su ELITe InGenius finora eseguiti per i prodotti "Viral Load".
Ripetere tutti i test di verifica su ELITe InGenius finora eseguiti per il prodotto HBV ELITe MGB Kit.
Aggiornare protocollo diagnostico e schede appunti per il centro di validazione per i prodotti "Viral Load".
Verificare l',N'vira load SP200 - viral_load_1000-50_vs_200-50_21122020.pptx
HIV ELITe MGB Kit - Report di sviluppo REP2020-173, - Piano generale di V&V PLAN2020-012 - Piano di verifica degli Assay Protocols PLAN2020-038, - Protocollo di validazione PRT2020-005, - MOD 10,13 e MOD 10,12 di CQ   HBV - MOD 10,13 e MOD 10,12 di CQ - Protocollo di validazione PRT2020-004. - cc20-84_report.txt
Meeting Team sviluppo prodotto HIV/HCV - r_aggiornamento_hiv1_e_hcv_elite_mgb_kit_cc20-84_22012021.msg
Possibile impatto negativo: le performance dei prodotti in termini di LoD tenderebbero a peggiorare superando i limiti richiesti all''interno dei Market Requirement Document. A seguito dei test preliminari in particolare 1) per HBV il limite delle 10IU/ml verrebbe rispettato, 2) per HIV raggiungerebbe le 150IU/ml a fronte delle 70IU/ml richieste dal mercato, 3) per HCV raggiungerebbe le 160IU/ml a fronte delle 15IU/ml richieste dal mercato. Per i prodotti HCV e HIV questo tipo di performance NON consentirebbe la partecipazione alle gare.   Impatto positivo. Il passaggio al kit di estrazione SP200 consentirebbe l''uso del tubo primario su ELITe InGenius, cosa attualmente NON possibile con l''uso di SP1000.  Dai risultati dei documenti prodotti finora (HIV ELITe MGB Kit: Report di sviluppo REP2020-173,  Piano generale di V&V PLAN2020-012, Piano di verifica degli Assay Protocols PLAN2020-038, Protocollo di validazione PRT2020-005, MOD 10,13 e MOD 10,12 di CQ; HBV: MOD 10,13 e MOD 10,12 di CQ, Protocollo di validazione PRT2020-004) si valuta di NON APPROVARE il change control per HIV e HCV e di bloccare tutte le altre attività indicate. Il CC20-84 prosegue solo per HBV. - cc20-84_non_approvato_per_hiv_e_hcv.txt',N'PVS
PVM
QC
QCT/DS
QC
QCT/DS
PMS
RDM
RT
RS
PMS
RDM
RT
RS
RDM
RT
RS
PVS
PVM
PVS
PVM
QARA
RAS
MS',N'22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
22/12/2020
25/01/2021',NULL,N'25/01/2021',N'No',N'nessun impatto',N'No',N'NA, prodotto non ancora in commercio',N'No',N'NA, prodotto non ancora in commercio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.2 - Fase di Pianificazione: Definizione requisiti',N'nessun impatto su RA R&D1.2',N'No',N'NA, prodotto non ancora in commercio',N'No',N'nessun virtual manufacturer',N'No',N'aggiornare la documentazione prodotta come bozza e consegnata al primo Riesame in modo da integrare e valutare il cambiamento apportato',N'Samuela Margio',NULL,NULL,'2021-01-25 00:00:00',N'No',NULL,'2021-01-25 00:00:00'),
    (N'20-83','2020-12-22 00:00:00',N'Federica Farinazzo',N'Processo di CQ e criteri di CQ',N'La richiesta è di carattere temporaneo. A causa della pandemia, al fine di aiutare gli ospedali nel lavoro, è stato convertito all''utilizzo presso i centri ospedalieri lo strumento ELITe Galaxy installato presso EGSpA (codice interno EA3). L''installazione di un nuovo strumento è prevista ad aprile 2021, pertanto fino a questa data non sarà possibile eseguire internamente i CQ dei nuovi lotti del prodotto INT021EX, che dovranno pertanto essere eseguiti dai QCT presso un laboratorio esterno. ',N'Possibilità di eseguire il test funzionale',N'Il rilascio del lotto sarebbe fatto solo sulla base dei CoA del fornitore la cui produzione è RUO',N'902-INT021EX - ELITe GALAXY 300 Extr kit-1x96',N'QARA
QC
RDM
GASC',N'Assistenza applicativa
Controllo qualità
Manutenzione e taratura strumenti interni
Regolatorio
Sistema qualità',N'La soluzione valutata di eseguire il controllo funzionale presso un centro esterno garantisce la continuità di distribuzione del prodotto.
La modifica è di tipo temporanea e non richiede la modifica di procedure. Le modifiche applicate saranno registrate in un documento allegato alle registrazioni di CQ
La modifica è dovuta in quanto la fornitura del prodotto è RUO e il nostro CQ è richiesto per la marcatura CE.
Attualmente eseguiamo il CQ funzionale su INT021EX in quanto la fornitura di McNagel è di un prodotto RUO che abbiamo validato internamente come CE-IVD. L''impossibilità di eseguire il CQ funzionale non permetterebbe la vendita al cliente del prodotto. La soluzione valutata di eseguire il controllo funzionale presso un centro esterno garantisce la continuità di distribuzione del prodotto, ma introduce dei rischi, giudicati comunque accettabili, associati alle nuove variabili introdotte sull''esecuzione del CQ, ossia ambiente e strumento (il materiale per l''esecuzione del CQ ed il personale',N'22/12/2020
22/12/2020
22/12/2020
22/12/2020',N'Verificare la disponibilità di un centro per l''esecuzione dei 3/4 TEST di CQ 
Lotto già in sede
Lotto 2: MN date of manufacturing will be January 05th 2021
Lotto3: MN date of manufacturing will be at week 5 of  2021  
Tracciare le modifiche al processo in un documento dedicato
Preparare una lista di materiali da portare presso il centro
nessuna azione
valutazione della significatività della modifica al fine di effettuare notifica presso le autorità competenti
Aggiornare la documentazione dello strumento EA3 spostato presso i laboratori del cliente e gestire l''installazione e la documentazione del nuovo estrattore',N'report 1 - cc20-83_report_cq_int021ex_2011-001+lista-materiali_2020-12-21.pdf
la modifica non è significativa, pertanto non è da notificare. - cc20-83_mod05,36__non_significativa.pdf',N'GASC
QC
QCT/DS
QARA
QM
QAS
QARA
QAS
T&GSS',N'22/12/2020
22/12/2020
22/12/2020
22/12/2020',N'30/04/2021',N'10/03/2021
10/03/2021
22/12/2020',N'No',N'nessun impatto sul DMRI del prodotto',N'No',N'non necessaria, il prodotto è testato al controllo qualità',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'vedere MOD05,36. La modifica del luogo di esecuzione del CQ e dello strumento utilizzato è temporanea, per sopperire ad esigenze sorte durante la pandemia. si ritiene pertanto non necessario effettuare alcun tipo di notifica alle autorità competenti dei paesi presso cui il prodotto è registrato.',N'No',N'nessun virtual manufaturer',N'No',N'na',NULL,'2021-04-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-82','2020-12-15 00:00:00',N'Roberta  Paviolo',N'Fornitore',N'A causa della pandemia COVID-19 e dell''elevato quantitativo di kit "SARS-CoV-2 Elite MGB KIT" prodotti (codici RTS170ING e RTS170ING-96), il fornitore Thermo-Fischer Scientific non è in grado di evadere tutti gli ordini relativi al componente "RT EnzymeMix" (codice interno 950-219) , per mancanza del contenitore primario (tubo Sarstedt). Thermo-Fischer Scientific ha individuato un secondo fornitore di tubi (Nalgene) con caratteristiche equivalenti all''attuale tubo Sarstedt. La modifica deve essere verificata e   approvata.',N'Continuità di produzione.',N'Impossibilità di fornire il kit ai clienti.',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'MM
PP
PW
QARA
QM
RDM',N'Acquisti
Assistenza applicativa
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo',N'Gestione accordi con Thermo-Fischer Scientific per l''uso dei nuovi tubi Nalgene.
Gestione accordi con Thermo-Fischer Scientific per l''uso dei nuovi tubi Nalgene.
Nessun impatto, i moduli di produzione non variano.
I nuovi tubi Nalgene differiscono dai Sarstedt per alcune caratteristiche fisiche: tappo senza laccetto con inserto giallo/arancio, altezza superiore (ma comunque non influente per il posizionamento nella NUNC), plastica opaca. Queste caratteristiche non influiscono sul confezionamento del prodotto. 
l contenuto del tubo (RT EnzymeMix) è il medesimo in tutti i casi.
Possibile necessità di notifica di questo cambiamento di packaging (modifica del contenitore primario del reagente "RT EnzymeMix") anche se è solo provvisoria ed c''è equivalenza tra i due tubi.
Possibile necessità di notifica di questo cambiamento di packaging (modifica del contenitore primario del reagente "RT EnzymeMix") anche se è solo provvisoria ed c''è equivalenza tra i due tubi.
Verifica dell''equivalenza tra l''a',N'15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020',N'Gestione accordi con Thermo-Fischer Scientific per l''uso dei nuovi tubi Nalgene.
nessuna attività a carico
Il personale di Produzione sarà formato per il confezionamento del prodotto SARS-CoV-2 ELITe MGB Kit con il componente RT EnzymeMix con:
o	tappo con inserto nero (quello attuale),
o	tappo con inserto giallo-dorato.
Segnalare a SGQ e FPS la data di inizio e di fine dell''utilizzo di tappi con inserto giallo dorato.
Introduzione di un avvertenza nelle IFU del prodotto RTS170ING e RTS170ING-96 che segnala la presenza di tubi con tappi di colore differente.
Verificare con i distributori se la modifica transitoria del contenitore primario del "RT EnzymeMix", le cui caratteristiche sono equivalenti rispetto al precedente tubo, è da notificare presso le autorità competenti.
Controllo delle caratteristiche dei tubi attraverso i certificati di analisi.
I nuovi tubi Nalgene differiscono da quelli Sarstedt per alcune caratteristiche fisiche: tappo senza laccetto con inserto giallo/arancio, altezza ',N'I nuovi tubi Nalgene differiscono dai Sarsted per le caratteristiche fisiche: tappo senza laccetto con inserto giallo/arancio, altezza superiore (ma comunque non influente per il posizionamento nella NUNC), plastica opaca.  - cc20-82_caratteristichefisichedeltapponalgenrispettoall_attuale.msg
equivalenza attuali tubi sarsted con nuovi nalgene - cc20-82_tubonalgene_342800-0005_tappiassociati_342830-0117.docx
avvertenza del 14/12/2020 - cc20-82_avvertenza_rts170ing.pdf
Gestione accordi con ThermoFischerScientific - cc20-82_gestione_accordi_con_thermo-fischer_scientific.msg
Approvazione acquisto nuovi tubi Nalgene, codice 342800-0005, con tappi associati, codice 342830-0117 - cc20-82_approvazioneegspa_nuovitubinalgeneetappiassociati.msg
richiesta ai distributori se la modifica transitoria del contenitore primario del "RT EnzymeMix", le cui caratteristiche sono equivalenti rispetto al precedente tubo, è da notificare presso le autorità competenti. - cc20-82_informazionixnotifica.msg
si ritiene non necessario effettuare notifica in quanto il contenitore primario adottato da thermofisher che ci fornisce il prodotto RT-Enzyme è equivalente al precedente, ma è provvisorio. Le caratteristiche del reagente non variano. - cc20-82__mod05,36_significativa.pdf
mail informativa al personale sul campo - cc20-82_informazione_fps.msg',N'QC
MM
RDM
RAS
PW
QARA
QAS
GASC',N'15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020',NULL,N'18/11/2020
15/12/2020
14/12/2020
15/12/2020
18/12/2020
22/12/2020',N'No',N'nessun impatto in quanto il codice dell''rt-enzyme non varia',N'Si',N'Fatta attraverso l''avvertenza allegata all''IFU',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'NESSUN IMPATTO SUL RA A2',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-81','2020-12-15 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ
Processo di produzione
Processo di CQ e criteri di CQ',N'In base all''alto volume di utilizzo del CTR170ING e STD170ING per il CQ del prodotto RTS170ING, si chiede di prevedere l''invio al CQ dei due prodotti a livello della preparazione del lotto come materiale uso CQ, anziché richiedere il materiale a magazzino come viene fatto attualmente. ',N'La modifica migliora le modalità di approviggionamento e di verifica delle scorte del campione',N'Nel caso peggiore potrebbe non essere disponibile un kit a magazzino e il CQ potrebbe subire dei ritardi.',N'956-CTR170ING - CoV-2 Positive Control',N'MM
QARA
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Impatto positivo in quanto il materiale richiesto sarà maggiore e in linea con le necessità di produzione
Durante la pianificazione del numero di lotti è necessario considerare il numero maggiore da fornire al CQ
Nessun impatto solo in termini di attività
Nessun impatto solo in termini di attività',N'15/12/2020
16/12/2020
16/12/2020
16/12/2020',N'Modificare il MOD09,23 
MOD956-CTR170ING
formazione
nessuna attività
messa in uso moduli
valutare la significatività della modifica per eventuale notifica autorità competente',N'MOD956-CTR170ING rev01, MOD18,02 del 16/12/2020 - cc20-81_messa_in_uso_mod956-ctr170ing_cc20-81_mod1802_del_16122020.msg
MOD956-CTR170ING rev01, MOD18,02 del 16/12/202 - cc20-81_messa_in_uso_mod956-ctr170ing_cc20-81_mod1802_del_16122020_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-81_mod05,36_non_significativa.pdf',N'QC
QCT/DS
MM
QARA
QM
QAS
QARA
QAS',N'15/12/2020
16/12/2020
16/12/2020
16/12/2020',N'20/12/2020',N'21/12/2020
21/12/2020
21/12/2020
21/12/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ4 - Pianificazione CQ',N'valutare aggiornamento RA CQ4',N'No',N'vedere MOD05,36. la modifica non è significativa, il processo di cq non viene modificato, viene anticipato l''invio dei lotti al cq (intesi come materiale per uso cq) per evitare la mancata disponibilità del kit a magazzino.',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Federica Farinazzo','2020-12-20 00:00:00',NULL,'2020-12-21 00:00:00',N'No',NULL,NULL),
    (N'20-80','2020-12-15 00:00:00',N'Sonia Mercurio',N'Fornitore',N'A causa della pandemia covid si sono verificati problemi di approvvigionamento di alcuni codici articolo durante i mesi di settembre/ottobre/novembre, in particolare sono coinvolti i  seguenti codici prodotto: 
- puntali per il trasferimento dei volumi: 953-079 Puntali_ART_10_µL, 953-080 Puntali_ART_100_µL, 953-081 Puntali_ART_1000_µL e 953-082 Puntali_ART_300_µL forniti da VWR, 
- piastre per i saggi di amplificazione degli acidi nucleici con la metodica dell''amplificazione real time  e per i foglietti: 953-198 Micropiastra amplificazione FAST fornita da THERMOFISHER che costituisce l''accessorio  RTSACC02 "Q - PCR Microplates Fast"; 953-205 Micropiastra amplificazione fornita da Life Technologies che costituisce l''accessorio RTSACC01 "Q - PCR Microplates. 
Con questo change control si chiede di individuare dei fornitori alternativi con materiali equivalenti al fine di utilizzarli all''interno dei processi EGSpA o presso i clienti. In particolare i puntali sono utilizzati esclusivamente internamente, mentre le piastre di PCR e foglietti (utilizzate con gli strumenti 7300 RT PCR System, 7500Fast dx e CFX96) sono utilizzate sia internamente che presso i clienti. ',N'La ricerca di un secondo fornitore evita il blocco della produzione, controllo qualità e dei test svolti da R&D.',N'Blocco della produzione, controllo qualità e dei test svolti da R&D.',N'953-079 - Puntali con Filtro ART 10 0.1-
953-080 - Puntali con Filtro ART 100 E 1
953-081 - Puntali con filtro ART 1000 E
953-082 - Puntali con Filtro ART 300 1-3
953-198 - Fast Optical 96w plates 7500DX
953-205 - MicroAmp Optic 96w React Plate',N'MM
PW
QC
QM
RDM',N'Acquisti
Controllo qualità
Magazzino
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'I materiali plastici oggetto del cambiamento sono utilizzati durante i processi produttivi di controllo qualità e di ricerca. E'' necessario dimostrare l''equivalenza dei nuovi materiali plastici selezionati con quelli correntemente in uso. In generale, l''introduzione di secondi fornitori di materiale plastico (equivalente a quello in uso) ha un impatto positivo in quanto garantisce l''approvvigionamento anche in situazioni critiche, come la pandemia in corso.
I materiali plastici oggetto del cambiamento sono utilizzati durante i processi produttivi di controllo qualità e di ricerca. E'' necessario dimostrare l''equivalenza dei nuovi materiali plastici selezionati con quelli correntemente in uso. In generale, l''introduzione di secondi fornitori di materiale plastico (equivalente a quello in uso) ha un impatto positivo in quanto garantisce l''approvvigionamento anche in situazioni critiche, come la pandemia in corso.
I materiali plastici oggetto del cambiamento sono utilizzati durante i processi produttivi di ',N'15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020',N'Testare i materiali approvvigionati da nuovi fornitori per dimostrarne l''equivalenza rispetto a quelli in uso. Possibilità di aggiornare le specifiche con i nuovi fornitori, in particolare la SPEC903-205 e 053-oo4 relative a piastre e foglietti per 7300 .
Valutare l''aggiornamento delle specifiche
informare il personale di magazzino su come effettuare il controllo al ricevimento del nuovo materiale approvvigionato non presente nelle specifiche.
Ricerca fornitori ed eventuale approvazione specifiche
Eventuale messa in uso specifiche
Per i materiali rilasciati ai clienti verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Testare i prodotti dei nuovi fornitori in modo da poterli eventualmente inserire in specifica e utilizzarli come "second source".
La notifica alle autorità competenti non si ritiene necessaria.
informazione del personale di produzione
informare il personale di magazzino su come effet',N'Le piastre e foglietti per 7500 approvvigionati da Steroglass risultano NC, si cerca un ulteriore fornitore. Le piastre e foglietti per 7300 approvvigionati da Steroglass risultano C al controllo qualità, si ritiene pertanto possibile utilizzarle sia per uso interno che da fornire ai clienti. - cc20-80-testcq_piastre7500-7300steroglass_pipettemettlertoledo.msg
I puntali mettler toledo risultano conformi, tranne che con la P100. Si ritiene pertanto possibile utilizzarli nei processi interni. - cc20-80_richiesta_elenco_piastre_testate_per_secondo_fornitore.msg
Le piastre per CFX96 sono state testate da diversi fornitori ed è stato associato un grado di compatibilità differente per l''acquisto. - cc20-80_richiesta_elenco_piastre_testate_per_secondo_fornitore_1.msg
test puntali sarstedt da10 ul CONFORME - io11,03_puntali_sarstedt_10_ul.pdf
test puntali sarstedt da100 ul CONFORME - io11,03_puntali_sarstedt_100_ul.pdf
test puntali sarstedt da1000 ul CONFORME - io11,03_puntali_sarstedt_1000_ul.pdf
test puntali sarstedt da 200 ul CONFORME - io11,03_puntali_sarstedt_200_ul.pdf
Le piastre e foglietti per 7500 approvvigionati da Steroglass risultano NC al controllo qualità, si cerca un ulteriore fornitore. Le piastre e foglietti per 7300 approvvigionati da Steroglass risultano C al controllo qualità. - cc20-80-testcq_piastre7500-7300steroglass_pipettemettlertoledo_1.msg
Eseguiti test sui puntali mettlere toldeo. solo i codici con esito conforme entreranno nei processi produttivi. - cc20-80-testcq_piastre7500-7300steroglass_pipettemettlertoledo_2.msg
Le piastre per real time sono utilizzate sia internamente che fornite ai clienti, essendo una modifica provvisoria e dimostrata l''equivalenza si ritiene non è necessario effettuare notifica all''autorità competente. - cc20-80_mod05,36_01_significativa.pdf',N'RT
PW
RS
QAS
QARA
QAS
QC
QARA
QAS
MM
T&GSC
RS',N'15/12/2020
15/12/2020
15/12/2020
15/12/2020
15/12/2020',NULL,N'14/04/2021
15/12/2020
14/04/2021
14/04/2021',N'No',N'nessun impatto',N'No',N'la comunicazione al cliente non è necessaria in quanto solo i materiali plastici equivalenti quelli in uso possono entrare nei processi interni o essere forniti ai clienti',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto sul RA A2 ',N'No',N'vedere MOD05,36. La notifica alle autorità competenti non si ritiene necessaria.',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Anna Capizzi',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-79','2020-12-10 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ
Processo di produzione',N'Si chiede l''introduzione di un modello generico da cui partire per la stesura delle schede di produzione dei prodotti Elite Ingenius che permetta di uniformare gli step di produzione e i tempi di esposizione a temperatura ambiente, con relativi cicli di dispensazione e confezionamento, per i prodotti con analogo processo.  ',N'Attualmente risulta difficile conservare le stesse informazioni per i vari prodotti durante il Design Transfer e riportare le eventuali modifiche su tutti i documenti codice-prodotto interessati, a causa del numero elevato di prodotti. L''introduzione di un modello unico che contenga le informazioni che si mantengono identiche per i vari prodotti permetterebbe di tenere sotto controllo questi aspetti, uniformandoli. Inoltre, permetterebbe di semplificare le attività legate alla modifica dei documenti in quanto le correzioni/modifiche verrebbero fatte solo sul modello unico e non su tutti i moduli codice specifici, semplificando anche il lavoro di DS.',N'L''attuale modalità di stesura dei documenti presenta delle lacune in termini di uniformità, questo può essere critico in particolare se queste informazioni sono riferite ai tempi di esposizione a temperatura ambiente e dei vari step interprocesso.',N'962-RTS200ING - CRE ELITe MGB Kit
962-RTS120ING - MDR TB ELITe MGB Kit
962-RTS201ING - ESBL ELITe MGB Kit
962-RTSD00ING - Coagulation - ELITe MGB KIT
962-RTS202ING - COLISTIN ELITe MGB Kit
962-RTS300ING - Meningitis Bact ELITe MGB Kit
962-RTS400ING - STI PLUS ELITe MGB Kit
962-RTS140ING - BORDETELLA ELITe MGB Kit
962-RTS150ING - Pneumocystis ELITe MGB Kit
962-RTS401ING - Macrolide-R/MG ELITe MGB Kit
962-RTS160ING - Resp Vir PLUS ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo sulla gestione dei documenti, permette di avere uniformità di informazioni per categora di prodotti e velocizzazione nella revisione dei documenti
Impatto positivo: un modello unico migliora la standardizzazione delle lavorazioni di produzione e dei documenti. In particolare, un modello unico permette di avere uniformità di informazioni per categoria di prodotti (per esempio per quanto riguarda i tempi di esposizione a temperatura ambiente).
Impatto positivo: un modello unico migliora la standardizzazione delle lavorazioni di produzione e dei documenti. In particolare, un modello unico permette di avere uniformità di informazioni per categoria di prodotti (per esempio per quanto riguarda i tempi di esposizione a temperatura ambiente).
Impatto positivo: un modello unico migliora la standardizzazione dei documenti e delle lavorazioni di produzione riducendo potenzialmente il numero di NC',N'10/12/2020
10/12/2020',N'Creazione del modello per 962 e per mix di oligo. Creazione della lista dei prodotti relative informazioni. Creazione dei pdf dei vari documenti. Formazione
revisione e approvazione dei documenti
Verifica del modello
messa in uso dei documenti',N'MOD962-RTS401ING-48 MOD962-RTS150ING MOD962-RTS140ING MOD962-RTS200ING MOD962-RTS202ING-48 MOD962-RTS120ING - cc20-79_messa_in_uso_mod962-rtsxxxing.msg
mod962-rts400ing_02 - cc21-12_mod962-rts400ing_02_2.msg
MOD955-40X-XXX MOD955-20X-XXX ALTRI IC - cc20-79_moduli_prod_1.msg
MODELLI MODULI PRODUZIONE SARS - cc20-79_messa_in_uso_modli_prod_sars.msg
mod955-40x-xxx_e_mod955-rts078pld-xxx - cc20-79_messa_in_uso_mod955-40x-xxx_e_mod955-rts078pld-xxx.msg
MOD955-MRSA E C-DIFFICILE-30X-XXX - cc20-79_messa_in_uso_mod955-xxx-30x_xxx.msg
MOD955-XX BUFFER - cc20-79_messa_in_uso_mod955-xx_buffer.msg
MOD962-RTS401ING-48 MOD962-RTS150ING MOD962-RTS140ING MOD962-RTS200ING MOD962-RTS202ING-48 MOD962-RTS120ING - cc20-79_messa_in_uso_mod962-rtsxxxing_1.msg
MOD962-RTS401ING-48 MOD962-RTS150ING MOD962-RTS140ING MOD962-RTS200ING MOD962-RTS202ING-48 MOD962-RTS120ING - cc20-79_messa_in_uso_mod962-rtsxxxing_2.msg
mod962-rts400ing_02 - cc21-12_mod962-rts400ing_02_3.msg
MOD955-40X-XXX MOD955-20X-XXX ALTRI IC - cc20-79_moduli_prod.msg
MODELLI MODULI PRODUZIONE SARS - cc20-79_messa_in_uso_modli_prod_sars_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-79_mod05,36_01_non_significativa.xlsx
mod955-40x-xxx_e_mod955-rts078pld-xxx - cc20-79_messa_in_uso_mod955-40x-xxx_e_mod955-rts078pld-xxx_1.msg',N'QC
QCT/DS
MM
PT
RDM
RT
QAS
RAS',N'10/12/2020
10/12/2020',NULL,N'18/01/2021
12/03/2021',N'No',N'non necessario',N'No',N'non necessaria in quanto trattasi di un miglioramento della documentazione di produzione',N'No',N'nessun impatto in  in quanto trattasi di un miglioramento della documentazione di produzione',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',N'Federica Farinazzo',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-78','2020-12-10 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ
Processo di CQ e criteri di CQ',N'Si chiede la modifica della procedura di CQ (PR10,02) e di non conformità (PR13,01) al fine di prevedere la possibilità di accettazione in concessione da parte di QC nei casi di preCQ e lotti pilota non destinati alla vendita. Tali accettazioni saranno fatte in accordo con QARA, QM e/o RDM in base al tipo di non conformità rilevata; il trattamento sarà tracciato sui documenti di produzione, su linkomm e/o sui documenti di Design Transfer.',N'Semplificazione del processo di gestione delle NC e delle seguenti attività di produzione quando tali non conformità non riguardano lotti destinati alla vendita e che come tali non impattano sul requisito 8.3.2 c) della ISO13485:2016 e la cui gestione può essere trattata in modo più semplificato. ',N'La procedura attuale prevede che l''accettazione in deroga sia registrata su un report approvato da un gruppo di persone che sia valutata la compliance rispetto ai requisiti regolatori prima del rilascio del lotto per la vendita, per tale motivo i i tempi di stesura dei report sono piuttosto lunghi e non permettono di agire in modo veloce data la natura della causa che ha generato tali NC che possono dipendere da OOS di CQ o dal settaggio dei requisiti di CQ in fase di loro definizione durante lo sviluppo.',NULL,N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Maggiore efficienza di gestione di non conformità rilevate al CQ che non impattano sulla produzione e che possono essere causate da OOS di CQ o possono essere corrette nell''ambito del DT.
Maggiore efficienza di gestione di non conformità rilevate al CQ che non impattano sulla produzione e che possono essere causate da OOS di CQ o possono essere corrette nell''ambito del DT.
Maggiore efficienza di gestione di non conformità rilevate al CQ che non impattano sulla produzione e che possono essere causate da OOS di CQ o possono essere corrette nell''ambito del DT.
Maggiore efficienza di gestione di non conformità rilevate al CQ che non riguardano lotti destinati alla vendita e che come tali non impattano sul requisito 8.3.2 c) della ISO13485:2016 e che possono essere causate da OOS di CQ o possono essere corrette nell''ambito del DT nel caso dei lotti pilota di un progetto in fase di sviluppo.
Maggiore efficienza di gestione di non conformità rilevate al CQ che non riguardano lotti destinati alla vendita',N'10/12/2020
10/12/2020
10/12/2020
10/12/2020
10/12/2020',N'Aggiornamento della PR10,02 e della IO10,14
formazione
approvazione delle procedure
approvazione delle procedure
aggiornamento PR13,01 GestioneDelleNonConformità
formazione
messa in uso documenti
valutare significatività modifica',N'PR10,02_08 - cc20-78_messa_in_uso_pr1002_08.msg
PR10,02_08 - cc20-78_messa_in_uso_pr1002_08_1.msg
PR10,02_08 - cc20-78_messa_in_uso_pr1002_08_2.msg
PR10,02_08 - cc20-78_messa_in_uso_pr1002_08_3.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-78_mod05,36_01_non_significativa.xlsx',N'QC
QCT/DS
MM
RDM
QM
QAS
QARA
QAS',N'10/12/2020
10/12/2020
10/12/2020
10/12/2020
10/12/2020
10/12/2020',N'30/01/2021',N'08/04/2021',N'No',N'nessun impatto',N'No',N'non necessaria, è una modifica interna',N'No',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ17 - Gestione OOS di CQ',N'da aggiornare RA CQ17',N'No',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Federica Farinazzo','2021-01-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-77','2020-11-20 00:00:00',N'Gabriella Campanale',N'Componenti principali
Materia prima',N'Second Source  per Enzima di retrotrascrizione (RT) e per la DNA polimerasi (Taq) per il prodotto SARS-CoV-2 ELITe MGB kit (RTS170ING)',N'Mantenere il prodotto sul mercato',N'ThermoFischer, attuale fornitore di RT (RevertAid H Minus Reverse Transcriptase) e Taq (PlatinumTaq Hot-Start DNA Polymerase) non è più in grado di fornire le quantità ordinate di queste materie prime',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'MM
PC
QARA
QC
RDM
RT',N'Acquisti
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini si attività.
Potrà essere necessario definire dei nuovi criteri di monitoraggio.
Potrà essere necessario definire dei nuovi criteri di monitoraggio.
Impatto in termini di attività: la retrotrascrittasi sarà da diluire e da aliquotare.
Impatto in termini di attività: la retrotrascrittasi sarà da diluire e da aliquotare.
La modifica delle materie prime non ha impatto sul prodotto se si trovano materie prime cono le stesse caratteristiche di quelle utilizzate in fase di progettazione, e se si dimostra l''equivalenza.
---
La modifica delle materie prime è significativa, pertanto da notificare.
La modifica delle materie prime è significativa, pertanto da notificare.',N'18/12/2020
18/12/2020
18/12/2020
14/12/2020
14/12/2020
14/12/2020
14/12/2020
14/12/2020',N'creare anagrafiche delle nuove materie prime individuate; redazione contratti per approvvigionamento delle nuove materie prime
Verifica dei moduli di produzione. 
Formazione del personale di produzione.
Studio dei nuovi criteri di monitoraggio
Modifica della Verifica Tecnica
Formazione
Utilizzo di nuova modulistica.
Definizione di una nuova procedura per la manipolazione a freddo, preparazione, miscelazione e dispensazione degli Enzimi di retrotrascrizione 
verifica del risk assessment ed eventuale aggiornamento del VMP
Ricerca ed identificazione delle nuove materie prime, esecuzione test e stesura di un report, stesura bozze dei moduli di produzione, preparazione delle specifiche relative alle nuove materie prime individuate
Notificare alle autorità competenti la modifica delle materie prime, laddove sono stati forniti dati a riguardo, previa autorizzazione di QARA essendo una modifica a livello di progettazione.
Aggiornare il FTP',N'create anagrafiche 950-231 GO Taq MDx HOT STAR Polymerase, 950-232 GOSCRIPT ENZYME MIX, 950-233 Recombinant RNAsin RNAse Inhibitor - cc20-77_messa_in_uso_spec950-23x_reagenti_scp2020-015_cc20-77_mod1802_del_13012021.msg
messa in uso moduli di produzione - cc20-77_messa_in_uso_mod_prod_promega_cc20-77_rap20-44_scp2020-015.msg
messa in uso moduli di cq - cc20-77_messa_in_uso_mod10-xx-rts170ing_cc20-77_nc21-8_rac21-2.msg
messa in uso moduli di produzione - cc20-77_messa_in_uso_mod_prod_promega_cc20-77_rap20-44_scp2020-015_1.msg
essesndo un''informazione riservata non si ritiene necessario notificarlo - cc20-77_mod05,36_01_significativa.pdf',N'PW
QC
QCT/DS
MM
RT
QARA
QAS
RAS
RAS
MM',N'18/12/2020
18/12/2020
14/12/2020
14/12/2020
14/12/2020
14/12/2020
14/12/2020',N'18/01/2021
18/01/2021
18/01/2021
18/01/2021
18/01/2021
29/01/2021',N'25/01/2021
25/01/2021
25/01/2021
24/02/2021
24/02/2021',N'Si',N'Il DMRI è da aggiornare per riportare le specifiche dei nuovi materiali e la nuova modulistica di produzione',N'No',N'non necessaria, il prodotto con le nuove materie prime è equivalente al precedente',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto sulla fase di pianificazione di progetto, R&D1.1, la ricerca di un secondo fornitore di materia prima è avvenuto in seguito  a mancata disponibilità del materiale da parte del fornitore',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'Si',N'la documentazione di gestione del rischio è da aggiornare per valutare l''impatto dell''introduzione dei nuovi enzimi sui pericoli identificati e sulla stima e valutazione dei rischi corrispondenti',N'Gabriella Campanale','2021-01-29 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-76','2020-11-20 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Rilascio della nuova versione del SW SGAT, V02.07.00, aggiornata per:
- migliorare il processo di gestione e apertura dei Work Order 
- migliorare la gestione  delle attività service ed applicative che vengono registrate sul sistema 
- migliorare la gestione della configurazione macchina
- migliorare la gestione ed il calcolo dei KPI (in particolare delle Work Unit WU)
- implementazione del nuovo modulo denominato Global KPI per estrazione e calcolo dei KPI a livello globale da tutti gli ambienti SGAT 
- migliorare il modulo "Magazzino" per la gestione dei movimenti e carico/scarico strumenti, attraverso l''implementazione della gestione dell''inventario .
Per il dettaglio degli aggiornamenti vedere il documento di VOLOS prodotto per il rilascio della versione test "SGAT ELITECH V02.07.00 RELEASE NOTES DEL 19/11/2020".',N'Miglioramento nell''utilizzabilità del sw. Miglioramento nella gestione dei KPI a livello globale',N'Mancato aggiornamento tecnologico, utilizzo di un sistema più complesso per l''estrazione dei KPI a livello globale ',NULL,N'CSC
GSC
QARA',N'Global Service
Regolatorio
Sistema informatico',N'L''impatto è positivo, in quanto permette l''aggiornamento tecnologio del sw.
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto, informazione sull''aggiornamento del sw.',N'20/11/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020',N'aggiornamento del risk assessment
installazione da parte del fornitore qualificato VOLOS della nuova versione in ambiente di test
verifica delle modifiche in ambiente di test
verifica del rilascio da parte di VOLOS della documentazione di validazione (test plan e report, video della validazione, relase notes delsw "live", user guide)
formazione agli utilizzatori
invio comunicazione a tutte le ICO dell''avvenuta implementazione in ambiente produzione 
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
approvazione del risk assessment aggiornato, piano e report di validazione
messa in uso del sw internamente in egspa
nessuna attività a carico',N'elenco delle modifiche richieste (con la distinzione della consociata che l''ha richiesta) - sgat_elitech_-_028_-_a_-_v2.07.00_-_release_notes.pdf
SGAT ELITECH - 029 - A - Test Plan; SGAT ELITECH - 030 - A - V02.07.00 Test Report - cc20-76_testverificavalidazione.txt
Release note  volos ottobre-2020 (versione 02.07.00) - sgat_elitech_-_028_-_a_-_v2.07.00_-_release_notes.pdf
invio comunicazione a tutte le ICO dell''avvenuta implementazione in ambiente produzione  - 180121_messa_in_uso_sgat_evo_versione_v2.07.00.pdf
formazione utilizzatori - cc20-76_formazione_nuova_versione_sgatevo.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-76_mod05,36_01_non_significativa.pdf
RA aggiornata - cc21-76_mod09,27_01_sw32_sgat_evo_2.7.00.pdf
messa in uso sgat evo 02.07.00 - cc20-76_messa_in_uso_sgat_evo_egspa_02.07.00.msg',N'GSC
QARA
QAS
QARA
QAS
ITM',N'04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020',N'18/01/2021
18/01/2021
18/01/2021
18/01/2021',N'22/02/2021
09/12/2020
15/01/2021
28/01/2021
04/12/2020',N'No',N'na',N'No',N'na, il sw non è utilizzato dai clienti',N'No',N'na, il sw non è utilizzato dai clienti',N'SW - Elenco Software',N'SW32 - sgat (Software globale per la gestione delle richieste di assistenza tecnica sugli strumenti )',N'si, da aggiornare RA SW32',N'No',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Marcello Pedrazzini','2021-01-18 00:00:00',NULL,'2021-02-22 00:00:00',N'No',NULL,NULL),
    (N'20-75','2020-11-19 00:00:00',N'Cinzia Bittoto',N'Documentazione SGQ',N'Modifica dell''allegato alla PR04,03 (ALL-1 "CONDIZIONI DI CONSERVAZIONE STABILITE PER I PRODOTTI") per cambiare il numero di cicli di congelamento/scongelamento dei prodotti della linea onco-ematologia (RTSG07PLD210 e RTSG07PLD190). In particolare, durante l''estensione d''uso del prodotto BCR-ABL P210 ELITe MGB Kit (RTSG07PLD210) in associazione a ELITe InGenius (codice progetto SCP2019-016) è emersa la necessità di aumentare il numero dei cicli di congelamenti/scongelamento dei componenti "PCR PreMix" e "PCR MasterMix" da 5 (attualmente dichiarati nell''IFU) a 6.',N'In associazione a ELITe InGenius, la possibilità di effettuare 6 cicli di scongelamento/congelamento permette di effettuare 6 sessioni di lavoro anziché 5, con minor spreco dei reagenti.',N'In associazione a ELITe InGenius, 5 cicli di scongelamento/congelamento limiterebbero il numero di sessioni con maggior spreco dei reagenti.',N'910-RTSG07PLD190 - P190 ELITe MGB Kit
910-RTSG07PLD210 - BCR-ABL P210 ELITe MGB® Kit',N'QARA
RDM
PVM',N'Ricerca e sviluppo
Sistema qualità
Validazioni',N'Nessun impatto se non in termini di attività da svolgere.
Nessun impatto se non in termini di attività da svolgere.
Nessun impatto se non in termini di attività da svolgere: l''integrazione  dell''aumento dei cicli di congelamento/scongelamento (da 5 a 6) nella revisione dell''ifu dedicata all''estensione d''uso permetterà la gestione di un''unica revisione dell''ifu e l''effettuazione di un''unica notifica alle autorità competenti in sede di chiusura dei progetti di estensione d''uso.
Nessun impatto se non in termini di attività da svolgere: l''integrazione  dell''aumento dei cicli di congelamento/scongelamento (da 5 a 6) nella revisione dell''ifu dedicata all''estensione d''uso permetterà la gestione di un''unica revisione dell''ifu e l''effettuazione di un''unica notifica alle autorità competenti in sede di chiusura dei progetti di estensione d''uso.
Nessun impatto se non in termini di attività da svolgere: l''integrazione  dell''aumento dei cicli di congelamento/scongelamento (da 5 a 6) nella revisione dell''if',N'19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020',N'Modificare ALL-1_PR04,03
revisionare IFU aggiornata
messa in uso dell''ALL-1 aggiornato
messo in uso dell''IFU aggiornata
La modifica è significativa, pertanto da notificare. la notifica avverrà insieme alla notifica di estensione d''uso del prodotto RTSG07PLD210 
La modifica è significativa, pertanto da notificare. la notifica avverrà insieme alla notifica di estensione d''uso del prodotto RTSG07PLD190
Revisionare, approvare e mettere in uso l''IFU aggiornata del prodotto RTSG07PLD210
Revisionare, approvare e mettere in uso l''IFU aggiornata del prodotto RTSG07PLD190
Aggiornare IFU del prodotto a seguito dell''estensione d''uso del prodotto su ELITe InGenius (SCP2019-016 - SCP2019-017)',NULL,N'RS
QAS
PVS
RAS
RAS
RAS
RAS',N'19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020
19/11/2020',N'30/11/2020
30/04/2021
30/04/2021',NULL,N'No',N'Il DMRI non richiede alcun aggiornamento',N'No',N'avverrà con l''avvertenza dell''IFU',N'No',N'nessun impatto',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL3 - Fase di Verifica, Validazione e Rilascio sul Mercato: attività',N'nessun impatto sul RA VAL3',N'Si',N'la notifica alle autorità competente verrà svolta insieme alla chiusura dell''estensione d''uso dei prodotti',N'No',N'nessun virtual manufacturer',N'No',N'La documentazione di gestione dei rischi non è da aggiornare in quanto le condizioni di congelamento e scongelamento del prodotto riportate sull''IFU a seguito dell''implementazione del CC non hanno richiesto l''esecuzione di test aggiuntivi ma sono riferite ai test di stabilità già effettuati e registrati nei report presenti nel Fascicolo Tecnico di Prodotto e considerati nella documentazione di gestione dei rischi corrente. Pertanto la modifica non determina alcuna variazione del rischio.',N'Cinzia Bittoto','2021-04-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-74','2020-11-18 00:00:00',N'Ferdinando Fiorini',N'Dismissione prodotto',N'Richiesta di dismissione del prodotto di Estrazione EXTD02 per calo drastico delle vendite. Il prodotto ha già un suo naturale sostituto che è l''ETXTB01',N'Semplificazione attività di Pianificazione, Acquisti, Produzione e Controllo Qualità',N'Difficoltà a Produrre e Controllare piccoli lotti, Sprechi consistenti.',N'910-EXTD02 - EXTRAcell 50 T',N'MM
OP
PC
QC
QM
MS',N'Acquisti
Controllo qualità
Gare
Magazzino
Marketing
Marketing Internazionale
Ordini
Produzione
Sistema qualità',N'Eliminazione dell''acquisto di diverse materie prime e plasticheria acquistate solo per questo prodotto
Eliminazione dell''acquisto di diverse materie prime e plasticheria acquistate solo per questo prodotto
Evitare produzione non economica di un prodotto in volumi molto bassi
Evitare produzione non economica di un prodotto in volumi molto bassi
Evitare un Controllo Qualità complesso
Impatto positivo: eliminazione di un prodotto non adeguato al nuovo IVDR. La dismissione e la conseguente eliminazione del prodotto a listino semplificherà la gestione documentale a favore dei prodotti tecnologicamente più avanzati. EXTD02 dovrà comunque essere cancellato dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria.
Nessun impatto.
Verificare se ci sono ancora clienti in essere cui fare comunicazione; il prodotto non è più a listino dal 2019.
Verificare se ancora presenti gare',N'19/01/2021
19/01/2021
01/12/2020
01/12/2020
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',N'aggiornamento dell''acquisto di materie prime e plasticheria
Aggiornamento dei documenti di pianificazione, in accordo con il reparto acquisiti e CQ.
fornire a QAS gli ultimi lotti prodotti e rispettiva data di scadenza e l''elenco di tutti i moduli  e le specifiche che è possibile archiviare.
nessun attività a carico.
attività come da MOD04,12
attività come da MOD04,12, gestito da QAS
inviare lettera di comunicazione dismissione ai clienti ancora in essere. ',N'MODULI CQ ARCHIVIATI - cc20-74__mod_cq_da_archiviare.xlsx
clienti in assere al 09/12/2020 - cc20-76_clientixletteradismissione.msg',N'PW
PP
PP
QCT/DS
QAS
SAD-OP
MS',N'19/01/2021
01/12/2020
19/01/2021
19/01/2021
19/01/2021
19/01/2021
19/01/2021',NULL,N'19/04/2021',N'No',N'nessun impatto',N'No',N'da effettuare solo ai clienti utilizzatori',N'No',N'nessun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'DC12',N'Si',N'da notificare',N'No',N'nessun virtual manufatiurer',N'No',N'na',N'Roberta  Paviolo',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-73','2020-11-11 00:00:00',N'Samuela Margio',N'Progetto',N'Modifica della concentrazione dello standard STD600ING del prodotto HIV ELITe MGB Kit (RTK600ING) a seguito dell''esito della VEQ HIVRNA20. Nonostante il buon esito della VEQ rispetto ai dati quantitativi espressi in Unità Internazionali/ml (score ottenuto = 0), l''esito della stessa VEQ in copie/ml ha fatto emergere una sottostima sistemica del nostro prodotto rispetto alla media dei competitors (Roche, Hologic, Abbott, Cepheid) pari a 0,344 Log. Tale sottostima è probabilmente dovuta alla differenza tra lo standard utilizzato nel nostro prodotto (plasmide a DNA a doppio filamento) rispetto a quello utilizzato da altre aziende (RNA a singolo filamento). Questo spiegherebbe l''entità della sottostima.',N'Il valore aggiunto è quello di avere un prodotto allineato sia in termini di copie/ml che UI/ml ai competitors predominanti sul mercato. ',N'Qualora in fase di transizione da un prodotto della concorrenza al nostro dovessero emergere tali discrepanze il cliente potrebbe manifestare maggiore resistenza al cambiamento. Questo va visto in un contesto in cui la maggior parte dei laboratori utilizza ancora le copie/ml per refertare piuttosto che le unità internazionali.',NULL,N'MM
QC
RDM
MS
RAS',N'Controllo qualità
Produzione
Ricerca e sviluppo',N'Utilizzo di nuovi moduli per il controllo qualità dei lotti Pilota e dei lotti di produzione
Utilizzo di nuovi moduli per il controllo qualità dei lotti Pilota e dei lotti di produzione
Utilizzo di nuovi moduli per il controllo qualità dei lotti Pilota e dei lotti di produzione
Utilizzo di moduli di preparazione dello Standard e del Controllo positivo diverso rispetto a quanto pianificato.
Nessun impatto a seguito del cambiamento
Nessun impatto a seguito del cambiamento',NULL,N'Modifica e successiva messa in uso di moduli di CQ aggiornati
Aggiornamento della formazione ai reparti di Prod e CQ
Formazione su utilizzo modulistica corretta per la produzione di STD e CTR
Proposta criteri di accettazione in CQ e aggiornamento documentazione di Design Transfer (PML, DT Plan, DMRI)',N'STD600ING REV00 - cc20-73_messa_in_uso_mod_prod_e_cq_rtk600ing_scp2019-021.msg
le date di modifica sono presenti nel fascicolo di progetto - cc20-73_vedere_documentazione_di_progetto.txt',N'QCT/DS
QCT/DS
PCT
RS',NULL,N'24/11/2020
24/11/2020
24/11/2020
24/11/2020',N'16/04/2021
16/04/2021
16/04/2021
11/03/2021',N'Si',N'aggiornamento del dmri',N'No',N'non applicabile, il prodotto non è ancora stato immesso in commercio',N'No',N'non applicabile, il prodotto non è ancora stato immesso in commercio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9',N'No',N'non applicabile, il prodotto non è ancora stato immesso in commercio',N'No',N'nessun virtual manufacturer',N'No',N'la variazione non richiede un aggiornamento della documentazione di gestione dei rischi. In ogni caso nel corso della fase di Verifica e Validazione, qualora fosse necessario, la gestione dei rischi ora disponibile in bozza sarà aggiornata e approvata',N'Samuela Margio','2020-11-24 00:00:00',NULL,'2021-04-16 00:00:00',N'No',NULL,NULL),
    (N'20-72','2020-11-09 00:00:00',N'Clelia  Ramello',N'Etichette',N'In accordo con ECN/ECR 138, PSS richiede di cambiare il codice ''F'' in basso a destra sotto il simbolo di Fabbricante per poter distinguere la produzione manuale "F2119-000" da quella automatica "F2119-001"  nelle etichette di spedizione esterna e nelle etichette di prodotto esterne di INT032CS (900-500M167A e 900-500M192A) e INT035PCR (900-500M165A e 900-500M194A).',N'Possibilità di distinguere se i prodotti usati dai clienti INT032CS e INT035PCR provengono dal sito produttivo con assemblaggio manuale o automatico.',N'In caso di notifiche dai clienti sui prodotti INT032CS e INT035PCR, solo il lotto potrebbe aiutare PSS a distinguere tra produzione autometica o manuale.',N'972-INT032CS - ELITeInGenius SP200 ConsSet 48
972-INT035PCR - ELITe InGenius PCR Casset 192',N'QARA
QM
SIS',N'Program Managment
Sistema qualità',N'Una volta avvenuto il cambio, non ci sono impatti sulle attività del Sistema Qualità
Una volta avvenuto il cambio, non ci sono impatti sulle attività del Sistema Qualità
Una volta avvenuto il cambio, non ci sono impatti sulle attività del Sistema Qualità
Una volta avvenuto il cambio, non ci sono impatti sulle attività del Program Management',N'25/11/2020
25/11/2020
25/11/2020
25/11/2020',N'Valutare se è necessario riapprovare le etichette a fronte del cambiamento proposto da PSS
Aggiornare il Fascicolo Tecnico per inserire la documentazione aggiornata prodotta da PSS relativa alla modifica
Raccogliere una copia delle nuove etichette e fornire al QM per archiviazione',N'FTP AGGIORNATO - cc20-72_aggiornamento_ftp_consumabili_ingenius.msg',N'QM
SIS
RAS',N'25/11/2020
25/11/2020
25/11/2020',NULL,N'14/05/2021
15/01/2021
14/05/2021',N'No',N'Aggiornare la documentazione di PSS che traccia il processo produttivo dei prodotti interessati',N'No',N'non necessaria',N'No',N'nessun impatto ',N'PVM3 - Virtual Manufacturer: produzione prodotto ',N'PVM 3.1 - Produzione prodotto presso Virtual Manufacturer',N'valutare il numero di eventi sul sotto-processo PVM3.1',N'No',N'vedere MOD05,36. la modifica non è significativa in quanto non riguarda il labelling del prodotto.',N'No',N'nessun virtual manufacturer',N'No',N'valutare',N'Clelia  Ramello',NULL,NULL,'2021-05-14 00:00:00',N'No',NULL,NULL),
    (N'20-71','2020-10-30 00:00:00',N'Clelia  Ramello',N'Progetto',N'Nuova versione del software Results Manager:
- miglioramenti nelle traduzioni di GUI e Manuale Utente in Italiano, 
- supportare lo strumento AriaDx,  
- possibilità di esportare il dato della versione del software nella lista dei dump caricati in formato "csv", 
- possibilità di definire per risultati quantitativi il numero di decimali e migliorare l''arrotondamento del risultato riportando 3 cifre dopo la virgola.         ',N'La possibilità di esportare l''informazione della versione software agevolerà l''attività degli FPS per il  monitoraggio delle versioni dai clienti. Le migliorie sulla lingua italiana miglioreranno l''usabilità del sw. L''aggiunta dell'' AriaDx tra gli strumenti supportati permetterà di supportare un maggior numero di clienti. Le migliorie sul calcolo di risultati quantitativi permetteranno di sviluppare dump file migliorati.',N'Senza la funzione della versione software la tracciabilità delle versioni dei clienti da parte degli FPS risulterebbe difficoltosa. Utilizzo del sw in lingua italiana poco comprensibile. Impossibile supportare l''analisi di risultati provenienti da AriaDx.',NULL,N'FPS
PVS
QM
RT
RAS
SIS
PVM',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema informatico
Sistema qualità
Validazioni',N'Una volta rilasciata la nuova versione software, il PMS non ha impatto in termini di attività
Una volta rilasciata la nuova versione software, R&D non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, Validazioni non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, IT non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, il Sistema Qualità ha impatto in termini di attività: messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente
Una volta rilasciata la nuova versione software, il Sistema Qualità ha impatto in termini di attività: messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente
Il sw Result Manager può variare in base alla piattaforma su cui è installato, è per tanto',N'04/11/2020
04/11/2020
20/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020',N'Supervisionare attività fornitore, supportare validazioni nella redazione del piano di validazione con MOD05,23, ed approvare il report di validazione con MOD05,24
Revisione del piano di validazione, validazione del sw e collaborazione alla stesura del report di validazione secondo modello MOD05,24 in collaborazione con Validazioni
Stesura del piano di validazione, validazione del sw e stesura del report di validazione secondo modello MOD05,24 in collaborazione con R&D
 Revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24)
Messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente
Revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24)

Comunicazione al cliente del cambiamento e formazione. L''eventuale comunicazione e formazione verrà registrata con l''apertura di un foglio di lavoro su SGAT. La versione del Sw dovrebbe anche essere registrata su SGAT.
',N'MOD05,23 PLAN, MOD05,24 REPORT E REP2020-157 FIRMATI IL 4/12/2020 - cc20-71_validazione.txt
MOD05,23 PLAN, MOD05,24 REPORT FIRMATI IL 4/12/2020 - cc20-71_validazione_1.txt
MOD05,23 PLAN, MOD05,24 REPORT FIRMATI IL 4/12/2020 - cc20-71_validazione_2.txt
MOD05,23 PLAN, MOD05,24 REPORT FIRMATI IL 4/12/2020 - cc20-71_validazione_3.txt
SW46 v20.10.30.335 in uso il 04/12/2020 - cc20-71_messa_in_uso_result_manager_v.20.10.30.335_mod1802_04122020.msg
release note - cc20-71_release_note__v_20.10.30.335.pdf
MOD05,23 PLAN, MOD05,24 REPORT FIRMATI IL 4/12/2020 - cc20-71_validazione_4.txt
Mail di formazione, con la pianificazione dell''unico centro da aggiornare in modo urgente (Montichiari), per gli altri centri l''aggiornamento può essere eseguito gradualmente. - cc20-71_mail_formazioneresults_manager_sw_version_20.10.30.335.msg
slide formazione, MOD18,02 del 4/12/2020 - cc20-71_formazione_results_manager_aggiornamento_v_20.10.30.335.pptx
release note - cc20-71_release_note__v_20.10.30.335_1.pdf',N'PMS
SIS
RDM
RT
RS
PVS
PVM
ITM
QAS
QM
FPS
GSC
QARA
GASC
CSC
CSC
RAS',N'04/11/2020
04/11/2020
20/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020
04/11/2020',NULL,N'04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020
04/12/2020',N'No',N'nessun DMRI',N'Si',N'comunicazione al cliente',N'No',N'nessun impatto. la nuova versione è migliorativa, anche la presenza della versione precedente è sufficiente per l''uso previsto del sw',N'SW - Elenco Software',N'SW46 - Result Manager',N'aggiornare il RA SW46',N'No',N'non applicabile, il prodotto è RUO.',N'No',N'nessun virtula manufacturer',N'No',N'Non essendo un prodotto IVD non è richiesta la produzione di una documentazione di gestione dei rischi',N'Clelia  Ramello',NULL,NULL,'2020-12-04 00:00:00',N'No',NULL,NULL),
    (N'20-70','2020-10-27 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'A causa della pandemia di SARS-CoV2 si ritiene necessario rendere elettronico il sotto-processo di approvazione all''uso delle etichette di prodotto, in deroga alla procedura.',N'Rendere il sotto-processo, da procedura corrente cartaceo, in forma elettronica al fine di consentire l''attuazione di questo anche in regime di smart working intensificato a seguito delle misure restrittive imposte dal DPCM del 24/10/2020 relativo alle Misure urgenti di contenimento del contagio sull''intero territorio nazionale ed evitare i contatti personali per la firma dei documenti.',N'Difficoltà a svolgere l''attività di approvazione delle etichette dato il regime di smart working intensificato che riduce la presenza in azienda e mantenimento dei contatti personali.',NULL,N'MM
QARA
QM',N'Produzione
Sistema qualità',N'Impatto positivo in termini di protezione aggiuntiva del personale, negativo in termini di tempo necessario per l''approvazione etichette.
Impatto positivo in termini di protezione aggiuntiva del personale, negativo in termini di tempo necessario per l''approvazione etichette.',N'27/10/2020
27/10/2020',N'FLUSSO APPROVAZIONE ETICHETTE:
PRODUZIONE
compilazione del MOD09,37 da parte del personale di produzione come fatto finora, scansione del MOD09,37 compilato, della lista materiali estratta da nav e del MOD962-xxx, ed invio via mail a sgq
 
SGQ
Verifica etichette per confronto con lista materiali e invia approvazione via mail
 
PRODUZIONE 
Stampa mail di approvazione SGQ e la pinza al modulo di confezionamento
FLUSSO APPROVAZIONE ETICHETTE:
PRODUZIONE
compilazione del MOD09,37 da parte del personale di produzione come fatto finora, scansione del MOD09,37 compilato, della lista materiali estratta da nav e del MOD962-xxx, ed invio via mail a sgq
 
SGQ
Verifica etichette per confronto con lista materiali, approvazione via mail (pianificare dei giorni fissi a persona per il controllo etichette per evitare ritardi)
Frase mail: 
Roberta Paviolo (delegata da QM) autorizza l''etichetta ad essere utilizzata
 
PRODUZIONE 
Stampa mail di approvazione SGQ e la pinza al modulo di confezionamento',N'esempio controllo elettronico etichette - cc20-70_esempio_controlloelettronicoetichette_962-rts020pld_lotto_u1020-013.msg
esempio controllo elettronico etichette - cc20-70_esempio_controlloelettronicoetichette_962-rts020pld_lotto_u1020-013_1.msg',N'MM
QM',N'27/10/2020
27/10/2020',NULL,N'09/11/2020
09/11/2020',N'No',N'nessun impatto sul DMRI, la modifica riguarda il sotto-processo',N'No',N'nessun impatto, trattasi di modifiche interne',N'No',N'nessun impatto, trattasi di modifiche interne',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.6 - Etichettatura semilavorati e componenti',N'l''approvazione elettronica delle etichette non aggiunge pericoli, il RA P1.6 non è pertanto da modificare',N'No',N'NA. ',N'No',N'NA',N'No',N'nessun impatto',NULL,NULL,NULL,'2020-11-09 00:00:00',N'No',NULL,NULL),
    (N'20-69','2020-10-26 00:00:00',N'Alessandra Gallizio',N'Stabilità',N'Estendere la validità della calibrazione da 30 a 60 giorni del prodotto SARS-CoV-2 ELITe MGB Kit (962-RTS170ING) ed il formato ridotto RTS170ING-96 sulla base dei dati ottenuti con i test di stabilità della curva standard (REP2020-138, rev.01)',N'uniformare la validità della calibrazione a quella degli altri prodotti quantitativi',N'la validità della calibrazione di questo prodotto sarebbe di 30 giorni invece che 60 giorni, determinando un aumento delle sessioni di calibrazione richieste  al cliente.',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'QARA
QM',N'Assistenza applicativa
Controllo qualità
Global Service
Ricerca e sviluppo
Sistema qualità',N'nessun impatto, se non in termini di attività
nessun impatto, se non in termini di attività
nessun impatto, se non in termini di attività
nessun impatto
necessità di informare i clienti e e sostituire CalibratorSet_CoV-2 Q-PCR Standard in uso con quello a 60 giorni
nessun impatto
L''estensione della durata della calibrazione di RTS170ING in associazione ad InGenius ha un impatto positivo in quanto il cliente potrà eseguire una calibrazione ogni 60 giorni, invece che ogni 30.
L''estensione della durata della calibrazione di RTS170ING in associazione ad InGenius ha un impatto positivo in quanto il cliente potrà eseguire una calibrazione ogni 60 giorni, invece che ogni 30.',N'26/10/2020
26/10/2020
26/10/2020
26/10/2020
26/10/2020
26/10/2020
26/10/2020',N'aggiornamento documentazione di prodotto (RTS170ING, RTS170ING-96, STD170ING: FTP e IFU)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornamento CalibratorSet_CoV-2 Q-PCR Standard. 
aggiornamento documentazione  (RTS170ING, RTS170ING-96, STD170ING)
Pianificazione dell''aggiornamento degli assay protocol presso i clienti, con tempistiche non restrittive in quanto il nuovo assay non influisce sulle performance del prodotto, ma solo sulle tempistiche di calibrazione.
creazione TAB',N'rep2020-138_del_19-11-2020 nel FTP - cc20-69_rep2020-138_del_19-11-2020.txt
ifu rev05 - cc20-69_ifu_sars-cov-2_extension_of_use_open_platforms_scp2020-009.msg
aggiornamento IFU schm_RTS170ING_05 e schm_STD170ING_01 con calibrazione a 60 giorni - cc20-69_ifu_sars-cov-2_extension_of_use_open_platforms_scp2020-009.msg
REP2020-138 firmato il 19-11-2020 - cc20-69_rep2020-138_del_19-11-2020.txt
la modifica non è significativa, pertanto non è da notificare. - cc20-69_mod05,36_01_non_significativa.pdf
Febbraio 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti.  Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16.  Il monitoraggio dell''installazione degli AP presso il cliente è a carico degli FPS. - cc20-69_monitoraggioap.txt',N'RAS
RS
QC
QCT/DS
QARA
QAS
GASC
GPS',N'26/10/2020
26/10/2020
26/10/2020
26/10/2020
26/10/2020',NULL,N'20/11/2020
11/02/2021
19/11/2020
25/01/2021
11/02/2021',N'No',N'nessun impatto sul DMRI',N'Si',N'notifica da effettuare con l''avvertenza presente con l''IFU, da inviare TAB',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'vedere MOD05,36, la modifica non si reputa significativa in quanto non influisce sulle performance del prodotto, ma solo sulle tempistiche di calibrazione.',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-68','2020-10-23 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede l''aggiornamento dei documenti di CQ del prodotto RTS170ING, aggiungendo gli identificativi dei componenti relativi al lotto unico da controllare. ',N'Questa modifica permette di agevolare la tracciabilità dei lotti dei componenti del prodotto rts170ing appartenenti al lotto in controllo rendendola fattibile sia a partire dal modulo di produzione che dal modulo di cq.',N'Tracciabilità fatta dai moduli di produzione del prodotto rts170ing con tempi più lunghi rispetto alla possibilità di effettuare una tracciabilità a partire dai documenti di CQ.',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'QARA
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Impatto positivo, agevolazione della tracciabilità dei lotti dei componenti in controllo  a partire dai documenti di CQ.
Impatto positivo, la tracciabilità dei lotti dei componenti del prodotto rts170ing può essere fatta sia dai moduli di produzione che dai moduli di CQ.
Agevolazione della tracciabilità degli esiti di cq rispetto al lotto in controllo.',N'04/11/2020',N'Modifica dei moduli di CQ MOD10,13-RTS170ING e MOD10,12-RTS170ING. Formazione.
Messa in uso dei documenti
Formazione sulla nuova modalità che riguarda allegare l''esito del CQ al modulo di produzione',N'messa_in_uso_moduli_cq_170ing_mod1802_del_29102020 - cc20-68__messa_in_uso_moduli_cq_170ing_mod1802_del_29102020.msg
messa_in_uso_moduli_cq_170ing_mod1802_del_29102020 - cc20-68__messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_1.msg',N'QC
QCT/DS
QARA
QM
QAS
MM',N'23/10/2020
04/11/2020',N'30/10/2020',N'29/10/2020
29/10/2020',N'No',N'nessun impatto',N'No',N'non necessaria, modifica della procedura interna di rintracciabilità',N'No',N'nessun impatto, modifica della procedura interna di rintracciabilità',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'altro',N'No',N'vedere MOD05,36. la modifica non è significativa in quanto tratta il miglioramento interno della tracciabilità dei documenti a partire dal moduli di CQ.',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,'2020-10-30 00:00:00',NULL,'2020-10-29 00:00:00',N'No',NULL,NULL),
    (N'20-67','2020-10-22 00:00:00',N'Benedetto Maschietto',N'Dismissione prodotto',N'Dismissione dei kit BANG e sostituzione con KIT di Terze parti ( ExperTeam): 
BANG07 PHILADELPHIA
BANG08 BCL2
BANG12 t(15;17)
BANG13 inv(16)
BANG14 t(12;21)
BANG15 t(8;21)
BANG16 t(4;11)
CTRG07 PHILADELPHIA
CTRG08 BCL2
CTRG12 t(15;17)
CTRG13 inv(16)
CTRG14 t(12;21)
CTRG15 t(8;21)
CTRG16 t(4;11)
BRK200
ER140
EXTR01 
EPH03 ',N'Nell''ottica di un continuo miglioramento dei prodotti e con l''intenzione di perseguire costantemente soluzioni tecnologiche sempre più efficaci e in linea con lo stato dell''arte della diagnostica molecolare, la produzione di tutti i kit basati sulla tecnologia NESTED verrà cessata.',N'Non conformità dei prodotti della linea ALERT al nuovo IVDR, mantenimento sul mercato di prodotti tecnologicamente superati.',NULL,N'MM
OP
PC
QM',N'Acquisti
Assistenza applicativa
Controllo qualità
Fornitore
Gare
Magazzino
Marketing
Ordini
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità
Vendite Italia',N'positivo: eliminazione della gestione dei materiali di produzione della linea NESTED  e gestione del nuovo fornitore ExperTeam.
Impatto è positivo, eliminazione della gestione dei prodotti della linea Nested, con sostituzione degli stessi con prodotti analoghi acquistati da fornitore qualificato di terze parti. I nuovi Kit individuati hanno caratteristiche leggermente diverse dai nostri BANG, alcune vantaggiose (PC e Taq si trovano all''interno del kit) ed altre svantaggiose (all''interno del kit non è presente il controllo interno).
Impatto è positivo, eliminazione della gestione dei prodotti della linea Nested, con sostituzione degli stessi con prodotti analoghi acquistati da fornitore qualificato di terze parti. I nuovi Kit individuati hanno caratteristiche leggermente diverse dai nostri BANG, alcune vantaggiose (PC e Taq si trovano all''interno del kit) ed altre svantaggiose (all''interno del kit non è presente il controllo interno).
Impatto positivo: eliminazione dello stoccaggio dei kit (voluminos',N'11/11/2020
11/11/2020
11/11/2020
11/11/2020
22/10/2020
22/10/2020
22/10/2020
22/10/2020
22/10/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
26/11/2020',N'gestione del nuovo fornitore ExperTeam a livello di qualifica selezione e valutazione.
invio delle lettere di dismissione.
gara in essere fino ad 04/2021 per BANG14 e CTRG14 (ex CC19-44 bloccato ed adesso sostituito dal presente), valutare se la dismissione a 12/2020 è fattibile o se necessario attendere 04/2021
nessuna attività a carico.
17/05/2021: aggiornare IO15,01 eliminando i dettagli sull''imballaggio dei prodotti contenenti gel (EPHxx)
Aggiornamento dei documenti di pianificazione, in accordo con il reparto acquisiti e cq. 
fornire a QAS gli ultimi lotti prodotti e rispettiva data di scadenza e l''elenco di tutti i moduli (anche comuni) e le specifiche (anche reagenti) che è possibile archiviare.
1/3/2021 aggiornare IO09,08
Compilazione nel corso del tempo del MOD04,12, secondo le scadenze registrate
contribuire alla stesura della notifica / lettera dismissione da inviare ad autorità competenti e distributori
Verificare se presenti prodotti comuni con altre linee produttive le cui',N'MODULI CQ ARCHIVIATI - cc20-67__mod_cq_da_archiviare.xlsx
IO0904_02_IO0914_07 - cc20-67_io0904_02_io0914_07.msg
sostituzione prodotti in gare laddove necessario - cc20-67_garabang14.msg
IO10,06_07 - cc20-67_messa_in_uso_io1006_07.msg
IO0904_02_IO0914_07 - cc20-67_io0904_02_io0914_07_1.msg',N'PW
L&CS
PW
TGSWC
PP
PP
QAS
QARA
QAS
QCT/DS
SAD-OP
RAS
B&TC
QC
MS
QCT/DS
MM',N'11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
01/03/2021
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
11/11/2020
01/03/2021
11/11/2020
26/11/2020',N'31/12/2020
31/12/2020
31/12/2020
29/01/2021
31/12/2020
25/11/2020
25/11/2020
30/06/2021',N'11/11/2020
30/04/2021
12/05/2021
12/04/2021',N'No',N'prodotti in dismissione',N'No',N'si',N'No',N'prodotti in dismissione',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'Nessun impatto su ra DC12',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'dismissione prodotti',N'Roberta  Paviolo','2021-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-66','2020-10-20 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Il distributore australiano chiede che venga modificato l''Intended Use dei prodotti "Bordetella ELITe MGB KiT" (ref. RTS140ING) e "Meningitis Bacterial ELITe MGB KiT" (ref. RTS300ING). In partcolare si richiede che la frase "The assay has to be carried out in association with ELITe InGenius® system...." venga sostituita con la frase "The assay was validated in association with ELITe InGenius® system..."',N'I prodotti potrebbero essere registrati e venduti in Australia. Non esistono limiti o ostacoli tecnici che precludano l''uso del prodotto con altre piattaforme (basate su sistemi ottici e di fluorescenza simili all''InGenius). Rimane tuttavia chiaro che la validazione del prodotto è stata eseguita con campioni clinici in associazione al sistema InGenius e che solo tali performance vengono riportate nel Manuale d''istruzione per l''uso',N'I prodotti potrebbero non essere registrati e venduti in Australia. ',N'962-RTS300ING - Meningitis Bact ELITe MGB Kit
962-RTS140ING - BORDETELLA ELITe MGB Kit',N'QARA',N'Regolatorio',N'Impatto positivo, si dettaglia che i prodotti sono validati sullo strumento InGenius.',N'21/10/2020',N'modifica IFU ',N'sch rts140ing_01 - cc20-66_ifu_rts140ing_rev.01.msg
sch rts300ing_01 - cc20-66_ifu_rts300ng_rev.01.msg',N'RAS',N'21/10/2020',NULL,N'28/10/2020',N'No',N'nessun impatto',N'No',N'nessuna comunicazione specifica',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'nessun cambiamento',N'No',N'nessuna notifica. L''intended Use non cambia',N'No',N'nessuna',N'No',N'nessuna',N'Michela Boi',NULL,NULL,'2020-10-28 00:00:00',N'No',NULL,NULL),
    (N'20-65','2020-10-19 00:00:00',N'Alessandra Gallizio',N'Documentazione SGQ',N'Nelle fasi finali del progetto SCP2020-005 per lo sviluppo del prodotto SARS-CoV-2 ELITe Standard si è deciso di modificare il nome degli Assay Protocol da utilizzare con il kit SARS-CoV-2 ELITe MGB kit. La modifica riguarda la sostituzione, all''interno del nome dell''AP, di "QT" con "QTIC". 
Si richiede di revisionare i moduli CQ MOD10,13-RTS170ING, MOD10,13-CTR170ING E MOD10,13-STD170ING  inserendo la nuova denominazione degli Assay Protocol,  e di aggiornare il DMRI del prodotto.',N'Gli Assay protocol per l''applicazione quantitativa impongono oggi l''utilizzo dell''IC. Con il nuovo software InGenius di prossima applicazione, sarà possibile anche l''utilizzo del kit quantitativo senza IC per cui si creerà un AP dedicato,  contente la dicitura "QT".  La nuova denominazione degli AP permetterebbe di identificare facilmente l''AP che prevede l''utilizzo dell''IC da quello che non lo prevede.',N'Mancata corrispondenza  tra la nuova denominazione dell''AP e  quella riportata sui moduli di CQ (MOD 10,13) e sul DMRI, con rischio di utilizzo di Assay protocol errato',N'956-RTS170ING - CoV-2 PCR Mix',N'QARA
QC
QM
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'allineamento AP e modulistica
allineamento AP e modulistica
allineamento AP e modulistica
allineamento AP e modulistica
allineamento AP e modulistica
nessun impatto
nessun impatto
nessun impatto
nessun impatto
nessun impatto
nessun impatto',N'19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020
27/10/2020',N'aggiornare i moduli MOD10,13-RTS170ING, MOD10,13-CTR170ING E MOD10,13-STD170ING  inserendo la nuova denominazione degli Assay Protocol ed effettuare la formazione
sostituire i nuovi AP sulla strumentazione in uso al CQ
fornire le informazioni a CQ per la modifica del nome degli AP sui moduli
aggiornare il DMRI di prodotto
Mettere in uso la modulistica di CQ aggiornata
Aggiornare il Fascicolo Tecnico di Prodotto',N'MOD10,13-ctr170ing rev02 - cc20-65_messa_in_uso_mod1013-ctr170ing.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - cc20-65_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020.msg
MOD10,13-ctr170ing rev02 - cc20-65_messa_in_uso_mod1013-ctr170ing_1.msg
MOD10,13-RTS170ING rev02, MOD10,13-STD170ING rev01 - cc20-65_messa_in_uso_moduli_cq_170ing_mod1802_del_29102020_1.msg',N'QC
QCT/DS
QCT/DS
RS
RAS
QAS
RAS',N'19/10/2020
27/10/2020
27/10/2020
21/10/2020
21/10/2020
21/10/2020',N'30/10/2020',N'06/11/2020
24/02/2021
06/11/2020
06/11/2020',N'Si',N'da aggiornare',N'No',N'prodotto ancora da immettere in commercio',N'No',N'prodotto ancora da immettere in commercio',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36, il prodotto non è immesso in commercio',N'No',N'nessun virtual manufacturer',N'No',N'Sul documento MOD22,01 della gestione dei rischi del prodotto RTS170ING/RTS170ING-96 il nome dell''Assay Protocol oggetto del cambiamento è già stato riportato correttamente',N'Alessandra Gallizio','2020-10-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-64','2020-10-15 00:00:00',N'Laura  Bertola',N'Documentazione SGQ',N'Aggiornamento del MOD04-Disp.Matrici "Scheda Dispensazione Matrici" per allinearlo alla nuova procedura di dispensazione delle matrici.',N'Modulo allineato alla nuova procedura di dispensazione delle matrici.',N'Ci sarebbe una discrepanza rispetto alla procedura di dispensazione da effettuare.',NULL,N'RDM
PVM',N'Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo, la documentazione è allineata alla nuova procedura di dispensazione delle matrici.
nessun impatto se non in termini di attività.',N'15/10/2020
15/10/2020',N'Aggiornare il MOD04-Disp.Matrici.
Fare formazione del Modulo a RD e Validazioni
Mettere in uso il modulo aggiornato',N'mod04,01-disp.matrici_01 in uso, formazione MOD18,02 del 15/10/2020 - cc20_64_messa_in_uso_mod04-disp.matrici_01_cc20-64.msg
mod04,01-disp.matrici_01 in uso, formazione MOD18,02 del 15/10/2020 - cc20_64_messa_in_uso_mod04-disp.matrici_01_cc20-64_1.msg',N'RS
QAS',N'15/10/2020
15/10/2020',N'22/10/2020
22/10/2020',N'19/10/2020
19/10/2020',N'No',N'nessun impatto',N'No',N'nessun impatto è una modifica di documentazione operativa interna',N'No',N'nessun impatto è una modifica di documentazione operativa interna',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36, modifica non significativa, aggiornamento di un documento',N'No',N'nessun virtual manufacturer',N'No',N'no',N'Laura  Bertola','2020-10-22 00:00:00',NULL,'2020-10-19 00:00:00',N'No',NULL,NULL),
    (N'20-63','2020-10-13 00:00:00',N'Roberta  Paviolo',N'Manuale di istruzioni per l''uso',N'Si richiede di aggiornare l''uso previsto sull''IFU, SCH mRTS100PLD rev01, del prodotto WNV ELITe MGB® Kit, eliminando la frase "Il prodotto non deve essere impiegato per lo screening di sangue o tessuti od organi per trasfusioni o donazioni d''organo" ed inserendo la frase "NON sono disponibili dati di validazione del prodotto per l''esecuzione del test con matrici biologiche diverse da quelle validate o per applicazioni differenti dall''uso previsto".',N'Si specifica che non sono disponibili dati di validazione su matrici differenti e applicazioni diverse da quelle indicate nel IFU.',N'Prodotto non vendibile in Francia, nemmeno nei centri disponibili a validare gli usi NON previsti.',N'962-RTS100PLD - WNV ELITe MGB Kit 100T',N'QARA
RDM
IMM
PVM
S&MM',N'Regolatorio
Ricerca e sviluppo
Validazioni',N'Impatto positivo, si specifica che non sono disponibili dati di validazione su matrici differenti e applicazioni diverse da quelle indicate nel IFU.
Impatto positivo, si specifica che non sono disponibili dati di validazione su matrici differenti e applicazioni diverse da quelle indicate nel IFU.',N'15/10/2020
15/10/2020',N'modifica IFU in tutte le lingue
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'sch mRTS100PLD_02 inglese, italiano, francese - cc20-63_qualita_sgq_ifu_rts100pld_rev.02_cc20-63.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-63_mod05,36__non_significativa.pdf',N'RAS
QARA
QAS',N'15/10/2020
15/10/2020',NULL,N'19/10/2020
16/10/2020',N'No',N'nessun impatto',N'No',N'avvertenza esplicitata sul IFU',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtula manufacturer',N'No',N'nessun impatto',NULL,NULL,NULL,'2020-10-19 00:00:00',N'No',NULL,NULL),
    (N'20-62','2020-10-12 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 12 a 15 mesi del prodotto SARS-CoV-2 ELITe MGB Kit (962-RTS170ING) ed il formato ridotto RTS170ING-96 sulla base dei dati di stabilità accelerata, come comunicato da R&D in data 07/10/2020.',N'Aggiungere 3 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è
sicuramente positivo.
L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è
sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Modifica dei Moduli di Produzione
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS170ING (STB2020-103) non c''è nessun impatto sulle prestazioni del prodotto',N'19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020',N'nessuna attività a carico
messa in uso documento modificato
Aggiornamento del Fascicolo Tecnico di Prodotto inserendo il Report di Stabilità accelerata (STB2020-103)
modifica modulo di produzione per il prodotto MOD962-RTS170ING_01 e MOD962-RTS170ING-96_00
L''estensione della scadenza è una modifica  significativa, identificare i paesi in cui il prodotto è registrato, effettuare la modifica preliminare
 Effettuare la notifica a modifica svolta
Aggiornare FTP
nessuna attività a carico',N'Viste le tempistiche ristrette tra l''apertura del cc e la messa in uso dei documenti con l''estensione della scadenza a 15 mesi  è stata eseguita solo la notifica ad attività svolte. - cc20-60_notification_of_change_-rts170ing.msg
la modifica è significativa, pertanto è da notificare. - cc20-62_mod05,36__significativa.pdf
Viste le tempistiche ristrette tra l''apertura del cc e la messa in uso dei documenti con l''estensione della scadenza a 15 mesi è stata eseguita solo la notifica ad attività svolte.	29/10/2020 Funzioni coinvolte - cc20-60_notification_of_change_-rts170ing_1.msg
MOD962-RTS170ING, MOD962-RTS170ING-96 E MOD956-RTS170ING  - cc20-62_messa_in_uso_mod962-rts170ingxx_e_mod956-rts170ing.msg
MOD962-RTS170ING, MOD962-RTS170ING-96 E MOD956-RTS170ING  - cc20-62_messa_in_uso_mod962-rts170ingxx_e_mod956-rts170ing_1.msg',N'RAS
RAS
MM
QAS
QCT/DS
RS
RAS
RAS',N'19/10/2020
19/10/2020
21/10/2020
19/10/2020
15/10/2020
15/10/2020
21/10/2020
19/10/2020',N'30/10/2020',N'29/10/2020
29/10/2020
29/10/2020
29/10/2020
29/10/2020
29/10/2020',N'No',N'non necessario',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9, le attività di estensione della scadenza sono parte della procedura di sviluppo',N'Si',N'MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'La documentazione di gestione dei rischi relativa al prodotto SARS-CoV-2 ELITe MGB Kit riporta che per il prodotto sono richiesti 24 mesi di conservazione a -20°C e non è dunque da aggiornare in caso di estensione di scadenza entro i 24 mesi',N'Paolo Saracco','2020-10-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-61','2020-10-02 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 12 a 18 mesi del prodotto Respiratory Viral PLUS ELITe MGB Kit (962-RTS160ING) sulla base dei dati di stabilità accelerata, come comunicato da R&D in data 30/09/2020',N'Aggiungere 6 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS160ING - Resp Vir PLUS ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è
sicuramente positivo.
L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è
sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Modifica dei Moduli di Produzione
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS160ING non c''è nessun impatto sulle prestazioni del prodotto (STB2020-075).
Come dimostrato dai report di stabilità del prodotto RTS160ING non c''è nessun impatto sulle prestazioni del prodotto (STB2020-075).',N'02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020',N'rilavorazione dei lotti a magazzino con estensione della scadenza a 18 mesi
messa in uso documento modificato
modifica modulo di produzione 
la notifica alle autorità competenti non è da effettuare perché il prodotto non è ancora stato immesso in commercio. 
Aggiornare FTP
nessuna attività a carico',N'MOD962-RTS160ING_01, MOD18,02 del 23/11/2020 - cc20-61_messa_in_uso_mod962-rts160ing_01_mod1802_del_23112020.msg
messa in uso MOD956-RTS160ING - cc20-61_messa_in_uso_mod956-rts160ing.msg
MOD962-RTS160ING_01, MOD18,02 del 23/11/2020 - cc20-61_messa_in_uso_mod962-rts160ing_01_mod1802_del_23112020_1.msg
MOD956-RTS160ING rev01 - cc20-61_messa_in_uso_mod956-rts160ing_1.msg
la notifica alle autorità competenti non è da effettuare perché il prodotto non è ancora stato immesso in commercio.  - cc20-61_mod05,36__non_significativa.pdf
STB2020-075 Rev.00 of 29/09/2020 - section_12_stability_test_00.docx',N'PT
QAS
QCT/DS
QAS
RS
RAS',N'02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020
02/10/2020',N'18/12/2020
30/10/2020',N'21/10/2020
25/11/2020
25/11/2020
12/10/2020
23/11/2020
02/10/2020',N'No',N'nessun impatto',N'No',N' nessun impatto il prodotto non è ancora stato immesso in commercio',N'No',N' nessun impatto il prodotto non è ancora stato immesso in commercio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9, le attività di estensione della scadenza sono parte della procedura di sviluppo',N'No',N'la notifica alle autorità competenti non è da effettuare perché il prodotto non è ancora stato immesso in commercio. ',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,'2020-12-18 00:00:00',NULL,'2020-11-25 00:00:00',N'No',NULL,NULL),
    (N'20-60','2020-09-29 00:00:00',N'Clelia  Ramello',N'Luogo di produzione',N'Integrazione nella documentazione EGSpA del nuovo sito produttivo per il prodotto INT035PCR, come comunicato da PSS attraverso ECR 135. Il nuovo sito è  TrustMedical, certificato ISO13485:2016 (vedere allegato), i nuovi lotti verranno indicati con la lettera ''T'' (esempio: Txxxxxxxx), a cui verrà aggiunta una busta dove riporre le 4 buste di PCR cassette. Il primo lotto prodotto sarà il TJ200811A.',N'L''introduzione del nuovo sito produttivo aumenterà la produzione di INT035PCR e farà fronte alle richieste dei clienti.',N'La produzione attuale del componente potrebbe non soddisfare la domanda dei clienti, con conseguente aumento dei tempi di attesa di ricezione del materiale.',N'972-INT035PCR - ELITe InGenius PCR Casset 192',N'GSC
QM
GASC
RAS',N'Assistenza applicativa
Global Service
Program Managment
Sistema qualità',N'Nessun impatto
Nessun impatto
Nessun impatto, TrustMedical è un fornitore qualificato di PSS, non fa parte della lista dei fornitori qualificati EGSpA. Nessun impatto sul prodotto: la busta aggiuntiva NON è un contenitore primario e non modifica il confezionamento, non ha pertanto alcuna significatività in termini di notifica alle autorità competenti. 
Nessun impatto, TrustMedical è un fornitore qualificato di PSS, non fa parte della lista dei fornitori qualificati EGSpA. Nessun impatto sul prodotto: la busta aggiuntiva NON è un contenitore primario e non modifica il confezionamento, non ha pertanto alcuna significatività in termini di notifica alle autorità competenti. 
Nessun impatto sul prodotto',N'05/10/2020
05/10/2020
05/10/2020',N'Comunicazione ai clienti del cambiamento di packaging del INT035PCR
Comunicazione ai clienti del cambiamento di packaging del INT035PCR
Valutazione dell''eventuale necessità di notifica alle Autorità competenti dei paesi in cui il prodotto è registrato.
Aggiornare il Fascicolo Tecnico FTPCONS (Sezione 5) per inserire la documentazione relativa al Manufacturing Transfer fra PSS e TrustMedical e la documentazione relativa al processo produttivo in uso presso TrustMedical 
nessuna attività',N'la modifica non è significativa, pertanto non è da notificare. - cc20-60_mod05,36__non_significativa.pdf
FTP AGGIORNATO - cc20-60_aggiornamento_ftp_consumabili_ingenius.msg',N'GSC
GASC
QM
RAS
SIS',N'05/10/2020
05/10/2020',N'18/12/2020',N'15/01/2021
05/10/2020',N'No',N'nessun impatto',N'No',N'non necessaria (inviata comunicazione ai distributori, TSB 066 revA il 30/09/2020)',N'No',N'nessun impatto',N'PVM3 - Virtual Manufacturer: produzione prodotto ',N'PVM 3.1 - Produzione prodotto presso Virtual Manufacturer',N'verificare a fine anno quanti eventi sono ricaduti sul RA PVM3.1',N'No',N' vedere MOD05,36. Nessun impatto sul prodotto: la busta aggiuntiva NON è un contenitore primario e non modifica il confezionamento, non ha pertanto alcuna significatività in termini di notifica alle autorità competenti. ',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',N'Clelia  Ramello','2020-12-18 00:00:00',NULL,'2021-01-15 00:00:00',N'No',NULL,NULL),
    (N'20-59','2020-09-24 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Il distributore francese ELITechFrance, in seguito alla richiesta di ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé), richiede che venga inserito all''interno del manuale del prodotto "SARS - CoV- 2 ELITe MGB Kit" (ref. RTS170ING) una nota riguardo la possibile delezione del gene ORF8 e che venga aggiornata la bibliografia, con l''aggiunta della referenza B. E. Young et al. (2020) The Lancet 396: 603 - 611, paper in cui si mostra la possibile delezione del gene.',N'Il kit potrebbe essere autorizzato e venduto in Francia ',N'Il kit non potrebbe essere venduto e utilizzato in Francia',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'QARA
RDM
IMM',N'Marketing
Regolatorio
Ricerca e sviluppo
Validazioni',N'L''IFU è adeguata allo stato dell''arte della ricerca scientifica e si conforma alle richieste fatte da ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé).
L''IFU è adeguata allo stato dell''arte della ricerca scientifica e si conforma alle richieste fatte da ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé).
L''IFU è adeguata allo stato dell''arte della ricerca scientifica e si conforma alle richieste fatte da ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé).
L''IFU è adeguata allo stato dell''arte della ricerca scientifica e si conforma alle richieste fatte da ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé).',N'24/09/2020
24/09/2020
24/09/2020
24/09/2020',N'Messa in uso IFU
Aggiornare l''IFU secondo quanto richiesto da ANSM (Agence Nationale de Sécurité du Médicament et des Produits de Santé)
Verifica IFU',N'SCH mRTS170ING_02 - cc20-59_ifu_rts170ing_cc20-59.msg
SCH mRTS170ING_03 - cc20-59_qualita_sgq_ifu_rts170ing_rev.03_cc20-59.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-59_mod05,36__non_significativa.pdf',N'RAS
RDM
PVM',N'24/09/2020
24/09/2020
24/09/2020',NULL,N'02/10/2020
24/09/2020
24/09/2020',N'No',N'nessun impatto',N'No',N'nessuna comunicazione',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU1 - stesura manuale',N'nessun impatto',N'No',N'notifica non necessaria, vedere MOD05,36',N'No',N'nessuna notifica ',N'No',N'nessun intervento',N'Michela Boi',NULL,NULL,'2020-10-02 00:00:00',N'No',NULL,NULL),
    (N'20-58','2020-09-21 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Introduzione del software  zucchetti dedicato alla gestione della formazione in EGSpA',N'Gestione elettronica della documentazione relativa alla formazione che viene svolta in EGSpA, sia interna che esterna.',N'La documentazione relativa alla formazione continuerebbe ad essere gestita attraverso la compilazione cartacea dei documenti.',NULL,N'HRP
QARA
QC
QM
EA-HRS
QAS',N'Controllo qualità
Regolatorio
Risorse Umane
Sistema qualità',N'Gestione elettronica della documentazione relativa alla formazione interna.
Gestione elettronica della documentazione relativa alla formazione che viene svolta in EGSpA, sia interna che esterna.
Gestione elettronica della documentazione relativa alla formazione che viene svolta in EGSpA, sia interna che esterna, introduzione nel software di tutta la documentazione relativa al personale: dall''organigramma al mansionario. Possibilità di collegare il software zucchetti dedicato alla formazione con gli altri software zucchetti, per esempio quello dedicato alle presenze in azienda (timbrature ecc.).
Gestione elettronica della documentazione relativa alla formazione che viene svolta in EGSpA, sia interna che esterna, introduzione nel software di tutta la documentazione relativa al personale: dall''organigramma al mansionario. Possibilità di collegare il software zucchetti dedicato alla formazione con gli altri software zucchetti, per esempio quello dedicato alle presenze in azienda (timbrature ecc.).
Gesti',N'19/10/2020',N'Collaborare nella definizione dei requisiti aggiuntivi (customizzazione) richiesti da EGSpA relativi al MOD18,02.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
A seguito di selezione del fornitore (già identificato come Zucchetti) approvare i requisiti base del software e definire i requisiti aggiuntivi (customizzazione) richiesti da EGSpA:
- introduzione dei dati relativi alla formazione MOD18,02, (registrazione della formazione interna ed esterna), 
- introduzione dei dati relativi al dipendente MOD18,04 (registrazione dei dati del dipendente e della formazione iniziale che viene svolta prima dell''inserimento autonoma alla mansione)),
- caricamento massivo dei dati relativi ai corsi di formazione pregressi relativi alla formazione interna come da MOD18,16A "Database formazione interna" (anni 2018, 2019, 2020)
Creazione del modulo di risk assessment al fine di definire se il softawre deve essere valid',N'identificazione delle caratteristiche che EGSpA richiede al sw per la gestione delle risorse umane (formazione) - cc20-58_identificazionecaratteristichenuovoportale_risorse_umane.docx
Rapporto di riunione relativo alla verifica dei work flow (05/2020) - cc20-58_elitech-attivita_remoto-verifica_workflow_05-2020.pdf
Rapporto di riunione relativo alla richieste di customizzazione (07/2020) - cc20-58_elitech-meetingminute-customizzazionei_richieste_07-2020.pdf
Richiesta introduzione del MOD18,02 sul sw - cc20-58_modifiche_definitive_mod1802-zucchetti_v2_def.docx
Richiesta introduzione del MOD18,04 sul sw - cc20-58_modifiche_definitive_mod1804_00_scheda_dipendente-_neo_assunto_def.xlsx',N'HRP
EA-HRS
QAS
QC
QM
QAS
QAS
QARA
QAS
HRP
QAS
EA-HRS
EA-HRS',N'19/10/2020',NULL,NULL,N'No',N'nessun impatto',N'No',N'non necessaria, trattasi di un software interno per la gestione della documentazione relativa alla formazione egspa',N'No',N'non necessaria, trattasi di un software interno per la gestione della documentazione relativa alla formazione egspa',N'PE - Gestione del Personale
PE - Gestione del Personale',N'PE2 - Formazione/addestramento neoassunto - cambio mansione
PE3 - Formazione/addestramento continuo',N'assegnare il codice al sw e creare un risk assessment',N'No',N'vedere MOD05,36',N'No',N'nessun virtula manufacturer',N'No',N'na',N'Lorena Funes',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-57','2020-09-15 00:00:00',N'Katia Arena',N'Cambiamenti strutturali della sede',N'Durante la pandemia di SARS-COV-2 si è registrato un aumento delle vendite con conseguente aumento della produzione dei kit EGSpA e della vendita dei prodotti di rivendita, determinando un''inadeguatezza degli spazi destinati allo stoccaggio dei materiali destinati alla produzione ed ai prodotti di rivendita.
La pandemia ha, inoltre, reso più difficoltosi gli approvvigionamenti dei materiali, portando l''azienda ad incrementare i quantitativi delle scorte per evitare carenza, o nel peggiore dei casi, assenza dei materiali approvvigionati.
Infine, l''aumento delle vendite ha determinato, anche, una crescita del numero delle spedizioni giornaliere evidenziando spazi dedicati al magazzino non adeguati.
Per tali motivi si ritiene fondamentale ricercare una nuova area da destinare a magazzino / area spedizione: quest''area dovrà essere all''interno del centro Piero della Francesca in modo facilitare gli spostamenti quotidiani e la gestione del magazzino/area spedizione.',N'L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle merce in arrivo, dei prodotti finiti, dei prodotti di rivendita
- l''area dedicata alla spedizione',N'Se tale cambiamento non venisse implementato, avremmo un magazzino con spazi non adeguati all''aumento della merce sia in ingresso che in uscita, con conseguente stoccaggio della merce in aree temporanee destinate ad altro uso (corridoi, area marketing). Un magazzino ed un''area spedizione non adeguati non permetterebbero di sostenere il business aziendale.',NULL,N'CFO
MM
QC
QM
SAD-OP
ITM
QAS
T&GSC',N'Acquisti
Assistenza strumenti esterni (Italia)
Controllo qualità
Magazzino
Manutenzione e taratura strumenti interni
Produzione
Ricerca e sviluppo
Sistema informatico
Sistema qualità',N'Valutare l''impatto che Io spostamento del magazzino ha sul processo acquisti.
L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle merce in arrivo, dei prodotti finiti, dei prodotti di rivendita
- l''area dedicata alla spedizione
L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle merce in arrivo, dei prodotti finiti, dei prodotti di rivendita
- l''area dedicata alla spedizione
L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle merce in arrivo, dei prodotti finiti, dei prodotti di rivendita
- l''area dedicata alla spedizione
L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle merce in arrivo, dei prodotti finiti, dei prodotti di rivendita
- l''area dedicata alla spedizione
L''implementazione di un nuovo magazzino permetterà la gestione di spazi idonei per:
- lo stoccaggio delle ',N'21/09/2020
21/09/2020',N'valutare se e come varia il processo di archiviazione elettronica delle specifiche
identificazione dei requisiti delle nuove aree destinate al magazzino ed alla spedizione
valutazione del flusso di autorizzazione al rilascio del lotto, in accordo con QM e MM. Aggiornamento PR15,02 "Gestione del magazzino", rev02
valutazione del nuovo flusso di prelievo a magazzino (rendere elettronico il MOD09,21 che deve essere compilabile dai reparti richiedenti materiale, stabilire  un orario settimanale per effettuare la richiesta, stabilire le tempistiche necessarie a W per preparare il materiale da distribuire nei reparti, scaricare i lotti su nav, distribuire il materiale ai reparti). Stabilire come organizzare il prelievo delle plastiche per il reparto produzione (mantenimento o meno dei cartellini kanban). Aggiornamento PR15,02 "Gestione del magazzino", rev02
verifica ed aggiornamento dei risk assessment dedicati all''area magazzino ed alla spedizione
verifica che il flusso di recezione, accettazione e stocc',N'PR09,01_rev04 - cc20-57_messa_in_uso_pr0901rev04.msg',N'PW
T&GSC
T&GSS
PW
T&GSC
T&GSS
RDM
MM
QC
ITM
QM
QAS
MM
QM
T&GSC
T&GSC
T&GSC
T&GSC
T&GSC
CSC
CSC
QAS',N'21/09/2020
21/09/2020
30/11/2020',N'11/12/2020',NULL,N'No',N'-',N'No',N'-',N'No',N'-',N'AM - Risk assessment ambientali collegati ai processi
AM - Risk assessment ambientali collegati ai processi
CS - Confezionamento (anche per CQ), Stoccaggio  e Rilascio al cliente del prodotto
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'AM78 - Magazzino (Refrigereted Warehouse)
AM77 - Magazzino a temperatura ambiente (Warehouse)
CS14 - stoccaggio prodotto finito in magazzino EG SpA
SP6 - Stoccaggio dei prodotti presso magazzino esterno ',N'aggiornare / creare dei risk assessemtn dedicati alle nuove aree di magazzino. Valutare l''aggiornamento di tutti i risk assessment riferiti al processo di spedizione (SPxx) eliminando quelli dedicati a processi esternalizzati',N'No',N'vedere mod05,36',N'No',N'da verificare',N'No',N'non applicabile',N'Katia Arena','2020-12-11 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-56','2020-09-15 00:00:00',N'Samuela Margio',N'Componenti principali
Progetto',N'Il presente change control richiede la modifica della formulazione del prodotto HIV1 ELITe MGB Kit (codice RTK600ING) nella sua componente HIV1 PCR Mix (RTS600ING). Nel dettaglio si rende necessaria l''introduzione delle sonde TaqMan-MGB al fine di potenziare la fluorescenza finale e migliorare la lettura del segnale da parte dello strumento ELITe InGenius. Tale richiesta, qualora se sonde TaqMan-MGB sostituissero completamente le sonde Pleiadi-MGB, rappresenta una variazione rispetto a quanto indicato nel MR44 (MRD2019-003), ripreso nel DR27 (PRD2019-012), in cui è richiesto l''usi di sonde Pleiadi-MGB. Attualmente il progetto si trova in fase di sviluppo, con design transfer appena avviato (produzione mini-lotto per preCQ del LP1 effettuata in data 11/09/2020). Tale cambiamento richiederebbe uno slittamento del design transfer di circa 4 settimane con conseguente chiusura del progetto in luglio 2021, anziché giugno 2021.',N'L''uso delle sonde TaqMan-MGB, in sostituzione o in affiancamento alle sonde Pleiadi-MGB, consente di migliorare la fluorescenza finale generata dall''amplificazione del target HIV1. Questo, soprattutto in presenza di campioni a basso titolo, aumenta la probabilità che tale segnale venga correttamente interpretato come positivo dall''ELITe InGenius, incrementando la riproducibilità del test a bassi titoli. ',N'L''attuale formulazione consente di raggiungere con difficoltà i limiti di sensibilità analitica (LoD) ed inclusività documentati nel MRD2019-003 e nel PRD2019-012. Questo espone il progetto al rischio che nella successiva fase di verifica e validazione il prodotto non superi i criteri di accettazione.',NULL,N'MM
PMS
QARA
QC
RDM',N'Acquisti
Controlling
Controllo qualità
Produzione
Program Managment
Ricerca e sviluppo
Sistema qualità',N'Acquisto materie prime per i lotti pilota
Costificazione del prodotto con nuova formulazone
Aggiornamento documentazione di produzione e controllo qualità per la realizzazione dei lotti pilota
Riorganizzazione delle attività del design transfer con avvio produzione LP1 in ottobre e conclusione del LP3 in dicembre
Riorganizzazione delle attività del design transfer con avvio produzione LP1 in ottobre e conclusione del LP3 in dicembre
Ripianificazione del progetto e monitoraggio implementazione delle attività per la chiusura del CC
Test per confermare la nuova formulazione e aggiornamento della documentazione legata al design transfer: moduli di Produzione e CQ, PML, DMRI e DHRI, design transfer plan
Test per confermare la nuova formulazione e aggiornamento della documentazione legata al design transfer: moduli di Produzione e CQ, PML, DMRI e DHRI, design transfer plan
Ripianificazione delle attività legate al controllo del DHR durante la produzione dei lotti pilota in un periodo compreso t',N'28/09/2020
28/09/2020
28/09/2020
15/09/2020
15/09/2020
28/09/2020
28/09/2020
28/09/2020
28/09/2020',N'Acquisto materie prime per i lotti pilota
Costificazione del prodotto con nuova formulazone
Aggiornamento moduli di produzione e CQ
Avvio produzione LP1 in ottobre
Creazione nuove anagrafiche 
Ripianificazione del progetto e monitoraggio implementazione delle attività per la chiusura del CC
Test per confermare la nuova formulazione
Aggiornamento della documentazione legata al design transfer: moduli di Produzione e CQ, PML, DMRI e DHRI, design transfer plan
Aggiornamento della documentazione di gestione dei rischi dei prodotti HIV, HCV e HBV da consegnare al Design Freeze',N'per i test vedere la documentazione di progetto - cc20-56_vedere_documentazione_di_progetto_2.txt
Per le date di firma vedere la documentazione di progetto - cc20-56_vedere_documentazione_di_progetto_1.txt
Per le date di firma vedere la documentazione di progetto - cc20-56_vedere_documentazione_di_progetto.txt',N'PW
C
QCT/DS
PP
PMS
RS
RS
RAS
PT',N'28/09/2020
28/09/2020
21/09/2020
28/09/2020
28/09/2020
28/09/2020
28/09/2020',NULL,N'11/03/2021
11/03/2021
11/03/2021
11/03/2021
23/09/2020
11/03/2021
11/03/2021
11/03/2021',N'Si',N'da aggiornare',N'No',N'non necessario, il prodotto non è ancora immesso in commercio',N'No',N'nessun impatto, il prodotto non è ancora immesso in commercio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'a fine anno valutare gli eventi associati al RA R&D1.1',N'No',N'non necessario, il prodotto non è ancora immesso in commercio',N'No',N'nessun virtual manufacturer',N'Si',N'Aggiornamento della documentazione di gestione del rischio da consegnare al Design Freeze',N'Samuela Margio',NULL,NULL,'2021-03-11 00:00:00',N'No',NULL,NULL),
    (N'20-55','2020-09-11 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 12 a 24 mesi del prodotto Bordetella ELITe MGB Kit (RTS140ING) sulla base dei dati di stabilità accelerata, come comunicato da R&D in data 10/09/2020',N'Aggiungere 12 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS140ING - BORDETELLA ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS140ING (STB2020-023_CdSacc_RTS140ING_U0819AM_U0919BY_U1019CH_U0120-027_00) non c''è nessun impatto sulle prestazioni del prodotto.',N'11/09/2020
16/09/2020
16/09/2020
16/09/2020
16/09/2020',N'rilavorazione dei lotti a magazzino con estensione della scadenza a 24 mesi
messa in uso documento modificato
modifica modulo di produzione
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornare FTP
nessuna attività a carico',N'rilavorazione lotto U0120-027 con lotto U0920-063 - cc20-55_rilavorazionelottou0120-027_1.pdf
MOD962-RTS140ING REV01 - cc20-55_messa_in_uso_mod962-rtsxxxing.msg
MOD962-RTS140ING REV01 - cc20-55_messa_in_uso_mod962-rtsxxxing_1.msg
la modifica è significativa, ma il prodotto, essendo in commercio solo dal 29/07/2020, non è ancora stato registrato presso nessuna autorità competente.  - cc20-55_mod05,36__significativa.pdf
Notifica definitiva per informare i distributori europei - cc20-55_qualita_egspa_sgq_notification_of_change_-rts140ing_-_cc20-55.msg',N'MM
QAS
QCT/DS
QARA
QAS
RDM
RAS',N'16/09/2020
16/09/2020
16/09/2020
22/09/2020
16/09/2020',N'18/09/2020
27/11/2020',N'21/09/2020
12/03/2021
12/03/2021
22/09/2020
16/09/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9, le attività di estensione della scadenza sono parte della procedura di sviluppo',N'Si',N'vedere MOD05,36. la modifica è significativa, ma il prodotto, essendo in commercio solo dal 29/07/2020, non è ancora stato registrato presso nessuna autorità competente.	',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,'2020-11-27 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-54','2020-09-07 00:00:00',N'Davide Barberis',N'Assay Protocol',N'Miglioramento del prodotto RTS160ING "Respiratory Viral Elite MGB kit", non ancora immesso in commercio, attraverso l''aggiornamento dell''assay protocol che ne migliora la sensibilità (abbassamento del Limit of Detection (LoD)).',N'Prodotto più sensibile.',N'Limit of detection più alto.',NULL,N'QARA
QC
QM
RDM
PVM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Aggiornamento dell AP di CQ
Impatto positivo in quanto si rende più sensibile il prodotto
Impatto in termini di attività.
impatto in termini di attività',N'19/10/2020
07/09/2020',N'installazione su strumenti di CQ
Ri-analisi dati e aggiornamento AP
messa in uso AP
Revione AP prima della messa in uso',N'market release respiratory viral plus elite mgb kut del 16/10/2020 - cc20-54_market_release_-_respiratory_viral_plus_elite_mgb_kit.msg',N'RS
RAS
PVM
QC
QCT/DS',N'19/10/2020
07/09/2020',NULL,N'16/10/2020
07/10/2020
16/10/2020
16/10/2020',N'No',N'nessun impatto',N'No',N'na, prodotto ancora in fase di progettazione',N'No',N'na, prodotto ancora in fase di progettazione',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto su RA R&D1.9',N'No',N'na, prodotto ancora in fase di progettazione',N'No',N'na, prodotto ancora in fase di progettazione',N'No',N'-',N'Davide Barberis',NULL,NULL,'2020-10-16 00:00:00',N'No',NULL,NULL),
    (N'20-53','2020-08-28 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Integrazione nella documentazione SGQ del Regolamento Giapponese (MHLW MO 169).',N'Possibilità di registrare i prodotti in Giappone e venderli',N'Nessun business in Giappone.',NULL,N'QARA
QAS
RAS',N'Regolatorio
Sistema qualità',N'La commercializzazione dei prodotti in Giappone impone l''adeguamento della documentazione SGQ al regolamento Giapponese.
La commercializzazione dei prodotti in Giappone impone l''adeguamento della documentazione SGQ al regolamento Giapponese.
La commercializzazione dei prodotti in Giappone impone l''adeguamento della documentazione SGQ al regolamento Giapponese.',N'28/08/2020
28/08/2020
28/08/2020',N'Formazione sul nuovo regolamento giapponese (QARAD pianificata per il 11/09/2020)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Aggiornamento della documentazione SGQ,come da gap analysis allegata, di seguito l''elenco dei documenti impattati:
PR02,01_03_Verifiche ispettive (audit)_03
PR04,01_03_Sviluppo_e_Commercializzazione_di_Prodotto
PR05,01_04_GestioneDellaDocumentazione 
PR05,05_05_GestioneDelleModifiche, 
PR05,02_03_GestioneDellaDocumentazioneProdottiIVD
PR09,03_01_Validazione di processo
PR09,05_01_Device Master Record
PR14,03_04_NotificaAutoritàCompetenteERitiroDelLotto e allegati 
PR14,04_00_SorveglianzaPostProduzione
IO04,02_03_Registrazione Prodotti nei paesi extra EEA
MOD05,36 analisi significatività
Creazione istruzione operativa per la notifica incidente e recall del lotto in Giappone.
Aggiornare MOD18,06 matrice delle competenze e mansionario con i reguisiti del rogolamento',N'PR02,01_04_mod18,02 del 10/09/2020 - cc20-53_messa_in_uso_pr0201_04_cc20-53_mod1802_10092020.msg
PR14,03_05, ALLEGATO12_01, IO14,11_00, formazione come da MOD18,02 del 11/09/2020 (formazione QARAD) - cc20-53_messa_in_uso_pr1403_05_all12_pr1403_01_io1411_00_regolamento_giappone_cc20-53.msg
Aggiornamento della documentazione come da requisiti del regolamento Giapponese presenti nella presentazione fornita da QARAD e come da Companion document 2017-01-06 MDSAP AU G0002.1.004. l''elenco delle procedure da aggiornare è definito nella "Descrizione azione definitiva" - formazione_qms_japan_elitech_sep_20_1.pdf
sono stai archiviati i seguenti documenti del Canada: ALL1_PR05,05	Class III Medical Device Licence Amendment ALL2_PR05,05	Class II Medical Device Licence Amendment ALL3_PR05,05	Class Iv Medical Device Licence Amendment  I nuovi form sono scaricabili  - cc20-53_archiviazione_form_notifica_modifiche_canada_cc20-53.msg
PR09,03_02_Validazione di processo - cc20-53_in_uso_pr0903_02_rap18-21__cc20-53.msg
PR05,01 rev05,PR05,05 rev06, MOD05,36 rev01, MOD05,38 rev01 - c20-53_messa_in_uso_pr_e_mod_per_ivdr_e_reg_giapppone.msg
IO05,08_00MOD18,02 del 2/12/2020 - sgq_messa_in_uso_io0508_cc20-53_mod1802_del_02122020.msg
PR09,01_rev04 - cc20-53_messa_in_uso_pr0901rev04.msg
PR14,04_01 E MESSA IN USO NUOVI PIANI E REPORT - cc20-53_messa_in_uso_dcoumenti_sorveglianza_post_market.msg
formazione da parte di QARAD, MOD18,02 11/09/2020 - formazione_qms_japan_elitech_sep_20.pdf
la modifica non è significativa, pertanto non è da notificare. - cc20-53_mod05,36__non_significativa.pdf',N'QM
QAS
RAS
QARA
QARA
QAS',N'28/08/2020
15/09/2020
28/08/2020',N'30/09/2020',N'15/09/2020
28/08/2020',N'No',N'nessun impatto',N'No',N'non applicabile',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessuno RA relativo alla registrazione dei prodotti presso le autorità competenti',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'no',N'Stefania Brun','2020-09-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-52','2020-08-24 00:00:00',N'Pim Giesen',N'Documentazione SGQ',N'Modifica dell''istruzione di decontaminazione (IO11,14 rev01) e del modulo di registrazione delle attività (MOD19,24 rev01) introducendo nuovi passaggi. ',N'L''introduzione di nuovi passaggi migliora la procedura di decontaminazione.',N'Documentazione obsoleta',N'972-INT030 - ELITe InGenius™
972-INT030-US - ELITe InGenius™',N'CSC
GSC',N'Assistenza strumenti esterni (Italia)
Global Service
Sistema qualità',N'Fruizione più efficace dell''informazione ai tecnici dei distributori/ICO/interni 
Fruizione più efficace dell''informazione ai tecnici dei distributori/ICO/interni 
nessun impatto
nessun impatto se non in termini di attività
nessun impatto se non in termini di attività',N'31/08/2020
31/08/2020
31/08/2020
26/08/2020
26/08/2020',N'aggiornamento  IO11,14 rev01 e MOD19,24 rev01. Formazione al personale egspa (tecnici FSE) ed esterno (distributori/ICO/fornitori di assistenza (es. EDS)
revisione della TSB 014 e formazione
nessuna attività a carico
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'la modifica non è significativa, pertanto non è da notificare. - cc20-52_mod05,36__non_significativa.pdf',N'GSTL
CSC
QAS
QARA
QAS
GSTL',N'31/08/2020
31/08/2020
31/08/2020
26/08/2020',NULL,N'26/08/2020
31/08/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non esiste un risk assessment specifico che tratta la decontaminazione dello strumento Elite InGenis',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Pim Giesen',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-51','2020-08-03 00:00:00',N'Alessandra Gallizio',N'Confezione',N'Si richiede di implementare il formato DEMO per il kit RTS170ING: il kit sarà da 48 test/kit, in una nunc da 5 verrà inserito un tubo di Mix e un tubo di RT enzymemix. Il codice del kit demo sarà RTS170ING-D',N'Favorire il lancio del kit',N'spreco di materiale essendo il formato del kit da 240 test/kit',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'MM
QARA
QM
IMM
S&MM',N'Assistenza applicativa
Controlling
Marketing
Marketing Internazionale
Produzione
Program Managment
Regolatorio
Sistema qualità',N'calcolo del costo del prodotto e sblocco del codice
Impatto positivo in quanto consente la promozione del kit con un dispendio minimo di materiale
Impatto positivo in quanto consente la promozione del kit con un dispendio minimo di materiale
Creazione anagrafiche in Navision e rilavorazione dei lotti a magazzino
Creazione anagrafiche in Navision e rilavorazione dei lotti a magazzino
Creazione anagrafiche in Navision e rilavorazione dei lotti a magazzino
Creazione anagrafiche in Navision e rilavorazione dei lotti a magazzino
Impatto in termini di attività
Impatto in termini di attività
Impatto positivo in quanto consente la promozione del kit con un dispendio minimo di materiale',N'04/08/2020
04/08/2020
04/08/2020
03/08/2020
03/08/2020
03/08/2020
03/08/2020
04/08/2020
04/08/2020
04/08/2020',N'Informare GASC  fornendo i centri interessati alle DEMO
Informare GASC  fornendo i centri interessati alle DEMO
Creazione in NAVISION anagrafica nuovo codice 962-RTS170ING-D
Creazione in NAVISION della Distinta Base e del Ciclo per il nuovo codice
Creazione SKU (stockkeeping units) per procedere con la rilavorazione
Rilavorazione di 4 kit interi di 962-RTS170ING per ottenere 20 kit DEMO
coordinamento  attività per implementazione nuovo formato
Creazione etichette
Messa in uso documenti associati
implementazione della documentazione associata al nuovo codice prodotto 
pianificazione DEMO secondo l''elenco dei centri forniti dal personale mkt',N'Creazione in NAVISION anagrafica nuovo codice 962-RTS170ING-D - cc20-51_anagrafica_prodotto.msg
anagrafica prodotto - cc20-51_anagrafica_prodotto_1.msg
creazione SKU - cc20-51_calcolo_sku.msg
rilavorazione per 40 kit, lotto U0820-042 - cc20-51_rilavorazionex40kit_2.pdf
Nessun modulo deve essere messo in uso per la produzione dei kit demo (modulo generico), l''etichetta approvata è visibile nel modulo di rilavorazione. nessun''altra documentazione è da aggiornare. - cc20-51_rilavorazionex40kit.pdf',N'PT
PT
PT
PP
PMS
QM
QAS
S&MM
IMM
GASC',N'04/08/2020
04/08/2020
03/08/2020
03/08/2020
03/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020',N'04/08/2020
04/08/2020
05/08/2020
05/08/2020',N'04/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020',N'No',N'nessun impatto, il prodotto è per DEMO',N'No',N'nessun impatto, il prodotto è per DEMO',N'No',N'nessun impatto, il prodotto è per DEMO',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'valutazione a fine anno dell''impatto sul RA R&D1.1',N'No',N'na, il prodotto è per DEMO',N'No',N'na',N'No',N'na, il prodotto è per DEMO',NULL,'2020-08-05 00:00:00',NULL,'2020-08-04 00:00:00',N'No',NULL,NULL),
    (N'20-50','2020-07-30 00:00:00',N'Ferdinando Fiorini',N'Documentazione SGQ',N'Creazione e adozione di alcuni nuovi script per i Dispensatori STARlet, codice interno DA9 e DA10 (OneStep1200v2.med - OneStep1200_NALGENE.med - OneStep1200v2_NALGENE.med - Acqua1900v2_NALGENE.med - Acqua19000_NALGENE.med - PleiadiMonoReagente540_NALGENE.med) per utilizzare 2 vaschette per i reagenti in parallelo sulle 2 posizioni refrigerate e le vaschette da 300 mL, da utilizzare per i lotti più grossi.',N'Possibilità di utilizzare entrambe le posizioni refrigerate e le vaschette a 1 solo comparto da 300 mL',N'Difficoltà nel gestire i lotti più grossi, con rallentamento dei tempi, difficoltà nel rabboccare le vaschette dei reagenti e maggior rischio di spreco dei reagenti nella fase di rabbocco.',NULL,N'MM
QM
T&GSC',N'Manutenzione e taratura strumenti interni
Produzione
Sistema qualità',N'Impatto molto positivo perché permette di utilizzare 2 vaschette per i reagenti in parallelo sulle 2 posizioni refrigerate e le vaschette da 300 ml, da utilizzare per i lotti più grossi, con chiari vantaggi di semplicità d''uso, riduzione dei tempi necessari e dei rischi connessi alla manipolazione dei reagenti da dispensare.
Impatto molto positivo perché permette di utilizzare 2 vaschette per i reagenti in parallelo sulle 2 posizioni refrigerate e le vaschette da 300 ml, da utilizzare per i lotti più grossi, con chiari vantaggi di semplicità d''uso, riduzione dei tempi necessari e dei rischi connessi alla manipolazione dei reagenti da dispensare.
Impatto molto positivo perché permette di utilizzare 2 vaschette per i reagenti in parallelo sulle 2 posizioni refrigerate e le vaschette da 300 ml, da utilizzare per i lotti più grossi, con chiari vantaggi di semplicità d''uso, riduzione dei tempi necessari e dei rischi connessi alla manipolazione dei reagenti da dispensare.
Impatto molto positivo perch',N'30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
04/08/2020
04/08/2020
04/08/2020',N'Richiesta al fornitore Hamilton della creazione dei nuovi script:
OneStep1200v2.med
OneStep1200_NALGENE.med
OneStep1200v2_NALGENE.med
Acqua1900v2_NALGENE.med
Acqua19000_NALGENE.med
PleiadiMonoReagente540_NALGENE.med
Verifica dei nuovi script e consegna a SGQ dei file PDF che descrivono ciascuno dei nuovi script
Aggiornamento della specifica d''uso del DA9-DA10 con il nuovo script (SPEC-USO-DA9-DA10 rev02)
Modifica del MOD11,29_VerificaScriptDA, con l''aggiunta dei nuovi script
Formazione
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Verifica e creazione della documentazione per la messa in uso del nuovo DA10',N'spec-uso-da9da10_03 - cc20-50_messa_in_uso_spec-uso-da9da10_03.msg
MOD11,29_03 del 31/07/2020, in uso al 28/09/2020 - cc20-50_messa_in_uso_mod1129_cc20-50.msg
MOD11,29_03 del 31/07/2020, in uso al 28/09/2020 - cc20-50_messa_in_uso_mod1129_cc20-50_1.msg
spec-uso-da9da10_03 - cc20-50_messa_in_uso_spec-uso-da9da10_03_1.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-50_mod05,36__non_significativa.pdf
SPEC-DA_04 - cc20-50_messa_in_uso_spec-da_04_cc20-50.msg
MOD11,10B_01 - cc20-50_messa_in_uso_mod1110b_01.msg',N'PT
PT
PT
PT
QAS
QARA
QAS
MM
T&GSS',N'30/07/2020
30/07/2020
30/07/2020
30/07/2020
04/08/2020
04/08/2020
04/08/2020
04/08/2020',N'31/07/2020
31/07/2020
31/07/2020
30/09/2020',N'31/07/2020
13/04/2021
28/09/2020
13/04/2021
04/08/2020
13/04/2021',N'No',N'nessun impatto ',N'No',N'non applicabile, trattasi di uno strumento utilizzato internamente',N'No',N'non applicabile, trattasi di uno strumento utilizzato internamente',N'ST - Elenco famiglie Strumenti',N'ST16 - Disepnsatori Automatici (DA)',N'nessun impatto, sul RA ST16, si tratta della messa in uso di un nuovo strumento',N'No',N'vedere MOD05,36. la modifica non è significativa, lo strumento messo in uso non apporta alcuna modifica alla tecnologia di produzione',N'No',N'non applicabile, trattasi di uno strumento utilizzato internamente',N'No',N'non applicabile, trattasi di uno strumento utilizzato internamente',NULL,'2020-09-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-49','2020-07-21 00:00:00',N'Cinzia Bittoto',N'Manuale di istruzioni per l''uso',N'Modifica dei valori di "Threshold" e "Noiseband" dei detector FAM e VIC da impostare nel corso della procedura analitica del prodotto «HHV6 ELITe MGB Kit» (RTS036PLD) quando utilizzato in associazione alla piattaforma di amplificazione real time «cobas® z 480 Analyzer» (Roche) e a campioni di sangue intero. La modifica di tali valori è la seguente: da 0,55 a 0,8 per il detector FAM e da 1,2 a 1,5 per il detector VIC.',N'La modifica dei valori di "Threshold" e "Noiseband" dei detector FAM e VIC per il prodotto RTS036PLD consente di avere valori allineati a quelli definiti per altri saggi ELITe MGB validati per la medesima applicazione (sistema Roche, matrice sangue intero). ',N'Disomogeneità fra i diversi prodotti ELITe MGB nei parametri utilizzati per l''analisi di campioni di sangue in associazione al sistema Roche.',N'910-RTS036PLD - HHV6 ELITe MGB KIT 100T',N'QARA
RDM
PVM',N'Marketing
Regolatorio
Ricerca e sviluppo
Validazioni',N'Impatto positivo, tutti i prodotti ELITe MGB presenterebbero gli stessi valori di "Threshold" e "Noiseband" da impostare in caso di analisi con
la piattaforma Roche e campioni di sangue, generando una situazione omogenea
Impatto positivo, tutti i prodotti ELITe MGB presenterebbero gli stessi valori di "Threshold" e "Noiseband" da impostare in caso di analisi con
la piattaforma Roche e campioni di sangue, generando una situazione omogenea
Impatto positivo, tutti i prodotti ELITe MGB presenterebbero gli stessi valori di "Threshold" e "Noiseband" da impostare in caso di analisi con
la piattaforma Roche e campioni di sangue, generando una situazione omogenea
Impatto positivo, tutti i prodotti ELITe MGB presenterebbero gli stessi valori di "Threshold" e "Noiseband" da impostare in caso di analisi con
la piattaforma Roche e campioni di sangue, generando una situazione omogenea
Impatto positivo, tutti i prodotti ELITe MGB presenterebbero gli stessi valori di "Threshold" e "Noiseband" da impostare in caso',N'22/07/2020
22/07/2020
22/07/2020
22/07/2020
22/07/2020
24/07/2020
24/07/2020
24/07/2020
24/07/2020
24/07/2020',N'Rianalisi dei dati relativi alle prestazioni diagnostiche ottenute con il prodotto RTS036PLD validato in associazione al sistema Roche e a campioni di sangue. La rianalisi verrà effettuata utilizzando i nuovi valori di "Threshold" e "Noiseband" dei detector FAM e VIC definiti
Aggiornamento del Report di Validazione e del Summary Verification and Validation Report per riportare i risultati dell''analisi condotta applicando i nuovi valori soglia
Aggiornamento del manuale di istruzioni per l''uso del prodotto RTS036PLD per riportare i nuovi valori soglia
aggiornamento formazione al team di supporto tecnico, marketing e Gestione reclami
Revisione e approvazione dei nuovi Report e dell'' IFU aggiornata
Revisione e approvazione dei nuovi Report prodotti e dell''IFU aggiornata, messa in uso delle IFU
Valutazione della significatività della modifica ed eventuale invio della notifica preliminare alle Autorità Competenti, Ente Notificato e Aziende OEM

Aggiornamento FTP RTS036PLD
Aggiornamento brochure,',N'- REP2018-200 Rev.01 data firma 24/07/2020 - REP2018-201 Rev.01 data firma 24/07/2020 - cc20-49_rep2018-200_rep2018-201.txt
- REP2018-200 Rev.01 data firma 24/07/2020 - REP2018-201 Rev.01 data firma 24/07/2020 - cc20-49_rep2018-200_rep2018-201_1.txt
IFU RTS036PLD rev16 - cc20-49_ifu_rts036pld_cc20-49.msg
formazionemkt e fps, MOD18,02 del 27/07/2020 - cc20-49_formazione_mkte_fps.txt
- REP2018-200 Rev.01 data firma 24/07/2020 - REP2018-201 Rev.01 data firma 24/07/2020 - cc20-49_rep2018-200_rep2018-201_2.txt
IFU RTSP036LD rev16 - cc20-49_ifu_rts036pld_cc20-49_1.msg
- REP2018-200 Rev.01 data firma 24/07/2020 - REP2018-201 Rev.01 data firma 24/07/2020 - cc20-49_rep2018-200_rep2018-201_3.txt
la modifica è significativa, è necessario effettuare una notifica nei paesi presso cui il prodotto è registrato. - cc20-49_mod05,36__significative.pdf
notifica preliminare effettuata - cc20-49__prior_notification_of_change_-rts036pld_-_cc20-49.msg
notifica a modifica effettuata - cc20-49_notifica_a_modifica_effettuata.msg
FTP AGGIORNATO - sezione_1_product_technical_file_index_13_1.docx
Aggiornamento sito web - cc20-49_aggiornamentositoweb.docx',N'PVS
PVS
PVM
PVS
PVM
PVS
RDM
RAS
RAS
RAS
MS',N'22/07/2020
22/07/2020
22/07/2020
22/07/2020
24/07/2020
24/07/2020
24/07/2020
24/07/2020
24/07/2020',NULL,N'29/07/2020
29/07/2020
29/07/2020
29/07/2020
29/07/2020
29/07/2020
31/07/2020
27/07/2020
10/03/2021',N'No',N'nessun impatto',N'Si',N'si attraverso l''avvertenza presente nel IFU',N'No',N'Nessun impatto. Omogeneità delle impostazioni dei parametri di analisi dei prodotti RTK015PLD e RTS020PLD rispetto agli altri prodotti ELITe MGB validati sul sistema Roche, maggior numero di dati analitici a disposizione',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL1 - Fase di pianificazione: pianificazione',N'nessun impatto sul RA VAL1',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'La modifica delle soglie di analisi e il calcolo di nuove performance analitiche non determinano l''introduzione di nuovi rischi ma potrebbero potenzialmente modificare la stima e la valutazione dei rischi già considerati nella documentazione corrente. I risultati ottenuti dai test analitici e dalla rianalisi dei test diagnostici hanno però confermato la stima e valutazione dei rischi in essere e dunque non è necessario l''aggiornamento della documentazione di gestione dei rischi.',N'Cinzia Bittoto',NULL,NULL,'2021-03-10 00:00:00',N'No',NULL,NULL),
    (N'20-48','2020-07-14 00:00:00',N'Eleonora  Mastrapasqua',N'Documentazione SGQ',N'Aggiornamento del MOD04,30 per includere il controllo sui messaggi di output negli Assay Protocol (AP) nella lingua selezionata',N'negli AP utilizzati dal cliente appare il messaggio corretto di output nella lingua selezionata',N'i messaggi di output potrebbero non essere corretti',NULL,N'QARA
QM
RDM
PVC',N'Ricerca e sviluppo
Sistema qualità
Validazioni',N'impatto positivo miglior controllo degli AP, diminuzione del numero di Non Conformità
impatto positivo in quanto si introduce un ulteriore controllo sugli Assay protocols
impatto positivo in quanto si introduce un ulteriore controllo sugli Assay protocols
impatto positivo miglior controllo degli AP, diminuzione del numero di Non Conformità',N'15/07/2020
15/07/2020
15/07/2020
20/07/2020',N'aggiornamento del MOD04,30 1, 2, 3, 4, 7, 9, 11
messa in uso della documentazione
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
verifica dell''aggiornamento del MOD04,30 1, 2, 3, 4, 7, 9, 11',N'MOD04,30_int1 rev02 del 27/07/2020 - cc20-48_messa_in_uso_mod0430int1.msg
MOD04,30_int2 rev02 del 27/07/2020 - cc20-48_messa_in_uso_mod0430_int2_02_cc20-48.msg
MOD04,30_int7 rev01 IN USO AL 17/03/2021 - cc20-48_mod0430_int7_rev01_sw1.3.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-48_mod05,36__non_significativa.pdf
MOD04,30_int1 rev02 del 27/07/2020 - cc20-48_messa_in_uso_mod0430int1.msg
MOD04,30_int2 rev02 del 27/07/2020 - cc20-48_messa_in_uso_mod0430_int2_02_cc20-48_1.msg
MOD04,30_int7 rev01 IN USO AL 17/03/2021 - cc20-48_mod0430_int7_rev01_sw1.3_1.msg',N'RT
QAS
QARA
QAS
PVM',N'15/07/2020
15/07/2020
20/07/2020',NULL,N'14/07/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'AP - Gestione Assay Protocol',N'AP1 - Creazione, verifica, approvazione Assay Protocol',N'valutare',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',N'Eleonora  Mastrapasqua',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-47','2020-07-13 00:00:00',N'Alessandra Gallizio',N'Confezione',N'Per il prodotto RTS170ING (240 test/kit) si richiede di predisporre un formato aggiuntivo da 96 test/kit, accanto a quello attuale da 240 test/kit. Il kit dovrà contenere 2 tubi di "CoV-2 PCR Mix" da 1200 µl ciascuno e 2 tubi di "RT EnzymeMix" da 20 µl ciascuno, in una nunc da 5.',N'Adattamento alle attuali richieste di mercato, 
Il formato da 96 test/kit era stato inizialmente previsto dal "Marketing Requirement Document" MRD2020-001 rev.00 (MR.53), accanto a quello da 240 tes/kit, ma a causa della forte domanda dovuta alla pandemia in corso, in fase di progetto SCP2020-002 si era scelto soltanto il formato da 240 test/kit.
La possibilità di avere anche il formato da 96 test/kit, permetterebbe oggi una maggiore adattabilità alle diverse esigenze dei clienti.
Il formato da 96 test/kit con due tubi di PCR mix da 1200 µl ciascuno comporta, rispetto al formato originale, la diminuzione del numero di tubi per nunc e non la modifica di volume nei tubi,evitando in questo modo l''esecuzione di test di stabilità aggiuntivi.',N'il formato attuale da 240 test/kit non è ottimale in caso di diminuzione della domanda, e risulta attualmente troppo grande per alcuni clienti.',N'962-RTS170ING - SARS-CoV-2 ELITe MGB Kit - 240',N'MM
QARA
QC
QM
RDM
SA-R&D
BDM-MDx
P&MMI',N'Budgeting control
Controllo qualità
Marketing
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'nessun impatto se non in termini di attività
nessun impatto se non in termini di attività
Impatto positivo per adattamento alle attuali richieste di mercato
Impatto positivo per adattamento alle attuali richieste di mercato
Impatto positivo per adattamento alle attuali richieste di mercato
nessun impatto se non in termine di attività
nessun impatto se non in termine di attività
nessun impatto se non in termini di attività
nessun impatto se non in termini di attività
nessun impatto se non in termini di attività
impatto in termini di attività, il prodotto non subisce modifiche dal punto di vista regolatorio,viene introdotto un nuovo formato.
impatto in termini di attività, il prodotto non subisce modifiche dal punto di vista regolatorio,viene introdotto un nuovo formato.
impatto in termini di attività, il prodotto non subisce modifiche dal punto di vista regolatorio,viene introdotto un nuovo formato.
impatto in termini di attività
impatto in termini di attività
impatto ',N'28/07/2020
28/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
28/07/2020
28/07/2020',N'stesura modulistica per nuovo formato/codice
Stesura/aggiornamento modulistica Produzione/CQ per nuovo formato/codice; formazione sulla modulistica al personale di Produzione/CQ
attività di promozione nuovo formato
aggiornamento documentazione di marketing; attività di promozione nuovo formato
formazione sulla nuova modulistica di Produzione; produzione nuovo codice
aggiornamento anagrafiche sul sistema gestionale nav
coordinamento attività per nuovo prodotto
aggiornamento PML2020-004 rev01
aggiornamento PLP
aggiornamento dell'' IFU di RTS170ING e CTR170ING introducendo il nuovo formato
approvazione nuove etichette nuovo formato
aggiornamento documentazione fascicolo tecnico
creazione, verifica ed approvazione nuove etichette nuovo formato
registrazione al ministero e aggiornamernto dichiarazione di conformità
costificazione del nuovo prodotto in formato ridotto
nessuna attività',N'in uso moduli produzione, MOD18,02 del 28/08/2020 e integrazione del 08/09/2020 - cc20-47_messa_in_uso_mod_prod_rts170ing-96_cc20-47.msg
sch mRTS170ING rev01, sch mCTR170ING rev01 in uso al 08/09/2020 - cc20-47_qualita_sgq_market_release_-_sars-cov-2_elite_mgb_kit_96_test_cc20-47_1.msg
etichette approvate - cc20-47_chiusura_ftp170ing-96_2.msg
FTP AGGIORNATO - cc20-47_chiusura_ftp170ing-96_3.msg
dichiarazione ce aggiornata con immissione in commercio rts170ing-96 del 8-09-2020 - mod05,05c_real_time_elite_mgb_31.pdf
PML3030-004 rev01 - cc20-47_chiusura_ftp170ing-96.msg
PLP2020-005 rev01 - cc20-47_chiusura_ftp170ing-96_1.msg',N'QCT/DS
P&MMI
MM
PMS
RAS
QARA
RAS
QM
QM
PMS
PMS
BC
PT
RDM',N'28/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
20/07/2020
28/07/2020',N'28/08/2020
28/08/2020
28/08/2020
30/09/2020
28/08/2020
30/09/2020
30/09/2020
30/09/2020
30/09/2020
30/09/2020
30/09/2020
28/08/2020',N'09/09/2020
18/09/2020
05/08/2020
05/08/2020
18/09/2020
18/09/2020
18/09/2020
09/09/2020
18/09/2020
18/09/2020
18/09/2020
18/09/2020
28/08/2020
28/07/2020',N'Si',N'da aggiornare',N'Si',N'si, attraverso l''avvertenza legata all''IFU',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'da valutare se le richieste del mercato sono state adeguatamente valutate, ed eventualmente aggiornare il RA R&D1.1',N'No',N'non necessaria, il prodotto RTS170ING-96 è un  codice nuovo (vedere MOD05,36)',N'No',N'nessun virtual manufacturer',N'Si',N'occorre aggiornare la descrizione generale del prodotto (MOD22,01 RTS170ING e CTR170ING). Dal momento che la composizione del reagente, il volume di dispensazione, il materiale di confezionamento e la modalità di utilizzo del prodotto non variano, nessun pericolo identificato nella corrente gestione dei rischi è impattato dal cambiamento. Non è pertanto richiesto un aggiornamento della documentazione di gestione dei rischi, a parte il MOD22,01',N'Alessandra Gallizio','2020-09-30 00:00:00',NULL,'2020-09-18 00:00:00',N'No',NULL,NULL),
    (N'20-46','2020-07-10 00:00:00',N'Clelia  Ramello',N'Documentazione SGQ
Etichette
Manuale di istruzioni per l''uso',N'Per il prodotto ELITe InGenius SP200 codice INT032CS si chiede di sostituire il componente ''Sonication tube'' (tubi utilizzati per il processo di estrazione) con tubi alternativi ''Extraction tube''.',N'Sostituire questo componente permetterà al fornitore di migliorare il tempo di produzione.',N'Tempi piu'' lunghi per il riordino (dovuti ai tempi piu'' lunghi di produzione)',N'972-INT032CS - ELITeInGenius SP200 ConsSet 48
972-INT032CS-US - ELITeInGenius SP200 ConsSet 48',N'GSC
QARA
QM
GASC
MS
QAS
RAS',N'Assistenza applicativa
Controllo qualità
Global Service
Marketing
Program Managment
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto in termini di attività
Impatto positivo:  la riduzione del tempo di produzione permette una diminuzione dei costi a EGSpA.
Impatto in termini di attività
Impatto in termini di attività, nessun impatto sul cliente
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività',N'29/07/2020
29/07/2020
29/07/2020
29/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020',N'Aggiornamento documentazione di Marketing per la modifica dei codici INT032CS
Supervisionare attività fornitore PSS, raccogliere la documentazione prodotta da PSS e collaborare chiusura CC e fascicolo tecnico
Valutazione della necessità di comunicare l''aggiornamento all''FDA e verifica se la registrazione del prodotto sulla GUDIDD o su altro DB comprenda etichette, IFU o composizioni che richiedono aggiornamento.
Aggiornamento etichette e approvazione MOD04,13
Aggiornamento IFU e traduzione IFU
Check delle IFU relative ai kit di estrazione per valutare se occorre un aggiornamento
Compilazione MOD05,36 e se necessario effettuare la notifica preliminare alle autorità competenti
Check lotti di produzione
Aggiornamento registrazione al ministero
Aggiornamento fascicolo tecnico del prodotto INT032CS

comunicazione al cliente del cambiamento
Performance evaluation dei nuovi extraction tube
Verificare con il Controllo Qualità se usare gli extraction tube al posto dei sonication tube nei design t',N'BROCHURE AGGIORNATA - cc20-46_emd-elite_ingenius_menu-490-2020-03_en.pdf
la modifica è significativa, pertanto è da notificare ai paesi presso cui il prodotto è registrato - cc20-46_mod05,36_significativa.pdf
la modifica è stata notificata in forma preliminare - cc20-46_egspa_prior_notification_of_change_-int032cs-_cc20-46.msg
lotto J201001 in sdoganamento - cc20-46_attivita_in_corso_a_nov-2020_rispostasis_2.msg
INT032SP200 rev09, INT033SP1000 rev04 - cc20-46_ifu_int032sp200_rev.09_e_int033sp1000_rev.04.msg
notifica a cambiamento avvenuto - cc20-46_notificaacambimanetoavvenuto-int032cs.msg
INT032CS rev04 - cc20-46_inuso_ifu_int032cs_rev.04.msg
AGGIORNAMENTO FTP INT035PCR, INT032CS, INT032SON E F2102-000 - cc20-46_aggiornamento_ftp.msg
TSB 0068 Rev. A  "INT032CS SP200 Consumable Set New Extraction Tube" - cc20-46_tsb068reva_int032cs_sp200consumablesetnewextractiontube.msg
COMPARISON OF EXTRACTION TUBE ON INGENIUS SYSTEM Eppendorf tube vs Sonicator tube - 20-46_comparison_extraction_tubes_ingenius.pdf
REP2020-098 REPORT OF PERFORMANCE EVALUATION OF ELITE INGENIUS NEW EXTRACTION TUBE CC20-46 - cc20-46_rep2020-098_report_of_performance_evaluation_of_new_extraction_tube_cc20-46__rev00_signed.pdf
TSB068 Rev. A  "INT032CS SP200 Consumable Set New Extraction Tube" - cc20-46_tsb068reva_int032cs_sp200consumablesetnewextractiontube.msg
i tappi utilizzati sui sonication tube per il trasporto da cappa di classe II sono compatibili (tappi usati in emergenza COVID) - cc20-46_extractiontubecap_925-cap_compatibilita.jpeg
i tappi utilizzati sui sonication tube per il trasporto da cappa di classe II sono compatibili (tappi usati in emergenza COVID) - cc20-46_extractiontubecap1_925-cap_compatibilita.jpeg
comparazione tubi estrazione e sonicazione, test al CQ - cc20-46_comparazione_tubi_estrazione_e_sonicazione_test_cq.msg
i nuovi tubi sono stati ufficialmente rilasciati - cc20-46_attivita_in_corso_a_nov-2020_rispostasis_1.msg
i nuovi tubi sono stati ufficialmente rilasciati - cc20-46_attivita_in_corso_a_nov-2020_rispostasis.msg',N'MS
SIS
QARA
QM
QAS
RAS
GASC
RT
FSE
GSTL
PVC
QC
QCT/DS
RT',N'29/07/2020
29/07/2020
29/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020
30/07/2020',NULL,N'15/01/2021
13/11/2020
13/11/2020
04/11/2020
24/07/2020
04/11/2020
13/11/2020
12/11/2020
15/01/2021',N'No',N'PSS aggiornerà la documentazione di Produzione',N'Si',N'comunicazione al cliente del cambiamento attraverso TAB',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non applicabile, la modifica è in corso dal fornitore PSS al fine di diminuire tempi e costi di produzione.',N'Si',N'vedere MOD05,36. La modifica è significativa, pertanto da notificare. Secondo le linee guida FDA "Deciding when to submit a 510(k) ..." non è necessario fare un nuovo 510(k).',N'No',N'nessun virtual manufacturer',N'Si',N'La valutazione circa la necessità di aggiornare la documentazione di gestione dei rischi è a carico di PSS',N'Clelia  Ramello',NULL,NULL,'2021-01-15 00:00:00',N'No',NULL,NULL),
    (N'20-45','2020-07-10 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 18 a 24 mesi del prodotto COLISTINA-R ELITe MGB Kit (RTS202ING-48) sulla base dei dati di stabilità reale,
come comunicato da R&D in data 08/07/2020 Report Stabilità STB2019-193',N'Aggiungere 6 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS202ING-48 - COLISTIN-R ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS202ING-48 (Report Stabilità STB2019-193, RTS202ING U1017BK per 24 mesi ) non c''è nessun impatto sulle prestazioni del prodotto.
Come dimostrato dai report di stabilità del prodotto RTS202ING-48 (Report Stabi',N'10/07/2020
10/07/2020
23/07/2020
23/07/2020
10/07/2020
10/07/2020
23/07/2020
23/07/2020
23/07/2020
23/07/2020
23/07/2020',N'nessuna attività a carico
Messa in uso documento modificato.
modifica modulo di produzione
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornare FTP
Nessuna altra azione a carico di R&D.',N'MOD962-RTS202ING-48 rev02, MOD18,02 del 16/07/2020 - cc20-45_mod962-rts202ing-48_02_rac19-20_cc20-45_mod1802_del_16072020_3.msg
MOD962-RTS202ING-48 rev02, MOD18,02 del 16/07/2020 - cc20-45_mod962-rts202ing-48_02_rac19-20_cc20-45_mod1802_del_16072020_2.msg
MOD962-RTS202ING-48 rev02, MOD18,02 del 16/07/2020 - cc20-45_mod962-rts202ing-48_02_rac19-20_cc20-45_mod1802_del_16072020.msg
la modifica è significativa, i paesi presso cui il prodotto è registrato sono vitenam e statio europei - cc20-45_mod05,36__significative.pdf
Comunicazione della modifica a vitenam e distributori europei - cc20-45_notification_of_change_-rts202ing-48_-_cc20-45.msg',N'QC
QCT/DS
MM
QAS
QARA
QAS
RAS
RS',N'10/07/2020
23/07/2020
10/07/2020
23/07/2020
23/07/2020
23/07/2020',N'28/08/2020',N'28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul RA R&D1.9, le attività di estensione della scadenza sono parte della procedura di sviluppo',N'Si',N'vedere MOD05,36. il prodotto è registrato solo in Vietnam.',N'No',N'nessun virtual maufacturer',N'No',N'nessun impatto',NULL,'2020-08-28 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-44','2020-07-09 00:00:00',N'Clelia  Ramello',N'Progetto',N'Nuova versione del software Results Manager per supportare lo strumento InGenius, la possibilità di esportare le informazioni del Ct e dei warning impostati sul dump file, possibilità di esportare la lista dei dump caricati in formato "csv", disponibilità di GUI e manuale utente in italiano. Queste funzionalità sono frutto di richieste del Marketing e di Application, in particolare la parte dell''export è indispensabile per la prima connettività con il Middleware.',N'L''aggiunta di informazioni come il valore di Ct e warning nel file di export permette al cliente di visualizzare sul middleware interfacciato con il software un maggior numero di informazioni. La possibilità di esportare la lista dei dump file agevolerà l''attività degli FPS di monitoraggio delle versioni dump dai clienti. La possibilità di utilizzare la lingua italiana migliorerà l''usabilità del sw.',N'Senza l''export, il cliente deve recuperare le informazioni di Ct e warning direttamente dalla GUI del Results Manager. Senza la funzione di export della lista dei dump la tracciabilità delle versioni dei clienti da parte degli FPS risulterebbe difficoltosa. Utilizzo del sw in lingua inglese.',NULL,N'FPS
PVS
QM
RT
ITM
SIS',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Program Managment
Ricerca e sviluppo
Sistema informatico
Sistema qualità
Validazioni',N'Una volta rilasciata la nuova versione software, il PMS non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, R&D non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, Validazioni non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, l''impatto sull''assistenza strumenti esterna è in termini di attività:  connettività della nuova versione verso middleware e LIS
Una volta rilasciata la nuova versione software, IT non ha impatto in termini di attività.
Una volta rilasciata la nuova versione software, il Sistema Qualità ha impatto in termini di attività: messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente
Una volta rilasciata la nuova versione software, il Sistema Qualità ha impatto in termini di attività: messa in uso del software ed informazione sull''indirizzo di salvataggio cui ',N'19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020',N'Supervisionare attività fornitore , redigere il piano di validazione con MOD05,23, approvare il report di validazione con MOD05,24
Revisione del piano di validazione, validazione del sw e stesura del report di validazione secondo modello MOD05,24 in collaborazione con Validazioni
Revisione del piano di validazione, validazione del sw e stesura del report di validazione secondo modello MOD05,24 in collaborazione con R&D
Supportare la connettività della nuova versione verso middleware e LIS
Revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24)
Messa in uso del software ed informazione sull''indirizzo di salvataggio cui gli FPS possono accedere per effettuare le installazioni dello stesso presso il cliente
Revisionare e approvare piano e report di validazione (MOD05,23 MOD05,24)
Comunicazione al cliente del cambiamento, training al cliente, supportare interfacciamento con middleware',N'piano validazione firmato il 22/10/2020 - mod0523_00_piano_validazione_sw46_results_manager_-_cc20-44_signed_1.pdf
rapporto validazione firmato il 22/10/2020 - mod0524_00_rapporto_validazione_sw46_results_manager_-cc20-44_signed_1.pdf
esempio di installazione, formazione e collegamento con il LIS del sw result manager presso il cliente "AZIENDA SANITARIA LOCALE AT - ASTI / LAB ANALISI BIOLOGIA MOLECOLARE / ASTI". - cc20-36_esempioinstallazioneefromazionecliente_2.pdf
sw46 messo in uso il 27/10/2020 - cc20-44_messa_in_uso_sw_result_manager.msg
esempio di installazione, formazione e collegamento con il LIS del sw result manager presso il cliente "AZIENDA SANITARIA LOCALE AT - ASTI / LAB ANALISI BIOLOGIA MOLECOLARE / ASTI". - cc20-36_esempioinstallazioneefromazionecliente_3.pdf',N'PMS
SIS
RDM
RT
PVS
PVC
CSC
ITM
QAS
QM
CSC
FPS',N'19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020
19/10/2020',NULL,N'29/10/2020
29/10/2020
29/10/2020
04/11/2020
29/10/2020
29/10/2020
29/10/2020
04/11/2020',N'No',N'na',N'No',N'non applicabile, il sw non è ancora in commercio',N'No',N'non applicabile, il sw non è ancora in commercio',N'SW - Elenco Software',N'SW46 - Result Manager',N'nessun impatto sul RA SW46',N'No',N'il sw non è ancora immenso in commercio',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,'2020-11-04 00:00:00',N'No',NULL,NULL),
    (N'20-43','2020-07-09 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Si richiede la modifica delle seguenti specifiche: 
- SPEC953-207_02_epDualfilterTIPS_5000_µL: cambio del codice del fornitore VWR in 613-6783 (il codice 613-3688 attualmente inserito in specifica non è più utilizzato dal fornitore)
- SPEC953-232_00_Tubi_1,6_mL_Tappo_a_vite_viola, SPEC953-233_00_Tubi_1,6_mL_Tappo_a_vite_giallo e SPEC953-234_00_Tubi_1,6_mL_Tappo_a_vite_arancione: avere la possibilità di poter ordinare i prodotti non solo dal fornitore "AXON" ma anche dal produttore "NxtBio Technologies" (da notare che è cambiata la ragione sociale rispetto a quella inserita attualmente in specifica "National Scientific, Supply Company Inc").',N'Nel caso della richiesta di modifica della specifica SPEC953-207, il cambiamento risulta necessario per poter facilitare il controllo in accettazione del materiale di consumo.

Nel caso della richiesta di modifica delle specifiche SPEC953-232, SPEC953-233 e SPEC953-234 il fatto di poter ordinare direttamente dal produttore ci permette di acquistare con tempistiche e costi migliorativi. Inoltre il fornitore "Axon" ci ha comunicato di non aver attualmente disponibili i codici in questione.
Si tenga presente che il produttore ha cambiato la sua ragione sociale in: NxtBio Technologies (ex "National Scientific, Supply Company Inc").
',N'Se il cambiamento non fosse implementato alla specifica SPEC953-207, al momento dell'' arrivo della merce il codice inserito sulla specifica non corrisponderebbe al codice prodotto e ciò complicherebbe il controllo in accettazione.

Se il cambiamento non fosse implementato alle specifiche SPEC953-232, SPEC953-233 e SPEC953-234 si rischierebbe di rimanere sprovvisti del materiale plastico utilizzato per la dispensazione delle bulk di FAST-TRACK.',NULL,N'MM
PW
RDM',N'Acquisti
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo in quanto sarebbe possibile approvvigionarsi da un doppio fornitore di materiale plastico.
Impatto positivo in quanto sarebbe possibile approvvigionarsi da un doppio fornitore di materiale plastico.
Si ha così la possibilità di ordinare sia dal produttore che dal fornitore scegliendo per ogni acquisto l'' opzione più vantaggiosa in termini di costi e disponibilità dei prodotti.
Nessun impatto se non in termini di attività. 
Nessun impatto se non in termini di attività. ',N'09/07/2020
09/07/2020',N'aggiornamento specifiche 
- SPEC953-207_02_epDualfilterTIPS_5000_µL
- SPEC953-232_00_Tubi_1,6_mL_Tappo_a_vite_viola, 
- SPEC953-233_00_Tubi_1,6_mL_Tappo_a_vite_giallo 
- SPEC953-234_00_Tubi_1,6_mL_Tappo_a_vite_arancione
formazione al personale interessato
nessuna attività
messa in uso specifiche
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'spec9563-207_03, spec953-232_01, spec953-234_01 - cc20-43_messa_in_uso_spec953-xx_e_950-180_cc19-45_cc20-43.msg
SPEC953-233 rev01 - cc20-43_messa_in_uso_spec.msg
spec9563-207_03, spec953-232_01, spec953-234_01 - cc20-43_messa_in_uso_spec953-xx_e_950-180_cc19-45_cc20-43_1.msg
SPEC953-233 rev01 - cc20-43_messa_in_uso_spec_1.msg
la modifica non è significativa, pertanto non è da notificare. Il fornitore qualificato in uso non viene sostituito, ma affiancato da un secondo fornitore per ridurre il rischio di mancate forniture, si ritiene pertanto una modifica non significativa. - cc20-36_mod05,36__non_significativa_1.pdf',N'RT
PW
RT
QAS
QARA
QAS',N'09/07/2020
09/07/2020',NULL,N'23/03/2021
22/09/2020
22/09/2020
23/03/2021
14/07/2020',N'No',N'nessun impatto sul DMRI perchè non si modificano i codici delle specifiche',N'No',N'non necessaria',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'L''aggiunta di un fornitore non ha impatto sul RA A2',N'No',N'vedere MOD05,36. Il fornitore qualificato in uso non viene sostituito, ma affiancato da un secondo fornitore per ridurre il rischio di mancate forniture, si rtiene pertanto una modifica non significativa.',N'No',N'le specifiche non sono inviate al virtual manufacturer',N'No',N'nessun impatto',N'Anna Capizzi',NULL,NULL,'2021-03-23 00:00:00',N'No',NULL,NULL),
    (N'20-42','2020-07-03 00:00:00',N'Lucia  Liofante',N'Processo di CQ e criteri di CQ
Annullato / non approvato',N'Eliminare l''esecuzione del Controllo Qualità sui prodotti 902-INT021EX (reagenti di estrazione associati allo strumento ELITe GALAXY) e 910-EXTB01 (kit di estrazione manuale su colonna), entrambi prodotti da Macherey Nagel.
La richiesta si inserisce nell''ottica di uniformare le procedure di controllo e rilascio al cliente riguardanti i reagenti dei sistemi di estrazione degli acidi nucleici NON prodotti internamente: attualmente è previsto il controllo funzionale  sui codici 902-INT021EX e 910-EXTB01, non sul codice 940-INT011EX (associato a ELITe STAR) e sui codici 972-INT032SP200/972-INT033SP1000 (associati a ELITe InGenius).
La richiesta è supportata dall''esito CONFORME ottenuto in CQ su TUTTI i lotti di:
- 902-INT021EX, 6 lotti controllati dal 2015 ad oggi
- 910-EXTB01, 10 lotti controllati dal 2010 ad oggi,
da cui deriva un giudizio positivo nell''ambito della valutazione del fornitore Macherey Nagel. ',N'Maggiore uniformità delle procedure interne e riduzione dei tempi di attesa per il rilascio a magazzino',N'Rallentamento sul rilascio dei lotti, nell''ambito di un quadro in cui i reagenti per gli altri sistemi di estrazione non vengono sottoposti a controllo funzionale',N'902-INT021EX - ELITe GALAXY 300 Extr kit-1x96
910-EXTB01 - EXTRAblood 50 T',N'MM
PW
QARA
QC
QM
RDM',N'Acquisti
Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Viene meno la necessità di pre-allertare il reparto Controllo Qualità in previsione dell''arrivo di nuovi lotti di prodotto da controllare
Viene meno la necessità di pre-allertare il reparto Controllo Qualità in previsione dell''arrivo di nuovi lotti di prodotto da controllare
Viene meno la necessità di pre-allertare il reparto Controllo Qualità in previsione dell''arrivo di nuovi lotti di prodotto da controllare
Si solleva il reparto dall''esecuzione di test impegnativi dal punto di vista di tempo e risorse. In aggiunta, per il CQ di 902-INT021EX è necassario utilizzare lo strumenato Galaxy ubicato nel laboratorio BSL2 di Ricerca e Sviluppo, quindi in un ambiente di lavoro non destinato al personale di CQ.
Si solleva il reparto dall''esecuzione di test impegnativi dal punto di vista di tempo e risorse. In aggiunta, per il CQ di 902-INT021EX è necassario utilizzare lo strumenato Galaxy ubicato nel laboratorio BSL2 di Ricerca e Sviluppo, quindi in un ambiente di lavoro non destinato al personale di ',N'09/07/2020
09/07/2020
09/07/2020
09/07/2020',N'Segnalare alla Qualità l''elenco dei lotti in arrivo di 902-INT021EX e 910-EXTB01 fino a dicembre 2020 e che non verranno sottoposti alla verifica funzionale sulla base del CC in oggetto, onde monitorare le prestazioni sul campo (eventuali reclami di performance)
Aggiornamento elenco MOD10,18 con eliminazione righe dedicate al CQ di 902-INT021EX e 910-EXTB01
Aggiornamento previsioni di consumo di campioni di riferimento e reagenti utilizzati nei CQ dei codici in oggetto (vedi WHO IS CMV, etanolo...)
Revisione del modulo di Produzione 910-EXTB01 con eliminazione riferimento a CQ
Aggiornamento ALL1-PR10,01 "CONTROLLI FUNZIONALI IN INGRESSO", rev01
nessuna attività a carico
Messa non in uso MOD10,13-INT021EX, MOD10,12-INT021EX, MOD10,13-EXTB01 e MOD10,12-EXTB01
Monitoraggio delle prestazioni dei lotti rilasciati fino a dicembre 2020 (mediante analisi dei reclami)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare l',NULL,N'QCT/DS
QCT/DS
QAS
PW
RT
QM
CCA
QCT/DS
MM
QARA
QAS
QC',N'09/07/2020
09/07/2020
09/07/2020
09/07/2020',N'09/07/2020
31/12/2020',N'09/07/2020',N'No',N'verificare se presente ',N'No',N'non necessaria',N'No',N'nessun impatto sul prodotto in commercio',N'CQ - Controllo Qualità prodotti',N'CQ4 - Pianificazione CQ',N'nessun impatto sul risk assessment CQ4, si elimina dalla pianificazione di CQ il controllo sui kit di estrazione dei fornitori esterni',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,NULL,NULL,'2020-09-07 00:00:00',N'Si',N'il CC20-42 non è approvato: non è possibile snellire il processo legato ai kit di estrazione eliminando il cq funzionale. Attualmente eseguiamo il CQ funzionale in quanto la fornitura di McNagel è di un prodotto RUO che abbiamo validato internamente come CE-IVD. La disponibilità da parte di McNagel di "kit gemelli" ai nostri marcati CE-IVD, permetterebbe di eliminare il CQ funzionale come controllo in ingresso. Dato che McNagel  ha a disposizione un kit CE-IVD per il prodotto NucleoSpin Blood ma non per NucleoMag Blood, si ritiene opportuno lasciare per entrambi il controllo qualità funzionale a supporto del Controllo in ingresso.
','2020-09-07 00:00:00'),
    (N'20-41','2020-07-03 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Aggiornamento specifica assistenza tecnica cappe a flusso laminare per nuovo laboratorio, elenco cappe e prove richieste',N'Nuove cappe installate all''interno dei laboratori',N'Specifica assistenza tecnica cappe a flusso laminare incompleta',NULL,N'T&GSS',N'Assistenza strumenti interni
Manutenzione e taratura strumenti interni
Sistema qualità',N'-
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'06/07/2020
06/07/2020',N'aggiornamento della SPEC-CL rev01 specifiche AssistenzaTecnica Cappe A flusso Laminare
messa in uso  SPEC-CL specifiche AssistenzaTecnica Cappe A flusso Laminare, rev02',N'SPEC-CL rev02 - cc20-41_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020.msg
SPEC-CL rev02 - cc20-41_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020_1.msg',N'T&GSS
QAS',N'06/07/2020
06/07/2020',NULL,N'29/07/2020
29/07/2020',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'ST - Elenco famiglie Strumenti',N'ST4 - Cappe Flusso Laminare (CL)',N'impatto su ST4, la modifica riguarda l''aggiunta di alcuni modelli',N'No',N'MOD05,36. La modifica non è significativa, non è da notificare. Notifica virtual',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'Fabio Panariti',NULL,NULL,'2020-07-29 00:00:00',N'No',NULL,NULL),
    (N'20-40','2020-07-03 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Aggiornamento specifica dI assistenza tecnica Refrigeratori con i nuovi modelli',N'Nuovi modelli di congelatori e frigoriferi installati',N'Specifica d''assistenza tecnica Refrigeratori incompleta',NULL,N'T&GSS',N'Assistenza strumenti interni
Manutenzione e taratura strumenti interni
Sistema qualità',N'-
nessun impatto se non in termini d i attività
Nessun impatto se non in termini di attività. ',N'06/07/2020
06/07/2020',N'aggiornamento della SPEC-CG-CR-FR-UC rev02 Specifiche Assistenza Tecnica Refrigeratori
messa in uso SPEC-CG-CR-FR-UC Specifiche Assistenza Tecnica Refrigeratori in rev03. ',N'SPEC-CG-CR-FR-UC rev03 - cc20-40_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020.msg
SPEC-CG-CR-FR-UC rev03 - cc20-40_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020_1.msg',N'T&GSS
QAS',N'06/07/2020
06/07/2020',NULL,N'29/07/2020
29/07/2020',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'ST - Elenco famiglie Strumenti
ST - Elenco famiglie Strumenti',N'ST13 - Frigoriferi (FR/CR)
ST17 - Congelatori(CG)',N'nessun impatto su ST13 e ST17, la modifica riguarda l''aggiunta di alcuni modelli',N'No',N'MOD05,36. La modifica non è significativa, non è da notificare.',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'No',N'Non applicabile, la modifica riguarda l''aggiornamento dei modelli di refrigeratori utilizzati in EGSpA',N'Fabio Panariti',NULL,NULL,'2020-07-29 00:00:00',N'No',NULL,NULL),
    (N'20-39','2020-06-29 00:00:00',N'Paola Gonzalez Ruiz',N'Manuale di istruzioni per l''uso
Annullato / non approvato',N'Durante la compilazione della richiesta di creazione di assay protodol di terze parti, il forniotre Benelux ha evidenziato un incongruenza nelle "Specifiche della PCR": è indicato il volume minimo per la reazione di PCR di 10uL, mentre nel manuale di istruzioni per l''uso dello strumento ELITe InGenius (Sezione 10.10)  è indicato il volume minimo per la reazione di PCR di 20uL. Si richiede l''aggiornamento del modulo per la richiesta di creazione degli assay di terze parti: il range per il corretto riscaldamento della soluzione nelle cassette di PCR è infatti tra 20 µL e 50 µL. ',N'Maggiore flessibilità del sistema. ',N'Nonostante tecnicamente si possa caricare più o di meno volume, i volumi non rientrerebbero nelle specifiche di riscaldamento dello strumento.',NULL,N'QARA
RDM',N'Assistenza applicativa
Global Service
Regolatorio
Ricerca e sviluppo',N'nessun impatto
Impatto positivo, maggiore flessibilità del sistema. 
nessun impatto.',NULL,N'messa in uso documentazione
Aggiornamento MOD19,23_02_Assay Compatibility evaluation form
valutazione tecnica della richiesta di change control.',NULL,N'RDM
RT
GASC
QAS',NULL,NULL,NULL,N'No',N'na',N'No',N'da valutare',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non applicabile. la creazione degli assay protocl di terze parti non sono gestite nei risk assessment',N'No',N'na',N'No',N'na',N'No',N'na',N'Alessandro Bondi',NULL,NULL,'2020-07-17 00:00:00',N'Si',N'IL CC NON è APPROVATO COME DA MAIL ALLEGATA DI GASC DEL 16/07/2020:
"Le specifiche da manuale abbiamo verificato essere corrette e non farei modifiche al MOD19,23 che si occupa esclusivamente di prodotti di terze parti per l''estero le cui performance sono di esclusiva responsabilità del distributore e i cui volumi sono generalmente all''interno del range indicato."','2020-07-17 00:00:00'),
    (N'20-38','2020-06-25 00:00:00',N'Stefania Brun',N'Materia prima',N'A seguito delle richieste da parte degli utilizzatori Labnet si rende necessaria la modifica del prodotto RNA Reference con l''inserimento del nuovo punto di diluizione 10^-4.5, oppure 0.003125%, in quanto questo identifica il cut off clinico per valutare la dismissione della terapia nei pazienti con leucemia mieloide cronica. Questo punto è ritenuto utile per valutare quanti replicati sono ritenuti necessari per il monitoraggio dei pazienti alla dismissione.',N'Rispondenza ai requisiti del cliente e a quelli delle linee guida Gimema per la gestione della terapia nei pazienti CML in remissione molecolare profonda',N'Rischio di perdita del mercato a favore dei competitors diretti (Acrometrix, Bioclarma) che propongono il prodotto a 5 punti',N'910-SPG07-210 - PHILA p210 RNA REFERENCE 6T',N'MM
PW
QC
RDM
BDM-MDx',N'Acquisti
Controllo qualità
Marketing
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema informatico
Sistema qualità',N'Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nel periodo di tempo in cui il punto di diluizione 10^-4.5  verrà prodotto internamente sarà necessario effettuare un CQ su ogni diluizione, andando ad impattare negativamente il carico di lavoro. Quando sarà il fornitore a produrre il punto 10^-4.5  il numero di CQ tornerà ad essere quello originario.
Nel periodo di tempo in cui il punto di diluizione 10^-4.5  verrà prodotto internamente sarà necessario effettuare un CQ su ogni diluizione, andando ad impattare negativamente il carico di lavoro. Quando sarà il fornitore a produrre il punto 10^-4.5  il numero di CQ tornerà ad essere quello originario.
Nel periodo di tempo in cui il punto di diluizione 10^-4.5  verrà prodotto internamente sarà necessario effettuare un CQ su ogni diluizione, andando ad impattare negativamente il carico di lavoro. Quando sarà il fornitore a produrre il punto 10^-4.5  il numero di CQ tornerà ad essere quello originario.#',N'10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
27/07/2020',N'Modifica del contratto con Invivoscribe per aggiungere fornitura del punto di diluizione 10^-4.5 
Il fornitore non riesce a fornire in tempi brevi il nuovo punto di diluizione a 10^-4.5 , è perciò necessario richiedere un incremento di materiale affinché possa essere prodotta la diluizione internamente. L''incremento è su due livelli:
- del IVS0035 in occasione dell''invio dei raw materilas
- del punto 10^-4 e IVS0035 in occasione dell''invio del pannello.
Nel periodo di tempo in cui il punto di diluizione 10^-4.5 verrà prodotto internamente sarà necessario effettuare un CQ su ogni diluizione
Revisione moduli di produzione relativi a SPG07-210: creare il modulo per il semilavorato intermedio (punto diluizione 10^-4.5 ) creato internamente; aggiornare i moduli di produzione e di cq in uso e definire le nuove modalità di confezionamento (mantenere nunc da 5, inserire inserto viola)
Formazione al personale di produzione sulle nuove revisioni dei moduli di produzione
Revisione moduli di produzione',N'PRIMO LOTTO CQ U0920-052 - cc20-38_esecuzionecq5puntoegspa.docx
aggiornati moduli produzione e cq per la produzione del punto di diluizione 10^-4.5  internamente.  - cc230-38_messa_in_uso_modxxx_spg07-210_produzione_5tubo_interna_cc20-38_mod1802_del_28072020.msg
messa in uso moduli produzione con diluizione egspa - cc20-38_messa_in_uso_modspg07-210_produzione_cc20-38__rac19-20.msg
Il materiale primario è  testato da CQ e ha passato il CQ e risponde ai requisiti richiesti. LA fornitura è stata confermata - cc20-38_chiusuraattivitasard.msg
E'' stata confermata a GIMEMA e a Gottardi, rappresentante dei centri di riferimento LabNet, la spedizione del lotto a 5 punti per le date indicate da LabNet.  - cc20-38_chiusuraattivitasard_1.msg
IFU sch SPG07-210 rev08 - cc20-38_ifu_spg07-210_rev.08.msg
la modifica è significativa, pertanto è da notificare nei paesi presso cui il prodotto è registrato - cc20-38_mod05,36__significativa.pdf
notifica preliminare - cc20-38_qualita_egspa_sgq_prior_notification_of_change_-spg07-210_-_cc20-38.msg
notifica a modifica effettuata - cc20-38_notificamodificasignificativa.msg
approvazione template 78 - cc20-38_messa_in_uso_template_1.docx
messa in uso moduli produzione con diluizione egspa - cc20-38_messa_in_uso_modspg07-210_produzione_cc20-38__rac19-20_2.msg
MOD962-SPG07-210 rev 04 e MOD956-IVS-0011_4.5 rev00 - cc20-38_messa_in_uso_modspg07-210_produzione_cc20-38__rac19-20.msg
CC20-38_MODxxx SPG07-210 cq 5tubo interna (MOD18,02 del 28-07-2020) - cc20-38_modxxx_spg07-210_cq_5tubo_interna_mod1802_del_28072020.msg
STB2020-129_Test_Cong-Scong_SGP07-210_U0920-052 STB2020-121_CdS_Acc_SGP07-210_U0920-052_CW - cc20-38_stb.txt
creazione template 78 - cc20-38__template_78_spg07-210_aggiunta_punto_diluizione_10-5.msg
i uso template 78 - cc20-38_messa_in_uso_template.docx
Nella prospettiva marketing, i kit a 4 punti saranno in parte usati per collaborazione scientifica/Demo, in parte rimarranno in vendita per i clienti non afferenti al LabNet.  - cc20-38_chiusuraattivitasard_2.msg
creazione nuovo template 78, modello verde, cont 5, temperatura -70°C, nunc da 5 - cc20-38__template_78_spg07-210_aggiunta_punto_diluizione_10-5.msg
Gantt aggiornata al 29/07/2020 - cc20-38_gantt_spg.xlsx',N'QC
QCT/DS
PW
QC
QCT/DS
SA-R&D
SA-R&D
MM
RDM
QC
RAS
QARA
QAS
QARA
QM
PW
PT
QC
RT
QARA
QM
MM
ITT
PMS
QM
QAS',N'10/07/2020
27/07/2020
10/07/2020
10/07/2020
14/07/2020
27/07/2020
10/07/2020
10/07/2020
10/07/2020
27/07/2020
27/07/2020
10/07/2020
27/07/2020
10/07/2020
27/07/2020
27/07/2020
09/07/2020
14/07/2020
27/07/2020
27/07/2020',N'30/06/2020
19/09/2020
20/07/2020',N'03/11/2020
06/11/2020
03/11/2020
06/11/2020
10/11/2020
06/11/2020
02/09/2020
06/11/2020
02/09/2020
29/07/2020',N'Si',N'si ',N'Si',N'si attraverso l''avvertenza',N'No',N'no ',N'P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',N'P3.15 - Misurazione e dispensazione semilavorati',N'nessun impatto su P3.15, la modifica è di prodotto',N'Si',N'vedere MOD05,36',N'No',N'No in quanto il prodotto non è oggetto di contratto con virtual Manufacture',N'No',N'No',NULL,'2020-09-19 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-37','2020-06-19 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Includere all''interno dei documenti / file delle IFU  di tutti i prodotti ELITe in commercio (RTS, vedi elenco Allegato 1) un''appendice costituita da una trattazione dei contenuti tecnici più rilevanti che sia sintetica, schematica e di immediato impatto visivo (IFU semplificata). (Questo CC sostituisce il CC20-21 che viene annullato). ',N'Rendere più maneggevole la consultazione dei contenuti tecnici da parte del cliente / utilizzatore del prodotto.',N'Possibile disagio nella consultazione delle IFU complete da parte del cliente / utilizzatore finale. IFU ridotte fornite sotto forma di materiale di mkt e non allegate all''IFU del prodotto.',NULL,N'QARA
RDM
GASC
MAI
PVC',N'Assistenza applicativa
Marketing
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'L''integrazione nelle IFU di un''appendice che tratti in modo sintetico i contenuti tecnici (definite IFU ridotte) è positivo dal punto di vista del cliente/utilizzatore che può accedere in modo agevole alle informazioni tecniche relative al prodotto. Le IFU ridotte non sostituiscono l''IFU nel formato completo. La gestione delle IFU ridotte come appendice dell''IFU ha un impatto positivo a livello di gestione delle modifiche: ogni qualvolta una modifica impatta sull''IFU viene valutata automaticamente anche l''iFU ridotta. Non ci sono impatti sulle registrazioni del prodotto, in quanto l''IFU ridotta non include dati diversi o aggiuntivi rispetto al IFU, nè informazioni non propriamente validate. Non è pertanto necessario integrare le registrazioni presso le autorità competenti, ma effettuare una notifica informativa ai distributori. Le IFU ridotte saranno solo in lingua inglese, non sono previste traduzioni in altre lingue.
L''integrazione nelle IFU di un''appendice che tratti in modo sintetico i contenuti te',N'28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020',N'valutare la significatività del cambiamento ed eventualmente notificarlo ai distributori / ICO
Trasferimento di tutte le Appendici (IFU ridotte) dalle cartelle del MKT alle cartelle SGQ (tutti i prodotti RTS ELITe In commercio, vedi Allegato 1). I documenti dovranno essere disponibili ufficialmente solo nelle cartelle SGQ.
verifica del contenuto delle singole appendici. In particolare si richiede la verifica dei contenuti Regolatori e della codifica del documento.
valutazione delle modalità di gestione e codifica delle IFU ridotte (tracciare sull''IFU ridotta solo la revisione in formato alfabetico "AA" al fine di evitare confusione con la data e revisione dell''IFU completa es: IFU COMPLETA "SCH mRTS036PLD_en 19/03/2020 Review 15", IFU RIDOTTA SCH m RTS036PLD HHV6_InGenius AA).  Approvazione e messa in uso delle Appendici come documenti singoli. Le Appendici saranno disponibili nelle cartelle di SGQ all''interno delle cartelle prodotto-specifiche insieme alle IFU.
Aggiornamento delle IFU per allegare ',N'prodotti RTS ELITe In commercio per cui deve essere disponibile l''Appendice - mod05,10_05_02_stato_prodotti_egspa.xlsx
la modifica non è significativa, pertanto non è da notificare. - cc20-37_mod05,36__non_significativa.pdf
PR05,04_03 E mod0507_03;_mod0507a_03 - cc20-37_messa_in_uso_pr0504_03_e_mod0507_03;_mod0507a_03.msg',N'MS
RAS
RAS
QARA
QAS
RAS
RAS
RAS
MS
GPS
RAS
MS
RDM
PVM
RDM
PVM',N'28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020
28/07/2020',N'23/12/2020',N'06/08/2020
12/05/2021
28/07/2020
28/07/2020',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'Aggiornamento IFU',N'IFU - Gestione Manuali',N'IFU1 - stesura manuale',N'possibile rischio di uso off-label del prodotto se eventuali usi off-label venissero riportati nell''Appendice in modo non adeguatamente segnalato.',N'No',N'non necessaria, veder MOD05,36',N'No',N'non necessaria',N'Si',N'possibile rischio di uso off-label del prodotto se eventuali usi off-label venissero riportati nell''Appendice in modo non adeguatamente segnalato.',N'Michela Boi','2020-12-23 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-36','2020-06-17 00:00:00',N'Clelia  Ramello',N'Progetto',N'Delineare la gestione dei dump, file utilizzati dal software Results Manager per migliorare e velocizzare l''interpretazione dei dati rilasciati da strumenti amplificatori, quali per esempio 7500 fast dx; e gestione della messa in uso del software stesso.',N'L''uso del Results Manager  e la definizione della gestione dei dump file, tramite procedure e istruzioni operative, migliora e velocizza l''interpretazione dei dati da parte dei clienti. ',N'L''analisi dei dati in modalità manuale ossia attraverso le IFU resterebbe l''unica soluzione per il cliente.',NULL,N'FPS
QARA
RT
BDM-MDx
GASC
GPS
PVC
QAS
SIS
QCT/DS',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Gare
Global Service
Marketing
Program Managment
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Una volta delineata la gestione dei dump secondo un processo definito, l''impatto sull''assistenza strumenti esterna è in termini di attività: gestione delle licenze attive del sw Results Manager installato presso i clienti e delle scadenze della manutenzione annuale.
Una volta delineata la gestione dei dump secondo un processo definito, l''impatto sull''assistenza strumenti esterna è in termini di attività: gestione delle licenze attive del sw Results Manager installato presso i clienti e delle scadenze della manutenzione annuale.
Una volta delineata la gestione dei dump secondo un processo definito, l''impatto sull''assistenza strumenti esterna è in termini di attività: gestione delle licenze attive del sw Results Manager installato presso i clienti e delle scadenze della manutenzione annuale.
Una volta delineata la gestione dei dump secondo un processo definito, il Global Support  sarà responsabile dei training verso i clienti, dell''installazione del Results Manager e della gestione dei dump file pr',N'10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
01/07/2020
01/07/2020
01/07/2020
01/07/2020
01/07/2020
01/07/2020
01/07/2020
01/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020',N'Gestire e registrare licenze attive del sw Results Manager, installato presso i clienti; gestire le scadenze annuali del supporto tecnico Finbiosoft annuale
Gestire la fornitura del PC per l''installazione del sw Results Manager
Seguire le connettività del Results Manager verso middleware e LIS
Training verso MKT, Reclami sul Results Manager in collaborazione con Validazioni e R&D
Training verso i clienti sull''uso del Results Manager
Stesura IO installazione e aggiornamento software/dump e caricamento licenze
Installazione e aggiornamento del Results Manager e dump file presso i clienti
Documentazione:
Brics
Presentazione ppt (coinvolgimento del gruppo di (Appllication Specialist)
Teaser
Litsa prirità da fornire al PM:
- Brics
- traduzioni lingue GUI E manuale 
-priorità TEST-matrici per creazione e rilascio dump per l''anno 2020
Coordinare le funzioni coinvolte al fine di poter chiudere il CC compresi le relazioni con il fornitore
formazione, in collaborazione con VAL, al personale',N'piano di validazione - mod0523_00_piano_validazione_sw46_results_manager_-_cc20-44_signed.pdf
rapporto di validazione - mod0524_00_rapporto_validazione_sw46_results_manager_-cc20-44_signed.pdf
release note - cc20-36_release_note_result_manager_20.9.15.319.pdf
messa in uso sw - cc20-36_messa_in_uso_sw_result_manager.msg
PR05,08_02 - cc20-36_messa_in_uso_pr0508_02_dump_file.msg
MOD04,33_00 in uso al 04/08/2020 - cc20-36_messa_in_uso_mod0433_controllo_dump_file_cc20-36_2.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-36_mod05,36__non_significativa.pdf
MOD04,33_00 in uso al 04/08/2020 - cc20-36_messa_in_uso_mod0433_controllo_dump_file_cc20-36_1.msg
RESULT MANAGER, SW46 IN USO IL 27/10/2020 - cc20-36_messa_in_uso_sw_result_manager.msg
pr0508_02 - cc20-36_messa_in_uso_pr0508_02_dump_file.msg
MOD05,07A_03 - cc20-36_messa_in_uso_pr0504_03_e_mod0507_03;_mod0507a_03.msg
MOD04,33_01 - cc20-36_messa_in_uso_mod0433_01.msg
MOD05,07A_03 - cc20-36_messa_in_uso_pr0504_03_e_mod0507_03;_mod0507a_03_1.msg
creazione in sgat evo dell''anagrafica di Result Manager, gestione delle licenze con seriale e possibilità di registrazione delle attività di installazione, collaudo ecc. - cc20-36_attivitaacaricodicsc.msg
pr0508_02 - cc20-36_messa_in_uso_pr0508_02_dump_file_1.msg
MOD04,33_00 in uso al 04/08/2020 - cc20-36_messa_in_uso_mod0433_controllo_dump_file_cc20-36.msg
la fornitura del PC non è indispensabile per la messa in uso del Results manager , se è previsto l''invio di PC è gestito e tracciato con ddt specifico verso il cliente - cc20-36_attivitaacaricodicsc_1.msg
esempio di installazione, formazione e collegamento con il LIS del sw result manager presso il cliente "AZIENDA SANITARIA LOCALE AT - ASTI / LAB ANALISI BIOLOGIA MOLECOLARE / ASTI". - cc20-36_esempioinstallazioneefromazionecliente_4.pdf
esempio di installazione del sw result manager presso il cliente "AZIENDA SANITARIA LOCALE AT - ASTI / LAB ANALISI BIOLOGIA MOLECOLARE / ASTI". - cc20-36_esempioinstallazioneefromazionecliente.pdf
esempio di installazione e formazione del sw result manager presso il cliente "AZIENDA SANITARIA LOCALE AT - ASTI / LAB ANALISI BIOLOGIA MOLECOLARE / ASTI". - cc20-36_esempioinstallazioneefromazionecliente_1.pdf
IO19,15 rev00 - cc20-36_messa_in_uso_io19,15_00.msg
MOD04,33_re01  - cc20-36_messa_in_uso_mod0433_01.msg',N'SIS
PVC
PVC
QARA
QM
RAS
RAS
PVC
PVC
CSC
ESA
RT
RT
FPS
GASC
FPS
GASC
GPS
FPS
FPS
GASC
RT
RT
CSC
ESA
CSC
FSE
MAI
MS
MAI
MS
GASC
GPS
FPS
FPS
GASC
T&CM
RAS
PVM',N'10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
01/07/2020
01/07/2020
01/07/2020
25/11/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020
10/07/2020',NULL,N'04/11/2020
04/11/2020
04/11/2020
27/01/2021
12/05/2021
27/01/2021
02/12/2020
03/11/2020
04/11/2020
04/11/2020
10/07/2020',N'No',N'na',N'No',N'a livello di mkt',N'No',N'nessun impatto',N'SW - Elenco Software
AP - Gestione Assay Protocol',N'SW46 - Result Manager
AP1 - Creazione, verifica, approvazione Assay Protocol',N'valutarese i dump devono essere inclusi nella gestione  degli AP o a parte',N'No',N'na. non è un una modifica, ma la gestione dei file associati al nuovo sw Result Manager (SW46)',N'No',N'nessun virtual manufacturer',N'No',N'L''introduzione dei dump file ha impatto sul pericolo A.2.11 relativo all''interpretazione dei dati, riducendo la probabilità di accadimento grazie all''automatizzazione del processo. Poichè però l''utilizzo dei dump file non verrà effettuato da tutti i clienti e l''interpretazione manuale rimarrà comunque in uso, non si ritiene di modificare la documentazione di gestione del rischio poichè la probabilità assegnata si basa sull''evento a più alto rischio (interpretazione manuale) ',N'Clelia  Ramello',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-35','2020-06-15 00:00:00',N'Benedetto Maschietto',N'Confezione
Etichette',N'A seguito di fornitura da parte del fornitore Aurogene si richiede l''etichettatura delle bustine contenenti plastiche (non etichettate) con un etichetta contenente lo stesso codice e lotto del confezionamento primario in cartone con cui arriva a noi il materiale codice BSH05S1. 
Le provette contenenti l''enzima Proteinasi K fanno parte del kit codice BSC71S1E ma arrivano a parte in quanto devono essere stoccate a T° +2/+8°C. 
Al momento della spedizione al cliente il codice materiale BSH05S1 deve essere composto da 1 provetta di proteinasi k, due di bustine di plastiche e il kit di estrazione cod. BSC71S1E.',N'Modifica necessaria per non incorrere in problematiche di spedizione e tracciabilità dei reagenti',N'Possibile problematiche nella spedizione e non tracciabilità dei consumabili',NULL,N'PW
SAD-OP
SAD-TCO',N'Acquisti
Gare
Magazzino
Ordini
Produzione
Sistema qualità',N'Solo in termini di attività.
nessun impatto
Solo in termini di attività.
Solo in termini di attività. 
Solo in termini di attività. 
Solo in termini di attività. 
Solo in termini di attività',N'15/06/2020
15/06/2020
15/06/2020
15/06/2020
22/06/2020
22/06/2020
15/06/2020',N'Accordi con fornitore per avere l''implementazione della modifica di confezionamento dalla prossima fornitura
Attività di applicazione delle etichette sui sacchetti codice BSH05S1 ricevute dalla Produzione, incaricata della stampa.
Prestare attenzione nell''allestimento della spedizione del prodotto per quanto riguarda l''associazione di n° 2 buste del codice BSH05S1 per 1 kit di BSC71S1E e n°1 proteinasi K che deve essere associata con lo stesso lotto del kit di estrazione.
Predisporre la picking list indicando di associare la Proteinase K da spedire insieme al kit di estrazione.
Attenzione: la nota nella picking list deve specificare che la proteinasi K che deve essere associata con lo stesso lotto del kit di estrazione.
Stampa delle etichette secondo le indicazioni di SGQ. L''attività di applicazione delle etichette sui sacchetti codice BSH05S1 è a carico del personale di magazzino
. Valutare attività di applicazione etichette su sacchetti codice BSH05S1 a magazzino
Creazione etichetta codice',N'Crteazione etichette come da MOD09,37 del 16/06/2020, da apporre sulla busta contenente materiale plastico - cc20-35_rilavorazione_etichetta_plastiche_2.txt
Stampa etichette come da MOD09,37 del 16/06/2020, da apporre sulla busta contenente materiale plastico - cc20-35_rilavorazione_etichetta_plastiche_1.txt
MOD09,08 del 16/06/2020, apposizione etichetta (come da MOD09,37 del 16/06/2020) sulla busta contenente materiale plastico - cc20-35_rilavorazione_etichetta_plastiche.txt
dichiarazione su caratteristiche di stoccaggio del produttore HANGZHOU BIOER TECHNOLOGY CO., LTD - cc20-35_diachiarazione_aurogene_su_caratteristiche_stoccagio_prodotto.pdf',N'QM
MM
SAD-OP
PW
MM
PW',N'15/06/2020
15/06/2020
15/06/2020
22/06/2020
15/06/2020',NULL,N'16/06/2020
16/06/2020
16/06/2020',N'No',N'No in quanto è un prodotto di terze parti',N'No',N'No in quanto è un prodotto di terze parti',N'No',N'No in quanto è un prodotto di terze parti',N'ALTRO - Altro',N'ALTRO - ALTRO',N'no ',N'No',N'non applicabile',N'No',N'non applicabile',N'No',N'non applicabile',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-34','2020-06-12 00:00:00',N'Alessandra Gallizio',N'Componenti principali',N'Si richiede lo sviluppo di un nuovo prodotto SARS-CoV-2 ELITe Standard, codice STD170ING  da utilizzare in associazione al prodotto SARS-CoV-2 ELITe MGB Kit, codice RTS170ING per ampliare l''offerta commerciale rendendo disponibile un''applicazione semi-quantitativa per l''identificazione e la quantificazione dell''RNA di SARS CoV-2.
Da valutare se il prodotto in questione sarà RUO o CE-IVD.',N'A causa del rapido evolversi delle situazione dovuta alla pandemia da COVID-19, potrebbe a breve essere richiesta una versione quantitativa del kit SARS-CoV-2, che, a fianco di quella qualitativa CE IVD già sul mercato, permetterebbe di differenziare EG SpA dai competitors. R&D ha effettuato delle prove di fattibilità  utilizzando il kit SARS-CoV-2 per un''applicazione quantitativa dimostrando che è possibile con un set di Assay Protocols basati sul Modello 2 utilizzare l''attuale kit come saggio quantitativo. Lo sviluppo di una versione RUO del prodotto consentirebbe di avere un prodotto disponibile in tempi più rapidi. Lo sviluppo di una versione CE-IVD del prodotto consentirebbe di avere un prodotto marcato CE e dunque maggiormente spendibile sul mercato che richiederebbe però tempi di sviluppo più lunghi. ',N'Se non ci si indirizzasse verso un''applicazione quantitativa del kit si rischierebbe di perdere l''opportunità di rimanere competitivi sul mercato.',NULL,N'MM
QARA
QC
QM
RDM
SA-R&D
VP Molecul',N'Budgeting control
Controllo qualità
Marketing
Operation Site
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'5) Esecuzione del test di CQ per il prodotto STD170ING una volta commercializzato. L''introduzione di un prodotto di tipo quantitativo impatta sulle specifiche e sulle modalità di controllo di Controllo Qualità di RTS170ING; tali modifiche impattano sia sulle modalità di attribuzione della conformità, sui tempi dedicati al test e di emissione dell''esito  e utilizzo degli strumenti di Laboratorio (da 1 a 2 sessioni di CQ). 
In caso vi siano variazioni in termini di numerosità di lotti da produrre, sarà necessario rivedere la pianificazione di produzione e CQ ed eventuali risorse necessarie ( strumenti/personale), tale valutazione deve essere fatta coinvolgendo OM. 

1) Impatto positivo, disponibilità di un prodotto da utilizzare in associazione al prodotto SARS-CoV-2 ELITe MGB Kit, codice RTS170ING per ampliare l''offerta commerciale rendendo disponibile un''applicazione semi-quantitativa per l''identificazione e la quantificazione dell''RNA di SARS CoV-2. Per poter essere competitivi sul mercato si racc',N'21/07/2020
21/07/2020
15/06/2020
21/07/2020
21/07/2020
21/07/2020',N'Per le azioni del CQfar riferimento all'' SCP relativo allo sviluppo del prodotto STD170ING

Per le azioni del MKT far riferimento all'' SCP relativo allo sviluppo del prodotto STD170ING
Per le azioni della Produzione far riferimento all'' SCP relativo allo sviluppo del prodotto STD170ING
Per le azioni del Regolatorio far riferimento all'' SCP relativo allo sviluppo del prodotto STD170ING
Per le azioni dell''R&D far riferimento all'' SCP relativo allo sviluppo del prodotto STD170ING
Apertura SCP dedicato al prodotto STD170ING. Per le azioni dell''Assicurazione di Qualità far riferimento all'' SCP ',N'SCP2020-005, SARS-CoV-2 ELITe MGB Kit Quantitativo, codice assegnato il 19/06/2020 - cc20-34_assegnazionecodiceprogetto.txt',N'QC
IMM
QARA
RDM
MM
QM',N'21/07/2020
21/07/2020
21/07/2020
21/07/2020
21/07/2020',NULL,N'05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020
05/08/2020',N'Si',N'da aggiornare',N'No',N'a livello di mkt',N'No',N'il prodotto STD170ING non è in commercio',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'la creazione di un nuovo prodotto non ha impatto sul RA R&D1.1',N'No',N'na. Il prodotto non è in commercio.',N'No',N'nessun virtual manufacturer',N'Si',N'da aggiornare la gestione dei rischi dei prodotti RTS170ING e CTR170ING per introdurre il prodotto STD170ING',N'Alessandra Gallizio',NULL,NULL,'2020-08-05 00:00:00',N'No',NULL,NULL),
    (N'20-33','2020-06-11 00:00:00',N'Katia Arena',N'Confezione',N'A causa della recente pandemia, sono aumentati notevolmente i consumi del prodotto INT02100100 "ELITe GALAXY Consumable Set" e in contemporanea si hanno molte difficoltà nel reperire i materiali necessari per il suo confezionamento, pertanto si richiede di procedere con un confezionamento diverso.',N'Attuando queste modifiche si potranno ottimizzare i materiali necessari a produrre il prodotto finito ed avere un numero maggiore di kit',N'Produzione di prodotto finito che non soddisfa la richiesta attuale',N'910-INT02100100 - ELITe GALAXY Consumable Set',N'BC
MM
QC
QM
RDM
SAD-TCO',N'Acquisti
Produzione
Regolatorio
Sistema qualità',N'Valutare con i fornitori tempi di approvvigionamento ed eventuali soluzioni alternative temporanee.
Valutare con i fornitori tempi di approvvigionamento ed eventuali soluzioni alternative temporanee.
La soluzione che si ritiene più consona e si approva è quella di effettuare dei confezionamenti con un numero inferiore di materiali. Non si approva il confezionamento misto con tubi sterili e non sterili.
L''impatto sulla produzione è la modifica del confezionamento del prodotto che dovrà prevedere fasi di etichettatura e confezionamento attualmente non presenti. 
La soluzione che si ritiene più consona e si approva è quella di effettuare dei confezionamenti con un numero inferiore di materiali. Non si approva il confezionamento misto con tubi sterili e non sterili.
L''impatto sulla produzione è la modifica del confezionamento del prodotto che dovrà prevedere fasi di etichettatura e confezionamento attualmente non presenti. 
La soluzione che si ritiene più consona e si approva è quella di effet',N'16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020
16/06/2020',N'Valutare un''alternativa ai tubi sterili 953-212 di cui si ha difficoltà a ricevere il materiale: VWR propone lo stesso tubo identico ma non sterile. Per il primo lotto, C0420-024, è stata attuata questo tipo di soluzione. Successivamente si è valutata una soluzione di difficile attuazione in quanto sarebbe necessario confezionare il prodotto con entrambi i tubi in modo da avere  lotti sufficientemente grandi.
Un''altra alternativa proposta è il confezionamento contenente materiali in quantità inferiore, in questo modo sarebbe possibile produrre un lotto molto più grande (500/600 kit) invece dei 300 kit previsti secondo il confezionamento attuale. Questa alternativa potrebbe essere mantenuta fino a 12/2020.
valutare se mantenere il nuovo confezionamento con materiali ridotti o tornare al confezionamento originario.
Nel caso del confezionamento con tubi sterili e non sterili: bisogna prevedere etichettatura ddei due tipi di tubi
Modificare il confezionamento del prodotto modificando il MOD910-INT021',N'MOD910-INT02100100 rev03 12/06/2020 - cc20-33_modifica_confezionamento_elite_galaxy_consumable_set_-_910-int02100100.msg
INT02100100 rev02 - cc20-33_ifu_int02100100_cc20-33.msg
la modifica è significativa, pertanto è da notificare. Il prodotto è registrato, oltre che in Europa, solo in Macedonia - cc20-33_significativa.pdf
Notifica effettuata nell''unico paese in cui era registrato (Macedonia) - cc20-33_notification_of_change_-int02100100-_cc20-33.msg',N'PC
PT
QM
PCT
QM
QM
QM
RAS
QARA
QAS
QARA
PW
MM
QARA
QM',N'16/06/2020
16/06/2020
16/06/2020
07/04/2021
16/06/2020
16/06/2020
16/06/2020
16/06/2020
01/04/2021',N'16/06/2020
21/12/2020
19/06/2020
07/04/2021
19/06/2020
19/06/2020
19/06/2020
19/06/2020',N'16/06/2020
16/06/2020
07/04/2021
18/06/2020
18/06/2020
25/06/2020
18/06/2020',N'No',N'nessun impatto',N'No',N'attraverso l''avvertenza',N'No',N'nessun impatto',N'CS - Confezionamento (anche per CQ), Stoccaggio  e Rilascio al cliente del prodotto',N'CS8 - assemblaggio prodotto per CQ / prodotto finito',N'il RA CS8 non subisce impatti',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2021-04-07 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-32','2020-06-10 00:00:00',N'Ferdinando Fiorini',N'Documentazione SGQ',N'Si richiede la modifica dei 2 moduli di produzione relativi al reagente CHAPS (MOD09,36-Stoccaggio_CHAPS_00 e MOD955-CHAPS-10_00) per adeguamento ai grossi quantitativi necessari per la produzione in grande scala del nuovo prodotto SARS-CoV-2',N'Possibilità di usare una sola aliquota di materia prima e quindi un solo lotto di semilavorato. Questo riduce anche il rischio di errore.',N'Spreco di tempo nel pesare le aliquote che poi devono essere riunite nella preparazione del buffer ',NULL,N'MM
QC
QM
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Nessun impatto se non in termini di attività
Semplificazione del processo e riduzione del rischio di errori
La modifica della preparazione non ha impatto sul reagente finale
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'23/06/2020
22/06/2020
23/06/2020
23/06/2020
23/06/2020',N'Modifica dei 2 moduli MOD09,36-Stoccaggio_CHAPS_00 e MOD955-CHAPS-10_00
verifica ed approvazione dei moduli
nessuna attività
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'MOD955-chaps-10, rev01_MOD0936 rev01_formazione MOD18,02 del 20/07/2020 - cc20-32_mod955-chaps-10_01_mod0936_01_cc20-32_mod1802_del_20072020.msg
MOD955-chaps-10, rev01_MOD0936 rev01_formazione MOD18,02 del 20/07/2020 - cc20-32_mod955-chaps-10_01_mod0936_01_cc20-32_mod1802_del_20072020_1.msg
MOD955-chaps-10, rev01_MOD0936 rev01_formazione MOD18,02 del 20/07/2020 - cc20-32_mod955-chaps-10_01_mod0936_01_cc20-32_mod1802_del_20072020_2.msg
la modifica NON è significativi, pertanto non è da notificare nei paesi presso cui il prodotto è registrato - cc20-32__non_significativa.pdf',N'QC
QCT/DS
MM
QAS
RDM
QARA
QAS',N'23/06/2020
22/06/2020
23/06/2020
23/06/2020
23/06/2020',NULL,N'28/07/2020
28/07/2020
28/07/2020
28/07/2020
10/06/2020',N'No',N'Nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.14 - Misurazione e dispensazione semilavorati',N'nessun impatto sul RA P1.14',N'No',N'vedere MOD05,36. La modifica non è significativa pertanto non è da notificare, inoltre il CHAPS non è coinvolto in alcun prodotto Dekra.',N'No',N'Il CC non ha impatto sui prodotti forniti al Virtual Manufacturer in quanto il CHAPS non è contenuto nei prodotti ELITe MGB PLD forniti.',N'No',N'La modifica ha impatto sui pericoli B.2 a I (disomogeneità del lotto) e B.2 a II (incoerenza da lotto a lotto). La modifica riduce la probabilità di accadimento dell''evento. Non si ritiene però di modificare la probabilità assegnata e, di conseguenza, la valutazione del rischio poiché tali pericoli possono essere generati da molteplici fattori e la preparazione del CHAPS rappresenta solo uno di essi.',NULL,NULL,NULL,'2020-07-28 00:00:00',N'No',NULL,NULL),
    (N'20-31','2020-05-28 00:00:00',N'Renata  Catalano',N'Documentazione SGQ',N'Dismissione dei prodotti Research Use Only  INT032SP200-US, INT035PCR-US, INT032CS-US e F2104-000',N'A seguito dell''approvazione da parte dell''FDA dello strumento ELITe InGenius (INT030), i prodotti collegati e sottomessi all'' FDA in associazione allo strumento (ELITe InGenius SP200, ELITe InGenius PCR Cassette, ELITe InGenius SP200 Consumable Set, ELITe InGenius Waste Box) possono essere commercializzati negli Stati Uniti come 510k senza dover ricorrere alla versione RUO.
',N'Se il cambiamento non fosse implementato si commercializzerebbe negli Stati Uniti la versione RUO dei prodotti ELITe InGenius SP200, ELITe InGenius PCR Cassette, ELITe InGenius SP200 Consumable Set, ELITe InGenius Waste Box anzichè quella 510k',N'972-INT032CS-US - ELITeInGenius SP200 ConsSet 48
972-INT032SP200-US - ELITe InGenius SP 200-48T
972-INT035PCR-US - ELITe InGenius PCR Casset 192',N'GSC
PW
QARA
QM
SAD-OP
SAD-TCO
SBDM',N'Acquisti
Gare
Global Service
Marketing
Ordini
Program Managment
Regolatorio
Sistema qualità',N'Impatto positivo, il prodotto RUO viene sostituito dal codice unico CE/510k
Impatto in termini di attività
Il MKT International e Italia non è coinvolto per i prodotti esclusivi per il mercato US. Nessun impatto e nessuna attività.
Impatto positivo, si semplifica il processo di acquisti uniformando su singoli codici. 
Impatto in termini di attività
da compilare
da compilare
Impatto positivo, il prodotto RUO viene sostituito dal codice unico CE/510k
Impatto positivo, il prodotto RUO viene sostituito dal codice unico CE/510k',N'05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020',N'Archiviazione documentazione relativa alle versioni RUO dei prodotti e aggiornamento dei Fascicoli Tecnici 
Aggiornare le TSB (installazione e PM) dove è elencato il materiale da utilizzare per eseguire queste attività
nessuna attività a carico
•	Creazione nuove anagrafiche in NAV e blocco delle vecchie
•	Inserimento nuovi articoli nelle pricelist di NAV legati alle ICO (principalmente US)
•	Verifica di eventuali ordini clienti aperti
Archiviazione documentazione relativa alle versioni RUO dei prodotti
Creare MOD04,12 (nei quali saranno presenti le date di scadenza per le varie attività)',N'FTPCONS-1 Rev. 03 (dismissione prodotti US) - section_1_product_technical_file_index_xx_-_copia_1.docx
ftp archiviato - cc20-31_aggiornamento_ftp_consumabili_ingenius.msg
la modifica non è significativa, pertanto non è da notifica. Essendo prodotti RUO non è necessaria alcuna notifica all''Autorità Competente in quanto prodotti non registratire. - cc20-31_mod05,36__non_significativa.pdf',N'RAS
QM
GSC
SAD-OP
PW
QAS',N'05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020',N'30/09/2020
15/01/2021',N'16/07/2020
16/07/2020
15/01/2021',N'No',N'No',N'Si',N'da effettuare',N'No',N'no',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'nessuna variazione sul RA del DC12',N'No',N'Essendo prodotti RUO non è necessaria alcuna notifica all''Autorità Competente in quanto prodotti non registrati',N'No',N'no, prodotti non presenti sul MOD05,37',N'No',N'no, dismissione prodotti',N'Renata  Catalano','2021-01-15 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-30','2020-05-19 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'aAgiornamento SDS 89 per l''aggiunta del ref. RTS170ING (SARS-CoV-2) e SDS 27 per l''aggiunta dei ref. CTRXXXPLD-R (Controlli Positivi per Roche)',N'SDS aggiornate per i nuovi prodotti da commercializzare: SARS-CoV-2 (RTS170ING) e Controlli Positivi ELITe-R da utilizzare in associazione al sistema Roche cobas z 480 (CTRXXXPLD-R).',N'Il nuovo prodotto SARS-CoV-2 (RTS170ING) e i nuovi Controlli Positivi Elite-R, da utilizzare in associazione al sistema Roche cobas z 480 (CTRXXXPLD-R), non troverebbero riscontro in nessuna delle SDS in uso.',N'962-CTR015PLD-R - CMV - ELITe Positive Control R
962-CTR020PLD-R - EBV - ELITe Positive Control R
962-CTR031PLD-R - HSV1 - ELITe Positive Control 
962-CTR032PLD-R - HSV2 - ELITe Positive Control 
962-CTR035PLD-R - VZV - ELITe Positive Control R
962-CTR036PLD-R - HHV6 - ELITe Positive Control 
962-CTR078PLD-R - ADV ELITe Positive Control RF
962-CTR175PLD-R - BKV ELITe Positive Control RF',N'QARA',N'Regolatorio',N'Positivo in quanto i nuovi prodotti troverebbero riscontro nelle SDS in uso
Positivo in quanto i nuovi prodotti troverebbero riscontro nelle SDS in uso
Positivo in quanto i nuovi prodotti troverebbero riscontro nelle SDS in uso',N'21/05/2020
21/05/2020
21/05/2020',N'Aggiornamento della SDS 89 e della SDS 27. I documenti rimangono invariati nei contenuti, vengono aggiunti solo i riferimenti ai nuovi codici RTS170ING (SDS 89) e CTRXXXPLD-R (SDS 27)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Il FTP dei prodotti già commercializzati inclusi nelle SDS 89 e 27 non è da aggiornare in quanto non c''è alcuna variazione nei contenuti delle schede e nel FTP non è riportata la revisione in corso dell'' SDS ma solo il numero che rimane invariato.  Il FTP dei prodotti RTS170PLD e CTRXXXPLD-R, non ancora creato, riporterà nei documenti che lo richiedono la SDS 89 e 27 rispettivamente.',N'SDS n89_02 - cc20-30_messa_in_uso_sds_n.89_rev.02_cc20-30.msg
SDS n27_05 - cc20-30__messa_in_uso_sds_n.27,_rev.05_cc20-30.pdf
la modifica non è significativa in quanto non riguarda i contenuti delle SDS, ma solo l''aggiunta di nuovi prodotti, pertanto non è da notificare. - cc20-30__non_significativa.pdf',N'RAS
QARA
QAS
RAS',N'21/05/2020
21/05/2020
25/05/2020',N'03/06/2020',N'22/06/2020
21/05/2020
25/05/2020',N'No',N'NA',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'nessun impatto su RA R&D1.10, normale esecuzione del processo',N'No',N'vedere MOD05,36. La notifica NON è necessaria in quanto i contenuti della SDS non sono modificati, viene solo aggiunto il riferimento di nuovi prodotti.',N'No',N'non necessaria, nessun virtual manufacturer coinvolto ',N'No',N'La documentazione di gestione dei rischi dei prodotti già commercializzati inclusi nelle SDS 89 e 27 non è da modificare in quanto non è stata apportata alcuna variazione nei contenuti della scheda e la documentazione di gestione dei rischi non riporta la revisione in corso della SDS. La documentazione di gestione dei rischi dei prodotti RTS170ING e CTRXXXPLD-R (in parte già prodotta, in parte in corso di creazione) non richiede alcun aggiornamento in quanto non riporta la numerazione e la revisione in corso della SDS. ',N'Michela Boi','2020-06-03 00:00:00',NULL,'2020-06-22 00:00:00',N'No',NULL,NULL),
    (N'20-29','2020-05-18 00:00:00',N'Stefania Brun',N'Documentazione SGQ',N'Adeguamento del QMS ai requisiti del nuovo IVDR "Regolamento (UE) 2017/746 del Parlamento Europeo e del Consiglio del 05 aprile 2017 relativo ai dispositivi medico-diagnostici in vitro..."',N'Obbligatorio ai fini della conformità dei prodotti ai requisiti dell'' IVDR e alla loro sottomissione all''Organismo Notificato per il rilascio dei nuovi certificati CE',N'Impossibilità di completare la procedura di conformità dei prodotti ai requisiti dell''IVDR',NULL,N'QARA',N'Program Managment
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Positivo in quanto permetterà di convertire gli attuali prodotti ai nuovi requisiti dell''IVDR nei tempi prescritti dal Regolamento, ossia entro il 26/05/2022 a fronte di un aumento dei costi per la certificazione dei prodotti in quanto la maggior parte di questi sono classificati in classe C e per questo richiedono l''intervento dell''Organismo Notificato per il rilascio del certificato CE
Positivo in quanto permetterà di convertire gli attuali prodotti ai nuovi requisiti dell''IVDR nei tempi prescritti dal Regolamento, ossia entro il 26/05/2022 a fronte di un aumento dei costi per la certificazione dei prodotti in quanto la maggior parte di questi sono classificati in classe C e per questo richiedono l''intervento dell''Organismo Notificato per il rilascio del certificato CE
Positivo in quanto permetterà di convertire gli attuali prodotti ai nuovi requisiti dell''IVDR nei tempi prescritti dal Regolamento, ossia entro il 26/05/2022 a fronte di un aumento dei costi per la certificazione dei prodotti in quanto',N'05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020',N'Definizione piano di adeguamento dei prodotti attualmente in commercio ai requisiti dell'' IVDR 
Approvazione gap analysis del QMS rispetto ai requisiti dell'' IVDR e piano di adeguamento
Creazione template nuovi documenti e aggiornamento di quelli in uso del Fascicolo Tecnico ai requisiti dell''IVDR quali: Requisiti essenziali, Piano di valutazione delle performance, Piano di validità scientifica, Piano delle performance analitiche, Piano delle performance cliniche e relativi Report; Piano della sorveglianza post-market, Piano di follow-up post-market delle performance e rispettivi report; Sommario della Sicurezza e delle Prestazioni; Dichiarazione di Conformità
Revisione delle IFU dei prodotti in commercio per definizione intended use prodotti secondo l''IVDR
Aggiornamento, verifica e messa in uso della documentazione SGQ
Aggiornamento della documentazione di prodotto e del Fascicolo Tecnico secondo il nuovo regolamento IVDR
12/11/2020: aggiornare MOD04,12 relativo al registro dei prodotti in dism',N'piano di adeguamento dei prodotti attualmente in commercio ai requisiti dell'' IVDR  - ivdr_gantt_chart.xlsx
la modifica è significativa, pertanto è da notificare quando i prodotti dovranno essere riclassificati secondo la nuova classificazione dell''IVDR secondo i requisiti dell''Allegato VIII - cc20-29_mod05,36__significativa.pdf
piano di adeguamento del SGQ ai requisiti dell'' IVDR - ivdr_gap_analysis_qms.xlsx
Gap analysis - ivdr_gap_analysis_qms.xlsx
PR05,01_04_mod18,02 del 19/06/2020 - cc20-29_formazione_pr0501_04_gestione_documenti.msg
IO14,01_00, MOD18,02 del 10/09/2020 - cc20-29_messa_in_uso_io1401_00_cc18-52_cc20-29.msg
pr19,01_rev02 - cc20-29_messa_in_uso_pr1901_rev02.msg
messa in uso LR05,01_rev03 - cc20-29_messa_in_uso_lr0501-xxx_rev03_mod1802_del_12012021.msg
pr03,02 rev01 - cc20-29_messa_in_uso_procedura_offerte_commerciali.msg
PR05,01_06_mod18,02 del 22/03/2021 (introdotta figura del distributore, importatore e mandatario in relazione alla gestione dei documenti relativi ai prodotti) - cc20-29_messa_in_uso_pr0501_rev06_distributore_mandatario_importatore.msg
PR09,01_rev04 - cc20-29_messa_in_uso_pr0901rev04.msg
IO05,01_02 - cc20-29_io0501_gestione_delle_traduzioni_ifu_02.msg
PR14,04_01 E MESSA IN USO NUOVI PIANI E REPORT - cc20-29_messa_in_uso_dcoumenti_sorveglianza_post_market.msg
PR14,03_06; IO04,05_03; IO04,06_04 - cc20-29_messa_in_uso_pr1403_06_io0405_03_io0406_04.msg
PR05,04_03 E mod0507_03;_mod0507a_03 - cc20-29_messa_in_uso_pr0504_03_e_mod0507_03;_mod0507a_03.msg
piano di adeguamento del SGQ ai requisiti dell''IVDR - ivdr_gap_analysis_qms.xlsx',N'QARA
RAS
RDM
VP Molecul
QM
QAS
RAS
RAS
PVC
VP Molecul
PVC
VP Molecul
QM
QAS
RAS
PVS
PVC
RDM
PVS
PVC
RDM
RT
PVS
PVC
PMS
QM
QAS
QAS',N'05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
05/06/2020
09/06/2020
09/06/2020
05/06/2020
05/06/2020
09/06/2020
09/06/2020
05/06/2020
05/06/2020',N'05/06/2020
05/06/2020
30/06/2020
16/05/2020
21/12/2020
16/05/2022
29/12/2021
30/06/2020
16/05/2022
30/06/2020
16/05/2020
16/05/2022
29/12/2021
05/06/2020',N'05/06/2020
05/06/2020
05/06/2020',N'Si',N'Da valutare se si introducono nuove procedure/istruzioni operative ai fini dell''adeguamento dei prodotti',N'No',N'No in quanto l''adeguamento è un requisito di legge',N'Si',N'Si in quanto i prodotti attualmente sul mercato dovranno essere  convertiti ai requisiti dell''IVDR e dovrà essere verificata la loro conformità da parte dell''Organismo Notificato',N'CS - Confezionamento (anche per CQ), Stoccaggio  e Rilascio al cliente del prodotto
ET - Gestione Etichette
IFU - Gestione Manuali
ISW - IQ-OQ-PQ software customizzati
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente
R&D - Svilupppo e commercializzazione prodotti
R&D - Svilupppo e commercializzazione prodotti
ASA - Assistenza applicativa prodotti
ASA - Assistenza applicativa prodotti
DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)
PR - Preparazione Lotto Unico
VAL - Validazione delle estensioni d''uso di prodotto',N'CS14 - stoccaggio prodotto finito in magazzino EG SpA
ET1 - Individuazione dei simboli/dati da inserire sull''etichetta
IFU1 - stesura manuale
ISW5 - Rapporto IQ / OQ / PQ
P1.1 - Pianificazione
P1.10 - Mescolamento materiali e semilavorati
SP13 - Spedizione al cliente da EGSpA
R&D1.8 - Fase di Sviluppo: Reportistica
R&D1.12 - Fase di Preparazione del Rilascio sul Mercato: Attività
ASA3 - Gestione reclamo con impatto sulla vigilanza (FSCA)
ASA5 - Sorveglianza post-produzione
DC13 - Fascicolo tecnico di Prodotto
PR10 - registrazione esito CQ / sblocco del lotto
VAL1 - Fase di pianificazione: pianificazione',N'Valutare se i cambiamenti che verranno apportati alla documentazione R&D e VAL avranno impatto sui sotto-processi R&D1.8  "fase di sviluppo reportisitca" e VAL1 "fase di pianificazione". 
Valutare se l''aggiornamento della documentazione di analisi dei rsichi ha impattoi su R&D1.8, valutare se tale sotto-processo è da segmentare per poter effettuare un''analisi più approfondita.
Valutare se l''aggiornamento delle procedure di immissione in commercio hanno impatto su R&D1.12.
Valutare se i nuovi documenti che verranno prodotti per la sorveglianza post mkt avranno impatto sul sotto-processo di ASA5.  
Valutare l''impatto sul sotto-processo PR10 "registrazine esito CQ e sblocco del lotto" per i prodotti in classe D per i quali ogni nuovo lotto di prodotto deve essere analizzato presso laboratorio di riferimento eu; valutare se questo ha impatto sulla pianificazione delle attività di produzione (P1.1) e sul volume di bulk prodotte (P1.10 mescolamento). 
Valutare se l''aggiornamento del fascicolo tecnico di prodotto avrà impatto sul sotto-processo DC13.
Valutare se l''aggiornamento del PR14,03 impatto sul sotto-processo ASA3.
Valutare se l''inserimento del codice a barre UDI ha impatto sul sotto-processo ET1 (veder CC20-24 specifico per l''UDI).
Valutare se l''aggiornamento della procedura di gestione magazzino e gestione spedizione hanno impatto sui sotto-processi CS14 e SP13.
Valutare se l''aggiornamento delle IFU ha impatto sul sotto-processo IFU1 "stesura IFU".
Valutare se l''aggiornamento della procedura di gestione sw ha impatto sui sotto-processi di convalida sw ISWxx (vedere anche RAC19-33).
Creare un risk-assessment per la gestione delle pubblicazioni.
Valutare se gli obblighi dell''importatore e del distributore  hanno impatto sui risk assessment relativi alla valutazione e selezione del fornitore/distributore, agli acquisti ed al controllo al ricevimento.',N'Si',N'Si in quanto i prodotti dovranno essere riclassificati secondo la nuova classificazione dell''IVDR secondo i requisiti dell''Allegato VIII',N'Si',N'Si per i prodotti OEM per R-Biopharm',N'Si',N'la documentazione di gestione dei rischi di prodotto andrà aggiornata per allinearsi ai requisiti IVDR',N'Sergio  Fazari','2022-05-16 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-28','2020-05-04 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Aggiunta del fornitore "VWR International S.r.l." (codice fornitore F01813) alla specifica 950-018_Na Blu Bromofenolo',N'Necessità di avere un secondo fornitore ("VWR International S.r.l.") per l'' approvvigionamento della materia prima 950-018. L'' unico fornitore presente attualmente in specifica (Merck Life Science, F01825) ha problemi di produzione della materia prima e non è in grado di fornirci il materiale prima di 3 mesi dalla data dell'' ordine. ',N'Le tempistiche di consegna risultano critiche per la produzione dei nostri kit e se il materiale non venisse acquistato dal fornitore VWR avremmo problemi nella produzione del kit EPH.',NULL,N'MM
PW
RDM',N'Acquisti
Produzione
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo in quanto è presente un doppio fornitore di materia prima.
Impatto positivo in quanto non solo la materia prima 950-018 avrà due fornitori da cui sarà possibile approvvigionarsi, ma il nuovo fornitore "VWR International S.r.l." ha mediamente tempi più celeri di spedizione rispetto al fornitore attuale.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Impatto positivo in quanto non solo la materia prima 950-018 avrà due fornitori da cui sarà possibile approvvigionarsi, ma il nuovo fornitore "VWR International S.r.l." ha mediamente tempi più celeri di spedizione rispetto al fornitore attuale.',N'04/05/2020
04/05/2020
07/05/2020
07/05/2020',N'Aggiornamento SPEC950-018 e formazione
approvazione specifica 
messa in uso specifica
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
segnalazione al CQ del primo lotto prodotto con la materia prima acquista da VWR e comunicazione dell''esito del cq da allegare a questo cc',N'SPEC950-018_01 - cc20-28_messa_in_usoi_spec950-018_01_cc20-28.msg
SPEC950-018_01 - cc20-28_messa_in_usoi_spec950-018_01_cc20-28_1.msg
SPEC950-018_01 - cc20-28_messa_in_usoi_spec950-018_01_cc20-28_2.msg
la modifica non è significativia - cc20-28_mod05,36__non_significativa.pdf',N'RT
PW
QAS
QARA
QAS
PP',N'04/05/2020
04/05/2020
07/05/2020
07/05/2020',N'29/05/2020
29/05/2020
29/05/2020
29/05/2020',N'26/05/2020
26/05/2020
26/05/2020
26/05/2020',N'No',N'nessun impatto, il DMRI non deve essere aggiornato se introdotto un nuovo fornitore',N'No',N'non necessaria',N'No',N'nesssun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'il risk assessment A2 non subisce impatto, l''aggiornamento riguarda l''integrazione di un secondo fornitore di materia prima',N'No',N'vedere MOD05,36. Il fornitore qualificato in uso non viene sostituito, ma affiancato da un secondo fornitore per ridurre il rischio di mancate forniture, si rtiene pertanto una modifica non significativa.',N'No',N'nessun impatto, le specifiche di materia prima non sono inviate al virtual manufacturer',N'No',N'nessun impatto.',NULL,'2020-05-29 00:00:00',NULL,'2020-05-26 00:00:00',N'No',NULL,NULL),
    (N'20-27','2020-04-21 00:00:00',N'Ferdinando Fiorini',N'Documentazione SGQ',N'Creazione e adozione di un nuovo script per il Dispensatore, codice interno DA9, per il volume di 1,2 mL (OneStep1200.med) legato allo sviluppo del nuovo prodotto SARS-CoV-2, codice interno  962-RTS170ING.',N'Possibilità di utilizzare il Dispensatore Automatico DA9 per dispensare il nuovo prodotto SARS-CoV-2',N'Impossibilità di produrre i volumi richiesti per questo prodotto SARS-CoV-2',NULL,N'MM
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Impatto positivo, possibilità di utilizzare il Dispensatore Automatico (DA9) per dispensare il nuovo prodotto SARS-CoV-2
Impatto positivo, possibilità di utilizzare il Dispensatore Automatico (DA9) per dispensare il nuovo prodotto SARS-CoV-2
Impatto positivo, possibilità di utilizzare il Dispensatore Automatico (DA9) per dispensare il nuovo prodotto SARS-CoV-2
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività',N'29/04/2020
29/04/2020
29/04/2020
28/04/2020
28/04/2020
28/04/2020
28/04/2020',N'Chiedere al fornitore Hamilton la creazione del nuovo script "OneStep1200.med" per il volume di 1,2 mL
Verificare il nuovo script attraverso simulazione del MOD11,29
Aggiornamento della specifica d''uso del DA9 con il nuovo script (SPEC-USO-DA9 rev01)
Adozione del nuovo Script nel Modulo di Produzione del nuovo prodotto 962-RTS170ING, aggiornamento del MOD11,29
Messa in uso della documentazione e del nuovo script
messa in uso documenti',N'script onestep1200 rev00 - cc20-27__messa_in_uso_spec-uso-da9_02_e_script_onestep1200_e_one_step290_1.pdf
MOD11,29_02 - cc20-27_messa_in_uso_mod11,29_02_cc20-27,_rac20-5_mod18,02_del_27_04_2020_1.pdf
aggiunto script one 1200 nel MOD956-RTS170ING rev. LP1 del 21/04/2020 - cc20-27_aggiuntascriptonestep1200_mod956-rts170ing_lp1.docx
ONESTEP1200, VERIFICA REGISTRATA SUL MOD11,29, rev00, FIRMATO DA MM IL 29/04/2020 ING290, VERIFICA REGISTRATA SUL MOD11,29, rev00, FIRMATO DA MM IL 30/04/2020 - cc20-27_verifica_script.txt
SPEC-USO-DA9_rev02 del 30/04/2020, in uso il 04/05/2020 - cc20-27__messa_in_uso_spec-uso-da9_02_e_script_onestep1200_e_one_step290_2.pdf
script onestep1200 e script ing290 rev00 - cc20-27__messa_in_uso_spec-uso-da9_02_e_script_onestep1200_e_one_step290.pdf
MOD11,29_02 - cc20-27_messa_in_uso_mod11,29_02_cc20-27,_rac20-5_mod18,02_del_27_04_2020.pdf
SPEC-USO-DA9_rev02 del 30/04/2020, in uso il 04/05/2020 e scrip onestep1200 e ing290 rev00 - cc20-27__messa_in_uso_spec-uso-da9_02_e_script_onestep1200_e_one_step290_3.pdf',N'MM
QC
PT
PT
QAS
QAS',N'29/04/2020
29/04/2020
29/04/2020
28/04/2020
28/04/2020
28/04/2020',N'22/04/2020
27/04/2020
04/05/2020
27/04/2020
08/05/2020
04/05/2020',N'04/05/2020
04/05/2020
04/05/2020
04/05/2020
04/05/2020',N'No',N'prodotto in corso di sviluppo',N'No',N'il prodotto non è ancora in commercio',N'No',N'il prodotto non è ancora in commercio',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
ST - Elenco famiglie Strumenti',N'P1.14 - Misurazione e dispensazione semilavorati
ST16 - Disepnsatori Automatici (DA)',N'nessun impatto sui risk assessment P1.14 e ST16',N'No',N'il prodotto non è ancora in commercio',N'No',N'nessun virtual manufacturer',N'No',N'prodotto in corso di sviluppo',NULL,'2020-05-04 00:00:00',NULL,'2020-05-04 00:00:00',N'No',NULL,NULL),
    (N'20-26','2020-04-08 00:00:00',N'Clelia  Ramello',N'Etichette
Manuale di istruzioni per l''uso',N'Per il prodotto ELITe InGenius® SP 200 Consumable Set, codice INT032CS e INT032CS-US, si chiede di eliminare il componente "Sonication tube cap" (tappi utilizzati per sigillare i Sonication Tubes durante il processo di sonicazione). I tubi di sonoicazione con i tappi associati faranno parte di un nuovo kit il cui codice sarà, INT032SON.',N'Eliminare questo componente permetterà al fornitore di produrre più velocemente il consumable set',N'Tempi più lunghi per il riordino (dovuti a tempi più lunghi di produzione)',N'972-INT032CS - ELITeInGenius SP200 ConsSet 48
972-INT032CS-US - ELITeInGenius SP200 ConsSet 48',N'GSC
QARA
QM
GASC
MS
QAS
RAS',N'Assistenza applicativa
Marketing
Program Managment
Sistema qualità',N'Impatto positivo, la maggior parte dei clienti non effettua la sonicazione, non ha quindi necessità di avere i tappi utilizzati per sigillare i Sonication Tubes durante il processo di sonicazione, che dovrebbe poi smaltire.
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività
Impatto in termini di attività
Impatto positivo, la maggior parte dei clienti non effettua la sonicazione, non ha quindi necessità di avere i tappi utilizzati per sigillare i Sonication Tubes durante il processo di sonicazione, che dovrebbe poi smaltire. 
Durante l''Emergenza COVID-19 però, l''uso dei tappi è largamente utilizzato nel trasporto dei tubi dalle cappe di classe II. Ci potrebbe quindi in questo caso essere un''impatto da non sottostimare nella necessità di approvvigionare di una soluzione adeguata ed evitare di raddoppiare il numero di tubi che forniamo con il Consumable set e in aggiunta con il pacchetto di tubi più tappi.
Il codice per il riordino dei Sonication tu',N'13/05/2020
13/05/2020
16/04/2020
16/04/2020
16/04/2020
07/05/2020
07/05/2020',N'Aggiornamento documentazione di marketing con il nuovo codice di riordino dei sonication tube&cap (codice  INT032SON) e per la modifica al codice INT032CS e INT032CS-US consumable set che onn conterrà i tappi, quindi non permetterà la sonicazione.
Supervisionare le attività documentali di PSS e collaborare per la chiusura del fascicolo tecnico 
Aggiornamento del consumable set (INT032CS), kit che non permetterà la sonicazione:
Aggiornamento etichette (MOD04,13)
Aggioramento IFU e traduzioni IFU, compreso un check delle IFU relative ai kit di estrazione
Compilazione del MOD05,36 e notifica preliminare all''autorità competente
Check lotti di produzione
Aggiornamento del fascicolo tecnico
Aggiornamento registrazione al MInistero

Per il nuovo codice di INT032SON, kit che permetterà la sonicazione:
MOD04,13
nuove IFU e traduzioni, verifica IFU correlate
Check lotti di produzione
Aggiornamento dichiarazione di conformità per citare il nuovo codice
Registrazione alle autorità
Creazione del ',N'materiale mkt aggiornato - cc20-26_emd-elite_ingenius_menu_-490-2020-01_en.pdf
Notifica alle autorità competenti dei paesi in cui il prodotto è registrato - cc20-26_prior_notification_of_change_-int032cs-_cc20-26.msg
La modifica è significativa, pertanto da notificare - cc20-26_mod05,36_significativa.pdf
INT032CS	ELITe InGenius SP200 Consumable Set	Rev. 03 (English, Italian) INT032SP200	ELITe InGenius SP200	Rev. 08 (English, Italian)  INT033SP1000	ELITe InGenius SP1000	Rev. 03 (English, Italian) - cc20-26_ifu_int032cs_int032sp200_and_int033sp1000_cc20-26.msg
FTPCONS-1 Rev. 03 - section_1_product_technical_file_index_xx_-_copia.docx
etichette prodorro INT032CS, kit che non permetterà la sonicazione: - product_label_sp200_cons._set_rev05.pdf
etichetta prodotto INT032SON, kit che permetterà la sonicazione - mod0413int032son.pdf
IFU ELITe InGenius Sonication tubes, INT32SON rev00 - cc20-26_market_release_-_elite_ingenius_sonication_tubes_int032son.msg
FTP INT032SON aggiornato - cc20-26_ftpaggiornato.msg
Registrazione al ministero INT032SON - cc20-26_int032son_ricevuta_ministero.txt
notifica di esecuzione delle modificamodifica  - cc20-26__prior_notification_of_change_-int032cs-_cc20-26.msg
TSB 062 - Rev A  - cc20-26_tsb_release_tsb_062_-_rev_a_-_elimination_sonication_tube_cap_from_the_consumable_set.msg
IFU MRSA/SA ELITe MGB Kit REV.09 - cc20-26__qualita_sgq_ifu_m800351_rev.09_cc20-26.msg',N'MS
SIS
QARA
QM
QAS
RAS
GPS
RAS',N'13/05/2020
13/05/2020
16/04/2020
16/04/2020
07/05/2020',N'07/05/2020',N'30/07/2020
30/09/2020
30/09/2020
19/10/2020
17/07/2020',N'Si',N'da creare per il nuovo codice',N'Si',N'attraverso l''avvertenza presente sul IFU',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'Si',N'MOD05,36. il cambiamento del prodotto deve essere comunicato alle autorità competenti, varia l''uso previsto del ki INT032CS: senza i tappi non è possibile effettuare la sonicazione. Secondo le linee guida FDA "Deciding when to submit a 510(k) ..." non è necessario sottomettere un nuovo 510(k).',N'No',N'nessun virtual manufaturer',N'Si',N'secondo PR05,02 per il nuovo codice',N'Clelia  Ramello','2020-05-07 00:00:00',NULL,'2020-10-19 00:00:00',N'No',NULL,NULL),
    (N'20-25','2020-03-31 00:00:00',N'Cinzia Bittoto',N'Manuale di istruzioni per l''uso',N'Modifica dei valori di "Threshold" e "Noiseband" dei detector FAM e VIC da impostare nel corso della procedura analitica dei prodotti «CMV ELITe MGB Kit» (RTK015PLD) e «EBV ELITe MGB Kit» (RTS020PLD) quando utilizzati in associazione alla piattaforma di amplificazione real time «cobas® z 480 Analyzer» (Roche) e a campioni di sangue intero. La modifica di tali valori è la seguente: da 0,55 a 0,8 per il detector FAM e da 1,2 a 1,5 per il detector VIC.
Inoltre verranno effettuati per entrambi i target test per valutare ulteriori performance analitiche rispetto a quelle valutate nel corso dell''estensione d''uso in associazione al sistema Roche; per l''analisi dei risultati ottenuti verranno utilizzati, laddove applicabili, i nuovi valori di "Threshold" e "Noiseband" definiti.',N'La modifica dei valori di "Threshold" e "Noiseband" dei detector FAM e VIC per i prodotti RTK015PLD e RTS020PLD consente di avere valori allineati a quelli definiti per altri saggi ELITe MGB validati per la medesima applicazione (sistema Roche, matrice sangue intero). La valutazione di performance analitiche aggiuntive consente di avere a disposizione dati più completi per i prodotti in oggetto e relativamente alle applicazioni sul sistema Roche.',N'Disomogeneità fra i diversi prodotti ELITe MGB nei parametri utilizzati per l''analisi di campioni di sangue in associazione al sistema Roche.
Minor completezza dei dati di prestazioni analitiche disponibili relativi ai prodotti RTK015PLD e RTS020PLD in associazione al sistema Roche.',N'962-RTK015PLD. - CMV ELITe MGB Kit 100T
962-RTS020PLD. - EBV ELITe MGB KIT 100T',N'QARA
RDM
PVC',N'Marketing
Regolatorio
Ricerca e sviluppo
Validazioni',N'Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatto: positivo, maggior completezza dei dati disponibili per CMV e EBV rispetto alle estensioni d''uso effettuate in associazione al sistema Roche
Impatti: nessun impatto se non in termini di attività da svolgere per la validazione del cambiamento
Impatto positi',N'03/04/2020
03/04/2020
03/04/2020
03/04/2020
03/04/2020
03/04/2020
14/04/2020
03/04/2020
03/04/2020
03/04/2020',N'Rianalisi dei dati relativi alle prestazioni diagnostiche ottenute con i prodotti RTK015PLD e RTS020PLD validati in associazione al sistema Roche e a campioni di sangue. La rianalisi verrà effettuata utilizzando i nuovi valori di "Threshold" e "Noiseband" dei detector FAM e VIC definiti
-	Per ciascun prodotto, stesura di un nuovo Report per riportare i dati di performance analitiche ottenuti, aggiornamento del Report di Validazione per riportare i risultati dell''analisi condotta applicando i nuovi valori soglia, aggiornamento del Summary Verification and Validation Report per riportare tutti i risultati ottenuti.
Aggiornamento del manuale di istruzioni per l''uso dei prodotti RTK015PLD e RTS020PLD per riportare i nuovi valori soglia definiti e le nuove performance ottenute.
verifica e approvazione report e IFU
aggiornamento formazione al team di supporto tecnico, marketing e Gestione reclami
Esecuzione di test per la definizione delle performance analitiche aggiuntive per i prodotti RTK015PLD e RTS0',N'reprt EBV REP2020-023 Rev.00 27.04.2020 REP2018-106 Rev.01 27.04.2020 REP2018-107 Rev.01 27.04.2020  - cc20-25_report_validazioni.txt
- REP2018-103 Re.01 - REP2018-104 Re.01 - REP2018-168 Re.01. Data di firma 17/06/2020. - cc20-25_cmv_report_cc20-25.msg
report ebv REP2020-023 Rev.00 27.04.2020 REP2018-106 Rev.01 27.04.2020 REP2018-107 Rev.01 27.04.2020  - cc20-25_report_validazioni_1.txt
- REP2018-103 Re.01 - REP2018-104 Re.01 - REP2018-168 Re.01. firma il 17/06/2020. - cc20-25_cmv_report_cc20-25_1.msg
RTS020PLD_17, CTR020PLD_14 - cc20-25_ifu_rts020pld_ctr020pld_rac19-32;_cc20-25.msg
RTK015PLD_18 - cc20-25_ifu_rtk015pld.msg
MOD18,02 RTK015PLD DEL 26/02/2020, MOD18,02 RTK020PLD DEL 29/04/2020 - cc20-25_formazione_1.txt
ifu rts020pld,ebv, rev17 in uso, report ebv revisionati - cc20-25_ifu_rts020pld_ctr020pld_rac19-32;_cc20-25_1.msg
ifu rtk015pld, cmv rev18 in uso, report cmv revisionati - cc20-25_ifu_rtk015pld_1.msg
 ifu rts020pld,ebv, rev17 in uso, report ebv revisionati  - cc20-25_ifu_rts020pld_ctr020pld_rac19-32;_cc20-25_2.msg
ifu rtk015pld, cmv rev18 in uso, report cmv revisionati - cc20-25_ifu_rtk015pld_2.msg
La modifica è significativa, pertanto da comunicare a DEKRA ed alle autorità competenti - cc20-25_mod05,36_significativa.pdf
mail dekra  11/05/2020 - cc20-25_mail_noc_dekra_cmv.pdf
notice of change DEKRA del 29/04/2020 - cc20-25_noc_eg_spa_cmv_20-25.pdf
invio notifica distributori RTS020PLD_IFU rev17 - cc20-25_egspa_sgq_prior_notification_of_change_-rts020pld.msg
notifica preliminare RTK015PLD - cc20-25_notifica__prior_notification_of_change_-rtk015pld_-_cc20-25.pdf
notifica a modifica  effettuata di RTK015PLD alle autorità competenti - cc20-25_notifica_della_modifica_effettuata.msg
notifica a modifica effettuata di RTK015PLD a DEKRA - cc20-25_chiusura_notice_of_change.msg
FTP EBV aggiornato - cc20-25_aggiornamento_ftp.txt
Aggiornamento del FTP CMV in data 24/07/2020 (su cartaceo) della copertina, aggiungendo i Report REP2018-168, REP2018-103 e REP2018-104 Rev.01 del 17/06/2020 - cc20-25_aggiornamento_ftp_cmv_pleiadi.msg
sito web aggiornato per cmv su cobas - cc20-25_aggiornamentositoweb.docx',N'PVS
PVS
PVS
PVS
RDM
RAS
RAS
RAS
MS
PVS',N'03/04/2020
03/04/2020
03/04/2020
03/04/2020
03/04/2020
14/04/2020
03/04/2020
03/04/2020
03/04/2020
13/11/2020',N'10/04/2020
29/05/2020
29/05/2020
29/05/2020
29/05/2020
29/05/2020
29/05/2020
29/06/2020
29/06/2020',N'19/06/2020
19/06/2020
06/08/2020
29/04/2020
19/06/2020
06/08/2020
06/08/2020
01/09/2020
24/07/2020
13/11/2020',N'No',N'nessun impatto',N'Si',N'si attraverso l''avvertenza presente nel IFU',N'No',N'Nessun impatto. Omogeneità delle impostazioni dei parametri di analisi dei prodotti RTK015PLD e RTS020PLD rispetto agli altri prodotti ELITe MGB validati sul sistema Roche, maggior numero di dati analitici a disposizione',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL1 - Fase di pianificazione: pianificazione',N'nessun impatto su VAL1',N'Si',N'vedere MOD05,36. ',N'No',N'Sebbene il prodotto RTK015PLD sia contenuto nel MOD05,37, la notifica all''azienda R-BIOPHARM non verrà effettuata in quanto la modifica ritenuta significativa è relativa alla variazione di parametri di analisi impostati quando il prodotto ELITe MGB è utilizzato in associazione allo strumento real time «cobas® z 480 Analyzer» (Roche), differente da quello validato da R-BIOPHARM con il prodotto RIDAGENE',N'No',N'La modifica delle soglie di analisi e il calcolo di nuove performance analitiche non determinano l''introduzione di nuovi rischi ma potrebbero potenzialmente modificare la stima e la valutazione dei rischi già considerati nella documentazione corrente. I risultati ottenuti dai test analitici e dalla rianalisi dei test diagnostici hanno però confermato la stima e valutazione dei rischi in essere e dunque non è necessario l''aggiornamento della documentazione di gestione dei rischi.',N'Cinzia Bittoto','2020-06-29 00:00:00',NULL,'2020-11-13 00:00:00',N'No',NULL,NULL),
    (N'20-24','2020-03-30 00:00:00',N'Stefania Brun',N'Etichette',N'A seguito dell''entrata in vigore dell''UDI all''interno del Regolamento (Medical Device Act) IVD di KFDA per i dispositivi IVD registrati in Sout Korea previsto per Luglio 2020, per i prodotti appartenenti alla classe 3 è necessario adeguare le etichette esterne dei prodotti registrati in South Korea al fine di adeguare il barcode lineare e/o il QR code ai requisiti del regolamento.',N'L''adeguamento è obbligatorio per i dispositivi di classe 3 quindi necessario per continuare a commercializzare in South Korea.
L''introduzione dell''UDI su tutte le etichette esterne dei prodotti commercializzati (ad esclusione della linea ALERT e NESTED in dismissione) premetterà di essere conformi al nuovo regolamento IVDR.',N'Interdizione della commercializzazione dei dispositivi di classe 3 registrati in South Korea a partire da Luglio 2020.',NULL,N'ITT
QARA
RAS',N'Regolatorio
Ricerca e sviluppo
Sistema informatico
Sistema qualità',N'Impatto in termini di attività e valutazione che la modifica non comporti problemi di compatibilità con altri usi del barcode (es. utilizzo durante le fasi di prelievo e spedizione prodotti)
Impatto in termini di attività in quanto la modifica dei template comporta la rivalidazione del processo di stampa etichette. 
Impatto in termini di attività in quanto la modifica dei template comporta la rivalidazione del processo di stampa etichette. 
Impatto in termini di attività in quanto la modifica dei template comporta la rivalidazione del processo di stampa etichette. 
Positivo in quanto permette di adeguare l''etichettatura dei prodotti ai requisiti del regolamento KFDA e in questo modo essere già conformi anche al IVDR per quanto riguarda l''etichetta
Positivo in quanto permette di adeguare l''etichettatura dei prodotti ai requisiti del regolamento KFDA e in questo modo essere già conformi anche al IVDR per quanto riguarda l''etichetta
Positivo in quanto permette di adeguare l''etichettatura dei p',N'27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020',N'Valutare modifica dei template delle etichette in modo da adeguare il codice a barre ai requisiti UDI. 
Vedere i 2 esempi allegati:  codice a barre lineare o codice a bare QR-code. Valutare l''applicabilità dei codici a barre anche sulla base del seriale utilizzato nel QR-code per la gestione del magazzino interno.
Valutare la possibilità di creare template nuovi.
I template nuovi o aggiornati dovranno essere pronti entro il 20/05/2020,  a seguire l''aggiornamento degli altri template come da file allegato.
ATTENZIONE: aggiornare il sito web per scaricare le IFU (vedere anche CC20-18)
Rivalidazione del processo di stampa etichette attraverso stesura di un piano e report, compilazione dei moduli di approvazione template (MOD04,31) e etichette prodotti (MOD04,13).
Piano validazione entro il 30/04/2020: considerare l''aggiornamento di tutte le etichette con l''UDI, ma specificare che l''approvazione avverrà in più tranche: le etichette dei targhet richiesti da Osang sono da rilascaire entro il 15/06/2020, ',N'esempi codici a bare per UDI - cc20-24_esempi_udi_lineare_o_qrcode.docx
Aggiornamento ALL1-IO04,06 con i nuovi template 58, 59, 60 contenenti il nuovo codice a barre UDI - cc20-24_messa_in_uso_all-1_io0406_template_etichette_in_uso_cc20-24.msg
piano validazione - mod09,24_00_pianodivalidazionestampaetichetteudi.pdf
report validazione - mod09,12_01_rapportovalidazionestampaetichetteudi.pdf
la modifica non è significativa. è, tuttavia, necessario comuincare la modifica delle etichette come aggiornamento della documentazione inviata per la registrazione dei prodotti, alle autorità competenti ed a DEKRA. - cc20-24_mod05,36__non_significativa.pdf
comunicazione UDI prodotti a Osang - r_osang_healthcare_starting_the_korea_udi_system.msg
per i prodotti EGSpA non è necessario compilare il MOD05,33, in quanto si utilizano gli EAN13, è perciò sufficiente fare riferimento all''elenco MOD05,13. - cc20-24_mod05,33_non_utilizzato_per_i_prodotti_egspa,_ma_solo_per_ingenius.msg
sw galaxi versione 2.1.4 - sgq_messa_in_uso_elite_galaxy_software_vers_2.1.4_cc20-24.msg
Notifica sulla registrazione delle nuove etichette con UDI nei paesi presso cui i prodotti della prima tanche sono notificati.  - cc20-24_prior_notification_of_change_-rtxxxxpld_ctrxxxpld_stdxxxpld-_cc20-24.msg
Notifica a Dekra sull''aggiornamento delle etichette per l''inserimento del codice UDI - cc20-24_mail_noc_dekra.pdf
etichette da modificare pima tranche - cc20-24_etichette_da_modificare_con_template_prima_tranche.xlsx',N'ITT
QM
QAS
QARA
RAS
QM
RDM
RT
RAS
QARA
QM
QAS',N'27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020',N'20/05/2020
15/06/2021
15/06/2021
15/06/2020
30/04/2020',N'16/07/2020
30/07/2020
16/07/2020
31/03/2020
21/05/2020
08/06/2020',N'No',N'No in quanto la revisione dell''etichetta non è specificata all''interno del DMRI',N'Si',N'Si da valutare in quali paesi è richiesta la notifica della modifica',N'No',N'No in quanto il codice UDI conterrà le stesse informazioni disponibili oggi all''interno del codice a barre lineare',N'ET - Gestione Etichette',N'ET2 - Stesura dell''etichetta',N'no',N'Si',N'Secondo le linee guida relative all''analisi della significatività della modifica (NBOG BPG 2014-3, paragrafo "Changes to labeling" e "FDA deciding when to submit a 510 (K) for a change to an existing device" l''aggiunta di un codice a barre non è una modifica significativa da notificare alle autorità competenti. 
Comuincarlo come aggiornamento della documentazione inviata per la registrazione dei prodotti, alle autorità competenti ed a DEKRA.',N'Si',N'Si per i prodotti con Virtual Manufacturer R-Biopharm',N'No',N'No in quanto la modifica non ha impatti sull''analisi dei rischi',N'Stefania Brun','2021-06-15 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-23','2020-03-23 00:00:00',N'Alessandra Vinelli',N'Documentazione SGQ
Dismissione prodotto',N'Dismissione prodotti linea ALERT:
1) prima tranche di 23 codici, come da comunicazione del 15/01/2020 allegata, NON sarnno più disponibile a partire dall'' 1/7/2020: 
RTK015
STD015
RTS020
CTR02
STD020
RTS031
CTR03
STD031
RTS032
CTR032
STD032
RTS035
CTR035
STD035
RTS036
CTR036
STD036
RTS098
CTR098
RTSG07-210
STDG07-210
RTSG07-190
STDG07-190

2) seconda tranche di 23 codici, come da comunicazione del 21/04/2020, NON sarnno più disponibile a partire dall'' 1/7/2020: 
CTR074
CTR079
CTR094
CTR096
CTR097
CTRD01-II
CTRD01-V
CTRD08
CTRD13
CTRD14
RTS074-M
RTS079-M
RTS094-M
RTS096-M
RTS097-M
RTSD01-II-M
RTSD01-V-M
RTSD08-M
RTSD13-M
RTSD14-M
RTS000
RTS001
RTS002

3) La dismissione della linea alert era già iniziata nel 2018 con i seguenti change control:
- CC18-32 che prevedeva la dismissione di RTS038M, CTR038, STD038, STD098, EXTG01, 
- CC18-33 che prevedeva la dismissione di RTS076MR, CTR076MR, STD076MR; RTS176MR, CTR176MR, STD176MR; RTST01MR (RUO), CTRT01MR
- CC19-14 che prevedeva la dismissione di  RTS076-M, CTR076, STD076, RTS110-M, STD110',N'Nell''ottica di un continuo miglioramento dei prodotti e con l''intenzione di perseguire costantemente soluzioni tecnologiche sempre più efficaci e in linea con lo stato dell''arte della diagnostica molecolare, la produzione di tutti i kit basati sulla tecnologia ALERT verrà cessata.',N'Non conformità dei prodotti della linea ALERT al nuovo IVDR, mantenimento sul mercato di prodotti tecnologicamente superati. ',NULL,N'PP
QARA
SAD-OP
SM',N'Acquisti
Assistenza applicativa
Controllo qualità
Gare
Marketing
Ordini
Produzione
Regolatorio
Sistema qualità',N'Impatto positivo. 
Impatto positivo. I prodotti ALERT verranno sostituiti dai prodotti ELITe MGB,  tecnologicamente più avanzati e validati su  ELITe InGenius. Dal 2019 i prodotti ALERT non sono stati più promossi attivamente. La dismissione dei codici ALERT ridurrà anche il lavoro per la post market surveillance.
Impatto positivo. I prodotti ALERT verranno sostituiti dai prodotti ELITe MGB,  tecnologicamente più avanzati e validati su  ELITe InGenius. Dal 2019 i prodotti ALERT non sono stati più promossi attivamente. La dismissione dei codici ALERT ridurrà anche il lavoro per la post market surveillance.
Impatto positivo: ottimizzazione dei costi, dei tempi e degli spazi produttivi.
Impatto positivo: ottimizzazione dei costi, dei tempi e degli spazi produttivi.
Impatto positivo: ottimizzazione dei costi, dei tempi e degli spazi produttivi.
Impatto positivo: eliminazione di prodotti non adeguati al nuovo IVDR. I prodotti che si intende dismettere possono essere sostituiti dall''analogo prodot',N'23/04/2020
23/04/2020
23/04/2020
23/04/2020
23/04/2020
03/04/2020
03/04/2020
03/04/2020
03/04/2020
23/04/2020
23/04/2020
28/04/2020
28/04/2020
27/04/2020
23/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020
27/04/2020',N'Inviata comunicazione ai clienti italiani ed esteri relativamente alle I tranche di prodotti
Inviare comunicazione alle agenzie, KAM e FPS (Italia)
Per i prodotti alert cui  non esiste il corrispettivo EGSpA tecnologiacamente più avanzata, valutare prodotti di terze analoghi da acquistare da fornitori quaòificati.
Aggiornamento dei documenti di pianificazione, in accordo con il reparto acquisiti e cq.
fornire a QAS gli ultimi lotti prodotti e rispettva data di scadenza.
valutazione dei documenti generici (per es. script dispensatori automatici) da archiviare
1/3/2021 aggiornare IO09,08
Aggiornamento del MOD04,12: trasferire i dati registrati da file word a file excel per poter mantenere lo scadenziario delle attività (es. data scadenza ultimo lotto prodotto)
Compilazione nel corso del tempo del MOD04,12, secondo le scadenze registrate
Effettuare comunicazione a DEKRA per i seguenti prodotti RTS097-M, CTR097, RTS098-M, CTR098, RTK015, STD015.
Ritiro del certificato CE a scadenza dell''ultim',N'Comunicazione alle Agenzie, KAM, FPS Italia - egspa_marketing_dismissione_prodotti_linea_alert.msg
Comunicazione ai clienti ITALIA - 2020_01_15_lettera_dismissione_alert_it.docx
Allegato alla comunicazione ai clienti ITALIA - 2020_01_15_annex_lettera_dismissione_alert_it.docx
Comunicazione ai clienti estero - 2020_01_15_alert_discontinuation_letter_en.docx
Allegato alla comunicazione dei clienti esteri - 2020_01_15_annex_to_alert_discontinuation_letter_en.docx
Lettera di dismissione Italia - 2020_01_15_lettera_dismissione_alert_it_1.docx
Allegato alla lettera di dismissione Italia - 2020_01_15_annex_lettera_dismissione_alert_it_1.docx
Lettera di dismissione estero - 2020_01_15_alert_discontinuation_letter_en_1.docx
Allegato alla lettera di dismissione estero - 2020_01_15_annex_to_alert_discontinuation_letter_en_1.docx
MOD04,12 rev03 - cc20-23_messa_in_uso_mod0412__03_cc20-23.msg
IO09,18_01 - cc20-23_messa_in_uso_i0918_01_mod1802_19052020.msg
ARCHVIAZIONE SPEC950-XX E SPEC951-XX - cc20-23_archiviazione_spec950-xx_e_spec951-xx_cc20-23_e_cc20-7.msg
ARCHIVIAZIONE MOD962-RTSXX-M, MOD962-CTRXX, MOD962-STDXX; CARTACEO ARCHIVIATO. ARCHIVIATI ANCHE GLI OBSOLETI MOD961-XXX - cc20-23_documentidaarchiviare.xlsx
IO0904_02_IO0914_07 - cc20-23_io0904_02_io0914_07.msg
esempio  mail dismissione alert I tranche IT e Estero - cc20-23_es_mail_it_dismissione_prodotti_alert_i_tranche.msg
allegato lettrea dismissione I tranche - 2020_01_15_alert_discontinuation_letter_en_1.docx
 allegato lettrea dismissione I tranche, inglese - 2020_01_15_alert_discontinuation_letter_en_1_1.docx
mail dismissione II tranche IT - cc20-23_mail_-_dismissione_prodotti_alert_ii_tranche.pdf
mail dismissione II tranche EN - cc20-23_mail_-_dismissione_prodotti_alert_ii_tranche_estero.pdf
 allegato lettera dismissione II tranche  - cc20-23_2020-04-21_annex_lettera_dismissione_alert_1.pdf
Nell''allegato sono presenti i prodotti di terze parti in sostituzione, laddove non è presente un prodotto tecnoclogiacamente piùà avanzato EGSpA. In particolare i prodotti di terze parti sostituiscono i seguenti prodotti ALERT e loro accessori CTR: RTSD01-II, RTSD01-V, RTSD08, RTSD13 e RTSD14 - cc20-23_2020-04-21_annex_lettera_dismissione_alert.pdf
omunicazione a DEKRA per i seguenti prodotti RTS097-M, CTR097, RTS098-M, CTR098, RTK015, STD015 - cc20-23_notice_of_change_product_discontinuation.msg
la modifica è significativa, pertanto è da notificare. - cc20-23_mod05,36_significativa.pdf
per il CQ di EXTR01 devono rimanere in produzione RTS076M, STD076 (in scadenza), RTS000 (si dovrà acquistare solo per CQ) e BRK200. per il CQ di BRK200 devono rimanere in produzione RTS076M, STD076 (in scadenza) e RTS000 (si dovrà acquistare solo per CQ).  - cc20-23_alertprodottiperusocq.msg
IO0904_02_IO0914_07 - cc20-23_io0904_02_io0914_07_1.msg
IO10,06_07 - cc20-23_messa_in_uso_io1006_07.msg',N'PMI
SBDM
QARA
QARA
QAS
PP
QAS
QM
PW
SAD-OP
GASC
SAD-TCO
PMI
PP
QARA
RAS
MM
QC
QC
QC
QC
QC',N'23/04/2020
23/04/2020
23/04/2020
23/04/2020
23/04/2020
03/04/2020
23/04/2020
23/04/2020
23/04/2020
28/04/2020
28/04/2020
27/04/2020
23/04/2020
27/04/2020
27/04/2020
27/04/2020
01/03/2021',N'27/02/2020
23/04/2020
30/06/2020
30/06/2020
30/06/2020
04/05/2020
31/12/2020
30/04/2020
31/12/2020
30/06/2020
23/04/2020
30/06/2020
23/04/2020
27/04/2020
30/06/2020
30/06/2020
31/12/2020',N'27/02/2020
23/04/2020
12/05/2020
23/04/2020
27/04/2020
12/05/2021',N'No',N'nessun impatto',N'Si',N'effettuata, vedere mail allegate',N'No',N'messun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'Nessun impatto su ra DC12',N'Si',N'si da effettuare per dismettere i prodotti, MOD005,36',N'No',N'nessun virtual manufacturer',N'No',N'dismissione prodotti',NULL,'2020-12-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-22','2020-03-15 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Creazione di un modulo (check list) per la registrazione delle attività svolte per il ricondizionamento degli strumenti Elite InGenius, ossia tutte quelle attività volte a rimettere in servizio uno strumento precedentemente utilizzato da un altro cliente.',N'Messa a sistema delle attività  di ricondizionamento degli strumenti Elite InGenius, prima di ri-metterli in servizio presso un altro cliente. Una check list di attività da svolgere permette la standardizzazione del ricondizionamento.',N'Rischio di non eseguire tutte le attività richieste e necessarie durante l''attività di ricondizionamento.  ',N'972-INT030 - ELITe InGenius™
972-INT030-US - ELITe InGenius™',N'CSC
GSC
QARA
GSTL',N'Global Service
Regolatorio
Sistema qualità',N'Impatto positivo in quanto si standardizzano, attraverso la stesura e rilascio di un modulo check list, le attività di ricondizionamento strumentale di ELITe InGenius.
Impatto positivo in quanto si standardizzano, attraverso la stesura e rilascio di un modulo check list, le attività di ricondizionamento strumentale di ELITe InGenius.
Impatto positivo in quanto si standardizzano, attraverso la stesura e rilascio di un modulo check list, le attività di ricondizionamento strumentale di ELITe InGenius.
Impatto positivo, adempimento alla normativa sul ricondizionamento degli strumenti
Impatto positivo, adempimento alla normativa sul ricondizionamento degli strumenti
La messa a sistema del processo di ricondizionamento ha impatto positvo sul SGQ, rendendo standardizzate le attività.
La messa a sistema del processo di ricondizionamento ha impatto positvo sul SGQ, rendendo standardizzate le attività.',N'15/03/2020
15/03/2020
15/03/2020
15/03/2020',N'Creazione di un modulo check list e istruzione operativa
Aggiornamento della PR19,01 rev. 01 "Assistenza tecnica (Application and technical support)  in corso di revisione con il riferimento ai documenti specifici relativi all''attività di ricondizionamento degli strumenti InGenius
creare un risk assessment che gestisca il processo di ricondizionamento degli strumenti
revisione della documentazione
Verifica della significatività della modifica e, se necessario, identificazione dei paesi in cui il prodotto è registrato per effettuare la notifica
revisione della documentazione (procedura e risk assessment)
Aggiornamneto del VMP',N'PR19,01_rev02 - cc20-22_messa_in_uso_pr1901_rev02.msg
PR19,01_rev02 - cc20-22_messa_in_uso_pr1901_rev02_1.msg
PR19,01_rev02 - cc20-22_messa_in_uso_pr1901_rev02_2.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-22_mod05,36__non_significativa.pdf',N'GSC
GSC
GSC
QARA
QM
QAS
QARA
QAS',N'07/04/2020
07/04/2020
15/03/2020
15/03/2020
15/03/2020
15/03/2020',N'16/03/2020
30/09/2020
30/09/2020
30/09/2020
18/03/2020
30/09/2020',N'07/04/2020
31/12/2020
07/04/2020
18/03/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'creare un risk assessment che gestisca il processo di ricondizionamento degli strumenti',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'non necessaria',NULL,'2020-09-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-21','2020-03-12 00:00:00',N'Alessandra Vinelli',N'Documentazione SGQ
Manuale di istruzioni per l''uso
Annullato / non approvato',N'Revisione della PR05,04 "Gestione dei manuali di istruzioni per l''uso e documenti correlati" rev.02: inserimento delle IFU abbreviate (simplified workflow) in un iter di revisione e approvazione al pari delle IFU convenzionali.',N'Le IFU abbreviate sono utilizzate dal personale applicativo e rilasciate ai clienti e devono essere allineate con l''ultima revisione dell IFU convenzionali. Devono quindi essere sottoposte a un iter di revisione che garantisce la veridicità delle informazioni riportate, riducendo la probabilità che il cliente possa utilizzare in modo inappropriato i prodotti.  Per questo motivo devono essere integrate nella procedura. 
',N'Mancato allineamento con le IFU convenzionali. Mancata valutazione degli impatti delle revisioni successive delle IFU convenzionali su queste ultime.',NULL,N'AM
QM
BDM-MDx
PMI
QAS',N'Controllo qualità
Marketing
Regolatorio',N'Le  IFU semplificate sono state aggiornate dal marketing, allineate alle attuali versioni delle IFU convenzionali disponibili e codificate secondo la IO19,03 "GESTIONE DOCUMENTO DI MARKETING".  
L''impatto è a livello delle attività per l''aggiornamento della PR05,04 "Gestione dei manuali di istruzioni per l''uso e documenti correlati" e della verifica delle informazioni riportate all''interno delle IFU semplificcate
Le  IFU semplificate sono state aggiornate dal marketing, allineate alle attuali versioni delle IFU convenzionali disponibili e codificate secondo la IO19,03 "GESTIONE DOCUMENTO DI MARKETING".  
L''impatto è a livello delle attività per l''aggiornamento della PR05,04 "Gestione dei manuali di istruzioni per l''uso e documenti correlati" e della verifica delle informazioni riportate all''interno delle IFU semplificcate
-',NULL,N'Aggiornamento della PR05,04 "Gestione dei manuali di istruzioni per l''uso e documenti correlati" 
Revisione delle IFU semplificate con definizione di una tempistica. ',NULL,N'QAS
QM
QAS',NULL,NULL,NULL,N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',NULL,NULL,NULL,'2020-06-22 00:00:00',N'Si',N' ANNULLATO, sostituito dal CC20-37, aperto da SGQ, non come richiesta di aggiornamento della procedura di gestione delle IFU, ma come aggiornamento del processo di gestione delle IFU per includere al loro interno un''appendice sintetica, schematica e di immediato impatto visivo dei contenuti tecnici più rilevanti del manuale di istruzioni per l''uso.
','2020-06-22 00:00:00'),
    (N'20-20','2020-03-11 00:00:00',N'Clelia  Ramello',N'Manuale di istruzioni per l''uso',N'E'' stato richiesto al fornitore Lutech di implementare delle nuove funzionalità sul software ELITeWare. Queste nuove funzionalità sono il frutto del monitoraggio del software sul campo e sono descritte di seguito:
STANDARDIZZAZIONE ACQUISIZIONE CT DA FILE CSV
AGGIORNAMENTO CONTATORI LISTE CHIUSE
STATO DEI CONTROLLI DI QUALITÀ
IMPORT TRACCIABILITÀ DA FILE CSV
TRADUZIONE IN LINGUA FRANCESE	
INTERFACCIA SOFTWARE E MANUALE UTENTE.',N'Le nuove funzionalità velocizzeranno la configurazione del software durante l''installazione presso il cliente.',N'La configurazione del software presso il cliente richiederebbe piu'' tempo da parte del personale tecnico.',NULL,N'GASC
GPS
P&MMI
SIS',N'Assistenza applicativa
Assistenza strumenti esterni (Italia)
Global Service
Program Managment
Regolatorio',N'Impatto a livello di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Impatto positivo, le nuove funzionalità velocizzeranno la configurazione del software durante l''installazione presso il cliente.
Impatto positivo, le nuove funzionalità velocizzeranno la configurazione del software durante l''installazione presso il cliente.
Impatto positivo, le nuove funzionalità velocizzeranno la configurazione del software durante l''installazione presso il cliente.
Impatto positivo, le nuove funzionalità velocizzeranno la configurazione del software durante l''installazione presso il cliente.
impatto in termini di attività.',N'02/05/2020
02/05/2020
02/05/2020
02/05/2020
02/05/2020
02/05/2020
07/05/2020
30/04/2020',N'Seguire il change control garantendo che tutte le attività vengano svolte, come da Gantt.
Interfacciarsi con il fornitore per concordare le tempistiche per lo svolgimento delle attività, e per il rilascio della documentazione relativa alle modifiche apportate al sw ed alla loro verifica (incluso il manuale utente).
30/07/2020  Interfacciarsi con il fornitore per concordare le tempistiche per lo svolgimento delle attività, e per il rilascio della documentazione relativa alle modifiche apportate al sw ed alla loro verifica (incluso il manuale utente e il driver per la connettività al Results Manager)
Rendere disponibili al rilascio del sw da parte del fornitore di un sistema che comprenda la strumentazione InGenius collegata al middleware 
Coordinare le attività di revisione delle traduzioni in Francese in collaborazione con ELITech France
Stesura TSB
Validare le nuove funzionalità sul sistema InGenius-ELITeWare, utilizzando i MOD05,23 "piano validazione sw" e MOD05,24 "rapporto validazione sw"#',N'Gantt iniziale - cc20-20_30042020_gantt.xlsx
Gantt aggiornato a 07/2020 - cc20-20_10072020_gantt_aggiornamneto_luglio.xlsx
RELEASE NOTE 274/12/2020 ALLEGATO 7 TEST SUMMARY E RILASCIO 24/12/2020 ALLEGATO 5 TEST PROCEDURE 10/08/2020 ALLEGATO 5 TEST PROCEDURE 27/10/2020 ALLEGATO 5 TEST PROCEDURE 28/10/2020 ALLEGATO 5 TEST PROCEDURE 29/10/2020 ALLEGATO 5 TEST PROCEDURE 02/11/2020 ALLEGATO 4 VERIFICATION & VALIDATION TEST 27/10/2020 VALIDATION STATUS 24/04/2020 - cc20-20_docrilasciatidalutech.txt
PLAN2020-036 ELITEWARE CONNECTIVITY VALIDATION PLAN SCP2018-030, CC20-20 08/01/2021 REP2020-176 ELITEWARE CONNECTIVITY VALIDATION REPORT SCP2018-030, CC20-20 12/01/2021 - cc20-20_docrilasciatidaelitech.txt
release_note_eliteware_version_1.1.0 - cc20-20__release_note_eliteware_version_1.1.0.pdf
messa_in_uso_eliteware_v1.1.0 - cc20-20_messa_in_uso_eliteware_v1.1.0.msg
PLAN2020-036 ELITEWARE CONNECTIVITY VALIDATION PLAN SCP2018-030, CC20-20 08/01/2021 REP2020-176 ELITEWARE CONNECTIVITY VALIDATION REPORT SCP2018-030, CC20-20 12/01/2021 - cc20-20_docrilasciatidaelitech_2.txt
ELITWARE TSB 001 rev A VERS 1.1.0 - cc20-20_tsb_001_-_rev_a_-_sw_version_1.1.0.msg
formazione al personale sul campo - cc20-20_formazionequalita_eliteware_nuova_versione_1.1.0.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-20_mod05,36__non_significativa.pdf
release_note_eliteware_version_1.1.0 - cc20-20__release_note_eliteware_version_1.1.0_1.pdf
supportare_l_interfacciamento_del_driver_con_il_results_manager_dal_cliente - cc20-20_supportare_l_interfacciamento_del_driver_con_il_results_manager_dal_cliente.msg',N'SIS
GSC
GSTL
GASC
CSC
GASC
CSC
GASC
QARA
CSC',N'30/07/2020
02/05/2020
02/05/2020
02/05/2020
02/05/2020
02/05/2020
30/07/2020
07/05/2020
30/04/2020',N'16/11/2020
30/10/2020
30/10/2020
30/10/2020
15/10/2020
30/10/2020
30/10/2020
15/11/2020',N'03/03/2021
03/03/2021
03/03/2021
10/03/2021
03/03/2021
03/03/2021
03/03/2021',N'No',N'nessun impatto',N'Si',N'attraverso TSB',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW45 - Ingenius ELITeWare',N'creare il risk assessment del sw InGenius-ELITeWare, la cui codifica interna è SW45',N'No',N'vedere MOD05,36',N'No',N'non necessaria',N'No',N'valutare',N'Clelia  Ramello','2020-11-16 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-19','2020-03-09 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede l''aggiornamento delle Istruzioni Operative di produzione e Manutenzione la cui ultima revisione precede il 2016. Lo scopo è di allineare le IO all''operatività che attualmente viene spiegata in modo dettagliato nei documenti di utilizzo.',N'Istruzioni operative allineate all''operatività (ai moduli di produzione, manutenzione e tratura) come previsto dalla GDP.',N'Il mancato aggiornamento delle istruzioni operative rispetto ai moduli può causare ambiguita ed errori.',NULL,N'MM
QARA
QC
QM
T&GSC',N'Assistenza strumenti interni
Controllo qualità
Produzione
Sistema qualità',N'L''aggiornamento delle IO ha un impatto positivo in quanto permette di allineare il processo all''operatività ed eventualmente aggiornarlo rispetto a quanto emerso dall''analisi annuale legata al VMP (monitoraggio di reclami e nc la cui causa primaria ricade sul sotto-processo d''interesse).
L''aggiornamento delle IO ha un impatto positivo in quanto permette di allineare il processo all''operatività ed eventualmente aggiornarlo rispetto a quanto emerso dall''analisi annuale legata al VMP (monitoraggio di reclami e nc la cui causa primaria ricade sul sotto-processo d''interesse).
Le IO di CQ sono allineate e aggiornate, nessun impatto quindi se non in termini di attività
L''aggiornamento delle IO ha un impatto positivo in quanto permette di allineare il processo all''operatività ed eventualmente aggiornarlo rispetto a quanto emerso dall''analisi annuale legata al VMP (monitoraggio di reclami e nc la cui causa primaria ricade sul sotto-processo d''interesse).
L''aggiornamento delle IO ha un impatto positivo in q',N'09/03/2020
09/03/2020
09/03/2020
09/03/2020
09/03/2020
09/03/2020
09/03/2020',N'Verifica delle informazioni da aggiornare, riferendo a QC e DS le informazioni che vanno modificate. Revisione delle IO. Approvazione
-	PROCESSO "TE" TARATURA ESTERNA STRUMENTI: i risk assessment sono da rivedere per la messa in uso di linkomm ed archiviazione del db manutenzioni, e per la gestione differente rispetto a quella impostata da Maurizio
-	PROCESSO "M" MANUTENZIONE INTERNA: nel risk assessment M4 (MPI) non è mai stato considerato il rischio di esecuzione errata di una MPI, e le misure di controllo atte ad eliminare un tale rischio (ci sono state 2 nc a CG relative ad una diminuzione di temperatura sotto i limiti per un mancato spegnimento del superfrost).
Aggiornare le IO con revisione antecedente il 2016. Vedi allegato
Verifica delle informazioni da aggiornare, riferendo a QC e DS le informazioni che vanno modificate. Revisione delle IO. Approvazione
Per il sotto-processo P1.4 "Misurazione e dispensazione semilavorati" esiste un unico RA che raccoglie tutte le NC/R che derivano da dispen',N'elenco Io da aggiornare - elenco-io.xlsx
IO09,18_01 - cc20-19_messa_in_uso_i0918_01_mod1802_19052020.msg
io09,06_01 - cc20-19_messa_in_uso_io09,06_01,_io09,14_06_cc18-51,_cc20-19,_mod18,02_del_16_04_2020.pdf
io0901_01 - cc20-19_messa_in__uso_io0901_01.msg
IO09,03_01 - cc20-19_messa_in_uso_io0903_01.msg
le istruzioni di manutenzione e taratura strumenti interni sono tutte aggiornate. La IO11,06 relativa all''utilizzo del db manutenzioni è stata archiviata, sostituita dal sw linkomm. - cc20-19_controllo_aggiornamento_istruzioni_manutenzione_e_taratura_1.msg
elenco IO da aggiornare - elenco-io_1.xlsx
IO09,18_01 - cc20-19_messa_in_uso_i0918_01_mod1802_19052020_1.msg
IO09,03_01 - cc20-19_messa_in_uso_io0903_01.msg
IO0901_01 - cc20-19_messa_in__uso_io0901_01_1.msg
IO09,06_01, IO09,14_06 - cc20-19_messa_in_uso_io09,06_01,_io09,14_06_cc18-51,_cc20-19,_mod18,02_del_16_04_2020.pdf
elenco IO da aggiornare - elenco-io_2.xlsx
le istruzioni di manutenzione e taratura strumenti interni sono tutte aggiornate. La IO11,06 relativa all''utilizzo del db manutenzioni è stata archiviata, sotituita dal sw linkomm. - cc20-19_controllo_aggiornamento_istruzioni_manutenzione_e_taratura.msg
IO09,18_01 - cc20-19_messa_in_uso_i0918_01_mod1802_19052020_2.msg
IO09,03_01 - cc20-19_messa_in_uso_io0903_01_1.msg
io0901_01 - cc20-19_messa_in__uso_io0901_01_2.msg
IO09,06_01, IO09,14_06 - cc20-19_messa_in_uso_io09,06_01,_io09,14_06_cc18-51,_cc20-19,_mod18,02_del_16_04_2020_1.pdf
valutazione e aggiornamento RA e VMP in accordo con l''analisi annuale 2021 e secondo la RAP21-3 - cc20-19_valutazione_e_aggiornamento_ra_e_vmp_in_accordo_con_l_analisi_annuale_2021_e_secondo_la_rap21-3.txt
valutazione e aggiornamento RA e VMP in accordo con l''analisi annuale 2021 e secondo la RAP21-3.  - cc20-19_valutazione_e_aggiornamento_ra_e_vmp_in_accordo_con_l_analisi_annuale_2021_e_secondo_la_rap21-3_1.txt
valutazione e aggiornamento RA e VMP in accordo con l''analisi annuale 2021 e secondo la RAP21-3 - cc20-19_valutazione_e_aggiornamento_ra_e_vmp_in_accordo_con_l_analisi_annuale_2021_e_secondo_la_rap21-3_2.txt',N'QC
QCT/DS
MM
PCT
PT
T&GSC
T&GSS
QARA
QM
QAS
QAS
T&GSC
MM
QAS
QAS',N'09/03/2020
09/03/2020
09/03/2020
09/03/2020
09/03/2020
09/03/2020
10/03/2020',N'30/10/2020
29/12/2020
30/10/2020
30/10/2020
29/12/2020
30/10/2020
29/12/2020',N'04/05/2021
04/05/2021
04/05/2021
04/05/2021
04/05/2021
04/05/2021
04/05/2021',N'No',N'nessun impatto ',N'No',N'non necessaria, è un''aggiornamento di documentazione interna',N'No',N'non necessaria, è un''aggiornamento di documentazione interna',N'M - Manutenzione Sturmenti
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
T - Taratura Esterna Strumenti',N'M4 - Esecuzione manutenzione interna
P1.14 - Misurazione e dispensazione semilavorati
T1 - Gestione informazioni dello strumento',N'aggiornare i risk assessment anche in base agli esiti dell''analisi annuale del VMP esempio(M4, P1,14, T1)',N'No',N'Vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'na',NULL,'2020-12-29 00:00:00',NULL,'2021-05-04 00:00:00',N'No',NULL,NULL),
    (N'20-18','2020-02-27 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento del sito web ELITechGroup (codice interno SW30) al fine di:
- caricare (identificare e tracciare) la documentazione dei prodotti commercilizzati negli USA differenziandola dalla documentazione dei prodotti commercilaizzati in tutti gli altri paesi
- caricare altri tipi di documenti come Schede dati Sicurezza (SDS) e certificati ISO.',N'Identificazione e tracciabilità della documentazione dei prodotti commercilizzati negli USA, libero accesso da parte dei clienti a SDS e certificati ISO.',N'Difficoltà nell''identificazione della documentazione specifica per i prodotti commercializzati negli USA da parte dei clienti USA. SDS e dei certificati ISO accessibili ai clienti solo tramite richiesta al personale EGSpA.',NULL,N'QARA',N'Regolatorio
Sistema qualità',N'Impatto positivo: i clienti USA identificano e rintracciano la documentazione dei prodotti commercilizzati negli USA che si differenzia chiaramente da quella dei prodotti commercilaizzati in tutti gli altri paesi, si riduce così il rischio di mix-up dei documenti. Rendere accessibili sul sito web le SDS e i certificati ISO permette di ridurre il tempo lavoro che il personale SGQ dedicata all''invio degli stessi ai distributori e ICO ad ogni aggiornamento.
Impatto positivo: i clienti USA identificano e rintracciano la documentazione dei prodotti commercilizzati negli USA che si differenzia chiaramente da quella dei prodotti commercilaizzati in tutti gli altri paesi, si riduce così il rischio di mix-up dei documenti. Rendere accessibili sul sito web le SDS e i certificati ISO permette di ridurre il tempo lavoro che il personale SGQ dedicata all''invio degli stessi ai distributori e ICO ad ogni aggiornamento.
Impatto positivo: i clienti USA identificano e rintracciano la documentazione dei prodotti commercil',N'18/05/2020
18/05/2020
18/05/2020
18/05/2020
18/05/2020
18/05/2020
18/05/2020',N'Aggiornamento del risk assessment sito web ELITechGroup (codice interno SW30) includendo:
- la mitigazione del rischio relatovo al mix-up dei documenti (documentazione specifica dei prodotti USA identificata e differenziata (tracciabile) rispetto alla documentazione dei prodotti commercializzati nel resto del mondo
- l''inserimento sul sito di documentazione soggetta a modifica nel corso del tempo (SDS, certificati ISO)
validazione della nuova versione del sito web (MOD05,23 e MOD05,24) per il caricamento delle IFU
''Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
validazione della nuova versione del sito web (MOD05,23 e MOD05,24) per il caricamento delle SDS e certificati ISO
verifica ed approvazione documenti di validazione, aggiornamento VMP
aggiornamento dei template delle etichette con il nuovo indirzzo web e, conseguentemente di tutte le etichette (vedere anche CC20-24 relativo all''inserimento del ',N'piano validazione - mod05,23_00_piano_validazione_sito_web_ifu_2020_1.pdf
report validazione - mod05,24_00_rapporto_validazione_sito_web_ifu_2020_1.pdf
la modifica non è significativa, pertanto non è da notificare. - cc20-18_mod05,36__non_significativa.pdf
piano di validazione - mod05,23_00_piano_validazione_sito_web_ifu_2020.pdf
report di validazione - mod05,24_00_rapporto_validazione_sito_web_ifu_2020.pdf
la verifica delle etichette è stata svolta collegata al CC20-24 relativo all''introduzione del UDI - cc20-18_etichette_ocn_sito_web_aggiornato.txt',N'RAS
RAS
QARA
QAS
RAS
QARA
QM
QAS
QARA
QM
RAS',N'18/05/2020
18/05/2020
30/03/2020
18/05/2020
18/05/2020
18/05/2020
18/05/2020',N'29/12/2020
31/08/2020
29/12/2020
29/12/2020
30/05/2020
29/12/2020',N'30/07/2020
30/03/2020
30/07/2020
30/07/2020',N'No',N'nessun impatto',N'No',N'da valutare se da fare via mail o inserendo un pop-up sul sito web',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW30 - elitechgroup IFU (Sito web in cui caricare i manuali di istruzioni per l''uso)',N'Aggiornamento del risk assessment SW30 includendo la mitigazione del rischio che il cliente acceda a documentazione non specifia per la commercializzazione in USA.',N'No',N'vedere MOD05,36. Non significativa, non si tratta di un sw associato ad un ivd.',N'No',N'nessun virtual manufacturer',N'No',N'non applicabile',N'Michela Boi','2020-12-29 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-17','2020-02-24 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Inserimento di un paragrafo sulla "protezione contro i rischi di sicurezza informatica" nell''Operator''s manual dello strumento ELITe InGenius CE-IVD (ref. INT030)',N'il manuale dello strumento riporterebbe dei suggerimenti per salvaguardare il sistema ELITe InGenius dai rischi legati alla Cybersecurity',N'L''utilizzatore dello strumento non prenderebbe le precauzioni suggerite per evitare di incorrere in problematiche legate alla Cybersecurity',N'972-INT030 - ELITe InGenius™',N'QARA
GSTL
SIS',N'Global Service
Operation Site
Regolatorio',N'Impatto positivo perchè il manuale dello strumento riporterebbe dei suggerimenti per salvaguardare il sistema ELITe InGenius dai rischi legati alla Cybersecurity.
Impatto positivo perchè il manuale dello strumento riporterebbe dei suggerimenti per salvaguardare il sistema ELITe InGenius dai rischi legati alla Cybersecurity.
Impatto positivo perchè il manuale dello strumento riporterebbe dei suggerimenti per salvaguardare il sistema ELITe InGenius dai rischi legati alla Cybersecurity
Impatto positivo perchè il manuale dello strumento riporterebbe dei suggerimenti per salvaguardare il sistema ELITe InGenius dai rischi legati alla Cybersecurity',N'27/02/2020
27/02/2020
27/02/2020
27/02/2020',N'Inserimento nel manuale del paragrafo "protezione contro i rischi di sicurezza informatica"
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Revisione del paragrafo inserito "protezione contro i rischi di sicurezza informatica"
creazione TSB ed invio',N'IFU rev03 - cc20-17_ifu_int030_-_cc20-17.msg
IFU rev03 - cc20-17_ifu_int030_-_cc20-17_1.msg
La modifica è significativa, pertano da notificare - cc20-17_mod05,36_significativa.pdf
Notifica effettuata - cc20-17_qualita_egspa_sgq_notification_of_change_-int030_-_cc20-17.msg
TSB002_revH_mail di inivio del 3/04/2020 - cc20-17_tsb_002_-_rev_h_-_operator_manual_signed.pdf',N'RAS
GSTL
QARA
QAS
GSTL',N'27/02/2020
27/02/2020
27/02/2020
27/02/2020',N'06/03/2020',N'20/02/2020
08/04/2020
20/02/2020
06/04/2020',N'No',N'nessun impatto',N'Si',N'attraverso tramite TSB',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU10 - gestione delle modifiche dei manuali e degli assay protocol (aggiornamenti, estensioni d''uso)',N'nessun impatto, trattasi di un aggiornamento del IFU',N'Si',N'vedere MOD05,36',N'No',N'nessun virtula manufacturer',N'No',N'la gestione dei rischi non è da aggiornare: i rischi di Cybersecurity erano già stati trattati, ma non riportartati sull''IFU.',N'Michela Boi','2020-03-06 00:00:00',NULL,'2020-04-08 00:00:00',N'No',NULL,NULL),
    (N'20-16','2020-02-19 00:00:00',N'Alessandra Vinelli',N'Documentazione SGQ',N'(Da 1-20-OSS1 Report audit 1-20) Revisione delle istruzioni operative di MKT:
- IO19,03 "Gestione Documento Di Marketing": introduzione delle nuove tipologie di documenti: Teaser, did yuo know?, Customer Presentation, Marketing presentation, Simplified IFU; aggiornamento delle funzioni; introduzione della gestione dell''aggiornamento dei contenuti: del sito web di EGSpA, delle pagine dedicate a EGSpA nei social (esempio: linkedin) e dei servizi web  di condivisione (esempio: google drive);
- IO19,04 "Gestione Mailing List Per Comunicazioni Ai Clienti" aggiornamento delle funzioni.',N'Allineamento delle istruzioni del marketing all''operatività. La documentazione aggiornata rispecchierà gli attuali documenti di marketing,  le mansioni e le funzioni associate al personale del gruppo marketing.',N'Disallineamento delle istruzioni rispetto all''operatività e alle funzioni in organigramma con conseguente confusione a livello della gestione del materiale marketing e del suo processo di approvazione e revisione. ',NULL,N'QARA
QM
MS
IMM
S&MM',N'Marketing
Regolatorio
Sistema qualità',N'Impatto positivo. La revisione delle istruzioni operative agevolerà la gestione del materiale di marketing, garantendo il rilascio di informazioni corrette,  sempre aggiornate, revisionate ed approvate secondo il flusso operativo individuato. 
Impatto positivo. La revisione delle istruzioni operative agevolerà la gestione del materiale di marketing, garantendo il rilascio di informazioni corrette,  sempre aggiornate, revisionate ed approvate secondo il flusso operativo individuato. 
Impatto positivo, la documentazione aggiornata sarà conforme alle attività effettivamente svolte, inclusa la gestione dei servizi sul web,  e rispecchierà le mansioni e le funzioni associate al personale del gruppo marketing.
Impatto positivo, la documentazione aggiornata sarà conforme alle attività effettivamente svolte, inclusa la gestione dei servizi sul web,  e rispecchierà le mansioni e le funzioni associate al personale del gruppo marketing.
Impatto positivo, la documentazione aggiornata sarà conforme alle a',N'08/10/2020
08/10/2020
08/10/2020
08/10/2020
08/10/2020
08/10/2020
08/10/2020',N' Revisione delle istruzioni operative di MKT:
- IO19,03 "Gestione Documento Di Marketing": introduzione delle nuove tipologie di documenti: Teaser, did yuo know?, Customer Presentation, Marketing presentation, Simplified IFU; aggiornamento delle funzioni; introduzione della gestione dell''aggiornamento dei contenuti: del sito web di EGSpA, delle pagine dedicate a EGSpA nei social (esempio: linkedin) e dei servizi web  di condivisione (esempio: google drive);
- IO19,04 "Gestione Mailing List Per Comunicazioni Ai Clienti" aggiornamento delle funzioni.
Aggiornamento del modulo di revisione e approvazione MOD05,06 "RICHIESTA APPROVAZIONE MATERIALE PROMOZIONALE" in modo che contenga la tipologia dei documenti di MKT che necessitano di revisione ed approvazione in quanto contengono contenuti tecnico-scientifici e le funzioni responsabili per la revisione del materiale marketing 
Aggiornamento della matrice delle competenze.
Come marcoattività includere, almeno, 
- la raccolta di requisiti sia per lo sviluppo',N'IO19,04_00 - cc20-16_messa_in_uso_io1904_00.msg
IO19,04_00 - cc20-16_messa_in_uso_io1904_00_1.msg
verbale riunione gestione sito web - cc20-16_mod01,03_00_verbaleriunione_website_managment_sf.doc
la modifica non è significativa, pertanto non è da notificare. - cc20-16_mod05,36__non_significativa.pdf
pr05,01 rev05 - c20-16_messa_in_uso_pr_e_mod_per_ivdr_e_reg_giapppone.msg',N'MS
MA
QM
QAS
E&SM
S&MM
QARA
QAS
QAS
QARA',N'08/10/2020
08/10/2020
08/10/2020
08/10/2020
03/04/2020
08/10/2020',N'18/12/2020
18/12/2020
18/12/2020
18/12/2020
18/12/2020',N'11/11/2020
03/04/2020',N'No',N'nessun impatto ',N'No',N'aggiornamento documentazione interna',N'No',N'aggiornamento documentazione interna',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun risk assessment associato, valutare se crearne uno',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'aggiornamento documentazione interna',NULL,'2020-12-18 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-15','2020-02-19 00:00:00',N'Alessandra Vinelli',N'Documentazione SGQ',N'Si richiede l''aggiunta dei prodotti di terze parti al modulo MOD14,21_00_Riesame Validazioni da parte del marketing.',N'L''implementazione del "Riesame validazioni" con i prodotti di terze parti migliora la preparazione delle gare Italia a cura del Marketing.',N'L''utilizzo di due moduli separati complicherebbe la selezione dei prodotti per la stesura della relazione tecnica. ',NULL,N'AM
PVS
QARA
GASC
GPS
PMI
PVC',N'Assistenza applicativa
Marketing
Regolatorio
Sistema qualità
Validazioni',N'Il modulo è utilizzato per la sorveglianza post-market dei prodotti di cui EGSpA è fabbricante. I prodotti di terze parti non verranno considerati durante il processo di post-market surveillance.
Il modulo è utilizzato per la sorveglianza post-market dei prodotti di cui EGSpA è fabbricante. I prodotti di terze parti non verranno considerati durante il processo di post-market surveillance.
Impatto positivo in quanto tutta l''offerta EGSPA sarà disponibile in un unico archivio gestito ed aggiornato. Codici, descrizione prodotti, formato, caratteristiche tecniche, performance analitiche  e cliniche saranno accessibili immediatamente, facilitando il supporto della funzione alla preparazione delle gare. 
Impatto positivo in quanto tutta l''offerta EGSPA sarà disponibile in un unico archivio gestito ed aggiornato. Codici, descrizione prodotti, formato, caratteristiche tecniche, performance analitiche  e cliniche saranno accessibili immediatamente, facilitando il supporto della funzione alla preparazione d',N'25/02/2020
25/02/2020
30/03/2020
30/03/2020
27/03/2020
25/02/2020',N'mettere in uso il mdulo
Aggiornamento del modulo con i prodotti di terze parti.
Possibilità di lettura e scrittura
aggiornamento layout MOD14,21
Nessuna attività. Possibilità di lettura del file da arte degli FPS.',N'MOD14,21_01 - cc20-15_messa_in_uso_layout_bianco_mod14,21_01.docx
MOD14,21_01  - cc20-15_messa_in_uso_layout_bianco_mod14,21_01_1.docx',N'QM
PMI
PVC
GASC',N'25/02/2020
30/03/2020
27/03/2020
25/02/2020',N'03/04/2020
03/04/2020
03/04/2020
03/04/2020',N'31/03/2020
31/03/2020
31/03/2020
25/02/2020',N'No',N'non applicabile',N'No',N'documento interno di sistema che non ha impatto sul cliente',N'No',N'documento interno di sistema che non ha impatto sul prodotto ',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'nessun impatto su ASA5',N'No',N'vedere MOD05,36',N'No',N'documento interno di sistema che non ha impatto sul virtual manufacturer',N'No',N'non applicabile',NULL,'2020-04-03 00:00:00',NULL,'2020-03-31 00:00:00',N'No',NULL,NULL),
    (N'20-14','2020-02-16 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Creazione di un''Istruzione Operativa che descriva le modalità e le responsabilità per l''esecuzione delle prove di sicurezza elettrica (secondo la normativa IEC 61010-1:2010 e UL 61010-1:2012)  sullo strumento ELITe InGenius. Creazione di un modulo di registrazione della prova effettuata.',N'Esecuzione delle prove di sicurezza elettrica sullo strumento ELITe InGenius con rilascio dell''esito du un documentoELITech che ne attesta la conformità.',N'Al termine delle prove eseguite viene rilasciato solo il documento emesso dallo struemnto di misura. ',N'972-INT030 - ELITe InGenius™
972-INT030-US - ELITe InGenius™',N'CSC
GSC
QARA',N'Assistenza strumenti esterni (Italia)
Assistenza strumenti interni
Global Service
Regolatorio',N'Impatto positivo in quanto si standardizza, attraverso la stesura di un''istruzione operativa, la metodica di esecuzione delle prove di sicurezza elettrica.
Impatto positivo in quanto oltre all''esito rilasciato dallo srumento è possibile rilasciare anche un documento ELITech firmato dal personale che ha eseguito la verifica di sicurezza elettrica.
Impatto positivo in quanto oltre all''esito rilasciato dallo srumento è possibile rilasciare anche un documento ELITech firmato dal personale che ha eseguito la verifica di sicurezza elettrica.
Impatto positivo in quanto si standardizza, attraverso la stesura di un''istruzione operativa, la metodica di esecuzione delle prove di sicurezza elettrica.
Impatto positivo in quanto si standardizza, attraverso la stesura di un''istruzione operativa, la metodica di esecuzione delle prove di sicurezza elettrica.
Nessun impatto se non in termini di attività.',N'30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020
03/04/2020',N'Stesura dell''istruzione operativa e del modulo di registrazione. 
Formazione al personale interessato (FSE, T&GSC, T&GSS).
Nessuna attività.
rilascio dell''dell''istruzione operativa
Verificare che l''istruzione operativa contenga i corretti riferimenti normativi.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Coordinamento con il personale dell''assitenza strumenti esterni sulla gestione degli strumenti utilizzati per effettuare le verifiche di sicurezza elettrica (codice interno VSExx): informazione sulle tempistiche per la manutenzione e taratura.',N'IO11,20_00, MOD11,30_00 - cc20-14__messa_in_uso_io11,20e_mod11,30_verifiche_sicurezza_elettrica_ingenius_cc20-24.pdf
IO11,20_00, MOD11,30_00 - cc20-14__messa_in_uso_io11,20e_mod11,30_verifiche_sicurezza_elettrica_ingenius_cc20-24_2.pdf
la modifica non è significativa, pertanto non deve essere notificata. - cc20-24_mod05,36__non_significativa.pdf
vedere scheda apparecchiatura e logbook VSE2 (MOD11,02A e MOD11,02B) - cc20-24_vse2.pdf
IO11,20_00, MOD11,30_00 - cc20-14__messa_in_uso_io11,20e_mod11,30_verifiche_sicurezza_elettrica_ingenius_cc20-24_1.pdf',N'GSC
GSTL
CSC
QARA
QARA
QAS
T&GSC
T&GSS
GSC',N'30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020',NULL,N'12/05/2020
12/05/2020
12/05/2020
12/05/2020
30/03/2020
12/05/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto ',N'ST - Elenco famiglie Strumenti',N'ST38 - Strumento per la Verifica  Sicurezza Elettrica (VSExx)',N'da creare il nuovo risk assessment sul VSE (ST38)',N'No',N'vedere MOD05,36',N'No',N'nessun Virtual manufacturer.',N'No',N'Nessun impatto.',NULL,NULL,NULL,'2020-05-12 00:00:00',N'No',NULL,NULL),
    (N'20-13','2020-02-11 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Modifica delle specifiche SPEC951-M800351_02_MRSA e SPEC951-M800358_00_C_difficile per di allinearle con i documenti di Elitech Inc (EGI), il quale ha comunicato che la sequenza del primer MEC-E6 (prodotto M800351_MRSA) contiene una INOSINA al posto di una SUPER-I, e la sonda CtxB-FAM41 (prodotto M800358_00_C_difficile) contiene una P* = Pyrene = Z76 e non una N* = Super N = Z65. Queste modifiche erano state introdotte durante la progettazione e la validazione del prodotto, ma non comunicata tempestivamente (si vedano le specifiche allegate, precedenti e attuali, fornite da EGInc per i dettagli).',N'Allineamento documentale con le modifiche segnalate da EGI.',N'EGInc produce i due oligonucleotidi contenenti le sequenze con le modifiche: MEC-E6 --> I al posto di I* e CtxB-FAM41--> P* al posto di N*. Se le specifiche non vengono aggiornate il controllo in accettazione potrebbe risultare non conforme rispetto al certificato rilasciato da EGI.',N'962-M800351 - MRSA/SA ELITe MGB Kit
962-M800358 - C.difficile ELITe MGB Kit',N'PW
QC
RDM',N'Magazzino
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Nessun impatto se non in termini di attività.
Nessun impatto sui prodotti che contenevano già la modifica segnalata da EGI, impatto esclusivamente documentale per l''aggiornamento delle specifiche.
Impatto positivo, il controllo in accettazione è allineato con le caratteristiche degli oligonucleotidi rilasciati dal fornitore EGI.
La modifica non ha impatto sul prodotto in quanto era stata introdotta durante il progetto e la validazione del prodotto, ha solo un impatto documentale. ',N'18/02/2020
25/02/2020
25/02/2020
18/02/2020',N'Messa in uso documenti.
Modifica delle specifiche SPEC951-M800351_02_MRSA e SPEC951-M800358_00_C_difficile.:
- MEC-E6 (facente parte del prodotto M800351_MRSA) contiene una INOSINA al posto di una SUPER-I
- la sonda CtxB-FAM41 (facente parte del prodotto M800358_00_C_difficile) contiene una P* = Pyrene = Z76 e non una N* = Super N = Z65.
Formazione al personale.
Nessun attività.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica. In caso positivo verificare se le specifiche oggetto di modifica sono state rilasciate presso le autorità compententi dei paesi in cui i prodotti sono registrati.',N'SPEC. R&D non aggiornata_C.difficile - spec.m300592_r2_c._difficile_toxin_b_gene_ctxb-fam41_pleiades_probe.doc
Specifica C. difficile AGGIORNATA - spec.m300592_r3_c.difficile_toxin_b_gene,_ctxb-fam41,_pleiades_probe.pdf
Specifica MRSA AGGIORNATA - spec.m300602_r3_mrsa_meca_gene,_mec-e6,_primer.pdf
SPEC. R&D non aggiornata_MRSA - spec.m300602_r2_mrsa_meca_gene_mec-e6_primer.doc
spec951-m800351_03_spec951-m800358_01 - cc20-13_spec951-m800351_03_spec951-m800358_01_1.msg
spec951-m800351_03_spec951-m800358_01 - cc20-13_spec951-m800351_03_spec951-m800358_01.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-13_nonsignificativa.pdf',N'RT
QAS
MM
QARA
QAS',N'18/02/2020
25/02/2020
25/02/2020
18/02/2020',NULL,N'19/04/2021
19/04/2021
19/04/2021
25/02/2020',N'No',N'nessun impatto ',N'No',N'non necessria',N'No',N'non necessario',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto, la modifica riguarda un aggiornamento comunicato dal fornitore qualificato (EGI).',N'No',N'vedere MOD05,36',N'No',N'nussun virtual manufacture coinvolto',N'No',N'non necessario',NULL,NULL,NULL,'2021-04-19 00:00:00',N'No',NULL,NULL),
    (N'20-12','2020-02-10 00:00:00',N'Serena Zaza',N'Documentazione SGQ',N'Revisione ed aggiornamento del MOD05,35 LIS_Assay_Parameter:
- creare un''istruzione che dettagli le modalità e responsabilità di aggiornamento e la verifica da parte di un secondo operatore degli aggiornamenti
- indicare la cronologia delle modifiche,
- integrazioni con di frasi standard per il referto rilasxciato dal LIS,
- permettere la stampa di parte del modulo, con dei campi aggiuntivi per l''accettazione da parte del cliente
Con il fornitore qualificato (Lutech) del sw ELITeWare, codice interno SW45, veriificare la possibilità che le informazioni presenti sul MOD05,35 possano essere importate automaticamente al sw ELITeWare. Tale verifica deve essere documentata (vedere anche CC20-20).',N'STANDARDIZZAZIONE: 
I dati inseriti, non modificabili dall''utilizzatore (FPS), possono essere selezionati (nascondere quelli non utili e aggiungerne di Custom per TPP).
Possibilità di scelta di Transcodifica per il referto finale solo in 4 tipologie, le più comuni.

TRACCIABILITA'':
Possibilità di tracciare il livello di conversione ad Unità Internazionali
Il modulo può essere stampato e presenta dei campi firma per accettazione da parte del cliente.

SEMPLIFICAZIONE: 
Unico modulo per il cliente e per gli operatori Informatici coinvolti.
Utilizzo per l''FPS molto rapido e aggiunta di una guida di utilizzo, con conseguente agevolazione nell''esecuzione degli Interfacciamenti.
Gestione standardizzata del modulo da parte delle figure di revisione e controllo.

USABIILITA'' DEL DOCUMENTO:
il modulo conterrà le istruzioni d''uso, ci sarà un doppio operatore per la verifica dei dati, si manterrà la tracciabilità delle modifiche apportate.

Riduzione delle ore lavoro attraverso il trasferimento automatico delle informazion idal MOD05,35 al sw eELITeWare.',N'Mancata tracciabilità e standardizzazione dei collegamenti LIS.',NULL,N'QM
RDM
GASC
PVC',N'Assistenza applicativa
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo: comunicazione agevolata e standardizzata con i clienti e le figure informatiche coinvolte negli Interfacciamenti.
Impatto positivo: comunicazione agevolata e standardizzata con i clienti e le figure informatiche coinvolte negli Interfacciamenti.
Impatto positivo: comunicazione agevolata e standardizzata con i clienti e le figure informatiche coinvolte negli Interfacciamenti.
Nessun impatto se non in termini di attività
Una gestione standardizzata, che include dei controlli aggiuntivi e che permette la tracciabilità delle modifiche ha un impatto positivo.
Una gestione standardizzata, che include dei controlli aggiuntivi e che permette la tracciabilità delle modifiche ha un impatto positivo.
Una gestione standardizzata, che include dei controlli aggiuntivi e che permette la tracciabilità delle modifiche ha un impatto positivo.
Impatto positivo: la nuova gestione del modulo include un''istruzione di compilazione, un doppio operatore per la verifica dei dati inseriti ed una tabe',N'30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020
30/03/2020',N'Verifica ed approvazione del nuovo modulo.
pianficiare e documentare la verifica del trasferimento automatico dei dati dal MOD05,35 al sw elitware in accordo con il fornitore lutech ( vedere anche CC20-20) e documentare la verifica sulle funzinalità del MOD05,35
verifica del nuovo modulo.
verifica del modulo per la gestione della cronologia delle modifiche e messa in uso dello stesso
Aggiornamento del modulo e formazione al personale coinvolto.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'cc20-12_messa_in_uso_mod0535_e_io1914_rev00 - cc20-12_messa_in_uso_mod0535_e_io1914_rev00.msg
Cc20-12_messa_in_uso_mod0535_rev01 - cc20-12_messa_in_uso_mod0535_rev01.msg
Formazione reparto validazioni - cc20-12_allegato_formazione_validazioni15-04-2020.pdf
MOD05,35_00 contiene il foglio per la cronologia delle modifiche - cc20-12_messa_in_uso_mod0535_e_io1914_rev00.msg
la modifica non è significativa, pertanto non è da notificare. - cc20-12_mod05,36__non_significativa.pdf',N'GASC
PVS
PVC
QAS
RT
QARA
QAS
GASC',N'30/03/2020
30/03/2020
30/03/2020
30/03/2020',NULL,N'19/05/2020
15/04/2020
30/03/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW45 - Ingenius ELITeWare',N'in corso la creazione di risk assessment specifici per la gestione degli assay protocol (RAC19-32) e la creazione del risk assessment specifico per il sw elitwre (SW45)',N'No',N'vedre MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'non applicabile',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-11','2020-02-05 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Richiesta di un nuovo fornitore per la manutenzione e la taratura esterna di micropipette e dispensatori elettronici: Eppendorf S.r.L che sostituirà Mettler-Toledo.',N'Precisione nella stesura del certificato fornito.',N'Certificati di taratura non idonei in base ai requisiti richiesti da EGSPA',NULL,N'QM
T&GSC
T&GSS',N'Assistenza strumenti interni
Sistema qualità',N'Impatto positivo in quanto la documetnazione fornita da Eppendorf S.r.L rispetta i requisiti richiesti da EGSpA.
Impatto positivo in quanto la documetnazione fornita da Eppendorf S.r.L rispetta i requisiti richiesti da EGSpA.
Impatto positivo in quanto la documetnazione fornita da Eppendorf S.r.L rispetta i requisiti richiesti da EGSpA.',N'05/02/2020
05/02/2020
05/02/2020',N'Aggiornamento SPEC-MP-DPE rev03
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'SPEC-MP-DPE rev04 del 20/01/2020 - cc20-11_messa_in_uso_spec-mp-dpe_04.msg
 SPEC-MP-DPE rev04 del 20/01/2020 , in uso al 17/02/2020 - cc20-11_messa_in_uso_spec-mp-dpe_04_1.msg
La modifica non è significativa, pertanto non è da notificare presso le autorità competenti. - cc20-11_mod05,36_non_significativa.pdf',N'T&GSS
QAS
QARA
QAS',N'05/02/2020
05/02/2020
05/02/2020',NULL,N'17/02/2020
17/02/2020
17/02/2020',N'No',N'nessun impatto',N'No',N'non necessaria, la modifica riguarda un processo interno.',N'No',N'nessun impatto',N'ST - Elenco famiglie Strumenti
ST - Elenco famiglie Strumenti',N'ST15 - Dispensatori Elettronici (DPE)
ST11 - Micorpipette (MP)',N'Nessun impatto sul risk assessment, la modifica riguarda l''aggiornamento del fornitore di assistenza.',N'No',N'vedere MOD05,36. La modifica non è sognificativa.',N'No',N'nessun virtual manufacturer coinvolto',N'No',N'non applicabile, in quanto si tratta dell''aggiornamento del fornitore di assiestenza.',NULL,NULL,NULL,'2020-02-17 00:00:00',N'No',NULL,NULL),
    (N'20-10','2020-01-31 00:00:00',N'Stefania Brun',N'Documentazione SGQ',N'A seguito dell'' audit interno al processo gare e contratti (rif. report audit 16-19) è risultato che la procedura PR03,01 "GESTIONE ORDINI CLIENTE" necessita di essere allineata a seguito dell''entrata in uso del software Gare&Offerte per quanto riguarda i controlli relativi al prezzo previsti dal paragrafo 3.1 Riesame degli ordini che sono stati resi automatici grazie all''interfacciamento del sw Gare&Offerte con il gestionale Navision',N'Allineamento procedura all''operatività modificata a seguito dell''introduzione del SW Gare&Offerte  da parte dell''ufficio gare',N'Mancato allineamento procedura all''operatività per quanto riguarda il riesame dei prezzi relativi all''ordine cliente',NULL,N'SAD-OP
T&CM
QAS',N'Gare
Ordini
Sistema qualità',N'Positivo in quanto il controllo sui prezzi è stato automatizzato e reso più sicuro grazie all''interfacciamento tra il SW Gare&Offerte e il gestionale Navision
Positivo in quanto il controllo sui prezzi è stato automatizzato e reso più sicuro grazie all''interfacciamento tra il SW Gare&Offerte e il gestionale Navision
Positivo in quanto il controllo sui prezzi è stato automatizzato e reso più sicuro grazie all''interfacciamento tra il SW Gare&Offerte e il gestionale Navision
Positivo in quanto l''aggiornamento della procedura PR03,01 rispetto all''introduzione del SW  permette di essere in compliance con l''operatività svolta e prevenire non conformità
Positivo in quanto l''aggiornamento della procedura PR03,01 rispetto all''introduzione del SW  permette di essere in compliance con l''operatività svolta e prevenire non conformità',N'24/03/2020
24/03/2020
24/03/2020
03/02/2020
03/02/2020',N'Aggiornare la procedura PR03,01 con l''indicazione dell''acquisizione automatica dei prezzi a seguito di aggiudicazione gara o firma del contratto/i con distributore/i
Eseguire formazione sulla nuova revisione della PR03,01
nessun attività a carico
Messa in uso della nuova revisione della procedura
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'la modifica non è significativa, pertanto non è da notificare. - cc20-10_mod05,36_non_significativa.pdf',N'SAD-OP
SAD-OP
QAS
QARA
QAS
T&CM',N'24/03/2020
24/03/2020
24/03/2020
24/03/2020
24/03/2020',N'31/08/2020
31/08/2020',N'24/03/2020',N'No',N'Non applicabile in quanto non riguarda documentazione di produzione dei prodotti',N'No',N'Non applicabile',N'No',N'No in quanto non riguarda le prestazioni del prodotto',N'SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'SP8 - Ordine da EGSpA',N'La modifica introdotta rende più sicuro il controllo sui prezzi e quindi il riesame dell''ordine, verificare se aggiornare il RA nel paragrafo "Misure di controllo già in atto".',N'No',N'No in quanto non riguarda processi correlati alla produzione del prodotto/i',N'No',N'No in quanto non riguarda processi correlati alla produzione del prodotto/i',N'No',N'No in quanto non riguarda le prestazione del prodotto/i',NULL,'2020-08-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-9','2020-01-31 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Si richiede l''aggiornamento della specifica SPEC950-210 "50 U/ul PLATINUM TAQ"  affinchè sia possibile riportare la data di scandenza della materia prima calcolata dal fornitore Thermofisher (è stato richiesto al fornitore di riportare sul CoA, oltre alla D.O.M., anche la data di scadenza), il quale ha fornito gli studi di stabilità (vedere allegato "Stability Studies of the EP1470 Low Glycerol Platinum™ Taq, CG") del prodotto "EP1470 Low Glycerol Platinum™ Taq", codice interno 950-210,che ne garantiscono una stabilità di 36 mesi. ',N'Allungamento della scadenza della TAQ"50 U/ul PLATINUM TAQ" secondo i dati di stabilità forniti da Thermofisher.',N'Continuando ad attribuire la data di scadenza seguendo le indicazioni riportate sull''attuale specifica (basata sui test di stabilità interni a 30 mesi) si limiterebbe la data di scadenza del prodotto (si continuerebbe ad attribuire 30 mesi anzichè 36).',N'950-210 - Low Glycerol Platinum Taq',N'MM
PW
RDM',N'Acquisti
Magazzino
Produzione
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità del prodotto 950-210 non ha nessun impatto sulla scadenza dei prodotti EGSPA.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Aggiornamento dell''assegnazione scadenza al prodotto  SPEC950-210. Impatto positivo: al controllo in accettazione, il tecnico di produzione dovrà riportare la data di scadenza indicata dal fornitore sul CoA, anzichè calcolare 30 mesi dalla data di produzione, e verificare che sia realmente di 36 mesi dalla data di produzione.
Impatto positivo: si possono aggiornare le tempistiche di riordino della materia prima considerando una scadenza di 36 mesi, invece che di 30.',N'06/02/2020
06/02/2020
06/02/2020
31/01/2020
06/02/2020',N'Modifica della specifica SPEC950-210: aggiunta dello spazio dove poter riportare la data di scadenza indicata dal fornire sul CoA e verifica del calcolo che sia realmente 36 mesi dalla D.O.M.
messa in uso docoumenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
formazione
aggiornare le tempistiche di riordino della materia prima considerando una scadenza di 36 mesi, invece che di 30.',N'Dichiarazione risultati test stabilità a 36 mm_950-210 - stability_studies_950-210_ep1470.pdf
spec950-210_04 in uso al 02/09/2020 - cc20-9_messa_in_uso_spec950-210_04_cc20-9.msg
spec950-210_04 in uso al 02/09/2020 - cc20-9_messa_in_uso_spec950-210_04_cc20-9_1.msg
Notifica del cambiamento da parte di thermofisher - cc20-9_thermofischer_notice_of_change.pdf
la modifica non è significativa, pertanto non è da notificare. - cc20-9_mod05,36_non_significativa.pdf',N'RT
QAS
MM
PW
QAS',N'06/02/2020
06/02/2020
06/02/2020
06/02/2020
06/02/2020',NULL,N'02/09/2020
02/09/2020
06/02/2020
24/02/2021
24/02/2021',N'No',N'nessun impatto ',N'No',N'non necessaria',N'No',N'nessun impatto',N'R - Ricevimento / stoccaggio Materiali e Strumenti',N'R5 - controllo di accettazione',N'nessun impatto, la modifica tratta un cambiamento prodotto specifico, non di processo',N'No',N'veder MOD05,36',N'No',N'da valutare',N'No',N'-',NULL,NULL,NULL,'2021-02-24 00:00:00',N'No',NULL,NULL),
    (N'20-8','2020-01-29 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Modifica della specifica SPEC902-744502.205976 (Ed. 3 Rev. 00) "Reagent Macherey-Nagel per INT021EX". In particolare i cambiamenti prevederebbero:
- modifica del nome della specifica: da SPEC902-744502.205976 a SPEC902-INT021EX
- modifica della tabella a pag. 2 utilizzata per il confronto tra la specifica tecnica e un campione di reagente: da strutturare sulla base CoA del produttore in modo che non ci siano discrepanze
- tale confronto dovrebbe essere effettuato dal personale tecnico di produzione (attualmente viene effettuato dal personale di magazzino)',N'Tutte e tre le modifiche richieste migliorerebbero il controllo in accettazione della merce in termini di tempo.',N'Il nome della specifica è fuorviante. Inoltre, la tabella a pag. 2 che confronta la specifica tecnica e il campione è strutturata diversamente rispetto al CoA e questo fa perdere tempo durante il controllo al ricevimento. ',NULL,N'MM
PW
QC',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'nessun impatto se non in termini di attività
Il controllo in accettazione dovrà essere svolto dal personale di produzione
Valutare se la modifica del codice ha impato sulla documentazione del fascicolo tecnico di prodotto.
Valutare se la modifica del codice ha impato sulla documentazione del fascicolo tecnico di prodotto.',NULL,N'Modifica della specifica SPEC902-744502.205976 (Ed. 3 Rev. 00):
- modifica del nome della specifica: da SPEC902-744502.205976 a SPEC902-INT021EX
- modifica della tabella a pag. 2 utilizzata per il confronto tra la specifica tecnica e un campione di reagente: da strutturare sulla base CoA del produttore in modo che non ci siano discrepanze e sul manuale di istruzioni per l''uso per il numero di componenti
- tale confronto dovrebbe essere effettuato dal personale tecnico di produzione (attualmente viene effettuato dal personale di magazzino)
formazione
messa in uso dei documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',NULL,N'RT
MM
QAS
QAS',NULL,NULL,NULL,N'No',N'non applicabile',N'No',N'verificare',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto, la modifica tratta un cambiamento prodotto specifico, non di processo',N'No',N'verificare',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-7','2020-01-24 00:00:00',N'Renata  Catalano',N'Documentazione SGQ
Dismissione prodotto',N'A seguito della commercializzazione con la marcatura CE dei prodotti "COLISTIN-R ELITe MGB Kit", codice RTS202ING-48 e "HEV ELITe MGB Kit", codice RTS130ING e dei prodotti collegati si rende necessaria la dismissione della versione RUO dei prodotti e l''archiviazione della relativa documentazione. 
A seguito della commercializzazione del prodotto RTS400ING "STI PLUS ELITe MGB® Kit" e CTR400ING"  si comunica la dismissione deI prodottI RTS533ING "STI ELITe MGB Panel" e CTR533ING "STI-ELITe Positive Control".',N'Dal momento che la versione CE dei prodotti RTS202ING-48 e RTS130ING e dei prodotti collegati sostituirà la versione RUO precedentemente commercializzata, la dismissione di quest''ultima consente di non avere in uso documentazione non utilizzata.
Disponibilità di un unico prodotto EG SpA per la rilevazione di un pannello di target relativo alle patologie sessualmente trasmesse. In quest''ottica verrà mantenuto il prodotto RTS400ING con il relativo controllo poiché è sviluppato e prodotto  da EGSpA (mentre RTS533ING e relativo controllo sono sviluppati e parzialmente prodotti dall''azienda FAstTrack) e contiene un target in più (Trichomonas vaginalis).',N'La documentazione relativa alle versioni RUO dei prodotti non più commercializzati RTS202ING-48 e RTS130ING e dei prodotti collegati rimarrebbe in uso. Si manterrebero i costi con l''organismo notificato per due prodotti simili.',N'962-CTR533ING - STI-ELITe Positive Control
962-RTS533ING - STI ELITe MGB Panel
962-CTR130ING - HEV  ELITe Positive Control
962-CTR202ING - COLISTIN-R ELITe Pos. Control
962-RTS202ING - COLISTIN ELITe MGB Kit
962-RTS130ING. - HEV ELITe MGB Kit',N'MM
QC
T&CM
MS
QAS
RAS
QCT/DS',N'Controllo qualità
Regolatorio',N'Impatto positivo, la dismissione della versione RUO dei prodotti ed il mantenimento della sola versione CE consente di avere una documentazione unica legata al prodotto relativa solo alla versione CE. L''impatto sugli altri reparti è già stato gestito all''immissione in commercio della versione CE-IVD. Con un unico prodotto per la diagnosi delle malattie sessualmente trasmese si riducono le spese legate al mantenimento dei Certificati di Prodotto e si riduce il lavoro legato al processo di registrazione e di mantenimento dei Fascicoli Tecnici.
Impatto positivo, la dismissione della versione RUO dei prodotti ed il mantenimento della sola versione CE consente di avere una documentazione unica legata al prodotto relativa solo alla versione CE. L''impatto sugli altri reparti è già stato gestito all''immissione in commercio della versione CE-IVD. Con un unico prodotto per la diagnosi delle malattie sessualmente trasmese si riducono le spese legate al mantenimento dei Certificati di Prodotto e si riduce il lavoro',N'17/03/2020
17/03/2020
17/03/2020
17/03/2020
17/03/2020
02/04/2020
02/04/2020',N'Aggiornare i Fascicoli Tecnici di Prodotto FTP202ING e FTP130ING archiviando la documentazione relativa alla versione RUO dei prodotti. Azione già condotta a chiusura dei progetti SCP2018-028 e SCP2018-026. Archiviare FTP RT533ING e CTR533ING. 
compilare il MOD04,12_02_DismissioneProdotto per ciascun prodotto
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Modifica moduli CQ per IC500 MOD10,13-IC500 e MOD10,12-IC500
modifica moduli elenchi MOD10,18 e MOD09,23',N'archiviazione SPEC950-CTR533ING e SPEC950-RTS533ING - cc20-7_archiviazione_spec950-xx_e_spec951-xx_cc20-23_e_cc20-7.msg
Invio NoC di dismissione RTS533ING e CTR533ING a DEKRA - cc20-7_mail_dekra_dismissione_rts533ing.pdf
NoC DEKRA - cc20-7_2020.03.25_noc_sti_discontinuation_elitech_dekra.pdf
elenco clienti che acquistavano RTS533ING - 2020_01_15__sti_panel_discontinuation_clienti.xlsx
òettera dismissioni RTS533ING clienti italia - cc20-7_2020_01_15_rts533ing_discontinuation_clientiit.pdf
lettera dismisioni clienti estero - cc20-7_2020_01_15_rts533ingdiscontinuation_clientien.pdf
la modifica è significativa, perciò da notificare. - cc20-7_mod05,36__significativa.pdf',N'QARA
RAS
QAS
QARA
QAS
QC
QCT/DS
QC
QCT/DS',N'17/03/2020
17/03/2020
17/03/2020
02/04/2020
02/04/2020',N'30/04/2020
30/04/2020',N'17/03/2020',N'No',N'I DMRI dei prodotti sono già stati aggiornati nel corso dei progetti SCP2018-028 e SCP2018-026.',N'Si',N'da valutare azioni condotte/ da condurre per dismissione RUO',N'Si',N'valutare anche impatto su gare in essere',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'nessun impatto. la dismissione segue dei prodotti include il psotto-processo di archiviazione dei documenti (DC12).',N'Si',N'vedere MOD05,36. La notifica non è necessaria per i prodotti RUO che non sono registrati. La notifica è necessaria per i prodotti RTS533ING e CTR533ING.',N'No',N'la notifica non è necessaria in quanto i prodotti non sono presenti nel MOD05,37',N'No',N'la documentazione di gestione dei rischi dei prodotti è stata aggiornata nel corso dei progetti SCP2018-028 e SCP2018-026.',N'Roberta  Paviolo','2020-04-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-6','2020-01-21 00:00:00',N'Renata  Catalano',N'Documentazione SGQ',N'Nel corso dell''audit interno al processo di "Coordinamento vendite estero", Report numero 17-19 è stata assegnata l''OSS1 17-19 che richiede l''aggiornamento della procedura PR19,04 "Gestione vendite export" che risulta essere obsoleta  e non allineata con il processo di gestione vendite export correntemente in uso. In particolare le modifiche richieste sono le seguenti:
- aggiornamento sigle funzioni secondo il mansionario corrente;
- nel paragrafo 3.1 riportare il collegamento con la procedura PR06,01;
- revisione del processo alla luce del fatto che a partire da 01/2020 ELITech Distribution (ED) non esisterà e le attività relative alla linea di biologia molecolare in precedenza gestite da ED (es Contratti, fatturazione, recupero crediti) passeranno sotto la gestione diretta di EG SpA;
- allineamento di quanto descritto in procedura con il processo correntemente in uso (es. utilizzo di SGAT G&O per la gestione delle Price List)
',N'L''aggiornamento della procedura PR19,04 consente di avere la documentazione SGQ relativa al processo gestione vendite export allineata con il processo in essere',N'In caso di mancato aggiornamento della procedura PR19,04, la documentazione SGQ relativa al processo gestione vendite export risulterebbe obsoleta e disallineata rispetto al processo in essere',NULL,N'QARA
QM
S&MM-MDx',N'Regolatorio
Sistema qualità
Vendite Estero',N'Impatto positivo. L''aggiornamento della PR19,04 consente di avere la principale documentazione relativa al processo gestione vendite export allineata con il processo in essere e con le altre procedure del SGQ, garantendo un miglior controllo dei processi aziendali 
Impatto positivo. L''aggiornamento della PR19,04 consente di avere la principale documentazione relativa al processo gestione vendite export allineata con il processo in essere e con le altre procedure del SGQ, garantendo un miglior controllo dei processi aziendali 
Impatto positivo. L''aggiornamento della PR19,04 consente di avere la principale documentazione relativa al processo gestione vendite export allineata con il processo in essere e con le altre procedure del SGQ, garantendo un miglior controllo dei processi aziendali 
Impatto positivo. L''aggiornamento della PR19,04 consente di avere la principale documentazione relativa al processo gestione vendite export allineata con il processo in essere e con le altre procedure del SGQ, garantendo',N'06/02/2020
06/02/2020
06/02/2020
06/02/2020
06/02/2020
06/02/2020',N'Revisionare, approvare e mettere in uso la PR19,04 aggiornata
a seguito di aggiornamento della procedura, valutare se necessario aggiornare i risk assessment SP8, F1, F3, F5 e VMP
Revisionare e approvare la PR19,04 aggiornata.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Aggiornare e approvare la procedura PR19,04. Effettuare la formazione al personale coinvolto nel processo.',NULL,N'QM
QARA
S&MM-MDx
QAS
QAS
QAS',N'06/02/2020
06/02/2020
06/02/2020
06/02/2020
06/02/2020',NULL,NULL,N'No',N'L''aggiornamento della PR19,04 non richiede l''aggiornamento di alcun DMRI di prodotto',N'No',N'L''aggiornamento della PR19,04 non richiede nessuna comunicazione al cliente',N'No',N'L''aggiornamento della PR19,04 non ha alcun impatto sul prodotto immesso in commercio',N'F - Gestione dei Fornitori
F - Gestione dei Fornitori
F - Gestione dei Fornitori
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'F1 - Identificazione del fornitore
F3 - Selezione nuovo fornitore
F5 - Qualificazione del nuovo fornitore
SP8 - Ordine da EGSpA',N'verificare l''impatto dell''utilizzo di SGAT G&O per la gestione delle Price List sul processo di gestione degli ordini al cliente (SP8), verificare se è necessario introdurre nei risk assessment di identificazione, selezione e qualifica dei fornitori anche i distributori, o se questi sono trattati come una sotto categoria dei fornitori.',N'No',N'vedere, MOD05,36',N'No',N'Non necessaria, la modifica non è relativa ad un prodotto ma ad un processo che non riguarda i prodotti sviluppati in OEM',N'No',N'Non necessario alcun intervento, la modifica non è relativa ad un prodotto ma ad un processo che non è trattato nella documentazione di gestione del rischio',N'Renata  Catalano',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'20-5','2020-01-17 00:00:00',N'Samuela Margio',N'Progetto',N'Si richiede la variazione del formato dei prodotti RTS600ING HIV1 ELITe MGB Kit (SCP2019-021), RTS601ING HCV ELITe MGB Kit (SCP2019-020) e RTS602ING HBV ELITe MGB Kit (SCP2019-022) attualmente in fase di selezione del candidato e non ancora commercializzati come descritto nell''allegato HIV1-HCV-HBV_New Packaging Proposal_17012020.
Il formato attuale prevede 3 prodotti (RTS, CTR e STD) da utilizzarsi in associazione a CTRCPE. Il formato proposto sarà un prodotto unico, RTK, costituito da più componenti RTS, CTR, STD e CPE.',N'VINCOLO DEI LOTTI RILASCIATI DA PEI: il rilascio dei Lotti per la vendita effettuato da PEI (Paul Ehlrich Institut) con buona probabilità prevederà un unico certificato nel quale i lotti di RTS (amplificazione), STD (Standard), CTR (Controllo Positivo) e CPE (Controllo Interno) verranno testati e rilasciati unitamente.  
CQ SUL CONTROLLO INTERNO: implementazione del CQ sul CPE includendo anche HIV, HBV e HCV attualmente non testati.
OTTIMIZZAZIONE SPESA X CERTIFICAZIONE LOTTI: ogni lotto rilasciato da PEI sarà quotato (4 codici ? 4 quotazioni vs 1 codice ? 1 quotazione)
MAGGIORE SICUREZZA D''USO PER END USER: abbattere il rischio che il cliente mischi lotti dei singoli reagenti aventi unico CQ e certificato PEI con reagenti testati certificati in momenti diversi. ',N'Il rischio è un aumento della costificazione del prodotto legata al pagamento della certificazione per ogni singolo componente invece che per ogni singolo kit.
Appesantimento del CQ sul CPE (componente comune ad altri prodotti) che dovrebbe essere testato anche per HIV, HBV e HCV, il che renderebbe il CQ piuttosto complesso. Creando invece un formato unico con un CPE specifico per ogni prodotti il CQ verrebbe fatto unitamente al controllo del lotto di RTS, STD e CTR.
Rischio che il cliente possa utilizzare componenti appartenenti a lotti diversi (non certificati e rilasciati insieme tra loro). ',NULL,N'MM
PMS
PW
QARA
QC
RDM
BDM-MDx
P&MMI
PVC',N'Acquisti
Controllo qualità
Marketing
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema informatico',N'Gestione approvvigionamento di nuovi codici (scatola esterna). Rischio analogo a quanto già previsto in procedura.
La modalità di CQ dei nuovi prodotti in oggetto differità dai normali prodotti EGSPA, prevedendo il controlo del kit completo. Questa procedura è già in uso per i prodoti R-Bio.
Da un punto di vista di controllo qualità il kit unico garantisce una migliore tracciabilità delle performance dei componenti il cui uso sarà vincolato dal lotto e dal formato.
Definizione di formato e quantità x kit DEMO (ridotti). 
Utilizzo di un nuovo flusso di lavoro per produzione kit. 
Ripianificazione delle attività di progetto.
Ripianificazione delle attività di progetto.
L''impatto è positivo in quanto il nuovo formato proposto  dovrebbe facilitare il rilascio dei lotti per la vendita da parte di PEI e ridurre i costi di certificazione.
Definizione nuovo CQ e aggiornamento documentazione in corso (Product Requirement Document)
Definizione nuovo CQ e aggiornamento documentazione in cors',N'17/01/2020
20/01/2020
20/01/2020
17/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020',N'approvvigionamento nuova confezione esterna
Implementazione documenti di produzione e CQ per il kit unico.
Definizione nuovo formato Kit DEMO
Assegnazione nuova codifica (961 o 957, 910, ...) a componenti e lotto finale
Aggiornamento MRD e Piano Progetto
Valutazione dell''impatto sulla costificazione del prodotto
Aggiornamento Bozza Regulatory Assessment e Requisiti Essenziali
Creazione Bozza mod Prod e CQ 
Aggiornamento Bozza Product Requirement Document
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Approvvigionamento nuova scatola esterna
Definizione nuovo CQ
creazione bozza moduli prod e CQ 
Definizione nuovo formato kit DEMO 
Definizione flusso di lavoro in produzione e codici 962
Aggiornamento MRD e Piano Progetto
Aggiornamento della documentazione in corso (Regulatory Assessment e Essential Requirements)
Aggiornamento Product Requirement Document
Implementazione nuove eti',N' - hiv1-hcv-hbv_new_packaging_proposal_17012020.pptx
nuove etichette implementate (esempio MOD04,13RTK602ING)   - cc20-5_aggiornamentostato11-2020_1.msg
SPEC954-195, scatola MOD G, 730 x 950 x H650 stoccata a magazzino - cc20-5_ggiornamentostato11-2020.msg
esempio moduli prod e cq del prodotto HBV, RTK602ING (SCP2019-022), messi in uso a metà ottobre ed a inizio novembre - cc20-5_ggiornamentostato11-2020_1.msg
esempio moduli prod e cq del prodotto HBV, RTK602ING (SCP2019-022), messi in uso a metà ottobre ed a inizio novembre	 - cc20-5_ggiornamentostato11-2020_6.msg
Assegnato il codice 964 e distinte base aggiornate in Navision per i codici RTK600ING, RTK601ING e RTK602ING  - cc20-5_ggiornamentostato11-2020_2.msg
HIV1-HCV-HBV_New Packaging Proposal_17012020 - hiv1-hcv-hbv_new_packaging_proposal_17012020_1.pptx
: MRD 2019-003 Rev.01, MRD2019-005 Rev.01, MRD2019-004 Rev.01. - cc20-5_ggiornamentostato11-2020_3.msg
Bozza Regulatory Assessment e Requisiti Essenziali svolta - cc20-5_ggiornamentostato11-2020_5.msg
aggiornati PRD2019-012, PRD2019-010, PRD2019-011. - cc20-5_aggiornamentostato11-2020.msg
Costificazione effettuata dal Finance, dati presenti su Navision. - cc20-5_ggiornamentostato11-2020_4.msg
non significativa, i prodotti non sono ancora stati  immessi in commercio. - cc20-5_mod05,36_non_significativa.pdf',N'PW
QC
RDM
RT
MM
QC
RT
BDM-MDx
MM
QC
RT
PMS
RAS
RT
ITT
QM
PW
QC
RDM
RT
QCT/DS
MM
QC
RDM
RT
PMS
BDM-MDx
MM
PMS
QM
RDM
FAM
PMS
RAS
RDM
RT
BC
PT
P&MMI
QARA
QAS',N'17/01/2020
20/01/2020
20/01/2020
17/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020
20/01/2020',N'02/03/2020',N'20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020
20/11/2020',N'No',N'nessun impatto, i prodotti sono in fase di progettazione',N'No',N'i prodotti sono in fase di progettazione',N'No',N'i prodotti non sono immessi in commercio , sono in fase di progettazione',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.5 - Fase di Pianificazione e Selezione del Prototipo',N'valutare',N'No',N'non necessaria, i prodotti sono in fase di progettazione',N'No',N'nessun virtual manufacturer',N'No',N'i prodotti sono in fase di progettazione',N'Samuela Margio','2020-03-02 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-4','2020-01-17 00:00:00',N'Paolo Saracco',N'Etichette',N'Eliminazione del Package Insert dalla confezione e introduzione sull''etichetta delle istruzioni per scaricare le IFU dal sito.
',N'Risparmio di materiale e mandopera',N'Costi aggiuntivi',N'910-RTSACC01 - Q-PCR Microplates Sconto Merce
910-RTSACC02 - Q-PCR Micropl FAST sc merce',N'MM
QC
QM
P&MMI',N'Controllo qualità
Produzione
Sistema qualità',N'Impatto in termini di attività.
Impatto positivo: risparmio di materiale e mandopera
nessun impatto se non in termini di attività
nessun impatto se non in termini di attività',N'20/01/2020
17/01/2020
17/01/2020
17/01/2020',N'Aggiornare MOD910-RTSACC01_01 e MOD910-RTSACC02_01 eliminando l''inserimento del pakage insert nella confezione.
formazione del personale
approvazione dei moduli
messa in uso dei moduli
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020 - cc20-4_messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020.docx
messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020 - cc20-4_messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020_1.docx
messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020 - cc20-4_messa_in_uso_mod910-rtsaccxx_02_mod18,02_del_19-02-2020_2.docx
la modifica non è significativa, pertanto non è necessario effettuare alcuna notifica. - cc20-4_mod05,36_non_significativa.pdf',N'QC
QCT/DS
MM
QAS
QAS',N'20/01/2020
17/01/2020
17/01/2020
17/01/2020',N'31/01/2020
31/03/2020',N'19/02/2020
03/03/2020
03/03/2020
20/01/2020',N'No',N'nessun impatto',N'No',N'sufficienti le informazioni per scaricre l''IFU elettronica presenti sull''etichetta,come per gli altri prodotti di EGSpA.',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36. Secondo la linea guida NBOG BPG 2014-3, paragrafo "Changes Labelling", l''aggiornamento delll''etichetta per introdurre le informazioni per scaricare l''IFU non modifica, nè limita l''uso previsto del prodotto, e non è un cambiamento che necessita di approvazione da parte dell''ìorganisom notificato. Pertanto si può considerare una modifica non significativa, non da notificare alle autorità competenti.',N'No',N'nessun virtual manufacturer',N'No',N'no',NULL,'2020-03-31 00:00:00',NULL,'2020-03-03 00:00:00',N'No',NULL,NULL),
    (N'20-3','2020-01-09 00:00:00',N'Paola Gonzalez Ruiz',N'Manuale di istruzioni per l''uso',N'Calcolo dei valori di LoD per le nuove matrici validate per il kit MTB/MDR: Urine, Cavity fluids, Biopsies, Gastric aspirate, da inserire sul manuale di istruzioni per l''uso.',N'I dati di LoD non sono essenziali per il rilascio delle estensioni d''uso, tuttavia una loro integrazione sulle IFU migliora l''utilizzabilità del prodotto. ',N'1. Informazioni sulle prestazione del prodotto incomplete. 
2. Disomogeneità rispetto a tutte le IFU dei nostri prodotti.',N'962-RTS120ING - MDR TB ELITe MGB Kit',N'QARA
GPS
MS
PVC',N'Assistenza applicativa
Marketing
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo, per la completezza delle informazioni sulle prestazioni del prodotto.
Impatto positivo, per la completezza delle informazioni sulle prestazioni del prodotto.
Impatto positivo, per la completezza delle informazioni sulle prestazioni del prodotto.
Impatto positivo, per la completezza delle informazioni sulle prestazioni del prodotto.
Impatto positivo, per la completezza delle informazioni sulle prestazioni del prodotto.
Aggiornamento IFU
Completezza delle informazioni sulle prestazioni del prodotto.
Completezza delle informazioni sulle prestazioni del prodotto.
Completezza delle informazioni sulle prestazioni del prodotto.',N'24/01/2020
29/01/2020
29/01/2020
29/01/2020
29/01/2020
06/02/2020
06/02/2020
29/01/2020',N'Aggiornamento TAB
Aggiornamento brochure/ppt/eventuale materiale.
Mettere in uso IFU
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Aggiornamento FTP e valutare aggiornamento analisi dei rischi
Calolare gli LoD da riportare sul manuale ed aggiornare il REP2019-041 "VERIFICATION REPORT MDR/MTB ELITe MGB® Kit, RTS120ING MDR/MTB - ELITe Positive Control, CTR120ING ELITe InGenius®, INT030 BAL/BA and non-pulmonary samples SCP2018-007" ed il REP2019-052 "SUMMARY VERIFICATION AND VALIDATION REPORT MDR/MTB ELITe MGB® Kit, RTS120ING MDR/MTB - ELITe Positive Control, CTR120ING ELITe InGenius®, INT030 BAL/BA and non-pulmonary samples CP2018-007". 
Revisionare TAB ed aggiornare MOD05,35
Verificare gli LoD ed approvare il manuale di istruzioni ed i REP2019-041 e REP2019-052.',N'TAB P24 Rev. AD, MAIL INVIATA IL 7/7/2020 - cc20-3_elite_ingenius_-_tab_p24_-_rev_ad_-__product_mdr_mtb_elite_mgb_notice.pdf
IFU RTS120ING_03 - cc20-3_ifu_rts120ing_cc20-3_1.msg
La modifica non è significativa, pertanto non deve essere notificata. - cc20-3_mod05,36_non_significativa.pdf
FTP AGGIORNATO - cc20-3_aggiornamento_ftp_di_mdrmtb.msg',N'GASC
GPS
PMI
RAS
QARA
QAS
PVC
RDM
RT
RAS
PVC',N'24/01/2020
29/01/2020
29/01/2020
29/01/2020
29/01/2020
29/01/2020
29/01/2020
29/01/2020',N'29/01/2020
20/03/2020
28/02/2020
20/03/2020',N'07/07/2020
06/03/2020
29/01/2020',N'No',N'nessun impatto',N'No',N'sufficiente l''avvertenza sul IFU',N'No',N'nessun impatto',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL1 - Fase di pianificazione: pianificazione',N'valutare ',N'No',N'vedere MOD05,36. L''aggiornamento del manuale di sitruzioni per l''uso con i valori di LoD non introducono controindicazioni, nè hanno impatto sulla sicurezza ed efficacia del prodotto.',N'No',N'nessun virtual manufacturer',N'No',N'La modifica non è imputabile ad un errore nella definizione dei requisiti o delle prove da fare ma ad un cambiamento che migliora l''utilizzabilità del prodotto, questo non impatta sulla gestione del rischio',NULL,'2020-03-20 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'20-2','2020-01-07 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Rilascio della nuova versione del SW SGAT, V02.03.00, aggiornata per:
- migliorare il processo di gestione delle password (sicurezza) , 
- migliorare le attività service ed applicative, 
- migliorare la gestione dei KPI
- attivazione del modulo di "Magazzino" per la gestione dei movimenti e carico/scarico strumenti.
Per il dettaglio degli aggiornamenti vedere il documento di VOLOS prodotto per il rilascio della verrsione test "SGAT ELITECH V02.03.00 RELEASE NOTES DEL 09/12/2019".',N'Miglioramento nell''utilizzabilità del sw. Automazione nella gestione del magazzino strumenti e della loro tracciabilità.',N'Mancato aggiornamento tecnologico, utilizzo di un sistema manuale obsoleto per la getsione del magazzino e movimentazione strumenti',NULL,N'CSC
GSC
QARA',N'Assistenza strumenti esterni (Italia)
Global Service
Regolatorio
Sistema informatico
Sistema qualità',N'L''impatto è positivo, in quanto permette l''aggiornamento tecnologio del sw.
L''attivazione del modulo di "Magazzino" per la gestione dei movimenti e carico/scarico strumenti ha un impatto positivo in quanto permette l''automazione del processo.
Nessun impatto, se non in termini di attività
Nessun impatto, se non in termini di attività
Nessun impatto
nessun impatto,m se non in termini di attività
nessun impatto,m se non in termini di attività',N'07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020',N'aggiornamento del risk assessment
installazione da parte del fornitore qualificato VOLOS della nuova versione in ambiente di test
verifica delle modifiche in ambiente di test
verifica del rilascio da parte di VOLOS della documentazione di validazione (test plan e report, video della validazione, relase notes delsw "live", user guide)
formazione agli utilizzatori
creazione ed invio TSB
aggiornamento del risk assessment
installazione da parte del fornitore qualificato VOLOS della nuova versione in ambiente di test
verifica delle modifiche in ambiente di test
formazione agli utilizzatori
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Approvazione del risk assessment aggiornato
nessun aattività a carico.
messa in uso SW32, ed aggiornamento VMP con il nuovo risk assessment
eventuale aggiornamento del VMP',N'Releases notes vOLOS - cc20-2_sgat_elitech_-_023_-_a_-_v2.03.00_-_release_notes_1.pdf
Functional Specifications and Impact Analysis - VOLOS - cc20-2_sgat_elitech_-_024_-_a_-_v2.03.00_-_functional_specifications_and_impact_analysis.pdf
TEST REPORT VOLOS - cc20-2_sgat_elitech_-_026_-_a_-_v02.03.00_test_report.xlsx
Mnuale utente - cc20-2_sgat_evo_02.03_user_guide.pdf
SGAT versione 2.03.00 in uso  - cc20-2_messa_in_uso_sgq.docx
messa in uso presso personale esterno EGSpA - cc20-2_messa_in_uso_sgat_evo_versione_v2.03.00_04-02-20_.pdf
messa in uso sgat evo versione  2.03.00 - cc20-2_messa_in_uso_sgq_1.docx
RA SW32 firmato - mod09,27_01_sw32_sgat_evo_modulomagazzino-cc20-2.pdf
Modifica non significativa, pertanto non necessita di notifica - cc20-2_non_significativa.pdf
ra sw32 firmato - mod09,27_01_sw32_sgat_evo_modulomagazzino-cc20-2_1.pdf',N'GSC
CSC
QAS
QAS
QARA
QAS
ITM
QARA',N'30/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020',N'28/02/2020
28/02/2020
28/02/2020',N'04/02/2020
04/02/2020
07/01/2020
03/02/2020
07/01/2020
04/02/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'non applicabile',N'SW - Elenco Software',N'SW32 - sgat (Software globale per la gestione delle richieste di assistenza tecnica sugli strumenti )',N'aggiornare il risk assessment con l''inroduzione delle nuove fiunizonalità del sw',N'No',N'non necessaria, vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'non applicabile',N'Marcello Pedrazzini','2020-02-28 00:00:00',NULL,'2020-02-04 00:00:00',N'No',NULL,NULL),
    (N'20-1','2020-01-02 00:00:00',N'Andrea  Colantuono',N'Documentazione SGQ',N'Modifiche IO11,13, IO 11,12, IO11,17 e IO11,14 per servizio tecnico ',N'Aggiornamento per introduzione nuovi passaggi, i sommari e la taella delle revisione.',N'Documentazione obsoleta nel layout.',N'972-INT030 - ELITe InGenius™',N'CSC
T&GSC',N'Assistenza strumenti esterni (Italia)
Assistenza strumenti interni
Global Service
Sistema qualità',N'Fruizione più efficace dell''informazione ai tecnici dei distributori/ICO/interni 
Fruizione più efficace dell''informazione ai tecnici dei distributori/ICO/interni 
Miglioramento delle attività di installazione, manutenzione , decontaminazione e disinstallazione eseguite dai tecnici Italia
Miglioramento delle attività di installazione, manutenzione , decontaminazione e disinstallazione eseguite sugli strumenti interni
nessun impattto se non in termini di attività.
nessun impattto se non in termini di attività.
nessun impattto se non in termini di attività.',N'07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020',N'Modifiche IO11,13, IO 11,12, IO11,17 e IO11,14 
fromazione agli FSE, T&GSC e T&GSS
nessuna attività a carico
nessuna attività a carico
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'IO11,14_01 e MOD19,24_01 - cc20-1_messa_in_uso_io1114_mod1924_rev01_cc20-1.msg
 IO11,12_04, IO11,13_03, MOD11,16_03_MOD11,27_01 - cc20-1_messa_in_uso_io1112_04_io1113_03_mod1116_03_mod1127_01.msg
IO11,14_01_MOD19,24_01 - cc20-1_messa_in_uso_io1114_mod1924_rev01_cc20-1.msg
 IO11,12_04, IO11,13_03, MOD11,16_03_MOD11,27_01 - cc20-1_messa_in_uso_io1112_04_io1113_03_mod1116_03_mod1127_01_1.msg
La modifica non è significativa, pertanto non deve essere notificata. - cc20-1_non_significativa.pdf',N'FSE
GSC
GSTL
CSC
T&GSC
QAS
QAS',N'07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020
07/01/2020',NULL,N'07/01/2020
07/01/2020
07/01/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non applicabile',N'No',N'vedere MOD05,36',N'No',N'nessun impatto',N'No',N'non applicabile',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-52','2019-12-13 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Aggiornamento procedura PR14,03, modulo MIR (Manufacturing Incident Report) (_ALLEGATO 3_PR14,03_Report Form INCIDENT) e template FSN (__ALLEGATO 5...)',N'Allineamento con la nuova linea guida MEDDEV 2.12/1 rev.8. e con i nuovi moduli MIR',N'Non conformità del processo di vigilanza con la linea guida MEDDEV 2.12/1 rev.8',NULL,N'QARA',N'Regolatorio',N'L''impatto sul sistema qualità è tale da consentire l''adeguamento alle linee guida europee in materia di vigilanza.
L''impatto sul sistema qualità è tale da consentire l''adeguamento alle linee guida europee in materia di vigilanza.
L''impatto sul sistema qualità è tale da consentire l''adeguamento alle linee guida europee in materia di vigilanza.',N'13/12/2019
13/12/2019
13/12/2019',N'Aggiornare ALLEGATO 3_PR14,03_Report Form INCIDENT
Aggiornare _PR14,03 ALLEGATO 5-ITA - FSN rev.01 e _PR14,03 ALLEGATO 5-ENG - FSN rev.01
Aggiornare la procedura PR14,03 NotificaAutoritàCompetenteERitiroDelLotto',N'allegato 3 PR14,03 version 7.2.1 - cc19-52_messa_in_uso_allegati_pr14,03.docx
allegato 5 PR14,03 EN e ITA rev02 - cc19-52_messa_in_uso_allegati_pr14,03_1.docx
l''aggiornamento della procedura è minimo (introdotta solo una frase esplicativa sull''uso dell''allegato 5), si ritiene pertanto non necessario metterla in uso, ma attendere aggiornamenti più corposi. - cc19-52_messa_in_uso_procedura_in_futuro_per_revisioni_piu_corpose_1.txt',N'RAS
RAS
RAS',N'13/12/2019
13/12/2019
13/12/2019',N'28/02/2020
28/02/2020
28/02/2020',N'03/02/2020
03/02/2020
03/02/2020',N'No',N'NA',N'No',N'NA',N'No',N'NA',N'ASA - Assistenza applicativa prodotti',N'ASA3 - Gestione reclamo con impatto sulla vigilanza (FSCA)',N'Il profilo di rischio del processo di vigilanza ASA3 non è cambiato, trattandosi della sostituzione di un modulo pre-esistente all''interno della procedura.',N'No',N'NA',N'No',N'NA',N'No',N'NA',N'Roberta  Paviolo','2020-02-28 00:00:00',NULL,'2020-02-03 00:00:00',N'No',NULL,NULL),
    (N'19-51','2019-12-13 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 12 a 24 mesi del prodotto MDR/MTB ELITe MGB® Kit (RTS120ING) sulla base dei dati di stabilità reale, come comunicato da R&D in data 12/12/2019',N'Aggiungere 12 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS120ING - MDR TB ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è
sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS120ING (STB2019-164, U0817AS, -20°C per 24 mesi, firmato il 10/01/2020, STB2019-163, U0717BC, -20°C per 24 mesi, firmato il 10/01/2020, STB2019-188, U1017BO, -20°C per 24 mesi, firmato il 19/12/2019) non c''è nessun impatto sulle prestazioni del prodotto.',N'10/01/2020
03/01/2020
10/01/2020
03/01/2020
03/01/2020
10/01/2020',N'nessuna attività 
Messa in uso documento modificato.
Aggiornamento del modulo di produzione MOD962-RTS120ING, modificando la frase "attribuire la scadenza del Prodotto (massimo 12 mesi)" in "attribuire la scadenza del Prodotto (massimo xx mesi)".
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Report stabilità archiviati nel FTP
Nessuna altra azione a carico di R&D.',N'MOD962-RTS120ING_REV02 - cc19-51_messa_in_uso_mod962-rts120ing_02_cc19-51_rac19-20_mod1802_08012020.msg
la notifica è significativa, da notificare nei paesi in cui il prodotto è registrato - cc19-51_significativa.pdf
notifica preliminare. - cc19-51_notification_of_change_-rts120ing.msg',N'PT
QAS
QCT/DS
QAS
RAS
RDM',N'10/01/2020
03/01/2020
10/01/2020
03/01/2020
03/01/2020
03/01/2020',N'31/01/2020
03/01/2020',N'10/01/2020
17/02/2020
07/01/2020
20/02/2020
10/01/2020
10/01/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul risk assessment R&D1,9',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufaturer',N'No',N'-',NULL,'2020-01-31 00:00:00',NULL,'2020-02-20 00:00:00',N'No',NULL,NULL),
    (N'19-50','2019-12-10 00:00:00',N'Manuela  Consalvo',N'Documentazione SGQ',N'Si richiede l''aggiornamento dei moduli di preparazione dell''RNA di West Nile Virus (utilizzato come campione per il CQ). ',N'Miglioramento nella manipolazione dell''RNA. 961 codice in disuso.',N'Possibile errore nell''utilizzo dei tubi non RNAsi free durante la preparazione. Codice 961 non più utilizzato per la codifica nella fase di dispensazione, rimarrebbe esclusivamente per questo campione.',N'955-RNAWNV-5 - RNA WNV 2x10^5 c/µL
955-RNAWNV-PLD2 - RNA WNV PLD2 1x10^1 c/µL
955-RNAWNV-PLD3 - RNA WNV PLD3 1x10^2 c/µL
955-RNAWNV-PLD4 - RNA WNV PLD4 1x10^3 c/µL
955-RNAWNV-PLD5 - RNA WNV PLD5 1x10^4 c/µL
961-RNAWNV-10 - RNA WNV 10 c/µL',N'MM
QARA
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Miglioramento nella qualità dei campioni di riferimento per il CQ
Miglioramento delle indicazioni della modalità di preparazione
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività',N'10/12/2019
03/01/2020
03/01/2020',N'Modifica dei documenti: 
MOD955-RNAWNV-5
MOD961-RNAWNV-10
MOD955-RNAWNV-10-DiluizioneRNA
Formazione.
Simulazione.
Messa in uso dei moduli.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N' MODOULI MODIFICATI, formazione del 05/02/2020 - cc19-50messa_in_uso_moduli_wnv_cc19-50_mod1802_05022020.msg
SIMULAZIONE EFFETTUATA - cc19-50messa_in_uso_moduli_wnv_cc19-50_mod1802_05022020_1.msg
MOD955-RNAWNV-5_02, MOD955-RNAWNV-10-DiluizioneRNA_03, MOD957-RNAWNV-10_00, archiviato MOD961-RNAWNV-10 - cc19-50messa_in_uso_moduli_wnv_cc19-50_mod1802_05022020_2.msg
La modifica non è significativa, pertanto non è da notificare presso le autorità competenti. - cc19-50_mod05,36_non_significativa.pdf',N'QC
QCT/DS
MM
PCT
QARA
QM
QAS
QAS',N'10/12/2019
10/12/2019
10/12/2019',NULL,N'17/02/2020
17/02/2020
17/02/2020
17/02/2020',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',N'P3.3 - Identificazione materiali e consumabili',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'non necessaria',NULL,NULL,NULL,'2020-02-17 00:00:00',N'No',NULL,NULL),
    (N'19-49','2019-12-09 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'aggiornamento della SDS n.89 indicando il nuovo ref. RTS150ING (Pneumocystis ELIte MGB Kit)',N'SDS aggiornate con l''ultimo prodotto rilasciato sul mercato',N'la documentazione non sarebbe aggiornata e gli utilizzatori del nuovo prodotto non avrebbero una SDS a cui far riferimento',NULL,N'QARA',N'Regolatorio',N'aggiornamento SDS
aggiornamento SDS',N'09/12/2019
09/12/2019',N'aggiornamento SDS n. 89
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'sda_89_rev01 - cc19-49_messa_in_uso_sds_n.89_rev.01_cc19-49.msg
L''SDS è aggiornata per l''aggiunta di un nuovo prodotto, non per modifiche ai contenuti, si reputa pertanto non necessrio inviare notifica. - cc19-49_non_significativa.pdf',N'RAS
QAS',N'09/12/2019
09/12/2019',N'30/12/2019
20/12/2019',N'09/12/2019
09/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessuno',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'nessun impatto, aggiornamento di una sds con l''aggiunta di un nuovo prodotto',N'No',N'vedere MOD05,23. La SDS non non è modificata nei contenuti, ma è aggiunto un nuovo prodotto, si reputa non necessario effettuare notifiche.',N'No',N'nessun virtual manufacturer',N'No',N'nessun immpatto',N'Michela Boi','2019-12-30 00:00:00',NULL,'2019-12-09 00:00:00',N'No',NULL,NULL),
    (N'19-48','2019-12-03 00:00:00',N'Marta Carena',N'Documentazione SGQ',N'Si richiede di aggiornare la procedura PR19,03 "ProgettiScientificiEAttività Promozionali" per dividerla in due parti: una relativa alle collaborazione scientifiche e una relativa alle attività promozionali, nel rispetto del nuovo codice etico Assobiomedica.',N'Rispetto del nuovo codice etico Assobiomedica',N'Procedura obsoleta',NULL,N'QARA
QM
T&CM
P&MMI
PVC',N'Gare
Marketing
Regolatorio
Sistema qualità
Validazioni',N'Piena compliance del nuovo codice etico Assobiomedica
Procedura con riferimento diretto al Marketing nel rispetto del nuovo processo
Rispetto del codice etico Assobiomedica
Rispetto del codice etico Assobiomedica
Procedura con riferimento diretto alle Validazioni nel rispetto del nuovo processo
Nessun impatto se non in termni di attività.',N'05/12/2019
05/12/2019
05/12/2019
05/12/2019
05/12/2019
03/12/2019',N'revisione delle procedure
creare una nuova procedura  relativa alle attività promozionali
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
revisionare le 2 procedure
creare una nuova procedura  relativa ai progetti scientifici
codifica e messa in uso procedure',N'PR19,06_00 - cc19-48_messa_in_uso_pr1906_00_attivita_promozionali_cc19-48_mod1802_del_31072020.msg
PR19,06_00, IN USO AL 05/08/2020 - cc19-48_messa_in_uso_pr1906_00_attivita_promozionali_cc19-48_mod1802_del_31072020_1.msg
 La modifica non è significativa, pertanto non è da notificare presso alcuna autorità competente.  - cc19-48_mod05,36_non_significativa_1.pdf
PR19,06_00, IN USO AL 05/08/2020 - cc19-48_messa_in_uso_pr1906_00_attivita_promozionali_cc19-48_mod1802_del_31072020_3.msg
PR19,06_00, IN USO AL 05/08/2020 - cc19-48_messa_in_uso_pr1906_00_attivita_promozionali_cc19-48_mod1802_del_31072020_2.msg',N'T&CM
MAI
QAS
PVC
QAS
QARA',N'05/12/2019
05/12/2019
05/12/2019
05/12/2019
05/12/2019
05/12/2019',NULL,N'05/08/2020
03/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun ra associato',N'No',N'vedere MOD05,36',N'No',N'non necessaria',N'No',N'non applcabile',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-47','2019-11-20 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede l''introduzione di un''istruzione operativa che descriva le attività svolte dalla funzione di Document specialist per la creazione, modifica e correzione dei documenti operativi quali moduli di produzione, controllo qualità e moduli/istruzioni relativi agli strumenti.',N'L''istruzione operativa è necessaria per tracciare il flusso di lavoro svolto per la creazione, modifica e correzione dei documenti operativi. Descrive l''utilizzo delle varie cartelle durante le attività, definisce il flusso di lavoro sui moduli di produzione e CQ utilizzati per i lotti pilota ed i controllo effettuati sul DHR durante il Design Transfer.',N'La mancanza di un''istruzione operativa sulla complessa attività di gestione dei documenti operativi può far insorgere dubbi durante le varie fasi',NULL,N'MM
QC
QM
RDM
QAS
T&GSC
QCT/DS',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo: DS può verificare le attività dubbie sull''istruzione operativa
L''istruzione operativa chiarisce il flusso di lavoro durante la revisione dei documetni LP e le interazioni con R&D.
Impatto positivo in quanto l''istruzione operativa descrive i flussi operativi di DS con i vari reparti nell''attività di modifica, revisione e correzione dei documenti operativi, dettagliando anche le tempistiche di esecuzione della modifica.',N'22/11/2019
22/11/2019',N'Stesura della IO di Revisione Documenti Operativi gestita da DS
Formazione
Approvazione dell''IO
Attribuzione di un ref alla nuova IO per DS. 
Approvazione della IO per le parti di competenza
Messa in uso della IO',N'IO05,07_00, MOD18,02 DEL 2/12/19 - cc19-47_messa_in_uso_io0507_00rev_doc_operativi_cc19-47_mod1802_021219.msg
 IO05,07_00, MOD18,02 DEL 2/12/19  - cc19-47_messa_in_uso_io0507_00rev_doc_operativi_cc19-47_mod1802_021219_1.msg
 IO05,07_00, MOD18,02 DEL 2/12/19  - cc19-47_messa_in_uso_io0507_00rev_doc_operativi_cc19-47_mod1802_021219_2.msg',N'QC
QCT/DS
RDM
QM
QAS',N'20/11/2019
22/11/2019
22/11/2019',N'20/12/2019',N'04/12/2019
04/12/2019
04/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC2 - Redazione documenti',N'nessun impatto, si descrivono le attività legate a modifica revisione e correzione dei documenti operativi  in un''istruzione operativa. non sono introdotti altre misure di controllo.',N'No',N'vedere MOD05,36, la modifica non è significativa, non è da notificare.',N'No',N'nessun impato',N'No',N'NA',NULL,'2019-12-20 00:00:00',NULL,'2019-12-04 00:00:00',N'No',NULL,NULL),
    (N'19-46','2019-11-15 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 18 a 24 mesi del prodotto ESBL ELITe MGB KIT (RTS201ING) sulla base dei dati di stabilità reale, come comunicato da R&D in data 25/10/2019',N'Aggiungere 6 mesi alla durata del prodotto',N'Restare con la scedenza di 18 mesi',N'962-RTS201ING - ESBL ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente e economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
L''estensione della stabilità permette una produzione più efficiente e economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS201ING (STB2019-011, lotto U1016AZ -20°C per 24 mesi, STB2018-035, lotto U0916AW -20°C per 24 mesi, STB2019-055 U1116BC -20°C per 24 mesi) non c''è nessun impatto sulle prestazioni del prodotto.',N'15/11/2019
15/11/2019
15/11/2019
15/11/2019
15/11/2019
15/11/2019',N'Rietichettare il lotto U0219BB cambiando la scadenza in 2021/02
Messa in uso documento modificato
Aggiornamento del modulo di produzione MOD962-RTS201ING,modificando la frase "attribuire la scadenza del Prodotto (massimo 18 mesi)" in "attribuire la scadenza del Prodotto (massimo 24 mesi)".
Verificare la significatività della  modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Nessuna altra azione a carico di R&D.',N'Rilavorazione - cc19-46_rilavorazioneu0219bbinu1119aw.pdf
MOD962-RTS201ING_02 - cc19-46_messa_in_uso__mod962-rts201ing_02_cc19-46_rac19-20_mod1802_del_91219.msg
MOD962-RTS201ING_02 formazione del 9/12/19 - cc19-46_messa_in_uso__mod962-rts201ing_02_cc19-46_rac19-20_mod1802_del_91219_1.msg
La modifica è significativa pertanto da notificare nei paesi presso cui il prodotto è registrato. - cc19-46_mod05,36_significativa.pdf
Notifica - cc19-46_notification_of_change_-rts201ing_-_cc19-46.msg',N'PP
PT
QAS
QC
QAS
RAS
RDM',N'15/11/2019
15/11/2019
15/11/2019
15/11/2019
15/11/2019',NULL,N'25/11/2019
09/12/2019
09/12/2019
06/12/2019
15/11/2019',N'No',N'nessun impatto sul DMRI',N'No',N'non necessaria',N'No',N'nessun impatto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.5 - Assegnazione scadenza',N'nessun impatto',N'Si',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'non necessario',NULL,NULL,NULL,'2019-12-09 00:00:00',N'No',NULL,NULL),
    (N'19-45','2019-11-14 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Aggiunta del fornitore "VWR" (F01813) alla specifica SPEC950-180_TRIS HCl - pH 9 - 1M. Essendo un prodotto critico è necessario che compaia un secondo fornitore per l''ordine del prodotto (attualmente risultano Sigma e Merk, che dopo la fusione in un''unica azienda non sono considerabili come due fornitori totalmente distinti).',N'VWR fornisce il codice VWRCE694-250M (produttore: Amresco - a VWR company) che presenta le stesse caratteristiche rispetto all''attuale codice utilizzato e fornito da Sigma (inserito in specifica, insieme a Merk), e presenta tempistiche di consegna più celeri.',N'Dopo la recente fusione di Sigma con Merk, il codice Merk inserito in specifica (9295-OP) è stato dismesso, quindi attualmente non risultano due reali fornitori per il codice 950-180; trattandosi però di un prodotto molto critico è indispensabile avere due distinti fornitori da cui poter acquistare il prodotto.',NULL,N'MM
PW
RDM',N'Acquisti
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo in quanto non solo la materia prima avrà due fornitori distinti da cui possibile approvvigionarsi, ma il nuovo fornitore "VWR" ha tempi più celeri di spedizione rispetto a Sigma.
Impatto positivo in quanto è presente un doppio fornitore di materia prima. Il TRIS HCl - pH 9 - 1M è un reagente a bassa criticità e, sulla base del confronto tra i certificati di analisi, è possibile dire che i prodotti di VWR e SIGMA sono equivalenti.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'15/11/2019
15/11/2019
15/11/2019
15/11/2019',N'nessun attività
Modifica della specifica SPEC950-180, aggiungendo il nuovo fornitore con codice VWRCE694-250M
messa in uso documenti
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'spec950-180 rev02 - cc19-45_messa_in_uso_spec953-xx_e_950-180_cc19-45_cc20-43_1.msg
spec950-180 rev02 - cc19-45_messa_in_uso_spec953-xx_e_950-180_cc19-45_cc20-43.msg
Il cambio fornitore di materia prima sarebbe una modifica significativa, ma nel caso del TRIS, reagente a bassa criticità, NON si ritiene significativa, pertanto non è necessario effettuare alcuna notifica. - cc19-45_mod05,36_non_significativa.pdf
primo lotto 950-180_22-11-19 - cc19-45_950-180_19-po-01640_18i0556014-1-2.pdf
esito cq primo lotto - cc19-45_primolottoprodottocontrishcl_esitocq_conforme.docx',N'RT
QAS
QAS
PW',N'15/11/2019
15/11/2019
15/11/2019
15/11/2019',NULL,N'26/11/2019
22/09/2020
22/09/2020
22/11/2019',N'No',N'nessun impatto sul DMRI in quanto il codice interno della materia prima non varia',N'No',N'non necessaria',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto',N'No',N'vedere MOD05,36. La modifica non è significativa in quanto trattasi di introduzione di un nuovo fornitore di un reagente a bassa criticità equivalente a quello attualmente in uso.',N'No',N'La materia prima è presente in prodotti R-BioPharma tuttavia, essendo un reagente a bassa criticità ed essendo equivalente a quello attualmente in uso, la modifica non è significativa, pertanto non è necessario inviare coipia del documento modificato al virtual manufacturer.',N'No',N'nessun impatto',NULL,NULL,NULL,'2020-09-22 00:00:00',N'No',NULL,NULL),
    (N'19-44','2019-11-13 00:00:00',N'Ferdinando Fiorini',N'Documentazione SGQ
Dismissione prodotto',N'Dismissione prodotti BANG14-02 e CTRG14',N'L''obiettivo della dismissione dei BANG14-02 e CTRG14  è il risparmio dei costi di produzione ad essi legati. 
L''apertura del change control avviene a seguito della valutazione del volume delle vendite in Italia e all''estero negli ultimi 24 mesi.',N'Mantenimento sul mercato di prodotti tecnologicamente superati e che non vengono più richeisti.',NULL,N'OM
QARA
QM
S&MM-MDx
SAD-OP
GASC
P&MMI
SM',N'Acquisti
Assistenza applicativa
Gare
Marketing
Ordini
Produzione
Regolatorio
Sistema qualità
Vendite Estero
Vendite Italia',N'La dismissione e la conseguente eliminazione dei prodotti a listino semplificherà la gestione documentale.
I prodotti BANG14-02 e CTRG14 dovranno comunque essere cancellati dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria. 
Gestione dei quantitativi dei materiali di produzione della linea BANG, riquantificati in base al prodotto dismesso.
Impatto in termini di ore di lavoro.
Valutare prodotti aternativi da fornire nelle gare/offerte in essere: Azienda Ospedaliera Cosenza ? Gara 0408/15 scadenza 27/04/202, Azienda Policlinico di Bari ? gara 1113/17 scadenza 08/03/2020 e Offerta ad Ardea per la partecipazione ad una gara per i presidi Binaghi e Businico. ? non abbiamo una scadenza ma in base a questa offerta comprano ancora molti prodotti.

Valutare prodotti aternativi da fornire nelle gare/offerte in essere: Azienda Ospedaliera Cosenza ? Gara 0408/15 scadenza 27/04/202, Azienda Policlinico di Bari ? gara 1113/17 scadenza 08/03/2020 e Offerta ad Ardea per la part',N'18/11/2019
15/11/2019
15/11/2019
18/11/2019',N'Compilazione ed aggiornamento nel corso del tempo del MOD04,12 per ciascuno dei prodotti in elenco secondo le fasi riportate (prodotto in dismissione, dismesso, cancellato). 
Allineamento con il personale che pianifica le attività di produzione relativamente all''acquisto delle materie prime per la linea BANG in base ai prodotti dismessi.
Individuazione dei clienti che utilizzano ancora i BANG.
valutare prodotti alternativi per le gare in essere se non verrà prodotto un ultimo lotto di produzione
verifica del materiale di MKT in cui il prodotto è citato
Blocco delle anagrafiche sul sistema gestionale, come e quando richiesto dal MOD04,12, secondo le fasi riportate. 
Allineamento in collaborazione con l''ufficio acquisti dei quantitativi di acquisto delle materie prime per la linea BANG in base ai prodotti dismessi.
Valutazione della produzione di un ultimo lotto a fronte delle gare e offerte ancora attive, in caso di assenza di prodotti alternativi da offrire: dal momento che i potenziali clien',N'Analisi vendite BANG14 2010-2019 - r_dismissione_bang14_e_ctrg14_cc19-44.msg
Vendite BANG14_2''00-2019 - vendite_bang14.xlsx',N'QARA
QM
QAS
PW
GASC
PMI
SAD-OP
PP
QARA
QM
QAS
S&MM-MDx
SM
SAD-OP
PMI
TCOA
MM',N'18/11/2019
25/11/2019
25/11/2019
18/11/2019',NULL,NULL,N'No',N'il prodotto verrà dismesso',N'Si',N'è necessario comunicare la dismissione ai clienti ancora attivi per l''aquisto di questo prodotto.',N'No',N'nessun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'nessun impatto',N'Si',N'veder MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'prodotto dismesso',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-43','2019-11-11 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede la modifica del MOD10,24 PIANO INDAGINE CQ, al fine di prevedere la firma sia di QC che di QCT e una sezione che riassuma le conclusioni delle indagini. Infatti ad oggi la dicitura "firma" è ambigua e non é presente un nota conclusiva da parte di QC. In base all''esperienza acquisita durante l''utilizzo è risultato utile inserire una sezione su test non pertinenti all''indagine ma correlati alla problematica del lotto (esempio Test funzionali alla preparazione del lotto ), inserire ulteriori categorie in particolare sull''"errore umano", valutare la possibilità di indicare meglio la dicitura P e CQ e la categoria di NC, inalcuni casi risulta infatti ambigua la conclusione in particolare per distinguere OOS di CQ da NC di requisiti o processo CQ.',N'Il documento risulterebbe più completo con le modifiche richieste e permetterebbe di registrare ed evidenziare più facilmente le conclusioni sulla causa di NC',N'Ad oggi risulta un pò ambiguo chi deve firmare il documento compilato e non vi sono le firme di chi esegue l''indagine e di QC, inoltre per alcune indagine risulta complicato capire come effettuare le registrazioni e attribuire la causa primaria',NULL,N'QC
QM',N'Controllo qualità
Sistema qualità',N'Nessun impatto particolare sul processo ma sulla registrazione  e firme sulle indagini effettutate in caso di NC
Nessun impatto sul processo.
Nessun impatto sul processo.',N'11/11/2019
15/11/2019
15/11/2019',N'Modifica del MOD10,24
messa in uso del MOD10,24
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'La modifica non è significativa, pertanto non è da notificare presso alcuna autorità competente. - cc19-43_mod05,36_non_significativa.pdf',N'QC
QCT/DS
QM
QAS
QAS',N'11/11/2019
15/11/2019
15/11/2019',N'20/12/2019',N'15/11/2019',N'No',N'nessun impatto',N'No',N'nessuna comunicazione',N'No',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'non necessaria',N'No',N'nessun impatto',NULL,'2019-12-20 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-42','2019-11-08 00:00:00',N'Federica Rinaldo',N'Documentazione SGQ',N'Adozione di nuovi script del dispensatore DA9 ed aggiornamento degli esistenti secondo le modalità di gestione delle modifiche descitte nella PR05,05.',N'Corretta gestione degli script e delle loro modifiche.',N'Mancata tracciabilità dei cambiamenti sugli script.',NULL,N'MM
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Deve essere condiviso con DS l''utilizzo dei nuovi script al fine di adottarli  correttamente nei moduli di produzione. 
Deve essere condiviso con DS l''utilizzo dei nuovi script al fine di adottarli  correttamente nei moduli di produzione. 
Impatto positivo in quanto l''uso dei nuovi script e l''aggiornamento degli esistenti permetterà di produrre con il dispensatore automatico. La gestione degli script attraverso delle revisioni migliora la tracciabilità.
Impatto positivo in quanto l''uso dei nuovi script e l''aggiornamento degli esistenti permetterà di produrre con il dispensatore automatico. La gestione degli script attraverso delle revisioni migliora la tracciabilità.
La gestione degli script attraverso delle revisioni migliora la tracciabilità.
La gestione degli script attraverso delle revisioni migliora la tracciabilità.',N'13/11/2019
13/11/2019
15/11/2019
15/11/2019
15/11/2019
15/11/2019',N'Aggiornamento MOD11,29
La gestione delle modifiche degli script e relative revisioni saranno gestite in accordo con la PR05,05 di gestione delle modifiche, per ogni script modificato. Il fornitore qualificato deve creare una nuova versione dello script modificato e NON sovrascrivere l''esistente (con numero di revisione successiva). Aggiornare dei nomi degli script sulla specifica d''uso e sul MOD11,29
Aggiornamento SPEC-USO_DA_00 con i nuovi script e con quelli modificati. Introduzione della "Daily maintenance" procedura messa a punto dal fornitore qualificato.
La gestione delle modifiche degli script e relative revisioni saranno gestite in accordo con la PR05,05 di gestione delle modifiche, per ogni script modificato. Il fornitore qualificato deve creare una nuova versione dello script modificato e NON sovrascrivere l''esistente (con numero di revisione successiva). Aggiornare dei nomi degli script sulla specifica d''uso e sul MOD11,29
Mantenere traccia delle revisioni degli script nell''elenco MOD05,03_',N'MOD11,29_01, MOD18,02 22/11/19 - cc19-42_messa_in_uso_mod1129_e_registrazione_script_in_elenco_cc19-42_mod1802_del_22112019.msg
SPEC-USO-DA9_01 - cc19-42_messa_in_uso_spc-uso-da9_01.docx
SCRIPT DA9 IN USO - cc19-42_messa_in_uso_script_in_elenco_cc19-42.msg
registrazione degli sript come allegati al la SPEC-USO-DA9 - cc19-42_messa_in_uso_mod1129_e_registrazione_script_in_elenco_cc19-42_mod1802_del_22112019_1.msg
La modifica non è significativa per tanto non è necessario eseguire alcuna notifica. - cc19-42_mod05,36_non_significativa.pdf',N'QC
QCT/DS
PT
MM
QC
QAS
QAS',N'13/11/2019
15/11/2019
15/11/2019
15/11/2019
18/11/2019',N'31/12/2019
11/11/2019',N'04/12/2019
31/03/2020
21/01/2020
04/12/2019
18/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW40 - Venus sw MICROLAB STRAlet Instrument (Hamilton - DA9)',N'Nessun impatto, le modifiche riguradano la gestione degli script',N'No',N'vedere MOD05,36',N'No',N'nessun impatto',N'No',N'nessun impatto',NULL,'2019-12-31 00:00:00',NULL,'2020-03-31 00:00:00',N'No',NULL,NULL),
    (N'19-41','2019-11-07 00:00:00',N'Paolo Saracco',N'Stabilità',N'Estendere la scadenza da 12 a 18 mesi del prodotto COLISTINA-R ELITe MGB Kit (RTS202ING-48) sulla base dei dati di stabilità reale, come comunicato da R&D in data 22/10/2019',N'Aggiungere 6 mesi alla durata del prodotto',N'Avere in commercio un prodotto con una durata inferiore rispetto agli studi di stabilità effettuati',N'962-RTS202ING-48 - COLISTIN-R ELITe MGB Kit',N'MM
QARA
QC
QM
RDM',N'Controllo qualità
Magazzino
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
L''estensione della stabilità permette una produzione più efficiente ed economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS202ING-48 (STB2019-105, lotto U1217AO -20°C per 18 mesi, STB2019-104, lotto U0118BL-20°C per 18 mesi, STB2019-108, lotto U1017BK -20°C per 18 mesi) non c''è nessun impatto sulle prestazioni del prodotto.',N'08/11/2019
08/11/2019
08/11/2019
08/11/2019
08/11/2019
08/11/2019
08/11/2019',N'Rietichettare il lotto U0319BS cambiando la scadenza in da 31/03/20 a 30/09/2020.
Messa in uso documento modificato.
Aggiornamento del modulo di produzione MOD962-RTS202ING-48, modificando la frase "attribuire la scadenza del Prodotto (massimo 12 mesi)" in "attribuire la scadenza del Prodotto (massimo 18 mesi)".
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornare FTP
Nessuna altra azione a carico di R&D.',N'Lotto rilavorato U1119AI in data 8/11/19 - cc19_41_rilavorazione_u0319bsinu1119ai.pdf
MOD962-RTS202ING-48_01 - cc19-41_messa_in_uso_mod962-rts202ing-48_01_cc19-41_mod1802del_15112019_1.msg
MOD962-RTS202ING-48_01 - cc19-41_messa_in_uso_mod962-rts202ing-48_01_cc19-41_mod1802del_15112019.msg
La modifica è significativa, effettuare e notifiche dove il prodotto è registrato - cc19-41_mod05,36_significativa.pdf
15-11-2019: il prodotto non è registrato in nessun paese esxtra eu - cc19-41_prodottononregistratoinnessunpaese_15-11-219.txt',N'PT
QC
QCT/DS
QAS
RDM
QARA
QAS
RAS
RAS',N'08/11/2019
08/11/2019
11/11/2019
08/11/2019
08/11/2019
08/11/2019',N'30/11/2019',N'11/11/2019
25/11/2019
25/11/2019
15/01/2019
25/11/2019
25/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'nessun impatto sul risk assessment',N'Si',N'vedere MOD05,36. Il prodotto non è registrato in nessun paese extra ce, pertanto non è stata effettuata alcuna notifica.',N'No',N'non necessaria',N'No',N'-',NULL,'2019-11-30 00:00:00',NULL,'2019-11-25 00:00:00',N'No',NULL,NULL),
    (N'19-40','2019-10-31 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento delle SDS:
-SDS70 rev03 (ref. RTSGxxPLDxxx, RTS100PLD, RTS076PLD, RTS130ING): eliminare indicazione per RTS130ING
-SDS71 rev03 (ref RTSGxxPLDxxx, RTS100PLD, RTS076PLD, RTS130ING): eliminare indicazione per RTS130ING
-SDS83 rev01 (ref. RTSXXXPLD, RTK015PLD, RTSTXXPLD, M800XXX, RTSXXXING): eliminare indicazione presenza Tris>1%',N'avere le SDS aggiornate rispetto ai prodotti in commercio',N'documentazione non aggiornata e non in linea rispetto ai prodotti sul mercato',NULL,N'QARA',N'Regolatorio',N'Impatto positivo in quanto la documentazione sarebbe aggiornata rispetto ai prodotti sul mercato
Impatto positivo in quanto la documentazione sarebbe aggiornata rispetto ai prodotti sul mercato',N'31/10/2019
31/10/2019',N'Aggiornare le SDS 70, 71 e 83
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'messa in uso SDS n.70 71 83 - cc19-40_messa_in_uso_sds_n.70_71_83_nuove_revisioni_e_89.msg
L''aggiornamento delle SDS risulterebbe significativa, tuttavia non essendo stata modificata la gestione del rischio, ma solo alcuni riferimenti (eliminazione del prodotto RT130ING e presenza TRIS>1%), si reputa non necessaria la notifica. - cc19-40_mod05,36_non_significativa.pdf',N'RAS
QAS',N'31/10/2019
31/10/2019',NULL,N'31/10/2019
31/10/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'non necessaria',N'No',N'nessun intervento',N'Michela Boi',NULL,NULL,'2019-10-31 00:00:00',N'No',NULL,NULL),
    (N'19-39','2019-10-28 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento delle SDS per l''InGenius create appositamente per il mercato americano. Al momento questi documenti contengono solo il riferimento al prodotto ELITe InGenius SP200 (ref. INT032SP200). E'' necessario aggiornarle inserendo il riferimento al kit di estrazione ELITe InGenius SP1000 (ref.INT033SP1000-US, prodotto RUO negli USA).',N'SDS aggiornate e che abbiano il riferimento ad entrambi i kit di estrazione in commercio da utilizzare con lo strumento ELITe InGenius ',N'non si potrebbero fornire ai clienti americani delle SDS aggiornate e che coprano anche il kit di estrazione ELITe InGenius SP1000',N'972-INT033SP1000 - ELITe InGenius SP 1000-48T',N'QARA',N'Regolatorio',N'Impatto positivo. Le SDS per il mercato americano sarebbero aggiornate rispetto ai kit di estrazione in commercio da utilizzare con lo strumento ELITe InGenius
Impatto positivo. Le SDS per il mercato americano sarebbero aggiornate rispetto ai kit di estrazione in commercio da utilizzare con lo strumento ELITe InGenius
Impatto positivo. Le SDS per il mercato americano sarebbero aggiornate rispetto ai kit di estrazione in commercio da utilizzare con lo strumento ELITe InGenius',N'29/10/2019
29/10/2019
29/10/2019',N'messa in uso dei documenti e invio a Elitech Inc.
Nessun impatto sull''aggiornamento del FTP.',N'messa in uso SDS ed invio a ELITeCh INC - cc19-39_messa_in_uso_sds.msg',N'RAS
RAS',N'29/10/2019
29/10/2019',N'05/11/2019
29/10/2019',N'31/10/2019
29/10/2019',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'nessun impatto',N'No',N'non necessaira perchè il prodotto è RUO negli USA, veder MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessuno',N'Michela Boi','2019-11-05 00:00:00',NULL,'2019-10-31 00:00:00',N'No',NULL,NULL),
    (N'19-38','2019-10-15 00:00:00',N'Alessandra Gallizio',N'Confezione',N'A seguito di richiesta da parte di R-Biopharm, e secondo la PLPrevisonata, per i prodotti  "963-PG9015-HSV1RIDA GENE HSV1 KIT", "963-PG9025-HSV2RIDA GENE HSV2", "963-PG9005-CMVRIDA GENE CMV KIT"  si richiede la sostituzione del componente del packaging denominato "plastic zipper bag" (codice VWR 129-9146, specifica 954-184_R) con il materiale "sacchetto minigrip neutro 200x300 mm" (specifica 954-130).',N'il sacchetto ha dimensioni maggiori e permette l''inserimento di IFU più voluminose del previsto',N'non sarebbe agevole il confezionamento dei kit RIDAGENE con i sacchetti inizialmente proposti.',N'963-PG9005-CMV - RIDA GENE CMV KIT
963-PG9015-HSV1 - RIDA GENE HSV1 KIT
963-PG9025-HSV2 - RIDA GENE HSV2',N'BC
MM
OM
PW
QC
QM',N'Acquisti
Budgeting control
Controllo qualità
Produzione
Program Managment
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini di attività
il costo del prodotto potrebbe subire modifiche
Impatto in termini di attività
Impatto in termini di attività
si rende necessario tenere traccia del cambiamento nella documentazione relativa ad accordi/contratti
il DMRI 2018-004 deve essere aggiornato con la nuova specifica
Impatto in termini di attività
Impatto in termini di attività
impatto in termini di attività',N'24/10/2019
17/10/2019
15/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019',N'aumento dei quantitativi da acquistare per il codice 954-130
verificare se il costo deiprodotti subisce modifiche
 revisione dei moduli di produzione
modifica del materiale di confezionamento e delle anagrafiche
gestione modifica e comunicazione a R-Biopharm della presa in carico
aggiornamento dei DMRI di prodotto sostituendo la specifica 954-184_R con la specifica 954-130 (DMRI 2018-004, DMRI2019-001, DMRI2019-004)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Il DMRI dei prossimi prodotti ridagene dovrà contenere la modifica',N'mod963-xxx in uso al 31/10/19, mod18,02del 29-10-19 - cc19-38_messa_in_uso_mod963-xxx_cc19-38_rac19-20_mod1802_del_291019.msg
Aggiornaamento anagrafiche NAV - cc19-38_aggiornateanagrafichenav.msg
La modifica non è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - cc19-38_mod05,36_non_significativa.pdf',N'QC
QCT/DS
BC
PT
PMS
RAS
QAS
PW
RT',N'24/10/2019
17/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019
24/10/2019',N'29/11/2019
29/11/2019
29/11/2019
29/11/2019
20/12/2019
20/12/2019
20/12/2019',N'31/10/2019
04/12/2019
24/10/2019',N'Si',N'aggiornamento dei DMRI di prodotto sostituendo la specifica 954-184_R con la specifica 954-130 (DMRI 2018-004, DMRI2019-001, DMRI2019-004)',N'No',N'non necessaria',N'No',N'nessun impattp',N'ALTRO - Altro',N'ALTRO - ALTRO',N'la modifica è stata richiesta da R-Biopharma, non ci sono impatti sul VMP',N'No',N'vedere MOD05,36, non significartiva, il materiale di confezionamento non è nè un contenitore primario, nè una scatola esterna',N'No',N'Nessun documento inviato a R-BIOPHARM contiene l''informazione modificata da questo CC per cui non occorre aggiornare/inviare a R-BIO nessun documento ',N'No',N'nessun impatto',N'Alessandra Gallizio','2019-12-20 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-37','2019-10-14 00:00:00',N'Katia Arena',N'Materia prima',N'Introduzione del  fornitore Life Technologies per l''acquisto dei plasmidi per la produzione del prodotto RTS200ING - CRE ELITe MGB. Attualmente i plasmidi sono acquistati da EGI,  si vuole passare al fornitore Life Technologies (plasmisdi prodotti da GeneArt) come per gli altri prodotti.',N'Uniformare la produzione, acquistando i plasmidi sempre dallo stesso fornitore.',N'Impossibilità di produzione del lotto nel caso l''attuale fornitore (EGI) non fosse in grado di fornirci i plasmidi con una scadenza consona.',N'962-RTS200ING - CRE ELITe MGB Kit',N'BC
MM
QARA
QC
RDM',N'Acquisti
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo',N'Impatto positivo in quanto tutti i plasmidi sono acquistati da un unico fornitore.
Il prodotto non subirà modifiche delle prestazioni dal momento che i costrutti plasmidici attualmente forniti da Bothell saranno sostituiti da costrutti plasmidici equivalenti forniti da GeneArt (solo ad una concentrazione diversa) e la preparazione dei relativi CTR seguirà le modalità già in uso per gli altri prodotti ELITe Positive Control.
Impatti sul processo: aumento dei tempi di manodopera e modifica delle anagrafiche di NAV. I nuovi plasmidi saranno equivalenti ai precedetni tranne per la concentrazione, è necessario comunicare  al CQ il primo lotto di CTR prodotto con i nuovi plasmidi.
Impatti sul processo: aumento dei tempi di manodopera e modifica delle anagrafiche di NAV. I nuovi plasmidi saranno equivalenti ai precedetni tranne per la concentrazione, è necessario comunicare  al CQ il primo lotto di CTR prodotto con i nuovi plasmidi.
Tale cambiamento ha impatto sulla modulistica di produzione in quanto ',N'05/12/2019
06/12/2019
22/10/2019
22/10/2019
17/10/2019
06/12/2019
06/12/2019
06/12/2019',N'Acquisto dei plasmidi dal nuovo fornitore, dopo la creazione della specifica da parte di RT
Revisione delle specifiche con introduzione del nuovo fornitore: 
952-pM301384	pKPC 1x107 c/ul
952-pM301402	pOXA 1x107 c/ul
952-pM301411	pNDM 1x107 c/ul
952-pM301429	pCTX-M-1-15 1x107 c/ul
952-pM301536	pCTX-M-9 1x107 c/ul
952-pM301420	pVIM 1x107 c/ul
952-pM301393	pIMP 1x107 c/ul
verifica della modulistica di produzione adeguata al nuovo formato di plasmide da acquistare
Aggiornamento delle anagrafiche
MOD09,23
MOD962-CTR200ING 
MOD962-CTR201ING
MOD10,13 RTS200ING-CTR e MOD10,13 RTS201ING-CTR e MOD10,18 
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
aggiornare FTP per  modifica DMRI
messa in uso documenti',N'SPECIFICHE PLASMIDI IN USO - cc19-37_messa_in_uso_spec952-xx_cre_cc19-37_rac20-1.msg
nUOVE SPECIFICHE PLASMIDI (spec952-XXX) - cc19-37_messa_in_uso_spec952-xx_cre_cc19-37_rac20-1_1.msg
Messa in uso MOD CQ e PROD, MOD18,02 del 9-12-19 - cc19-37_messa_in_uso_mod_cq_prod_cc19-37_rac19-20_mod1802_del_91219.msg
in uso MOD10,12_MOD10,13, MOD962_MOD955XX ESBL E CRE - cc19-37_messa_in_uso_mod_cq_prod_cc19-37_rac19-20_mod1802_del_91219.msg
La otifica è significativa, da notificare nei paesi in cui il prodotto è registrato - cc19-37_significativa.pdf
la notifica non è stata effettuata in quanto l''informazione riguardante il fornitore del plasmide non è mai stata inviata nei paesi dobve il prodotto è registrato (argentina, colombia, ecuador, guatemala, messico, panama, paraguay, serbia, tailandya, turchia, uruguay) - cc19-37_notifica_non_effettuata.txt
In uso MOD10,12_MOD10,13, MOD962_MOD955XX ESBL E CRE  - cc19-37_messa_in_uso_mod_cq_prod_cc19-37_rac19-20_mod1802_del_91219_1.msg
NUOVE SPECIFICHE PLASMIDI (spec952-XXX) - cc19-37_messa_in_uso_spec952-xx_cre_cc19-37_rac20-1_2.msg
ARCHIVIAZIONE SPEC952-XXX - cc19-37_archiviazione_spec952-xxx_di_cre_e_esbl.msg',N'PC
RT
MM
QCT/DS
QAS
RAS
PT
QAS',N'05/12/2019
05/12/2019
06/12/2019
06/12/2019
17/10/2019
06/12/2019
06/12/2019
06/12/2019',NULL,N'26/05/2020
26/05/2020
23/12/2019
23/12/2019
09/03/2019
19/02/2020
08/06/2020',N'Si',N'da aggiornare sostituendo il Calibrator (955-M300479) con il Diluente dei Plasmidi (955-diluente Plasmidi),',N'No',N'non necessaria',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun prodotto',N'No',N'la documentazione di gestione dei rischi non è da aggiornare. Il cambio di fornitore di plasmidi non modifica gli effetti del reagenti sulle persone e sull''ambiente né le performance di prodotto. Il processo produttivo risulta invece modificato: i pericoli legati all''introduzione di nuovi processi di fabbricazione sono però già considerati nella presente documentazione di gestione del rischio del prodotto CTR200ING e la modifica introdotta non determina variazioni nella valutazione del rischio.',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-36','2019-10-09 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Modifica dell'' intestazione del produttore e fornitore nella specifica SPEC-074_Scatola interna bianca (da "LITO-CARTOTECNICA di Bergantin Adriano s.a.s." a "BBF CARTOTECNICA SRL").',N'Il fornitore, pur mantenendo la stessa partita IVA, ha modificato la sua ragione sociale che deve quindi essere aggiornata in specifica per il corretto controllo al ricevimento.',N'Se la ragione sociale del fornitore non venisse aggiornata ("BBF CARTOTECNICA SRL") si avrebbero delle discrepanze tra la documentazione ricevuta dal fornitore e le informazioni contenute nella specifica durante la procedura del controllo al ricevimento.',NULL,N'QM
RDM',N'Acquisti
Regolatorio
Ricerca e sviluppo',N'nessun impatto se no in termini di attività.
nessun impatto se no in termini di attività.
nessun impatto se no in termini di attività.
nessun impatto se no in termini di attività.',N'24/10/2019
23/10/2019
24/10/2019
24/10/2019',N'Allinemento dei dati del fornitore tra quelli inseriti su NAVISION e quelli inseriti in specifica
Modifica della specifica SPEC954-074 nella parte delle "informazioni per l''ordine" (denominazione del produttore e fornitore)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
messa in uso specifica aggiornata',N'SPEC954-074_01, formazione del 2/12/19 - cc19-36_messa_in_uso_specifiche_cc17-32_cc19-36_cc10-17_mod1802_021219.msg
La modifica non è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - cc19-36_mod05,36_non_significativa.pdf
SPEC954-074_01 - cc19-36_messa_in_uso_specifiche_cc17-32_cc19-36_cc10-17_mod1802_021219_1.msg',N'RT
PC
QAS
QAS',N'24/10/2019
23/10/2019
24/10/2019
24/10/2019',N'20/12/2019
20/12/2019
20/12/2019
20/12/2019',N'09/12/2019
09/12/2019
24/10/2019
09/12/2019',N'No',N'nessun impatto perch nè l codice, nè il titolo della specifica variano',N'No',N'non necessariia',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'la modifica riguarda solo l''aggiornamento della ragione sociale del fonitore',N'No',N'vedere MOD05,36, la modifica non è significativa in quanto non varia il fornitore del materiale di confezionamento collegato all''IVD, ma solo la sua ragione sociale.',N'No',N'nessun mpatto',N'No',N'nessun impatto,  in quanto non varia il materiale di confezionamento ',NULL,'2019-12-20 00:00:00',NULL,'2019-12-09 00:00:00',N'No',NULL,NULL),
    (N'19-35','2019-10-02 00:00:00',N'Federica Rinaldo',N'Documentazione SGQ
Processo di produzione',N'Si richiede di modificare la procedura di preparazione del semilavorato 955-RANDOMPRIMER750 (BRK200) per la fase di determinazione della concentrazione fornita. Nella procedura in uso i criteri di accettazione risultano essere troppo stringenti e in alcuni è stato necessario ripetere le letture. Dall''analisi dello storico delle letture allo spettrofotometro (anno 2008- 2019, allegato A) emerge che il rapporto tra la concentrazione fornita e quella di utilizzo è costante. Pertanto è possibile fare affidamento sulla concentrazione dichiarata dal fornitore e procedere alla diluizione dei random sulla base del rapporto medio calcolato, escludendo la lettura allo spettrofotometro.',N'Si evitano manipolazioni del materiale, riducendo il rischio di errori. La modifica permette di ridurre i tempi di preparazione del semilavorato.',N'Se il cambiamento non viene attuato è comunque necessario valutare la modifica del range di accettazione dei valori delle assorbanze. Si sono verificati casi in cui i valori delle assorbanze superassero il limite superiore del range di accettazione e di conseguenza si è reso necessario ripetere le letture, allungando i tempi di preparazione. Inoltre sarebbe anche necessario aggiungere il valore del MEC sulla specifica della materia prima (950-065) e/o sul modulo di preparazione dei random primer.',N'955-RANDOMPRIMER750 - 955-RANDOMPRIMER750',N'BC
MM
QARA
QC
QM
RDM',N'Budgeting control
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Riduzione dei tempi di lavorazione. 
Nessun impatto nel processo se non in termini di valutazione della modifica e delle attività.
Si evitano manipolazioni del materiale, riducendo il rischio di errori. La modifica permette di ridurre i tempi di preparazione del semilavorato
Si evitano manipolazioni del materiale, riducendo il rischio di errori. La modifica permette di ridurre i tempi di preparazione del semilavorato
Nessun impatto nel processo se non in termini di valutazione della modifica e delle attività. La modifica non ha impatto sulle prestazioni e sicurezza del prodotto.
Nessun impatto nel processo se non in termini di attività
Nessun impatto nel processo se non in termini di attività
La modifica non ha impatto sulle prestazioni e sicurezza del prodotto.
La modifica non ha impatto sulle prestazioni e sicurezza del prodotto.
Impatto positivo per la riduzione dei tempi di preparazione del semialvorato',N'02/10/2019
17/10/2019
02/10/2019
02/10/2019
23/10/2019
17/10/2019
17/10/2019
17/10/2019
17/10/2019
03/10/2019',N'Modifica documento 955-RandomPrimer750 e formazione al personale di produzione
Analisi storico letture dal 2008 al 2019 ALLEGATO A
Aggiornamento dei tempi di preparazione sull''anagrafica di NAV a fine anno
verifica del modulo aggiornato prima della messa in uso
messa in uso nuovo modulo di produzione MOD955-RAndomPrimer750
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
messa in uso nuovo modulo di produzione MOD955-RAndomPrimer750
Aggiornamento dei tempi di preparazione sull''anagrafica di NAV a fine anno',N'ALLEGATO A storico letture rando primers - cc19-35_allegato_a_letture_random_primers_2008-2019.xlsx
La modifica non è significativa, non è da notificare- - cc19-35_mod05,36_non_significativa.pdf',N'QAS
PT
QAS
QC
QCT/DS
RDM
QAS
QAS
PT
BC',N'17/10/2019
03/10/2019
03/10/2019
23/10/2019
17/10/2019
17/10/2019
17/10/2019
17/10/2019
17/10/2019',N'24/10/2019
03/10/2019
20/12/2019
24/10/2019
24/10/2019
23/10/2019
23/10/2019
24/10/2019
20/12/2019',N'15/10/2019
03/10/2019
17/10/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,NULL,NULL,'2020-11-13 00:00:00',N'Si',N'il prodotto è in dismissione entro 12/2020','2020-11-13 00:00:00'),
    (N'19-34','2019-10-01 00:00:00',N'Eleonora  Mastrapasqua',N'Materia prima',N'Sostituzione della sonda rtS2R BH S3 con la nuova sonda rtS2R BH2 S3 nel prodotto RTS140ING - BORDETELLA ELITe MGB Kit: il cambiamento prevede il mantenimento della stessa sequenza ma sostituzione del quencher Q575 con EDQ.
Il codice della materia prima resta invariato.',N'Eliminazione di segnale di fondo della sonda rtS2R BH S3 per il target BH dovuto all''instabilità del quencher Q575.',N'Possibile non conformità in controllo qualità dei lotti di produzione dovuta a un segnale di fondo della sonda rtS2R BH S3 causato dallo scorretto funzionamento del quencher.
Possibile rilevazione in campioni clinici di segnali aspecifici per Bordetella holmesii (falsi positivi per BH).',N'951-RTS140ING-S3 - rtS2R BH2 S3',N'MM
PMS
PW
QC
RDM',N'Acquisti
Controllo qualità
Produzione
Program Managment
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii in campioni clinici dovuti a segnali aspecifici di BH.
Eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii in campioni clinici dovuti a segnali aspecifici di BH.
Eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii durante il controllo qualità dei lotti di produzione, dovuti a segnali aspecifici di BH.
Eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii durante il controllo qualità dei lotti di produzione, dovuti a segnali aspecifici di BH.
Nessun impatto se non in termine di attività.
Nessun impatto se non in termine di attività.
Nessun impatto se non in terimini di attività
Nessun impatto se non in terimini di attività
Nessun impatto
L''eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii in campioni clinici dovuti a segnali aspecifici di BH.
L''eliminazione del rischio di rilevazione di',N'02/10/2019
02/10/2019
17/10/2019
17/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019',N'Aggiornamento della specifica oligo 951-RTS140ING con il nuovo nome rtS2R BH2 S3 e il quencher EDQ e relativa formazione a personale di produzione e acquisti
Aggiornamento DHRI
aggiornamento del modulo MOD955-20X-BH-MIX  con l''inserimento del nuovo nome della sonda rtS2R BH2 S3 e relativa formazione a personale di produzione
CQ di un 4° lotto pilota
Aggiornamento della descrizione della materia prima sull''anagrafica di NAV 
Verifica/acquisto di materie prime per la realizzaione di un 4° lotto pilota
Utilizzo della nuova sonda rtS2R BH2 S3 nel terzo Lotto Pilota a sostituzione della precedente
Realizzazione di un 4° lotto pilota
Attività di program managment
Integrazione nell''analisi dei rischi dell''eliminazione del rischio di rilevazione di falsi positivi per Bordetella holmesii in campioni clinici dovuti a segnali aspecifici di BH.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Messa in',N'SPEC951-RTS140ING_01 - cc19-34_in_uso_la_spec951-rts140ing.msg
MOD955-20X-BH-MIX rev00 - cc19-34_messa_in_uso_moduli_bordettella.msg
962-RTS140ING,lotto U0120-027; 962-CTR140ING, lotto U00120-031	13/07/2020 - cc19-34_4_lotti_pilota_bordetella_1.msg
DHRI2019-013 firmato il 25/11/2019 - dhri2019-013_1.pdf
MOD955-20X-BH-MIX rev00 - cc19-34_messa_in_uso_moduli_bordettella_1.msg
La modifica non è significativia in quanto il prodotto non è ancora immmenso in commercio. - cc19-34_mod05,36_non_significativa.pdf
962-RTS140ING,lotto U0120-027; 962-CTR140ING, lotto U00120-031	13/07/2020 - cc19-34_4_lotti_pilota_bordetella_2.msg
962-RTS140ING,lotto U0120-027; 962-CTR140ING, lotto U00120-031	13/07/2020 - cc19-34_4_lotti_pilota_bordetella.msg',N'RT
QC
QCT/DS
PW
PP
RT
PMS
RAS
QAS
QAS
PP
PT
QCT/DS
PW',N'02/10/2019
02/10/2019
17/10/2019
17/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019
02/10/2019',N'23/10/2019
23/10/2019
23/10/2019
23/10/2019
23/10/2019
02/10/2019
23/10/2019
23/10/2019',N'11/10/2019
25/11/2019
11/12/2019
10/01/2020
10/01/2020
10/01/2020
10/01/2020
10/01/2020
02/10/2019
02/10/2019
11/12/2019',N'No',N'nessun impatto',N'No',N'non necessaria in quanto il rpodotto è in fase di sviluppo',N'No',N'prodotto non ancora immesso in commercio, in fase di sviluppo',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'nessun impatto',N'No',N'non necessria in quanto il prodotto non è ancora stato registrato, ma è in fase di sviluppo',N'No',N'nessun virtual manufacturer',N'No',N'prodotto in fase di sviluppo, analisi dei rischi ancora da completare',N'Eleonora  Mastrapasqua','2019-10-23 00:00:00',NULL,'2020-01-10 00:00:00',N'No',NULL,NULL),
    (N'19-33','2019-09-10 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'La procedura di controllo qualità deve essere aggioranta per quanto riguarda le mansioni e attività di Document specialist e di Customer Complaint Analyst. Si chiede inoltre di far riferimento all''attività di monitoraggio dei prodotti quando prevista.',N'La modifica permette di allineare la procedura con le attività svolte dal personale di controllo qualità.',N'Le attività sono dettagliate in altre procedure, ma non sarebbero menzionate nella procedura dell''ufficio di competenza del personale.',NULL,N'QARA
QC
QM',N'Controllo qualità
Sistema qualità',N'Nessun impatto particolare in quanto le attività sono già svolte dal personale di CQ
Nessun impatto particolare in quanto le attività sono già svolte dal personale di CQ.
Nessun impatto particolare in quanto le attività sono già svolte dal personale di CQ.',N'10/09/2019
10/09/2019
10/09/2019',N'Aggiornamento della Procedura PR10,02
Approvazione della PR e messa in uso
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'PR10,02_07_12/09/19 - cc19-33_messa_in_suo_pr1002_07_rap18-21_cc19-33_mod18,02_del_12-09-2019.msg
 PR10,02_07_12/09/19  - cc19-33_messa_in_suo_pr1002_07_rap18-21_cc19-33_mod18,02_del_12-09-2019_1.msg
La modifica non è significativa per tanto non è necessario effettuare alcun tipo di notifica - cc19-33_mod05,36_non_significativa.pdf',N'QC
QARA
QM
QAS
QAS',N'10/09/2019
10/09/2019
10/09/2019',N'11/09/2019
16/09/2019
16/09/2019',N'12/09/2019
12/09/2019
12/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'CQ - Controllo Qualità prodotti',N'CQ0 - ALTRO',N'nessun risk assessment impattato',N'No',N'vedere MOD05,36',N'No',N'vedere MOD05,36',N'No',N'nessun impatto',N'Federica Farinazzo','2019-09-16 00:00:00',NULL,'2019-09-12 00:00:00',N'No',NULL,NULL),
    (N'19-32','2019-08-09 00:00:00',N'Sergio  Fazari',N'Documentazione SGQ',N'Aggiornamento del processo di vigilanza con l''adozione di nuovi documenti emessi dalla Commissione EU (report e istruzioni aggiuntive) e dall''uso della firma digitale per la chiusura dei documenti ufficiali (report) da indirizzare alle Autorità Competenti . 
Di seguito sono riportati i documenti interessati:
- Manufacturer Incident Report (MIR) Version 7.1 che SOSTITUISCE l''attuale Modello di Notifica dell''Incidente (ALL. 3 PR 14,03 / version 2.26en 2012-12-04)
- "Additional Guidance Regarding the Vigilance System as outlined in MEDDEV 2.12-1 rev. 8" da INCLUDERE nella documentazione di origine esterna (norme e leggi)
- Template per la replica di avvenuta ricezione/comprensione dell''FSN da parte del cliente e del distributore (2 documenti distinti) da INTRODURRE nell''uso
',N'L''adozione del MIR risulta obbligatorio entro il 31/12/2019. Le restanti modifiche, sebbene volontarie, hanno le potenzialità di rendere più accurato il processo di vigilanza (in particolare quello di reporting) e più aderente alle normative internazionali',N'NON conformità nel caso della mancata adozione del nuovo MIR. Possibilità ',NULL,N'QARA
QM
QAS
RAS',N'Regolatorio
Sistema qualità',N'xxxxxxx
xxxxxxx
xxxxxxx
Stesura più impegnativa/complessa del Report d''Incidente. Replica più complessa da parte di distributori e clienti
Stesura più impegnativa/complessa del Report d''Incidente. Replica più complessa da parte di distributori e clienti
Stesura più impegnativa/complessa del Report d''Incidente. Replica più complessa da parte di distributori e clienti
Stesura più impegnativa/complessa del Report d''Incidente. Replica più complessa da parte di distributori e clienti',NULL,N'Messa in uso dei nuovi documenti
Formazione al personale QARA e al supporto applicativo per quanto concerne i nuovi moduli di replica delle FSN
Aggiornamento PR di Vigilanza
Confronto tra vecchio e nuovo Report d''Incidente con valutazione delle modifiche utilizzando il documento "helptext" prodotto dalla Commissione EU. Aggiungere nel documento helptext i commenti riferiti a EGSPA. 
Analisi del doc "Additional Guidance Regarding the Vigilance System as outlined in MEDDEV 2.12-1 rev. 8" e valutazione degli impatti sul sitema di vigilanza di EGSPA delle istruzioni riportate .
richiesta e ottenimento della firma digitale per QARA da utilizzarsi nell''autenticazione dei doc ufficiali da trasmettere alle Aut Comp',NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Sergio  Fazari',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-31','2019-08-01 00:00:00',N'Sergio  Fazari',N'Manuale di istruzioni per l''uso
Annullato / non approvato',N'Prevedere il caricamento su InGenius di un tubo primario per l''analisi delle urine con STI PLUS, riportando nel paragrafo dell''IFU "campioni e controlli" le caratteristiche tecniche di tale tubo: per esempio dimensioni, forma, sterilità, materiale e/o l''identificativo commerciale degli articoli eventualmente individuati. Attualmente la procedura indicata nel paragrafo "campioni e controlli" dell''IFU del kit prescrive solo il trasferimento di un''aliquota di campione nel tubo secondario (sonication tube) ',N'L''utilizzo del tubo primario può essere un requisito commerciale critico alla luce del fatto che si stanno diffondendo dei sistemi di raccolta per urine composti da barattolo classico e provetta vaccum, in cui raccogliere parte delle urine del barattolo, e utilizzabile direttamente su varie piattaforme automatiche.',N'Minore valore competitivo. Si è già verificato che un''azienda concorrente, Seegene, abbia vinto una fornitura grazie al requisito del "tubo primario"',N'962-RTS400ING - STI PLUS ELITe MGB Kit',N'QARA
RDM
BDM-MDx
GASC
PVC',N'Assistenza applicativa
Global Service
Marketing
Ordini
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Impatto positivo, l''utilizzo del tubo primario può essere utilizzabile direttamente su varie piattaforme automatiche.  è ipotizzabile qualche impatto sul volume delle vendite / richieste dell''attuale tubo di raccolta del campione rispetto a quello nuovo?
Impatto positivo, l''utilizzo del tubo primario può essere utilizzabile direttamente su varie piattaforme automatiche.  è ipotizzabile qualche impatto sul volume delle vendite / richieste dell''attuale tubo di raccolta del campione rispetto a quello nuovo?
v
Nessun impatto
Nessun impatto
Nessun impatto
Nessun impatto
Non sono ipotizzabili impatti sulle performance e sull''uso del prodotto in quanto le caratteristiche del tubo primario utilizzabile sono indicate sul IFU del prodotto in uso SCH mRTS400ING rev01, mentre le dimensioni del tubo sono riportate nel IFU SCH mINT032SP200 rev06. Tali caratteristiche devono essere invariate, in particolare non deve contenere conservanti ed avere un volume > di 2,2 mL.
Non sono ipotizzabili impatti sulle',N'24/10/2019
24/10/2019
24/10/2019
24/10/2019
23/10/2019
23/10/2019
24/10/2019
24/10/2019',N'Informare il mercato/rete vendita della misura migliorativa adottata
Individuazione di tubi primari idonei già disponibili sul mercato. Valutazione impatti sul flusso di lavoro. 
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
m
messa in uso IFU
Aggiornamento FTP 
nessun a ttività a carico
aggiornamento IFU
possibile esecuzione di test sperimentali di verifica
nessuna attività
Formazione dei clienti sull''utilizzo',NULL,N'RDM
PVC
RT
QAS
QM
PMI
RAS
RAS
FPS
PMI',N'24/10/2019
24/10/2019
24/10/2019
23/10/2019
23/10/2019
24/10/2019
24/10/2019',N'23/10/2019
24/10/2019',N'23/10/2019
24/10/2019',N'No',N'nessun impatto',N'No',N'comunicazione da svolgere  con avvertenza IFU',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'altro',N'No',N'compilare MOD05,36',N'No',N'compilare MOD05,36',N'No',N'l''introduzione del tubo primario per STI PLUS non richiede l''aggiornamento della documentazione di gestione del rischio. La modifica introdotta andrebbe ad agire su rischi già esistenti senza introdurne di nuovi, le mitigazioni già messe in atto sono adeguate per la riduzione del rischio.',NULL,NULL,NULL,'2020-07-13 00:00:00',N'Si',N'Dalla data di apertura, 1/08/2020, al 13/07/2020 il change control non è partito.  L''utilizzo del tubo primario utilizzabile direttamente su varie piattaforme automatiche avrebbe migliorato il prodotto dal punto di vista commerciale, non attuandolo si mantiene un minore valore competitivo.','2020-07-13 00:00:00'),
    (N'19-30','2019-07-10 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Dall''audit al Sistema Informatico, report n°9-19, è emersa la necessità di ri-progettare le modalità di assegnazione dei livelli di permesso di lettura e scrittura alle cartelle di G/DOCSGQ, in quanto l''organizzazione in gruppi gestita attraverso il MOD07,09 non solo è complessa, ma non è sempre stata seguita nel corso degli anni (sono stati assegnati permessi differenti per persona ai singoli file). Il MOD07,09 risulta non aggiornato, (vedere OSS6-15-18report n°9-19).',N'Rendere il sistema dell''assegnazione dei livelli di permesso lettura e scrittura meno complesso, più facile da gestire e da mantenere sotto controllo.',N'Impossibilità di mantenere sotto controllo i livelli di permesso in lettura e scrittura alla cartella G/DOCSGQ.',NULL,N'QARA
QM
ITM',N'Regolatorio
Sistema informatico
Sistema qualità',N'Migliorare la gestione dell''assegnazione dei livelli di permesso lettura e scrittura alle cartelle presenti all''indirizzo G/DOCSGQ, rendendo il sistema meno complesso, più facile da gestire e da mantenere sotto controllo.
Migliorare la gestione dell''assegnazione dei livelli di permesso lettura e scrittura alle cartelle presenti all''indirizzo G/DOCSGQ, rendendo il sistema meno complesso, più facile da gestire e da mantenere sotto controllo.
Migliorare la gestione dell''assegnazione dei livelli di permesso lettura e scrittura alle cartelle presenti all''indirizzo G/DOCSGQ, rendendo il sistema meno complesso, più facile da gestire e da mantenere sotto controllo. 
Migliorare la gestione dell''assegnazione dei livelli di permesso lettura e scrittura alle cartelle presenti all''indirizzo G/DOCSGQ, rendendo il sistema meno complesso, più facile da gestire e da mantenere sotto controllo. 
Migliorare la gestione dell''assegnazione dei livelli di permesso lettura e scrittura alle cartelle presenti all''indirizzo ',N'10/07/2019
10/07/2019
10/07/2019',N'ri-progettare le modalità di assegnazione dei livelli di permesso lettura e scrittura di DOCSGQ, secondo i requisiti forniti da ITM.
aggiornare il MOD07,09 "MATRICE DEI GRUPPI DI RETE AD" sulla base delle nuove modalità di assegnazione.
fornire al personale SGQ i requisiti per ri-progettare le modalità di assegnazione dei livelli di permesso lettura e scrittura di DOCSGQ.
formare ITT sulle modalità di assegnazione dei livelli di permesso lettura e scrittura di DOCSGQ.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'DRAFT ri-progettazione gruppi per i permessi di accesso alla cartella DOCSGQ - mod07,09_02_draft_matrice_dei_gruppi_di_protezione_di_rete.xlsx
Aggiornamento del MOD07,09, draft rev.02 - mod07,09_02_draft_matrice_dei_gruppi_di_protezione_di_rete_1.xlsx
Mail informativa sulle regole e modalità di assegnazione di gestione dei permessi. messa in uso del MOD07,09 rev02, formazione al personale sgq del 21/07/2020 - cc19-30_ri-progettazione_delle_modalita_di_definizione_ed_assegnazione_dei_permessi_di_lettura_e_scrittura_in_docsgq.msg
DRAFT ri-progettazione gruppi per i permessi di accesso alla cartella DOCSGQ - mod07,09_02_draft_matrice_dei_gruppi_di_protezione_di_rete_2.xlsx
pr05,01_04 - cc19-30_formazione_pr0501_04_gestione_documenti.msg
Mail informativa sulle regole e modalità di assegnazione di gestione dei permessi. messa in uso del MOD07,09 rev02, formazione al personale sgq del 21/07/2020 - cc19-30_ri-progettazione_delle_modalita_di_definizione_ed_assegnazione_dei_permessi_di_lettura_e_scrittura_in_docsgq_1.msg
La modifica non è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - cc19-30_mod05,36_non_significativa.pdf',N'QMA
QMA
ITM
QAS
QMA',NULL,N'29/11/2019
30/09/2019
20/12/2019',N'16/07/2020
21/07/2020
16/07/2020
21/07/2020
13/09/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC3 - Verifica documenti',N'verificare il risk assessment interessato ',N'No',N'non necessaria vedere MOD05,36',N'No',N'virtual manufacturer non interessato, gestione interna',N'No',N'nessun impatto, modfiche di sistema',N'Roberta  Paviolo','2019-12-20 00:00:00',NULL,'2020-07-21 00:00:00',N'No',NULL,NULL),
    (N'19-29','2019-07-10 00:00:00',N'Stefania Brun',N'Documentazione SGQ',N'Modifica modalità gestione della richiesta di autorizzazione alla spesa dal supporto del file Excel su Outlook all''integrazione della RDA sul gestionale Navision',N'Integrazione sul gestionale Navision della RDA. Questo garantisce una più efficace integrazione con le fasi successive del ciclo passivo e controllo da parte della contabilità in fase di verifica della fattura ',N'Perdita di efficacia del processo autorizzativo delle spese dei vari reparti rispetto al budget assegnato',NULL,N'BC
PW
QM
ITM',N'Acquisti
Budgeting control
Regolatorio
Sistema informatico
Sistema qualità',N'Positivo in quanto l''integrazione del RDA sul gestionale Navision permette di velocizzare l''attività di compilazione dell''ordine di acquisto e di visualizzare lo stato di approvazione di una RDA
Positivo in quanto l''integrazione del RDA sul gestionale Navision permette di velocizzare l''attività di compilazione dell''ordine di acquisto e di visualizzare lo stato di approvazione di una RDA
Positivo in quanto permette al controllo di gestione di esercitare un controllo efficace sulle richieste di spesa di ciascun reparto rispetto al budget assegnato 
L''implementazione del modulo di RDA all''interno del gestionale Navision non richiede nuove implementazioni dal punto di vista sw in quanto può essere convertito all''uso un modulo già presente all''interno del core che è quello delle Offerte
L''implementazione del modulo di RDA all''interno del gestionale Navision non richiede nuove implementazioni dal punto di vista sw in quanto può essere convertito all''uso un modulo già presente all''interno del core che',N'10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019',N'Aggiornare la PR06,02 in modo da allinearla alle modalità introdotte con il modulo di RDA integrato sul gestionale
Aggiornare il risk assessment A4 con le nuove modalità di richiesta di autorizzazione per acquisto beni e servizi
Integrazione all''interno della procedura e nuova istruzione delle RDA dell''attività di verifica e approvazione sulle richieste di autorizzazione alla spesa esercitata dal Budgeting Controller
Predisporre all''interno del gestionale Navision il modulo di RDA secondo i requisiti indicati dalla funzione acquisti e dal finance per quanto riguarda il controllo sulla compilazione dei campi obbligatori e sui livelli autorizzativi
Integrare il modulo Offerte presente su Navision con i requisiti richiesti dagli acquisti e già presenti sull''attuale MOD06,06 Richiesta autorizzazione spesa beni e servizi e quelli richiesti dal Finance e dal controllo di gestione
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per e',N'PR06,02_02, MOD18,02 del 12/07/19 - cc19-29_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719.msg
 PR06,02_02, MOD18,02 del 12/07/19  - cc19-29_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719_1.msg
Procedura ed allegati in uso, formazione sull''uso di NAV per le RDA del 12/07/19 - cc19-29_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719_2.msg
 Procedura ed allegati in uso, formazione sull''uso di NAV per le RDA del 12/07/19  - cc19-29_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719_3.msg
A4 firmato il 26/11/2019 - mod09,27_01_a4_stesura_e_approvazione_richiesta_di_autorizzazione_acquisto_beni_e_servizi_cc19-29_1.pdf
La modifica non è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - cc19-29_mod05,36_non_significativa.pdf
 Procedura ed allegati in uso - cc19-29_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719_4.msg
A4 firmato il 26/11/2019 - mod09,27_01_a4_stesura_e_approvazione_richiesta_di_autorizzazione_acquisto_beni_e_servizi_cc19-29.pdf',N'PW
BC
ITM
ITM
PW
QAS
QM
QAS
QAS',N'10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019
10/07/2019',N'30/09/2019
12/07/2019
12/07/2019
12/07/2019
30/09/2019
30/09/2019',N'12/09/2019
26/11/2019
12/09/2019
12/09/2019
12/09/2019
11/09/2019
12/09/2019
26/11/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A4 - Stesura e approvazione Richiesta di autorizzazione acquisto beni e servizi',N'Aggiornare il risk assessment A4 con le nuove modalità di richiesta di autorizzazione per acquisto beni e servizi',N'No',N'vedere MOD05,36',N'No',N'non necessario',N'No',N'nessun impatto sui prodotti, modifica di sistema',N'Stefania Brun','2019-09-30 00:00:00',NULL,'2019-11-26 00:00:00',N'No',NULL,NULL),
    (N'19-28','2019-07-10 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'In uscita dal report n°18-18, audit interno non annunciato ai laboratori, è emerso non chiaro dove, l''impresa di pulizia, elimini l''acqua utilizzata per la pulizia dei pavimenti dei laboratori (OSS7). Per migliorare le modalità di pulizia dei laboratori da parte del personale dell''impresa di pulizia si richiede l''utilizzo di panni mono-uso.',N'L''utilizzo di panni monouso elimina i reflui derivanti dalla pulizia dei pavimenti dei laboratori.',N'Possibile utilizzo dei lavandini dei laboratori (dedicati all''uso per attività di laboratorio) per l''eliminazione dei reflui derivanti dalla pulizia dei pavimenti dei laboratori.',NULL,N'MM
QC
RT',N'Acquisti
Controllo qualità
Magazzino
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
L''utilizzo di panni monouso elimina i reflui della pulizia dei pavimenti dei laboratori.
nessun impatto se non acquisto del materiale scelto.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.
L''u',N'26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019',N'etichettatura del materiale utilizzato in ciascun laboratorio
 valutazione impatti sul risk assessment PU1 "Controllopulizia periodica area di lavoro" in accordo con QC e MM
Scelta dei panni mono-uso da acquistare, in accordo con RDM
Aggiornamento MOD09,18 scheda pulizia pavimenti rev00 e dei percorsi da seguire. Formazione al personale
etichettatura del materiale utilizzato in ciascun laboratorio
 valutazione impatti sul risk assessment PU1 "Controllopulizia periodica area di lavoro" in accordo con RDM e MM
 valutazione impatti sul risk assessment PU1 "Controllopulizia periodica area di lavoro" in accordo con QC e MM
etichettatura del materiale utilizzato in ciascun laboratorio
 valutazione impatti sul risk assessment PU1 "Controllopulizia periodica area di lavoro" in accordo con QC e RDM
Acquisto del materiale scelto nelle quantità identificate da QC, RDM e MM.
messa in uso documentazione
aggiornamento VMP a seguito di valutazione impatti sul risk assessment PU1 "Controllopulizia peri',N'IO09,05_02, MOD09,18_01_Materiale utilizzato per la pulizia etichettato - cc19-28__messa_in_uso_io09,05_02,_mod09,18_01_cc19-28,_mod18,02_del_06_05_2020.pdf
offerta vwr - cc19-28_offerta_vwr.pdf
IO09,05_02, MOD09,18_01_formazione al personale interno del 06/05/2020, al personale dell''impresa di pulizie del 12/05/2020 (MOD18,02) - cc19-28__messa_in_uso_io09,05_02,_mod09,18_01_cc19-28,_mod18,02_del_06_05_2020_1.pdf
IO09,05_02, MOD09,18_01_Materiale utilizzato per la pulizia etichettato - cc19-28__messa_in_uso_io09,05_02,_mod09,18_01_cc19-28,_mod18,02_del_06_05_2020_2.pdf
IO09,05_02, MOD09,18_01_Materiale utilizzato per la pulizia etichettato - cc19-28__messa_in_uso_io09,05_02,_mod09,18_01_cc19-28,_mod18,02_del_06_05_2020_3.pdf
IO09,05_02, MOD09,18_01_Materiale utilizzato per la pulizia etichettato - cc19-28__messa_in_uso_io09,05_02,_mod09,18_01_cc19-28,_mod18,02_del_06_05_2020_4.pdf
PU1 aggiornato il 9/06/2020 - cc19-28-rac19-10_mod09,27_01_controllopulizia_periodica_area_di_lavoro_pu1_1.pdf
PU1 aggiornato il 9/06/2020 - cc19-28-rac19-10_mod09,27_01_controllopulizia_periodica_area_di_lavoro_pu1_2.pdf
PU1 aggiornato il 9/06/2020 - cc19-28-rac19-10_mod09,27_01_controllopulizia_periodica_area_di_lavoro_pu1_3.pdf
La modifica non è significativa, pertanto non è necessario effettuare alcun tipo di notifica. - cc19-28_mod05,36_non_significativa.pdf
PU1 aggiornato il 9/06/2020 - cc19-28-rac19-10_mod09,27_01_controllopulizia_periodica_area_di_lavoro_pu1.pdf',N'RT
QC
QC
QC
MM
PW
QMA
QMA
QC
RDM
MM
QMA
PW
PVC
RDM',N'26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019
26/07/2019',N'29/11/2019
29/11/2019
31/07/2019
29/11/2019
29/11/2019
29/11/2019
29/11/2019
29/11/2019
29/11/2019
30/09/2019
29/11/2019
29/11/2019
31/07/2019
26/07/2019
26/07/2019',N'12/05/2020
09/06/2020
31/07/2019
12/05/2020
12/05/2020
09/06/2020
09/06/2020
12/05/2020
09/06/2020
12/05/2020
12/05/2020
11/09/2019
26/07/2019
26/07/2019',N'No',N'Nessun impatto sul DMRI di prodotto',N'No',N'Non è necessaria alcuna comunicazione al cliente',N'No',N'Non è presente alcun impatto sul prodotto',N'PU - Pulizia Aree di lavoro e Strumenti',N'PU1 - Controllo/pulizia periodica area di lavoro',N'valutazione impatti sul risk assessment PU1 "Controllopulizia periodica area di lavoro"',N'No',N'vedere MOD05,36',N'No',N'Nessun virtual manufacturer',N'No',N'L''analisi dei rischi del prodotto non è intressata da tale modifica',NULL,'2019-11-29 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-27','2019-07-02 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Aggiornamento dell''istruzioni di installazione IO11,12:
- Enhanced Streamline process
- Updated II Mass Production photos
- Updated SW1.3.0.12 Screenshots
 ed allineamento della procedura alle TSB rilasciate negli ultimi 12 mesi.',N'Allineamento della documentazione con le attività svolte',N'Disallineamento della documentazione e possibilità di utilizzo della versione obsoleta del sw InGenius 1.2.',NULL,N'CSC
QM',N'Assistenza applicativa
Assistenza strumenti esterni
Regolatorio
Sistema qualità',N'documentazione relativa l''installazione che non hanno influenza sulla qualità del prodotto rilasciato al cliente.
documentazione relativa l''installazione che non hanno influenza sulla qualità del prodotto rilasciato al cliente.
Formazione del personale.
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività
Nessun impatto se non in termini di attività',N'17/07/2019
17/07/2019
17/07/2019
17/07/2019
17/07/2019
17/07/2019',N'aggiornamento istruzione operativa
Formazione del personale.
nessuna attività a carico
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
messa in uso documentazione',N'ISTRUZIONE OPERATIVA AGGIORNATA - cc19-27_messa_in_uso_io1112_03_cc19-27.msg
INVIO TSB012revC - cc19-27_tsb__elite_ingenius_-_tsb_012_-_rev_c_-_installation_and_qualification_procedure.msg
MOD18,02 DEL 26/07/2019 - cc19-27_tsb__elite_ingenius_-_tsb_012_-_rev_c_-_installation_and_qualification_procedure.msg
La modifica non è significativa, non è pertanto necessario effettuare alcuna modifica. - cc19-27_mod05,36_00_non_significativa.pdf
IO11,12_03 IN USO - cc19-27_messa_in_uso_io1112_03_cc19-27_1.msg',N'FSE
GSC
FSE
QAS
QAS',N'17/07/2019
17/07/2019
17/07/2019
17/07/2019
17/07/2019',NULL,N'25/07/2019
26/07/2019
25/07/2019
25/07/2019',N'No',N'I DMRI di prodotto non subiscono alcun impatto',N'No',N'La documentazione in questione non è rilasciata ai clienti.',N'No',N'Il prodotto immesso in commercio non subisce alcun impatto.',N'AST - Assistenza tecnica strumenti',N'AST5 - Manutenzione preventiva (MP)',N'-',N'No',N'-',N'No',N'-',N'No',N'-',NULL,NULL,NULL,'2019-07-26 00:00:00',N'No',NULL,NULL),
    (N'19-26','2019-06-27 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Integrazione in SGAT Evo versione 02.00.04 del modulo di PQ per lo strumento ELITe InGenius.',N'Miglioramento delle attività di service che richiedono l''esecuzione della PQ per lo strumento ELITe InGenius, in quanto è possibile registrare l''intervento di PQ direttamente in SGAT Evo invece che su un file excell successivamente allegato in SGAT Evo',N'Utilizzo dell''attuale modulo PQ (testato e validato) in formato excel.',NULL,N'QARA
QM',N'Assistenza strumenti esterni
Regolatorio
Sistema qualità',N'Miglioramento delle attività di service che richiedono l''esecuzione della PQ per lo strumento ELITe InGenius
Nessun impatto se non in termini di attvità.
Nessun impatto se non in termini di attvità.',N'27/06/2019
27/06/2019
27/06/2019',N'Integrazione da parte di Volos del modulo PQ per lo strumento ELITe InGenius all''interno del SW SGAT Evo e verifica del funzionamento. Valutazione dell''analisi del rischio in relazione alle nuove funioznalità introdotte. Attività di Validazione da parte di EGSPA. Verifica dell''aggiornamento del VMP.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Aggiornamento dell''elenco sw, raccolta della documentazinoe di risk assessment e della verifica e validazione. Aggiornamento del VMP.',N'A seguitio di validazione del SW32 (verifcia e validazione introduzione modulo di PQ) il risk asssessment è stato aggiornato e firmato in data 27/6/19 - cc19-26_mod09,27_01_sw32_sgat_2.01.00.pdf
piano validazione SW32 con PQ - cc19-26_mod05,23_00_piano_validazione_sw32_modulo_pq.pdf
 piano validazione SW32 con PQ  - cc19-26_mod05,24_00_rapporto_validazione_sw32_modulo_pq.pdf
VMP aggiornato, ancora in corso di firma in attesa di chiusura della validazione di altri sw - cc19-26_draft_vmp_aggiornato.xlsx
la modifica non è significativa, pertanto non è necessario effettuare alcuna notifica - cc19-26_mod05,36_non_significativa.pdf
RA sw32 aggiornato con l''introduzione del modulo di PQ e sua validazione. MOD05,08 aggiornato con la versione 2.01.00.  - cc19-26_mod09,27_01_sw32_sgat_2.01.00_1.pdf
piano validazione modulo pq su sgat evo verisione 2.01.00 - cc19-26_mod05,23_00_piano_validazione_sw32_modulo_pq_1.pdf
rapporto validazione modulo pq su sgat evo verisione 2.01.00 - cc19-26_mod05,24_00_rapporto_validazione_sw32_modulo_pq_1.pdf
DRAFT VMP aggiornato - cc19-26_draft_vmp_aggiornato_1.xlsx',N'FSE
QMA
QMA',N'27/06/2019
27/06/2019
27/06/2019',NULL,N'01/07/2019
08/08/2019
08/08/2019',N'No',N'nessun impatto  ul DMRI di prodotto',N'No',N'non necessaria alcuna comunicazione al cliente in quanto trattasi di sw interno egspa / consociate',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW32 - sgat (Software globale per la gestione delle richieste di assistenza tecnica sugli strumenti )',N'SW 32 SGAT Evo aggiornato e  firmato in data 27/06/2019',N'No',N'non necessaria, vedere MOD05,36',N'No',N'nessun Virtual Manufacturer',N'No',N'nessun impatto',N'Marcello Pedrazzini',NULL,NULL,'2019-08-08 00:00:00',N'No',NULL,NULL),
    (N'19-25','2019-06-21 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Si richiede la modifica del codice del produttore/fornitore presente sulle specifiche relative ai dNTPs (950-038, 950-039, 950-040, 950-041 e 950-188).',N'In accordo con il fonritore ThermoFisher, si è deciso di creare un codice OEM per i dNTPs acquistati in modo tale da garantirci una scadenza minima di 24 mesi. Per questo motivo il codice attualmente presente in specifica (codice commerciale) è diverso rispetto a quello inserito nell''offerta in corso (codice OEM). Le caratteristiche generali del prodotto non mutano (si veda dichiarazione fornitore).',N'Si rischierebbe di ricevere dNTPs con scadenza minore di 24 mesi. Ricevere un qualsiasi materiale (per lo più ubiquitario) che ha scadenza più lunga possibile permette di dover fare meno ordini di acquisto e ricevere quindi meno lotti possibili, con conseguente meno spreco di materiale.',NULL,N'PW
RT',N'Acquisti
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Acquisto materiali con il nuovo codice OEM
Nessun impatto se nonin termini di attività
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività. ',N'21/06/2019
21/06/2019
21/06/2019
21/06/2019',N'Ordinare i dNTPs con i nuovi codici di acquisto
Aggiornamento delle seguenti specifiche 950-038, 950-039, 950-040, 950-041 e 950-188), e formazione
messa in uso specifiche
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica',N'esempio materia prima codice 950-038 acquistata con i nuovi codici di acquisto e controllata al ricevimento ed accettazione - 950-038_20-po-01463_00988437-00987360.pdf
ELENCO SPECIFICHE AGGIORNATA, MOD18,02 del 24/09/19 - sgq_messa_in_uso_spec-xx_cc19-25_cc34-17_cc19-12_mod1802_del_24092019____.msg
ELENCO SPECIFICHE AGGIORNATE, in uso al 24/09/19 - sgq_messa_in_uso_spec-xx_cc19-25_cc34-17_cc19-12_mod1802_del_24092019_____1.msg
La modifica non è significativa, pertanto non è necessario effettuare alcuna notifica. - cc19-25_mod05,36_non_significativa.pdf',N'PC
RT
QM
QMA',N'21/06/2019
21/06/2019
21/06/2019
26/07/2019',NULL,N'13/11/2020
24/09/2019
24/09/2019
13/09/2019',N'No',N'nessun impatto sui DMRI di prodotto',N'No',N'non è necessaria nessuna comunicazione al cliente ',N'No',N'non è presente nessun impatto sul prodotto',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto sul risk assessement',N'No',N'non necessaria, vedere MOD05,36',N'No',N'Nessun Virtual manufacturer',N'No',N'non necessario alcun tipo di aggiornamento',N'Anna Capizzi',NULL,NULL,'2020-11-13 00:00:00',N'No',NULL,NULL),
    (N'19-24','2019-05-27 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento della documentazione inerente la sorveglianza post-market come richiesto dal nuovo regolamento IVDR.',N'Adeguamento al nuovo regolamento IVDR ed analisi dei dati più completa ed efficiente per tutti i prodotti commercializzati.',N'Ritardo nell''adeguamento al nuovo regolamento IVDR .',NULL,N'QARA
QM',N'Regolatorio
Sistema qualità',N'Impatto positivo per adeguamento al nuovo regolamento IVDR, processo meglio strutturato sia per la raccolta dei dati (database specifici per reparto) che per l''analisi (i dati sono analizzati nel complesso dal personale SGQ).
Impatto positivo per adeguamento al nuovo regolamento IVDR, processo meglio strutturato sia per la raccolta dei dati (database specifici per reparto) che per l''analisi (i dati sono analizzati nel complesso dal personale SGQ).
Impatto positivo per adeguamento al nuovo regolamento IVDR, processo meglio strutturato sia per la raccolta dei dati (database specifici per reparto) che per l''analisi (i dati sono analizzati nel complesso dal personale SGQ).
Impatto positivo per adeguamento al nuovo regolamento IVDR, processo meglio strutturato sia per la raccolta dei dati (database specifici per reparto) che per l''analisi (i dati sono analizzati nel complesso dal personale SGQ).
Impatto positivo per adeguamento al nuovo regolamento IVDR, processo meglio strutturato sia per la raccolta dei ',N'12/06/2019
12/06/2019
12/06/2019
12/06/2019
12/06/2019
12/06/2019
12/06/2019',N'Aggiornamento PR14,04 rev00 ed integrazione dei moduli correlati.
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Formazione a tutto il personale interessato nella raccolta dei dati.
Creazione di documentazione specifica secondo IVDR (plan e report per ogni prodotto da includere nel FTP)
Messa in uso nuova documentazione e documenti aggiornati.
Verifica dell''eventuale aggiornamento del Risk assemsnt ASA5 e del VMP',N'Moduli in uso il 14/11/19 - cc19-24_messa_in_uso_moduli_post-market_surveillance_cc19-24.msg
La modifica si ritiene NON significativa, pertanto non è necessario effettuare alcuna notifica. - cc19-24_mod05,36_non_significativa.pdf',N'QMA
QMA
QMA
QMA
QMA
QMA',N'12/06/2019
12/06/2019
12/06/2019
12/06/2019
12/06/2019
12/06/2019',N'20/12/2019',N'02/07/2019',N'No',N'nessun mpatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'ASA5 "Sorveglianza post-produzione: 1) Valutazione della conformità dei prodotti ai requisiti essenziali di sicurezza (numero e gestione dei reclami, non conformità e azioni correttive/preventive), 2) Valutazione dei dati commerciali (venduto, prodotti della concorrenza), 3) Valutazione  dei dati tecnico scientifici (congressi, opinion leader, abstract ecc.), 4) Valutazione  dei dati di assistenza applicativa al cliente (numero di demo, visite, istruzioni, messa in servizio di strumentazione IVD), 5) Valutazione dei dai di Valutazione Esterna della Qualità (VEQ), 6) Valutazione dati ottenuti dalle analisi in silico (VIS), 7) Consultazione di avvisi di sicurezza per prodotti analoghi (stesso analita e tipologia NAT) sul sito dell''autorità nazionale competente, 8) Registrazioni dell''esito dell''attività di sorveglianza post -produzione fatta in sede di Riesame della Direzione"',N'No',N'vedere MOD05,36',N'No',N'nessun impatto',N'No',N'non applicabile',N'Michela Boi','2019-12-20 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-23','2019-05-13 00:00:00',N'Katia Arena',N'Documentazione SGQ',N'A seguito dell''Audit Approvvigionamento e Controllo al Ricevimento sono emerse le seguenti osservazioni: 
- 5-19-OSS1  Aggiornare l''ALL-1_PR06,02 
- 5-19-OSS2 Aggiornare la PR06,02
- 5-19-OSS3: Aggiornare l''IO06,01 per allinearla con la nuova versione di NAVISION',N'Allineamento della Procedura di Approvvigionamento e della documentazione correlata con le attività che vengono svolte.',N'Documentazione non allineata con le attivitò svolte.',NULL,N'OM
QM
QMA',N'Acquisti
Gare
Regolatorio
Sistema informatico
Sistema qualità',N'Impatto positivo in quanto si allineano i documenti con le attività effettivamente svolte.
Impatto positivo in quanto si allineano i documenti con le attività effettivamente svolte.
Impatto positivo in quanto si allineano i documenti con le attività effettivamente svolte.
Impatto positivo: l''allineamento delle istruzioni operative con la nuova versione di NAVISION, le rende utilizzabili come protocolli dei test per la validazione dello stesso.
Impatto positivo: l''allineamento delle istruzioni operative con la nuova versione di NAVISION, le rende utilizzabili come protocolli dei test per la validazione dello stesso.
Impatto positivo: l''allineamento delle istruzioni operative con la nuova versione di NAVISION, le rende utilizzabili come protocolli dei test per la validazione dello stesso.
Impatto positivo in quanto si allineano i documenti con le attività effettivamente svolte.
Impatto positivo in quanto si allineano i documenti con le attività effettivamente svolte.',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019',N'(5-19-OSS1) Aggiornare l''ALL-1_PR06,02 rev00 apportando le seguenti modifiche:
- allineare i livelli di criticità e le categorie di fornitura con quelle riportate nell''ALL-1_PR06,01;
- revisionare per ciascuna categoria l''elenco dei documenti/ requisiti/ personale responsabile relativi alle specifiche di fornitura.
(5-19-OSS2) Aggiornare la PR06,02 rev01 apportando le seguenti modifiche:
- aggiornare i riferimenti normativi, le sigle ed alcuni riferimenti documentali (es ALL-1MDX050) che risultano obsoleti;
- inserire l''indicazione circa la necessità da parte del fornitore di mantenere un controcampione, laddove applicabile, del prodotto commercializzato;
- revisionare le funzioni deputate all''autorizzazione degli ordini di acquisto rispetto all''importo;
- implementare la sezione dedicata all''acquisto della strumentazione con la descrizione dell''iter seguito per l''acquisto degli strumenti ELITe InGenius.
(5-19-OSS3) Aggiornare l''IO06,01 per allinearla con la nuova versione di NAVISION, affinchè p',N' La modifica NON è significativa, pertanto NON è necessario effettiuare alcun tipo di notifica  - cc19-23_mod05,36_non_significativa.pdf',N'PC
ITM
ITM
PW
PW
QMA
QMA',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
27/05/2019
13/05/2019',N'30/08/2019
30/08/2019
30/10/2019
30/10/2019',N'13/05/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'A - Gestione degli Acquisti
A - Gestione degli Acquisti',N'A3 - Riesame dell''offerta  del fornitore
A5 - Stesura e invio Ordine di acquisto',N'A5 "Stesura e invio Ordine di acquisto"; A3 "Riesame dell''offerta  del fornitore"',N'No',N'-',N'No',N'-',N'No',N'-',N'Katia Arena','2019-10-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-22','2019-05-07 00:00:00',N'Samuela Margio',N'Manuale di istruzioni per l''uso',N'Integrare il manuale d''uso e gli Assay Protocol del prodotto MDR/MTB ELITe MGB Kit (RTS120ING) aggiungendo la possibilità di analizzare i campioni solo per la rilevazione di MTB, senza identificazione della resistenza (attulamente è possibile solo rilevarli entrambi). A tale scopo è necessario creare un Assay Protocol ad hoc che preveda l''utilizzo di una sola miscela di amplificazione (TB1 PCR Mix)  e non due.',N'L''utilizzo del prodotto per la sola identificazione di MTB complex consentirebbe: 1) la promozione/vendita dello stesso nei paesi in cui il piano nazionale dei rimborsi NON contempla l''analisi delle resistenze per MTB con test molecolari. 2) l''aumento del numero di campioni analizzabili per ogni sessione di ELITe InGenius che passerebbero da 6/run a 12/run.',N'Perdita dell''opportunità di vendita in paesi in cui il solo screening molecolare per MTB è rimborsato.',NULL,N'SPM
QARA
RDM',N'Assistenza applicativa
Gare
Marketing
Program mangment
Regolatorio
Ricerca e sviluppo
Validazioni',N'Nessun impatto se non in temini di attività.
Nessun impatto se non in temini di attività.
Nessun impatto se non in temini di attività.
Nessun impatto se non in temini di attività.
Impatto positivo, in quanto si amplia l''opportunità di vendita in paesi in cui il solo screening molecolare per MTB è rimborsato.
Coordinamento delle attività tra le diverse funzioni.
Nessun impatto se non in temini di attività.
Nessun impatto se non in temini di attività.
Nessun impatto se non in temini di attività.
Gli impatti riguardano la creazione dei nuovi AP e la modifica delle IFU.
La nuova applicazione del prodotto è già verificata e validata all''interno dell''uso previsto dell''attuale prodotto.
Non ci sono impatti sul prodotto, la creazione del nuovo assay utilizza già dati esistenti.',N'02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019',N'L''assistenza applicativa ITA dovrà ricevere formazione adeguata sui nuovi Assay Protocols e IFU e a sua volta trasmetterla agli applicativi Export (distributori/ICO) e ai clienti finali.
Creazione e invio della TAB.
Installare il nuovo assay presso i clienti.
Creazione TAB ed invio di quella approvata
Promozione attiva del cambiamento nei confronti delle funzioni/aree deputate alla vendita (ITA e Export)
Coordinamento delle attività tra le diverse funzioni.
Gestione messa in uso IFU e Assay  protocol.
Aggiornamento FTP
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Creazione di Assay Protocols dedicati.
Aggiornamento delle IFU.
Comunicazione al personale di MKT e Assistenza Applicativa. 
Verifica di Assay Protocols dedicati, IFU e della TAB ed aggiornamento MOD05,35',N'ifu e assay protocol in uso il 5/8/19 - cc19-22_ifu_rts120ing_scp2018-007_2.msg
FTP aggiornato - cc19-22_section_1_product_technical_file_index_01.docx
La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti. - cc19_22_mod05,36_non_significativa.pdf
ifu e assay in uso il 5/8/19 - cc19-22_ifu_rts120ing_scp2018-007_3.msg
ifu e assay in uso il 5/8/19 - cc19-22_ifu_rts120ing_scp2018-007_4.msg
ifu in uso il 5/8/19 - cc19-22_ifu_rts120ing_scp2018-007_1.msg
ifu in uso il 5/08/19 - cc19-22_ifu_rts120ing_scp2018-007.msg
MARZO 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva - cc19-22_nonnecessariomonitoraggioinstallazioneappresspclienti.txt',N'GATL
QMA
QMA
QMA
RT
PVS
PMS
SPM
FPS
PVS',N'02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019
02/07/2019',NULL,N'05/03/2021
05/03/2021
08/08/2019
08/08/2019
08/08/2019
08/08/2019
26/06/2019
08/08/2019
08/08/2019',N'No',N'nessun impatto in quanto assay utilizzato dal cliente',N'No',N'comunicazione attraverso l''avvertenza posta con l''IFU',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'ALTRO',N'No',N'Non necessaria, vedere MOD05,36',N'No',N'nesun virtual manufaturer',N'No',N'MOD22,01 Rev.01 del01/08/2019 (RTS120ING) e MOD22,01 Rev.01 del 01/08/2019 (CTR120ING)',N'Samuela Margio',NULL,NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'19-21','2019-04-17 00:00:00',N'Cinzia Bittoto',N'Manuale di istruzioni per l''uso',N'Si richiede di estendere la durata della calibrazione (da 30 a 60 giorni) del prodotto CMV ELITe MGB Kit, codice RTK015PLD attraverso l''uso del prodotto STD015PLD,  CMV ELITe Standard, in associazione a ELITe InGenius.',N'Estendere la durata della calibrazione sullo strumento ELITe InGenius con conseguente riduzione del numero di sedute da parte del cliente. 
Specificare il numero massimo di sessioni che si possono fare con un tubo di STD in associazione al sistema ELITe InGenius che è diverso rispetto agli altri sistemi validati',N'Necessità di calibrazioni più frequenti sullo strumento ELITe InGenius da parte del cliente
Errato utilizzo dello STD da parte del cliente',NULL,N'PVS
QMA
RDM',N'Assistenza applicativa
Regolatorio
Ricerca e sviluppo
Validazioni',N'L''estensione della durata della calibrazione di STD015PLD in associazione ad InGenius non modifica le prestazioni del prodotto. Sulla base dei test pianificati (PRT2019-008) si mira a dimostrare che la calibrazione rimane stabile durante il periodo di estensione. La modifica avrà impatto positivo sul cliente che potrà eseguire una calibrazione ogni 60 giorni, invece che ogni 30.0.
L''estensione della durata della calibrazione di STD015PLD in associazione ad InGenius non modifica le prestazioni del prodotto. Sulla base dei test pianificati (PRT2019-008) si mira a dimostrare che la calibrazione rimane stabile durante il periodo di estensione. La modifica avrà impatto positivo sul cliente che potrà eseguire una calibrazione ogni 60 giorni, invece che ogni 30.0.
L''estensione della durata della calibrazione di STD015PLD in associazione ad InGenius non modifica le prestazioni del prodotto. Sulla base dei test pianificati (PRT2019-008) si mira a dimostrare che la calibrazione rimane stabile durante il periodo',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019',N'Completare i dati riguardo la Calibration/CTR Validity e integrare i test di stabilità on board di STD e CTR per verificare il loro utilizzo con lo strumento ELITe InGenius. Eseguire test come da PRT2019-008.
Stesura report con i risultati dei test che dimostrano che la calibrazione rimane stabile durante il periodo di estensione (da 30 a 60 giorni).
Aggiornamento delle IFU, degli assay protocol e relative TAB con i dati ottenuti.
Aggiornamento formazione FPS.
Revisione dei documenti e delle IFU aggiornate.
messa in uso IFU aggiornata
aggiornamento FTP
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica
Pianificazione dell''aggiornamento degli assay protocol presso i clienti, con tempistiche non restrittive in quanto il nuovo assay non influisce sulle performance del prodotto, ma solo sulle tempistiche di calibrazione.',N'PRT2019-008 FIRMATO IL 01/04/2019 - cc19-21_codici_documenti_firmati.txt
REP2019-064 FIRMATO IL 30/07/2019 - cc19-21_codici_documenti_firmati_1.txt
IFU E ASSAY AGIORNATI - cc19-21_ifu_rtk015pld_and_std015pld_cc19-21_-_rap19-13.msg
MAIL DI MESSA IN USO IFU - cc19-21_ifu_rtk015pld_and_std015pld_cc19-21_-_rap19-13.msg
IFU E ASSAY REVISIONATI - cc19-21_ifu_rtk015pld_and_std015pld_cc19-21_-_rap19-13_1.msg
IFU E ASSAY IN USO IL 2/08/19 - cc19-21_ifu_rtk015pld_and_std015pld_cc19-21_-_rap19-13_2.msg
FTP AGGIORNATO - sezione_1_indice_generale_ftp_12.docx
TAB P16_REV AC - cc19-21_elite_ingenius_-tab_p16_-_rev_ac_-__product_cmv_elite_mgb_notice.pdf
MARZO 2021: attualmente il sistema di monitoraggio delle revisioni degli ap installati sugli strumenti avviene attraverso la dashboard, ma manca un processo di pianificazione e controllo dell''installazione degli aggiornamenti degli AP presso i clienti. Tale processo verrà implementato con l''aggiornamento della dashboard, come previsto dalla RAC20-16. Nonostante non sia chiara la situazione di aggiornamento degli AP presso i clienti utilizzatori, non sono emersi reclami con uguale causa primaria, si ritiene pertanto possibile chiudere l''azione correttiva - cc19-21_nonnecessariomonitoraggioinstallazioneappresspclienti.txt
La modifica è significativa, pertanto da notificare alle autorità competenti presso cui il prodotto è registrato ed all''organismo notificato. La notifica al fornitore R-BIOPHARM si ritiene non necessaria, in quanto relativa all''uso specifico in associazione con lo strumento ELITe InGenius. - cc19-21_mod05,36_significativa.pdf
Notifica ai distributori - cc19-21_egspa_sgq_notification_of_change_-std015pld;_rtk015pld.msg
NoC DEKRA - cc19-21_noc_cmv_cc19-21.pdf',N'PVS
PVS
PVS
PVS
RDM
QMA
QMA
GATL
QMA',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019',NULL,N'01/04/2019
30/07/2019
08/08/2019
02/08/2019
08/08/2019
08/08/2019
28/02/2020
05/09/2019
05/03/2021',N'No',N'nessun impatto sul DMRI',N'No',N'notifica effettuata con l''avvertenza presente con l''IFU, da inviare TAB',N'No',N'nessun impatto sul prodotto immesso in commercio',N'ALTRO - Altro',N'ALTRO - ALTRO',N'altro',N'No',N'da notificare, vedereMOD05,36',N'No',N'La notifica a R_BIOPHARM non è necessaria in quanto la modifica è relativa all''associazione con uno strumento (vedere MOD05,36)',N'No',N'verificare',N'Cinzia Bittoto',NULL,NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'19-20','2019-04-17 00:00:00',N'Fabio Panariti',N'Uso Previsto',N'Si richiede di introdurre un nuovo modello di puntali (art 20 Thermofischer) per l''utilizzo con le nuove micropipette codice MP150 - MP199, intervallo di utilizzo 2-20 uL. Mentre per gli strumenti già in uso, MP86 - MP109 - MP126 - MP127, intervallo di utilizzo 2-20 μL, rimane invriato l''utilizzo dei puntali art 100.',N'Le nuove micropipette 2-20 uL "agganciano" solo puntali art 20.',N'Impossibilità di utilizzo dei nuovi modelli di micropipette da 2-20 uL.',NULL,N'QM',N'Assistenza strumenti interni
Produzione
Regolatorio
Ricerca e sviluppo',N'Impatto positivo in quanto è possibile utilizzare puntali idonei ai volumi aspirati con le micropipette in questione.
Le nuove micorpipette, codice MP150 - MP199 / intervallo di utilizzo 2-20 μL, non "agganciano" i puntali in uso presso EGSpA codice art100, comunemente usati dalle micropipette attulamente in uso (codici MP86 - MP109 - MP126 - MP127), ma solo i puntali art20.
Le nuove micorpipette, codice MP150 - MP199 / intervallo di utilizzo 2-20 μL, non "agganciano" i puntali in uso presso EGSpA codice art100, comunemente usati dalle micropipette attulamente in uso (codici MP86 - MP109 - MP126 - MP127), ma solo i puntali art20.
Impatto positivo in quanto è possibile utilizzare puntali idonei ai volumi aspirati con le micropipette in questione.
Impatto positivo in quanto è possibile utilizzare puntali idonei ai volumi aspirati con le micropipette in questione.
Nessun impatto se non in termini di attività.
Nessun impatto se non in termini di attività.',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019',N'Formare i tecnici diproduzione sull''uso dei puntali art100 con le micropipette identificate da un''etichetta.
Verificare l''idoneità dei puntali art20 thermofisher all''utilizzo con le MP150 e MP199. Eseguire una taratura con un maggior numero di pesate: se tutti i dati risultano conformi è possibile introdurre l''uso di questi puntali nei processi di produzione. Allegare i test eseguiti al MOD11,02B specifico per lo strumento.
Apporre un''etichetta sulle micropipette codice MP86 - MP109 - MP126 - MP127, con l''indicazione di utilizzo esclusivo con i puntali art100 ed aggiornare la scheda apparecchiatura specificando tra le restrizoini di impiego l''utilizzo dei puntali.
Si ritiene non necessario apporre un''etichetta sulle micropipette nuove che utilizzano i puntali art20 in quanto i puntali art100 non agganciano.
Formare i tecnici di laboratorio sull''uso dei puntali art100 con le micropipette identificate da un''etichetta.
Creazione della SPECIFICA per l''acquisto del codice ART20
 Verificare la signific',N'Test verifica - cc19-20_test_di_verifica_mp.pdf
 personale informato  - cc19-20_formazione_tecnici-etichetta_sulle_mp.txt
MOD11,02A aggiornato - cc19-20_aggionamentomod11,02anelle_restrizionid_impiego.txt
etichetta apposta sugli strumenti - cc19-20_informazione_tecnici-etichetta_sulle_mp.txt
informazione tecnici - cc19-20_informazione_tecnici-etichetta_sulle_mp_1.txt
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-20_mod05,36_non_significativa.pdf
SPEC953-245_00 - cc19-20_messa_in_uso_spec953-245art_20_pipet_tips_cc19-20_informazione_del_17042019.msg',N'TSS
MM
T&GSS
RT
QMA
RT
RAS',N'13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019
13/05/2019',N'30/06/2020',N'17/04/2019
17/04/2019
17/04/2019
17/04/2019
31/01/2020
17/05/2019',N'Si',N'aggiornare i DMRI dei prodotti che utilizzano questi puntali, allegare elenco',N'No',N'non necessaria',N'No',N'nessun impatto',N'ST - Elenco famiglie Strumenti',N'ST11 - Micorpipette (MP)',N'ST11',N'No',N'non necessaria',N'No',N'non necessaria',N'No',N'nessun impatto',N'Fabio Panariti','2020-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-19','2019-04-08 00:00:00',N'Ferdinando Fiorini',N'Tecnologia di produzione',N'Sostituzione del DA6 Tecan EVO 75 (per ii quale il produttore dal 31/12/2019 non avrebbe più eseguito la manutenzione/taratura" e che è già stato dismesso e rottamato), con il dispensatore automatico Starlet.
Il dispensatore automatico Starlet ha le seguenti caratteristiche: ha 4 puntali al posto di 1; ha un funzionamento ad aria e non a liquido; lavora sotto cappa; ha celle refrigerate per i reagentio da dispensare; ha l''aggancio dei puntali meccanico che permette di non cambiare i puntali a ogni ciclo di dispensazione. Inoltre è possibile accedere allo strumento durante la lavorazione, estraendo i rack già dispensati e inserendo rack con tubi vuoti pronti per le successive dispensazioni.',N'E'' stato scelto un dispensatori automatici STARlet della Hamilton, che ha caratteristiche equivalenti, ma superiori rispetto ai Tecan EVO 75 che sostituisce.',N'Non si potrebbe continuare a utilizzare la dispensazione automatica',NULL,N'MM',N'Assistenza strumenti interni
Controllo qualità
Produzione
Regolatorio
Sistema qualità',N'Impatto in termini di ore lavoro, per la codifica e la messa in uso dello strumento a seguito di validazione da parte del personale tecnico EGSpA.
Impatto in termini di ore lavoro, per la codifica e la messa in uso dello strumento a seguito di validazione da parte del personale tecnico EGSpA.
Impatto in termini di ore lavoro, per la codifica e la messa in uso dello strumento a seguito di validazione da parte del personale tecnico EGSpA.
L''introduzione di un nuovo dispensatore nel processo produttivo ha un impatto positivo grazie alle caratteristiche intrinseche ed innovative  del dispensatore Starlet rispetto all''obsoleto TECAN EVO 75, rottamato.
L''introduzione di un nuovo dispensatore nel processo produttivo ha un impatto positivo grazie alle caratteristiche intrinseche ed innovative  del dispensatore Starlet rispetto all''obsoleto TECAN EVO 75, rottamato.
L''introduzione di un nuovo dispensatore nel processo produttivo ha un impatto positivo grazie alle caratteristiche intrinseche ed innovative  del',N'08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
07/05/2019
07/05/2019
07/05/2019
08/05/2019
08/05/2019
08/05/2019',N'Stesura della specifica di assistenza tecnica. Verifica ed eventuale aggiornamento della IO11,07 di manutenzione e IO09,05 pulizia.
Codifica degli strumenti e creazione della scheda apparecchiatura (MOD11,02A e B).
Aggiornamento della specifica di assistenza tecnica SPEC-CL_00_SpecificheAssistenzaTecnicaCappeAflussoLaminare e SPEC-DA_02_SpecificheAssistenzaTecnicaDispensatoriAutomatici
Esecuzione PQ
verifica del risk assessment dei dispensatori auotmatici ed eventuale aggiornamento del VMP
creazione della specifica d''uso.
Aggiornamento del registro (MOD11,10A rev00)
Verifica degli script in uso
Esecuzione PQ funzionale
aggiornamento della documentazione
Nuova IO11,19 Starlet_Performance Qualification
Nuovo MOD11,28 VerificaDAStarletDraft
Nuovo MOD11,29_VerificaScriptDA
Revisione MOD11,23-TEST-Funzionale_PQ
IO11,05 Taratura Interna Dispensatori automatici

formazione al personale di produzione e QC
 Verificare la significatività della modifica e, se necessario, identificare i paesi ',N'IO11,07 e IO09,05 non da aggiornare - cc19-19_istruzioni_non_da_aggiornare.txt
MOD11,02A e MOD11,02B, strumento in uso al 23/07/2019 - cc19-19_doc_cartacei_da9.txt
 SPEC-DA_03 IN USO AL 16/9/19  - cc19-19_messa_in_uso_spec-da_rev03_cc19-19_1.msg
MOD11,28 19/6/19_MOD11,29_17/07/19 - cc19-19_doc_cartacei_da9_1.txt
MOD11,23 del 22/07/19, IO11,19 del 23/07/19, esecuita sul lotto U0719BE - cc19-19_doc_cartacei_da9_2.txt
AGGIORNAMENTO DRAFT VPM SW e ST - cc19-19_draft_vmp_st_e_sw_del_11-09-19.docx
creato RA SW40, sw Venus asociato al DA9 - mod0927_01_sw40_starlet_cc19-19.pdf
RA ST16, dispensatore automatico rimane invariato - mod09,27_01_modulo_di_risk_assessment_st16_dispensatore_automatico.pdf
IN USO DOCUMENTAZIONE STARLET, KOD18,02 DEL 8/05/2019 - cc19-19_messa_in_uso_documenti_starlet.msg
in uso IO11,19 rev01 aggiornata per diciture rilasciate dallo strumento. Non necessaria formazione aggiuntiva - cc19-19_messa_in_uso_io1119_rev_01_aggiornamento_diciture_rlasciate_dallo_strumento.msg
MOD18,02 DEL 8/05/2019 - cc19-19_messa_in_uso_documenti_starlet_1.msg
La modifica non è significativa, pertanto non si ritiene necessario effettuare alcun tipo di notifica. - cc19-19_mod05,36_non_significativa.pdf
SPECCL_01 - cc19-19_messa_in_uso_specifiche_assistenza_tecnica_cc19-6_cc19-19_1.msg
DOCUMENTI STARLET - cc19-19_messa_in_uso_documenti_starlet_2.msg
IO11,05_02 IN USO  - cc19-19_messa_in_uso_io1105_02_cc19-19.msg
MOD11,10B_00 IN USO - cc19-19_mod1110b_rev00_starlet_cc19-19_1.msg
SPEC-DA_03 IN USO AL 16/9/19 - cc19-19_messa_in_uso_spec-da_rev03_cc19-19.msg
DRAFT VMP SW e ST saranno messi in uso a seguito delle altre modifiche segnalate - cc19-19_draft_vmp_st_e_sw_del_11-09-19_1.docx
 SPEC-CL_01  - cc19-19_messa_in_uso_specifiche_assistenza_tecnica_cc19-6_cc19-19.msg
SPEC-DA rev03 - cc19-19_messa_in_uso_spec-da_rev03_cc19-19.msg
SPEC-USO-DA9 del 10/06/19, distribuita da QAS a produzione 9/8/19 - cc19-19_distribuzione_spec-uso-da9_rev00_cc19-19.msg
MOD11,10B_00 - cc19-19_mod1110b_rev00_starlet_cc19-19.msg
Messa in uso primo gruppo di script - cc19-19_messa_in_uso_script.txt
messa in uso secondo, ed ultimo, gruppo di script - r_sgq_messa_in_uso_mod1129_e_registrazione_script_in_elenco_cc19-42_mod1802_del_22112019.msg',N'TSS
MM
DS
QC
QCT
MM
QC
QC
QMA
QMA
QMA
TSS
MM
MM
MM',N'08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
08/05/2019
07/05/2019
07/05/2019
07/05/2019
08/05/2019
08/05/2019
08/05/2019',N'27/12/2019',N'16/09/2019
16/09/2019
17/07/2019
19/06/2019
12/09/2019
12/09/2019
29/01/2020
23/07/2019
29/01/2020
07/06/2019
17/05/2019
20/09/2019
12/09/2019',N'No',N'nessun impatto, documentazion relativa allo strumento',N'No',N'non necessaria in quanto strumento interno',N'No',N'nessun impatto',N'ST - Elenco famiglie Strumenti',N'ST16 - Disepnsatori Automatici (DA)',N'ST16',N'No',N'vedere MOD05,36',N'No',N'vedere MOD05,36',N'No',N'nessun intervento in quanto documentazione legata allo srumento',NULL,'2019-12-27 00:00:00',NULL,'2020-01-29 00:00:00',N'No',NULL,NULL),
    (N'19-18','2019-04-01 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'A seguito del cambiamento di composizione dei prodotti ELITe MGB Kit (passaggio da Tfi a Taq), si richiede l''aggiornamento delle Schede Dati Sicurezza. In particolare, si richiede l''aggiornamento della SDS n.83, redatta per i prodotti ELITe MGB Kit (ref. RTSXXXING), aggiungendo l''indicazione degli altri ref. per i prodotto ELITe MGB KIt (ref. RTSXXXPLD, RTSTXXPLD, RTK015PLD e M800XXX).',N'Poter fornire una Scheda Dati Sicurezza aggiornata a seguito del cambio di formulazione per i prodotti ELITe MGB Kit',N'La SDS attualmente in uso (sds n.57 per i prodotti con ref. RTSXXXPLD, RTSTXXPLD, RTK015PLD e M800XXX), non è allineata alla nuova formulazione e riporta dei pericoli che non sono più presenti a seguito del cambio.',NULL,N'QARA
RDM',N'Regolatorio',N'Positivo in quanto sarà possibile fornire una scheda dati sicurezza aggiornata e allineata alla nuova formulazione dei prodotti ELITe MGB Kit
Positivo in quanto sarà possibile fornire una scheda dati sicurezza aggiornata e allineata alla nuova formulazione dei prodotti ELITe MGB Kit
Positivo in quanto sarà possibile fornire una scheda dati sicurezza aggiornata e allineata alla nuova formulazione dei prodotti ELITe MGB Kit',N'01/04/2019
01/04/2019
01/04/2019',N'modificare la SDS n.83 con l''aggiunta dell''indicazione dei ref.  RTSXXXPLD, RTSTXXPLD, RTK015PLD e M800XXX, in quanto la formulazione di questi prodotti è uguale ai prodotti già citati in questa SDS (ref. RTSXXXING)
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica 
aggiornamento FTP prodotti interessati',N'SDS n°83_01 IN USO - cc19-18_messa_in_uso_sds_n.83_rev._01_cc19-18.msg
 La modifica è significativa, come da MOD05,36 allegato  - cc19-16_mod05,36_significativa_1.pdf
mail di notifica autorità extra eu - cc19-18_notification_of_change_-rtsxxxpld;_rtstxxpld;_rtk015pld;_m800xxx_-_cc19-18.msg
NoC DEKRA - cc19-18_notice_of_change_egspa_rtk015pld_rts098pld_rtst01pld.pdf
notifica a R-BIOPHARMA - cc19-18_r_bio-eg_spa_change_notification_cc19-18.msg
FTP_098PLD (RTS098PLD, CTR098PLD) aggiornato - cc19-18_section_1_product_technical_file_index_rts098pld.docx
FTP_RTK015PLD aggiornato - cc19-18_sezione_1_product_technical_file_index_rtk015pld.txt
FTP_020PLD (RTS020PLD, CTR020PLD, STD020PLD) aggiornato - cc19-18_sezione_1_product_technical_file_index_rts020pld.docx
 FTP_031PLD (RTS031PLD, CTR031PLD, STD031PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts031pld.docx
FTP_T01PLD (RTST01PLD, CTRT01PD) aggiornato - cc19-18_section_1_product_technical_file_index_rtst01pld.docx
 FTP_032PLD (RTS032PLD, CTR032PLD, STD032PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts032pld.docx
 FTP_035PLD (RTS035PLD, STD035PLD, CTR035PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts035pld.docx
 FTP_078PLD (RTS078PLD, STD078PLD, CTR078PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts078pld.docx
 FTP_175PLD (RTS175PLD, STD175PLD, CTR175PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts175pld.docx
 FTP_036PLD (RTS036PLD, STD036PLD, CTR036PLD) aggiornato  - cc19-18_sezione_1_product_technical_file_index_rts036pld.docx
FTP_037PLD (RTS037PLD, STD037PLD, CTR037PLD) aggiornato - cc19-18_sezione_1_product_technical_file_index_rts037pld.docx
FTP_038PLD (RTS038PLD, STD038PLD, CTR038PLD) aggiornato - cc19-18_sezione_1_product_technical_file_index_rts038pld.docx
FTP_070PLD (RTS070PLD, STD070PLD, CTR070PLD) aggiornato - cc19-18_sezione_1_product_technical_file_index_rts070pld.docx
FTP_110PLD (RTS110PLD, STD110PLD) aggiornato - cc19-18_sezione_1_product_technical_file_indexrts110pld.docx
FTP_176PLD (RTS176PLD, STD176PLD, CTR176PLD) aggiornato - cc19-18_sezione_1_product_technical_file_indexrts176pld.docx',N'QMA
QMA
QMA',N'01/04/2019
01/04/2019
09/04/2019',N'12/04/2019
30/04/2019',N'04/04/2019
26/09/2019',N'Si',N'vedere "ALLEGATO_MODIFICA DMRI_MOD05,27_00_Elenco DocumentazionediProgettoEvalidazione"',N'No',N'non necessaria',N'No',N'non necessaria',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'R&D1.10',N'No',N'vedere mail di notifica autorità extra eu e NoC notificato a organismo notificato',N'No',N'vedere notifica r-bio',N'No',N'intervento non necessario',N'Michela Boi','2019-04-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-17','2019-03-28 00:00:00',N'Anna Capizzi',N'Progetto
Annullato / non approvato',N'ANNULLATO, APERTA RAC19-13 IN DATA 20/05/2019',N'ANNULLATO, APERTA RAC19-13 IN DATA 20/05/2019',N'ANNULLATO, APERTA RAC19-13 IN DATA 20/05/2019',NULL,N'QM',N'Controllo qualità
Marketing
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Nessun impatto sul processo di CQ e di DS. Solo normale adeguamento dei moduli di produzione e formazione al personale di produzione.
Nessun impatto sul processo di Produzione ma solo normale adeguamento alla nuova documentazione a seguito di formazione.
Possibile impatto sullo sviluppo dei prodotti futuri. Verificare meglio il formato dei prodotti e prevedere test di simulazione d''uso on-board più approfonditi.
Nessun impatto sul processo se non in termini di attività.
Nessun impatto sul processo se non in termini di attività.
Possibile impatto nella valutazione del cambio formato (punto D)
Impatto gravoso in termini di attività: aggiornamento ed approvazone etichette esterne, gestione della messa in uso delle IFU aggiornate, aggiornamento del FTP.
Impatto gravoso in termini di attività: aggiornamento ed approvazone etichette esterne, gestione della messa in uso delle IFU aggiornate, aggiornamento del FTP.
Impatto gravoso in termini di attività: aggiornamento ed approvazone etichette es',N'20/05/2019',N'Messa in suo della documentazione aggiornata.
Aggiornamento delle etichette ed approvazione (MOD04,13) dei prodotti interessati (FORNIRE ALLEGATO)
Aggiornamento del FTP dei prodotti interessati (FORNIRE ALLEGATO)
Aggiornamento delle IFU dei prodotti interessati (FORNIRE ALLEGATO)
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
ANNULLATO, APERTA RAC19-13 IN DATA 20/05/2019',NULL,N'QARA
QM
QMA
QMA
QMA
QMA
QMA',N'20/05/2019',N'20/05/2019',N'20/05/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2019-05-20 00:00:00',NULL,'2019-05-20 00:00:00',N'No',NULL,NULL),
    (N'19-16','2019-03-25 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Estendere l''uso del prodotto "BCR-ABL P210 ELITe MGB Kit" (ref. RTSG07PLD210) sullo strumento compatibile/validato ABI 7900 Real-Time PCR System, dandone indicazione nel manuale d''uso del prodotto. Sulla base del lavoro pubblicato "One-Step Quantitative Molecular Approach for Detection of BCR/ABL1 Rearrangement and for Monitoring of Minimal Residual Disease in CML Patients: An Inter Laboratory Study", Daraio F. et al. Blood 2016 128:5423; e in base al tipo di test e ai risultati ottenuti, il lavoro dimostra l''equivalenza delle performance ottenute da studi eseguiti con lo strumento ABI 7500 Fast Dx Real-Time PCR Instrument e ABI 7900 Real-Time PCR System. Data questa equivalenza, è possibile indicare nell''IFU che il prodotto è compatibile / è stato validato con lo strumento  ABI 7900 Real-Time PCR System. 
Dal momento che il prodotto "BCR-ABL P190 ELITe MGB Kit" (ref. RTSG07PLD190) può essere considerato equivalente al prodotto "BCR-ABL P210 ELITe MGB Kit" (ref. RTSG07PLD210) sulla base della composizione (oligonucleotidi - ad eccezione di un  primer forward-, probe), dell''utilizzo dello stesso ciclo termico, delle Temperature di melting molto simili tra i primer forward per P210 e P190 e dell''analisi dei dati condotta secondo lo stesso modello interpretativo,  tale modifica può essere apportata anche al manuale di "BCR-ABL P190 ELITe MGB Kit".',N'I prodotti potrebbero essere utilizzati anche in associazione allo strumento  ABI 7900 Real-Time PCR system, al momento non citato nei manuali d''Uso del prodotto.',N'I prodotti non potrebbero essere usati in associazione allo strumento  ABI 7900 Real-Time PCR system.',NULL,N'QARA
RDM
GATL',N'Regolatorio
Ricerca e sviluppo',N'Positivo, in quanto i prodotti potrebbero essere utilizzati anche con un altro strumento (ABI 7900 Real-Time PCR System) equivalente, in termini di performance, allo strumento ABI 7500 Fast Dx Real-Time PCR Instrument.
Positivo, in quanto i prodotti potrebbero essere utilizzati anche con un altro strumento (ABI 7900 Real-Time PCR System) equivalente, in termini di performance, allo strumento ABI 7500 Fast Dx Real-Time PCR Instrument.
Positivo, in quanto i prodotti potrebbero essere utilizzati anche con un altro strumento (ABI 7900 Real-Time PCR System) equivalente, in termini di performance, allo strumento ABI 7500 Fast Dx Real-Time PCR Instrument.
Positivo, in quanto i prodotti potrebbero essere utilizzati anche con un altro strumento (ABI 7900 Real-Time PCR System) equivalente, in termini di performance, allo strumento ABI 7500 Fast Dx Real-Time PCR Instrument.
Positivo, in quanto i prodotti potrebbero essere utilizzati anche con un altro strumento (ABI 7900 Real-Time PCR System) equivalente, in ter',N'25/03/2019
25/03/2019
25/03/2019
25/03/2019
25/03/2019',N'Modificare il manuale d''istruzioni per l''uso del prodotto  "BCR-ABL P210 ELITe MGB Kit" (ref. RTSG07PLD210) e del prodotto "BCR-ABL P190 ELITe MGB Kit" (ref. RTSG07PLD190), inserendo l''indicazione di possibilità di utilizzo in associazione allo strumento compatibile/validato ABI 7900 Real-Time PCR System
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Aggiornamento FTP
Informare assistenza applicativa e mkt sulla compatibilità / validazione su ABI 7900 Real-Time PCR System
Verifica ed approvazione IFU',N' IFU RTSG07PLD190 rev 084 - cc19-16_messa_in_uso_ifu_rtsg07pld190_2.msg
 IFU RTSG07PLD210 rev 08  - cc19-16_messa_in_usoi_ifu_rtsg07pld210_2.msg
IFU RTSG07PLD210 rev 08 - cc19-16_messa_in_usoi_ifu_rtsg07pld210.msg
 IFU RTSG07PLD190 rev 04  - cc19-16_messa_in_uso_ifu_rtsg07pld190.msg
 La modifica è significativa, come da MOD05,36 allegato  - cc19-16_mod05,36_significativa.pdf
mail di notifica - _egspa_sgq___notification_of_change_-rtsg07pld190__rtsg07pld210-_cc19-16.msg
FTP_G07PLD210 RTSG07PLD210 e STDG07PLD210 aggiornato - cc19-16_sezione_1_product_technical_file_index_rtsg07pld210.docx
mail di messa in uso  IFU RTSG07PLD190 rev 04 - cc19-16_messa_in_uso_ifu_rtsg07pld190_1.msg
mail di messa in uso  IFU RTSG07PLD210 rev 08  - cc19-16_messa_in_usoi_ifu_rtsg07pld210_1.msg',N'QMA
RT
QMA
QMA
QMA',N'25/03/2019
25/03/2019
25/03/2019
25/03/2019
25/03/2019',NULL,N'04/04/2019
31/10/2019
28/08/2019
04/04/2019
04/04/2019',N'No',N'nessun impatto',N'Si',N'attraverso avvertenza IFU',N'No',N'nessun impatto',N'ASA - Assistenza applicativa prodotti',N'ASA5 - Sorveglianza post-produzione',N'ASA5 "sorveglianza post-produzione"',N'Si',N'modifica significativa, vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessun ntervento',N'Michela Boi',NULL,NULL,'2019-10-31 00:00:00',N'No',NULL,NULL),
    (N'19-15','2019-03-08 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Aggiunta del fornitore "S.A.E. SCIENTIFICA S.R.L." alle seguenti specifiche: SPEC950-005, SPEC950-007, SPEC950-044 e SPEC950-078, 950-070',N'Inserimento nella specifica di un secondo fornitore ("S.A.E. SCIENTIFICA S.R.L.") che fornisce le stesse materie prime (produttore: Honeywell FlukaTM) rispetto all''attuale fornitore inserito in specifica (FISHER), ma presenta tempistiche di consegna decisamente più celeri. ',N'Tre dei codici elencati (950-005, 950-007 e 950-078) sono materie prime comuni utilizzate in molti prodotti e quindi le tempistiche di consegna risultano critiche. Il codice 950-044_etanolo viene acquistato anche come codice di rivendita e quindi le tempistiche di consegna al cliente risultano ancora più critiche; inoltre l''attuale distributore richiede la compilazione a ogni ordine di una documentazione per il rilascio e l''utilizzo di etanolo, mentre il distributore proposto no.',NULL,N'MM
PW
RDM',N'Acquisti
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo in quanto non solo le materie prime considerate avranno due fornitori da cui possibile approvvigionarsi, ma il nuovo fornitore  "S.A.E. SCIENTIFICA S.R.L." ha tempi più celeri di spedizione rispetto al primo, inoltre, utilizza  gli stessi codici prodotto del produttore non rendendo necessario inserire nuovi codici in specifica.
Impatto positivo in quanto non solo le materie prime considerate avranno due fornitori da cui possibile approvvigionarsi, ma il nuovo fornitore  "S.A.E. SCIENTIFICA S.R.L." ha tempi più celeri di spedizione rispetto al primo, inoltre, utilizza  gli stessi codici prodotto del produttore non rendendo necessario inserire nuovi codici in specifica.
Impatto positivo in quanto non solo le materie prime considerate avranno due fornitori da cui possibile approvvigionarsi, ma il nuovo fornitore  "S.A.E. SCIENTIFICA S.R.L." ha tempi più celeri di spedizione rispetto al primo, inoltre, utilizza  gli stessi codici prodotto del produttore non rendendo necessario inserire nuo',N'12/03/2019
12/03/2019
12/03/2019
13/03/2019
12/03/2019
12/03/2019',N'Selezione e qualifica nuovo forniore secono la PR06,01.
Aggiornamento delle specifiche SPEC950-005, SPEC950-007, SPEC950-044 e SPEC950-078, SPEC950-070
Messa in uso documentazione.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'selezione nuovo fornitore SAE scientifca - cc19-15_mod06,02selezionefornitore.pdf
 SPEC950-005, SPEC950-007, SPEC950-044 e SPEC950-078 - cc19-15_messa_in_uso_spec950-xx_cc19-15.msg
SPEC950-070_02 - cc19-15_messa_i_uso_spec950-070_02_cc19-15.msg
SPEC950-005, SPEC950-007, SPEC950-044 e SPEC950-078  - cc19-15_messa_in_uso_spec950-xx_cc19-15_1.msg
 SPEC950-070_02  - cc19-15_messa_i_uso_spec950-070_02_cc19-15_1.msg
La modifica NON è significativa, pertanto NON è necessario effettiuare alcun tipo di notifica - cc19-15_mod05,36_non_significativa.pdf',N'PW
RT
QMA
QMA',N'12/03/2019
13/03/2019
12/03/2019
12/03/2019',N'30/04/2019
30/04/2019
30/04/2019',N'03/06/2019
31/07/2019
31/07/2019
09/04/2019',N'No',N'nessun impatto sui DMRI di prodotto',N'No',N'non necessaria alcuna comunicazione',N'No',N'nessu impatto',N'F - Gestione dei Fornitori',N'F3 - Selezione nuovo fornitore',N'F3 "selezione nuovo fornitore"',N'No',N'non encessaria, vedere MOD05,36',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto',NULL,'2019-04-30 00:00:00',NULL,'2019-07-31 00:00:00',N'No',NULL,NULL),
    (N'19-14','2019-03-07 00:00:00',N'Ferdinando Fiorini',N'Processo di produzione
Dismissione prodotto',N'Dismissione prodotti  RTS076-M, CTR076, STD076, RTS110-M, STD110',N'L''obiettivo della dismissione dei reagenti inclusi nell''elenco seguente è il contenimento dei costi di produzione.
Elenco prodotti: RTS076-M (ENTEROVIRUS Q - PCR Alert AmpliMIX); CTR076 (ENTEROVIRUS - Positive Control); STD076 (ENTEROVIRUS Q - PCR Standard); RTS110-M (ASPERGILLUS spp. Q - PCR Alert AmpliMIX); STD110 (ASPERGILLUS spp. Q - PCR Standard).
L''apertura del Change Control avviene a seguito della valutazione del volume delle vendite in Italia e all''estero negli ultimi 24 mesi. Si ritiene che gli attuali utilizzatori dei prodotti possano passare all''acquisto degli analoghi prodotti 
ELITe MGB se correttamente affiancati dal FPS.',N'Mantenimento sul mercato di prodotti tecnologicamente superati e obsoleti.',NULL,N'S&MM-MDx
SMD',N'Acquisti
Assistenza applicativa
Controllo qualità
Gestione vendite e mkt estero
Marketing
Marketing vendite italia
Ordini
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'I prodotti che si intende dismettere possono essere sostituiti dall''analogo prodotto pleiadi. La dismissione e la conseguente eliminazione dei prodotti a listino semplificherà la gestione documentale a favore dei prodotti tecnologicamente più avanzati. I prodotti inclusi nell''elenco dovranno comunque essere cancellati dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria.
I prodotti che si intende dismettere possono essere sostituiti dall''analogo prodotto pleiadi. La dismissione e la conseguente eliminazione dei prodotti a listino semplificherà la gestione documentale a favore dei prodotti tecnologicamente più avanzati. I prodotti inclusi nell''elenco dovranno comunque essere cancellati dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria.
Gestione dei quantitativi dei materiali di produzione della linea Alert, riquantificati in base ai prodotti dismessi.
Gestione dei quantitativi dei materiali di produzione della linea Alert, riquant',N'14/03/2019
14/03/2019
07/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
02/04/2019
02/04/2019',N'Compilazione ed aggiornamento nel corso del tempo del MOD04,12 per ciascuno dei prodotti in elenco, secondo le fasi riportate (prodotto in dismissione, dismesso, cancellato).
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Allinemaneto in collaborazione con il personale degli acquisti dei quantitativi di acquisto delle materie prime sulla base dei prodotti dismessi.
Allinemaneto in collaborazione con il personale che pianifica le attività di produzione dei quantitativi di acquisto delle materie prime sulla base dei prodotti dismessi.
come da indagini all''apertura del CC, non ci sono gare attive con vincoli commerciali e non ci sono impedimenti alla dismissione dei prodotti RTS076-M, CTR076, STD076, RTS110-M, STD110. Non ci sono accordi e/o vincoli commerciali con i paesi EXTRA EU. La verifica delle scorte a magazzino è riportata nel MOD04,12 compilato per ciascun prodotto.
Come da MOD04,12 effettuare',N'MODULI CQ ARCHIVIATI - cc19-14__mod_cq_da_archiviare.xlsx
ARCHIVIAZIONE MODULI CQ - cc19-14_archiviazione_moduli_cq_oligomix.msg
veder mod04,12 - cc19-14_vederemod04,12.txt
La modifica è significativa. La dismissione dei prodotti è da comunicare a tutte le autorità compteneti presso cui i prodotti sono registrati. - cc19-14_mod05,36_significativa.pdf
Aggiornamento MOD962-RTS000_02, MOD18,02 del 7/10/19 - cc19-14_messa_in_uso_mod962-rts000_02_cc17-12_cc19-14_mod1802_del_07102019.msg',N'QMA
PP
PW
JPM
JPM
SMD
S&MM-MDx
QMA
SAD-OP
FPS
QMA
QC
QAS
RT',N'14/03/2019
15/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
14/03/2019
12/04/2019
14/03/2019
14/03/2019
02/04/2019
02/04/2019',NULL,N'30/04/2021
30/04/2021
30/04/2021
30/04/2021
30/04/2021
30/04/2021
29/10/2019
30/04/2021
30/04/2021',N'No',N'prodotto dismesso',N'Si',N'Come da MOD04,12 comunicazione ufficiale ai distributori ed ai clienti italia sul prodotto fuori produzione e dismesso ad esaurimento scorte a magazzino.',N'Si',N'Dismissione prodotti  RTS076-M, CTR076, STD076, RTS110-M, STD110',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'DC12',N'Si',N'vedere MOD05,36. ',N'No',N'nessun virtual manfacturer',N'No',N'-',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-13','2019-03-06 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Aggiornamento dei seguenti documenti per la messa in uso della bilancia codice BL9: 
- SPEC-BL_01_SpecificheAssistenzaTecnicaBilance (da aggiornare anche per il nuovo fornitore di assistenza tecnica)      
- IO11,01_02_VerificaInternaBilance',N'Corretta gestione degli strumenti interni in fase di manutenzione.',N'- Difficoltà nel rintracciare il fornitore di assistenza tecnica e nel fornire i dati sugli strumenti.
- Impossibilità nel eseguire la corretta Taratura interna degli strumenti BL.',NULL,N'TSS',N'Assistenza strumenti interni
Sistema qualità',N'Impatto positivo in quanto i documenti sono aggiornati con l''introduzione della nuova bilancia e del fornitore qualificato che esegue gli interventi di assistenza tecnica.
Impatti in termini di attività.',N'08/03/2019
08/03/2019',N'Aggiornamento: 
- SPEC-BL_01_SpecificheAssistenzaTecnicaBilance (da aggiornare anche per il nuovo fornitore di assistenza tecnica)      
- IO11,01_02_VerificaInternaBilance
Messa in uso documenti.',N'SPEC-BL_rev02, IO11,01 rev03 - cc19-13_messa_in_uso_spec-bl_02_io1104_003_cc19-13_spec-mp-dpe_03_cc19-6_mod1802_180319.msg
SPEC-BL_rev02, IO11,01 rev03  IN USO - cc19-13_messa_in_uso_spec-bl_02_io1104_003_cc19-13_spec-mp-dpe_03_cc19-6_mod1802_180319_1.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-13_mod05,36_non_significativa.pdf',N'TSS
QMA',N'08/03/2019
08/03/2019',N'29/04/2019
29/04/2019',N'22/03/2019
04/04/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'T - Taratura Esterna Strumenti',N'T1 - Gestione informazioni dello strumento',N'T1 "Gestione informazioni dello strumento"',N'No',N'-',N'No',N'-',N'No',N'-',N'Fabio Panariti','2019-04-29 00:00:00',NULL,'2019-04-04 00:00:00',N'No',NULL,NULL),
    (N'19-12','2019-02-28 00:00:00',N'Alessandra Gallizio',N'Confezione',N'Durante lo Steering Committee è stato approvato il formato da 48 test/kit per Macrolide-R/MG ELITe MGB Kit. 
Il kit è stato sviluppato nell''ambito del progetto SCP2017-037 con formato da 96 test/kit, e il III lotto pilota, U1118AR, è stato prodotto ed etichettato per la vendita con questo formato.
Si richiede la rilavorazione del lotto U1118AR con nuovo formato da 48 test/kit, prima della chiusura del progetto SCP2017-037; si richiede inoltre di predisporre la documentazione di prodotto necessaria per la produzione ed il controllo dei prossimi lotti di vendita con il nuovo formato da 48 test/kit.
Il nuovo formato prevede 4 tubi di R/MG PCR Mix in una Nunc da 5.',N'Data la natura del test (test reflex solo sui campioni risultati positivi al test STI Plus), il formato da 48 test/kit meglio si adatta alle esigenze del cliente.',N'Il formato da 96 test/kit risulta meno appetibile per il cliente.',NULL,N'BC
SPM
MM
QARA
QM
RDM',N'Budgeting control
Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività. 
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrrà un prodotto che meglio si adatta alle esigenze del cliente. Impatto in termini di attività.
L''impatto è positivo in quanto si produrr',N'28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019',N' Creazione di un nuovo codice e calcolo del costo standard; costificazione e sblocco del codice.
anagrafiche nuovo formato
rilavorazione del lotto U1118AR: se possibile utilizzare MOD 09,08 e le informazioni relative al formato qui allegate (allegato 1)
revisionare modulo 962-RTS401ING, "scheda di produzione" (si veda allegato 1) e moduli CQ
approvazione etichetta revisionata riportante il nuovo numero di test/kit (48), il nuovo numero di tubi /kit (4) e i nuovi dati barcode (si veda allegato 1)
messa in uso nuovo modulo 962-RTS401ING, "scheda di produzione"
 Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.
Aggiornamento DMRI2018-007 con i nuovi codici dei moduli di produzione e cq
Aggiornamento SPEC951-RTS401ING',N'allegato 1 - allegato_1_al_cc19-12.docx
FORMAZIONE MOD962-RTS401ING-48_rev01 DEL 9/4/2019 - cc19-12_messa_in_uso_mod_macrolide_cc19-12.msg
rilavorazione lotto U1118AR_in_U0419AG - cc19-12_rilavorazione_u1118ar_in_u0419ag.pdf
MODULI produzione e cq in uso al 02/05/2019 - cc19-12_messa_in_uso_mod_macrolide_cc19-12_1.msg
Etichetta approvata il 15/3/19 - cc19-12_mod04,13_15-3-19.pdf
Messa in uso moduli produzione e cq - cc19-12_messa_in_uso_mod_macrolide_cc19-12_2.msg
La modifica NON è significativa, non è necessario notificarla. - cc19-12_mod05-36_9-4-19.pdf
SPEC951-RTS401ING-48_00 - cc19-12_messa_in_uso_spec-xx_cc19-25_cc34-17_cc19-12_mod1802_del_24092019.msg',N'PT
MM
PP
PT
DS
QM
QMA
BC
QMA
RT
QMA',N'28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019',N'22/03/2019
22/03/2019
22/03/2019
30/04/2019
22/03/2019
30/04/2019
30/04/2019',N'22/03/2019
22/03/2019
10/04/2019
02/05/2019
15/03/2019
02/05/2019
10/04/2019
24/09/2019',N'Si',N'DMRI2018-007',N'No',N'comunicazione al cliente a seguito di chiusura porgetto',N'No',N'prodotto nuovo',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.6 - Fase di Sviluppo: Pianificazione',N'nessun impatto',N'No',N'non necessaria perchè l modifica è avvenuta prima della chiusura di progetto',N'No',N'non necessaria',N'No',N'-',N'Alessandra Gallizio','2019-04-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'19-11','2019-02-27 00:00:00',N'Renata  Catalano',N'Etichette',N'Nel corso dell''Audit al fornitore R-BIOPHARM è emerso un disallineamento nella documentazione EG SpA circa le regole da adottare per la definizione del lotto dei prodotti OEM R-BIOPHARM (evidenze A03, A04 del Report di Audit) . Nel testo del Packaging and Labeling Plan correntemente in uso (PLP2018-002 Rev.00) è riportato di generare il numero di lotto per le etichette esterne secondo lo schema CmmyyZx e per le etichette interne seguendo le regole del fabbricante. Nello snapshot delle etichette presente sul medesimo documento il codice riportato per le etichette esterne è UmmyyYx e per quelle interne è Uxxxxxx. Nei moduli MOD04,13 il codice riportato per entrambe le etichette è CmmyyZx. Nel corso dell''audit è stato definito che il codice corretto è UmmyyYx per il lotto del prodotto finito e dei componenti; tale codice va riportato sulle etichette. Si richiede quindi di allineare in questo modo il documento PLP2018-002 (da allegare all''OEM Manufacture and  Supply Agreement) e i MOD04,13 dei prodotti RTK574ING, RTK573ING, RTK562ING, RTK561ING',N'La descrizione univoca nella documentazione EG SpA delle regole definite per assegnare i lotti di prodotto garantisce la corretta assegnazione dei lotti e del loro riporto sulle etichette da parte di R-BIOPHARM',N'Il mancato allineamento nella documentazione EG SpA delle regole definite per assegnare i lotti di prodotto potrebbe determinare l''assegnazione di lotti non corretti da parte di R-BIOPHARM',NULL,N'PMS
QM',N'Program mangment
Regolatorio
Sistema qualità',N'Impatto positivo. La descrizione univoca nella documentazione EG SpA delle regole definite per assegnare i lotti di prodotto riduce il rischio di dover gestire non conformità legate a questo aspetto
Impatto positivo. La descrizione univoca nella documentazione EG SpA delle regole definite per assegnare i lotti di prodotto  consente una maggiore chiarezza negli accordi con R-BIOPHARM
Impatto positivo. La descrizione univoca nella documentazione EG SpA delle regole definite per assegnare i lotti di prodotto riduce il rischio di dover gestire non conformità legate a questo aspetto.',N'13/03/2019
13/03/2019
13/03/2019',N'Aggiornare i MOD04,13 dei prodotti RTK574ING, RTK573ING, RTK562ING, RTK561ING inserendo le etichette con il codice del lotto corretto UmmyyYx . Inviare i MOD04,13 aggiornati a R-BIOPHARM/CONGEN.
Aggiornare il Packaging and Labeling Plan (PLP2018-002) con l''indicazione corretta per generare il numero di lotto (UmmyyYx). Inserire fra gli approvatori anche personale di R-BIOPHARM/CONGEN. Allegare il documento aggiornato all''OEM Manufacture and  Supply Agreement
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'La modifica non è significativa, pertanto non è necessario effettuare alcuna notifica. - cc19-11_mod05,36_non_significativa.pdf',N'QM
QMA
PMS
QARA',N'13/03/2019
13/03/2019
13/03/2019',NULL,N'30/04/2019',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.6 - Fase di Sviluppo: Pianificazione',N'R&D1.6 ""Fase di Sviluppo: Pianificazione, Stesura del piano generale  di Verifica e Validazione del prodotto, Stesura dei protocolli di Verifica, Stesura dei protocolli di Validazione, Contratti con centri esterni di Validazione, Comunicazione all''autorità competente, Stesura dei protocolli di Stabilità, Stesura del piano di confezionamento ed etichettatura, Stesura dell''elenco dei materiali del prodotto (approvigionamento, creazione anagrafiche, costificazione)", Stesura del piano di Design Transfer, Stesura bozza del DMRI, Stesura del piano di Sviluppo di Progetto".',N'No',N'non necessaria vedere modulo MOD05,36',N'Si',N'R-BIOPHARM è il virtual manufacturer oggetto delle notifica',N'No',N'nessun impatto, modifiche di sistema',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-10','2019-02-22 00:00:00',N'Samuela Margio',N'Confezione',N'Adeguamento del confezionamento dei prodotti STDxxxPLD (vedi allegato A) al confezionamento proposto per STD150ING (Pneumocystis ELITe Standard). Sui prodotti interessati dal CC viene richiesto 1) di modificare la tipologia di tubino utilizzato per la dispensazione degli standard passando da Tubo da 0,5 ml con laccetto e inserto colorato a tubo da 0,5 ml senza laccetto con tappo colorato.  2) di applicare di una etichetta con stesso codice colore del tappo per facilitare la chiusura del tubino ed evitare scambi di tappi tra standard a concentrazioni diverse.',N'Tale operazione risulta essere vantaggiosa per l''utilizzatore finale che non dovrà più rimuovere manualmente (o con forbici) il tappo degli standard prima dell''utilizzo su ELITe InGenius senza incorrere in errori durante la chiusura degli stessi e mitigando il rischio di scambio dei tappi tra concentrazioni diverse.
Per quanto riguarda il processo produttivo quest''ultimo sarà semplificato riducendo le operazioni manuali: non sarà più necessario aprire i tappi prima della dispensazione e inserire gli inserti colorati.',N'Per i prodotti validati su InGenius sarà necessario suggerire una procedura manuale di rimozione dei tappi con rischio di scambio e contaminazione reagenti.  ',NULL,N'SPM
MM
OM
QARA
RDM',N'Acquisti
Assistenza strumenti interni
Controllo qualità
Marketing
Marketing vendite italia
Operation Site
Produzione
Program mangment
Regolatorio
Ricerca e sviluppo
Sistema informatico
Sistema qualità',N'Impatto in termini di attività: valutazione delle offerte per delle stampanti a colori di etichette e richiesta di offerta al fonitore qualificato di rocchetti di etichette colorate pre-stampate. 
Impatto in termini di attività: valutazione delle offerte per delle stampanti a colori di etichette e richiesta di offerta al fonitore qualificato di rocchetti di etichette colorate pre-stampate. 
Impatto in termini di attività: valutazione delle offerte per delle stampanti a colori di etichette e richiesta di offerta al fonitore qualificato di rocchetti di etichette colorate pre-stampate. 
Aggiornamento documentazione di produzione con nuove specifiche di materiali plastici ed etichette.
Aggiornamento documentazione di produzione con nuove specifiche di materiali plastici ed etichette.
La corrispondenza codice colore tra tappo ed etichetta e l''uso di tubini senza laccetto e con tappo colorato migliora l''usabilità del prodotto e riduce il rischio di errore da parte del cliente durante l''utilizzo, deter',N'22/02/2019
22/02/2019
22/02/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019',N'Richiedere un''offerta per 1 stampante a colori e per i toner a colori necessari. 
Richiedere l''offerta per stampanti bianco e nero.
Richiedere l''offerta per etichette colorate pre-stampate. 
Aggiornamento documentazione di produzione con:
- nuove specifiche di materiali plastici ed etichette
- verifica dei tempi di esposizione a temperatura ambiente per l''etichettatura
- revisione processo produttivo in fase di preparazione dei materiali: eliminare inserzione inserti colorati ed apertura tubi dal processo produttivo e dalla documentazione (moduli e istruzioni)
- revisione processo produttivo in fase di etichettatura (eliminare l''etichettautra prima della dispensazione dal processo e dalla documentazione).
Valutare in accordo con MM gli attuali moduli degli standard al fine di stabilire se il tempo massimo di 60 minuti indicato per la dispensazione di 30 mL è ancora idoneo. Se l''etichettare dei tubi della dispensazione dovesse portare ad un rallentamento significativo, valutare la riduzione del vo',N'elenco prodotti i cui moduli di produzione sono da modi10/04/ficare. - cc19-10_allegato_a_elenco_std_da_modificare.xlsx
messa in uso MOD962-STDxxPLD - cc19-10_messa_in_uso_mod962-stdxxpld_cc19-10_rac19-20.msg
Scelte etichette codice colore e stampanti bianco e nero - cc19-10_etichettecodcolorestampb-n_1.txt
elenco prodotti cui approvare le nuove etichette - cc19-10_allegato_a_elenco_std_da_modificare_1.xlsx
SPEC954-189,SPEC954-190, SPEC954-191, SPEC954-192, SPEC954-193 revisione 00 - cc19-10_messa_in_uso_specifiche_codice_colore_etichette_std.msg
A seguito di installazione da parte del fornitore e di verifica delle modalità di manutenzione si stailisce che: le stampanti necessitano esclusivamente di una pulizia dei meccanismi che non influisce sulla qualità della stampa,  essendo questa a trasferimento termico, non si considerano pertanto strumenti influenti sulla qualità del prodotto.  le nuove stampanti sono state codificate come strumenti xx senza necessità di interventi di manutenzione programmata.  secondo la PR11,01 non è necessario produrre documenti quali iq/oq/pq ecc. - cc19-10_stampantinoninfluentiqualita.txt
messa in uso specifiche - cc19-10_messa_in_uso_specifiche_codice_colore_etichette_std.msg
messa in uso MOD962-STDxxPLD - cc19-10_messa_in_uso_mod962-stdxxpld_cc19-10_rac19-20_1.msg
stampanti non influenti sulla qualità del prodotto - cc19-10_stampantinoninfluentiqualita_1.txt
NOTIFICA DEKRA - cc19-10_elitechgroup_spa_notice_of_change_dekra.msg
 L''introduzione del tubo primario senza laccetto è una modifica significativa. deve essere notificata alle autorità competenti e all''organismo notificato prima di essere introdotta  - cc19-10_mod05,36_significativa.pdf
la sostituzione dei tubi con laccetto a tubi senza laccetto è tracciata solo nei moduli di produzione, documenti confidenziali,  solitamente, non inviati in fase di registrazione. si ritiene pertanto non necessario procedere in tal senso. - cc19-10_modificanondanotificare.txt
elenco DMRI da aggiornare - cc19-10_allegato_b_elenco_dmri_da_aggiornare.xlsx
elenco prodotti per i quali è necessario aggiornare FTP - cc19-10_allegato_a_elenco_std_da_modificare_2.xlsx
Aggiornamento FTP P210 - cc19-10_aggiornamento_ftp_p210.msg
nuovi kayout etichette - cc19-10_richiesta_offerta_per_nuovi_layout_etichette.msg
acquistate etichette codice colore e stampanti bianco e nero - cc19-10_etichettecodcolorestampb-n.txt
le stampanti sono strumenti non influenti sulla qualità del prodotto - cc19-10_stampantinoninfluentiqualita.txt',N'PW
DS
QC
OM
MM
MM
MM
PMS
QARA
QM
QMA
MM
RDM
RT
ITM
ITM
SPM
TSS
QMA
QARA
QM
QMA
QMA
QMA
QMA
QMA
PW
PW
OM
ITT
MM
PMS',N'22/02/2019
22/02/2019
22/02/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019
18/03/2019',NULL,N'01/04/2021
30/10/2020
10/04/2019
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
30/10/2020
01/04/2021
01/04/2021
12/06/2019
30/10/2020
30/10/2020
01/04/2021
30/10/2020
30/10/2020
30/10/2020',N'Si',N'Aggiornare i DMRI citati nell''ALLEGATO B',N'No',N'-',N'No',N'-',N'P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',N'P3.6 - Etichettatura semilavorati e componenti',N'Da aggiornare P1.6 "Etichettatura semilavorati e componenti", creazione RA stampanti ed aggiornamento VMP strumenti/sw',N'Si',N'Secondo NBOG BPG2014-3 è significativa per l''introduzione dei tubi primari senza laccetto in quanto si tratta di uuna modifica di packaging del contenitore primario, mentre la modiifica grafica dell''etichetta (introduzione codice colore) non è considerata significativa. 
01/04/2021 la sostituzione dei tubi con laccetto a tubi senza laccetto è tracciata solo nei moduli di produzione, documenti confidenziali, 
solitamente, non inviati in fase di registrazione. si ritiene pertanto non necessario procedere in tal senso.',N'Si',N'notificare il cambiamento per il prodotto CMV',N'No',N'-',N'Ferdinando Fiorini',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'19-9','2019-02-13 00:00:00',N'Alessandro Bondi',N'Documentazione SGQ',N'Revisione post rilascio versione software 1.3 ELITe InGenius del MOD19,23 Assay protocol evaluation form',N'Eliminare informazioni di poco valore e aggiungerne relative alla nuova implementazione del sw ELITe InGenius versione 1.3.0.12',N'Non aggiornamento del modulo in seguito al rilascio del nuovo sw',NULL,N'QM
GATL',N'Assistenza applicativa
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Utilizzo del nuovo modulo nella procedura di creazione degli assay di terze parti
Verifica dei documenti in cui è citato l''utilizzo del presente modulo
Impatto positivo: utilizzo di documentazione aggiornata.
Nessun impatto se non in  termini di attività.',N'15/03/2019
15/02/2019',N'Aggiornamento del modulo e informazione al personale R&D interessato.
Messa inuso modulo.',N'MOD19,23_02 in uso. personale INFORMATO - cc19-9_messa_in_uso_mod1923_rev02.msg
 MOD19,23_02 in uso. personale INFORMATO  - cc19-9_messa_in_uso_mod1923_rev02_1.msg
La modifica NON è significativa, NON è pertanto necessarioeffettuare alcun tipo di notifica - cc19-9_mod05,36_non_significativa.pdf',N'GATL
QMA',N'13/02/2019
02/03/2019',N'29/03/2019
29/03/2019',N'16/04/2019
16/04/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'-',N'No',N'-',N'No',N'-',N'Alessandro Bondi','2019-03-29 00:00:00',NULL,'2019-04-16 00:00:00',N'No',NULL,NULL),
    (N'19-8','2019-02-06 00:00:00',N'Alessandra Vinelli',N'Documentazione SGQ',N'Aggiornamento del logo sui documenti EGSpA.',N'Conformità alla nuova "Visual Identity" di gruppo.',N'Confusione dell''identità di gruppo presso i clienti.',NULL,N'SPM
OM
QARA
QM
T&CM
ITM
JPM',N'Acquisti
Amministrazione
Assistenza strumenti esterni
Assistenza strumenti interni
Gare
Gestione vendite e mkt estero
Marketing
Operation Site
Regolatorio
Sistema informatico
Sistema qualità',N'Impatto in termini di attività: verifica dei dati sulla carta intestata.
Impatti in termini di attività: valutazione  della presenza del logo sui software utilizzazti in EGSpA  e/o sui documenti in uscita dagli stassi.
Impatti in termini di attività: valutazione  della presenza del logo sui software utilizzazti in EGSpA  e/o sui documenti in uscita dagli stassi.
Impatto in termini di attività: messa in uso dei documenti agggiornati, valutazione dei documenti SGQ contenenti il logo.
Impatto in termini di attività: messa in uso dei documenti agggiornati, valutazione dei documenti SGQ contenenti il logo.
Impatto in termini di attività: aggiornamento delle documentazione di MKT.
Impatto in termini di attività: aggiornamento delle documentazione di MKT.
Impatto in termini di attività: verifica della carta intestata.
Impatto in termini di attività: aggiornamento del logo sui listini. 
L''aggiornamento dei loghi presenti nel sito aziendale e dei loghi presenti sulle etichette e sulle scatol',N'06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
02/05/2019
06/02/2019
06/02/2019
06/02/2019
02/05/2019
02/05/2019
02/05/2019
02/05/2019
06/02/2019
06/02/2019',N'Verifica dei dati presenti sulla carta intestata.
Aggiornamento del logo sui documenti in uscita dal software NAV.
Coordinandosi con i Resposnabili dei processi interessati e con i fornitori qualificati aggiornare il logo sui sw: Dashboard - Sgatt - QTool - Sgatt gare e offerte  
Aggiornamento delle documentazione interna laddove sia presente il logo. 
Messa in uso carta intestata e dei template in power point destinati alle presentazioni corporate e di prodotto
Aggiornamento delle Marketing Presentation, Customer Presentation, Pre-Launch Presentation e Teaser.  Cambio del logo sulle brochures, flyer, gadgets, vele e stand per congressi. Cambio del logo sul listino Italia e per i distributori. 
Valutazione dell''aggiornamento delle etichette interne dei prodotti ELITe MGB elimnando le stelline in alto a sinistra, e l''indirizzo in basso sotto la ragione sociale (quest''ultimo a seguito di conferma da parte del Regolatorio). 
Verifica dei dati presenti sulla carta intestata. Coordinandosi con ITM e ',N' in uso documentazione per collaudo, ritiro strumenti  - cc19-8_messa_in_suo_modxx_certificati_collaudo_report_ritiro_strumenti_check_list_con_nuovo_logo_cc19-8.msg
 in uso documentazione per collaudo, ritiro strumenti  - cc19-8_1_messa_in_suo_modxx_certificati_collaudo_report_ritiro_strumenti_check_list_con_nuovo_logo_cc19-8.msg
in uso tab/tsb - cc19-8_messa_in_uso_tab_e_tsb_nuovo_logo_cc19-8.msg
eliminazione stelline(in alto a sinistra)  dalle etichette dei tubi interni dei prodotti elite - cc19-10_modifiche_graficheetichette.pdf
carta intestata in uso - cc19-8_messa_in_uso_nuova_carta_intestata_cc19-8.msg
messa in uso nuova carta intestata - cc19-8_messa_in_uso_nuova_carta_intestata_cc19-8_1.msg
Messa in uso carta intestata e dei template in power point destinati alle presentazioni corporate e di prodotto - cc19-8_modelli_in_uso_per_le_presentazioni.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-8_mod05,36_non_significativa.pdf
messa in uso carta intestata - cc19-8_messa_in_uso_nuova_carta_intestata_cc19-8_2.msg
logo sagt aggiornato, loghi dashboard e qtool in uso con logo nuovo - cc19-8_logo_aggiornato_sui_sw_del_service.msg
in uso tab e tsb - cc19-8_messa_in_uso_tab_e_tsb_nuovo_logo_cc19-8_1.msg
approvazione dell''eliminazione stelline (in alto a sinistra) dalle etichette dei tubi interni dei prodotti elite e dell''indirizzo se necessario (a seguito di approvazione regolatorio) - cc19-10_modifiche_graficheetichette_1.pdf
lì''indirizzo può essere eliminato dalle etichette interne come da ISO15223-1 - cc19-10_modifiche_graficheetichette_2.pdf',N'QMA
CFO
ITM
QMA
QMA
QARA
QM
QMA
JPM
T&CM
S&MM-MDx
OM
GSC
PW
GSC
ITM
TSS
GSC
SPM
QARA',N'06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
07/05/2019
06/02/2019
06/02/2019
07/05/2019
06/02/2019
06/02/2019
06/02/2019
06/02/2019
02/05/2019
06/02/2019
06/02/2019',N'15/03/2019
31/12/2019
15/03/2019
30/06/2019
31/12/2019',N'06/03/2019
13/11/2020
13/11/2020
13/11/2020
29/03/2019
13/11/2020
17/05/2019
06/03/2019
13/11/2020
07/05/2019
17/05/2019
13/11/2020
08/03/2019
13/11/2020
09/04/2019
17/05/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'ET - Gestione Etichette',N'ET1 - Individuazione dei simboli/dati da inserire sull''etichetta',N'nessun impatto sul RA',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2019-12-31 00:00:00',NULL,'2020-11-13 00:00:00',N'No',NULL,NULL),
    (N'19-7','2019-02-04 00:00:00',N'Katia Arena',N'Materia prima',N'Si richiede di inserire un fornitore alternativo a quello già presente per il reagente 950-212 TAE 50% .',N'Un doppio fornitore aumenta la garanzia di fornitura.',N'Senza un fornitore alternativo, nel caso in cui il prodotto non venga fornito entro i tempi concordati in fase di ordine di acquisto, potrebbe esserci un ritardo nella produzione.',NULL,N'MM
QC
QM
RDM',N'Acquisti
Produzione
Ricerca e sviluppo
Sistema qualità',N'Impatto in termini di carico di ore lavoro.
Impatto positivo in quanto la presenza di un fornitore alternativo aumenta la garanzia di fornitura.
Impatto positivo in quanto la presenza di un fornitore alternativo aumenta la garanzia di fornitura e riduce la possibilità di ritardi nella produzione.
Nessun impatto senon in termini di carico ore lavoro.
impatto positivo in quanto la presenza di un fornitore alternativo aumenta la granazia di fornitura e riduce la possibilità di ritardi nella produzione.',N'07/02/2019
04/02/2019
07/02/2019
07/02/2019
07/02/2019',N'Ricerca fornitore alternativo ed aggiornamentro della SPEC950-2012 rev01
Testare la nuova specifica al primo arrivo del reagente acquistato presso il nuovo fornitore.
informazione/formazione sulla nuova specifica.
messa in uso SPEC950-212 rev02
informazione/formazione sulla nuova specifica',N'Fornitori alternativi a Sigma: ThermoFisherSC. e Invitrogen  - cc19-7_sgq_messa_in_uso_spec950-212_cc19-7_1.msg
primo lotto ThermoFisher del 12/02/2019 - cc19-7_950-212_19-po-00168_00729406,00723528.pdf
Il primo lotto di produzione in cui è stato utilizzato il reagente TAE è C0219AE, prodotto EPH03, risultato CONFORME al CQ in data 14/02/2019 - cc19-7_primo_lotto_c0219ae.docx
SPEC950-212rev02in uso al 13/2/2019 - cc19-7_sgq_messa_in_uso_spec950-212_cc19-7.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-7_mod05,36_non_significativa.pdf',N'RT
MM
W
QMA
MM',N'07/02/2019
07/02/2019
07/02/2019
07/02/2019
07/02/2019',N'28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019',N'12/02/2019
14/02/2019
12/02/2019
13/02/2019
13/02/2019',N'No',N'non necessario',N'No',N'non necessario',N'No',N'nessun impatto',N'A - Gestione degli Acquisti',N'A1 - Pianificazione approvvigionamento materiali',N'A1 "Pianificazione approvvigionamento materie prime"',N'No',N'non necessaria, vedere MOD05,36.  La modifica non è significativa in quanto trattasi di introduzione di un nuovo fornitore di un reagente a bassa criticità equivalente a quello attualmente in uso.',N'No',N'non necessaria',N'No',N'non necessarii',NULL,'2019-02-28 00:00:00',NULL,'2019-02-14 00:00:00',N'No',NULL,NULL),
    (N'19-6','2019-01-30 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Aggiornamento delle specifiche d''assistenza tecnica:
- SPEC-MP-PE_02 "Specifiche Assistenza Tecnica Micropipetta Dispensatore" per cambio della ragione sociale del fornitore del servizio di Taratura Esterna e 
- SPEC-TS_02 "Specifiche Assistenza Tecnica Termometro Di Riferimento" per cambio della tipologia di sonda utilizzata (da tipo "J"a tipo "T", la sonda "J" non è più fabbricata),
- SPEC-PS_00_ "Specifiche Assistenza Tecnica Pesi" per cambio della ragione sociale (30/4/19)',N'Corretta gestione degli strumenti interni in fase di manutenzione.',N'Difficoltà nel rintracciare il fornitore di assistenza tecnica e nel fornire i dati sugli strumenti.',NULL,N'MM
QC
QM
RDM
TSS',N'Assistenza strumenti interni
Sistema qualità',N'Impatto positivo in quanto i documenti sono aggiornati con il fornitore qualificato di assistenza tecnica, nel caso di dispensatori e micropipette, e con la corretta tipologia di sonda da tarare nel caso del termometro di riferimento.
Impatto positivo in quanto i documenti sono aggiornati con il fornitore qualificato di assistenza tecnica, nel caso di dispensatori e micropipette, e con la corretta tipologia di sonda da tarare nel caso del termometro di riferimento.
Nessun impatto, se non in termini di ore lavoro.',N'06/02/2019
06/02/2019
06/02/2019',N'Aggiornamento specifiche
IQ/OQ/PQ del TS2 con la nuova sonda "T" (MOD11,13A e MOD11,13B").
messa in uso specifiche',N'SPEC-MP-DPE_rev03 - cc19-6_messa_in_uso_spec-bl_02_io1104_003_cc19-13_spec-mp-dpe_03_cc19-6_mod1802_180319.msg
SPEC-PS_01 - cc19-6_messa_in_uso_specifiche_assistenza_tecnica_cc19-6,_cc19-19.pdf
SPEC-TS_03 - cc19-6_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020.msg
 SPEC-MP-DPE_rev03 IN USO - cc19-6_messa_in_uso_spec-bl_02_io1104_003_cc19-13_spec-mp-dpe_03_cc19-6_mod1802_180319_1.msg
SPEC-PS_01 - cc19-6_messa_in_uso_specifiche_assistenza_tecnica_cc19-6,_cc19-19_1.pdf
La modifica non è significativa, per tanto non è necessario effettuare alcun tipo di notifica - cc19-6_messa_in_uso_specifiche_assistenza_tecnica_cc19-6,_cc19-19_2.pdf
SPEC-TS_03 - cc19-6_messa_in_uso_specifiche_assistenza_tecnica_cc20-40_cc20-41_rac20-14_cc19-6_mod1802_del_20072020_1.msg
TS2 dismesso, sostituito dal TS12 - cc19-6_ts_sostituito_dal_ts12.txt',N'TSS
QM
TSS',N'06/02/2019
06/02/2019
06/02/2019',NULL,N'29/07/2020
29/07/2020
29/07/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'T - Taratura Esterna Strumenti',N'T3 - Richiesta offerta  al fornitore',N'T3 "Richiesta offerta al fornitore"',N'No',N'-',N'No',N'-',N'No',N'-',N'Fabio Panariti',NULL,NULL,'2020-07-29 00:00:00',N'No',NULL,NULL),
    (N'19-5','2019-01-29 00:00:00',N'Samuela Margio',N'Manuale di istruzioni per l''uso',N'Aggiornamento delle IFU di STI PLUS ELITe MGB Kit (RTS400ING) integrando nella sezione Limite di Rilevazione (LoD) i tamponi cervico-vaginali come riportato nel REP2018-151, ed integrare al paragrafo "Altri prodotti richiesti" l''uso di dispositivi di raccolta del campione con caratteristiche equivalenti agli SWAB-Kit.',N'Dimostrare di aver effettuato la verifica delle performance di LoD su tutte le matrici validate.  Diminuire la restrizione sull''uso di dispositivi di raccolta del campione con caratteristiche equivalenti agli SWAB-Kit.',N'Incongruenza tra report e IFU, unica identificazione del dispositivo di raccolta del campione ',NULL,N'SPM
QMA
RDM
GATL
JPM',N'Assistenza applicativa
Marketing
Program mangment
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate ed è meno restrittivo sull''uso dei tamponi di raccolta dei campioni.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate ed è meno restrittivo sull''uso dei tamponi di raccolta dei campioni.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate ed è meno restrittivo sull''uso dei tamponi di raccolta dei campioni.
Impatto positivo in quanto il manuale di istruzioni per l''uso riporta i dati su tutte le matrici validate ed è meno restrittivo sull''uso dei tamponi di raccolta d',N'04/02/2019
04/02/2019
04/02/2019
04/02/2019
04/02/2019
04/02/2019
05/02/2019
05/02/2019',N'Aggiornamento IFU in italiano ed inglese. Verifica aderenza contenuti IFU e REP2018-151, integrazione del IFU con  l''uso di dispositivi di raccolta del campione con caratteristiche equivalenti agli SWAB-Kit.
Approvazione IFU.
Verifica, messa in uso e caricamento sul WEB di IFU. Preparazione avvertenza.
Approvazione IFU.
Approvazione IFU.
Informare DEKRA sulla unova revisione del manuale.
Verificare la significatività della modifica e, se necessario, identificare i paesi in cui il prodotto è registrato per effettuare la notifica.',N'IFU RTS400ING rev01 - cc19-5_ifu_rts400ing_cc19-5.msg
IFU RTS400ING rev01 - cc19-5_ifu_rts400ing_cc19-5_1.msg
IFU RTS400ING rev01 - cc19-5_ifu_rts400ing_cc19-5_3.msg
IFU RTS400ING rev01 - cc19-5_ifu_rts400ing_cc19-5_4.msg
IFU RTS400ING rev01 - cc19-5_ifu_rts400ing_cc19-5_2.msg
La modifica non è significativa, pertanto non deve essere notificata. - cc19-5_mod05,36_non_significativa.pdf',N'RT
SPM
GATL
PMS
QMA
QMA
QMA
QMA',N'04/02/2019
04/02/2019
04/02/2019
04/02/2019
04/02/2019
04/02/2019
05/02/2019
05/02/2019',N'28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/02/2019
28/03/2019
28/03/2019',N'17/10/2019
17/10/2019
17/10/2019
17/10/2019
17/10/2019
09/04/2019',N'No',N'nessun impatto',N'Si',N'presente l''avvertenza con la IFU rev01',N'No',N'nessun impatto',N'IFU - Gestione Manuali',N'IFU2 - riesame manuale',N'IFU2 "riesame manuale"',N'No',N'vedere MOD05,36. La modifica non è significativa, in quanto non introduce controindicazioni o avvertenze con impatto sulla sicurzza del prodotto, ma integra dei dati già presenti sui report di progetto e quindi già visionati dall''organismo notificato.',N'No',N'nessun virtual manufacturer',N'No',N'non necessaira',NULL,'2019-03-28 00:00:00',NULL,'2019-10-17 00:00:00',N'No',NULL,NULL),
    (N'19-4','2019-01-22 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Aggiornamento del SW Linkomm, codice interno SW31, al fine di  semplificare l''interfaccia utente, permettere di effettuare ricerche efficaci, ampliare il contenuto per rispondere ai requisiti richiesti dal MDSAP.',N' Semplificare l''interfaccia utente, permettere di effettuare ricerche efficaci, ampliare il contenuto per rispondere ai requisiti richiesti dal MDSAP.',N'Difficoltosa interfaccia utente, difficioltà nell''effettuare ricerche, contenuto non rispondente ai requisiti richiesti dal MDSAP.',NULL,N'QARA
QM
TSS',N'Assistenza strumenti interni
Regolatorio
Sistema qualità',N'Impatto positivo, le modifiche apportate al software renderanno il sw più facilmente utilizzabile dagli utenti, permetterà di effettuare ricerche efficaci e di scaricare facilmente i dati desiserati per analisi statistiche, permetterà di inserire tutti i dati richiesti dalla linea guida MDSAP per rispondere ai regolamenti EXTRA UE.
Impatto positivo, le modifiche apportate al software renderanno il sw più facilmente utilizzabile dagli utenti, permetterà di effettuare ricerche efficaci e di scaricare facilmente i dati desiserati per analisi statistiche, permetterà di inserire tutti i dati richiesti dalla linea guida MDSAP per rispondere ai regolamenti EXTRA UE.
Impatto positivo, le modifiche apportate al software renderanno il sw più facilmente utilizzabile dagli utenti, permetterà di effettuare ricerche efficaci e di scaricare facilmente i dati desiserati per analisi statistiche, permetterà di inserire tutti i dati richiesti dalla linea guida MDSAP per rispondere ai regolamenti EXTRA UE.
Impatto',N'22/01/2019
22/01/2019
22/01/2019
22/01/2019
23/01/2019
23/01/2019',N'Individuate le modifiche da apportare al sw (ALLEGATO A), approvata l''oefferta con il fornitore Linkomm (ALLEGATO B) gestire all''attuazione delle modifiche con il fornitore ed effettuare il debug. 
Validazione  della versione 2.0
Messa in uso nuovo sw Linkomm versione 2.0 (sia per la parte di gestione SGQ che strumenti)
Approvazione documentazione di validazione sw.
Individuate le modifiche da apportare al sw (ALLEGATO A), approvata l''oefferta con il fornitore Linkomm (ALLEGATO B) gestire all''attuazione delle modifiche con il fornitore ed effettuare il debug. 
Validazione  della versione 2.0',N'ALLEGATO A1 elenco modifiche sw - allegato_a1_2018-11-30_linkomm_2.0.modifiche_sgq.xlsx
ALLEGATO A2 elenco menù a tendina con selezione multipla - allegato_a2_selezione_multipla.xlsx
ALLEGATO A3 elenco menù a tendina VMP - allegato_a3_caselle_combinate-selezione_multipla_vmp.xlsx
ALLEGATO A4 visualizzazione modifiche sul sw e sul modulo di CC - allegato_a4_cc.pdf
ALLEGATO A5 visualizzazione modifiche sul sw e sul modulo di RAC/RAP - allegato_a5_rac.pdf
ALLEGATO A6 visualizzazione modifiche sul sw e sul modulo di R - allegato_a6_r_1.pdf
ALLEGATO A7 visualizzazione modifiche sul sw e sul modulo di NC - allegato_a7_nc.pdf
ALLEGATO A8 visualizzazione modifiche sul sw e sul modulo di Richiesta assistenza - allegato_a8_ric_ass.pdf
ALLEGATO B OFFERTA LINKOMM - allegato_b_2018-12-16_offerta_elitech_v1f.docx
ALLEGATO B OFFERTA LINKOM_MACROAREE - allegato_b_elitech_macro_14_12_allegato_a.xlsx
Aggiornamento funzioni aziendali versione 2.0 - allegato9_aggiornamentomenuatendina_funzioni_25-07-2019.xlsx
Debug terminato_esportazione dati - cc19-4_debugterminato_esportazione_dati.msg
Aggiornamento Area Imapatto / funzoini aziendali versoine 2.0 - allegato10_aggiornamentomenuatendina_areeimpatto-funzioni_25-07-2019.xlsx
MOD05,23 piano - mod05,23_00_piano_validazione_sw31_2.0.pdf
MOD05,24 rapporto - mod05,24_00_rapporto_validazione_sw31_2.0_1.pdf
Linkom versione 2.0 in uso - cc19-4_messa_in_uso_versione_2.0.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-4_mod05,36_non_significativa.pdf
ALLEGATO C elenco modifiche parte relativa alla gestione strumenti - allegato_c_2018-11-19_linkomm_2.1_modifiche_strumenti.xlsx
MOD05,23 - mod05,23_00_piano_validazione_apparecchiature_sw31.pdf
MOD05,24 - mod05,24_00_rapporto_validazione_apparecchiature_sw31.pdf',N'QMA
QM
QMA
QMA
QARA
TSS
TSS',N'23/01/2019
23/01/2019
23/01/2019
23/01/2019
23/01/2019
23/01/2019',N'29/03/2019
31/05/2019
31/05/2019
29/03/2019
31/05/2019',N'25/07/2019
07/08/2019
25/07/2019
04/09/2019
04/09/2019
04/09/2019',N'No',N'Nessun DMRI è impattato dalla modifica',N'No',N'Non è neccessario alcun tipo di comunicazione al cliente.',N'No',N'Non c''è alcun impatto sui prodotti immessi in coimmercio in quanto è una modifica di sistema.',N'SW - Elenco Software',N'SW31 - linkomm (EGSpA) (archvio Non Conformità, Reclami, Azioni Correttive/Azioni Preventive, Change Control, archivio anagrafiche strumenti e date degli interventi di manutenzione e taratura)',N'SW31',N'No',N'La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti. ',N'No',N'La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso Virtual Manufacturer. ',N'No',N'Non c''è alcun impatto sull''analisi dei rischi di prodotti in quanto è una modifica di sistema.',N'Roberta  Paviolo','2019-05-31 00:00:00',NULL,'2019-09-04 00:00:00',N'No',NULL,NULL),
    (N'19-3','2019-01-22 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Modifica della PR10,02 per la parte relativa alla comunicazione dell''esito CQ. In particolare si chiede di includere nella scansione dell''esito presente nel MOD10,15 "Scheda Controllo Qualità" anche il MOD10,12" Verifica Tecnica" dove sono presenti i dati ed il superamento dei criteri di CQ.',N'La modifica permette di avere disponibili le scansioni dei dati di CQ richiesti per le registrazioni. In questo modo RAS può recuperare l''esito del CQ nella cartella senza dover eseguire una scansione. L''attività per QCT non aumenta in quanto esegue già la scansione del MOD10,15. ',N'Il processo di registrazioni e richiesta di documenti di CQ è sempre più frequente e richiede che venga eseguita questa attività, la mancata disponibilità delle scansioni già pronte causa perdita di tempo a QCT e RAS in modo poco pianificabile (soprattutto per QCT) e queste attività potrebbero paradossalmente essere eseguite più volte per alcuni lotti. ',NULL,N'QARA
QC
QM',N'Controllo qualità
Regolatorio
Sistema qualità',N'La richiesta prevede la modifica delle modalità di invio dell''esito, pertanto queste vanno integrate nelle attività. La problematica potrebbe essere collegata ai casi di OOS di CQ per i quali generalmente non si esegue la VT in quanto i replicati sono maggiori. E'' da valutare però generelamente questi CQ non sono inviati.
La modifica permette l''autonomia di RAS nella gestione delle richieste di registrazione per quanto riguarda i documetni di CQ. Nei casi di OOS di CQ può comunque essere necessario il supporto per DI QCT o QC per identifiacre i lotti da inviare.
La modifica non impatta il processo ma sono necessarie attività di messa in uso del documento',N'22/01/2019
22/01/2019
22/01/2019',N'Modifica della PR10,02
Formazione (incluedendo RAS)
formazione PR10,02
messa in uso PR10,02',N'PR10,02 rev06, MOD18,02 del 30/01/2018 - cc19-3_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2.msg
MOD18,02 del 30/01/2019 - cc19-3_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_1.msg
PR10,02 rev06 in uso al 14/02/2019 - cc19-3_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_2.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-3_mod05,36_non_significativa.pdf',N'QC
QMA
QMA',N'22/01/2019
22/01/2019
22/01/2019',NULL,N'15/02/2019
15/02/2019
15/02/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'CQ - Controllo Qualità prodotti',N'CQ16 - Comunicazione dell''esito del CQ',N'CQ16',N'No',N'-',N'No',N'-',N'No',N'-',N'Federica Farinazzo',NULL,NULL,'2019-02-15 00:00:00',N'No',NULL,NULL),
    (N'19-2','2019-01-07 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede la modifica del layout del modulo richiesta cq MOD10,01, al fine di agevolare l''attività di compilazione di PP in fase di invio. Infatti PP deve compilare sia la parte relativa al database di programmazione che la parte relativa alla mail preimpostata per la richiesta. La modifica permetterebbe di scrivere una sola volta nel database e incollare i dati nel MOD10,01 prima dell''invio via mail',N'Diminuirebbe la possibilità di errori di strascrizione e renderebbe più veloce la fase di preprazione della richiesta',N'PP continuerebbe adi impiegare più tempo nella preparazione delle richieste di CQ con una maggiore probabilità di errore nella trascrizione dei dati',NULL,N'MM
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'La modifica richiesta permetterebbe di migliorare e agevoalre la fase del  processo di richiesta di CQ
nessun impatto per il processo di CQ le informazioni fornite da PP rimangono le stesse così come la conferma di ricevimetno da parte di QCT
nessun impatto per il processo ci sono delle attività a carico',N'18/01/2019
07/01/2019
07/01/2019',N'Verifica e revisione del modulo.
Modifica del modulo MOD10,01 (la bozza è disponibile in G:DOCSGQBOZZEDocSpecialistDraft3-CCIN_CORSOCC19-02_MO10,01-Richiesta_CQ)
formazione a PP/PT/PTC/QCT
messa in uso del nuovo modulo',N'MOD10,01 rev03, MOD18,02 del 30/1/2019 - cc19-2_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2.msg
MOD10,01 rev03 in uso al 14/2/19 - cc19-2_messa_in_uso_pr1002_06_rac18-27_cc19-3_mod1001_03_cc19-2_1.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-2_mod05,36_non_significativa.pdf
DARFT MOD10,01 - cc19-2_draft_mod10,01_richiesta_cq_rev03.oft
simulazione MOD10,01 REV03 - cc19-2_simulazione_mod10,01_rev03.pdf',N'DS
QC
QMA
PP',N'18/01/2019
07/01/2019
07/01/2019',N'31/01/2019',N'28/01/2019
15/02/2019
15/02/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'CQ - Controllo Qualità prodotti',N'CQ4 - Pianificazione CQ',N'CQ4',N'No',N'-',N'No',N'-',N'No',N'-',N'Federica Farinazzo','2019-01-31 00:00:00',NULL,'2019-02-15 00:00:00',N'No',NULL,NULL),
    (N'19-1','2019-01-03 00:00:00',N'Salvatore Patanè',N'Documentazione SGQ',N'Modifica del modulo di consenso informato troppo restrittivo poichè legato ai soli kit di diagnosi di malattie infettive e alle sole matrici prelevabili autonomamente (urine, feci, sputo). E'' necessario rendere il documento più generico sull''uso dei kit associati e inserire la possibilità di prelievo con personale specializzato (es. sangue periferico). Da aggiornare inoltre la direttiva Privacy.',N'Utlizzabilità del modulo per diverse applicazioni ',N'Non potremmo collezionare sangue fresco per i test di estrazione e amplficazione',NULL,N'PVS
QARA
SA-R&D
T&CM',N'Gare
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Nessun impatto se non in termini di ore lavoro.
Impatto positivo in quanto il documento può essere utilizzato per diverse applicazioni.
Impatto positivo in quanto il documento può essere utilizzato per diverse applicazioni.
Impatto positivo in quanto il documento può essere utilizzato per diverse applicazioni.
Impatto positivo in quanto il documento può essere utilizzato per diverse applicazioni.',N'18/01/2019
18/01/2019
18/01/2019
18/01/2019
18/01/2019',N'Messa in uso della nuova versione del documento.
Revisione del modulo.
Revisione del modulo.
Impatto positivo in quanto il documento può essere utilizzato per diverse applicazioni.
Revisione del modulo.',N'MOD04,24 rev01 in uso al 5/2/2019 - cc19-1_messa_in_uso_mod0424_formulario_consenso_informato.msg
 La modifica non è significativa, pertanto non è neccessario alcun tipo di notifica presso le autorità competenti.  - cc19-1_mod05,36_non_significativa.pdf',N'T&CM
QM
PVS
RDM
SA-R&D
T&CM
T&CM',N'18/01/2019
18/01/2019
18/01/2019
18/01/2019',NULL,N'05/02/2019
05/02/2019
05/02/2019
05/02/2019',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',NULL,NULL,NULL,'2019-02-05 00:00:00',N'No',NULL,NULL),
    (N'18-52','2018-12-27 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento layout sito web e modalità di caricamento IFU.',N'Sito web differente.',N'Documentazione SGQ obsoleta',NULL,N'QARA',N'Regolatorio',N'Impatto in termini di carico ore lavoro.
Impatto in termini di carico ore lavoro.',N'27/12/2018
27/12/2018',N'Aggiornamento IO04,03 rev00
verifica ed aggiornamento risk assessment e VMP',N'IO04,03_rev01 in uso al 31/01/19 - io04,03_01_gestione_elettronica_delle_ifu.pdf',N'RAS
QAS',N'27/12/2018
27/12/2018',NULL,N'31/01/2019
31/01/2019',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'IFU - Gestione Manuali
SW - Elenco Software',N'IFU8 - upload manuali sul web site
SW30 - elitechgroup IFU (Sito web in cui caricare i manuali di istruzioni per l''uso)',N'IFU8 e SW30 verificare se le modifiche hanno impatto sul risk assessment / sUlla validazione',N'No',N'non necessaria, vedere MOD05,36',N'No',N'nessun virtual manufacturer coinvolto',N'No',N'nessu intervento',N'Michela Boi',NULL,NULL,'2019-01-31 00:00:00',N'No',NULL,NULL),
    (N'18-51','2018-12-21 00:00:00',N'Roberta  Paviolo',N'Documentazione SGQ',N'Introduzione della nuova veriosne del sw gestionale: Dynamic NAV2018 versione 11.0.21836.0 ',N'Maggiore automazione nei controlli di processo',N'mantenimento di una versione obsoleta del sw',NULL,N'BC
MM
PW
QARA
QM
ITM',N'Acquisti
Ordini
Produzione
Sistema informatico
Sistema qualità',N'Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei controlli di processo
Passaggio ad una versione di sw aggiornata che rende possibile una maggiore automazione nei cont',N'21/12/2018
21/12/2018
21/12/2018
21/12/2018',N'Gestione delle attività di verifica e validazione con i vari reparti coinvolti (secondo il risk assessment inziale)
La nuova veriosne del sw gestionale: Dynamic NAV2018 versione 11.0.21836.0 include anche la nuova versione del sw BarTender 2016R8 versione 11.0.8.3153, l''archiviazione dell''eseguibile LOTTI di EGSpA, utilizzato per l''assegnazione dei lotti, l''eseguibile LBLMNGR di EGSpA, utilizzato per la compilazione delle etichette.
Aggiornare IO06,02 rev00 "RDA"
creazione dei template di etichette non presenti sul gestionale NAV ed inserimento del codice a barre per la gestione con il modulo warehouse:
910-BANG07-02
910-BANG08-02
910-BANG12-02
910-BANG13-02
910-BANG14-02
910-BANG15-02
910-BANG16-02
910-BRK200-02
910-BRK200-05
910-EPH03
910-ER.140
910-EXTB01
910-EXTB02
910-EXTD02
910-EXTR01
910-INT02100100
Aggiornamento elenco SW (MOD05,07) 
Aggiornamento VMP e risk assessment
Aggiornamento IO04,06 rev03 con gli screenshot per la creazionedell''anagraficia etichette dal nuovo NAV.
',N'RA P1.6 firmato in data 12/05/2020 - mod09,27_01_modulo_di_risk_assessment_p1,6_etichettaturasemilavoraticomponenti_cc18-51_rap19-11_rac19-20_.pdf
IO06,02_00 - cc18-51_messa_in_uso_procedura_e_istruzione_integrazione_rda_su_nav_pr0602_io0602_mod1802_12719.msg
All-1_IO04,06_template_etichette - cc18-51_messa_in_uso_all-1_io0406_template_etichette_in_uso_cc20-24.msg
IO09,17_03, FORMAZIONE DEL 20/04/2020 - cc18-51_messa_in_uso_io0917_03_cc18-51_mod1802_del_20-04-2020.msg
IO09,18_01 - cc18-51_messa_in_uso_i0918_01_mod1802_19052020.msg
IO09,14_06 - cc18-51_messa_in_uso_io0906_01_io0914_06_cc18-51_cc20-19_mod1802_del_16042020.msg
IO09,03_01 - cc18-51_messa_in_uso_io0903_01.msg
piano validazione - mod09,24_00_pianodivalidazionestampaetichette_leggibili_da_swingenius_19-12-19.pdf
report validazione - mod09,12_01_rapportovalidazionestampaetichette_leggibili_da_swingenius-26-02-2020.pdf
tutte le etichette sono state approvate e salvate al seguente indirizzo G:DOCSGQIN USOETICHETTE PRODOTTI - cc18-51_mod04,13_approvati.txt',N'ITM
ITM
QAS
QAS
SAD-OP
PW
ITM
QM
MM
ITT
QARA
QM
QAS',NULL,NULL,N'12/09/2019
07/01/2020
12/05/2020
05/08/2020
26/02/2020',N'No',N'nesun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
SW - Elenco Software
SW - Elenco Software
SW - Elenco Software
SW - Elenco Software
PR - Preparazione Lotto Unico',N'P1.6 - Etichettatura semilavorati e componenti
SW1 - NAV (sistema gestionale)
SW2 - Eseguibile LOTTI (EGSpA)
SW3 - Bartender Edition Automation (gestione stampa etichette)
SW15 - Eseguibile LBLMNGR (EGSpA) (stampa etichette)
PR6 - etichettatura prodotti',N'Aggiornamento dei risk assessment di NAV e Bartender, rispettivamente SW1 EeSW3. archiviazione dei risk assessment SW15, SW2, SW3. Aggiornamneto P1.6 e PR6. Srtabilire se creare RA per SW43 n"generatore lotti 2" usato solo per generare lotti per uso cq e r&d prototipi.
ARCHIVIATI RA SW2 e SW15
',N'No',N'-',N'No',N'-',N'No',N'nessun intervento',NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'18-50','2018-12-14 00:00:00',N'Cinzia Bittoto',N'Manuale di istruzioni per l''uso',N'Durante l''estensione d''uso di Enterovirus ELITe MGB KIT, codice RTS076PLD, sulla matrice BAL sullo strumento InGenius, si chiede di integrare sul manuale di istruzioni per l''uso anche i dati  (LoD e linearità) relativi  alle matrici plasma e CSF come da PRT2018-044.',N'Completare i dati relativi alle matrici plasma e CSF.',N'Il cliente non dispone dei dati effettivi di LoD e linearità sul IFU.',NULL,N'PVS
QARA
RDM',N'Regolatorio
Ricerca e sviluppo
Validazioni',N'Completare i dati di Enterovirus ELITe MGB KIT sulle matrici plasma e CSF, durante il progetto di estensione d''uso su BAL.
Ad 04/2019 si è valutato di integrare i test riguardo le stabilità on-board di STD e CTR di Enterovirus per verificare il loro utilizzo con lo strumento InGenis.
Completare i dati di Enterovirus ELITe MGB KIT sulle matrici plasma e CSF, durante il progetto di estensione d''uso su BAL.
Ad 04/2019 si è valutato di integrare i test riguardo le stabilità on-board di STD e CTR di Enterovirus per verificare il loro utilizzo con lo strumento InGenis.
Completare i dati di Enterovirus ELITe MGB KIT sulle matrici plasma e CSF, durante il progetto di estensione d''uso su BAL.
Ad 04/2019 si è valutato di integrare i test riguardo le stabilità on-board di STD e CTR di Enterovirus per verificare il loro utilizzo con lo strumento InGenis.
Completare i dati di Enterovirus ELITe MGB KIT sulle matrici plasma e CSF, durante il progetto di estensione d''uso su BAL.
Ad 04/2019 si è valutato di i',N'14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018',N'Esecuzione test come da PRT2018-044 e stesura Report.
Modifica IFU e Assay protocol
Aggiornamento TAB
Formazione reparto assistenza applicativa.
04/2019: stesura PRT2019-008 "VERIFICATION PROTOCOL PCR ANALYTICAL PERFORMANCE ENTEROVIRUS ELITe MGB KIT, RTS076PLD ELITe InGenius®, INT030 CC18-50"
Verifica report e IFU
Messa in uso IFU con estensione d''uso BAL e LoD e linearità plasma e CSF.
Aggiornamento FTP
Verifica significatività della modifica e notifica presso i paesi extra UE.',NULL,N'PVS
PVS
PVS
RT
QMA
QMA
QMA
PVS
PVS',N'14/12/2018
14/12/2018
14/12/2018
14/12/2018
16/04/2019
14/12/2018
14/12/2018
14/12/2018
14/12/2018',N'18/12/2019
18/12/2019
18/12/2019
18/12/2019
18/12/2019
18/12/2019
18/12/2019',NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Cinzia Bittoto','2019-12-18 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'18-49','2018-12-14 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Aggiornamento del SW SGAT alla nuova piattaforma denominata SGAT Evo per aggiornamento tecnologico ed interfacciamento con il sistema ERP aziendale (NAV).',N'Aggiornamento tecnologico: compatibilità del sistema con tutti i browser internet disponibili, compatibilità del sistema con dispositivi tablet.
Interfacciamento con NAV: importazione automatica sul sistema SGAT di anagrafica clienti/siti e prodotti.',N'mancato aggiornamento tecnologico e mancata importazione automatica da NAV di anagrafica clienti/siti e prodotti.',NULL,N'CSC
GSC
QARA',N'Assistenza applicativa
Assistenza strumenti esterni
Regolatorio
Sistema qualità',N'Impatto positivo dovuto all''aggiornamento tecnologico della piattaforma e al collegamento con il sistema ERP di gruppo (Navision). Inoltre la nuova piattaforma permetterà una migliore gestione, estrazione ed analisi dei principali Service KPI (come ad esempio MTBF = mean time between failure, Work Unit, Faults Analysis)
Impatto positivo dovuto all''aggiornamento tecnologico della piattaforma e al collegamento con il sistema ERP di gruppo (Navision). Inoltre la nuova piattaforma permetterà una migliore gestione, estrazione ed analisi dei principali Service KPI (come ad esempio MTBF = mean time between failure, Work Unit, Faults Analysis)
impatto positivo dovuto all''aggiornamento tecnologico della piattaforma e al collegamento con NAV.
L''aggiornamento tecnologico della piattaforma ed, in particolare, il collegamento con NAV permette di effettuare più agevolmente ricerche per codice prodotto.
L''aggiornamento tecnologico della piattaforma ed, in particolare, il collegamento con NAV permette di effettuar',N'14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018',N'Valutazione dell''analisi del rischio in relazione alle nuove funzionalità di SGAT Evo (SGAT EVO ELITECH - V 02.00.00; verifica e validazione del sw (da parte del fornitore Volos) secondo i requisiti richiesti e descritti nell''allegato "ElitechGroup Global service software solution requirements". Per ogni versione di SW rilasciata il fornitore Volos rilascia la seguente documentazione: Functional Specifications and Impact Analysis, Test Report, Release Notes, User Guide (se necessita di modifica)  e Administrator USER GUIDE (se necessita di modifica). Verifica dell''aggiornamento del VMP.
formazione al personale utilizzatore e messa in uso
Nessuna attività a carico.
Aggiornamento dell''elenco sw e raccolta della documentazione risk assessment e verifica e validazione.
Nessuna attività a carico.',N' Requisiti richiesti per SGAT e SGAT EVO  - cc18-49_allegato_elitechgroup_service_software_requirements_.pdf
 Stato dell''implemtazine dei requisiti iniziaili richiesti da EGSpA a VOLOS nelle varie versioni di SGAT. Ciascun requisito iniziale presente nel documento "ELITechGroup Service Software Requirements" è associato alla versione di SGAT in cui è stato implementato  - cc18-49_general-004-r_190701_-_rev._a_sgat_evo_original_sw_requirements_check.pdf
formazione utilizzatori (MOD18,02) - cc18-49_formazione_sgat_evo_14_e_15-02-2019_mod18,02.docx
Risk assessment SGAT EVO 2.00.00, MOD05,08 aggiornato - cc18-49_mod09,27_01_sw32_sgat_evo_2.00.00_08012019.pdf
SW32 è un sw commerciale, verificato e validato dal fornitore qualificato VOLOS, come da documentazione allegata al RISK assessment del 14/12/2018. Il VMP necessita di aggiornamento per l''inserimento del CC18-49, non è stato ancora firmato in attesa del completamento delle validazioni a carico di atri sw - cc18-49_draft_vmp_aggiornato.xlsx',N'GSC
CSC
GSC
QMA
QARA',N'14/12/2018
14/12/2018
14/12/2018
14/12/2018
14/12/2018',NULL,N'10/07/2019
15/02/2019
14/01/2018
10/07/2019
14/12/2018',N'No',N'nessun impatto sul DMRI',N'No',N'Non necessaria alcuna comunicazione ai clienti in quanto sw ad uso interno/consociate',N'No',N'nessun impatto',N'SW - Elenco Software',N'SW32 - sgat (Software globale per la gestione delle richieste di assistenza tecnica sugli strumenti )',N'SW32',N'No',N'non necessaria alcuna notifica in quanto sw interno',N'No',N'nessunvirtual manufacturer coinvolto',N'No',N'nessun impatto ',N'Marcello Pedrazzini',NULL,NULL,'2019-07-10 00:00:00',N'No',NULL,NULL),
    (N'18-48','2018-12-14 00:00:00',N'Anna Capizzi',N'Documentazione SGQ',N'Aggiornamento della specifica SPEC950-134 e del MOD955-RNA-MS2-8 per allinearli alle attuali definizioni di scadenza ed utilizzabilità ed ai risultati ottenuti dai controlli di stabilità del prodotto CTRCPE che contiene la materia prima.',N'Allineamento alle attuali regole di assegnazione delle scadenze al prodotto.',N'Questo processo di produzione rimarrebbe al di fuori delle regole di assegnazione delle scadenze creando ambiguità nella definizione della durata del prodotto.',NULL,N'MM
QARA
QC
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Allineamento del processo di produzione del CTRCPE alle attuali regole di assegnazione delle scadenze. Eliminazione del possibile rischio di assegnazione della scadenza oltre la durata della stabilità testata.
Nessun impatto se non in termini di ore lavoro.
Allineamento del processo di produzione del CTRCPE alle attuali regole di assegnazione delle scadenze. Eliminazione del possibile rischio di assegnazione della scadenza oltre la durata della stabilità testata.
Nessun impatto se non in termini di ore lavoro.',N'02/01/2019
02/01/2019
19/02/2019
02/01/2019',N'Nessuna attività a carico.
Aggiornamento della specifica SPEC950-134 e formazione.
Aggiornamento del MOD955-RNA-MS2-8 e formazione.
Messa in uso documenti.',N'spec950-134_rev03, MOD18,02 del15/02/19 - cc18-48_messa_in_uso_specifiche_cc18-48_cc51-15.msg
MOD955-RNAMS2-8 rev01, mod18,02 20/2/2019  - cc18-48_messa_in_uso_mod955-rnams2-8_01_cc18-48_mod1802_del20219.msg
SPEC950-134 rev03 - cc18-48_messa_in_uso_specifiche_cc18-48_cc51-15_1.msg
MOD955-RNAMS2-8 rev01 - cc18-48_messa_in_uso_mod955-rnams2-8_01_cc18-48_mod1802_del20219_1.msg',N'MM
RT
DS
QMA',N'02/01/2019
02/01/2019
19/02/2019
02/01/2019',N'20/02/2019
29/03/2019',N'02/01/2019
19/02/2019
18/03/2019
18/03/2019',N'No',NULL,N'No',NULL,N'No',NULL,N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',NULL,N'P1.5 Assegnazione scadenza',N'No',NULL,N'No',NULL,N'No',NULL,NULL,'2019-03-29 00:00:00',NULL,'2019-03-18 00:00:00',N'No',NULL,NULL),
    (N'18-47','2018-12-03 00:00:00',N'Michela Boi',N'Etichette',N'Updating the RUO labeled ELITe Ingenius instrument units (ref. INT030-US) sold so far to EGI (ELITechGroup Inc.) with an IVD labeling (ref. INT030-K).',N'After the FDA clearance obtained by the HSV/ELITe InGenius system on October 29th 2018, ELITe InGenius can be placed on the US market with IVD labeling, by using the specific ref. INT030-K.
An ELITe InGenius unit released on the US market before the FDA clearance with RUO label can be re-labeled as INT030-K (IVD) IF the unit is equivalent in term of software and hardware (performances, user interface, usability) with the unit used in the clinical studies for HSV1/2 and presented into the 510k dossier. After that update, the re-labeles units can be used in association with HSV1/2 assay for providing IVD data.',N'Several units, equivalent to the instrument cleared by FDA, could not be used for IVD purposes.',NULL,N'GSC
QARA',N'Assistenza applicativa
Assistenza strumenti esterni
Regolatorio',N'The re-labeled units have to be included into EGSpA and EGI vigilance system since they are possible source of adverse events according to FDA regulation
The re-labeled units have to be included into EGSpA and EGI vigilance system since they are possible source of adverse events according to FDA regulation
The re-labeled units have to be included into EGSpA and EGI vigilance system since they are possible source of adverse events according to FDA regulation
The re-labeled units have to be included into EGSpA and EGI vigilance system since they are possible source of adverse events according to FDA regulation
The re-labeled units have to be included into EGSpA and EGI vigilance system since they are possible source of adverse events according to FDA regulation
The re-labeled instruments may be improperly used. Customer support team have to inform and train customer on IVD functionalities of re-labeled INT030-K units.
No impact since the servicing procedure for managing IVD units are the same of RUO',N'07/12/2018
07/12/2018',N'Equivalence assessment of RUO and IVD units. 
A PLAN and a REPORT will be prepared in order to establish the equivalence of the units sold in US so far with ref. INT030-US and the units used for clinical studies for >HSV1/2 assay and cleared by FDA.The equivalence will be established in terms of software and hardware features (performances, usability and user interface).
This assessment allows to identify units that could be re-labeled with ref. INT030-K.
Plan of re-labeling.
A checklist reporting all the actions to be implemented for the re-labeling of each single unit will be prepared.
This document allows to demonstrate that for each equivalent unit a series of actions and verifications are performed before the re-labeling
Production of new labels.
EGSpA has to require to PSS the production of new labels for the equivalent units. The new labels have to indicate the original serial number of the instrument but the new code for IVD instrument, INT03-K.
Training of EGI personnel about the update of',NULL,N'GSC
QARA
QMA
QARA
QMA
QARA
QMA
QARA
QMA
QARA
QMA
GSC',N'07/12/2018
07/12/2018
07/12/2018
07/12/2018
07/12/2018
07/12/2018',N'21/12/2018
21/12/2018
18/01/2019
31/01/2019
15/02/2019',NULL,N'No',N'-',N'Si',N'Official release on the market of the units with new IVD labels.',N'Si',N'Updating the RUO labeled ELITe Ingenius instrument units (ref. INT030-US) sold so far to EGI (ELITechGroup Inc.) with an IVD labeling (ref. INT030-K).',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Michela Boi',NULL,NULL,'2020-10-22 00:00:00',N'Si',N'22/10/2020, la ri-etichettatura non risulta essere necessaria, gli strumenti installati in USA rimarranno RUO.','2020-10-22 00:00:00'),
    (N'18-46','2018-11-30 00:00:00',N'Renata  Catalano',N'Documentazione SGQ',N'Il report Dekra 2232208-TDR11-R0 relativo alla revisione della documentazione tecnica per la marcatura CE dei prodotti «STI PLUS ELITe MGB® Kit» e «STI PLUS - ELITe Positive Control» riporta la seguente domanda TDR11/Q01 "Please explain in what document(s) the risks associated with biological hazards is covered, because Essential Requirement B 2.2 is stated as applicable for the STI PLUS ELITe MGB® Kit (REF RTS400ING) and STI PLUS - ELITe Positive Control (REF CTR400ING)." Per rispondere a tale quesito è necessario aggiornare il modello del modulo MOD22,01 "Product General Description" introducendo una sezione in cui riportare le sostanze di origine biologica contenute nel prodotto e valutare il rischio biologico. Tale modello verrà utilizzato per produrre la Revisione 01 dei moduli MOD22,01 relativi ai prodotti «STI PLUS ELITe MGB® Kit» e «STI PLUS - ELITe Positive Control».',N'Il cambiamento consente di dare maggiore evidenza documentale a quanto riportato nel requisito B 2.2 dei Requisiti Essenziali e di rispondere adeguatamente alla questione TDR11/Q01 posta da Dekra.',N'Mancata certificazione CE dei prodotti  «STI PLUS ELITe MGB® Kit» e «STI PLUS - ELITe Positive Control» da parte di Dekra',NULL,N'PMS
QARA
RDM',N'Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Una volta in uso il nuovo modello MOD22,01, in fase di stesura della documentazione di gestione dei rischi occorrerà riportare le sostanze di origine biologica incorporate nel prodotto ed effettuare una valutazione del rischio biologico
Una volta in uso il nuovo modello MOD22,01, in fase di stesura della documentazione di gestione dei rischi occorrerà riportare le sostanze di origine biologica incorporate nel prodotto ed effettuare una valutazione del rischio biologico
Una volta in uso il nuovo modello MOD22,01, in fase di stesura della documentazione di gestione dei rischi occorrerà collaborare con SGQ per riportare le sostanze di origine biologica incorporate nel prodotto ed effettuare una valutazione del rischio biologico.
Una volta in uso il nuovo modello MOD22,01, in fase di stesura della documentazione di gestione dei rischi occorrerà riportare le sostanze di origine biologica incorporate nel prodotto ed effettuare una valutazione del rischio biologico.',N'03/12/2018
03/12/2018
03/12/2018
03/12/2018',N'Creare nuovo modello del MOD22,01
Aggiornare i moduli MOD22,01 relativi ai prodotti «STI PLUS ELITe MGB® Kit» e «STI PLUS - ELITe Positive Control».
Nessuna attività a carico.
Inviare a DEKRA l''aggiornamento della documentazione di STI PLUS.',N'MOD22,01_03 - cc18-46_messa_in_uso_mod2201_rev03_cc18-46.msg
mod22,01_01_RTS400ING - cc18-46_mod22,01_rev.01_product_general_description_rts400ing.pdf
mod22,01_01_CTR400ING - cc18-46_mod22,01_rev.01_product_general_description_ctr400ing.pdf
invio aggiornamento documentazione a dekra - cc18-46_invio_aggiornamento_documentazione_dekra.msg',N'QMA
QMA
RDM
QARA',N'03/12/2018
03/12/2018
03/12/2018
03/12/2018',N'07/12/2018
07/12/2018
07/12/2018
07/12/2018',N'05/12/2018
05/12/2018
05/12/2018
05/12/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,N'R&D1.3 ',N'No',NULL,N'No',NULL,N'No',NULL,N'Renata  Catalano','2018-12-07 00:00:00',NULL,'2018-12-05 00:00:00',N'No',NULL,NULL),
    (N'18-45','2018-11-08 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Aggiornamento Schede Dati Sicurezza n. 79, 81 e 82 (relative al Binding Buffer, Washing Buffer 1 e Washing Buffer 2, componenti del kit di Estrazione per l''ELITe InGenius) con l''indicazione delle temperature di Flushing point e Initial boiling point.',N'L''indicazione di questi dati è necessaria per la spedizione del prodotto via nave ',N'Il prodotto non può essere spedito via nave',NULL,N'PW
QARA',N'Regolatorio',N'Possibilità di spedire il prodotto via nave.',N'09/11/2018',N'Aggiornamento e messa in uso delle Schede Dati Sicurezza n.79, 81 e 82 per l''inserimento dei dati relative alle temperature di Flushing point e Initial boiling point.',N'messa in usoSDS nà 79, 81, 82 - cc18-45_revisione_sds_ingenius_cc18-45.msg',N'QMA',N'09/11/2018',N'30/11/2018',N'16/11/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,'2018-11-30 00:00:00',NULL,'2018-11-16 00:00:00',N'No',NULL,NULL),
    (N'18-44','2018-11-07 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il fornitore OEM Fast Track ha implementato tre Change Control relativi a tutti I  prodotti forniti a ELITech:
- CCR2018-115: blank labels updated in order to harmonize manufacturing expiration dates format. (allegato 1)
- CCR2018-136: changes to the technical files documentation as per request of the Notify Body: new labels with updated IFU symbol (allegato 2)
- CCR2018-143: changes to the technical files documentation as per request of the Notify Body: new labels with updated IFU symbol ancd new logo of manufacturer (allegato 3).
E'' necessario valutare se tale cambiamento ha impatto sui prodotti ELITEch: Meningitis Viral ELITe MGB Kit (RTS507ING), Meningitis Viral 2 ELITe MGB MGB® Panel (RTS523ING), STI ELITe MGB Panel (RTS533ING), Respiratory Viral ELITe MGB Panel (RTS548ING), Respiratory Bacterial (RTS553ING).',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. La valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima fornitura/produzione tutte le modifiche che si rendessero necessarie alla documentazione di ELITech.',N'La modifica alle etichette delle bulk, se non riportata anche sulla documentazione di ELITEch, potrebbe generare confusione dovuta alla incongruenza con quanto indicato sulle specifiche al ricevimento, ed eventualmente sulla documentazione di produzione/CQ.',NULL,N'SPM
MM
PW
QM',N'Acquisti
Controllo qualità
Magazzino
Produzione
Ricerca e sviluppo',N'Nessun impatto se non in termini di carico ore/lavoro.
eventuale modifica documenti di manufacturing
La modifica è apportatata dal fornitore FTD, è perciò necessario valutare se ha impatto sulla documentazione EGSpA.
Nessun impatto se non in termini di carico ore/lavoro.
Nessun impatto se non in termini di carico ore/lavoro.',N'08/11/2018
08/11/2018
08/11/2018',N'Modifica delle specifiche a seguito di confronto (etichetta / specifica in uso) da parte del personale del magazzino.
eventuale modifica documenti di manufacturing
Confrontare, con il primo arrivo di materia prima, la nuova etichetta di bulk con i requisiti indicati nella specifica e fornire indicazione a RT per l''aggiornamento o meno della specifica.
Eventuale modifica della documentazione di produzione a seguito di confronto (etichetta / specifica in uso) da parte del personale del magazzino.
Eventuale formazione sulla modifica della documentazione di produzione a seguito di confronto (etichetta / specifica in uso) da parte del personale del magazzino.',N'allegato 1 - ccr_2018-115_closed_on_05.10.2018.pdf
allegato 2 - ccr_2018-136_post_cab.pdf
allegato 3 - ccr_2018-143_post-cab.pdf
 Le modifiche apportate alle etichette non hanno impatto sulla documentazione di EGSpA. nel caso specifico l''etichetta ha un nuovo logo che non influisce con i dati riportati nella specifica. non è necessario apportare alcun tipo di cambiamento.  - cc18-44_nuova_etichetta_1.jpg
Le modifiche apportate alle etichette non hanno impatto sulla documentazione di EGSpA. nel caso specifico l''etichetta ha un nuovo logo che non influisce con i dati riportati nella specifica. non è necessario apportare alcun tipo di cambiamento. - cc18-44_nuova_etichetta.jpg',N'RT
DS
PW
QC
MM',N'08/11/2018
08/11/2018
11/12/2018',NULL,N'11/12/2018
11/12/2018
11/12/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,'2018-12-11 00:00:00',N'No',NULL,NULL),
    (N'18-43','2018-11-07 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il fornitore OEM Fast Track ha implementato due  Change Control relativi al prodotto Atypical CAP: 
- CCR2018-072: new version of the validation file due to QCMD data update (allegato 1)
- CCR2018-085: align manual with already corrected website: "throat/nasal swabs, bronchoalveolar lavage, sputum and culture of human origin" shall be changed into "throat/nasal swabs, bronchoalveolar lavage, sputum of human origin and culture". (allegato 2).
E'' necessario valutare se tale cambiamento ha impatto sul prodotto ELITEch: Respiratory Bacterial RTS553ING.',N'Valutazione degli eventuali impatti sul prodotto ELITEch Respiratory Bacterial RTS553ING, a seguito del cambiamento apportato da FTD al prodotto Atypical CAP.',N'Se il cambiamento non ha impatto sul prodotto ELITEch "Respiratory Bacterial RTS553ING", questo CC verrà chiuso senza svolgere alcuna attività.',NULL,N'SPM
PVS
QARA
QC
QM
RDM',N'Ricerca e sviluppo
Sistema qualità',N'Le modifiche apportate al validation file ed al manuale del kit Atypical CAP riguardano test eseguiti su piattaforme non utilizzate/validate da ELITech. I dati relativi ai test su QCMD panel ottenuti da Fast Track, cosi come le matrici indicate nel manuale,  non vengono utilizzati per il kit Elitech Respiratory Bacterial RTS553ING. 
Le modifiche apportate al validation file ed al manuale del kit Atypical CAP riguardano test eseguiti su piattaforme non utilizzate/validate da ELITech. I dati relativi ai test su QCMD panel ottenuti da Fast Track, cosi come le matrici indicate nel manuale,  non vengono utilizzati per il kit Elitech Respiratory Bacterial RTS553ING. ',N'16/11/2018
16/11/2018',N'Non è necessario implementare nella documentazione (FTP) del kit Elitech Respiratory Bacterial queste modifiche apportate alla documentazione del kit FTD Atypical CAP.
Non è necessario implementare nella documentazione (IFU) del kit Elitech Respiratory Bacterial queste modifiche apportate alla documentazione del FTD kit Atypical CAP.',N'allegato 1 - ccr_2018-072_post_cab.pdf
allegato 2 - ccr_2018-085_post_cab.png',N'QM
RDM',N'16/11/2018
16/11/2018',NULL,N'16/11/2018
16/11/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,'2018-11-16 00:00:00',N'No',NULL,NULL),
    (N'18-42','2018-11-07 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il fornitore in OEM Fast Track ha implementato un Change Control relativo al prodotto Vesicular Rash. Il change control CCR2018-042 riguarda l''implementazione di una nuova revisione del "Validation File" del prodotto dovuta ad un aggiornamento dei test effettuati su QCMD panel. E'' necessario valutare se tale cambiamento ha impatto sul prodotto ELITEch: Meningitis Viral RTS507ING.',N'Valutazione degli eventuali impatti sul prodotto ELITEch "Meningitis Viral RTS507ING" a seguito del cambiamento apportato da FTD al prodotto Vesicular Rash.',N'Se il cambiamento non ha impatto sul prodotto ELITEch "Meningitis Viral RTS507ING"  questo CC verrà chiuso senza svolgere alcuna attività.',NULL,N'SPM
PVS
QARA
QC
QM
RDM',N'Ricerca e sviluppo
Sistema qualità',N'Le modifiche apportate al Validation File del prodotto Vesicular Rash riguardano test eseguiti su piattaforme non utilizzate con il kit ELITEch  "Meningitis Viral RTS507ING". I dati relativi ai test su QCMD panel ottenuti da Fast Track non vengono utilizzati da ELITech. 
Le modifiche apportate al Validation File del prodotto Vesicular Rash riguardano test eseguiti su piattaforme non utilizzate con il kit ELITEch  "Meningitis Viral RTS507ING". I dati relativi ai test su QCMD panel ottenuti da Fast Track non vengono utilizzati da ELITech. 
Le modifiche apportate al Validation File del prodotto Vesicular Rash riguardano test eseguiti su piattaforme non utilizzate con il kit ELITEch  "Meningitis Viral RTS507ING". I dati relativi ai test su QCMD panel ottenuti da Fast Track non vengono utilizzati da ELITech. ',NULL,N'Non è necessario implementare nella documentazione (FTP) del kit Elitech Meningitis Viral queste modifiche apportate alla documentazione del kit FTD Vesicular Rash. 
Non è necessario implementare nella documentazione (IFU) del kit Elitech Meningitis Viral queste modifiche apportate alla documentazione del kit FTD Vesicular Rash. ',N'CCR 2018-042 - ccr_2018-042_post_cab.pdf',N'QM
RDM',NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'18-41','2018-10-31 00:00:00',N'Roberta  Paviolo',N'Progetto',N'Inserire nel IFU di RTS098PLD, C. Trachomatis ELITe MGB Kit, la possibilità di utilizzo con lo strumento NucliSENSE easyMAG (BioMerieux), come concordato con il personale del MKT. ',N'Permettere l''utilizzo del prodotto TAQ con  NucliSENSE easyMAG (BioMerieux), rendendo il prdotto nella nuova versione equivalente alla versione TFI in cui era previsto l''utilizzo con NucliSENSE easyMAG.',N'Prodotto non utilzzabile con NucliSENSE easyMAG (BioMerieux).',NULL,N'SPM
RDM
JPM',N'Ricerca e sviluppo
Sistema qualità
Validazioni',N'Equiivalenza dell''uso in associazione con NucliSENSE easyMAG tra la versione del prodotto TFI e TAQ.
Equiivalenza dell''uso in associazione con NucliSENSE easyMAG tra la versione del prodotto TFI e TAQ.
Equiivalenza dell''uso in associazione con NucliSENSE easyMAG tra la versione del prodotto TFI e TAQ.
Nessun impatto se non in termini di ore lavoro.
Nessun impatto se non in termini di ore lavoro.',N'31/10/2018
31/10/2018
11/12/2018
31/10/2018
31/10/2018',N'Stesura dei protocolli di verifica del Limit of Detection (LoD) che verranno utilizzati dal Centro di Validazione qualificato per l''esecuzione dei test e successiva analisi dei dati e stesura del report.
Aggiornamento IFU.
Coordinare l''esecuzione dei test con il centro di validazione in concomitanza con l''estensione d''uso del prdotto su Ingenius.
Messa in uso IFU
Aggiornamento FTP.',N' AGGIORNAMENTO FTP ALLA SEZIONE 2, 6, 8, 11 - cc18-41_section_1_product_technical_file_index_04_2.docx
IFU REV 05 IN USO AL 15/02/19 - cc18-41_ifu_rts098pld.msg
AGGIORNAMENTO FTP ALLA SEZIONE 7 - cc18-41_section_1_product_technical_file_index_04_1.docx
IFU REV05 IN USO AL 15/02/19 - cc18-41_ifu_rts098pld_1.msg
FTP098PLD-1_REV04 FIRMATO IL 07/02/2019 - cc18-41_section_1_product_technical_file_index_04.docx',N'RT
RT
PVS
QMA
QMA',N'31/10/2018
31/10/2018
31/10/2018
31/10/2018
31/10/2018',N'19/12/2018
21/12/2018
21/12/2018
31/01/2019
28/02/2019',N'07/02/2019
15/02/2019
07/02/2019
15/02/2019
07/02/2019',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,N'-',N'No',NULL,N'No',NULL,N'No',NULL,NULL,'2019-02-28 00:00:00',NULL,'2019-02-15 00:00:00',N'No',NULL,NULL),
    (N'18-40','2018-10-09 00:00:00',N'Fabio Panariti',N'Documentazione SGQ',N'Aggiornamento della specifiche assistenza tecnica Refrigeratori (SPEC-CG-CR-FR-UC rev01) e assistenza tecnica Spettrofotometri (SPEC-SF rev00) per inserimento nuovi strumenti.',N'Corretta gestione degli strumenti interni in fase di manutenzione.',N'Errata gestione degli strumenti',NULL,N'QM',N'Assistenza strumenti interni
Sistema qualità',N'Corretta gestione degli strumenti interni in fase di manutenzione.
Corretta gestione degli strumenti interni in fase di manutenzione.
Nessun impatto se non in termini di ore lavoro.
Nessun impatto se non in termini di ore lavoro.',N'12/10/2018
12/10/2018
12/10/2018
12/10/2018',N'Aggiornamento SPEC-CG-CR-FR-UC rev01e SPEC-SF rev00.
Aggiornamento delle specifiche eliminando i codici strumento, ma mantenendo solo il modello.
Messa in uso.
Messa in uso delle specifiche senza i codici strumento, ma con solo il modello.',N'SPEC-SF rev01, SPEC-CG-CR-FR-UC rev02 - cc18-40_messa_in_uso_specifiche_ass.tecnica_refrigeratori_e_spettrofotometri_cc18-40.msg
 SPEC-SF rev01, SPEC-CG-CR-FR-UC rev02  - cc18-40_messa_in_uso_specifiche_ass.tecnica_refrigeratori_e_spettrofotometri_cc18-40_1.msg',N'TSS
QMA
TSS
QMA',N'12/10/2018
06/12/2018
12/10/2018
06/12/2018',NULL,N'20/11/2018
20/11/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'T - Taratura Esterna Strumenti',NULL,N'T3',N'No',NULL,N'No',NULL,N'No',NULL,N'Daniele Salemi',NULL,NULL,'2018-11-20 00:00:00',N'No',NULL,NULL),
    (N'18-39','2018-10-03 00:00:00',N'stefano borri',N'Documentazione SGQ',N'Nuovo flusso con portale gare e recepimento nuova organizzazione',N'Adozione di uno strumento per la gestione del flusso, l''automatizzazione del processo e la realizzazione di form standard',N'Maggiore complessità nella predisposizione dei documenti, tempi più lunghi per le approvazioni e minore possibilità di tracciare le operazioni effettuate',NULL,N'QM
ITM',N'Amministrazione
Assistenza applicativa
Gare
Marketing vendite italia
Sistema informatico
Sistema qualità',N'Automazione del processo di preparazione dei documenti e del flusso approvativo, miglior archiviazione dei documenti.
Automazione del processo di preparazione dei documenti e del flusso approvativo, miglior archiviazione dei documenti.
Automazione del processo approvativo, possibilità di inserire direttamente una proposta di offerta, maggior facilità nella condivisione delle informazioni.
Maggiore facilità di condivisione delle informazioni, possibilità di operare sul medesimo portale di vendite e marketing, archiviazione moduli standard per doc. amministrativa e relazione tecniche.
Allineamento tra sistemi aziendali, setting del portale per utilizzo da parte degli utenti.
Gestione delle documentazione e della validazione di processo.
Gestione delle documentazione e della validazione di processo.
Coinvolgimento nel flusso approvativo in relazione alle proposte di conto economico',N'18/10/2018
18/10/2018
18/10/2018
18/10/2018
18/10/2018
08/10/2018
08/10/2018',N'Aggiornamento della procedura PR03,02 "Gestione dei bandi di gara" rev00 e dei documenti collegati.
Gestione dell''installazione da parte del fornitore qualificato del portale gare e formazione agli utilizzatori.
Utilizzo del nuovo protale a seguito di formazione.
Utilizzo del nuovo protale a seguito di formazione.
Allineamento con NAV e setting del portale.
Messa in uso della procedura e dei documenti collegati.
Gestione dell''aggiornamento del validation master plan.
Coinvolgimento nel flusso approvativo in relazione alle proposte di conto economico',N'pr03,02 rev01 - cc18-39_messa_in_uso_procedura_offerte_commerciali.msg
Specifiche funzionali - cc18-39_sgat_g&o_-_1_-_a_-_v1.00.00_-_specifiche_funzionali_-_analisi_e_impatto.pdf
FORMAZIONE SGAT GARE &OFFERTE - cc18-39_formazione_2019-01-30_sm-kam_agenti_fps_csc_sgatgare.pdf
pr03,02 rev01 - cc18-39_messa_in_uso_procedura_offerte_commerciali_1.msg
formazione assistenza applicativa - cc18-39_formazione_2019-01-30_sm-kam_agenti_fps_csc_sgatgare.pdf
Verifica allineamento con NAV - cc18-37_allegato7_risk_assessment_sww38_test_case_prezzi_e_sconti_sgat_nav.pdf',N'T&CM
T&CM
QMA
QMA
SMD
CSC
FPS
ITM
CFO',N'18/10/2018
18/10/2018
18/10/2018
18/10/2018
08/10/2018
08/10/2018',NULL,N'12/03/2021
30/01/2019
12/03/2021
31/01/2019
09/05/2019
12/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'G - Gare',N'G1 - Selezione, nalisi e valutazione bandi di gara',N'rivedere tutti i risk assessment.',N'No',N'non onecessaria',N'No',N'non applicabile',N'No',N'non applicabile',N'stefano borri',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'18-38','2018-09-27 00:00:00',N'Samuela Margio',N'Documentazione SGQ',N'Viene richiesta la creazione di un nuovo documento: Market Launch Report. Tale documento riporta tutte le attività pianificate nel corrispettivo Market Launch Plan (documento di pianificazione delle attività di lancio di nuovi prodotti) indicando quali attività sono state effettuate, quando e dove è reperibile (archivio cartaceo o elettronico) l''eventuale documento prodotto. Vengono indicate anche le attività non pianificate nel MLP e quelle pianificate, ma non eseguite.  ',N'Il Market Launch Report consente di monitorare e dare facile evidenza e consultazione in un unico documento delle modalità di esecuzione delle attività effettuate per il lancio dei nuovi prodotti. ',N'Perdita della tracciabilità delle attività pianificate per il lancio dei nuovi prodotti.',NULL,N'SPM
PMS
QMA
SA-R&D
JPM',N'Marketing
Program mangment
Ricerca e sviluppo
Senior Advisor R&D
Sistema qualità',N'Atraverso il nuovo Market Launch Report è possibile monitorare  in modo semplice (consultazione di un unico documento) le modalità di esecuzione delle attività effettuate per il lancio dei nuovi prodotti, migliorandone la tracciabilità.
Atraverso il nuovo Market Launch Report è possibile monitorare  in modo semplice (consultazione di un unico documento) le modalità di esecuzione delle attività effettuate per il lancio dei nuovi prodotti, migliorandone la tracciabilità.
L''introduzione della registrazione dell''esecuzione delle attività effettuate per il lancio dei nuovi prodotti, migliora la tracciabilità delle informazioni relative all''introduzione di nuovi prodotti sul mercato.
L''introduzione della registrazione dell''esecuzione delle attività effettuate per il lancio dei nuovi prodotti, migliora la tracciabilità delle informazioni relative all''introduzione di nuovi prodotti sul mercato.
L''introduzione della registrazione dell''esecuzione delle attività effettuate per il lancio dei nuovi pr',N'27/09/2018
27/09/2018
03/12/2018
03/12/2018
03/12/2018
27/09/2018
27/09/2018
03/12/2018',N'Nessuna attività a carico.
Nessuna attività a carico.
Messa in uso del  Nuovo documento, creazione del corrispondente Modulo, aggiornamento delle relative procedure
Aggiornamento della procedura XX.
Coordinamento dell''aggiornamento della procedura PR19,01, per l''inserimento del MLR.
02/2020: la PR19,01riguarda esclusivamente l''aasistenza applicativa, includere il documento nella PR19,06 "Gestione Marketing"
Creazione del nuovo Market Launch Report  
Informazione e formazione del personale coinvolto sull''uso e sulla consultazione del MLR.
Nessuna attività da svolgere.',N'MESSA IN USO MLRyyyy,xxx - cc18-38_messa_in_uso_mlr_cc18-38.msg
 MESSA IN USO MLRyyyy,xxx  - cc18-38_messa_in_uso_mlr_cc18-38_1.msg',N'JPM
SA-R&D
QMA
PMS
QM
RDM
RT
QM
PMS',N'27/09/2018
27/09/2018
03/12/2018
03/12/2018
27/09/2018
03/12/2018
03/12/2018',N'30/11/2018
20/12/2018',N'14/12/2018
14/12/2018
14/12/2018',N'No',N'nessunimpatto',N'No',N'non necessaria, la modifica riiguarda un processo interno',N'No',N'nessun impatto, la modifica riiguarda un processo interno',N'ASA - Assistenza applicativa prodotti',N'ASA4 - Assistenza applicativa al cliente',N'ASA4',N'No',N'la modifica riiguarda un processo interno che non nnecessita di notifica',N'No',N'nessun virtual manufacturer',N'No',N'na',N'Samuela Margio','2018-12-20 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'18-37','2018-09-25 00:00:00',N'Katia Arena',N'Fornitore',N'Selezione nuovo fornitore per la produzione delle spugne (codice 954-044) utilizzate nei BANG e BRK200. Al termine delle scorte a magazzino dismissione delle spunge (codice 954-045) utilizzate come riempimento nei kit di estrazioni.',N'NA',N'Impossibilità a produrre i BANG/BRK200.',NULL,N'OM
QM
RDM',N'Acquisti
Magazzino
Produzione
Ricerca e sviluppo',N'La cessata attività del fornitore storico (BEQUBE) di spugne e la comunicazione della stessa in tempi non consoni, non ha permesso di acquistare delle scorte per coprire il 2018. 
La cessata attività del fornitore storico (BEQUBE) di spugne e la comunicazione della stessa in tempi non consoni, non ha permesso di acquistare delle scorte per coprire il 2018. 
Nessun impatto se non in termini di carico di lavoro.
Nessun impatto se non in termini di carico di lavoro.
Nessun impatto se non in termini di carico di lavoro.',N'25/09/2018
25/09/2018
25/09/2018
25/09/2018
25/09/2018',N'Ricerca di un fornitore alternativo: identificato il fornitore ESPAplast di Avigliana, che produce materiale in polipropilene espanso.  Inoltre è stata fatta richiesta all''attuale fornitore di scatole per avere una proposta alternativa alle spugne (inserti in cartone forato), da valutare.
Approvazione del materiale fornito da ESPAplast da R&D ed aggiornamento della specifica 954-044
Controllo al ricevimento del primo lotto utilizzando la nuova specifica.
Nessuna attività a carico, se non ricevere l''informazione sul primo lotto di spugne fornite da ESPAplas.',N'matrice inserto in cartone. Allo stato attuale non sono previste prove di tenuta del cartone, nè aggiornamenti del flusso produttivo (le spugne sono stoccata fuori dalle scatole nell''attesa del CQ, questo non sarebbe possibile per gli inserti in cartone che dovrebbero essere stoccati all''interno delle scatole in attesa del CQ). - cc18-37_matrice_inserto_in_cartone.jpg
SPEC954-044_02 - cc18-37_messa_in_uso_spec954-044_rev02_cc18-37.msg
primo lotto nuova spugna - primo_arrivo_nuova_spugna_954-044_18-po-01350.pdf
verificate le dimensioni della spugna e dei fori - primo_arrivo_nuova_spugna_954-044_18-po-01350_1.pdf',N'PW
RT
W
MM',N'25/09/2018
25/09/2018
25/09/2018
25/09/2018',N'09/11/2018
31/12/2018
31/12/2018',N'06/11/2018
26/10/2018
16/10/2018
16/10/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'F - Gestione dei Fornitori',NULL,N'F6',N'No',NULL,N'No',NULL,N'No',NULL,N'Katia Arena','2018-12-31 00:00:00',NULL,'2018-11-06 00:00:00',N'No',NULL,NULL),
    (N'18-36','2018-09-24 00:00:00',N'Francesco Gorreta',N'Progetto',N'Rilascio di una nuova versione del SW InGenius 1.3 per migliorare l''interpretazione delle Multiplex in caso di campioni altamente positivi.',N'Prevenire falsi positivi.',N'Possibilità di generare falsi positivi in condizioni particolari con saggi Multiplex.',NULL,N'SPM
GSC
QARA
RDM',N'Assistenza applicativa
Assistenza strumenti esterni
Program mangment
Regolatorio
Ricerca e sviluppo',N'Dedicare risorse per la generazione del nuovo sw e la sua validazione.
Dedicare risorse per la validazione del nuovo SW.
Dedicare risorse per la validazione del nuovo SW.
Dedicare risorse per la validazione del nuovo SW.
Dedicare risorse per la validazione del nuovo SW.
Dedicare risorse per la comunicazione della nuova versione del SW presso i clienti.
Dedicare risorse per l''installazione della nuova versione del SW presso i clienti.',N'24/09/2018
24/09/2018
24/09/2018
24/09/2018
24/09/2018
24/09/2018
08/10/2018',N'Segnalare il malfuzionamento a PSS (vedere bug list "MANTIS TICKET 402").
Test Plan,  esecuzione dei test di validazione e Test Report.
Verificare la documentazione per il rilascio della nuova versione del SW.
Aggiornare il FTP.
Creare la Release Note per il rilascio sul mercato.
Redigere il TSB.
Pianificare gli aggiornamenti in campo.',N'P280062SS019-03_geneLEADXII_RemainBugsList(Mantis)  - cc18-36_elite_ingenius-buglist_test_plan-reportsw1.3.0.12.msg
PLAN2018-018_24-09-2018, PLAN2018-040_24-09-2018, PLAN2018-043_07-11-2018, REP2018-061_29-10-2018, REP2018-152_14-11-2018, REP2018-174_19-11-2018, REP2018-184_19-11-2018. - cc18-36_elite_ingenius-buglist_test_plan-reportsw1.3.0.12_1.msg
release note - cc18-36_5_release_note_sw_1.3.0.12_ingenius_1.pdf
indice FTP08/07/2019 - cc18-36_section_1_product_technical_file_index_07.docx
release note - cc18-36_5_release_note_sw_1.3.0.12_ingenius.pdf
TSB036_versione sw 1.3.0.12 - cc18-36_elite_ingenius_-tsb_036_-_rev_a_-_sw_version_1.3.0.12_with_signature.pdf
CC18-36_la RAC19-23 e RAC20-20 gestiscono la nuova versione che verrà messa in uso 1.3.0.15 - cc18-36_la_rac19-23_e_rac20-20_gestiscono_la_nuova_versione_che_verra_messa_in_uso_1.3.0.15.txt',N'SPM
RDM
QARA
QMA
QARA
GSC
CSC',N'24/09/2018
24/09/2018
24/09/2018
24/09/2018
24/09/2018
24/09/2018
08/10/2018',NULL,N'13/11/2018
19/11/2018
20/11/2018
20/11/2018
20/11/2018
30/11/2018
23/09/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'SW - Elenco Software',N'SW20 - ELITe InGenius Software',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Francesco Gorreta',NULL,NULL,'2020-09-23 00:00:00',N'No',NULL,NULL),
    (N'18-35','2018-09-06 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il fornitore OEM Fast Track ha implementato un nuovo change control in data 30/07/18 relativo al prodotto EPA PC (bulk del nostro prodotto Meningitis Viral 2-ELITe Positive Control, CTR523ING). Il change control di Fast Track è il CCR2018-101 (allegato 1): reviewing of plasmid concentration in order to be in the middle of specification range (Ct between 25 and 30); impact: significant; date of closure 30/07/2018. 
Si richiede di implementare la modifica apportata da Fast Track dopo la valutazione dell''impatto sulle prestazioni e sulla documentazione dei prodotti Meningitis Viral 2 ELITe MGB Panel, RTS523ING, e CTR523ING; poiché tali prodotti sono validati su InGenius, è necessario valutare se il cambiamento apportato da Fast Track e valutato su ABI7500, possa causare modifiche di performance del kit quando utilizzato con ELITe InGenius.',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay Protocol',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate, con le nuove caratteristiche, verrebbero utilizzate non sotto controllo.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit in uso con lo strumento InGenius.
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit in uso con lo strumento InGenius.
Analisi del primo lotto prodotto con la bulk al fine di verificare la necessità di implementazione dei criteri di CQ.
Impatto in termini di ore lavoro.
Valutare se il cambiamento apportato da Fast Track su Mix e positive control, da loro valutato sullo strumento ABI7500, possa causare modifiche di performance quando utilizzato con ELITe InGenius.',NULL,N'Analisi dei report di Fast Track
Eventuale aggiornamento Assay Protocol e criteri di CQ
Analisi del primo lotto e modifica documenti di CQ
Implementazione del CC di Fast Track nel file tecnico di prodotto
Eventuale aggiornamento delle performance del kit sul manuale di istruzioni quando utilizzato con ELITe InGenius.',N'CC2018-101 - ccr_2018-101_closed_31.07.2018.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_4.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_5.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_6.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_7.pdf',N'RDM
RT
DS
QCT
QM
QMA',NULL,NULL,N'20/07/2020
20/07/2020
20/07/2020
20/07/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Alessandra Gallizio',NULL,NULL,'2020-07-20 00:00:00',N'No',NULL,NULL),
    (N'18-34','2018-09-05 00:00:00',N'Ferdinando Fiorini',N'Stabilità',N' Estendere la scadenza da 12 a 18 mesi del prodotto ESBL ELITe MGB KIT (RTS201ING) sulla base dei dati di stabilità reale, come comunicato da R&D in data 04/09/2018',N'Aggiungere 6 mesi alla durata del prodotto',N'Restare con la scedenza di 12 mesi',NULL,N'MM
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente e economica, riducendo i rischi di obsolescenza. Quindi l''impatto è 
sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS201ING (STB2018-081, lotto U1116BC -20°C per 18 mesi, STB2018-060, lotto U1016AZ -20°C per 18 mesi, STB2018-051, lotto U0916AW -20°C per 18 mesi) non c''è nessun impatto sulle prestazioni del prodotto.',N'05/09/2018
06/09/2018
06/09/2018
03/10/2018
06/09/2018
06/09/2018',N'Rietichettare il lotto U1117AP cambiando la scadenza in 31/05/2019.
Messa in uso documento modificato.
aggiornamento FTP
Aggiornamento del modulo di produzione MOD962-RTS201ING, modificando la frase "attribuire la scadenza del Prodotto (massimo 12 mesi)" in "attribuire la scadenza del Prodotto (massimo 18 mesi)".
Avvisare i distributori dove il prodotto è registrato.
Nessuna altra azione a carico di R&D.',N'rilav. nuovo lotto U0918AD 2019/05 - cc18-34_rilavorazione_per_aumento_scadenza.pdf
MOD962-RTS201INGrev01 - cc18-34_messa_in_uso_mod962-rts201ingrev01_cc18-34_mod1802_del_522019.msg
MOD962-RTS201ING rev01, MOD18,02 del 5/2/2019 - cc18-34_messa_in_uso_mod962-rts201ingrev01_cc18-34_mod1802_del_522019_1.msg
colombia-ecuador-messico-uruguai - colombia-ecuador-messico-uruguai_esbl_stability_extension.msg
serbia - serbia_esbl_stability_extension.msg
tailandia - tailandia_esbl_stability_extension.msg
turchia - turchia_esbl_stability_extension.msg',N'PT
QMA
QMA
DS
QC
QMA
RDM',N'06/09/2018
06/09/2018
06/09/2018
03/10/2018
06/09/2018
06/09/2018',N'07/09/2018
31/10/2018
06/09/2018',N'06/09/2018
07/02/2019
05/03/2021
15/02/2019
12/09/2018
06/09/2018',N'No',N'na',N'No',N'na',N'No',N'na',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.5 - Assegnazione scadenza',N'1.5',N'No',N'na',N'No',N'na',N'No',N'na',N'Ferdinando Fiorini','2018-10-31 00:00:00',NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'18-33','2018-08-24 00:00:00',N'Alessandra Vinelli',N'Processo di produzione',N'Dismissione prodotti _RTS076MR_RTS176MR_RTST01MR',N'L''obiettivo della dismissione dei reagenti inclusi nell''elenco è il risparmio dei costi di produzione ad essi legati. Elenco dei prodotti:  RTS076MR, CTR076MR, STD076MR; RTS176MR, CTR176MR, STD176MR; RTST01MR (RUO), CTRT01MR. L''apertura del change control avviene a seguito della valutazione del volume delle vendite in Italia e all''estero negli ultimi 24 mesi. Si ritiene che gli attuali utilizzatori dei prodotti possano passare all''acquisto degli analoghi prodotti ELITe MGB se correttamente affiancati dal FPS.  Criticità: un cliente (codice spedizione: C02173) applica nella sua routine diagnostica il prodotto RTS076MR per la  diagnosi di EV delle infezioni gastrointestinali, utilizzando come matrice le feci. Tale matrice non è validata per la rilevazione e la quantificazione di EV  né per il prodotto Alert, né per l''analogo Pleiadi (RTS076PLD). ',N'Mantenimento sul mercato di prodotti tecnologicamente superati. ',NULL,N'S&MM-MDx
SMD',N'Acquisti
Assistenza applicativa
Gestione vendite e mkt estero
Marketing
Marketing vendite italia
Ordini
Produzione
Regolatorio
Sistema qualità',N'I prodotti che si intende dismettere possono essere sostituiti dall''analogo prodotto pleiadi. La dismissione e la conseguente eliminazione dei prodotti a listino semplificherà la gestione documentale a favore dei prodotti tecnologicamente più avanzati. I prodotti inclusi nell''elenco dovranno comunque essere cancellati dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria. 
I prodotti che si intende dismettere possono essere sostituiti dall''analogo prodotto pleiadi. La dismissione e la conseguente eliminazione dei prodotti a listino semplificherà la gestione documentale a favore dei prodotti tecnologicamente più avanzati. I prodotti inclusi nell''elenco dovranno comunque essere cancellati dai database ministeriali, richiedendo ancora una seppur residua attività regolatoria. 
Gestione dei quantitativi dei materiali di produzione della linea Alert, riquantificati in base ai prodotti dismessi. 
Gestione dei quantitativi dei materiali di produzione della linea Alert, riqu',N'13/09/2018
13/09/2018',N'Compilazione ed aggiornamento nel corso del tempo del MOD04, 12 per ciascuno dei prodotti in elenco secondo le fasi riportate (prodotto in dismissione, dismesso, cancellato). 
Allineamento in collaborazione con l''ufficio acquisti dei quantitativi di acquisto delle materie prime per la linea ALERT in base ai prodotti dismessi. 
Allineamento con il personale che pianifica le attività di produzione relativamente all''acquisto delle materie prime per la linea ALERT in base ai prodotti dismessi.
Apertura del CC che gestisce la dismissione dei prodotti 
IndaginiRTS076M precedenti all''apertura del change control hanno evidenziato che negli ultimi 18 mesi i kit sono stati venduti per la maggior parte in Italia. Per questi prodotti non ci sono gare attive con vincoli commerciali. Relativamente al prodotto RTS076MR si evidenzia una criticità commerciale con un cliente che utilizza il kit per analizzare campioni di feci in pazienti con sospetta gastroenterite. Questo prodotto non è validato per le feci ma il c',NULL,N'QARA
QMA
PP
PW
JPM
JPM
JPM
S&MM-MDx
S&MM-MDx
AM
JPM
QMA
SAD-OP
FPS',N'13/09/2018',NULL,N'25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020',N'No',N'nessun impatto',N'Si',N'Come da MOD04,12 comunicazione ufficiale ai distributori ed ai clienti sul prodotto fuori porduzione e dismesso ad esaurimento scorte a magazzino.',N'Si',N' I prodotti RTS076MR, CTR076MR, STD076MR; RTS176MR, CTR176MR, STD176MR; RTST01MR (RUO), CTRT01MRverranno dismessi.',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'nessun impatto',N'No',N'eseguita, vedere anche cc20-23',N'No',N'nessun virtula manufaturer',N'No',N'na',NULL,NULL,NULL,'2020-11-25 00:00:00',N'No',NULL,NULL),
    (N'18-32','2018-07-25 00:00:00',N'Alessandra Vinelli',N'Processo di produzione',N'Dismissione prodotti_RTS120_RTS038M_STD098_EXTG01',N'L''obiettivo della dismissione dei reagenti inclusi nell''elenco seguente è il contenimento dei costi di produzione.  Elenco prodotti: RTS120 (MTB Q - PCR Alert Kit); RTS038-M (HHV8 Q - PCR Alert AmpliMIX); CTR038 (HHV8 - Positive Control); STD038 (HHV8 Q - PCR Standard); STD098 (CHLAMYDIA tr. Q - PCR Standard); EXTG01 (EXTRAgen®). Prima di aprire il change control si è accertato  che i prodotti inclusi non sono stati più prodotti negli ultimi 12 mesi in quanto le richieste dall''Italia, e dall''estero erano basse. Si è verificato che i clienti, che in passato hanno comprato questi prodotti lo siano ancora e, qualora lo siano, siano passati agli analoghi kit ELITe MGB. ',N'Mantenimento di una linea di prodotti obsoleta.',NULL,N'S&MM-MDx
SMD',N'Acquisti
Gestione vendite e mkt estero
Marketing
Marketing vendite italia
Ordini
Produzione
Regolatorio
Sistema qualità',N'I prodotti obsoleti sono già sostituiti sul mercato dall''analogo prodotto pleiadi. L''eliminazione dei prodotti a listino porterà all''ottimizzazione della gestione documentale dei prodotti tecnologicamente più avanzati, pur rimanendo una residua attività regolatoria fino alla cancellazione del prodotto dai Database Ministeriali.
I prodotti obsoleti sono già sostituiti sul mercato dall''analogo prodotto pleiadi. L''eliminazione dei prodotti a listino porterà all''ottimizzazione della gestione documentale dei prodotti tecnologicamente più avanzati, pur rimanendo una residua attività regolatoria fino alla cancellazione del prodotto dai Database Ministeriali.
I prodotti obsoleti sono già sostituiti sul mercato dall''analogo prodotto pleiadi. L''eliminazione dei prodotti a listino porterà all''ottimizzazione della gestione documentale dei prodotti tecnologicamente più avanzati, pur rimanendo una residua attività regolatoria fino alla cancellazione del prodotto dai Database Ministeriali.
I prodotti obsol',NULL,N'Compilazione ed aggiornamento nel corso del tempo del MOD04,12 per ciascuno dei prodotti in elenco, secondo le fasi riportate (prodotto in dismissione. dismesso, cancellato). 
comunicazione a DEKRA della dismissione del prodotto STD098, per cancellazione dal certificato (n°2110151CE02)
Aggiornamento di manuali ed analisi dei rischi dei prodotti utilizzati in associazione con EXTG01.
Aggiornamento / archiviazione SDS contenenti reagenti presenti nel EXTG01.
Allineamento in collaborazione con il personale degli acquisti dei quantitativi di acquisto delle materie prime per la linea alert in base ai prodotti dismessi.
Allineamento in collaborazione con il personale che pianifica le attività di produzione dei quantitativi di acquisto delle materie prime per la linea alert in base ai prodotti dismessi.
Apertura del CC che gestisce la dismissione dei prodotti.
Come da indagini precedenti all''apertura del CC, non sono presenti gare attive, con vincoli commerciali, per il prodotto in questione, non ci ',N'noC dekra - cc18-32_notice_of_change_egspa_std098.pdf',N'QARA
QMA
PP
PP
JPM
JPM
FPS
JPM
QMA
SAD-OP
JPM
S&MM-MDx
QARA
JPM
QMA
QMA',N'02/01/2019
02/01/2019',NULL,N'25/11/2020
02/08/2019
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/07/2018
25/07/2018
25/11/2020
25/11/2020
25/11/2020
25/11/2020
25/11/2020',N'No',N'nessun impatto',N'Si',N'Come da MOD04,12 comunicazione ufficiale ai distributori ed ai clienti Italia sul prodotto fuori produzione e dismesso ad esaurimento scorte a magazzino.',N'Si',N' I prodotti: RTS120; RTS038-M; CTR038; STD038; STD098; EXTG01 verranno dismessi.',N'DC - Gestione controllo dei documenti (esclusi i manuali e le etichette)',N'DC12 - Archiviazione cartacea dei documenti e di alcune registrazioni',N'nessun impatto sul risk assessement',N'No',N'svolta (vedere anche cc20-23)',N'No',N'non necessaria',N'No',N'nessun impatto',NULL,NULL,NULL,'2020-11-25 00:00:00',N'No',NULL,NULL),
    (N'18-31','2018-07-20 00:00:00',N'Alessandra Gallizio',N'Confezione',N'A seguito di richiesta da parte del cliente R-Biopharm, si richiede di utilizzare tappi di colore nero (codice 953-221) per I vial del componente Internal control per I prodotti della linea RIDAGENE, al posto dei tappi di colore marrone originalmente richiesti. ',N'soddisfare una richiesta specifica del cliente ',N'I vial con tappi di colore marrone potrebbero generare confusione nell''utilizzatore finale, in quanto già utilizzati in altri prodotti R-Biopharm',NULL,N'DS
MM
QC
QMA
RDM',N'Acquisti
Controllo qualità
Produzione
Program mangment
Ricerca e sviluppo
Sistema qualità',N'L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso.
L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso.
L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso.
Nessun impatto sul processo è necessario pianificare una revisione della versione LP1 dei documenti
Nessun impatto sul processo è necessario pianificare una revisione della versione LP1 dei documenti
L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso. 
L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso. 
L''efficace gestione delle modifiche richieste dal cliente ha un''impatto positivo sui rapporti col cliente stesso. 
Impatto in termini di ore lavoro, in quanto è necessario verificare la disponibilità del materiale.
Nessun impatto ',N'20/07/2018
20/07/2018
20/07/2018
24/07/2018
24/07/2018
20/07/2018
20/07/2018
20/07/2018
24/07/2018
20/07/2018',N'Acquisire il Packaging and Labelling Plan nuova revisione da R-Biopharm e implementarlo nella documentazione
tracciare e motivare il cambio del colore dei colore tappi (da marrone a nero) nel Design Transfer Report, rispetto a quanto pianificato nel Design Transfer Plan.
inserimento della specifica dei tappi neri (SPEC953-221) nel DMRI RIDAGENE CMV, in corso di revisione (DMRI2018-004)
inserimento del codice dei tappi (953-221)  nei moduli di produzione
formazione al personale di produzione
modificare MOD09,32 "Layout prodotto finito" datato 14/06/2018, e modificare "Letter of acknowledgement" datata 01/06/2018. inviare tali documenti a R-Biopharm.
Verificare la disponibilità del materiale,k ed eventualmente acquistarlo. 
formazione sull''uso dei tappi neri.',N'PLP - cc18-31_v03_plp_elitech_r-biopharm_2018_07_09_final.pdf
DMRI2018-004_18/10/18 - cc18-31_dmri2018-004_firmato_il_18-10-18.txt
MOD958_cmv_ic contiene il materiale 953-221 "tappo nero" - mod958_cmv_ic-d_00.pdf
messa in uso MOD958-cmv-ic rev00 - cc18-31_messa_in_uso_moduli_r-bio_mod958_cmv_ic.msg
comunicazione aggiornamento MOD09,32 e aggiornamento "letter of acknowledgement" - cc18-31_comunicazoinemod09,32_e_letterofacknowledgement.msg
REP2018-181 - cc18-31_rep2018-181_design_transfer_report_ridagene_cmv.docx
formazione MOD18,02 del 22/10/2018 - cc18-31_messa_in_uso_moduli_r-bio_mod958_cmv_ic_1.msg',N'PMS
RT
DS
QMA
PMS
PW
QC
QC',N'20/07/2018
20/07/2018
20/07/2018
24/07/2018
03/10/2018
20/07/2018
24/07/2018
20/07/2018',N'30/09/2018
30/11/2018
30/11/2018
30/09/2018
03/10/2018
30/09/2018
24/07/2018',N'12/09/2018
26/07/2019
18/10/2018
05/11/2018
05/11/2018
07/08/2018
24/07/2018',N'Si',N'DMRI2018-004',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'Si',N'Invio a R-Bioopharma dei seguenti documenti MOD09,32 "Layout prodotto finito" e "Letter of acknowledgement" il 07/08/2018',N'No',N'-',N'Alessandra Gallizio','2018-11-30 00:00:00',NULL,'2019-07-26 00:00:00',N'No',NULL,NULL),
    (N'18-30','2018-06-29 00:00:00',N'Eleonora  Mastrapasqua',N'Documentazione SGQ',N'Aggiornamento moduli MOD04,30 "MODULO CONTROLLO ASSAY PROTOCOL" per allineamento alla versione software InGenius 1.3 (aggiunta di controlli più dettagliati per la verifica degli assay protocol).',N'Allineamento alla versione software InGenius 1.3',N'Mancata registrazione di alcuni parametri degli Assay Protocol.',NULL,N'PVS
QC
QM
RDM',N'Ricerca e sviluppo
Sistema qualità',N'Impatto positivo: possibilità da parte di R&D e Val di controllare in modo adeguato gli Assay protocols sviluppati per la versione 1.3 del software InGenius
Impatto positivo: possibilità da parte di R&D e Val di controllare in modo adeguato gli Assay protocols sviluppati per la versione 1.3 del software InGenius
Impatto solo in termini di carico ore lavoro',N'29/06/2018
29/06/2018
29/06/2018',N'Aggiornamento dei moduli MOD04,30-xxx per allineamento alla versione software InGenius 1.3 (vedere allegato)
Messa in uso MOD04,30',N'moduli da modificare - cc18-30_allegato_modulidaverificare.docx
messa in uso MOD04,30_cq - cc18-30_sgq_messa_in_uso_modulo_mod0430_cq_rev.01_cc18-30_1.msg
messa in uso MOD04,30_int1 - cc18-30_messa_in_uso_mod0430_int1_sw_vers_1.3_rev01.msg
Messa in uso MOD04,30_int2 - cc18-30__messa_in_uso_mod0430_int2_01_versione1.3_cc18-30.msg
CC18-30_MOD04,30-3_MOD04,30-7_NON SONO DA AGGIORNARE IN QUANTO TRATTASI DI PRODOTTI POCO VENDUTI CHE VERRANNO AGGIORNATI ALL''OCCASIONE DI UNA MANUTENZIONE DI PRODOTTO - cc18-30_mod04,30-3_mod04,30-7_non_da_aggiornare.txt
messa in uso MOD04,30_CQ - cc18-30_sgq_messa_in_uso_modulo_mod0430_cq_rev.01_cc18-30.msg
messa in uso MOD04,30_INT1 - cc18-30__messa_in_uso_mod0430_int2_01_versione1.3_cc18-30_1.msg
messa in uso MOD04,03_INT2 - cc18-30__messa_in_uso_mod0430_int2_01_versione1.3_cc18-30_2.msg',N'RT
QMA',N'29/06/2018
29/06/2018',NULL,N'14/07/2020
14/07/2020',N'No',N'NA',N'No',N'NA',N'No',N'NA',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.10 - Fase di Verifica e Validazione: Reportistica',N'R&D1.10 NESSUN IMPATTO',N'No',N'NA',N'No',N'NA',N'No',N'NA',N'Eleonora  Mastrapasqua',NULL,NULL,'2020-07-14 00:00:00',N'No',NULL,NULL),
    (N'18-29','2018-06-06 00:00:00',N'Salvatore Patanè',N'Uso Previsto',N'Sostituzione della matrice Whole Blood per InGenius con software 1.3. La nuova matrice con il nuovo software implementa la possibilità di mixare il sangue durante l''estrazione da tubo primario. La matrice ha lo stesso nome della precedente in modo che venga sostituita a tutti gli assay che attualmente la utlizzano. La matrice verrà creata da R&D, messa in USO negli assay relativi da SGQ e sostituita presso i clienti da FPS.',N'Possibilità di estrarre con InGenius il sangue intero da tubo primario con il mixing del campione.',N'Il sangue verrebbe estratto da tubo primario senza mixing, con il rischio di separazione della fase plasmatica.',NULL,N'FPS
PVS
QMA
RT',N'Assistenza applicativa
Ricerca e sviluppo
Sistema qualità
Validazioni',N'uso della nuova matrice per gli assay in sviluppo su sangue intero
GLi assay già validati su sangue intero avranno questo step in più che è migliorativo rispetto ai test di validazione. 
Nessun impatto se non in termini di ore/attività
Il rischio che i clienti che usano già assay su sangue intero non abbiano l''aggiornamento della matrice insieme all''aggiornamento del software 1.3',N'06/06/2018
06/06/2018
06/06/2018
06/06/2018',N'Creare nuova matrice con assay parameter tool
nessuna attività a carico.
Mettere in uso la nuova matrice come template, sostituirla nella cartella degli assay che usano sangue intero già IN USO, aggiornare gli specialists sulla modifica da attuare presso i clienti.
Preparare una procedura di aggiornamento della matrice presso i clienti e registrazione degli strumenti in cui la sostituzione è stata effettuata.',N'Nuova matrice creata - samplematrix_whole_blood_wb.sam
Nella procedura di installazione (TSB036_REVa, IN USO IL 30/11/2018) è stata prevista una sezione specifica di aggiornamento della matrice. Il cambio viene fatto contestualmente all''aggiornamento della versione sw 1.3. - cc18-29_elite_ingenius_-tsb_036_-_rev_a_-_sw_version_1.3.0.12.pdf
vers1.2 e vers1.3 matrice WB - cc18-29_vers.1.3mixing_incompatibile_con_vers1.2.pdf
in uso matrice WBvers1.3 - cc18-29_sgq_messa_in_uso_matrice_wb_per_assay_protocol_cc18-29__r18-59.msg',N'RT
FPS
QMA',N'06/06/2018
06/06/2018
06/06/2018
06/06/2018',NULL,N'06/06/2018
06/06/2018
03/07/2018
11/12/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,'2018-12-11 00:00:00',N'No',NULL,NULL),
    (N'18-28','2018-06-06 00:00:00',N'Manuela  Consalvo',N'Processo di produzione',N'Si richiede una modifica della IO9,17 al Paragrafo 3.1 "Assegnazione dei lotti"  in cui si attribuisce il solo lotto Q al lotto di Controllo Qualità, mentre in realtà il lotto Q viene attribuito solamente nel caso in cui a questo lotto fanno capo più di un componente. Nel caso di un componente unico, al Controllo Qualità viene inviato un lotto P, U o D. ',N'Semplificazione nella tracciabilità e nell''attribuzione del lotto di Q.',N'Maggior complessità nel rintacciare i materiali presenti nel lotto Q e possibile assegnazione dei lotti di controllo qualità non uniforme.',NULL,N'MM
QC
QM',N'Controllo qualità
Produzione
Sistema qualità',N'Semplificazione nell''attribuzione del lotto Q e nella gestione degli OPR.
Semplificazione nell''attribuzione del lotto Q e nella gestione degli OPR.
Semplificazione nella tracciabilità dei materiali.
Nessun impatto.',N'06/06/2018
06/06/2018',N'Nesssuna attività.
Modifica IO09,17 e formazione
Messa in uso documenti.',N'formazione MOD18,02 17/09/2018 - cc18-28_messa_in_uso_io0917_rev02_cc18-21_nc18-116_oss3_report_audit_n418_cc18-28_mod1802_17092018.msg
IO09,17 rev02 del 17/9/18, in uso al 11/10/18 - cc18-28_messa_in_uso_io0917_rev02_cc18-21_nc18-116_oss3_report_audit_n418_cc18-28_mod1802_17092018_1.msg
IO09,17 rev02 in uso al 11/10/18 - cc18-28_messa_in_uso_io0917_rev02_cc18-21_nc18-116_oss3_report_audit_n418_cc18-28_mod1802_17092018_2.msg',N'MM
QC
QMA',N'06/06/2018
06/06/2018
06/06/2018',N'01/10/2018
15/10/2018',N'11/10/2018
11/10/2018
11/10/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'CQ - Controllo Qualità prodotti',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,'2018-10-15 00:00:00',NULL,'2018-10-11 00:00:00',N'No',NULL,NULL),
    (N'18-27','2018-05-17 00:00:00',N'Ferdinando Fiorini',N'Processo di produzione
Annullato / non approvato',N'Rivedere i tempi indicati sui Moduli di Produzione in conseguenza ai vari controlli introdotti nel tempo e alla crescita della dimensione dei lotti di produzione ANNULLATO RICHIESTO DI APRIRE UN''AZIONE CORRETTIVA LEGATA ALLA NC18-58',N'Maggior controllo e standardizzazione del processo produttivo',N'I tempi reali non corrispondono ai tempi indicati sui moduli',NULL,N'DS
MM
QARA
QM
RDM',N'Produzione
Regolatorio',N'ANNULLATO RICHIESTO DI APRIRE UN''AZIONE CORRETTIVA LEGATA ALLA NC18-58',NULL,N'ANNULLATO RICHIESTO DI APRIRE UN''AZIONE CORRETTIVA LEGATA ALLA NC18-58',NULL,N'QARA',N'08/08/2018',N'08/08/2018',N'08/08/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2018-08-08 00:00:00',NULL,'2018-08-08 00:00:00',N'No',NULL,NULL),
    (N'18-26','2018-05-17 00:00:00',N'Ferdinando Fiorini',N'Stabilità',N'Estendere la scadenza da 12 a 18 mesi del prodotto CRE ELITe MGB KIT (RTS200ING) sulla base dei dati di stabilità reale, come comunicato da R&D in data 26/04/2018',N'Aggiungere 6 mesi alla durata del prodotto',N'Restare con la scedenza di 12 mesi',NULL,N'MM
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'L''estensione della stabilità permette una produzione più efficiente e economica, riducendo i rischi di obsolescenza. Quindi l''impatto è sicuramente positivo.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.
Come dimostrato dai report di stabilità del prodotto RTS200ING (Come dimostrato dai rpeort di stabilità del prodotto RTS200ING (STB2017-091, lotto U00316AO -20°C per 18 mesi, STB2018-007, lotto U0416AU -20°C per 18 mesi, STB2018-029, lotto U0616BC -20°C per 18 mesi, STB2018-029, lotto U0616BC -20°C per 18 mesi) non c''è nessun impatto sulle prestazioni del prodotto.',N'17/05/2018
17/05/2018
17/05/2018
17/05/2018
17/05/2018
17/05/2018
17/05/2018',N'Rietichettare il lotto U0917BM cambiando la scadenza in 31/03/2019
Messa in uso documento modificato.
aggiornamento FTP
Aggiornamento del modulo di produzione MOD962-RTS200ING, modificando la frase "attribuire la scadenza del Prodotto (massimo 12 mesi)" in "attribuire la scadenza del Prodotto (massimo 18 mesi)".
Avvisare i distributori dove il prodotto è registrato.
Comunicare ai distributori che il prodotto è stato migliorato con la sonda OXA (SCP2018006), pertanto nell''attesa delle stabilità reali, il prodotto tornerà ad avere scadenza 12 mesi.
Nessuna altra azione a carico di R&D.',N'nuovo lotto a 18 mesi U0518BB - cc18-26_rietichettatura_lotto_u0917bm_con_estensione_scadenza.pdf
Con il progetto codice SCP2018-006 è stata aggiunta la sonda OXA, le stabilità reali del prodotto sono in corso, la scadenza del pordotto è 12 mesi. - cc18-26_messa_in_uso_moduli_cre_con_oxa_scadenza12mesi.msg
Il modulo è stato aggiornato con l''aggiunta della sonda OXA, la scadenza è a 12 mesi in attesa delle stabilità reali. - cc18-26_messa_in_uso_moduli_cre_con_oxa_scadenza12mesi_1.msg
informazioni aumento della scadenza ai distributori - cc18-26_mail_ai_distributori.pdf',N'PT
QMA
QMA
QC
QMA
RDM
QARA',N'17/05/2018
17/05/2018
17/05/2018
17/05/2018
17/05/2018
17/05/2018',N'30/07/2018
30/07/2018
30/09/2018
30/07/2018
30/06/2018
21/12/2018
17/05/2018',N'24/05/2018
06/09/2018
05/03/2021
06/09/2018
10/05/2018
05/03/2021
17/05/2018',N'No',N'NA',N'No',N'NA',N'No',N'na',N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',N'P1.5 - Assegnazione scadenza',N'1.5',N'No',N'na',N'No',N'na',N'No',N'na',N'Ferdinando Fiorini','2018-12-21 00:00:00',NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'18-25','2018-05-04 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede di modificare le modalità di utilizzo dei semilavorati MOD955-DiluentePlasmidi e 955-M300479 al fine di renderli omogenei agli altri semilavorati non critici. Nello specifico la richiesta è di attribuire un''utilizzabilità ad entrambi i semilavorati di 3 anni anzichè una scadenza di rispettivamente 6 e 3 anni.',N'I semilavorati utilizzati come diluenti risultano essere catalogati come non critici (nel caso specifico vedi i documetni SPEC950-125 e SPEc950-186)  pertanto non dovrebbero influire sulla scadenza finale del prodotto. Il fatto che tali semilavorati non influiscano sulla scadenza del prodotto finale è determinato dall''elevato periodo di scadenza (6 e 3 anni) mentre i prodotti finali hanno scadenza 24 e 30 mesi. 
La modifica permette di avere una attribuzione univoca per semilavorati con uguale livello di criticità. ',N'Il processo di produzione dovrebbe verificare che le scadenze di semilavorati non influiscano sul prodotto finito. Benchè tale ipotesi sia remota dato i 6 e 3 anni di scadenza dei diluenti plasmidi. Se il cambiamento non fosse implementato questi 2 semilavorati sarebbero delle eccezioni rispetto alla normale procedura di attribuzione dell''utilizzabilità.',NULL,N'MM
QC
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Tale modifica non impatta sulla scadenza del prodotto finale e il periodo complessivo del semilavorato 3 anni di utilizzabilità + 30 mesi massima di scadenza del prodotto finito non variano l''attuale disposizione della scadenza di 6 anni
Tale modifica non impatta sulla scadenza del prodotto finale e il periodo complessivo del semilavorato 3 anni di utilizzabilità + 30 mesi massima di scadenza del prodotto finito non variano l''attuale disposizione della scadenza di 6 anni
pianificazione della produzione del semilavorato tenendo conto di tale modifica. Valutare lo stato attuale delle scorte e la necessità o meno di riclassificazione. Il semilavorato non influenzerà più la scadenza finale del prodotto pertanto l''impatto complessivo è positivo.
Nessun impatto nel processo. 
nessun impatto sul processo ',N'04/05/2018
04/05/2018
16/05/2018
04/05/2018
04/05/2018',N'Nessuna azione a carico del R&D.
Valutare lo stato attuale delle scorte e la necessità o meno di riclassificazione. 
Modifica dei documenti MOD955-M300479 MOD955-DiluentePlasmidi e dei moduli di preparazione dei plasmidi e formazione
messa in uso dei documenti',N'messa in uso moduli - cc18-25_messa_in_uso_moduli_.pdf
messa in uso moduli, formazione MOD18,02 del 4/5/2018 - cc18-25_messa_in_uso_moduli__1.pdf',N'DS
QMA
RDM
MM',N'04/05/2018
16/05/2018
04/05/2018
04/05/2018',N'04/05/2018
30/06/2018
18/05/2018',N'04/05/2018
25/05/2018
25/05/2018
25/05/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'P3 - Preparazione / dispensazione / stoccaggio per Controlli Positivi / DNA Standard / DNA Marker',N'P3.5 - Assegnazione scadenza',N'nessun impatto',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2018-06-30 00:00:00',NULL,'2018-05-25 00:00:00',N'No',NULL,NULL),
    (N'18-24','2018-05-04 00:00:00',N'Renata  Catalano',N'Documentazione SGQ',N'Aggiornamento della procedure PR09,06 "Device History Records" e del modulo DHRI correlato',N'L''aggiornamento proposto prevede l''ottimizzazione e lo snellimento del processo di creazione e verifica del DHR',N'La mancata revisione del processo di creazione e verifica del DHR determina una minor efficacia del processo di design transfer e un prolungamento dei tempi necessari per il suo completamento',NULL,N'MM
QC
QM
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'Migliorativa in quanto la stesura del DHRI sarà effettuata da RT il quale ha la corretta visione della validazione da eseguire, inoltre la verifica sarà effettuata esclusivamente sui nuovi documenti e non sarà necessaria l''archiviazione elettronica . La copia cartacea dei documenti sarà fornita da PP. Potrebbe esserci un impatto negativo legato al mancato controllo dei moduli già in uso (per esempio i moduli comuni: diluenti ecc.).
Minor tempo richiesto nell''esecuzione della verifica del DHR dato il minor numero di documenti da verificare.
Mancata archiviazione dei DHR cartacei in Qualità in quanto la verifica verrà effettuata sulla modulistica originale archiviata nei Reparti di appartenenza (Produzione, Controllo Qualità,..) al termine del controllo.
L''impatto potrebbe essere negativo se non sono rispettate le tempistiche di effettuazione del controllo e la comunicazione al reparto che interviene nello step successivo.
Identificazione per ciascun prodotto della modulistica che si ritiene contr',N'17/07/2018
04/05/2018',N'verifica del nuovo flusso di creazione/verifica del DHR e del nuovo modello DHRI attraverso il suo utilizzo nel corso della produzione del prossimo lotto pilota
- revisione del nuovo modello di DHRI
- verifica del nuovo flusso di creazione/verifica del DHR e del nuovo modello DHRI attraverso il suo utilizzo nel corso della produzione del prossimo lotto pilota
- creazione della nuova versione della PR09,06. Messa in uso della PR09,06 + modello DHRI + formazione
- creazione del nuovo modello di DHRI
- verifica del nuovo flusso di creazione/verifica del DHR e del nuovo modello DHRI attraverso il suo utilizzo nel corso della produzione del prossimo lotto pilota
- verifica del nuovo flusso di creazione/verifica del DHR e del nuovo modello DHRI attraverso il suo utilizzo nel corso della produzione del prossimo lotto pilota
- revisione della nuova versione della PR09,06. ',N'revisione nuovo modello di DHRI - cc18-24_messa_in_uso_all-1_io0502_cc18-21_cc18-24_oss3_report_audit_9-18.msg',N'DS
QC
QCT
QM
QMA
RDM
MM
PP',N'17/07/2018
04/05/2018',N'30/06/2018
30/09/2018
30/06/2018
30/06/2018',N'10/01/2019',N'No',NULL,N'No',NULL,N'No',NULL,N'CS - Confezionamento (anche per CQ), Stoccaggio  e Rilascio al cliente del prodotto',NULL,N'CS13: valutare se il controllo aggiuntivo inserito nel 2015 ha dato esito positivo modificando la gestione della matrice del rischio.',N'No',NULL,N'No',NULL,N'No',NULL,N'Samuela Margio','2018-09-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'18-23','2018-04-20 00:00:00',N'Samuela Margio',N'Documentazione SGQ',N'Produzione di kit, per prodotti in corso di sviluppo, in formato ridotto forniti al cliente come DEMO.',N'Miglioramento nella fase di lancio del prodotto anticipando le DEMO con kit con un numero congruo di test.',N'Fornitura di kit DEMO in formato intero potrebbe pregiudicare l''acquisto del prodotto finito da parte del cliente.',NULL,N'OM
QM
SrPM',N'Budgeting control
Controllo qualità
Magazzino
Marketing
Produzione
Program mangment
Ricerca e sviluppo
Sistema qualità',N'Revisionare la PR04,01 per specificare in quale fase, in quale documento e la mansione responsabile di definire la quantità di kit DEMO. Formare il personale coinvolto.
Revisionare la PR04,01 per specificare in quale fase, in quale documento e la mansione responsabile di definire la quantità di kit DEMO. Formare il personale coinvolto.
Il personale del Program Managment gestisce la raccolta delle informazioni per la quantità di kit DEMO da produrre e la stesura del PLP.
Il personale del Program Managment gestisce la raccolta delle informazioni per la quantità di kit DEMO da produrre e la stesura del PLP.
Aggiornamento della procedura di produzione (PR09,01) per la descrizione della gestione dei kit DEMO (dati sul sistema gestionale (anagrafiche), e attività di  rilavorazione dei kit richiesti).
Aggiornamento della procedura di produzione (PR09,01) per la descrizione della gestione dei kit DEMO (dati sul sistema gestionale (anagrafiche), e attività di  rilavorazione dei kit richiesti).
Aggior',N'07/05/2018
07/05/2018
07/05/2018
07/05/2018
20/04/2018
20/04/2018
20/04/2018
07/05/2018
07/05/2018
07/05/2018
07/05/2018
23/04/2018
07/05/2018
07/05/2018
07/05/2018',N'Formare il personale che redige il design transfer plan ed il PLP sulla necessità di inserire la quantità di kit DEMO definita con PMS.
Revisionare la PR04,01 per specificare in quale fase, in quale documento e la mansione responsabile di definire la quantità di kit DEMO.
Gestione della raccolta delle informazioni per la quantità di kit DEMO da produrre e stesura del PLP.
stesura del PLP
Formazione al personale di produzione per la creazione delle anagrafica dei prodotti su NAV. MOD18,02 del 16/05/2018.
rilavorazione dei kit richiesti.
Aggiornamento della procedura di produzione (PR09,01) per la descrizione della gestione dei kit DEMO (dati sul sistema gestionale (anagrafiche), e attività di  rilavorazione dei kit richiesti).
Costificazione dei kit DEMO
Aggiornamento dell''istruzione operativa di creazione delle etichette (IO04,06), per specificare i contenuti dell''etichetta per kit DEMO.
Aggiornamento della procedura di identificazione e rintracciabilità (PR08,01).
mettere in uso i d',N'mod18,02 pr04,01 REV02 DEL 06/06/2018 - cc18-23_messa_in_uso_pr0401_rev02_e_documenti_allegati_rac18-2.msg
ESEMPIO PLP DEL PROGETTO SCP207-037, CON PARAGRAFO 7 "DEMO KIT IN REDUCED FORMAT" - cc18-23_plp2018-001_pag1scp2017-037.pdf
ESMPIO DESIGN TRANSFER PLAN DEL PROGETTO SCP2017-037, CON PARAGRAFO 5.5 "LOTTI PILOTA" NUMERO FORMATO KIT DEMO DA PRODURRE - cc18-23_plan2018-014_designtransferplan_pag1scp2017-037.pdf
PR09,01 rev03 - cc18-23_messa_in_uso_pr09,01_03_mod0937_mod0908_02_rac28-16_cc18-23_rap18-21_rac18-27_rac18-35_rac19-20.msg
pr04,01 REV02 5/02/2018 - cc18-23_messa_in_uso_pr0401_rev02_e_documenti_allegati_rac18-2.msg
MOD962-DEMO rev00 - cc18-235_messa_in_uso_mod962-demo_rev00.pdf
MOD962-DEMO rev00. MOD18,02 del 16/5/18. - cc18-235_messa_in_uso_mod962-demo_rev00_1.pdf
PLP FIRMATO DAL PERSONALE DEL MKT - cc18-23_plp2018-001_pag1scp2017-037_1.pdf
IL mod09,08 NON è STATO AGGIORNATO PER LE RILAVORAZIONI PER KIT DEMO. I KIT DEMO SONO STRUTTURATI NEL MOD962-DEMO - mod09,08_01_schedarilavorazioneprodottofinito.pdf',N'RDM
PMS
PMS
MM
PT
BC
QM
QM
MM
RDM
QMA
DS
QC
SrPM
GATL
SrPM
GATL
BC
QM',N'07/05/2018
07/05/2018
07/05/2018
20/04/2018
20/04/2018
07/05/2018
07/05/2018
07/05/2018
07/05/2018
23/04/2018
07/05/2018
07/05/2018',N'30/05/2018
30/05/2018
30/05/2018
30/06/2018
30/06/2018
30/06/2018
30/06/2018
30/06/2018
09/05/2018
30/05/2018
31/05/2018
30/05/2018',N'06/06/2018
14/06/2018
17/09/2018
16/05/2018
04/09/2019
17/09/2018
22/05/2018
17/09/2018
30/05/2018',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.1 - Fase di Pianificazione: Pianificazione',N'R&D1.1',N'No',N'non necessaria',N'No',N'nessun impattp',N'No',N'nessun impatto',N'Samuela Margio','2018-06-30 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'18-22','2018-04-19 00:00:00',N'Roberta  Paviolo',N'Organizzazione interna (es: organigramma livelli dirigenziali)',N'Cambio della sede legale di ELITechGroup SpA : dal 01/06/2018 la sede legale sarà trasferita a Milano Corso Italia 22.',N'NA',N'NA',NULL,N'CFO
QARA
T&CM',N'Acquisti
Gare
Regolatorio
Sistema informatico',N'Valutare a chi è necessario comunicare il cambio di sede legale, valutare come effettuare tale comunicazione, in che tempi e a chi affidare la responsabilità dell''invio in base ai settori interessati.
Valutare a chi è necessario comunicare il cambio di sede legale, valutare come effettuare tale comunicazione, in che tempi e a chi affidare la responsabilità dell''invio in base ai settori interessati.
Valutare la possibilità di un automatismo su Navision ( o altro) per la l''invio delle lettere informative di cambio della sede legale.
Estrarre dal registro fornitori attivi i fornitori cui si ritiene necessario inviare il cambio di ragione sociale.
Estrarre dal registro fornitori attivi i fornitori cui si ritiene necessario inviare il cambio di ragione sociale.
Comunicare il cambio di sede legale ai distributori che seguono la parte di registrazione dei prodotti ed all''organismo notifcato (DEKRA).
Comunicare il cambio di sede legale ai distributori che seguono la parte di registrazione dei prodott',NULL,N'Comunicare a tutti i ns. clienti attivi (in particolare ai pubblici con gare in corso) la variazione della sede legale
aggiornare il la sede legale (a partire dal 1 giugno) su tutti i portali (albi fornitori, sistemi telematici di gara, Me.Pa ecc).
Creare un automatismo su Navision ( o altro) per la l''invio delle lettere informative di cambio della 
Inviare le lettere di cambio di ragione sociale ai fornitori.
comunicazione del cambio di sede legale a DEKRA per il rilascio dei certificati con la nuova sede legale
Comunicazione del cambio di sede legale ai distributori che seguono la parte di registrazione dei prodotti EGSpA',N'lettere ai clienti attivi - cc18-22_lettere_ai_clienti_attivi.docx
esempio aggiornamento portale - esempio_aggiornamento_dati_sede_legale_portale_eprocurement_1.png
lettera ai fornitori - lettera_cambio_sede_legale_fornitori_en.pdf
lettera ai fornitori - lettera_cambio_sede_legale_amministrativa.pdf
mail dekra  - mail_dekra_cambio_sede_legale.msg
PR07,01 rev01 27/07/2018 - cc18-22_messa_in_uso_io0701_rev00,_legata_alla_pr07,01_rev01.msg
certificato DEKRA, mdsap n°2235223, del 31/01/2020 - 2235223_mdsap_iso_13485-2016.pdf',N'SAD-OP
SAD-TCO
SAD-TCO
ITM
PW
QM
QMA',NULL,NULL,N'01/06/2018
01/06/2018
01/06/2018
31/01/2020
31/01/2020',N'No',N'nessun impatto',N'Si',N'effettuata comunicazione',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'Si',N'effettuata comunicazione',N'Si',N'effettuata comunicazione',N'No',N'no',NULL,NULL,NULL,'2020-01-31 00:00:00',N'No',NULL,NULL),
    (N'18-21','2018-04-17 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede l''introduzione di 2 nuove categorie di codici interni per la gestione dei prodotti EGSpA confezionati ed etichettati per conto terzi (a marchio del cliente). Nello specifico i codici sono 963 per i prodotti finiti e 958 per i semilavorati.',N'I codici differenti permettono di distinguere e estrapolare facilmente i prodotti per conto terzi. Questo agevola il lavoro di BC nella selezione dei prodotti per la parte contabile e la produzione per identificare facilmente le categorie di prodotti .',N'L''attività di recupero dei dati per l''analisi contabile dovrebbe essere fatta per ciascun codice singolarmente. ',NULL,N'BC
SPM
MM
OM
QM',N'Budgeting control
Controllo qualità
Produzione
Sistema qualità',N'Impatto positivo in quanto permette di distinguere ed estrapolare facilmente i prodotti conto terzi.
Impatto positivo in quanto permette di distinguere e gestire facilmente i prodotti conto terzi.
Impatto positivo in quanto permette di distinguere e gestire facilmente i prodotti conto terzi.
Impatto positivo in quanto permette di distinguere e gestire facilmente i prodotti conto terzi.
Impatto positivo in quanto permette di distinguere e gestire facilmente i prodotti conto terzi.
Nessun impatto, se non in termini di attività/ore lavoro.
Nessun impatto, se non in termini di attività/ore lavoro.',N'16/05/2018
16/05/2018
17/04/2018
17/04/2018
17/04/2018
17/04/2018',N'Utilizzo dei nuovi codici per i prodotti conto terzi. Azione utile alla rintracciabilità su NAV.
Preparazione delle anagrafiche dei prodotti conto terzi tenendo conto dei nuovi codici.
 Modifica della PR09,01 per l''introduzione dei codici.
Inserimento nuovi codici nell''allegato ALL-1_IO05,02_01_CodificaDocumenti della IO05,02_01_CodificaeStrutturaDellaDocumentazione
Aggiornare in accordo con PMS l''istruzione operativa di autorizzazione al rilascio del lotto (IO09,07)
Vedi CC18-21_Codici_OEM_Action_Plan.xlsx',N'action plan - cc18-21_codici_oem_action_plan.xlsx
IO09,17 rev02 - cc18-21_messa_in_uso_io0917_rev02_cc18-21_nc18-116_oss3_report_audit_n418_cc18-28_mod1802_17092018.msg
nuove codifiche documenti per prodotti conto terzi - cc18-21_messa_in_uso_all-1_io0502_cc18-21_cc18-24_oss3_report_audit_9-18.msg
IO09,07rev03 in uso - cc18-21_messa_in_uso_io0907_rev03_rac17-35_cc18-21.msg
Aggiornamento PR10,01 ed allegato introducendo i codici oggetto del CC - cc18-21_messa_in_uso_pr1001_e_allegato_rap18-21_cc18-21_cc12-17_rac18-26.msg',N'DS
QM
QAS
BC
PT
MM
PMS
QM',N'28/06/2018
17/04/2018
17/04/2018
26/04/2018',N'30/07/2018
30/06/2018
01/08/2018',N'28/06/2018
28/06/2018
17/09/2018
06/05/2019
12/10/2018',N'Si',N'DMRI R-BIO',N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Federica Farinazzo','2018-08-01 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'18-20','2018-04-09 00:00:00',N'Stefania Brun',N'Documentazione SGQ',N'Descrivere i dettagli operativi delle attività gestite dalla procedura PR07,01 "Gestione sistema informatico" in istruzioni operative',N'Esplicitare all''interno del sistema qualità attività che all''interno della PR07,01 sono richiamate. Consentire a tutto il personale non solo quello del Sistema Informatico di fruire delle policy aziendali in tema di IT',N'Perdita di informazioni in quanto la PR07,01 non dettaglia a sufficienza le policy in tema di Information Technology System',NULL,N'CFO
QARA
ITM',N'Sistema informatico
Sistema qualità',N'Positivo in quanto si andranno a dettagliare le policy aziendali in tema di IT
codificare le istruzioni operative
codificare le istruzioni operative',N'09/04/2018
09/04/2018
09/04/2018',N'Trasferire e approfondire i capitoli dell''Information Technology in istruzione operative
codificare le istruzioni operative
mettere in uso le istruzioni operative',N'IO07,01_00 - cc18-20_messa_in_uso_io0701_rev00,_legata_alla_pr07,01_rev01.msg
 IO07,02, IO07,03, IO07,05_00, IO07,01_01 _MOD18,02 del 30-7-19 - cc18-20_messa_in_uso_pr0701_istruzione_e_moduli_allegati_mod1802_del_30072019.msg
IO07,06_00 - cc18-20_messa_in_uso_io0706_regolamento_aziendale_strumenti_informatici.msg
IO07,01_00 - cc18-20_messa_in_uso_io0701_rev00,_legata_alla_pr07,01_rev01_1.msg
IO07,02, IO07,03, IO07,05_00, IO07,01_01 - cc18-20_messa_in_uso_pr0701_istruzione_e_moduli_allegati_mod1802_del_30072019_1.msg
IO07,06_00 - cc18-20_messa_in_uso_io0706_regolamento_aziendale_strumenti_informatici_1.msg
Onsegna del regolamemto aziendale degli strumenti informatici al personale EGSpA per informazione/formazione e riconsegna firmata. - cc18-20_formazione_io07,06_00_egspa_tutti_pubblicate_nuove_istruzioni_reparto_it.msg',N'ITT
QM
ITM
QM
QMA
QMA',N'09/04/2018
09/04/2018
09/04/2018',N'30/05/2018
09/04/2018
30/05/2018',N'16/10/2019
16/10/2019
23/10/2019',N'No',N'nessun impatto',N'No',N'la modifica non riguardai i prodotti',N'No',N'la modifica non riguardai i prodotti',N'ALTRO - Altro',N'ALTRO - ALTRO',N'na',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'No',N'la modifica non riguarda i prodotti',NULL,'2018-05-30 00:00:00',NULL,'2019-10-23 00:00:00',N'No',NULL,NULL),
    (N'18-19','2018-03-29 00:00:00',N'Silvia Costa',N'Componenti principali',N'Al fine della estensione d''uso  del prodotto Aspergillus RTS110PLD su InGenius, è necessario creare il Controllo Positivo associato, con il codice CTR110PLD.
Il Controllo Positivo verrà prodotto utilizzando lo stesso materiale dello standard 10^5 del codice STD110PLD. Come per gli altri prodotti della serie ELITe MGB, il CTR110PLD dovrà essere dispensato in due tubi Sarstedt (volume riempimento 160 µl) e confezionato in una Nunc dedicata.',N'Rendere pienamente compatibile l''uso del kit con il sistema Elite Ingenius, allineando le caratteristiche con quelle previste dal sw della piattaforma. il sistema Ingenius prevede che le sedute di amplificazione siano validate con l''uso di un controllo positivo. ',N'Non sarebbe possibile estendere l''uso del kit su InGenius. Inoltre in occasione di ogni seduta bisognerebbe forzare il sistema usando il componente 10^5 del prdotto STD110PLD.',NULL,N'SPM
MM
QARA
QC
QM
RDM
SMD
SrPM',N'Assistenza applicativa
Budgeting control
Controllo qualità
Gestione vendite e mkt estero
Marketing vendite italia
Produzione
Program mangment
Ricerca e sviluppo
Sistema qualità
Validazioni',N'formazione / informazione dei clienti
formazione / informazione dei clienti
creazione anagrafica e costificazione
creazione/aggiornamento moduli e formazione (Document Specialist)
lancio del prodotto
produzione nuovo prodotto e inserimento distinta base in NAV
produzione nuovo prodotto e inserimento distinta base in NAV
approvazione etichette, registrazione al ministero della salute,messa in uso documenti, creazione del fascicolo tecnico, valutazione per registrazioni all''estero
approvazione etichette, registrazione al ministero della salute,messa in uso documenti, creazione del fascicolo tecnico, valutazione per registrazioni all''estero
approvazione etichette, registrazione al ministero della salute,messa in uso documenti, creazione del fascicolo tecnico, valutazione per registrazioni all''estero
approvazione etichette, registrazione al ministero della salute,messa in uso documenti, creazione del fascicolo tecnico, valutazione per registrazioni all''estero
validazione nuovo prodotto su In',N'20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018',N'formazione / informazione dei clienti
creazione anagrafica e costificazione
creazione/aggiornamento moduli e formazione (Document Specialist)
lancio del prodotto
produzione lotto
inserimento distinta base in NAV
approvazione etichette
messa in uso documenti (moduli, IFU, SDS).
formazione MOD962-CTR110PLD del 20/6/2018 (MOD18,02)
 creazione del fascicolo tecnico (analisi dei rischi di prodotto)
Registrazione al Ministero della salute e valutazione delle registrazioni EXTRACEE
validazione nuovo prodotto su InGenius
05/2019: a differenza di quanto stabilito nel V&V Master Plan del  21/05/2018, nel quale si indicava l''utilizzo del prodotto SP200 in associazione alla matrice BAL , si è valutato opprotuno utilizzare il prodotto SP1000. La valutazione è avvenuta sulla base di alcuni test di fattibilità, letteratura e in accordo e confronto con il centro di validazione (Ospedale S. Maria degli Angeli, Perdenone). Tutta la documentazione prodotta fino alla fase di validazoine non necessita di',N'Formazione assistenza applicativa, mkt e cq - cc18-19_formazione_ctr110pld_in_associazione_con_etsnsione_d_uso_su_ingenius.docx
CTR110PLD rev00 - cc18-19_sgq_messa_in_uso_mod962-ctr110pld_rev00_cc18-19.msg
primo lotto U0718AV - cc18-19_etichetta_primo_lotto__1.jpg
etichetta primo lotto U0718AV - cc18-19_etichetta_primo_lotto__2.jpg
MOD04,13_CTR110PLD - mod04,13_03_ctr110pld.pdf
CTR110PLD rev00 - cc18-19_sgq_messa_in_uso_mod962-ctr110pld_rev00_cc18-19_1.msg
SDS27 Revisione e data: 03, 25/10/2017 - sds27_positive_control_03.pdf
messa in uso IFU CTR110PLD - cc18-19_ifu_rts110pld_std110pld_ctr110pld__cc18-19.msg
FTP110pld, sezione 1 rev05 - sezione_1_product_technical_file_index_04.docx
CTR110PLD immesso in commercio il 19-12-2019 - mod05,05_real_time_elite_mgb_26.pdf
gantt CTR110PLD - cc18-19_ctr110pld_aspergillus_gantt.pdf
risk assessment CTR110PLD - cc18-19_risk_assessment_29-03-2018.docx
CTR110PLD - cc18-19_ifu_rts110pld_std110pld_ctr110pld__cc18-19_1.msg
DMRI2017-003_rev01 - cc18-19_dmri2017-003_primapag.pdf
AP in uso - cc18-19_ifu_rts110pld_std110pld_ctr110pld__cc18-19_2.msg
"Sample preparation: clinical samples_BAL" rev01 allegata al PRT2018-009 del 04/04/2019 - cc18-19_clinical_samples_bal_1000.doc',N'CSC
BC
DS
QC
GATL
PCT
PT
QARA
QM
QMA
QMA
QARA
PVS
SrPM
PMS
RT
RT
RT
PMS
PVC',N'20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
14/05/2019
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018
20/04/2018',N'28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018
28/09/2018',N'19/12/2019
26/06/2018
26/06/2018
19/12/2019
31/07/2018
26/06/2018
07/01/2020
23/12/2019
23/12/2019
12/12/2019
19/12/2019
14/05/2019
23/03/2020
07/05/2018
06/06/2018
23/12/2019
13/07/2018
23/12/2019',N'Si',N'Creazione DMRI CTR',N'No',N'non necessaria',N'No',N'nessun impatto, prodotto non ancora immesso in commercio',N'VAL - Validazione delle estensioni d''uso di prodotto',N'VAL1 - Fase di pianificazione: pianificazione',N'Sottoprocessi coinvolti VAL1',N'No',N'non ncessaria, in quanto prodotto non ancora immesso in commercio',N'No',N'nessun virtual manufacturer',N'No',N' prodotto in fase di sviluppo',N'Alessandra Gallizio','2018-09-28 00:00:00',NULL,'2020-03-23 00:00:00',N'No',NULL,NULL),
    (N'18-18','2018-03-14 00:00:00',N'Katia Arena',N'Materia prima',N'Il fornitore Axon ha segnalato ritardo nella consegna della merce ordinata a gennaio (ns ordine 18-PO-00089),  al fine di rendere disponibili alla vendita i prodotti OEM Fast Track RTS548ING e RTS533ING si è valutato possibile utilizzare i tubi Sarstedt (da 0.5 ml codice 953-216), attualmente usati per la dispensazione di enzimi e mix di primers e probe, e disponibili presso il magazzino.',N'I prodotti OEM Fast Track RTS548ING e RTS533ING  sono disponibili per la vendita.',N'Il ritardo nella fornitura dei tubi della National Scientific Supply ci avrebbe causato back order dei prodotti  RTS548ING e RTS533ING.',NULL,N'MM
OM
QARA
QC
RDM',N'Acquisti
Controllo qualità
Magazzino
Produzione
Program mangment
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Contattare altri fornitori per verificare la disponibilità dei tubi presso i loro magazzini.
Contattare altri fornitori per verificare la disponibilità dei tubi presso i loro magazzini.
Contattare altri fornitori per verificare la disponibilità dei tubi presso i loro magazzini.
Controllo al ricevimento del codice 953-234 (rif ns ordine 18-PO-00405 inviato a Fast Track): indicare sulla specifica il riferimento a questo CC.
Valutazione dell''utilizzo dei tubi sarstedt da 0.5 ml codice 953-216 in associazione con i tappi gialli (codice 953-218) in sostituzione del prodotto  953-233 NS-BS16YL-PS: BioStor Screw Cap  Vials 1.6ml Skirted, Tube : Nat Cap: YEL Assembled.
Valutazione dell''utilizzo dei tubi sarstedt da 0.5 ml codice 953-216 in associazione con i tappi viola (953-226) in sostituzione del prodotto 953-232 - NS-BS16VT-PS: BioStor Screw Cap  Vials 1.6ml Skirted, Tube : Nat Cap: VIO Assembled.
Valutazione dell''utilizzo dei tubi sarstedt da 0.5 ml codice 953-216 in associazione con i tappi gialli',N'14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018',N'Inviato ordine a Fast Track per acquisto tubi arancioni: 953-234 - NS-BS16OR-PS: BioStor Screw Cap  Vials 1.6ml Skirted, Tube : Nat Cap: ORA Assembled ( 18-PO-00405)
Vista la mancata disponibilità dei tubi presso il fornitore, rivalutare il quantitativo di materiale da acquistare al fine di evitare  back order.
Verificare che nel controllo al ricevimento del prodotto 953-234 sia riportato il riferimento a questo CC in quanto Fast Track non è tra i distributori indicati nella specifica.
La proposta di sostituzione dei tubi attualmente utilizzati per produrre i codici RTS548ING e RTS533ING con i tubi da 0.5 ml sterili Sarstedt si basa sull''assunzione che, ad oggi, utilizziamo questi tubi per la dispensazione dei nostri prodotti (enzimi e mix di primers e probe).
Stoccare un kit di ciascun prodotto per effettuare le prove di stabilità. STOCCATO LOTTO U0318AZ
Informare il personale di produzione sull''utilizzo del codice 953-216 in associazione con i tappi gialli (codice 953-218) e con i tappi viola (',N'controlli ricevimento tubi arancioni sarsted - cc18-18_spec953-234_controllo_al_ricevimentotubiarancionisarsted_18-po-00405.pdf
SPEC953-234, ordine PO-00405, riferimento CC18-18 - 953-234_18-po-00405.pdf
TUBI SARSTED NEL LOTTO U0318AZ - cc18-18_u0318az.pdf
TIBI SARSTED NEL LOTTO U05BP - cc18-18_u0518bp.pdf
nuovo arrivo tubo axon - cc18-18_nuovo_arrivo_axon_953-232-18-po-00548_tubo_tappo_viola.pdf
nuovo arrivo tubo axon - cc18-18_nuovo_arrivo_axon_953-233-18-po-00548_tubo_tappo_giallo.pdf
TUBU SARSTED NEL LOTTO U0219AU - cc18-18_u0219au.pdf
CQ CONFORME LOTTO U0318AZ - cc18-18_u0318az_1.pdf
CQ CONFORME LOTTO U0318BP - cc18-18_u0518bp_1.pdf
nuovo arrivo tubi axon - cc18-18_nuovo_arrivo_axon_953-233-18-po-00548_tubo_tappo_giallo_1.pdf
nuovo arrivi tabi axon - cc18-18_nuovo_arrivo_axon_953-232-18-po-00548_tubo_tappo_viola_1.pdf
aggiornamento cartelle eliminando CC18-18 - cc18-18_cartelle_aggiornate_senza_cc18-18.pdf
U0318AZ - cc18-18_u0318az.pdf
U0518BP - cc18-18_u0518bp.pdf
FTD non ha fornito indicazione sull''utilizzo all''interno dei propri prodotti dei tubi sarsted.  dal monitoraggio dell''utilizzo dei tubi sarsted nei nostri prodotti non sono emersi reclami o nc a carico degli stessi. - cc18-18_chiusuracc_1.txt
proposta a FTD utilizzo tubi sarsted - cc18-18_proposta_a_ftd_utilizzo_tubi_sarsted_nel_processo_di_produzione_.msg
FTD non ha fornito indicazione sull''utilizzo all''interno dei propri prodotti dei tubi sarsted.  dal monitoraggio dell''utilizzo dei tubi sarsted nei nostri prodotti non sono emersi reclami o nc a carico degli stessi. - cc18-18_chiusuracc.txt',N'PC
RDM
RDM
PW
PW
QMA
QM
PW
PMS
PW
RT',N'14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018
14/03/2018',N'31/03/2018
22/06/2018
14/03/2018
14/03/2018
22/03/2019
14/03/2018
14/03/2018
22/06/2018
22/03/2019
22/06/2018',N'22/03/2018
29/06/2018
14/03/2018
22/03/2018
23/05/2018
29/06/2018
29/06/2018
29/06/2018
13/11/2020
13/11/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'A - Gestione degli Acquisti',N'A1 - Pianificazione approvvigionamento materiali',N'A1',N'No',N'-',N'No',N'-',N'No',N'-',N'Katia Arena','2019-03-22 00:00:00',NULL,'2020-11-13 00:00:00',N'No',NULL,NULL),
    (N'18-17','2018-03-13 00:00:00',N'Sonia Mercurio',N'Documentazione SGQ',N'Si richiedono le seguenti modifiche ai moduli/elenchi "MOD09,23_07_Elenco_prodotti-Moduli-Diluizioni_03-10-17" e  "MOD10,18_13_ElencoModuliERiferimentiPerCQ" :
-	modificare il formato dall''attuale versione pdf al formato Excel (bloccato)
-	inserire delle colonne che riportino ulteriori informazioni, come ad esempio il numero di tubi per kit, il volume da dispensare per ogni tubino, se e quanti punti di diluizione devono preparare a uso CQ (per cui non è necessaria la richiesta), quanti tubi fornire di controcampione e PCQ
-	snellire la procedura di modifica del documento, questo perché le modifiche di questi documenti/elenchi sono molto frequenti, si potrebbe adottare ad esempio lo stesso procedimento usato per gli altri elenchi della qualità.
',N'Potendo sfruttare i filtri del formato Excel, la consultazione migliorerebbe sensibilmente rendendo più rapida la ricerca delle informazioni relative al prodotto. Si faciliterebbe notevolmente la compilazione dei moduli di produzione: tutte le informazioni richieste sarebbero elencate nelle nuove colonne, senza bisogno di cercarle su diversi moduli e in generale si ha un quadro più preciso di tutte le informazioni relative al prodotto. ',N'La versione pdf non permette di ricercare velocemente le informazioni richieste ma è necessario scorrere tutto il documento. ',NULL,N'DS
MM
QC
QM
QMA',N'Controllo qualità
Produzione
Sistema qualità',N'Valutazione con QC delle modalità di approvazione dei moduli/elenchi e valutazione dell''aggiornamento delle procedure. Approvazione del nuovo formato proposto.
DS: Modifica dei documenti e nuova gestione delle modifiche. QCT: miglioramento della consultazione dei documenti. QC: nuova modalità di approvazione dei documenti.
DS: Modifica dei documenti e nuova gestione delle modifiche. QCT: miglioramento della consultazione dei documenti. QC: nuova modalità di approvazione dei documenti.
PCT: miglioramento della consultazione dei documenti. MM: nuova modalità di approvazione dei documenti.
PCT: miglioramento della consultazione dei documenti. MM: nuova modalità di approvazione dei documenti.',N'27/03/2018
27/03/2018
27/03/2018
14/03/2018
14/03/2018',N'Valutazione con QC delle modalità di approvazione dei moduli/elenchi e valutazione dell''aggiornamento delle procedure. Approvazione del nuovo formato proposto.
Modifica dei documenti  "MOD09,23_07_Elenco_prodotti-Moduli-Diluizioni_03-10-17" e  "MOD10,18_13_ElencoModuliERiferimentiPerCQ".
 Formazione al personale utilizzatore.
nuova modalità di approvazione dei documenti.',N'MOD09,23 rev04 - cc18-17_messa_in_uso_moduli_1.pdf
mod09,23 rev04 - cc18-17_messa_in_uso_moduli_2.pdf
FORMAZIONE MOD18,02 DEL 5/7/2018, E 21/22/05/2018 - cc18-17__messa_in_uso_mod1018_rev14_cc18-17_1.msg
IO09,14rev05_MOD09,23 rev04 - cc18-17_messa_in_uso_moduli.pdf
MOD10,18rev14 - cc18-17__messa_in_uso_mod1018_rev14_cc18-17.msg',N'DS
QCT
MM
DS
QCT
QM',N'27/03/2018
27/03/2018
27/03/2018
14/03/2018',N'29/06/2018
30/06/2018
30/06/2018
30/06/2018',N'19/07/2018
19/07/2018
19/07/2018
06/06/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'CQ - Controllo Qualità prodotti',NULL,N'CQ5, P3.4',N'No',NULL,N'No',NULL,N'No',NULL,N'Sonia Mercurio','2018-06-30 00:00:00',NULL,'2018-07-19 00:00:00',N'No',NULL,NULL),
    (N'18-16','2018-02-21 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Aggiornamento delle istruzioni di installazione, IO11,12, e di ,manutenzione , IO11,13, per dettagliare che i relativi moduli  vengono divisi in  ceck list dedicate e separate dal nuovo modulo di PQ MOD11,26.',N'Creazione di un modulo di PQ utilizzabile a seguito di ogni intervento in cui è prevista/richiesta l''esecuzione della PQ stessa.',N'Impossibilità di utilizzare il modulo di PQ per interventi di service se non per installazioni o manutenzioni preventive.',NULL,N'GSC
QM',N'Assistenza strumenti esterni
Sistema qualità',N'Aggiornamento documentazione, formazione al personale interno, comunicazione ai distributori.
Aggiornamento documentazione, formazione al personale interno, comunicazione ai distributori.
Aggiornamento documentazione, formazione al personale interno, comunicazione ai distributori.
codifica nuovi documenti (IO che descrive come eseguire la Performance Qualification) e messa in uso documenti.',N'21/02/2018
21/02/2018
21/02/2018
21/02/2018',N'Aggiornamento documentazione
formazione al personale interno
comunicazione ai distributori.
codifica nuovi documenti (IO che descrive come eseguire la Performance Qualification) e messa in uso documenti.',N' IO11,12rev02, IO11,18rev00, MOD11,16rev02  - cc18-16_messa_in_uso_ioiq_e_io_pq.pdf
IO11,13_02 - cc18-16_messa_in_uso_io1113_cc18-16.msg
IO11,12rev02, IO11,18rev00, MOD11,16rev02 - cc18-16_messa_in_uso_ioiq_e_io_pq.pdf
IO11,13 rev02 - cc18-16_messa_in_uso_io11,13_cc18-16.msg
MOD11,27_00_17/07/2019 - mod11,27_00_elitech_elite_ingenius_preventive_maintenance_check_list.xlsx
 TSB012 revB IO11,12_02  - cc18-16_elite_ingenius_tsb_012_-_rev_b_-_installation_and_qualification_procedureio11,12_02_1.pdf
TSB013 revB IO11,13_02  - cc18-16_elite_ingenius_tsb_013_-_rev_b_-_preventive_maintenance_procedureio11,13_02.pdf
mail comunicazione ai distributori - cc18-16_mail_tsb012_io11,12.pdf
 comunicazione TSB013 revB IO11,13_02  - cc18-16_comunicazione_tsb_elite_ingenius_-_tsb_013_-_rev_b_-_preventive_maintenance_procedure.msg',N'GATL
QMA
GSC
GSC',N'21/02/2018
21/02/2018
21/02/2018
21/02/2018',N'30/04/2018
30/04/2018
30/04/2018
30/04/2018',N'25/07/2019
24/07/2019
31/07/2019
31/07/2019',N'No',N'nessun impatto sui DMRI',N'No',N'comunicazione ai distributori attraverso invio TSB012 revB e TSB013 revB',N'No',N'nessun impatto sul prodotto immesso in commercio',N'AST - Assistenza tecnica strumenti
AST - Assistenza tecnica strumenti',N'AST3 - Installazione e collaudo funzionale strumenti (IQ/OQ/VSE)
AST5 - Manutenzione preventiva (MP)',N'il profilo di rischio sul processo non è impattato dal cambiamento (moduifiche migliorative)',N'No',N'la modifica non è significativa in quanto sono aggiornate istruzoini interne',N'No',N'nessun virtual manufacturer',N'No',N'nessun impatto sull''analisi del rischio del prodotto',N'Marcello Pedrazzini','2018-04-30 00:00:00',NULL,'2019-07-31 00:00:00',N'No',NULL,NULL),
    (N'18-15','2018-02-20 00:00:00',N'Michela Boi',N'Etichette',N'Si richiede il cambiamento dei simboli di rischio riportati sulle etichette, sul manuale d''istruzioni per l''uso e sulle Schede Dati Sicurezza del kit di estrazione "ELITe InGenius SP200" (ref. INT032SP200). A seguito di un esame approfondito condotto dal fornitore del prodotto INT032SP200 (PSS) e da noi condiviso, in base alle sostanze contenute all''interno del kit, sono state modificate e/o aggiunte le frasi di Rischio e Prudenza associate all''uso del prodotto e di conseguenza sono cambiati i simboli di rischio associati al prodotto. In particolare, il simbolo di rischio "Corrosive" deve essere sostituito dal simbolo di rischio "Health Hazard".',N' L''utilizzo dei simboli di rischio sulle etichette, sul manuale e sulle SDS permette di essere conformi alla Regolamentazione e di fornire all''utilizzatore finale le informazioni necessarie per conoscere la pericolosità del prodotto per il suo maneggiamento.',N'Documentazione obsoleta.',NULL,N'QARA',N'Regolatorio
Sistema qualità',N'Modifica dei simboli di rischio riportati sulle etichette,  SDS n°80, e manuale del prodotto "ELITe InGenius SP200" ref. INT032SP200
Modifica dei simboli di rischio riportati sulle etichette,  SDS n°80, e manuale del prodotto "ELITe InGenius SP200" ref. INT032SP200',N'22/02/2018',N'Modifica dei simboli di rischio riportati sulle etichette,  SDS n°80, e manuale del prodotto "ELITe InGenius SP200" ref. INT032SP200',N'SDS N° 80, 81, 82 rev02 del 31/05/2018 - cc18-15_messa_in_uso_sds_n°_80,81,82_rev02.pdf
INT032SP200 rev05 in uso - cc18-15_ifu_int032sp200_-_cc18-15.msg',N'QARA',N'22/02/2018',N'15/06/2018',N'30/04/2019',N'No',NULL,N'No',NULL,N'No',NULL,N'ET - Gestione Etichette',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Michela Boi','2018-06-15 00:00:00',NULL,'2019-04-30 00:00:00',N'No',NULL,NULL),
    (N'18-14','2018-02-20 00:00:00',N'Alessandra Gallizio',N'Documentazione SGQ',N'Si richiede di modificare i nomi dei kit OEM RTK561ING, RTK562ING, RTK573ING, RTK574ING, in fase di acquisizione da parte di R-Biopharm, per evitare fraintendimenti con i kit già a catalogo. La dicitura "Enteric", dove presente, verrà sostituita con la dicitura "Gastrointestinal".
A seguito del cambiamento dei nomi dei kit si richiede che la stesura di un nuovo P&L Plan document, il PLP2018-002, comune ai quattro prodotti, sostituisca i documenti già approvati PLP2017-004 (Norovirus) e PLP 2017-007 (Enteric Bacterial).',N'Il cambiamento nel nome dei prodotti rende più chiara la loro identificazione in caso di abbreviazione del nome (ad esempio nel caso dei componenti o assay protocol).
La modifica del P&L Plan è necessaria perché è il documento di riferimento utilizzato da R-Biopharm per la preparazione (dispensazione e confezionamento) dei kit per ELITech.
Non si ritiene necessaria la modifica del Marketing requirement document.',N'In caso di abbreviazione del nome del prodotto Enteric Viral ELITe Panel in EV ELITe Panel si potrebbe generare confusione con il kit Enterovirus',NULL,N'SPM
QARA
QM
SrPM',N'Marketing
Program mangment
Regolatorio
Sistema qualità',N'A seguito del cambiamento dei nomi dei kit, stesura di un nuovo P&L Plan document, il PLP2018-002, comune ai quattro prodotti, che sostituisca i documenti già approvati PLP2017-004 (Norovirus) e PLP 2017-007 (Enteric Bacterial).
Approvazione del PLP',N'22/02/2018
22/02/2018',N'A seguito del cambiamento dei nomi dei kit, stesura di un nuovo P&L Plan document, il PLP2018-002, comune ai quattro prodotti, che sostituisca i documenti già approvati PLP2017-004 (Norovirus) e PLP 2017-007 (Enteric Bacterial).
Approvazione del PLP',N'PLP2018-002 - cc18-14_plp2018-002_primapag.pdf
PLP2018-002 - cc18-14_plp2018-002_primapag_1.pdf',N'PMS
QARA',N'22/02/2018
22/02/2018',N'30/04/2018
30/04/2018',N'23/04/2018
23/04/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio','2018-04-30 00:00:00',NULL,'2018-04-23 00:00:00',N'No',NULL,NULL),
    (N'18-13','2018-02-13 00:00:00',N'Alessandro Bondi',N'Etichette',N'Sulle etichette dei tubi di tutti i reagenti ELITechGroup associabili a ELITe InGenius inserire un codice a barre compatibile con il SW ELITe InGenius in modo tale che riconsca in maniera automatica il prodotto migliorando l''automazione del flusso e della tracciabilità del tubo. Il codice a barre deve contenere le informazioni per permettere la scansione automatica del lotto, scadenza, del numero seriale del tubo e delle reazioni rimanenti durante la fase di impostazione della seduta. ',N'Aumento della soddisfazione del clienti grazie all''automazione della tracciabilità del tubo, alla semplificazione del flusso di lavoro ed alla preparazione, da parte degli operatori, delle sedute di lavoro.
Risposta a segnalazione 103-17 dell''Ospedale Niguarda ed a feedback ricevuti dal campo.
Migliore gestione del rischio prevedibile di riportare manualmente in modo errato il numero di lotto.',N'Mantenimento della probabilità prevedibile di riportare manualmente in modo errato il numero di lotto (es: riportare la lettera O anziché il numero 0). ',NULL,N'SPM
GSC
MM
OM
QARA
QM
RDM
ITM
GATL',N'Assistenza applicativa
Marketing
Marketing vendite italia
Produzione
Program mangment
Regolatorio
Ricerca e sviluppo
Sistema informatico
Sistema qualità
Validazioni',N'Impatto positivo sul cliente che non dovrà più inserire manualemnte i dati, migliorando l''automazione e riducendo i potenziali casi di errore. Lo specialist avrà a disposizione una migliore traccibilità dei lotti.
Impatto positivo sul cliente che non dovrà più inserire manualemnte i dati, migliorando l''automazione e riducendo i potenziali casi di errore. Lo specialist avrà a disposizione una migliore traccibilità dei lotti.
Definizione della veicolazione del nuovo messaggio e cambiamento
Impatto positivo in  quanto si rende operativa una funzionalità del SW già presente nella versione 1.2, ma non applicabile per la mancanza fiscia del barcode sulle etichette. Se l''implementazione di tale modifica su MMix avrà esito positivo da parte dei clienti si valuterà l''implementazione anche su CTR e STD e IC in una futura versione del sw.
Da verificare un possibile impatto sui tempi di produzione in quanto il barcode presente sulle etichette potrebbe rallentarne la stampa.
L''impatto sul reparto Prod',N'27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018',N'Formazione agli specialist e comunicazione a clienti e distributori. 
Aggiornamento presso i clienti di tutti i file dei reagenti  associati agli assay protocol dell''InGenius.
Gestire il cambiamento coordinandosi con IT e R&D per la verifica delle funzionalità e compatibilità del cambiamento
Verifica dei requisiti definiti per l''implementazione del codice a barre sull''etichetta e verifica, in accordo con IT, dei dati contenuti nella stampa delle etichette.
 Eventuale aggiornamento dell''istruzione operativa di compilazione delle etichette. 
formazione del personale
Definizione, in accordo con QM e ITM, dei controlli di processo necessari.
Definire il processo di verifica/validazione delle nuove etichette con barcode, in accordo con QM e IT. Definire quali saranno i controlli da introdurre sui lotti di produzione e coordinare l''aggiornamento del VMP nei sotto-processi interessati. 
Verificare che, con l''aggiunta del barcode "pdf417", siano mantenuti i contenuti obbligatori delle etichette. 
',N'Etichetta che avverte il cliente della possibilità di utilizzare la lettura del datamatrix su InGenius. - cc18-13_etichettaverdexclienti_prodottietempi.msg
pianificazione attività al 22/2/2019 - cc18-13_attivita_validazione_processo_stampa_etichette_leggibili_da_ingenius_cc18-13.msg
Gantt - cc18-13_gantt_.xlsx
PLAN2018-044 - plan2018-044_reagent_barcode_validation_and_verification_plan_on_ingenius_sw1.3_code_int030_cc18-13_00_signed.pdf
PLP2019-002 - plp2019-002_labeling_plan_mmix_and_ctrcpe_with_datamatrix_barcode_cc18-13_rev00_signed.pdf
 PLP2019-002  - plp2019-002_labeling_plan_mmix_and_ctrcpe_with_datamatrix_barcode_cc18-13_rev00_signed_1.pdf
L''inserimento del codice a barre DTAMATRIX al posto del Q-RCODE sulle etichette dei tubini non comporta alcuna operazione differente in fase di stampa dal parte del personale di produzione. - cc18-13_nessunaggiornamentoxleattivitadiproduzione.txt
personle informato sulla presensa del datamatrix al posto del q-rcode sulle etichette dei tubini - cc18-13_etichettaverdexclienti_prodottietempi_2.msg
 PLP2019-002  - plp2019-002_labeling_plan_mmix_and_ctrcpe_with_datamatrix_barcode_cc18-13_rev00_signed_2.pdf
Piano validazione processo - mod09,24_00_pianodivalidazionestampaetichette_leggibili_da_swingenius_19-12-19.pdf
piano verifica e validazione del datamatrix su InGenius - plan2018-044_reagent_barcode_validation_and_verification_plan_on_ingenius_sw1.3_code_int030_cc18-13_00_signed.pdf
PLP: nuove etichette     interne con datamatrix - plp2019-002_labeling_plan_mmix_and_ctrcpe_with_datamatrix_barcode_cc18-13_rev00.pdf
non sono necessari controlli di processo aggiuntivi. - cc18-13_nonsononecessaricontrollidiprocessoaggiuntivi_1.txt
Messa in uso sw NAV2018, SW1, bartender2016, sw3 e reagent setupdater, SW42 - cc18-13_mailmessainusosw42.pdf
Report di verifica e validazione sulla lettura da parte dello strumento Elite InGenius delle nuove nuove etichette con datamatrix - rep2019-013_reagent_barcode_validation_and_verification_report_on_ingenius_sw1.3_code_int030_cc18-13_00_signed.pdf
AP con reference barcode in uso - cc18-13_progetto_etichette_2d_per_ic_e_mmix_apinusoconreferencebarcode_1.msg
Rapporto di verifica e validazione del processo di stampa etcihette (compilazion dei MOD04,13 per tutte le nuove etich prodotte) - mod09,12_01_rapportovalidazionestampaetichette_leggibili_da_swingenius-26-02-2020_1.pdf
comunicazione a MKT della rappresentazione grafica di un aggiornamento attraverso apposizione bollino blu - cc18-13integrazione_attivita__al_11-03-19_bondi_progetto_assay_protocols_lis_alias__barcode.msg
Etichetta che avverte il cliente della possibilità di utilizzare la lettura del datamatrix su InGenius.  - cc18-13_etichettaverdexclienti_prodottietempi_1.msg
IO04,08 rev01  - cc18-13_messa_in_uso_pr0508_01_rap18-34_mod1802_90919_io0408_cc18-13_rac19-7_mod1802_9919.msg
messa in uso assay con reference template - cc18-13_installation_database_1.msg
Secondo la linea guida NBOG BPG 2014-3, paragrafo "Changes Labelling", l''aggiornamento del codice a barre non modifica l''uso previsto del prodotto , non lo limita e non è un cambiamento che necessita di approvazione da parte dell''ìorganisom notificato. Pertanto si può considerare una modifica non significtiva, non da notificare alle autorità compettenti. - cc18-13_modificanonsignificativa.txt
MESSA IN USO TAB012revAA reagent database update for automatic scan - cc18-13_messa_in_uso_tab012_aa_cc18-13.msg
Rapporto di verifica e validazione del processo di stampa etichette - mod09,12_01_rapportovalidazionestampaetichette_leggibili_da_swingenius-26-02-2020.pdf
non sono necessari controlli di processo aggiuntivi - cc18-13_nonsononecessaricontrollidiprocessoaggiuntivi.txt
Ap con reference barcode in uso - cc18-13_progetto_etichette_2d_per_ic_e_mmix_apinusoconreferencebarcode_2.msg
AP con reference barode in so - cc18-13_progetto_etichette_2d_per_ic_e_mmix_apinusoconreferencebarcode.msg
Installation database in uso - cc18-13_installation_database.msg
non sono necessari controlli di processo aggiuntivi - cc18-13_nonsononecessaricontrollidiprocessoaggiuntivi_2.txt',N'RT
SPM
MM
MM
MM
QARA
QM
QARA
QM
QMA
ITM
ITM
RDM
RT
RT
QM
SrPM
PVS
PVS
QMA
FPS
MM
ITM
MM
QM
ITM
MM
QM
ITM
MM
QM
ITM
PVS
RT
QMA
MM
QM',N'27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018
27/06/2018',NULL,N'20/01/2020
23/09/2020
07/01/2020
14/03/2019
20/01/2020
20/01/2020
23/09/2020
23/09/2020
14/03/2019
23/09/2020
20/01/2020
07/01/2020
15/01/2020
26/02/2020
26/02/2020
16/12/2019
14/01/2020
14/01/2020
26/02/2020
23/09/2020
14/01/2020
20/01/2020
14/01/2020
14/01/2020',N'No',N'nessun impatto',N'Si',N'da effttuare',N'No',N'nessun impatto',N'ET - Gestione Etichette
P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni
R&D - Svilupppo e commercializzazione prodotti',N'ET2 - Stesura dell''etichetta
P1.6 - Etichettatura semilavorati e componenti
R&D1.8 - Fase di Sviluppo: Reportistica',N'Valutare l''impatto sui seguenti RA:
- Et2 "stesura etichetta" e ET3 "approvazione etichetta" per verificare i contenuti per della composizione del codice a barre, 
- P1.6"etichettatura semilavorati e componenti" e CS12 "controllo per l''autorizzazione al rilascio del lotto al cliente" per l''introduzione di controlli aggiuntivi sulla verifica delle etichette stampate poste sui prodotti. 
Sotto-processi interessati dal cambiamento: R&D1.8 stesura assay protocol. ',N'No',N'non necessario, il codice a barre sull''etichetta interna (datamatrix) migliora la tracciabilità del pordotto, ma non contiene dati differenti dal precedente codice a barre (QRcode)',N'No',N'non necessario ',N'No',N'no',N'Francesco Gorreta',NULL,NULL,'2020-09-23 00:00:00',N'No',NULL,NULL),
    (N'18-12','2018-02-13 00:00:00',N'Alessandro Bondi',N'Confezione',N'Revisione per tutti i prodotti OEM da Fast Track Diagnostic con codice di riferimento RTS5xxING dei volumi di componenti necessari alla preparazione della MasterMix finale come riportato sul manuale: cit. per  il  calcolo  dei  volumi dei tre componenti è necessario  definire il  numero  di  reazioni  (N) della  sessione di  lavoro sommando i campioni clinici da testare più una reazione (quando si analizzano fino a sei campioni), o due reazioni (quando si analizzano più di sei campioni) come margine di sicurezza.
Si propone di definire una tabella unica di calcolo con aggiunta del 20%.
In questo modo il fattore limitante diventano i cicli di congelamento e scongelamento pari a un massimo di 8 per cui in ogni caso ridurrebbe l''utilizzo dei reagenti. 
Proposta di mitigazione: Suddivisione dei reagenti in 3 tubi per ciascun componente (totale 9 tubi) contenente volume sufficiente per 12 reazioni (+ volume morto = 185 uL) e trasformando i kit in quantità da 36 reazioni ciascuna.',N'Ottimizzazione dei consumi del kit anche in utilizzo in urgenza di cui i kit in oggetto sono destinati (Meningiti e malattie respiratorie). Diminuzione degli sprechi e possibilità a livello commerciale di diminuire il numero di kit da offrire a compensazione.
Risposta alla segnalazione 72-17 dell''ospedale Niguarda',N'Ad oggi i clienti lamentano il consumo eccessivo di reagenti in caso di un numero medio di reazioni per seduta inferiore a 6. Questo causa una necessaria compensazione a livello commerciale spesso di natura gratuita quindi con la conseguente diminuzione del guadagno.',NULL,N'CFO
MM
OM
PMS
PW
QARA
QM
RDM
SMD
GATL',N'Acquisti
Controllo qualità
Gare
Magazzino
Marketing vendite italia
Produzione
Program mangment
Regolatorio',N'acquisto materiale aggiuntivo (tubi, nunc, ecc) relativi ai prodotti OEM
Impatto sui documenti a supporto del controllo di qualità dei prodotti OEM e di produzione
Impatto sui clienti (soddisfazione, miglioramentom ecc) e risparmio sui kit offerti a titolo gratuito in compensazione
Impatto su gare esistenti per i prodotti OEM
Impatto su produzione e documentazione di produzione
Impatto volumetrico sul magazzino con l''eventuale aumento di numero di scatole prodotte
Definire eventuali impatti per i prodotti dell''elenco B  (STI) per cui il cambiamento si applicherebbe all''approvazione dell''ente notificato.
Definizione impatto su progetti di OEM attuali e futuri. 27/06/2018: allo stato attuale, per l''alto costi dei prodotti OEM, non sono in corso valutazioni su investimenti sul cambio di formato di tali prodotti. Laddove necessario è importante valutare una compensazione a livello commerciale degli scarti . Un formato più consono all''utilizzo presso il cliente è valutato solo nei casi di interna',NULL,NULL,NULL,NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,NULL,N'No',NULL,NULL),
    (N'18-11','2018-02-09 00:00:00',N'Katia Arena',N'Materia prima',N'Ricerca di un fornitore alternativo per il codice TF-350-L-R-S, puntali Axygen utilizzati con lo strumento Ingenius. ',N'Fornitore alternativo di puntali.',N'In caso di ritardo nella fornitura non è possibile intervenire.',NULL,N'SPM
OM
QM
RDM',N'Acquisti
Program mangment
Ricerca e sviluppo
Sistema qualità',N'Richiesta offerta a EGInc e offerta al distributore negli USA SCIENTIFIC SUPPLY & EQUIPMENT per il prodotto cod 302-12-152.
Richiesta offerta a EGInc e offerta al distributore negli USA SCIENTIFIC SUPPLY & EQUIPMENT per il prodotto cod 302-12-152.
Test e prove sullo strumento Ingenius per valutare equivalenza dei puntali.
Richiesta documentazione al produttore Corning che attesta equivalenza dei due prodotti.
Verifica delle informazioni dell''etichetta.',N'19/02/2018
19/02/2018
19/02/2018
19/02/2018
09/02/2018',N'Richiesta offerta al distributore negli USA SCIENTIFIC SUPPLY & EQUIPMENT per il prodotto cod 302-12-152.
Invio di un primo ordine Acquisto a EG per avere i puntali da testare
Test e prove sullo strumento Ingenius per valutare equivalenza dei puntali.
Richiesta documentazione al produttore Corning che attesta equivalenza dei due prodotti.
Verifica delle informazioni dell''etichetta.',N'Test equivalenza Filter Tips - cc18-11_filtertipsaxygen.pdf
dichiarazione Corning - 301-12-152_confirmation_letter_-_signed.pdf
offerta - tips-axy-elitech_012318jen.pdf
ordine EGInc - 18-po-00105.pdf',N'RT
PMS
PW
PW
QM',N'09/02/2018
09/02/2018
09/02/2018
09/02/2018
09/02/2018',NULL,N'13/11/2020
09/02/2018
07/03/2018
09/02/2018
09/02/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Katia Arena',NULL,NULL,'2020-11-13 00:00:00',N'No',NULL,NULL),
    (N'18-10','2018-02-09 00:00:00',N'Katia Arena',N'Materia prima',N'Si richiede di acquistare direttamente da Life Technologies gli enzimi di restrizione, in quanto il fornitore (Carlo Erba) fa parte dello stesso gruppo. I codici coinvolti, e le rispettive specifiche da aggiornare, sono i seguenti: 950-155, 954-154,950-166, 954119 e 954-123.',N'Possibilità di migliore contrattazione nell''offerta e i tempi di consegna migliori.',N'Mantenimento del rivenditore Carlo Erba il quale effettua consegne a 4 settimane e non garantisce prodotti con scadenza superiore ai 12 mesi.',NULL,N'MM
QC
QM
RDM',N'Acquisti
Ricerca e sviluppo
Sistema qualità',N'Richiesta offerta a Life Technologies e approvazione delle specifiche.
Aggiornamento delle specifiche richieste
Messa in uso documenti',N'16/02/2018
16/02/2018',N'Richiesta offerta a Life Technologies e approvazione delle specifiche.
Aggiornamento delle specifiche 950-155, 954-154,950-166, 954119 e 954-123.
Messa in uso documenti',N'offerta enzimi - d1903899__enzimi_restrizione.pdf
SPEC950-119_01, 950-123_01, 950-154_03, 950-155_01, 950-166_03 - cc18-10_messa_in_uso_specifiche_cc18-10.msg
SPECIFICHE IN USO - cc18-10_messa_in_uso_specifiche_cc18-10_1.msg',N'RT
PW
QMA',N'16/02/2018
16/02/2018
16/02/2018',N'31/03/2018
31/03/2018
31/03/2018',N'19/10/2018
19/10/2018
19/10/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'F - Gestione dei Fornitori',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Katia Arena','2018-03-31 00:00:00',NULL,'2018-10-19 00:00:00',N'No',NULL,NULL),
    (N'18-9','2018-02-05 00:00:00',N'Katia Arena',N'Materia prima',N'Estensione della scadenza del prodotto Low Glycerol Platinum Taq ( cod. EP147B006), da 24 mesi a 30 mesi, a seguito di comunicazione da parte del fornitore Thermo, dell''esito dei test di stabilità da loro eseguiti.',N'L''estensione della scadenza del prodotto ci permette di poter preparare ed inviare ordini per quel prodotto che coprono le ns esigenze di produzione per almeno 3/4 mesi e ci permette di poter dare ai ns prodotti una scadenza di 24 mesi, mentre generalmente la materia prima arriva con una scadenza di 23 mesi comportando l''assegnazione di una scadenza uguale o inferiore. Infatti i ns prodotti hanno scadenza di 24 mesi e se la materia prima ha una scadenza maggiore ( 30 mesi ) è possibile ridurre gli sprechi, attraverso ordini più grossi senza impatto sulla scadenza dei ns prodotti.',N'impossibilità di invio ordini che coprono le esigenze di produzione per un periodo più ampio ( almeno 3/4 mesi).',NULL,N'MM
OM
QC
QMA
RDM',N'Acquisti
Magazzino
Produzione
Ricerca e sviluppo
Sistema qualità',N'Verificare i cambiamenti segnalati dal fornitore nei successivi arrivi (verificare che siano in linea con quanto segnalato). A cambiamento avvenuto aggiornare le tempistiche di riordino della materia prima tenendo in considerazione che adesso ha un scadenza a 30 mesi.
Modificare la specifica relativa al prodotto 950-210 e 950-214, secondo le informazioni segnalate dal fornitore sull''estensione della scadenza. e valutare l''estensione della scadenza sui prodotti a stock.
Modificare la specifica relativa al prodotto 950-210 e 950-214, secondo le informazioni segnalate dal fornitore sull''estensione della scadenza. e valutare l''estensione della scadenza sui prodotti a stock.
Formazione sul cambiamento della specifica e verifica con il primo arrivo dei cambiamenti sull''etichetta e sul CoA
formazione sul cambiamento della specifica. MOD18,02 del 15/02/218
Valutazione del CC e messa in uso dei documenti.',N'09/02/2018
09/02/2018
09/02/2018
09/02/2018
09/02/2018
09/02/2018',N'Verificare i cambiamenti segnalati dal fornitore nei successivi arrivi (verificare che siano in linea con quanto segnalato). A cambiamento avvenuto aggiornare le tempistiche di riordino della materia prima tenendo in considerazione che adesso ha un scadenza a 30 mesi.
Valutare se estendere la scadenza anche ai prodotti che abbiamo attualmente in stock: aggiornare la specifica facendo riferimento a questo CC e di conseguenza aggiornare il ns sistema gestionale
aggiornare SPEC950-210 e 950-214, e formazione magazzino e formazione
verifica con il primo arrivo dei cambiamenti sull''etichetta e sul CoA
 messa in uso dei documenti.',N'00567853_00567095 - 950-210_14-po-01051_00567853_00567095_estensione_scadenza_cc18-9.pdf
00520373_00519421 - 950-210_17-po-00688_00520373_00519421_estensione_scadenza_cc18-9.pdf
00551851_00545575 - 950-210_17-po-00855_00551851_00545575_estensione_scadenza_cc18-9.pdf
00590280_00589710 - 950-210_17-po-01319_00590280_00589710_estensione_scadenza_cc18-9.pdf
00610350_00610031 - 950-210_17-po-01578_00610350_00610031_estensione_scadenza_cc18-9.pdf
primo lotto con scadenza a 30 mesi dalla Data di Fabbricazione (DoM) - 2018-03-30_950-210_18-po-00309_00633273_00633111_1.pdf
certificato estensione stabilità thermo - cc18-9_stability_coc_2018_01_25.pdf
SPEC950-210rev03, SPEC950-214 rev01. Formazine del 15/2/2018 (MOD18,02) - cc18-9_messa_in_uso_spec950-210,214.pdf',N'RDM
W
PW
RT
QMA',N'09/02/2018
09/02/2018
09/02/2018
09/02/2018
09/02/2018',N'31/03/2018
31/03/2018
31/03/2018
31/03/2018',N'30/03/2018
22/03/2018
23/02/2018
30/03/2018
23/02/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'R - Ricevimento / stoccaggio Materiali e Strumenti',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Katia Arena','2018-03-31 00:00:00',NULL,'2018-03-30 00:00:00',N'No',NULL,NULL),
    (N'18-8','2018-01-30 00:00:00',N'Alessandro Bondi',N'Uso Previsto',N'Utilizzo dei valori di Tm (temperatura di melting) per discriminazione ceppi ipervirulenti in C.difficile',N'Il dato fornirebbe un dato di presunzione di possibile discriminazione dei ribotipi considerati ipervirulenti (027) che ad oggi il kit non possiede',N'Il kit ha un minore impatto rispetto alla concorrenza [C.diff GenXpert Cepheid]',NULL,N'SPM
QARA
QC
RDM
S&MM-MDx
SMD
GATL',N'Assistenza applicativa
Controllo qualità
Gestione vendite e mkt estero
Marketing
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Nessun impatto.
Definire se il cambiamento possa fornire un miglioramento competitivo del prodotto attuale
Definire quali e quanti distributori potrebbero essere positivamente impattati dal cambiamento e definire i numeri relativi
Definire quali e quanti distributori potrebbero essere positivamente impattati dal cambiamento e definire i numeri relativi
E'' previsto un design transfer per C. difficile ELITe MGB Kit per produrre a Torino il kit: sarà applicato il lean packaging e la Tfi dovrà essere sostituita con la Taq. Quest''ultima modifica potrebbe portare ad un cambiamento nei valori delle Tm, quindi potrebbe essere necessario rivedere e rivalidare gli intervalli di temperatura per i ceppi normali e ipervirulenti.
Questo CC non potrà essere portato avanti fino all''ottenimento di tali dati ed alla verifica della riproducibilità delle temperatura di melting, sia a livello di lotto che di strumento.
04/06/2018 RDM Firma:
Analisi dei dati di validazione già a disposizione per il sistema  InGeni',N'31/05/2018',N'Nessun impatto.
I paesi su cui impatta maggiormente la modifica sono i distributori europei ed il Canada che hanno già iniziato a promuovere il kit. 
Richiesta alla cliente di autenticazione dei campioni iniziali (ceppi presi in analisi) e dell''analisi dei dati con il prodotto EGSpA.
Verificare la riproducibilità delle temperatura di melting sia a livello di lotto che di strumento
Analisi dei dati di validazione già a disposizione per il sistema  InGenius, analisi dei dati QCMD.
Stesura di u un report che analizzi i dati rilasciati dalla cliente utilizzatrice del prodotto EGSpA, e che definisca i range di temperatura di melting per la discriminazione dei ceppi.
Implementazione dell''IFU.
Analisi della progettazione di un assay protocol in grado di discriminare entrambe le tossine A e B (e non solo la positività ai geni delle tossine).
messa in uso IFU e assay protocol aggiornati
aggiornamento FTP (analisi dei rischi)',N' IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_3.msg
 IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_5.msg
 IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_6.msg
IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato. - cc18-8_ifu_m800358_and_m800373.msg
messa in uso specifiche  SCP2018-022 - cc18-8_messa_in_uso_spec-951-m800358_00_e_spec952-pcdifficile_cc18-8.msg
 IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_2.msg
Lettera di autorizzazione firmata con dati forniti dalla Dr.ssa Rupnik - scan.pdf
Lettera di presentazione scansionata - scan1.pdf
Annex A1 relativo alla Lettera di autorizzazione firmata - 2017_06_06_maribor_backup.bdb
 IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_1.msg
 IFU IN USO per SCP2018-022 "m.p. Taq MMIX Backup C. difficile ELITe MGB Kit", quanto richiesto da questo CC non è stato attuato.  - cc18-8_ifu_m800358_and_m800373_4.msg',N'PVS
QMA
QMA
QC
S&MM-MDx
RDM
GATL
PVS',NULL,NULL,N'31/05/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019
31/05/2019',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandro Bondi',NULL,NULL,'2019-05-31 00:00:00',N'No',NULL,NULL),
    (N'18-7','2018-01-25 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il  fornitore in OEM Fast Track ha implementato nel 2017 due Change Control relativi al prodotto Pool IC, bulk del prodotto ELITech IC500. Un change control nel 2018. I Change Control sono i seguenti:
 - CRN2017-006 (allegato 1): change of preparation of plasmid method (mini prep to maxi prep), maxi prep is in higher sterile conditions at supplier site; an additional new culture lot of S.equi was used; the change has no impact on the performance and stability of the product; date of closure: 8/3/17
 - CCR2017-006 (allegato 2): a plasmid containing S.equi target sequence is used instead of culture, in order to incease efficiency and reproducibility of detection; date of closure 27/12/17. 
Poiché il prodotto 500 Internal Control (IC500), a differenza del corrispondente prodotto di Fast Track, è validato su InGenius, è necessario valutare se i cambiamenti apportati da Fast Track possano causare modifiche alle sue performance quando utilizzato su InGenius.
Contestualmente si richiede di effettuare uno studio di fattibilità per lo sviluppo di un nuovo IC500 contenente IC2 (utilizzato nel CTRCPE di ELITech).',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol.
La modifica relativa al CCR2017-006 è stata effettuata a seguito di segnalazione da parte di ELITech riguardo a un problema di riproducibilità quando 500-Internal Control viene utilizzato su InGenius in abbinamento al kit Respiratory Bacterial. Per ovviare al problema, dovuto alla sedimentazione delle cellule, ELITech aveva modificato il processo di diluizione e dispensazione della bulk.
La modifica apportata da Fast Track dovrebbe ovviare al problema riscontrato con l''utilizzo della coltura di S.equi.
Lo studio di fattibilità del nuovo Internal Control servirà da input per l''apertura di un progetto di sviluppo.',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate, con le nuove caratteristiche, verrebbero utilizzate non sotto controllo.
Inoltre se non si sviluppasse un solo Internal Control sarebbe necessario estrarre due campioni (CSF) per effettuare i test Menigitis Viral e Meningitis Viral 2 (500-Internal Control) e Meningitis Bacterial (CTRCPE).',NULL,N'SPM
MM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (assay protocol), determinazione eventuali nuovi criteri CQ, modifica processo preparazione IC, sviluppo di un nuovo Internal Control con IC2 a seguito di esito positivo dello studio di feasibility.
Aggiornamento SPEC950-IC500 rev00 per uniformare il requisito di accettazione per tutti i target per cui è utilizzato.
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (assay protocol), determinazione eventuali nuovi criteri CQ, modifica processo preparazione IC, sviluppo di un nuovo Internal Control con IC2 a seguito di esito positivo dello studio di feasibility.
Aggiornamento SPEC950-IC500 rev00 per uniformare il requisito di accettazione per tutti i target per cui è utilizzato.
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (assay protocol), determinazione eventuali nuovi criteri CQ, modifica processo preparazione IC, sviluppo di un nuovo In',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'sviluppo nuovo Internal Control
valutazione dell''impatto del cambiamento di Fast Track sulle prestazioni del kit (allegato2)
studio di feasibility e successiva apertura SCP di un nuovo progetto.
Nessun impatto.
In data19/02/2020 si chiede l''aggiornamento del MOD962-IC500.
Si propone di adottare come TEMPO MASSIMO DI ESPOSIZIONE A TEMPERATURA AMBIENTE lo stesso tempo indicato per la dispensazione del CPE, cioè 40 minuti.
Inoltre si propone di aumentare il volume del ciclo a 50 mL, mentre per il CPE sono indicate aliquote di 30 mL; infatti il volume dispensato per ogni tubo si CPE è 160 µL, mentre per l''IC500 si dispensano aliquote di 450 µL, per cui  a parità di tempo si ottengono meno tubi e il tempo di dispensazione è quindi inferiore
Nessun impatto.
Valutare la necessità di notifica a DEKRA (ad esclusione dello studio di feasibility). 3/10/2018 IC500 è un prodotto collegato cui non è necessario effettuare notifica a DEKRA.
A seguito di rilascio della nuova Scheda Dati Sicurezza con ',N'allegato 1 - crn_2017-006_pool.4_ic_08032017.pdf
allegato 2 - ccr_2017-006_closed_27.12.2017.zip
valutazione R&D - cc18-3_valutazioner&d_5.pdf
CCR2018-012 non relativo prodotti egspa - cc18-7_ccr_2018-012_post_cab_non_relativo_prodotti_oem.msg
Studio per la modifica al processo di preparazione IC - cc18-7_pg1rep2018-033_performance_study_report_of_ft_internal_control_with_s.equi_plasmid_dna.pdf
IC500 modificato può essere usato in associazione con Menigitis bacterial e Ingenius come alternativa al CPE_internal control. - cc18-7_pg1rep2018-108_feasibility_report_meningitis_bacterial_in_association_with_modified_500-internal_control.pdf
MOD962-IC500_04 - cc18-7_messa_in_uso_mod962-ic500_cc18-7_rac19-20mod1802_14042020.msg',N'RDM
RT
RDM
RT
MM
QARA
QC
RDM
QMA
QCT/DS',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
30/08/2018',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'28/09/2018
28/06/2018
31/01/2018
16/04/2020
31/01/2018
03/10/2018
05/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'non applicabile',N'No',N'non neessaria',N'No',N'nessun virtual maunfacturer',N'No',N'-',NULL,'2018-01-31 00:00:00',NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'18-6','2018-01-25 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il  fornitore in OEM Fast Track ha implementato nel 2017 due Change Control relativi ai prodotti CAP PP2 e CAP2 e di conseguenza ha modificato la bulk dei nostri prodotti Respiratory Bacterial ELITe MGB Panel (RTS553ING) e Respiratory Bacterial-ELITe Positive Control (CTR553ING). I Change Control sono i seguenti:
 - CRN2017-015 (allegato 1): change of vector for Lpneu (CAP2); concentration is not changed; the change has no impact on the performance and stability of the product; date of closure: 11/4/17
 - CCR2017-037 (allegato 2): change of the provider and concentration modified (better sensitivity) of the PPmix (CAP PP2); date of closure 15/12/17.
Si richiede di implementare le modifiche apportate da Fast Track, dopo la valutazione del loro impatto sulle prestazioni e sulla documentazione dei prodottiRespiratory Bacterial ELITe MGB Panel (RTS553ING) e Respiratory Bacterial-ELITe Positive Control (CTR553ING). 
Poiché i prodotti Respiratory Bacterial ELITe MGB Panel e Respiratory Bacterial-ELITe Positive Control, a differenza dei corrispondenti prodotti di Fast Track, sono validati su InGenius, è necessario valutare se i cambiamenti apportati da Fast Track possano causare modifiche alle performance dei kit quando utilizzati su InGenius.',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol.',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate, con le nuove caratteristiche, verrebbero utilizzate non sotto controllo.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Per il cambiamento del vettore Lpneu non ci sono impatti (è stato rilasciato il lotto U1117AZ).
Per la nuova versione della PPmix è necessario analizzare il cambiamento e valutare l''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD). Eventuale modifica assay protocol.
Per il cambiamento del vettore Lpneu non ci sono impatti (è stato rilasciato il lotto U1117AZ).
Per la nuova versione della PPmix è necessario analizzare il cambiamento e valutare l''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD). Eventuale modifica assay protocol.
Per il cambiamento del vettore Lpneu non ci sono impatti (è stato rilasciato il lotto U1117AZ).
Per la nuova versione della PPmix è necessario analizzare il cambiamento e valutare l''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità,',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'Valutazione degli impatti sulle prestazioni del kit: analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD).
Analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD).
Modifica assay protocol non necessaria.
Nessun impatto.
implementazione del CC nella documentazione del file tecnico di prodotto
notifica a DEKRA',N'allegato 1 - crn2017_015_signed.pdf
allegato 2 - ccr_2017-037_closed_15.12.2017.pdf
 valutazione R&D  - cc18-6_valutazioner&d.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22.pdf',N'RDM
RT
QARA
RDM
RT
QC
QMA',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'28/09/2018
31/01/2018
31/01/2018
20/07/2020
20/07/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Alessandra Gallizio','2018-01-31 00:00:00',NULL,'2020-07-20 00:00:00',N'No',NULL,NULL),
    (N'18-5','2018-01-25 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il  fornitore in OEM Fast Track ha implementato nel 2017 tre Change Control relativi al prodotto EPA PC e di conseguenza ha modificato la bulk del nostro prodotto Meningitis Viral 2-ELITe Positive Control (CTR523ING). I Change Control sono i seguenti:
CRN2017-003 (allegato 1): change of supplier of plasmid for HAdV and HPeV; the change has no impact on the performance and stability of the product; date of closure: 8/3/17;
CRN2017-004 (allegato 2): change of vector (same supplier) for HPeV; the change has no impact on the performance and stability of the product; date of closure: 8/3/17;
CCR2017-052 (allegato 3): change of supplier from G to E for EV plasmid; Final concentration for HAdV increased to 10^7 (instead of 10^6); date of closure 20/10/17. 
               Si richiede di valutare le modifiche apportate da Fast Track, per quanto concerna il  loro impatto sulle prestazioni e sulla documentazione dei prodotti Meningitis Viral 2 ELITe MGB Panel (RTS523ING) e Meningitis Viral 2-ELITe Positive Control (CTR523ING) in associazione con InGenius. ',N'L''accettazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione degli impatti consentirà di effettuare tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Il prodotto non potrebbe più essere venduto.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'Il cambiamento del fornitore del plasmide per HAdV e HPeV ed il cambio del vettore per HPeV non hanno impatto sul prodotto EGSpA. 
Il cambio di fornitore per il plasmide EV ed il cambio di concentrazione sono documentati dal "Validation Report PC" (version 5 - 8/5/17) di FTD  (allegato3).  Gli impatti sul prodotto EGSpA saranno da verificare con il lotto di CQ.
Nessun impatto.
Gli impatti sul prodotto EGSpA saranno da verificare con il lotto di CQ.',N'31/01/2018',N'nessun impatto
Nessun impatto.
Verifica conformità lotti al CQ.',N'allegato 1 - crn_2017-003_epa.2_pc_08032017.pdf
allegato 2 - crn_2017-004_epa.3_pc_08032017.pdf
allegato 3 - ccr_2017-052_closed_20.10.2017.pdf
RTS523ING: U0619BF, U0219AU, U0818BE. CTR523ING: U0619BG, U0219BQ, U1118BB, U0818BC - cc18-5_esiti_conformi_cq.pdf',N'RDM
QM
QC',N'31/01/2018
31/01/2018',N'31/01/2018',N'28/09/2018
31/01/2018
10/07/2019',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio','2018-01-31 00:00:00',NULL,'2019-07-10 00:00:00',N'No',NULL,NULL),
    (N'18-4','2018-01-25 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il  fornitore in OEM Fast Track ha implementato nel 2017 un Change Control relativo al prodotto "Vesic PC", e di conseguenza relativo alla bulk del nostro prodotto Meningitis Viral-ELITe Positive Control (CTR507ING), ed ha richiesto l''apertura di un change control per il prodotto "Vesicular rash", e di conseguenza per la bulk del nostro prodotto Meningitis Viral ELITe MGB Panel (RTS507ING). 
I Change Control sono i seguenti:
1) CRN2017-002 (allegato 1):  change of supplier of plasmid for HSV2 of the Vesic PP; the change has no impact on the performance and stability of the product; date of closure: 8/3/17
2) CCR2017-071 (allegato 2): change of the HSV2 probe of the Vesicular rash; reason: ROX crosstalk due to HSV2 HEX probe; A JOE probe will be used instead. date of closure june 2018
3) CRN2018-022 (allegato3): correction of mismatches in VZV probe and in HSV2 reverse primer, and adaptation of concentrations.
Si richiede di implementare le modifiche apportate da Fast Track, dopo la valutazione del loro impatto sulle prestazioni e sulla documentazione dei prodotti Meningitis Viral ELITe MGB Panel (RTS507ING) e Meningitis Viral-ELITe Positive Control (CTR507ING). 
Poiché i prodotti Meningitis Viral ELITe MGB Panel e Meningitis Viral-ELITe Positive Control, a differenza dei corrispondenti prodotti di Fast Track, sono validati su InGenius, è necessario valutare se i cambiamenti apportati da Fast Track possano causare modifiche alle performance dei kit quando utilizzati su InGenius.',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate, con le nuove caratteristiche, verrebbero utilizzate non sotto controllo.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD). Eventuale modifica assay protocol. Il cambiamento del plasmide non influisce sui cambiamenti di prodotto (il lotto U0108BA è stato rilasciato).
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD). Eventuale modifica assay protocol. Il cambiamento del plasmide non influisce sui cambiamenti di prodotto (il lotto U0108BA è stato rilasciato).
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD). Eventuale modifica assay protocol. Il cambiamento del plasmide non influisce sui cambiamenti di prodotto (il lotto U0108BA è stato rila',NULL,N'valutazione degli impatti sulle prestazione del kit
Analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD (in corso di implementazione da parte di FTD, da valutare con l''invio dei report).
Aggiornamento assay protocol.
implementazione del CC nella documentazione del file tecnico di prodotto',N'allegato 1 - crn_2017-002_vesic.5_pc_08032017.pdf
allegato 2 - ccr_2017-071_post_cab_04.12.2017.pdf
allegato 3 - ccr_2018-022_post_cab_1.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_2.pdf
valutazione R&D - cc18-3_valutazioner&d_3.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_1.pdf
All FTD CE-IVD kits Unsupported Performance Claims for FTD CE-IVD kits - fsn-fa-2019-22_3.pdf',N'RDM
RT
RDM
RT
QMA',NULL,N'30/09/2018',N'20/07/2020
20/07/2020
20/07/2020
20/07/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'-',N'No',N'-',N'No',N'-',N'No',N'-',N'Alessandra Gallizio','2018-09-30 00:00:00',NULL,'2020-07-20 00:00:00',N'No',NULL,NULL),
    (N'18-3','2018-01-25 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il  fornitore in OEM Fast Track ha implementato nel 2017 un Change Control relativo al prodotto "Urscreen PP" e di conseguenza ha modificato la bulk del nostro prodotto STI ELITe MGB Panel (RTS533ING).  Il Change Control è il seguente:
CRN2017-005 (allegato 1):  primer and probe mix was optimized to reduce the C. trachomatis background; the change has no impact of sensitivity nor specificity; date of closure: 7/3/17
Si richiede di implementare la modifica apportata da Fast Track, dopo la valutazione dell''eventuale impatto sulle prestazioni e sulla documentazione del prodotto STI ELITe MGB Panel (RTS533ING). 
Poiché il prodotto STI ELITe MGB Panel (RTS533ING), a differenza del corrispondente prodotto di Fast Track "Uretritis Plus", è validato su InGenius, è necessario valutare se i cambiamenti apportati da Fast Track possano causare modifiche alle performance quando utilizzato su InGenius.',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate, con le nuove caratteristiche, verrebbero utilizzate non sotto controllo.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Regolatorio
Ricerca e sviluppo
Sistema qualità',N'Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD).
Analisi del cambiamento e valutazione dell''eventuale impatto sulle prestazioni del kit (analisi delle performance: sensibilità, specificità, stabilità attraverso l''analisi dei documenti di FTD).
Analisi degli esiti dei controlli qualità sul primo/primi lotto/i con le nuove bulk.
implementazione del CRN nella documentazione del file tecnico di prodotto
notifica a DEKRA',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'Valutazione dell''impatto sulle prestazioni del kit STI ELITe MGB Panel ( a seguito di analisi degli esiti dei controlli qualità sul primo/primi lotto/i con le nuove bulk).
Analisi dei documenti di FTD (release after modification FTD FB-144 versione 3, allegato 2)
Analisi degli esiti dei controlli qualità sul primo/primi lotto/i con le nuove bulk.
lotti U0617AX, U0917BB, U0118AQ, U0318AZ.
implementazione del CRN nella documentazione del file tecnico di prodotto
notifica a DEKRA',N'allegato 1 (CRN2017-005) - crn_2017-005_urscreen.4_pp_08032017.pdf
Valutazione R&D - cc18-3_valutazioner&d_1.pdf
Notifica - cc18-3_notice_of_change_eg_spa_rts533ing_28-09-18.pdf
release after modification - cc18-3_allegato2_ftd_fb-144,_release_after_modification.pdf
Valutazione R&D - cc18-3_valutazioner&d.pdf',N'RDM
RT
QARA
RDM
QC
QM
QMA',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018',NULL,N'28/09/2018
28/09/2018
08/05/2018
28/09/2018
28/09/2018',N'No',NULL,N'No',NULL,N'No',NULL,NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio',NULL,NULL,'2018-09-28 00:00:00',N'No',NULL,NULL),
    (N'18-2','2018-01-24 00:00:00',N'Alessandra Gallizio',N'Materia prima',N'Il fornitore in OEM Fast Track ha implementato nel 2017 due Change Control relativi ai prodotti FluRSV e FluRSV PC e quindi alle bulk dei nostri prodotti Respiratory Viral ELITe MGB Panel (RTS548ING) e Respiratory Viral-ELITe Positive Control (CTR548ING). I Change Control sono i seguenti:
CRN2017-001 (allegato 1): change of supplier for RSVa/b plasmid and change concentration of RSVa plasmid from 10^7 to 10^6 copies/ml to optimize Ct values; date of closure: 8/3/17
CCR2017-002 (allegato 2): change of the IC assay from BMV to Sequi and change of the RSVa probe to a new design; date of closure 27/06/17 
Si richiede di implementare le modifiche apportate da Fast Track, dopo la valutazione del loro impatto sulle prestazioni e sulla documentazione dei prodotti Respiratory Viral ELITe MGB Panel (RTS548ING) e Respiratory Viral-ELITe Positive Control (CTR548ING). 
Poiché i prodotti Respiratory Viral ELITe MGB Panel (RTS548ING) e Respiratory Viral-ELITe Positive Control (CTR548ING), a differenza dei corrispondenti prodotti di Fast Track, sono validati su InGenius, è necessario valutare se i cambiamenti apportati da Fast Track possano causare modifiche alle performance dei kit quando utilizzati su InGenius.',N'L''implementazione delle modifiche apportate da Fast Track è necessaria in quanto fornitore esclusivo delle bulk. Una attenta valutazione preliminare degli impatti consente di effettuare in anticipo sulla prossima produzione tutte le modifiche che si rendessero necessarie alla documentazione di produzione/progetto/CQ e IFU/Assay protocol',N'Nessuna disponibilità di bulk con le vecchie caratteristiche. Le bulk modificate verrebbero utilizzate non sotto controllo.',NULL,N'SPM
QARA
QC
QM
RDM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'Analisi del report di validazione di FTD (FTD FB-106_version13_EndControlPreproduction_ALLEGATO3) . Analisi del cambiamento e valutazione delle prestazioni, eventuale modifica assay protocol (L''assay protocol attualmente in uso per il prodotto Respiratory Viral ELITe MGB Panel (RTS548ING) è settato per rilevare l''IC BMV sul channel 5 (fluoroforo ATTO-647N) e del manuale d''uso. Con il cambio del target per l''IC l''Assay Protocol potrebbe dover essere modificato).
Analisi del report di validazione di FTD (FTD FB-106_version13_EndControlPreproduction_ALLEGATO3) . Analisi del cambiamento e valutazione delle prestazioni, eventuale modifica assay protocol (L''assay protocol attualmente in uso per il prodotto Respiratory Viral ELITe MGB Panel (RTS548ING) è settato per rilevare l''IC BMV sul channel 5 (fluoroforo ATTO-647N) e del manuale d''uso. Con il cambio del target per l''IC l''Assay Protocol potrebbe dover essere modificato).
Analisi del report di validazione di FTD (FTD FB-106_version13_EndControlPreproduction',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'Analisi del report di validazione di FTD (FTD FB-106_version13_EndControlPreproduction_ALLEGATO3) . Analisi del cambiamento e valutazione delle prestazioni, eventuale modifica assay protocol (L''assay protocol attualmente in uso per il prodotto Respiratory Viral ELITe MGB Panel (RTS548ING) è settato per rilevare l''IC BMV sul channel 5 (fluoroforo ATTO-647N) e del manuale d''uso. Con il cambio del target per l''IC l''Assay Protocol potrebbe dover essere modificato).
ASSAY PROTOCOL NONDA MODIFICARE, MODIFICARE IFU
In data 12/10/2018 è stato inviato un change control da FTD "CCR2018-215" (ALLEGATO 4) relativo all''aggiornamento del codice Flu/HRSV kit da 48 a 48.1, è perciò necessario aggiornare la SPEC950-RTS548ING rev00. 
implementazione dei CCR nella documentazione del file tecnico di prodotto
Analisi del primo lotto con la nuova bulk ed eventuale modifica del modulo di CQ.
Lotto U0318BC conforme, MOD10,12-RTS548ING-CTR548ING rev02 del 26/4/18, in uso al 8/5/18, MOD10,12-IC500 rev04 del 26/04/2018. MOD',N'ALLEGATO1 - cc18-2_allegato1_crn_2017-001_flursv.2_pc_08032017.pdf
ALLEGATO2 - cc18-2_allegato2_ccr_2017-002_closed_27.06.2017.pdf
valutazione R&D - cc18-3_valutazioner&d_2.pdf
messa in sch mRTS548ING rev01 - cc18-2_ifu_rts548ing_-_cc18-2.msg
MOD10,02-IC500rev04, MOD10,12RTS548ING-CTR548ING rev02 - cc_18-2.pdf
modify the reference number of the Flu/HRSV kit from 48 to 48.1 - cc18-2_allegato4_ccr_2018-125_post_cab.pdf
SPEC950--RTS548ING_01 - cc18-2_messa_in_uso_spec950-rts548ing_01_cc18-2.msg',N'RDM
QC
QM
RDM',N'31/01/2018
31/01/2018',NULL,N'11/10/2018
09/08/2019
17/07/2020
08/05/2018',N'No',N'non necessario',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessu virtual manufacturer',N'No',N'nessuno',N'Alessandra Gallizio',NULL,NULL,'2020-07-17 00:00:00',N'No',NULL,NULL),
    (N'18-1','2018-01-10 00:00:00',N'Daniele Salemi',N'Documentazione SGQ',N'In uscita dall''audit n° 10-17 è emersa la necessità di inserire l''intervento di manutenzione programmata esterna su FR/CR/CG/UC effettuato dal Fornitore qualificato sulla specifica di assistenza tecnica.',N'Corretta gestione delle manutenzioni ed efficace mantenimento dell''apparecchiatura',N'Gestione delle manutenzioni non a sistema',NULL,N'QM
TSS',N'Assistenza strumenti interni
Sistema qualità',N'Aggiornamento SPEC-ASS TEC FR/CR/CG/UC e aggiornamento Database
Aggiornamento SPEC-ASS TEC FR/CR/CG/UC e aggiornamento Database
Analisi dell''impatto dell''introduzione di un intervento aggiuntivo di manutenzione programmata sui risk assessment degli strumenti frigoriferi, congelatori ed ultracongelatori (codici ST13, ST17, ST18) e sul processo manutenzione strumenti (codice M). Aggiornamento del VMP ed eventualmente dei RA. Messa in uso specifica.',N'10/01/2018
10/01/2018
10/01/2018',N'Aggiornamento SPEC-CG-CR-FR-UC_00_SpecificheAssistenzaTecnicaRefrigeratori, rev00 inserendo la MPE, le prove richieste, la frequenza  e la documentazione rilasciata.
Aggiornamento del database con la tipologia di intervento MPE e la frequenza
Analisi dell''impatto dell''introduzione di un intervento aggiuntivo di manutenzione programmata sui risk assessment degli strumenti frigoriferi, congelatori ed ultracongelatori (codici ST13, ST17, ST18) e sul processo manutenzione strumenti (codice M). Aggiornamento del VMP ed eventualmente dei RA.',N'spec-cg-cr-fr-uc_rev01 - cc18-1_messa_in_uso_spec-cg-cr-fr-uc_rev01.pdf
La MPE serve per migliorare il monitoraggio dell''apparecchiatura, ma non è un fattore predominante nel ridurre i guasti, in quanto questi dipendono anche dall''usura dell''apparecchitura. I RA degli strumenti e della manutenzione non cambiano - mod09,27_01_modulo_di_risk_assessment_st13_frigorifero.pdf',N'TSS
TSS
QMA',N'10/01/2018
10/01/2018
10/01/2018',N'20/02/2018
12/03/2018
12/03/2018',N'30/01/2018
12/03/2018
12/03/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ST - Elenco famiglie Strumenti',NULL,N'Analisi dell''impatto dell''introduzione di un intervento aggiuntivo di manutenzione programmata sui risk assessment degli strumenti frigoriferi, congelatori ed ultracongelatori (ST13, ST17, ST18) e verifica dell''impatto sul processo di Manutenzioni strumenti (M).
12/03/2018 La MPE serve per migliorare il monitoraggio dell''apparecchiatura, ma non è un fattore predominante nel ridurre i guasti, in quanto questi dipendono anche dall''usura dell''apparecchitura. I RA degli strumenti e della manutenzione non cambiano ',N'No',NULL,N'No',NULL,N'No',NULL,N'Daniele Salemi','2018-03-12 00:00:00',NULL,'2018-03-12 00:00:00',N'No',NULL,NULL),
    (N'17-52','2017-12-22 00:00:00',N'Alessandra Gallizio',N'Documentazione SGQ',N'Following DEKRA feedback related to the submission of the Respiratory Bacterial ELITe MGB Panel (report n° 2221510-TDR10-R0) one  change in the Packaging and Labelling Plan PLP2017-001is needed. Observation TDR10/Obs02: "No information is presented for this product regarding the verification of the following aspects: 
-	Packaging has no adverse effect on medical device.
-	Compatibility with the labeling system
-	Physical, chemical and microbial protection
-	Protection during transport and storage
The required information was presented in reply to the review of the Notice of Change for CE marking of the STI ELITe MGB Kit RTS533ING and STI ELITe Positive Control CTR533ING and this information can also be applied for the product under review. Nevertheless ELITechGroup S.p.A. didn''t do so"',N'This change could better clarify that packaging, storage and shipping conditions of Respiratory Bacterial ELITe Panel and Respiratory Bacterial ELITe-Positive Control products are suitable for the intended use and are exactly the same which have been in place since November 2015 for a wide range of other similar EG SpA products and no complains and no findings have been reported from users so far.',N'If this change is not implemented, a doubt could be generated related to the appropriateness of the packaging and labelling of the product.',NULL,N'PMS
QM',N'Program mangment
Sistema qualità',N'approval of the new Packaging and Labelling Plan
draft of the a new revision of the Packaging and Labelling Plan document',N'22/12/2017
22/12/2017',N'to approve the new revision of the PLP2017-001. PLP2018-002, signature 06/03/2018.
to draft of the a new revision of the PLP2017-001 document. A new plan PLP2018-002 "Packaging and Labeling Plan Norovirus ELITe Panel, code RTK561ING, Gastrointestinal Viral ELITe Panel, code RTK562ING, Gastrointestinal Parasitic ELITe Panel, code RTK574ING, Gastrointestinal Bacterial ELITe Panel, code RTK573ING, SCP2017-010/011/012/013", signature 06/03/2018.
',NULL,N'QM
PMS',N'22/12/2017
22/12/2017',N'28/02/2018
28/02/2018',N'06/03/2018
06/03/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ET - Gestione Etichette',NULL,N'no impact',N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio','2018-02-28 00:00:00',NULL,'2018-03-06 00:00:00',N'No',NULL,NULL),
    (N'17-51','2017-12-18 00:00:00',N'Alessandra Gallizio',N'Etichette',N'Following the observation (TDR10/Obs03) from DEKRA related to the product Respiratory Bacterial ELITe MGB Panel (report n° 2221510-TDR10-R0) it is required to change the reference to the website on the external label of the products from www.elitechgroup.com/corporate/ifu-emd to www.elitechgroup.com/instructions-for-use/.',N'www.elitechgroup.com/instructions-for-use/ is the address where the IFU are uploaded. www.elitechgroup.com/corporate/ifu-emd redirects to the correct page',N'because the www.elitechgroup.com/corporate/ifu-emd (current reference) redirects to the correct page, there is no risk of error by the end user;  however in the current situation the external label doesn''t report the correct address where the IFU are uploaded.',NULL,N'QM
SrPM',N'Marketing
Marketing vendite italia
Sistema qualità',N'update of the label templates and approval forms of all the products that report the reference to the web site on the external labels.
update of the label templates and approval forms of all the products that report the reference to the web site on the external labels.
a better satisfaction of the customers
a better satisfaction of the customers.  NOT APPROVAL',N'21/12/2017
21/12/2017
21/12/2017
21/12/2017',N'modify the website address on the label templates in the Navision system for all the products that report the reference to the web site on the external labels.  NOT APPROVAL
create a new revision of the MOD04,13 form for each modified label template.  NOT APPROVAL
to evaluate if communicate the change to the customer
to evaluate if communicate the change to the customer.  NOT APPROVAL',NULL,N'QM
QARA
QM
SrPM
SrPM',N'21/12/2017
21/12/2017
21/12/2017
21/12/2017',N'30/03/2018
30/03/2018',N'30/01/2018
30/01/2018
30/01/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ET - Gestione Etichette',NULL,N'no impact',N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio','2018-03-30 00:00:00',NULL,'2018-01-30 00:00:00',N'No',NULL,NULL),
    (N'17-50','2017-12-18 00:00:00',N'Alessandra Gallizio',N'Manuale di istruzioni per l''uso',N'Following DEKRA feedback related to the submission of the Respiratory Bacterial ELITe MGB Panel (report n° 2221510-TDR10-R0) two changes in the IFU are needed. Question TDR10/Q04: it is required to better explain how the end user is informed that three consecutive runs need to be performed in the same day of reconstitution of the mono-reagent.  Question TDR10/Q10: it is required to clarify if there is the supportive evidence to include the detection of Legionella longbeachae in the Intended Use',N'The changes will better clarify some information contained in the IFU and will allow to obtain the CE mark from DEKRA',N'Some information contained in the IFU could be not so clear for the user and the CE mark would not be obtained.',NULL,N'SPM
QARA
QMA',N'Program mangment
Sistema qualità',N'involvment of the marketing department and draft of the new revision of the IFU
possibility to complete the submission of the product Respiratory Bacterial ELITe MGB Panel to DEKRA',N'21/12/2017
21/12/2017',N' draft of the new revision of the IFU
approval of the new revision of the IFU and submission to DEKRA (date of submission 22/12/2017).',N'IFU in uso - cc17-50_messa_in_uso_ifu_553ing.pdf',N'PMS
QARA
QMA',N'21/12/2017
21/12/2017',N'31/12/2017
28/02/2018',N'22/12/2017
09/02/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'IFU - Gestione Manuali',NULL,N'No impact',N'No',NULL,N'No',NULL,N'No',NULL,N'Alessandra Gallizio','2018-02-28 00:00:00',NULL,'2018-02-09 00:00:00',N'No',NULL,NULL),
    (N'17-49','2017-12-18 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Si chiede la modifica dell''IO10,06 "REQUISITI GENERALI DEL CONTROLLO QUALITÀ" a livello del paragrafo archiviazione Run e verifiche tecniche, al fine di aggiornare il processo di archiviazione delle verifiche tecniche e Run.',N'Migliorare il processo di archiviazione delle verifiche tecniche e Run, al fine di agevolare le attività di archiviazione e di tracciabilità.',N'L''individuazione delle sessioni per prodotto, risultano attualmente laboriose e lunghe. L''archiviazione per codice di prodotto consente un recupero rapido delle sessioni.',NULL,N'QC
QM',N'Controllo qualità
Ricerca e sviluppo
Sistema qualità',N'Modifica della IO 10,06 e implementazione dell''archiviazione elettronica dei file Run e Verifica tecnica per codice prodotto
Modifica della IO 10,06 e implementazione dell''archiviazione elettronica dei file Run e Verifica tecnica per codice prodotto
Approvazione del documento e messa in uso
Approvazione del documento',N'18/12/2017
18/12/2017
18/12/2017
18/12/2017',N'Modifica della IO 10,06 e implementazione dell''archiviazione elettronica dei file Run e Verifica tecnica per codice prodotto.
FORMAZIONE DEL 29/01/2018 (MOD18,02)
Approvazione del documento e messa in uso
Approvazione del documento e messa in uso',N'io10,06 rev03 - cc17-49_messa_in_uso_io10,06_rev03.pdf',N'QC
QM
RDM',N'18/12/2017
18/12/2017
18/12/2017',N'30/03/2017
30/03/2018
30/03/2018',N'30/01/2018
30/01/2018
30/01/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'CQ - Controllo Qualità prodotti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Federica Farinazzo','2018-03-30 00:00:00',NULL,'2018-01-30 00:00:00',N'No',NULL,NULL),
    (N'17-48','2017-12-13 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Come richiesto dalla OSS6 in uscita dall''audit non annunciato ai laboratori, si richiede di: implementare e migliorare la procedura di distribuzione dei DPI per il personale interno e fuori sede.
Modifica del MOD21,01 DISTRIBUZIONE DISPOSITIVI DI PROTEZIONE INDIVIDUALI, introduzione di una nuova IO per la gestione e di una scheda che illustri le indicazioni del DUVR per quel che concerne la relazione tra Rischi mansioni e DPI. Prevedere l''archiviazione elettronica del modulo di distribuzione, nella cartella del personale. 
',N'Migliorare la gestione della distribuzione e l''archiviazione dei documenti di distribuzione dei DPI. Introdurre nell''operatività le informazioni relative ai DPI presenti nel DUVR',N'Non omogena procedura di distribuzione e registrazione della stessa. Vi è il richio di una mancata registrazione della consegna e il responsabile di funzione non ha facile accesso alla consultazione del documento. Le specifiche dei DPi e ei rischi delle mansioni resterebbero esclusivamene nel DUVR.',NULL,N'HRP
OM
QARA',N'Assistenza applicativa
Assistenza strumenti esterni
Controllo qualità
Magazzino
Operation Site
Regolatorio
Ricerca e sviluppo
Risorse Umane
Sistema qualità
Validazioni',N'approvazione documenti Formazione al personale 
approvazione documenti e Formazione al personale 
approvazione documenti e Formazione al personale 
approvazione documenti e Formazione al personale 
Messa in uso dei nuovi doumenti
approvazione documenti e gestione dell''archiviazione elettronica del documento di consegna
approvazione documenti e Formazione al personale 
approvazione documenti e Formazione al personale 
Stesura della documentazione operativa (DS)
Formazione al personale 
Formazione al personale ',N'13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017',N'approvazione documenti Formazione al personale 
approvazione documenti Formazione al personale 
approvazione documenti Formazione al personale 
approvazione documenti Formazione al personale 
messa in uso documenti
approvazione documenti e gestione dell''archiviazione elettronica del documento di consegna
approvazione documenti e Formazione al personale 
approvazione documenti e Formazione al personale 
Stesura della documentazione operativa
Formazione al personale AVVENUTA IN DATA (MOD18,02) 8/1/18, 2012/17, 18/12/17, 22/12/17.',N'messa in uso doc - cc17-48_messa_in_uso.pdf',N'OM
RDM
SPM
GSC
QMA
HRP
CSC
PW
DS',N'13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017
13/12/2017',N'31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018
31/01/2018',N'22/01/2018
22/01/2018
22/01/2018
22/01/2018
22/01/2018
22/01/2018
22/01/2018
22/01/2018
22/01/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,N'Nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Federica Farinazzo','2018-01-31 00:00:00',NULL,'2018-01-22 00:00:00',N'No',NULL,NULL),
    (N'17-47','2017-12-06 00:00:00',N'Sergio  Fazari',N'Documentazione SGQ',N'Aggiornare l''allegato ALL-1 alla Procedura di Sorveglianza Regolatoria eliminado tra le fonti d''informazione la newsletter CEISO poichè non più attiva ed eventualmente aggiornare l''elenco.',N'Avere in uso una procedura aggiornata',N'In occasione di audit il mancato allineamento potrebbe generare NC od osservazioni',NULL,N'QM',N'Regolatorio',N'Aggiornare l''allegato ALL1_PR05,06 "Elenco fonti e responsabilità sorveglianza regolatoria"',N'20/12/2017',N'Aggiornare l''allegato ALL1_PR05,06 "Elenco fonti e responsabilità sorveglianza regolatoria"',N'ALL1_PR05,06 in uso al 08//0/2019 - all-1_03_pr05,06_elencofontieresponsabilitasorveglianzaregolatoria.pdf',N'QARA',N'20/12/2017',N'31/01/2018',N'08/08/2019',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'Sergio  Fazari','2018-01-31 00:00:00',NULL,'2019-08-08 00:00:00',N'No',NULL,NULL),
    (N'17-46','2017-12-05 00:00:00',N'Sergio  Fazari',N'Documentazione SGQ',N'Sostituire la norma armonizzata EN 980:2008 sui simboli da utilizzare nell''etichettatura e nelle informazioni fornite con la nuova norma EN ISO 15223-1:2016 pubblicata sulla Gazzetta dell''UE del 17.11.2017',N'Cambiamento indispensabile per assicurare la presunzione di conformità',N'La vecchia norma perde di validità il 31.12.2017 pertanto la mancata sostituzione con la nuova farebbe decadere la presunzione di conformità dei simboli utilizzati',NULL,N'MM
QARA
QM
ITM',N'Produzione
Regolatorio
Sistema informatico
Sistema qualità',N'La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
La nuova norma non presenta simboli applicabili ai prodotti di cui EGSpA è fabbricante. Non è necessario aggiornare IFU ed etichette.
Formazione sugli eventuali aggiornamenti alle etichette, in base all''analisi della nuova norma fatta da QARA.
a seguito di verifica da parte di QARA dei contenuti della nuova norma, eventuale ag',N'05/12/2017
05/12/2017
05/12/2017
05/12/2017',N'Aggiornamento dei requisiti essenziali 
Aggiornamento dei requisiti essenziali dei prodotti con la nuova norma.
aggiornamento delle dichiarazioni di conformità
Se presenti nuovi simboli o aggiornamento degli stessi potrebbe essere necessario aggiornare li IFU
l''analisi dei contenuti della nuova norma ed impatto sulla documentazione SGQ.
Formazione del personale in relazione agli aggiornamenti individuati rispetto alla980.
a seguito di verifica da parte di QARA dei contenuti della nuova norma, eventuale aggiornamento dei simboli sulle etichette in NAV.',N'FTP_T01PLD (RTST01PLD, CTRT01PLD) - cc17-46_section_1_product_technical_file_index_rtst01pld.docx
FTP_098PLD (RTS098PLD, CTR098PLD)  - cc17-46_section_1_product_technical_file_index_rts098pld.docx
FTP_RTS015PLD, CTR015PLD, STD015PLD - cc17-46_sezione_1_product_technical_file_index_rtk015pld.txt
FTP_020PLD (RTS020PLD, CTR020PLD, STD020PLD)  - cc17-46_sezione_1_product_technical_file_index_rts020pld.docx
 FTP_031PLD (RTS031PLD, CTR031PLD, STD031PLD)  - cc17-46_sezione_1_product_technical_file_index_rtsd031pld.docx
FTP_D00ING (RTSD00ING, CTRD00ING) - cc17-46_section_1_product_technical_file_index_rtsd00ing.docx
FTP_015 (RTS015, STD015) - cc17-46_sezione_1_product_technical_file_index_ftp_rtkd015.docx
FTPRTS553_CTR553 - cc17-46_master_product_technical_file_index_ftprts553_ctr553.docx
 FTP_032PLD (RTS032PLD, CTR032PLD, STD032PLD)  - cc17-46_sezione_1_product_technical_file_index_rts032pld.docx
 FTP_035PLD (RTS035PLD, CTR035PLD, STD035PLD)  - cc17-46_sezione_1_product_technical_file_index_rts035pld.docx
 FTP_G07210PLD (RTSG07PLD210, STDG07PLD210)  - cc17-46_sezione_1_product_technical_file_index_rtsg07pld210.docx
 FTP_078PLD (RTS078PLD, CTR078PLD, STD078PLD)  - cc17-46_sezione_1_product_technical_file_index_rts078pld.docx
 FTP_175PLD (RTS175PLD, CTR175PLD, STD175PLD)  - cc17-46_sezione_1_product_technical_file_index_rts175pld.docx
 FTP_036PLD (RTS036PLD, CTR036PLD, STD036PLD)  - cc17-46_sezione_1_product_technical_file_index_rts036pld.docx
FTP_RTS200ING-CRE e FTP_RTS201ING-ESBL - cc17-46_ftp_rts200ing-cre_rts201ing-esbl_21-11-2019.msg
FTP_037PLD (RTS037PLD, CTR037PLD, STD037PLD) - cc17-46_sezione_1_product_technical_file_index_rts037pld.docx
FTP_038PLD (RTS038PLD, CTR038PLD, STD038PLD) - cc17-46_sezione_1_product_technical_file_index_rts038pld.docx
FTP_070PLD (RTS070PLD, CTR070PLD, STD070PLD) - cc17-46_sezione_1_product_technical_file_index_rts070pld.docx
FTP_110PLD (RTS110PLD, STD110PLD) - cc19-18_sezione_1_product_technical_file_indexrts110pld_1.docx
FTP_176PLD (RTS176PLD, CTR176PLD, STD176PLD) - cc19-18_sezione_1_product_technical_file_indexrts176pld_1.docx
FTP_INT030 aggiornato - cc17-46_ftp_int030_02-04-2019.msg
DICHIARAZIONE AGGIORNATA CON 15223-1:2016 - mod05,05d_related_products_10.pdf
DICHIARAZIONE AGGIORNATA CON 15223-1:2016 - mod05,05c_real_time_elite_mgb_21.pdf
DICHIARAZIONI PRODOTTI DEKRA AGGIORNATE CON 15223-1:2016 - cc17-46_dichiarazioniconformitaprodottidekra.docx
 DICHIARAZIONE AGGIORNATA CON 15223-1:2016  - mod05,05_int030_03.pdf
 DICHIARAZIONE AGGIORNATA CON 15223-1:2016  - mod05,05_int033sp1000_00.pdf',N'QMA
QMA
QARA
QARA
QM
ITM
QMA',N'05/12/2017',NULL,N'05/03/2021
05/03/2021
05/03/2021',N'No',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'ET - Gestione Etichette',N'ET1 - Individuazione dei simboli/dati da inserire sull''etichetta',N'ET1',N'No',N'non necessaria',N'No',N'na',N'No',N'nessun impatto',NULL,NULL,NULL,'2021-03-05 00:00:00',N'No',NULL,NULL),
    (N'17-45','2017-11-27 00:00:00',N'Michela Boi',N'Fornitore',N'aggiornamento dei requisiti del fornitore Gem for Lab, indicazione che gli studi effettuati da ABLE Bioscience sono conformi alle Good Clinical Practice (GCP), e non solo alle Good Laboratory Practice (GLP)',N'idonea identificazione dei requisiti del fornitore',N'Dai documenti (report) degli studi di validazione effettuati da Gem for Lab non si evincerebbe che che gli studi sono stati eseguiti in conformità alle linee guida GCP. Al momento sui report è riportata solo l''indicazione la conformità rispetto alle linee guida GLP.',NULL,N'SPM
QARA',N'Acquisti
Validazioni',N'Aggiornamento dei report di validazione eseguiti da Gem for Lab con l''indicazione che gli studi effettuati in accordo alle GCP e inserimento di tale indicazione nei prossimi documenti
aggiornamento selezione fornitore Gem for Lab con certificato GCP',N'27/11/2017
27/11/2017',N'Aggiornamento del REP2016-063 (Report di Validazione STI ELITe MGB Panel), e di altri eventuali documenti se richiesti da terze parti. Report aggiornato con firma del 5/12/2017
aggiornamento selezione fornitore Gem for Lab con certificato GCP',N'GCP - cc17-45_mo06,06_aggiornamentogem_forlab_srl_gcp.pdf',N'PVS
PW',N'27/11/2017
27/11/2017',N'30/12/2017
30/12/2017',N'10/01/2018
27/11/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'VAL - Validazione delle estensioni d''uso di prodotto',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Silvia Costa','2017-12-30 00:00:00',NULL,'2018-01-10 00:00:00',N'No',NULL,NULL),
    (N'17-44','2017-11-21 00:00:00',N'Matteo Milani',N'Documentazione SGQ',N'All''interno del flusso operativo del ciclo di vita strumenti EGSpA e di terzi (dall''approvvigionamento alla messa in servizio presso l''utilizzatore finale) sono state ridefinite responsabilità ed attività in base alle nuove funzioni: 
- approvazione dell''acquisto di nuova strumentazione e della relativa assistenza tecnica, essendo autorizzata in fase di partecipazione alla gara non necessita della successiva autorizzazione all''acquisto (MOD06,06)
- esplicitare nelle procedure del QMS le funzioni coinvolte nel flusso operativo del ciclo di vita strumenti
- gestione delle tempistiche e delle attività per ciascun settore interessato nel flusso operativo del ciclo di vita strumenti. ',N'A seguito della nuova struttura organizzativa del customer service è stato rivisto il flusso di  gestione e messa in servizio dello strumento e servizi correlati a fronte dell''aggiudicazione di gara. Queste modifiche riducono il rischio di ritardi nella fornitura al cliente.',N' Ritardi nella fornitura al cliente.',NULL,N'CFO
GSC
PW
QM
SAD-TCO
SMD
T&CM',N'Acquisti
Assistenza strumenti esterni
Budgeting control
Gare
Marketing vendite italia
Operation Site
Sistema qualità',N'Gestione acquisto strumento e assistenza correlata senza MOD06,06, approvazione nuova revisione procedura PR06,02 "Approvvigionamento".
Comunicazione dell''aggiudicazione della  gara alla mailing list definita sulla PR06,02 "Approvvigionamento"
Conferma fornitura strumentazione da parte dell''Area Manager competente ed approvazione di eventuali variazioni rispetto ai requisiti forniti in gara
Eliminazione dell''autorizzazione alla spesa di nuova strumentazione attraverso MOD06,06 in quanto non più non necessario perché i beni e servizi sono costificati e autorizzati in fase di risposta al capitolato (analisi di profittabilità) 
Approvazione del flusso operativo del ciclo di vita strumenti EGSpA e di terzi
Coordinamento con SGC per installazioni strumenti InGenius
Approvazione e messa in uso dei documenti.',N'01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017',N'approvazione nuova revisione procedura PR06,02 "Approvvigionamento"
Formazione sulla mailing list definita sulla PR06,02 "Approvvigionamento" per l''invio dell''aggiudicazione della gara.
Formazione al personale di MKT e vendite relativi alla conferma fornitura strumentazione da parte dell''Area Manager competente ed approvazione di eventuali variazioni rispetto ai requisiti forniti in gara
Approvazione dell''eliminazione dell''autorizzazione alla spesa di nuova strumentazione attraverso MOD06,06
Approvazione del flusso operativo del ciclo di vita strumenti EGSpA e di terzi
Formazione al personale del servizio tecnico per il coordinamento per le installazioni di strumenti InGenius
Messa in uso documenti',NULL,N'PW
CSC
CSC
CFO
OM
CSC
QMA',N'01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017
01/12/2017',N'31/12/2017
31/01/2018
31/01/2018
31/01/2018
31/12/2017
31/01/2017
31/01/2018',NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'AST - Assistenza tecnica strumenti',NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'Matteo Milani','2018-01-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'17-43','2017-10-31 00:00:00',N'Daniele Salemi',N'Documentazione SGQ',N'Posticipare la manutenzione e la taratura esterna di alcune micropipette, al fine di accorparle al gruppo di strumenti che effettuano l''intervento a gennaio 2018. Gli strumenti sono i seguenti:
- Controllo Qualità: MP158, MP159, MP160 - da Ottobre a Gennaio
- produzione controlli positivi: DPE16 - da Ottobre a Gennaio
- PRODUZIONE DPE17, DPE18, DPE19, DPE20 - da Ottobre a Gennaio
- SERV.TECNICO MP162, MP163 - da Novembre a Gennaio',N'Riduzione dei costi richiesti per l''uscita del personale qualificato del fornitore.',N'Aumento dei costi delle manutenzioni.',NULL,N'QM',N'Assistenza strumenti interni
Sistema qualità',N'Per evitare di mettere non in uso gli strumenti in attesa che venga svolto l''intervento di MPE/TE arrecando disguidi organizzativi all''interno dei processi si effettuano dei controlli aggiuntivi (verifiche sul controllo dello stato di taratura mensili, secondo la IO11,03) affinché si riduca il rischio di utilizzare degli strumenti NON tarati. 
Verifica che la gestione dello spostamento dell''intervento di MPE/TE sia gestito nel modo adeguato per ridurre al minimo il rischio di utilizzo di strumenti non controllati.',N'31/10/2017
31/10/2017',N'controllo mensile sullo stato di taratura dei codici sopra elencati fino a gennaio 2018.
Verifica che la gestione dello spostamento dell''intervento di MPE/TE sia gestito nel modo adeguato per ridurre al minimo il rischio di utilizzo di strumenti non controllati.',N'tarature esterne eseguite 01/2018 - cc17-43_tarature_esterne_fatte.pdf',N'TSS
QM',N'31/10/2017
31/10/2017',N'31/01/2018
31/10/2017',N'31/01/2018
31/10/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'M - Manutenzione Sturmenti',NULL,N'Nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Daniele Salemi','2018-01-31 00:00:00',NULL,'2018-01-31 00:00:00',N'No',NULL,NULL),
    (N'17-42','2017-10-31 00:00:00',N'Silvia Costa',N'Documentazione SGQ',N'Aggiornare gli assay protocol del prodotto RTK015PLD per lo strumento Ingenius per:
- aggiornamento della versione in uso del sw dello strumento (versione 1.2),
- modificare il valore di "Upper limit of quantification" come per il progetto SCP2017-001,
- modificare il nome (renderlo leggibile sullo strumento).',N'Avere gli assay protocol del prodotto RTK015PLD aggiornati ed allineati per tutte le matrici.',N'non uniformità degli assay protocol.',NULL,N'SPM
QARA
RDM
SMD',N'Assistenza applicativa
Regolatorio
Ricerca e sviluppo
Validazioni',N'Aggiornare gli assay protocol e l''IFU del prodotto RTK015PLD.
Revisionare gli assay protocol e l''IFU del prodotto RTK015PLD.
Messa in uso degli assay protocol e dell''IFU del prodotto RTK015PLD.
Pianificazione e caricamento dei nuovi protocolli sugli strumenti presso i clienti.',N'31/10/2017
31/10/2017
31/10/2017',N'Aggiornare gli assay protocol e l''IFU del prodotto RTK015PLD.
Aggiornare gli assay protocol e l''IFU del prodotto RTK015PLD.
Messa in uso degli assay protocol e dell''IFU del prodotto RTK015PLD.
Pianificazione e caricamento dei nuovi protocolli sugli strumenti presso i clienti.',N'IFU RTK015PLD rev12 e assay protocol - cc17-42_messa_in_uso.pdf
''la gestione dell''aggiornamento degli  AP presso clienti è trattata nella RAC20-16, si ritiene pertanto possibile chiudere il CC - cc17-42_installazioneapclientevedererac20-16.txt',N'PVS
RT
QMA
T&CM',N'31/10/2017
31/10/2017
31/10/2017',N'10/11/2017
10/11/2017
10/11/2017',N'15/11/2017
15/11/2017
20/11/2017
07/10/2020',N'No',N'-',N'No',N'-',N'No',N'-',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'-',N'No',N'-',N'No',N'-',N'Silvia Costa','2017-11-10 00:00:00',NULL,'2020-10-07 00:00:00',N'No',NULL,NULL),
    (N'17-41','2017-10-30 00:00:00',N'Federica Farinazzo',N'Stabilità',N'Si chiede l''estensione della scadenza del materiale 1 WHO International Standard for Human Cytomegalovirus (950-194). 
In base all''esito conforme del test eseguito dopo 30 mesi dalla risospensione, si chiede di estendere la scadenza da 18 mesi a 24 o 30 mesi.',N'Aumentare l''utilizzabilità di un reagente utilizzato come riferimento per il CQ e le validazioni. ',N'Non aumento la scadenza ci è uno spreco di materiale, che in base al test eseguito risulta funzionante ben oltre la scadenza assegnata',NULL,N'SPM
RDM',N'Controllo qualità
Ricerca e sviluppo',N'Modifica del modulo MOD956-WHO-IS_CMV
Valutazione dell''estensione della scadenza
Valutare se inserire un secondo lotto in stabilità per conferma del dato
Modifica della specifica 950-194
Formazione a QCT/PVT/W
Valutazione dell''estensione della scadenza
Valutare se inserire un secondo lotto in stabilità per conferma del dato
Modifica della specifica 950-194
Formazione a QCT/PVT/W',N'30/10/2017
30/10/2017
30/10/2017',N'Modifica del modulo MOD956-WHO-IS_CMV
Valutazione dell''estensione della scadenza
Valutare se inserire un secondo lotto in stabilità per conferma del dato
Modifica della specifica 950-194
Formazione a QCT/PVT/W. MOD18,02 del 5/4/2018',N'mod956-who-is-cmv rev01 - cc17-41_messa_in_uso_mod_cq.pdf
non necessario secondo lotto in stabilità per confermare il dato - cc17-41_valutato_non_necessario_un_secondo_lotto.pdf
SPEC9520-194 rev02 - cc17-41_cc17-32_cc51-15_messa_in_uso_specifiche.pdf',N'QCT
RT',N'30/10/2017
30/10/2017',N'24/04/2018',N'24/04/2018
06/04/2018',N'No',NULL,N'No',NULL,N'No',NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Federica Farinazzo','2018-04-24 00:00:00',NULL,'2018-04-24 00:00:00',N'No',NULL,NULL),
    (N'17-40','2017-10-25 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'aggiornamento Schede Dati Sicurezza n°70 (PreMix) e n°71 (PCR MasterMix) e n°27 (Positive Control)',N'l''indicazione del nuovo prodotto RTS130ING trova riscontro nelle SDS a cui fa riferimento (SDS per i prodotti one-step) e l''indicazione del prodotto IC500 trova riscontro nella SDS per i controlli positivi',N'un nuovo prodotto, RTS130ING, non è incluso in nessuna scheda dati sicurezza e l''utilizzatore finale potrebbe correre dei rischi per non aver letto le SDS collegate al prodotto di interesse. Anche il Controllo Interno IC500 non è incluso nella scheda dati sicurezza corrispondente',NULL,N'QARA
RDM',N'Regolatorio
Sistema qualità',N'aggiornamento delle SDS riguardanti i prodotti one-step (N°70 e 71)
aggiornamento delle SDS riguardanti i prodotti one-step (N°70 e 71) e Positive Control (N°27)
aggiornamento delle SDS riguardanti i prodotti one-step (N°70 e 71) e Positive Control (N°27)',N'25/10/2017
25/10/2017
25/10/2017',N'Aggiornamento delle SDS N°70 e 71 (per i prodotti one-step) con l''indicazione del nuovo prodotto RTS130ING
Aggiornamento delle SDS N°70 e 71 (per i prodotti one-step) con l''indicazione del nuovo prodotto RTS130ING e della SDS N°27 (Positive Control) con l''indicazione del prodotto IC500',N'messa in uso SD70 e SDS71 - cc17-40_messa_in_uso_1.pdf',N'QMA
QMA',N'25/10/2017
25/10/2017',N'31/10/2017
31/10/2017',N'26/10/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'R&D - Svilupppo e commercializzazione prodotti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Michela Boi','2017-10-31 00:00:00',NULL,'2017-10-26 00:00:00',N'No',NULL,NULL),
    (N'17-39','2017-10-16 00:00:00',N'Roberta  Paviolo',N'Organizzazione interna (es: organigramma livelli dirigenziali)',N'Aggiornamento dei documenti EGSpA con le nuove funzioni e mansioni aziendali, come da Org - EGSpa Rev05 - 1.10.2017 e mansionario MOD18,01, rev03 firmato il 17/10/2017 (per l''elenco delle funzioni vedere ALLEGATO A)',N'documentazione aggiornata',N'documentazione non aggiornata',NULL,N'QM',N'Sistema qualità',N'Aggiornamento delle procedure con le nuove funzioni / mansioni; con i nuovi codici funzione.',N'16/10/2017',N'Aggiornamento delle procedure con le nuove funzioni / mansioni; con i nuovi codici funzione, vedere allegato 1 "elenco pr da aggiornare"',N'elenco pr da aggiornare - cc_17-39_allegato1_procedure_da_aggiornare.pdf
MESSA IN USO_PR19,02_rev00 - cc17-39_messa_in_uso_pr19,02_rev00.pdf
PR04,04 rev01 - cc17-39_messa_in_uso_pr0404_rev01.msg
PR03,02 aggiornata secondo CC19-39, PR19,01 aggiornate secondo RAC19-27 - cc17-39_pr03,02epr19,01_aggiornate_secondo_altre_modifiche.txt',N'QM',N'16/10/2017',N'30/05/2018',N'08/04/2020',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'No',N'nessun impatto',N'Stefania Brun','2018-05-30 00:00:00',NULL,'2020-04-08 00:00:00',N'No',NULL,NULL),
    (N'17-38','2017-10-10 00:00:00',N'Debora Pensi',N'Etichette',N'Aggiornare l''etichetta del prodotto IC500 passando dal codice 954-171, colore rosso, al codice 954-173, colore verde identificativo del controllo interno (CTRCPE).
Aggiornare l''etichetta del prodotti RTSO76PLD, RTS100PLD, RTS130ING, RTS507ING, RTS523ING, RTS548ING, RTS533ING, RTS553ING passando dal codice 954-171, colore rosso, al codice 954-172, colore blu.',N'Il cliente è in grado di distinguere visivamente tra i kit necessari per la fase di estrazione (controllo interno con etichetta codice colore verde), come già accade con il CTRCPE, rispetto ai kit necessari per la fase di amplificazione, quali mix e controlli positivi con etichetta codice colore rosso, utilizzati sullo strumento Ingenius, diminuendo così il rischio di errore.
Rendere intuitiva ed immediatamente identificabile la differenza tra kit nei quali è necessaria la ricostituzione della mix (con codice colore blu 954-172) e quelli pronti all''uso (concodice colore rosso 954-171)',N'Non esiste un codice colore univoco del controllo interno.
La differenziazione tra kit con mix pronte all''uso e kit con mix da ricostituire non è immediata.',NULL,N'MM
OM
PW
QC
QM
SMD',N'Acquisti
Controllo qualità
Marketing
Produzione
Program mangment
Ricerca e sviluppo
Sistema qualità',N'Il numero di etichette 954-173 (colore verde) da acquistare includendo il prodotto IC500 non varia i volumi di acquisto.
Le etichette 954-172 (colore blu) per includere i prodotti RTSO76PLD, RTS100PLD, RTS130ING, RTS507ING, RTS523ING, RTS548ING, RTS553ING e RTS533ING devono essere modificate in quanto hanno il logo "Alert". è perciò necessario chiedere al fornitore un preventivo per un nuovo formato di etichette. sulla base di questi costi si valuterà l''approvazione del cc.
Formazione sul cambio di etichette
Valutazione dell''aggiornamento del materiale di MKT e sito web.
Aggiornamento del MOD962IC500 e formazione al personale.
Aggiornamento del MOD962-RTSO76PLD, MOD962-RTS100PLD, MOD962-RTS130ING, MOD962-RTS507ING, MOD962-RTS523ING, MOD962-RTS548ING, MOD962-RTS533ING,  MOD962-RTS553ING e formazione al personale.
Nessun impatto.
messa in uso del modulo di produzione ed aggiornamento del MOD04,13.
aggiornamento DMRI e FTP di RTSO76PLD, RTS100PLD, RTS130ING, RTS507ING, RTS523ING, RTS548ING, RTS53',N'08/11/2017
10/10/2017
10/10/2017
10/10/2017',N' chiedere al fornitore un preventivo per un nuovo formato di etichette. 
Aggiornamento del MOD962IC500 e formazione al personale.
Aggiornamento del MOD962-RTSO76PLD, MOD962-RTS100PLD, MOD962-RTS130ING, MOD962-RTS507ING, MOD962-RTS523ING, MOD962-RTS548ING, MOD962-RTS533ING,  MOD962-RTS553ING e formazione al personale.
messa in uso del modulo di produzione ed aggiornamento del MOD04,13.
aggiornamento DMRI e FTP di RTSO76PLD, RTS100PLD, RTS130ING, RTS507ING, RTS523ING, RTS548ING, RTS533ING, RTS553ING.',NULL,N'QMA
QMA
DS
PW
SMD',N'10/10/2017',N'31/12/2017',NULL,N'Si',N'RTSO76PLD, RTS100PLD, RTS130ING, RTS507ING, RTS523ING, RTS548ING, RTS533ING, RTS553ING ',N'No',NULL,N'No',NULL,N'ET - Gestione Etichette',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Debora Pensi','2017-12-31 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'17-37','2017-10-04 00:00:00',N'Michela Boi',N'Manuale di istruzioni per l''uso',N'Update of the "Possible Mitigation" indicated for Error Code 20060 (Leakage of DN100N during head movement) in the table titled "List of main Error Codes". The table is reported in Section 7.1 of the InGenius Operator''s Manual, RUO version (INT030-US)',N'A more accurate maintanance and assistance from EGSpA',N'The customer would not require EGSpA  assistance and the problem would not be solved. ',NULL,N'QARA',N'Assistenza strumenti esterni
Regolatorio
Sistema qualità',N'With this update with the indication of contacting EGSpA assistance in case of InGenius Error 20060, the customer has less risk of not to solve the problem.
INT030-US manual has to be updated as suggested by technical service: update of the "Possible Mitigation" indicated for Error Code 20060 in the table titled "List of main Error Codes" reported in Section 7.1
With this update with the indication of contacting EGSpA assistance in case of InGenius Error 20060, the customer has less risk of not to solve the problem.',N'06/10/2017
06/10/2017',N'INT030-US manual has to be corrected as suggested by technical service: update of the "Possible Mitigation" indicated for Error Code 20060 in the table titled "List of main Error Codes" reported in Section 7.1.
Update of the INT030-US Operator''s Manual and put in place of the IFUs
Approvals of the updated manual',N'INT030US rew01_in use - cc17-37_messa_in_uso_int030-us_1.pdf',N'QMA
QMA
GSC',N'06/10/2017
06/10/2017',N'16/10/2017
16/10/2017
16/10/2017',N'16/10/2017
16/10/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'IFU - Gestione Manuali',NULL,N'no impact',N'No',NULL,N'No',NULL,N'No',NULL,N'Michela Boi','2017-10-16 00:00:00',NULL,'2017-10-16 00:00:00',N'No',NULL,NULL),
    (N'17-36','2017-09-18 00:00:00',N'Katia Arena',N'Oggetto',N'Richiesta di valutare la spedizione dei prodotti RTS000, RTS001 e RTS002 a temperatura controllata ( -20°C) insieme ai prodotti associati mantenendo invariate le condizioni di stoccaggio a +2 C.',N'Permettendo la spedizione di questi prodotti ( RTS000, RTs001 e RTS002) a -20°C insieme ai loro prodotti associati questo permette di ridurre il numero di colli che vengono preparati al momento della spedizioni: si prevede una riduzione dei numero di imballi utilizzati per le spedizioni e di conseguenza una riduzione dei materiali quali panetti e DRY Ice.',N'Le condizioni di spedizione rimarebbero invariate : per esempio per spedire un kit ALERT si dovrebbe preparare un collo con i panetti e un collo con il DRY ICE questo comporta maggiori costi di spedizione e consumo di ghiaccio e panetti. ',NULL,N'OM
QARA
QM
RDM
SAD-OP',N'Magazzino
Ordini
Regolatorio
Ricerca e sviluppo
Senior Advisor R&D
Sistema informatico
Sistema qualità',N'Valutare la stabilità del prodotto a -20°C, dimostrare che il congelamento in fase di spedizione non ha impatto sul prodotto.
Formare il personale che esegue le spedizioni. Aggiornamento della documentazione
Formare il personale che esegue le spedizioni. Aggiornamento della documentazione
Formare il personale che esegue le spedizioni. Aggiornamento della documentazione
Messa in uso della documentazione.
Aggiornamento del FTP.
Messa in uso della documentazione.
Aggiornamento del FTP.
Messa in uso della documentazione.
Aggiornamento del FTP.
Inserimento di una nota informativa nel documento di trasporto (DDT), in modo che sia evidente che la spedizione a -20°C non è un errore ma la nuova procedura di spedizione.
Valutare l''impatto di questa modifica sul processo di spedizione (in particolare SP12 preparazione imballo e SP13 spedizione).
Inserimento di una nota informativa nel documento di trasporto (DDT), in modo che sia evidente che la spedizione a -20°C non è un errore ma la nuova pr',N'28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017
11/10/2017',N'dati di stabilità del prodotto RTS000
formazione e aggiornamento personale
Aggiornamento della documentazione di spedizione
Inserimento di una nota informativa in ogni imballo per un periodo di tempo di 12 mesi,
Messa in uso della documentazione.
Aggiornamento FTP con i dati della stabilità
Inserimento in Navision della frase in Italiano ed Inglese che riporta la nota informativa
Inserimento in Navision della frase in Italiano ed Inglese che riporta la nota informativa.
inserita la frase relativa alla tipologia di spedizione come si evince dal DDT preso ad esempio (DDT17-ps-04087 del 16/10/17), allegato.
Valutazione ed eventuale aggiornamento dei sottprocessi di preparazione imballo e spedizione (rispettivamente SP12 e SP13). Nessun impatto.
dati di stabilità del prodotto RTS000
Il rapporto Taqman Universal PCR Master MIX STability Study Report", codice NAD2012/007 è sufficiente per dimostrare che un congelamento a -20°C in fase di spedizione non influisce sulle prestazioni del prodo',N'nota informativa (nel DDT) - cc17-36_notainformativa_ddt.pdf
ESEMPIO DDT N°17-PS-04087 DEL 16/10/17 - cc17-36_notainformativa_ddt.pdf
mail nota informativa cambiamento modalità di trasporto - cc17-36_notainformativaaggiornamentomodalitaditrasporto.pdf
nota informativa - avviso_spedizione_rts000.docx
esempio ddt con nota informativa - cc17-36_notainformativa_ddt.pdf',N'RT
PW
QMA
ITM
PW
SAD-OP
QMA
SAD-OP
SAD-OP
RT
QARA
QMA
RDM
W',N'28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017
28/09/2017',N'31/10/2017
04/10/2017
31/10/2017
04/10/2018
31/10/2017
30/11/2017
04/10/2017
31/10/2017
31/10/2017
28/09/2017
04/10/2017',N'04/10/2017
31/10/2017
18/03/2019
13/10/2017
31/10/2017
28/09/2017
13/10/2017',N'No',N'nerssun impatto',N'No',N'invio della nota informativa',N'No',N'nessun impatto',N'SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente
SP - Trasferimento al magazzino esterno, stoccaggio e spedizione al cliente',N'SP12 - Preparazione imballo per spedizione al cliente da EGSpA
SP13 - Spedizione al cliente da EGSpA',N'nessun impatto su SP12 e SP13',N'No',N'non necessaria',N'No',N'nessun impatto',N'No',N'na',N'Katia Arena','2018-10-04 00:00:00',NULL,'2019-03-18 00:00:00',N'No',NULL,NULL),
    (N'17-35','2017-08-31 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Semplificazione della documentazione inerente le indagini di OOS di CQ e NC.',N'Ottimizzare la pianificazione delle verifiche da eseguire a seguito di una NC ed ottimizzare il numero di registrazioni delle attività e dei dati ottenuti durante l''indagine. Miglior utilizzo di Linkomm per la raccolta dei documenti.',N'Tempi lunghi per la gestione della documentazione e rischio di non includere tutte le informazioni necessarie all''interno della NC su Linkomm.',NULL,N'QARA
QC
QM',N'Controllo qualità
Sistema qualità',N'Nei mesi intercorsi dall''apertura del CC all''approvazione si è preso in analisi il processo al fine di ottimizzare la pianificazione delle verifiche da eseguire a seguito di una NC e di ridurre il numero di registrazioni. Da ciò ne deriva un miglior utilizzo di Linkomm per la raccolta dei documenti.
Nei mesi intercorsi dall''apertura del CC all''approvazione si è preso in analisi il processo al fine di ottimizzare la pianificazione delle verifiche da eseguire a seguito di una NC e di ridurre il numero di registrazioni. Da ciò ne deriva un miglior utilizzo di Linkomm per la raccolta dei documenti.
Le OSS di CQ, legate ad errori tecnici puntuali (campioni outlier), vengono monitorate all''interno del processo di CQ senza la gestione di una NC. 
Più agevole fruizione della documentazione legata all''indagine relativa alla NC (disponibilità dei documenti più significativi su linkomm).',N'07/05/2018
07/05/2018
07/05/2018',N'- definizione di campione outlier (su prodotti e campioni di riferimento già controllati) e creazione di un modulo per la loro registrazione
- creazione di un modulo di pianificazione delle indagini con l''elenco delle possibili cause primarie delle NC
- aggiornamento di istruzione operativa IO10,14 e porcedura PR10,02 e archiviazione dei precedenti report d''indagine.
Aggiornamento risk assessment CQ17 e CQ18.
Messa in uso ed archiviazione dei documenti.',N'IO10,14rev02,PR10,02rev05, MOD10,23, MOD10,24rev00 - cc17-35_messa_in_uso_documenti.msg
messa in uso documenti, MOD18,02del 25/07/2018 - cc17-35_messa_in_uso_documenti_1.msg
tutti i risk assesment del CQ verranno revisionati entro il 31122020 come previsto dal report annuale 2019, sio ritiene pertanto possibile chiudere il CC nonostante l''attività di aggiornamento rimanga aperta - cc17-35_ranonaggiornati.txt',N'QC
QMA
QC',N'07/05/2018
07/05/2018',N'31/07/2018
31/07/2018',N'31/07/2018
07/10/2020
31/07/2018',N'No',N'-',N'No',N'-',N'No',N'-',N'CQ - Controllo Qualità prodotti',N'CQ17 - Gestione OOS di CQ',N'CQ17 e CQ18',N'No',N'-',N'No',N'-',N'No',N'-',NULL,'2018-07-31 00:00:00',NULL,'2020-10-07 00:00:00',N'No',NULL,NULL),
    (N'17-34','2017-07-03 00:00:00',N'Katia Arena',N'Materia prima',N'A seguito di comunicazione da parte del fornitore FISHER relativa alla dismissione, da parte di Thermo, del codice DyNAzyme II DNA Polymerase, cod F-501L ( ns codice interno 950-106), è necessario valutare un nuovo enzima per la produzione dei prodotti codice BANG.',N'Continuità di produzione dei BANG.',N'è necessario valutare, attraverso i dati di vendita, le potenzialità dei prodotti BANG al fine di deciderne la dismissione o l''aggiornamento con un nuovo enzima. Se l''enzima non viene sostituito con uno in commercio i BANG non potranno più essere prodotti.',NULL,N'MM
OM
QC
QM
RDM
SMD',N'Acquisti
Marketing vendite italia
Ricerca e sviluppo
Senior Advisor R&D
Sistema qualità',N'Contattare il fornitore per definire le modalità su come procedere per individuare un prodotto alternativo
Contattare il fornitore per definire le modalità su come procedere per individuare un prodotto alternativo
valutare le proposte inviate e procedere con i test di verifica funzionale. Se, dai dati di vendita, si ritiene opportuno continuare a commercializzare i BANG è necessario: valutare il nuovo enzima attraverso test specifici anche in concomitanza con il nuovo buffer (buffer utilizzato per la produzione delle nested anch''esso dismesso con il prodotto F-501L), effettuare i test di stabilità ed eventualmente aggiornare i manuali di istruzioni.
valutare le proposte inviate e procedere con i test di verifica funzionale. Se, dai dati di vendita, si ritiene opportuno continuare a commercializzare i BANG è necessario: valutare il nuovo enzima attraverso test specifici anche in concomitanza con il nuovo buffer (buffer utilizzato per la produzione delle nested anch''esso dismesso con il prodotto F-50',N'03/07/2017
03/07/2017
27/07/2017
27/07/2017
27/07/2017
27/07/2017
27/07/2017
04/09/2017',N'Contattare il fornitore ( Thermo) per conoscere lo stock di materiale ancora disponibile al fine di procedere con un ultimo ordine. La quantità acquistata dovrà coprire un periodo tra i 6 e 12 mesi di stock. Con l''estensione della scadenza dei materiali (allegato 1) lo stock arriverà ad inizio 2019. In questi mesi si valuteranno i tempi di dismissione delle NESTED prima di procedere a valutare un prodotto alternativo che comporterebbe la validazione e l''adeguamento del manuale di istruzioni per l''uso.
Ricerca di prodotti alternativi
Valutare le proposte e le campionature
Eseguire i test funzionali sui campioni inviati
aggiornamento specifica/ creazione nuova specifica ( in sot 950-106)
La dismissione del codice F-501L comprende anche il buffer ad oggi utilizzato per produrre le Nested: valutare se produrre il buffer internamente e valutare come validarne l''uso (testere alcuni lotti di Nested)
effettuare delle stabilità con il nuovo enzima ed il nuovo buffer
Aggiornamento FTP prodotti
tempi',N'allegato1 - cc17-34_certificato_life_data_scaedenza_ultimi_lotti_enzimi_estesa_alla_scadenza_assegnata_alla_bulk.pdf
Allegato2 - cc17-34_certificato_life_data_scaedenza_ultimi_lotti_enzimi,_lotti_aggiornati.pdf
Test Tubi TF - test_validazione_nuovi_tubi_02_ml_fischer_scientific.pdf
test - test_validazione_nuovi_tubi_02_ml_fischer_scientific_1.pdf
SPEC950-106 rev01 - cc17-34_messa_in_uso_spec950-106_cc34-17_mod1802_del_24092019.msg',N'PW
PW
PW
QC
RT
QC
RT
QMA
SMD
RT',N'03/07/2017
03/07/2017
11/09/2017',N'27/07/2017
27/07/2017
11/09/2017',N'03/07/2017
03/07/2017
30/08/2017
24/09/2019',N'Si',N'I prodotti interessati non erano strutturati con il DMRI, ma sarà da aggiornare la sezione 5 dei FTP',N'No',N'non necessaria',N'No',N'nessun impatto',N'ALTRO - Altro',N'ALTRO - ALTRO',N'Nesun impatto ',N'No',N'non necessaria',N'No',N'nessun virtual manufacturer',N'No',N'non necessaria, è stato aggiornato solo il codice produttore/ fornitore che ci fornisce la materia prima',NULL,'2017-09-11 00:00:00',NULL,NULL,N'No',NULL,NULL),
    (N'17-33','2017-07-24 00:00:00',N'Samuela Margio',N'Progetto',N'Aggiornamento del MRD e PRD del progetto micobatterio:
1) Modifica del nome del prodotto MDR TB ELITe MGB Kit in MDR/MTB ELITe MGB Kit e del controllo positivo MDR TB ELITe-Positive Control in MDR/MTB ELITe-Positive Control.
2) Spostamento dei requisiti legati a eNAT da "Must" a "Want".
3) Modifica del campione da escreato a escreato dopo decontaminazione, fluidificazione e inattivazione.
4) Modifica della manipolazione del campione da un livello di sicurezza BSL2 a BSL3.
5) Cambiamento del numero di test per run da 12 campioni a 6 campioni contemporaneamente.',N'Maggior allineamento con gli altri prodotti in commercio.
Allineamento con le linee guida nazionali e internazionali per la gestione del campione.',N'Necessità di installare lo strumento Ingenius in livello di sicurezza BSL3.',NULL,N'SPM
RDO
RDM',N'Controllo qualità
Marketing
Produzione
Program mangment
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Aggiornare PRD e allineare il piano di verifica e validazione e tutti i documenti correlati.
Aggiornare PRD e allineare il piano di verifica e validazione e tutti i documenti correlati.
Aggiornamento protocolli di validazione.
Ricerca del centro di validazione con i requisiti adeguati.
Aggiornamento protocolli di validazione.
Ricerca del centro di validazione con i requisiti adeguati.
Modifica moduli CQ e produzione e formazione al personale.
Utilizzo della nuova documentazione LP2 a seguito di formazione.
Aggiornamento etichette.
Aggiornamento MRD
Revisione documenti aggiornati.',N'24/07/2017
24/07/2017
26/07/2017
26/07/2017
26/07/2017
26/07/2017
26/07/2017
26/07/2017
26/07/2017',N'Aggiornare PRD e allineare il piano di verifica e validazione e tutti i documenti correlati.
PRD2017-003 rev01, firmato il 9/8/2017. PLAN2017-031 (piano di verifica e validazione) rev00 firmato il 7/09/2017
Aggiornare i documenti di DT.
Aggiornata la PML2017-003 rev01 firmata il 5/09/2017, da cui originano etichette ed anagrafica prodotto.
Aggiornamento protocolli di validazione.
Protocollo di validazione "Performance del test MDR/MTB ELITe MGB kit nella diagnosi di tubercolosi" firmato il 7/11/2017.
Ricerca del centro di validazione con i requisiti adeguati.
Identificato Sant Oorsola di Bologna dip- Micribiologia
Modifica moduli CQ e produzione e formazione al personale.
moduli in uso al 10/10/17, come da mail allegata. formazione del 12/10/17
Aggiornamento etichette. fatto
Aggiornamento MRD.
MRD2016-002 rev01 firmato il 09/08/2017.
Revisione documenti aggiornati.
MRD2016-002 rev01 firmato il 09/08/2017.',N'messa in uso moduli cq - cc17-33_messa_in_uso_moduli_cq.pdf',N'RT
RT
PVS
PVS
QC
QM
PMS',N'24/07/2017
24/07/2017
26/07/2017
26/07/2017
28/07/2017
26/07/2017
26/07/2017
24/07/2017',N'28/08/2017
31/08/2017
08/09/2017
08/09/2017
31/08/2017
31/08/2017
31/08/2017
31/08/2017',N'07/09/2017
05/09/2017
20/11/2017
08/09/2017
13/10/2017
20/09/2017
09/08/2017
09/08/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Samuela Margio','2017-09-08 00:00:00',NULL,'2017-11-20 00:00:00',N'No',NULL,NULL),
    (N'17-32','2017-06-21 00:00:00',N'Roberta  Paviolo',N'Materia prima',N'Inserire un nuovo fornitore di etichette (Matec) che attualmente acquistiamo solo da Tecmark',N'Fornitore proposto, Matec, è più celere e con prezzo più competitivo.',N'Fornitore che non rispetta le consegne e che ha un prezzo più alto',NULL,N'BC
MM
OM
PW
RDM',N'Acquisti
Ricerca e sviluppo
Senior Advisor R&D',N'Richiesta offerta per acquisto etichette necessarie al momento (954-010)
Richiesta offerta per acquisto etichette necessarie al momento (954-010)
aggiornamento specifiche
Aggiornamento specifiche',N'21/06/2017
21/06/2017
21/06/2017
21/06/2017',N'Richiesta offerta per acquisto etichette necessarie al momento (954-010) e richiesta codice fornitore
elenco codice fornitore per le altre etichette interessate dal cambiamento
aggiornamento specifiche
Aggiornamento specifiche. MOD18,02 del 5/4/2018.',N'SPEC954-010_rev01, SPEC954-138rev02 - cc17-14_cc17-32_cc51-15_messa_in_uso_specifiche.pdf
in uso SPEC954-086/087_formazione del 4/10/18 - cc17-32_in_uso_spec954-086087_mod18,02_4-10-18.msg
SPEC954-158_01, SPEC954-159_01, SPEC954-164_01, FORMAZIONE DEL 2/12/19 - cc17-32_messa_in_uso_specifiche_cc17-32_cc19-36_cc10-17_mod1802_021219.msg',N'PW
PW
RT
RT',N'21/06/2017
21/06/2017
21/06/2017
21/06/2017',N'31/07/2017
30/09/2017
31/12/2017
31/12/2017',N'06/04/2018
09/12/2019
09/12/2019',N'No',N'NESSUN IMPATTO',N'No',N'non necessaria',N'No',N'nessun impattoo',N'A - Gestione degli Acquisti',N'A2 - Identificazione dei materiali/strumenti/servizi influenti sulla qualità del prodotto',N'nessun impatto',N'No',N'non necessaria',N'No',N'nessun impatto',N'No',N'non applicabile',N'Katia Arena','2017-12-31 00:00:00',NULL,'2019-12-09 00:00:00',N'No',NULL,NULL),
    (N'17-31','2017-06-07 00:00:00',N'Sergio  Fazari',N'Componenti principali',N'Sostituzione dei seguenti tubi non IVD con equivalenti tubi dotati di marcatura CE-IVD:
- 55.475.001 Sarsted 5 mL, 13x75 fondo rotondo
-72.694.005 Sarsted 2 mL, tubi tappo a vite.
I tubi  sono indicati come materiale richiesto e non incluso nei kit di estrazione degli strumenti ELITeGalaxi e ELITeIngenius. Tavolta sono forniti in gara ai clienti.
I tubi codice 55.459 Sarsted 13 mL, 16x100 fondo rotondo, verrano eliminati dal manuale di istruzioni per l''uso.',N'Eliminare il rischio di controversie legali/regolamentari con clienti.',N'Sarebbe possibile l''interruzione degli acquisti da parte di alcuni clienti a causa della mancanza del marchio CE-IVD.',NULL,N'PW
RDM
SAD-OP',N'Acquisti
Marketing vendite italia
Ordini
Ricerca e sviluppo
Sistema qualità',N'Verifica dell''equivalenza dei tubi non CE-IVD, con i tubi CE-IVD.
Aggiornamento specifiche.
Verifica dell''equivalenza dei tubi non CE-IVD, con i tubi CE-IVD.
Aggiornamento specifiche.
Ricerca prodotto equivalente
Aggiornamento e messa in uso IFU.
messa in uso specifiche.
Aggiornamento e messa in uso IFU.
messa in uso specifiche.
Aggiornamento listini
Aggiornamento dei codici presso i clienti
Aggiornamento dei codici presso i clienti
Aggiornamento dei codici presso i clienti',N'20/06/2017
20/06/2017
20/06/2017
20/06/2017
20/06/2017
21/06/2017
21/06/2017
21/06/2017',N'Aggiornamento specifiche e formazione. LE SPECIFICHE NON SONO DA AGGIORNARE IN QUANTO RIGUARDANO MATERIALI NON INCLUSI NEL KIT
Verifica dell''equivalenza dei tubi non CE-IVD, con i tubi CE-IVD.
Effettuare una prova sullo strumento ELITeInGenius con i tubi codice 55.475.001 Sarsted da 5 ml 13x75 e i nuovi tubi acquistati.
Allestimento della prova:
1 sessione su InGenius con i seguenti tubi:
- 4 tubi sarsted attualmente in uso
- 4 tubi sarsted IVD da testare
- 4 tubi di sonicazione.
Come matrice si userà plasma negativo positivizzato con CMV.
Ricerca prodotto equivalente. I tubi codice 55.475.001 Sarsted 5 mL, 13x75 fondo rotondo hanno come equivalente il codice 55.475.030 Sarsted 5 mL, 13x75 fondo rotondo come si evince dal report R&D.
A 11/2017 il fornitore Sarsted comunica che per il codice "72.694.005 2 mL, tubi tappo a vite" è prevista l''etichettatura CE-IVD entro 12/2017, la disponibilità del prodotto è prevista per  06/2018. la data di scadenza prevista per questo CC viene quindi spostata ',N'- - cc17-31_mail_per_creazione_anagrafica_nav_nuovo_codice_tubi_5_ml.pdf
72.694.005 2 ML ETICHETTA CE-IVD - cc17-31_etichettace-ivd_72-694-005.jpg
IFU Galaxi rev03 Extraction kit rev05 - cc17-31_messa_in_uso_ifuschmint020rev03_int021exrev05.pdf
IFU Ingenius rev02 - cc17-31_ifuschint032sp200_02_tubi_sarsted55.475.030.pdf
IFU INT032sp200 rev02 - cc17-31_ifuschint032sp200_02_tubi_sarsted55.475.030_1.pdf
listini - cc17-31_mail_per_modifica_listini.pdf
nel listino depositato in camera di commercio a 07/2018 sono presenti i 2 codici 72.694.005 e 55.4750030 - cc17-31_listinoitalia2018.pdf
REP2017-067 DEL 6/11/2017 - cc17-31_rep2017-067_rapportostudiofattbilita.pdf',N'SAD-OP
RT
PW
QMA
QMA
SAD-OP
SrPM
RT',N'20/06/2017
20/06/2017
20/06/2017
20/06/2017
21/06/2017
21/06/2017',N'30/07/2017
22/06/2017
20/06/2017
31/08/2017
31/08/2017
31/07/2017
31/08/2017',N'20/11/2017
20/11/2017
06/11/2018
14/03/2018
14/11/2017
30/07/2018
20/09/2018',N'No',NULL,N'Si',N'Nota informativa del 6/11/2017 "Agli utilizzatori dei kit ELITe MGB in associazione alle piattaforme ELITe InGenius and ELITe GALAXY", di cui copia firmata allegata al CC.',N'No',NULL,N'ALTRO - Altro',NULL,N'no',N'No',NULL,N'No',NULL,N'No',NULL,N'Katia Arena','2017-08-31 00:00:00',NULL,'2018-11-06 00:00:00',N'No',NULL,NULL),
    (N'17-30','2017-06-06 00:00:00',N'Sabrina Bertini',N'Processo di produzione',N'Per la preparazione del prodotto 500-Internal Control codice IC500, si richiede di poter miscelare aliquote diverse di 950-IC500 (tutte
dello stesso lotto), prima dello step di diluizione. Dopo diluizione si otterrà un semilavorato che avrà codice 956-IC500 e che verrà suddiviso in minibulk da circa 30 kit (circa 45 ml) ciascuna, poi congelate. In occasione della dispensazione della prima minibulk verrà fatto il CQ, le successive dispensazioni non saranno sottoposte a CQ. Si rende necessaria la creazione di un nuovo modulo di diluizione del codice 956-IC500 e la modifica del modulo di produzione del codice 962-IC500 che deve prevedere soltanto la dispensazione del 956-IC500, l''invio al CQ della prima dispensazione e il confezionamento finale.',N'Il codice 950-IC500 viene fornito da Fast Track, nella misura di 15 ml, ogni volta che si effettua l''acquisto dei materiali necessari per la produzione di
962-RTS533ING, 962-RTS523ING,962-RTS553ING,962-RTS507ING,962-RTS548ING e spesso molte aliquote hanno lo stesso numero di lotto. La modifica richiesta, permettendo di riunire più aliquote dello stesso lotto, riduce il numero dei controlli CQ sul prodotto finito IC500.',N'll numero dei controlli CQ sul prodotto finito IC500 sarebbe maggiore.',NULL,N'MM
OM
QARA
QC
QM
RDM',N'Controllo qualità
Produzione
Ricerca e sviluppo
Sistema qualità',N'aggiunta di uno step intermedio di lavorazione
aggiunta di uno step intermedio di lavorazione
Diminuzione dei lotti di prodotto finito da controllare. Aggiornamento documentazione.
NON APPROVATO VEDERE MAIL 27/7/17 FARINAZZO ALLEGATA
Diminuzione dei lotti di prodotto finito da controllare. Aggiornamento documentazione.
NON APPROVATO VEDERE MAIL 27/7/17 FARINAZZO ALLEGATA
Diminuzione dei lotti di prodotto finito da controllare. Aggiornamento documentazione.
NON APPROVATO VEDERE MAIL 27/7/17 FARINAZZO ALLEGATA
Diminuzione dei lotti di prodotto finito da controllare. Aggiornamento documentazione.
NON APPROVATO VEDERE MAIL 27/7/17 FARINAZZO ALLEGATA
Messa in uso documenti',NULL,N'preparazione semilavorato codice 956-IC500 da suddividere in aliquote e congelare prima della dispensazione del prodotto finale
definizione dei criteri per l''assegnazione dell''utilizzabilità, in accordo con RDM
aggiornamento dei documenti IO09,17 MOD955-pXXX-10 e formazione
Creazione di un nuovo modulo MOD956-IC500. Modifica MOD962-IC500.
Messa in uso documenti',N'CC attualmente NON approvato da QC - cc17-30_non_approvato_al_27-7-17.pdf',N'QC
DS
QMA
PP
PT
DS',NULL,NULL,NULL,N'No',NULL,N'No',NULL,N'No',NULL,N'P1 - Preparazione / dispensazione / stoccaggio reagenti per PCR e RT e Estrazioni',NULL,N'verificare',N'No',NULL,N'No',NULL,N'No',NULL,N'Ferdinando Fiorini',NULL,NULL,NULL,N'No',NULL,NULL),
    (N'17-29','2017-05-16 00:00:00',N'Michela Boi',N'Documentazione SGQ',N'Creazione di una tabella che specifichi i documenti richiesti per la registrazione dei prodotti nei territori extraeuropei. ',N'Avere una documento unico che riporti tutte le informazioni necessarie per la registrazione dei prodotti nei territori extraeuropei.',N'Dover ricercare le informazioni in vari documenti.',NULL,N'QARA
QM',N'Sistema qualità',N'creazione di un nuovo documento che raccolta tutte le informazioni pregresse, richieste dai distributori e richieste dai regolamenti. Aggiornamento dei documenti correlati, formazione e messa in uso. 
creazione di un nuovo documento che raccolta tutte le informazioni pregresse, richieste dai distributori e richieste dai regolamenti. Aggiornamento dei documenti correlati, formazione e messa in uso. 
creazione di un nuovo documento che raccolta tutte le informazioni pregresse, richieste dai distributori e richieste dai regolamenti. Aggiornamento dei documenti correlati, formazione e messa in uso. ',N'07/06/2017
07/06/2017
07/06/2017',N'creazione di un nuovo documento che raccolta tutte le informazioni pregresse, richieste dai distributori e richieste dai regolamenti. Aggiornamento dei documenti correlati, formazione e messa in uso. 
PR05,06
formazione e messa in uso (mail formazione allegata del 25/7/2017, formazione del 25/7/2017 MOD18,02)',N'ALL2-PR05,06 - cc17-29_messa_in_uso.pdf
PR05,06 - cc17-29_messa_in_uso_1.pdf',N'QMA
QMA
QMA',N'23/05/2017
23/05/2017
23/05/2017',N'31/07/2017
31/07/2017
31/07/2017',N'25/07/2017
25/07/2017
25/07/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'ALTRO - Altro',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Michela Boi','2017-07-31 00:00:00',NULL,'2017-07-25 00:00:00',N'No',NULL,NULL),
    (N'17-28','2017-05-14 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'CHANGE REQUEST: update IO11,13 rev01 "ELITech ELITe InGeniusTM Preventive Maintenance" :
- changed to streamline process
- added clenaing of fiber optics lenses 
- remove steps found to not be necessary (pietting accuracy test, PCR blck temperature verification)

Update MOD11,19 rev01 "ELITech ELITe InGeniusTM Preventive Maintenance"
The module has been updated to refelct the changes implemented on IO11,13',N'RATIONALE ADDING VALUE OF CHANGE: streamline and improve preventive maintenance procedure',N'CONSEQUENCES IF THE CHANGE COULDN''T BE APPROVED: perform not necessary steps during preventive maintenance',NULL,N'QM
FAS',N'Assistenza strumenti esterni
Sistema qualità',N'ELITe InGenius preventive Maintenance
Document release',N'15/05/2017
15/05/2017',N'Training (Rilascio documentazione per permettere la corretta esecuzione della manutenzione preventiva su ELITe InGenius)
Document release, formazione MOD18,02 del 10/7/2017.',NULL,N'QMA',N'15/05/2017
15/05/2017',N'19/05/2017
15/05/2017',N'18/05/2017
18/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'ST - Elenco famiglie Strumenti',NULL,N'NA',N'No',NULL,N'No',NULL,N'No',NULL,N'Marcello Pedrazzini','2017-05-19 00:00:00',NULL,'2017-05-18 00:00:00',N'No',NULL,NULL);
INSERT INTO ZZ_list_change VALUES
    (N'17-27','2017-05-04 00:00:00',N'Katia Arena',N'Materia prima',N'IL ns fornitore VWR ci ha segnalato che le provette colorate utilizzate per la dispensazione dei BANG non sono più disponibili. Nel dettaglio i codici interessati dalla dismissione: ns cod interno 953-087 tubi Blu ( formato da 0.2);  ns cod interno 953-088 tubi Verdi ( formato da 0.2);  ns cod interno 953-091 tubi Gialli ( formato da 0.2);  ns cod interno 953-090 tubi Rosso ( formato da 0.2). ',N'Bisogna individuare fornitore/fornitori alternativi per poter continuare a mantenere la linea dei BANG attiva',N'Se non viene scelto un fornitore alternativo non siamo in grado di fornire i kit BANG ai clienti ',NULL,N'MM
QC
QM
RDM',N'Acquisti
Controllo qualità
Produzione
Ricerca e sviluppo',N'Preparazione del materiale per effettuare la prova.
Identificazione nuovi materiali plastici.
Stesura specifiche, e formazione.
Identificazione nuovi materiali plastici.
Stesura specifiche, e formazione.
Contattare altri rivenditori per verificare se hanno disponibili a magazzino le provette che il produttore Axygen ha dismesso; Verificare se le provette Abgene sono ancora disponibili : anche queste sono state dismesse.
Contattare altri rivenditori per verificare se hanno disponibili a magazzino le provette che il produttore Axygen ha dismesso; Verificare se le provette Abgene sono ancora disponibili : anche queste sono state dismesse.
verificare le prestazioni di un lotto di prova con i nuovi tubi
verificare le prestazioni di un lotto di prova con i nuovi tubi',N'31/05/2017
31/05/2017
31/05/2017
31/05/2017
31/05/2017
13/06/2017
13/06/2017',N'1) Identificazione nuovi materiali plastici.
2) Modifica delle specifiche
3) Formazione.
SPECIFICHE IN USO AL 5/10/2017 E FORMAZIONE DEL 4/10/2017 (vedere mail allegata)
Contattare altri rivenditori (Sterograf, Fischer e thermo)
acquistare un campione
verificare le prestazioni di un lotto di prova con i nuovi tubi
verificare ed eventualmente aggiornare i moduli di produzione dei BANG. i moduli di produzione non devono essere modificati',N'- - cc17-27_rivenditori_contattati_per_valutare_l_acquisto.pdf
messa in uso specifiche tubi bang - cc17-27_messa_in_uso_spec953-xxx_tubi_1.pdf
Test Tubi TF - test_validazione_nuovi_tubi_02_ml_fischer_scientific.pdf',N'PW
RDM
RT
PW
QC
DS',N'31/05/2017
31/05/2017
31/05/2017
31/05/2017
31/05/2017',N'31/08/2017
30/06/2017
30/06/2017
31/07/2017
31/08/2017',N'09/10/2017
21/06/2017
30/06/2017
04/09/2017
14/11/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'A - Gestione degli Acquisti',NULL,N'non c''è impatto sui risk assessment',N'No',NULL,N'No',NULL,N'No',NULL,N'Katia Arena','2017-08-31 00:00:00',NULL,'2017-11-14 00:00:00',N'No',NULL,NULL),
    (N'17-26','2017-05-02 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'Revisione del Modulo 19,04 "Rapporto Assistenza Tecnica_Service Report"',N'Migliorare il report utilizzato al termine delle attività di assistenza tecnica ed includere la firma del cliente sul modulo',N'Mancanza firma accettazione delle attività svolte da parte del cliente',NULL,N'QM',N'Assistenza strumenti esterni
Sistema qualità',N'messa in uso documento
aggiornamento modulo',N'15/05/2017
15/05/2017',N'messa in uso documento
aggiornamento modulo, formazione MOD18,02 del 10/7/2017.',NULL,N'QMA
GSC',N'15/05/2017
15/05/2017',N'30/05/2017
30/05/2017',N'18/05/2017
18/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'ST - Elenco famiglie Strumenti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Marcello Pedrazzini','2017-05-30 00:00:00',NULL,'2017-05-18 00:00:00',N'No',NULL,NULL),
    (N'17-25','2017-04-21 00:00:00',N'Federica Farinazzo',N'Documentazione SGQ',N'Modifica dei moduli di CQ per i prodotti ingenius al fien di apportare le modifiche delle ultime versioni dei documenti. Tra questi:
- MOD10,12-RTS200ING-CTR200ING, al fine di inserire i dati di verifica tecnica partendo dal file di export dell''ingenius.
-MOD10,13-RTD000ING-CTRD000ING e MOD10,13-RTS200ING-CTR200ING la cui preparazione è fatta con l''utilizzo di xls, 
',N'La modifica agevola notevolemente la valutazione dei dati rispetto ai creiteri del prodotto, riducendo i tempi e rischi di errori. Tale modalità è già utilizzata per i prodotti ingenius usciti successivamente a CRE.
L''utilizzo di excel per la stesura delle schede apputni a partire da R&D permette la verifica sia da parte di R&D che di DS dei calcoli delle diluizioni. VI è inoltre una semplificazione delle tabelle contenenti le impostazioni per lo strumento. ',N'Tempi ecessivamente lunghi, maggiori possibilità di errori, attualmente viene incollata la foto del report in quanto era inizialemente l''ulnica alternativa a copiare manualemtne i dati. Dato che oira è possibile espertare i dati la modifica è auspicabile.
L''utilizzo di excel per la stesura dei moduli scehda appunti permette di agevoalre i controlli e semplificare la stesura',NULL,N'QC
QM
RDM',N'Controllo qualità',N'Modifica dei moduli di CQ
Modifica dei moduli di CQ
Modifica dei moduli di CQ',N'25/05/2017
25/05/2017
25/05/2017',N'Modifica dei moduli già in uso dei prodotti Ingenius 
Documento (nome)	Documento in bozza RIF.
MOD10,12-RTS200ING-CTR200ING	CC17-25
MOD10,13-RTS200ING-CTR200ING	CC17-25
MOD10,13-RTSD00ING-CTRD00ING	CC17-25
fornire bozza dei documenti per i nuovi progetti a RT
formazione (DS MOD18,02 del 24/05/2017) e messa in uso (QMA)',N'comunicazione che i nuovi documenti sono in formato excell - cc17-25_messa_in_uso.pdf
messa in uso documenti - cc17-25_messa_in_uso_1.pdf',N'DS
QC
DS
RT
DS
QMA',N'25/05/2017
25/05/2017
25/05/2017',N'31/05/2017
31/05/2017
31/05/2017',N'25/05/2017
25/05/2017
24/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'CQ - Controllo Qualità prodotti',NULL,N'nessun impatto',N'No',NULL,N'No',NULL,N'No',NULL,N'Federica Farinazzo','2017-05-31 00:00:00',NULL,'2017-05-25 00:00:00',N'No',NULL,NULL),
    (N'17-24','2017-04-18 00:00:00',N'Roberta  Paviolo',N'Nome/codice del prodotto',N'CHANGE REQUEST: the validation of MRSA/SA Elite MGB kit, ref. M80035, with ELITe InGenius Instrument was performed with "CPE-Internal control". Actually the materials provided in the product are: "MRSA/SA PCR Mix" and "MRSA/SA PCR Internal Control". It is required eliminate the "MRSA/SA Internal control", ref. M400385, from the kit and use the "CPE-Internal Control". Update the packaging of MRSA/SA Elite MGB kit, ref. M800351: no paper box, no package insert and eliminate the  Internal control from the kit. Update packaging of MRSA/SA Elite Positve Control, ref. M800356 ) : no paper box, no package insert and reduce the number of tubes from 4 x 65 ul, to 2 x160 uL. ',N'Lean packaging and compatibility with ELITe InGenius Instrument.',N'MRSA/SA Internal control is not compatible with ELITe InGenius Instrument. For MRSA/SA Elite Positve Control customers should join several tubes to use with ELITe InGenius Instrument.',NULL,N'BC
SPM
RDO
MM
OM
QARA
QC
QM
RDM',N'Acquisti
Amministrazione
Budgeting control
Controllo qualità
Marketing
Operation Site
Produzione
Regolatorio
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Stability of the positive control:
A) filling a higher reagent volume in the same tube: the change has no impact on stability (expiration date),
B) higher reagent volume lead to more freeze/thaw cycles: check the freeze/thawing tests carried out, perform new tests if needed.
"CPE-Internal Control" is compatible with instrument "NucliSENS easyMAG" (no specific validation required).
Relase document
Relase document
New process of production (lean Packaging and new filling volume).
Comunication to Distrubutors.
Update the risk assessment.
Update the IFU and the labels.
Update the Product Technical File of MRSA/SA kit.
Comunication to Distrubutors.
Update the risk assessment.
Update the IFU and the labels.
Update the Product Technical File of MRSA/SA kit.
Comunication to Distrubutors.
Update the risk assessment.
Update the IFU and the labels.
Update the Product Technical File of MRSA/SA kit.
Comunication to Distrubutors.
Update the risk assessment.
Update the IFU and the labels.
Update',N'24/05/2017
24/05/2017
24/05/2017
24/05/2017
24/05/2017
24/05/2017
24/05/2017
24/05/2017
07/06/2017
07/06/2017
07/06/2017
07/06/2017
19/07/2017
19/07/2017
19/07/2017',N'Check the freeze/thawing tests carried out in Bothell.
Perform new tests if needed.
Relase document.
TRAINING MOD18,02 del 20/04/2018, MOD18,02 del 10-12/10/2017 
Comunication to Distrubutors.
Update the risk assessment. View VMP PR
Update the IFU and the labels.
Update the Product Technical File of MRSA/SA kit (DMRI).
Update Marketing material about MRSA
Upadate NAV
Lean packaging of MRS/SA: eliminate "MRSA/SA Internal Control" from the kit and use only the "CPE-Interna Control" with "MRSA/SA PCR Mix" (update the IFU)..
Upadate NAV
Comunication to Distrubutors (where it is registrated).',N'nessun test eseguito - cc17-24_nessun_test_eseguito.txt
MOD962-M800351rev00, MOD955-M300796rev01, MOD955-M300795rev01, MOD962-M800356rev00, MOD10,12-MRSArev01, MOD10,13-MRSArev02 - cc17-24_messa_in_uso_moduli_produzione_e_cq.pdf
presente il VMP sulla preparazione del lotto unico. - mod09,30_01_pr_produzione_lotto_unico.pdf
CC24-17_messa in uso IFU M800351rev7 and M800356 rev5 - cc17-24_messa_in_uso_ifu_m800351rev7_and_m800356_rev5.msg
dmri2013-003_02 - cc17-24_dmri2013-003_02.docx
CC24-17_messa in uso IFU M800351rev7 and M800356 rev5 - cc17-24_messa_in_uso_ifu_m800351rev7_and_m800356_rev5_1.msg',N'RT
QC
QMA
QMA
QMA
QMA
MS
BC
BC
QMA
QMA',N'24/05/2017
19/07/2017
19/07/2017
07/06/2017
07/06/2017
07/06/2017
07/06/2017
19/07/2017
19/07/2017
19/07/2017',N'20/09/2017
10/10/2017
31/10/2017
10/10/2017
10/10/2017
31/10/2017
30/09/2017
10/10/2017
10/10/2017
10/10/2017
31/07/2017',N'07/10/2020
11/05/2018
11/05/2018
16/07/2018
23/10/2018
23/10/2018
16/07/2018
11/05/2018
23/10/2018',N'Si',N'mrsa/sa',N'Si',N'Comunication the changes to the customers.',N'No',N'nessun impatto',N'R&D - Svilupppo e commercializzazione prodotti',N'R&D1.9 - Fase di Verifica e Validazione: Attività',N'-',N'No',N'comunicare ai distributori',N'No',N'nessun virtula manufacturer',N'No',N'na',N'Michela Boi','2017-10-31 00:00:00',NULL,'2020-10-07 00:00:00',N'No',NULL,NULL),
    (N'17-23','2017-04-12 00:00:00',N'Lucia  Liofante',N'Documentazione SGQ',N'1) Modifica elenco prodotti associati a test di caratterizzazione del campione plasma EDTA su MOD10,18: eliminazione codici EXTG01/BRK200/RTS076-M/RTS000 e inserimento codice RTS076PLD (a seguito delle validazione di questo prodotto su ELITe GALAXY)
2) Aggiornamento del paragrafo 2.3 "Test di caratterizzazione" di IO10,02, con inserimento documentazione utilizzata per caratterizzazione emocomponeti',N'1) Risparmio di tempo/risorse per la caratterizzazione del campione plasma EDTA: utilizzando RTS076PLD invece di RTS076-M è possibile eseguire un''unica estrazione del campione di plasma con ELITe GALAXY e tastarne la negatività per entrambi i target di interesse del CQ, ovvero CMV ed Enterovirus
2) Maggiore chiarezza nella documentazione da utilizzare per la caratterizzazione dei campioni biologici',N'1) Per caratterizzare il campione plasma EDTA si dovrebbero continuare al allestire due diverse sessioni di estrazione, una automatica con ELITe GALAXY per CMV e una manuale con EXTG01 per Enterovirus
2) Difficoltà nell''identificazione della documentazione da utilizzare per la caratterizzazione dei campioni biologici',NULL,N'SPM
QC
QM
RDM',N'Controllo qualità
Ordini
Ricerca e sviluppo
Sistema qualità
Validazioni',N'Risparmio di tempo/risorse per la caratterizzazione del campione plasma EDTA: utilizzando RTS076PLD invece di RTS076-M è possibile eseguire un''unica estrazione del campione di plasma con ELITe GALAXY e tastarne la negatività per entrambi i target di interesse del CQ, ovvero CMV ed Enterovirus
2) Maggiore chiarezza nella documentazione da utilizzare per la caratterizzazione dei campioni biologici
Risparmio di tempo/risorse per la caratterizzazione del campione plasma EDTA: utilizzando RTS076PLD invece di RTS076-M è possibile eseguire un''unica estrazione del campione di plasma con ELITe GALAXY e tastarne la negatività per entrambi i target di interesse del CQ, ovvero CMV ed Enterovirus
2) Maggiore chiarezza nella documentazione da utilizzare per la caratterizzazione dei campioni biologici
Risparmio di tempo/risorse per la caratterizzazione del campione plasma EDTA: utilizzando RTS076PLD invece di RTS076-M è possibile eseguire un''unica estrazione del campione di plasma con ELITe GALAXY e tastarne la n',N'15/05/2017
15/05/2017
15/05/2017
15/05/2017
15/05/2017
18/04/2017',N'1) Modifica elenco prodotti associati a test di caratterizzazione del campione plasma EDTA su MOD10,18: eliminazione codici EXTG01/BRK200/RTS076-M/RTS000 e inserimento codice RTS076PLD (a seguito delle validazione di questo prodotto su ELITe GALAXY)
2) Aggiornamento del paragrafo 2.3 "Test di caratterizzazione" di IO10,02, con inserimento documentazione utilizzata per caratterizzazione emocomponeti
formazione al personale del controllo qualità, ricerca e sviluppo e validazioni (MOD18,02 del 22/05/2017)
messa in uso documenti',N'messa in uso moduli - cc35-16_cc8-17_cc17-23_rac29-16_nc23-17_messa_in_uso.pdf
messa in uso IO10,02, rev03 - cc17-23_messa_in_uso_io1002.pdf
mail: in uso MOD10,18 rev11 - sgq_messa_in_uso_documenti_diluizioni_nc23-17_cc35-16_cc8-17_rac29-16_cc17-23.msg',N'DS
QMA
DS
DS
CC',N'15/05/2017
15/05/2017
15/05/2017
15/05/2017',N'15/05/2017
15/05/2017
22/05/2017
30/06/2017',N'15/05/2017
25/05/2017
22/05/2017
25/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'CQ - Controllo Qualità prodotti',NULL,N'-',N'No',NULL,N'No',NULL,N'No',NULL,N'Lucia  Liofante','2017-06-30 00:00:00',NULL,'2017-05-25 00:00:00',N'No',NULL,NULL),
    (N'17-22','2017-04-04 00:00:00',N'Marcello Pedrazzini',N'Documentazione SGQ',N'CHANGE REQUEST: update IO11,12 rev00 "ELITech ELITe InGeniusTM Installation and Qualification" :
- changed to streamline process
- added lubricatoin stesp (single and 12 nozzles) 
- added DZ position teaching
- remove steps found to not be necessary (pietting accuracy test, PCR blck temperature verification)

Update MOD11,16 rev00 "ELITech ELITe InGeniusTM Installation and Qualification"
The module has been updated to include the changes implemented on IO11,12',N'RATIONALE ADDING VALUE OF CHANGE: streamline and improve installation procedure',N'CONSEQUENCES IF THE CHANGE COULDN''T BE APPROVED: perform not necessary steps during instrument installation',NULL,N'QM
FAS',N'Assistenza strumenti esterni
Sistema qualità',N'Documents release.
Instrument installation',N'06/04/2017
03/05/2017',N'Documents release.
Release installation instruction, to the field via TSB.',N'TSB_012_installation and qualification procedure - elite_ingenius_tsb_012_-_rev_a_-_installation_and_qualification_procedure_with_signature.pdf',N'QM
GSC
FAS',N'06/04/2017
03/05/2017',N'31/05/2017
31/05/2017',N'03/05/2017
30/05/2017',N'No',NULL,N'No',NULL,N'No',NULL,N'ST - Elenco famiglie Strumenti',NULL,N'no impact',N'No',NULL,N'No',NULL,N'No',NULL,N'Marcello Pedrazzini','2017-05-31 00:00:00',NULL,'2017-05-30 00:00:00',N'No',NULL,NULL);

﻿#region Using
using System;
using System.ComponentModel;
using System.Data.SqlClient;
#endregion

namespace EDFS.Db
{
    public class DataMain
    {
        public static string ByteArrayToString(byte[] ba)
        {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }
        public int? RunSetupID { get; set; }
        public int? TrackNumber { get; set; }
        public byte[] SampleID { get; set; }
        public string SampleIDString
        {
            get
            {
                return ByteArrayToString(SampleID);
            }
            set
            {
           
            }
        }
        public int? GeneralAssayID { get; set; }
        public string AssayName { get; set; }
        public int? SampleTypes{ get; set; }
        public string SampleTypesDecode
        {
            get
            {
                switch (SampleTypes)
                {
                    case 0: return "Sample";
                    case 1: return "Calibrator";
                    case 2: return "Control";
                    default: return SampleTypes.ToString();
                }
            }
            set
            {

            }
        }
        public string SampleMatrixName { get; set; }
        public int Process { get; set; }
        public string ProcessDecode
        {
            get
            {
                switch (Process)
                {
                    case 0: return "PCR";
                    case 1: return "Extraction PCR";
                    case 2: return "Extraction";
                    default: return Process.ToString();
                }
            }
            set
            {

            }
        }
        public int PositionToPlaceSample { get; set; }
        public int Sonication { get; set; }
        public int? SonicationOnTime { get; set; }
        public int? SonicationOffTime { get; set; }
        public int? SonicationCycle { get; set; }
        public int? Melt { get; set; }
        public string MeltDecode
        {
            get
            {
                switch (Melt)
                {
                    case 0: return "No Melting";
                    case 1: return "Melting";
                    default: return Melt.ToString();
                }
            }
            set
            {

            }
        }
        public int? RefEluteTrack { get; set; }
        public int? DilutionFactor { get; set; }
        public string SerialNumber { get; set; }
        public string PCRReactionCassetteSerialNumber { get; set; }
        public string ExtractionCassetteLot { get; set; }
        public DateTime? ExtractionCassetteExpiryDate { get; set; }
        public string ExtractionCassetteSerialNumber { get; set; }
        public string ReagentLot { get; set; }
        public DateTime? ReagentExpiryDate { get; set; }
        public int? ReagentTubeSize { get; set; }
        public string ICLot { get; set; }
        public DateTime? IcExpiryDate { get; set; }
        public int? IcTubeSize { get; set; }
        public string CalibratorLot { get; set; }
        public DateTime? CalibratorExpiryDate { get; set; }
        public string ControlLot { get; set; }
        public DateTime? ControlExpiryDate { get; set; }
        public int? MeasuredCalibrationResultID { get; set; }
        public int? MeasuredControlResultID { get; set; }
        public string AssayDetail { get; set; }
        public int? LISUploadSelected { get; set; }
        public int? LISStatusv { get; set; }
        public string ApprovalUserName { get; set; }
        public int? ApprovalUserRole { get; set; }
        public string ApprovalUserRoleDecode
        {
            get
            {
                switch (ApprovalUserRole)
                {
                    case 0: return "Service";
                    case 1: return "Admin";
                    case 2: return "Analyst";
                    default: return ApprovalUserRole.ToString();
                }
            }
            set
            {

            }
        }
        public DateTime? ApprovalDateTime { get; set; }
        public int? ApprovalStatus { get; set; }
        public string AssayDetailForCopiesPerMl { get; set; }
        public string AssayDetailForGeqPerMl { get; set; }
        public string AssayDetailForIuPerMl { get; set; }
        public string AssayDetailShortening { get; set; }
        public string AssayDetailShorteningForCopiesPerMl { get; set; }
        public string AssayDetailShorteningForGeqPerMl { get; set; }
        public string AssayDetailShorteningForIuPerMl { get; set; }
        public string AssayDetailShorteningUnit { get; set; }
        public string RunName { get; set; }
        public string UserName { get; set; }
        public DateTime? StartRunTime { get; set; }
        public DateTime? EndRunTime { get; set; }
        public DateTime? AbortTime { get; set; }
        public int? ExtractionInputVolume { get; set; }
        public int? ExtractedEluteVolume { get; set; }
        public string InventoryBlockConfirmationMethod { get; set; }
        public int? InventoryBlockNumber { get; set; }
        public string InventoryBlockName { get; set; }
        public string InventoryBlockBarcode { get; set; }
        public static string GetGeneralExtraction()
        {
            string sql;
            sql = "SELECT RunSetupID, TrackNumber, SampleID, GeneralAssayID, AssayName, SampleTypes, SampleMatrixName, ";
            sql += " Process, PositionToPlaceSample, Sonication, SonicationOnTime, SonicationOffTime, SonicationCycle, Melt, RefEluteTrack, DilutionFactor, ";
            sql += " SerialNumber, PCRReactionCassetteSerialNumber, ExtractionCassetteLot, ExtractionCassetteExpiryDate, ExtractionCassetteSerialNumber, ReagentLot,";
            sql += " ReagentExpiryDate, ReagentTubeSize, ICLot, IcExpiryDate, IcTubeSize, CalibratorLot, CalibratorExpiryDate, ";
            sql += " ControlLot, ControlExpiryDate, MeasuredCalibrationResultID, MeasuredControlResultID, ";
            sql += " AssayDetail, LISUploadSelected, LISStatus, ApprovalUserName, ApprovalUserRole, ApprovalDateTime, ApprovalStatus, ";
            sql += " ApprovalUserNameForExpiredReagent, ApprovalUserRoleForExpiredReagent, ApprovalUserNameForExpiredIC, ApprovalUserRoleForExpiredIC, ApprovalUserNameForExpiredControl, ";
            sql += " AssayDetailForCopiesPerMl, AssayDetailForGeqPerMl, AssayDetailForIuPerMl, AssayDetailShortening, AssayDetailShorteningForCopiesPerMl, AssayDetailShorteningForGeqPerMl, ";
            sql += " AssayDetailShorteningForIuPerMl, AssayDetailShorteningUnit,RunName,UserName,UserRole,StartRunTime,EndRunTime,AbortTime,ExtractionInputVolume,ExtractedEluteVolume,";
            sql += " InventoryBlockConfirmationMethod,InventoryBlockNumber,InventoryBlockName,InventoryBlockBarcode,WasInventoryBlockBarcodeRead ";
            sql += " FROM [RunResults].[dbo].[TrackSetups] inner join[RunResults].[dbo].[RunSetups] on([RunResults].[dbo].[TrackSetups].RunSetupID =  [RunResults].[dbo].[RunSetups].Id) ";
            sql += " ORDER BY StartRunTime Desc ";
            return sql;
        }
        public static string GetGeneralExtraction(DateTime startDate, DateTime endDate)
        {
            string sql;
            sql = "SELECT RunSetupID, TrackNumber, SampleID, GeneralAssayID, AssayName, SampleTypes, SampleMatrixName, ";
            sql += " Process, PositionToPlaceSample, Sonication, SonicationOnTime, SonicationOffTime, SonicationCycle, Melt, RefEluteTrack, DilutionFactor, ";
            sql += " SerialNumber, PCRReactionCassetteSerialNumber, ExtractionCassetteLot, ExtractionCassetteExpiryDate, ExtractionCassetteSerialNumber, ReagentLot,";
            sql += " ReagentExpiryDate, ReagentTubeSize, ICLot, IcExpiryDate, IcTubeSize, CalibratorLot, CalibratorExpiryDate, ";
            sql += " ControlLot, ControlExpiryDate, MeasuredCalibrationResultID, MeasuredControlResultID, ";
            sql += " AssayDetail, LISUploadSelected, LISStatus, ApprovalUserName, ApprovalUserRole, ApprovalDateTime, ApprovalStatus, ";
            sql += " ApprovalUserNameForExpiredReagent, ApprovalUserRoleForExpiredReagent, ApprovalUserNameForExpiredIC, ApprovalUserRoleForExpiredIC, ApprovalUserNameForExpiredControl, ";
            sql += " AssayDetailForCopiesPerMl, AssayDetailForGeqPerMl, AssayDetailForIuPerMl, AssayDetailShortening, AssayDetailShorteningForCopiesPerMl, AssayDetailShorteningForGeqPerMl, ";
            sql += " AssayDetailShorteningForIuPerMl, AssayDetailShorteningUnit,RunName,UserName,UserRole,StartRunTime,EndRunTime,AbortTime,ExtractionInputVolume,ExtractedEluteVolume,";
            sql += " InventoryBlockConfirmationMethod,InventoryBlockNumber,InventoryBlockName,InventoryBlockBarcode,WasInventoryBlockBarcodeRead ";
            sql += " FROM [RunResults].[dbo].[TrackSetups] inner join[RunResults].[dbo].[RunSetups] on([RunResults].[dbo].[TrackSetups].RunSetupID =  [RunResults].[dbo].[RunSetups].Id) ";
            sql += $" WHERE StartRunTime BETWEEN '{startDate.ToString(Properties.Settings.Default.CountryDateFormat)}' AND '{endDate.ToString(Properties.Settings.Default.CountryDateFormat)}'";
            sql += " ORDER BY StartRunTime Desc ";
            return sql;
        }

    }
    public class DataMainCollection : BindingList<DataMain>
    {

    }
}

DECLARE @CLAIMID AS INT = 1
DECLARE @SCRIPT AS NVARCHAR(50) = 'DNSSampleDisp.scr' 
DECLARE @SESSION AS INT = 1
--set @SCRIPT = 'ICDISP.scr' 
--set @SCRIPT = 'PROVA.scr' 

select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueInit as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
  and  CFG.CheckName = 'Init' 
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx < 0  -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueAsp as Pressure, ValueAsp as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'Asp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueDisp as Pressure, NULL as asp, ValueDisp as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'Disp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueTipon as Pressure, NULL as asp, NULL as disp, ValueTipon as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'TipOn'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueTipOff as Pressure, NULL as asp, NULL as disp, NULL as tipon, ValueTipOff as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'TipOff'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak1 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, ValueLeak1 as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'leak1'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak2 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, ValueLeak2 as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'Leak2'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak3 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, ValueLeak3 as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'leak3'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueClotAsp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, ValueClotAsp as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'ClotAsp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueClotDisp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, ValueClotDisp as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = 1 and CFG.CheckName = 'ClotDisp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx BETWEEN 1 AND 12 -- and TmstRif = xxxxxxx 
UNION 
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueAsp as Pressure, ValueAsp as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'Asp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueDisp as Pressure, NULL as asp, ValueDisp as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'Disp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueTipon as Pressure, NULL as asp, NULL as disp, ValueTipon as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'TipOn'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueTipOff as Pressure, NULL as asp, NULL as disp, NULL as tipon, ValueTipOff as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'TipOff'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak1 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, ValueLeak1 as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'leak1'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak2 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, ValueLeak2 as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'Leak2'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueLeak3 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, ValueLeak3 as leak3, null as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'leak3'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueClotAsp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, ValueClotAsp as ClotAsp, null as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'ClotAsp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 
UNION
select SN.trackIdx, CFG.CheckName, CFG.CheckSeq, SN.ClaimPressureSNId, ValueInit as init, ValueClotDisp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, ValueClotDisp as ClotDisp from ClaimPressuresSN SN
inner join ClaimCheckCfgs CFG on CFG.script = SN.script and CFG.TrackIdx = SN.trackIdx
                     and CFG.CheckName = 'ClotDisp'
where SN.claimid = @CLAIMID and SN.script = @SCRIPT and SessionNUmber = @SESSION and SN.trackIdx in (0, 99) -- and TmstRif = xxxxxxx 

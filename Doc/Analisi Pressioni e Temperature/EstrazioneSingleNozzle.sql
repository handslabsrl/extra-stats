DECLARE @CLAIMID AS INT = 555
DECLARE @SCRIPT AS NVARCHAR(50) = 'DNSSampleDisp.scr' 
DECLARE @SESSION AS INT = 1
--set @SCRIPT = 'ICDISP.scr' 
set @SCRIPT = 'PROVA.scr' 

select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueInit as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
  and  checkcfgs.checkid = 'Init' 
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx < 0  -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueAsp as Pressure, ValueAsp as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'Asp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueDisp as Pressure, NULL as asp, ValueDisp as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'Disp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueTipon as Pressure, NULL as asp, NULL as disp, ValueTipon as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'TipOn'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueTipOff as Pressure, NULL as asp, NULL as disp, NULL as tipon, ValueTipOff as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'TipOff'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueLeak1 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, ValueLeak1 as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'leak1'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueLeak2 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, valueLeak2 as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'Leak2'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueLeak3 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, valueLeak3 as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'leak3'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueClotAsp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, valueClotAsp as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'ClotAsp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueClotDisp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, valueClotDisp as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = 1 and checkcfgs.checkid = 'ClotDisp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx BETWEEN 1 AND 12 -- and tmststart = xxxxxxx 
UNION 
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueAsp as Pressure, ValueAsp as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'Asp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueDisp as Pressure, NULL as asp, ValueDisp as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'Disp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueTipon as Pressure, NULL as asp, NULL as disp, ValueTipon as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'TipOn'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueTipOff as Pressure, NULL as asp, NULL as disp, NULL as tipon, ValueTipOff as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'TipOff'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, ValueLeak1 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, ValueLeak1 as leak1, NULL as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'leak1'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueLeak2 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, valueLeak2 as leak2, null as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'Leak2'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueLeak3 as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, valueLeak3 as leak3, null as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'leak3'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueClotAsp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, valueClotAsp as ClotAsp, null as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'ClotAsp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 
UNION
select trackIdx, CheckCfgs.CheckId, checkcfgs.CheckSeq, ClaimPressureSN.Id, ValueInit as init, valueClotDisp as Pressure, NULL as asp, NULL as disp, NULL as tipon, NULL as tipoff, NULL as leak1, NULL as leak2, null as leak3, null as ClotAsp, valueClotDisp as ClotDisp from ClaimPressureSN 
inner join checkcfgs on checkcfgs.script = ClaimPressureSN.script and checkcfgs.CheckIdx = ClaimPressureSN.trackIdx
                     and checkcfgs.checkid = 'ClotDisp'
where ClaimPressureSN.claimid = @CLAIMID and ClaimPressureSN.script = @SCRIPT and SessionNUmber = @SESSION and TrackIdx in (0, 99) -- and tmststart = xxxxxxx 

ORDER BY  TrackIdx, CheckSeq
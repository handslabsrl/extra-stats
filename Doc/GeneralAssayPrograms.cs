﻿#region Using
using NPoco;
using System;
using System.ComponentModel;
using System.Data.SqlClient;
#endregion

namespace EDFS.Db
{
    public class GAssayPrograms
    {
        public int ID { get; set; }
        public string AssayName { get; set; }
        public int SampleTypes { get; set; }
        public string SampleTypesDecode
        {
            get
            {
                switch (SampleTypes)
                {
                    case 0: return "Sample";
                    case 1: return "Calibrator";
                    case 2: return "Control";
                    default: return SampleTypes.ToString();
                }
            }
            set
            {

            }
        }
        public int IVDCleared { get; set; }
        public int CtsAndTmOnly { get; set; }
        public string Pathogen_TargetName { get; set; }
        public int ExtractionInputVolume { get; set; }
        public int ExtractedEluteVolume { get; set; }
        public int ResultsScaling { get; set; }
        public int SonicationOnTime { get; set; }
        public int SonicationOffTime { get; set; }
        public int SonicationCycle { get; set; }
        public String ExtractionCassetteID { get; set; }
        public string PCRCassetteID { get; set; }
        public int PCRInputEluateVolume { get; set; }
        public int PreCycleStepNumber { get; set; }
        public int PCRAmplificationCycles { get; set; }
        public int PCRAmplificationSteps        { get; set; }
        public int MeltRequired { get; set; }
        public int MeltingOptional { get; set; }
        public int StartRampTemperature { get; set; }
        public int EndRampTemperature { get; set; }
        public double RampRate { get; set; }
        public int UpperTolerance { get; set; }
        public int LowerTolerance { get; set; }
        public int OffSet { get; set; }
        public double ConversionFactorIU { get; set; }
        public string UserName { get; set; }
        public DateTime RegisteredDate { get; set; }
        public int? SampleMatrix_Id { get; set; }
        public string AssayParameterToolVersion { get; set; }
        public string Serial { get; set; }
        public static string GetGeneralExtraction(string serial)
        {
            string sql;
            sql = $"SELECT [AssaySettings].[dbo].[GeneralAssayPrograms].*, '{serial}' as Serial ";
            sql += " FROM [AssaySettings].[dbo].[GeneralAssayPrograms]  ";
            sql += " ORDER BY AssayName Asc ";
            return sql;
        }
    }
    public class GAssayProgramsCollection : BindingList<GAssayPrograms>
    {

    }
}
